package bitevo.Zetal.LoMaS.Specializations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import bitevo.Zetal.LoMaS.BigFMod.BigFMod;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockSap;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockSapling;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockSpecBrewingStand;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockSpecFurnace;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockSpecWorkbench;
import bitevo.Zetal.LoMaS.Specializations.Capabilities.IPlayerCapability;
import bitevo.Zetal.LoMaS.Specializations.Capabilities.PlayerCapabilityStorage;
import bitevo.Zetal.LoMaS.Specializations.Enchantments.EnchantmentBowSkill;
import bitevo.Zetal.LoMaS.Specializations.Enchantments.EnchantmentHaste;
import bitevo.Zetal.LoMaS.Specializations.Entities.EntityCustomArrow;
import bitevo.Zetal.LoMaS.Specializations.Entities.EntityCustomXPOrb;
import bitevo.Zetal.LoMaS.Specializations.Handler.ChatEventContainerClass;
import bitevo.Zetal.LoMaS.Specializations.Handler.CommandCustomChat;
import bitevo.Zetal.LoMaS.Specializations.Handler.CommandSpecializations;
import bitevo.Zetal.LoMaS.Specializations.Handler.FMLHandler;
import bitevo.Zetal.LoMaS.Specializations.Handler.SCMClientEventContainerClass;
import bitevo.Zetal.LoMaS.Specializations.Handler.SCMEventHandler;
import bitevo.Zetal.LoMaS.Specializations.Handler.TickHandler;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemArrow;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemBow;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemCustomArmor;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemCustomAxe;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemCustomHoe;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemCustomPickaxe;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemCustomSpade;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemCustomSword;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemEmeraldHeart;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemIronHeart;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemSapBottle;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemBlock.ItemSapling;
import bitevo.Zetal.LoMaS.Specializations.Message.AbilityMessage;
import bitevo.Zetal.LoMaS.Specializations.Message.AbilityMessageHandler;
import bitevo.Zetal.LoMaS.Specializations.Message.AnimalMessage;
import bitevo.Zetal.LoMaS.Specializations.Message.AnimalMessageHandler;
import bitevo.Zetal.LoMaS.Specializations.Message.ArrowMessage;
import bitevo.Zetal.LoMaS.Specializations.Message.ArrowMessageHandler;
import bitevo.Zetal.LoMaS.Specializations.Message.CraftMessage;
import bitevo.Zetal.LoMaS.Specializations.Message.CraftMessageHandler;
import bitevo.Zetal.LoMaS.Specializations.Message.SpecializationMessage;
import bitevo.Zetal.LoMaS.Specializations.Message.SpecializationMessageHandler;
import bitevo.Zetal.LoMaS.Specializations.Message.TimedUsageMessage;
import bitevo.Zetal.LoMaS.Specializations.Message.TimedUsageMessageHandler;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntityPlant;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySapBlock;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySpecBrewingStand;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySpecFurnace;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySpecWorkbench;
import net.minecraft.block.Block;
import net.minecraft.block.BlockDispenser;
import net.minecraft.block.BlockLeaves;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.BlockLog;
import net.minecraft.block.BlockOre;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.command.ServerCommandManager;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.dispenser.BehaviorProjectileDispense;
import net.minecraft.dispenser.IBlockSource;
import net.minecraft.dispenser.IPosition;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IProjectile;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.entity.passive.EntityOcelot;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.entity.passive.EntityRabbit;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.MobEffects;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemBlockSpecial;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.RecipeRepairItem;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.ISpecialArmor;
import net.minecraftforge.common.ISpecialArmor.ArmorProperties;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;

@Mod(modid = SpecializationsMod.modid, name = "Specializations", version = "0.1")
public class SpecializationsMod
{
	public static final String modid = "lomas_scm";
	public static MinecraftServer server;
	@SidedProxy(clientSide = "bitevo.Zetal.LoMaS.Specializations.SCMClientProxy", serverSide = "bitevo.Zetal.LoMaS.Specializations.SCMCommonProxy")
	public static SCMCommonProxy proxy;
	@Instance(modid)
	public static SpecializationsMod instance;

	public static ToolMaterial HARDWOOD_TOOL = EnumHelper.addToolMaterial("Hardwood", 0, 163, 2.0F, 0, 20);
	public static ToolMaterial OBSIDIAN_TOOL = EnumHelper.addToolMaterial("Obsidian", 3, 3122, 10.0F, 4, 3);
	public static ArmorMaterial OBSIDIAN_ARMOR = EnumHelper.addArmorMaterial("Obsidian", "obsidian_layer_1", 45, new int[] { 4, 9, 7, 4 }, 4, SoundEvents.ITEM_ARMOR_EQUIP_CHAIN, 3);

	public static ItemSapBottle SAPBOTTLE = (ItemSapBottle) (new ItemSapBottle()).setUnlocalizedName("sapBottle").setCreativeTab(CreativeTabs.MATERIALS);
	public static ItemIronHeart IRONHEART = (ItemIronHeart) (new ItemIronHeart()).setUnlocalizedName("ironHeart").setCreativeTab(CreativeTabs.MATERIALS);
	public static ItemEmeraldHeart EMERALDHEART = (ItemEmeraldHeart) (new ItemEmeraldHeart()).setUnlocalizedName("emeraldHeart").setCreativeTab(CreativeTabs.MATERIALS);
	public static BlockSap SAPBLOCK = (BlockSap) (new BlockSap()).setHardness(0.0F).setUnlocalizedName("sapBlock").setCreativeTab(CreativeTabs.DECORATIONS);
	public static BlockSpecBrewingStand BREWING_STAND = (BlockSpecBrewingStand) (new BlockSpecBrewingStand()).setHardness(0.5F).setLightLevel(0.125F).setUnlocalizedName("brewingStand");
	public static ItemBlockSpecial BREWING_STAND_ITEM = (ItemBlockSpecial) (new ItemBlockSpecial(BREWING_STAND)).setUnlocalizedName("brewingStand").setCreativeTab(CreativeTabs.BREWING);

	public static BlockSpecFurnace FURNACE = (BlockSpecFurnace) (new BlockSpecFurnace(false)).setHardness(3.5F).setUnlocalizedName("furnace").setCreativeTab(CreativeTabs.DECORATIONS);
	public static BlockSpecFurnace LIT_FURNACE = (BlockSpecFurnace) (new BlockSpecFurnace(true)).setHardness(3.5F).setLightLevel(0.875F).setUnlocalizedName("furnace");
	public static Block CRAFTING_TABLE = (new BlockSpecWorkbench()).setHardness(2.5F).setUnlocalizedName("workbench");

	public static ItemArmor HELMETOBSIDIAN = (ItemArmor) (new ItemCustomArmor(OBSIDIAN_ARMOR, 5, EntityEquipmentSlot.HEAD)).setUnlocalizedName("helmetObsidian");
	public static ItemArmor PLATEOBSIDIAN = (ItemArmor) (new ItemCustomArmor(OBSIDIAN_ARMOR, 5, EntityEquipmentSlot.CHEST)).setUnlocalizedName("plateObsidian");
	public static ItemArmor LEGSOBSIDIAN = (ItemArmor) (new ItemCustomArmor(OBSIDIAN_ARMOR, 5, EntityEquipmentSlot.LEGS)).setUnlocalizedName("legsObsidian");
	public static ItemArmor BOOTSOBSIDIAN = (ItemArmor) (new ItemCustomArmor(OBSIDIAN_ARMOR, 5, EntityEquipmentSlot.FEET)).setUnlocalizedName("bootsObsidian");

	public static ItemArmor LEATHER_HELMET = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.LEATHER, 5, EntityEquipmentSlot.HEAD)).setUnlocalizedName("helmetCloth");
	public static ItemArmor LEATHER_CHESTPLATE = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.LEATHER, 5, EntityEquipmentSlot.CHEST)).setUnlocalizedName("chestplateCloth");
	public static ItemArmor LEATHER_LEGGINGS = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.LEATHER, 5, EntityEquipmentSlot.LEGS)).setUnlocalizedName("leggingsCloth");
	public static ItemArmor LEATHER_BOOTS = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.LEATHER, 5, EntityEquipmentSlot.FEET)).setUnlocalizedName("bootsCloth");

	public static ItemArmor CHAINMAIL_HELMET = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.CHAIN, 5, EntityEquipmentSlot.HEAD)).setUnlocalizedName("helmetChain");
	public static ItemArmor CHAINMAIL_CHESTPLATE = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.CHAIN, 5, EntityEquipmentSlot.CHEST)).setUnlocalizedName("chestplateChain");
	public static ItemArmor CHAINMAIL_LEGGINGS = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.CHAIN, 5, EntityEquipmentSlot.LEGS)).setUnlocalizedName("leggingsChain");
	public static ItemArmor CHAINMAIL_BOOTS = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.CHAIN, 5, EntityEquipmentSlot.FEET)).setUnlocalizedName("bootsChain");

	public static ItemArmor IRON_HELMET = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.IRON, 5, EntityEquipmentSlot.HEAD)).setUnlocalizedName("helmetIron");
	public static ItemArmor IRON_CHESTPLATE = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.IRON, 5, EntityEquipmentSlot.CHEST)).setUnlocalizedName("chestplateIron");
	public static ItemArmor IRON_LEGGINGS = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.IRON, 5, EntityEquipmentSlot.LEGS)).setUnlocalizedName("leggingsIron");
	public static ItemArmor IRON_BOOTS = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.IRON, 5, EntityEquipmentSlot.FEET)).setUnlocalizedName("bootsIron");

	public static ItemArmor GOLDEN_HELMET = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.GOLD, 5, EntityEquipmentSlot.HEAD)).setUnlocalizedName("helmetGold");
	public static ItemArmor GOLDEN_CHESTPLATE = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.GOLD, 5, EntityEquipmentSlot.CHEST)).setUnlocalizedName("chestplateGold");
	public static ItemArmor GOLDEN_LEGGINGS = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.GOLD, 5, EntityEquipmentSlot.LEGS)).setUnlocalizedName("leggingsGold");
	public static ItemArmor GOLDEN_BOOTS = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.GOLD, 5, EntityEquipmentSlot.FEET)).setUnlocalizedName("bootsGold");

	public static ItemArmor DIAMOND_HELMET = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.DIAMOND, 5, EntityEquipmentSlot.HEAD)).setUnlocalizedName("helmetDiamond");
	public static ItemArmor DIAMOND_CHESTPLATE = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.DIAMOND, 5, EntityEquipmentSlot.CHEST)).setUnlocalizedName("chestplateDiamond");
	public static ItemArmor DIAMOND_LEGGINGS = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.DIAMOND, 5, EntityEquipmentSlot.LEGS)).setUnlocalizedName("leggingsDiamond");
	public static ItemArmor DIAMOND_BOOTS = (ItemArmor) (new ItemCustomArmor(ItemArmor.ArmorMaterial.DIAMOND, 5, EntityEquipmentSlot.FEET)).setUnlocalizedName("bootsDiamond");

	public static Item SHOVELOBSIDIAN = (new ItemCustomSpade(OBSIDIAN_TOOL)).setUnlocalizedName("shovelObsidian");
	public static Item PICKAXEOBSIDIAN = (new ItemCustomPickaxe(OBSIDIAN_TOOL)).setUnlocalizedName("pickaxeObsidian");
	public static Item AXEOBSIDIAN = (new ItemCustomAxe(OBSIDIAN_TOOL)).setUnlocalizedName("hatchetObsidian");
	public static Item SWORDOBSIDIAN = (new ItemCustomSword(OBSIDIAN_TOOL)).setUnlocalizedName("swordObsidian");
	public static Item HOEOBSIDIAN = (new ItemCustomHoe(OBSIDIAN_TOOL)).setUnlocalizedName("hoeObsidian");

	public static Item SHOVELHARDWOOD = (new ItemCustomSpade(HARDWOOD_TOOL)).setUnlocalizedName("shovelHardWood");
	public static Item PICKAXEHARDWOOD = (new ItemCustomPickaxe(HARDWOOD_TOOL)).setUnlocalizedName("pickaxeHardWood");
	public static Item AXEHARDWOOD = (new ItemCustomAxe(HARDWOOD_TOOL)).setUnlocalizedName("hatchetHardWood");
	public static Item SWORDHARDWOOD = (new ItemCustomSword(HARDWOOD_TOOL)).setUnlocalizedName("swordHardWood");
	public static Item HOEHARDWOOD = (new ItemCustomHoe(HARDWOOD_TOOL)).setUnlocalizedName("hoeHardWood");

	public static Item WOODEN_AXE = (new ItemCustomAxe(Item.ToolMaterial.WOOD)).setUnlocalizedName("hatchetWood");
	public static Item STONE_AXE = (new ItemCustomAxe(Item.ToolMaterial.STONE)).setUnlocalizedName("hatchetStone");
	public static Item IRON_AXE = (new ItemCustomAxe(Item.ToolMaterial.IRON)).setUnlocalizedName("hatchetIron");
	public static Item GOLDEN_AXE = (new ItemCustomAxe(Item.ToolMaterial.GOLD)).setUnlocalizedName("hatchetGold");
	public static Item DIAMOND_AXE = (new ItemCustomAxe(Item.ToolMaterial.DIAMOND)).setUnlocalizedName("hatchetDiamond");

	public static Item WOODEN_PICKAXE = (new ItemCustomPickaxe(Item.ToolMaterial.WOOD)).setUnlocalizedName("pickaxeWood");
	public static Item STONE_PICKAXE = (new ItemCustomPickaxe(Item.ToolMaterial.STONE)).setUnlocalizedName("pickaxeStone");
	public static Item IRON_PICKAXE = (new ItemCustomPickaxe(Item.ToolMaterial.IRON)).setUnlocalizedName("pickaxeIron");
	public static Item GOLDEN_PICKAXE = (new ItemCustomPickaxe(Item.ToolMaterial.GOLD)).setUnlocalizedName("pickaxeGold");
	public static Item DIAMOND_PICKAXE = (new ItemCustomPickaxe(Item.ToolMaterial.DIAMOND)).setUnlocalizedName("pickaxeDiamond");

	public static Item WOODEN_SWORD = (new ItemCustomSword(Item.ToolMaterial.WOOD)).setUnlocalizedName("swordWood");
	public static Item STONE_SWORD = (new ItemCustomSword(Item.ToolMaterial.STONE)).setUnlocalizedName("swordStone");
	public static Item IRON_SWORD = (new ItemCustomSword(Item.ToolMaterial.IRON)).setUnlocalizedName("swordIron");
	public static Item GOLDEN_SWORD = (new ItemCustomSword(Item.ToolMaterial.GOLD)).setUnlocalizedName("swordGold");
	public static Item DIAMOND_SWORD = (new ItemCustomSword(Item.ToolMaterial.DIAMOND)).setUnlocalizedName("swordDiamond");

	public static Item WOODEN_SHOVEL = (new ItemCustomSpade(Item.ToolMaterial.WOOD)).setUnlocalizedName("shovelWood");
	public static Item STONE_SHOVEL = (new ItemCustomSpade(Item.ToolMaterial.STONE)).setUnlocalizedName("shovelStone");
	public static Item IRON_SHOVEL = (new ItemCustomSpade(Item.ToolMaterial.IRON)).setUnlocalizedName("shovelIron");
	public static Item GOLDEN_SHOVEL = (new ItemCustomSpade(Item.ToolMaterial.GOLD)).setUnlocalizedName("shovelGold");
	public static Item DIAMOND_SHOVEL = (new ItemCustomSpade(Item.ToolMaterial.DIAMOND)).setUnlocalizedName("shovelDiamond");

	public static Item WOODEN_HOE = (new ItemCustomHoe(Item.ToolMaterial.WOOD)).setUnlocalizedName("hoeWood");
	public static Item STONE_HOE = (new ItemCustomHoe(Item.ToolMaterial.STONE)).setUnlocalizedName("hoeStone");
	public static Item IRON_HOE = (new ItemCustomHoe(Item.ToolMaterial.IRON)).setUnlocalizedName("hoeIron");
	public static Item GOLDEN_HOE = (new ItemCustomHoe(Item.ToolMaterial.GOLD)).setUnlocalizedName("hoeGold");
	public static Item DIAMOND_HOE = (new ItemCustomHoe(Item.ToolMaterial.DIAMOND)).setUnlocalizedName("hoeDiamond");

	public static ItemArrow ARROW = (ItemArrow) (new ItemArrow()).setUnlocalizedName("arrow").setCreativeTab(CreativeTabs.COMBAT);
	public static ItemBow BOW = (ItemBow) (new ItemBow()).setUnlocalizedName("bow").setCreativeTab(CreativeTabs.COMBAT);
	//public static ItemPotion COMPACTPOTION = (ItemPotion) (new ItemPotion()).setUnlocalizedName("compactPotion").setMaxStackSize(4);

	public static Block SAPLING = (new BlockSapling()).setHardness(0.0F).setUnlocalizedName("sapling");

	public static Enchantment haste = new EnchantmentHaste(Enchantment.Rarity.RARE, new EntityEquipmentSlot[] {EntityEquipmentSlot.FEET}).setName("Haste");
	public static Enchantment skill = new EnchantmentBowSkill(Enchantment.Rarity.RARE, new EntityEquipmentSlot[] {EntityEquipmentSlot.MAINHAND}).setName("Skill");
	public static EnumAction farming = EnumHelper.addAction("farming");

	public static final ChatEventContainerClass serverEventHandler = new ChatEventContainerClass();
	public static SimpleNetworkWrapper snw;

	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		NetworkRegistry.INSTANCE.registerGuiHandler(instance, proxy);
		FMLCommonHandler.instance().bus().register(new FMLHandler());
		FMLCommonHandler.instance().bus().register(new TickHandler());
		MinecraftForge.EVENT_BUS.register(new SCMEventHandler());

		GameRegistry.registerTileEntity(TileEntitySapBlock.class, "TileEntitySapBlock");
		GameRegistry.registerTileEntity(TileEntitySpecBrewingStand.class, "TileEntitySpecBrewingStand");
		GameRegistry.registerTileEntity(TileEntitySpecFurnace.class, "TileEntitySpecFurnace");
		GameRegistry.registerTileEntity(TileEntitySpecWorkbench.class, "TileEntitySpecWorkbench");

		LoMaS_Utils.registerBlock(SAPBLOCK.setRegistryName( "sapBlock"));

		LoMaS_Utils.registerBlock(BREWING_STAND.setRegistryName( "brewing_stand"), BREWING_STAND_ITEM.setRegistryName( "brewing_stand"));

		LoMaS_Utils.registerBlock(FURNACE.setRegistryName( "furnace"));
		LoMaS_Utils.registerBlock(LIT_FURNACE.setRegistryName( "lit_furnace"));
		LoMaS_Utils.registerBlock(CRAFTING_TABLE.setRegistryName( "crafting_table"));
		OreDictionary.registerOre("workbench", this.CRAFTING_TABLE);

		int[] sapling_metas = {0,1,2,3,4,5};
		LoMaS_Utils.registerBlock(SAPLING.setRegistryName( "sapling"), new ItemSapling(SAPLING).setRegistryName( "sapling"), sapling_metas, BlockSapling.TYPE);
		OreDictionary.registerOre("treeSapling", new ItemStack(this.SAPLING, 1, OreDictionary.WILDCARD_VALUE));
		
		LoMaS_Utils.registerItem(AXEHARDWOOD.setRegistryName( "axeHardWood"));
		LoMaS_Utils.registerItem(HOEHARDWOOD.setRegistryName( "hoeHardWood"));
		LoMaS_Utils.registerItem(SHOVELHARDWOOD.setRegistryName( "shovelHardWood"));
		LoMaS_Utils.registerItem(PICKAXEHARDWOOD.setRegistryName( "pickaxeHardWood"));
		LoMaS_Utils.registerItem(SWORDHARDWOOD.setRegistryName( "swordHardWood"));

		LoMaS_Utils.registerItem(AXEOBSIDIAN.setRegistryName( "axeObsidian"));
		LoMaS_Utils.registerItem(HOEOBSIDIAN.setRegistryName( "hoeObsidian"));
		LoMaS_Utils.registerItem(SHOVELOBSIDIAN.setRegistryName( "shovelObsidian"));
		LoMaS_Utils.registerItem(PICKAXEOBSIDIAN.setRegistryName( "pickaxeObsidian"));
		LoMaS_Utils.registerItem(SWORDOBSIDIAN.setRegistryName( "swordObsidian"));

		LoMaS_Utils.registerItem(WOODEN_AXE.setRegistryName( "wooden_axe"));
		LoMaS_Utils.registerItem(STONE_AXE.setRegistryName( "stone_axe"));
		LoMaS_Utils.registerItem(IRON_AXE.setRegistryName( "iron_axe"));
		LoMaS_Utils.registerItem(GOLDEN_AXE.setRegistryName( "golden_axe"));
		LoMaS_Utils.registerItem(DIAMOND_AXE.setRegistryName( "diamond_axe"));

		LoMaS_Utils.registerItem(WOODEN_PICKAXE.setRegistryName( "wooden_pickaxe"));
		LoMaS_Utils.registerItem(STONE_PICKAXE.setRegistryName( "stone_pickaxe"));
		LoMaS_Utils.registerItem(IRON_PICKAXE.setRegistryName( "iron_pickaxe"));
		LoMaS_Utils.registerItem(GOLDEN_PICKAXE.setRegistryName( "golden_pickaxe"));
		LoMaS_Utils.registerItem(DIAMOND_PICKAXE.setRegistryName( "diamond_pickaxe"));

		LoMaS_Utils.registerItem(WOODEN_SHOVEL.setRegistryName( "wooden_shovel"));
		LoMaS_Utils.registerItem(STONE_SHOVEL.setRegistryName( "stone_shovel"));
		LoMaS_Utils.registerItem(IRON_SHOVEL.setRegistryName( "iron_shovel"));
		LoMaS_Utils.registerItem(GOLDEN_SHOVEL.setRegistryName( "golden_shovel"));
		LoMaS_Utils.registerItem(DIAMOND_SHOVEL.setRegistryName( "diamond_shovel"));

		LoMaS_Utils.registerItem(WOODEN_SWORD.setRegistryName( "wooden_sword"));
		LoMaS_Utils.registerItem(STONE_SWORD.setRegistryName( "stone_sword"));
		LoMaS_Utils.registerItem(IRON_SWORD.setRegistryName( "iron_sword"));
		LoMaS_Utils.registerItem(GOLDEN_SWORD.setRegistryName( "golden_sword"));
		LoMaS_Utils.registerItem(DIAMOND_SWORD.setRegistryName( "diamond_sword"));

		LoMaS_Utils.registerItem(WOODEN_HOE.setRegistryName( "wooden_hoe"));
		LoMaS_Utils.registerItem(STONE_HOE.setRegistryName( "stone_hoe"));
		LoMaS_Utils.registerItem(IRON_HOE.setRegistryName( "iron_hoe"));
		LoMaS_Utils.registerItem(GOLDEN_HOE.setRegistryName( "golden_hoe"));
		LoMaS_Utils.registerItem(DIAMOND_HOE.setRegistryName( "diamond_hoe"));

		LoMaS_Utils.registerItem(BOOTSOBSIDIAN.setRegistryName( "bootsObsidian"));
		LoMaS_Utils.registerItem(HELMETOBSIDIAN.setRegistryName( "helmetObsidian"));
		LoMaS_Utils.registerItem(PLATEOBSIDIAN.setRegistryName( "plateObsidian"));
		LoMaS_Utils.registerItem(LEGSOBSIDIAN.setRegistryName( "legsObsidian"));

		LoMaS_Utils.registerItem(LEATHER_BOOTS.setRegistryName( "leather_boots"));
		LoMaS_Utils.registerItem(LEATHER_HELMET.setRegistryName( "leather_helmet"));
		LoMaS_Utils.registerItem(LEATHER_CHESTPLATE.setRegistryName( "leather_chestplate"));
		LoMaS_Utils.registerItem(LEATHER_LEGGINGS.setRegistryName( "leather_leggings"));

		LoMaS_Utils.registerItem(GOLDEN_BOOTS.setRegistryName( "golden_boots"));
		LoMaS_Utils.registerItem(GOLDEN_HELMET.setRegistryName( "golden_helmet"));
		LoMaS_Utils.registerItem(GOLDEN_CHESTPLATE.setRegistryName( "golden_chestplate"));
		LoMaS_Utils.registerItem(GOLDEN_LEGGINGS.setRegistryName( "golden_leggings"));

		LoMaS_Utils.registerItem(CHAINMAIL_BOOTS.setRegistryName( "chainmail_boots"));
		LoMaS_Utils.registerItem(CHAINMAIL_HELMET.setRegistryName( "chainmail_helmet"));
		LoMaS_Utils.registerItem(CHAINMAIL_CHESTPLATE.setRegistryName( "chainmail_chestplate"));
		LoMaS_Utils.registerItem(CHAINMAIL_LEGGINGS.setRegistryName( "chainmail_leggings"));

		LoMaS_Utils.registerItem(IRON_BOOTS.setRegistryName( "iron_boots"));
		LoMaS_Utils.registerItem(IRON_HELMET.setRegistryName( "iron_helmet"));
		LoMaS_Utils.registerItem(IRON_CHESTPLATE.setRegistryName( "iron_chestplate"));
		LoMaS_Utils.registerItem(IRON_LEGGINGS.setRegistryName( "iron_leggings"));

		LoMaS_Utils.registerItem(DIAMOND_BOOTS.setRegistryName( "diamond_boots"));
		LoMaS_Utils.registerItem(DIAMOND_HELMET.setRegistryName( "diamond_helmet"));
		LoMaS_Utils.registerItem(DIAMOND_CHESTPLATE.setRegistryName( "diamond_chestplate"));
		LoMaS_Utils.registerItem(DIAMOND_LEGGINGS.setRegistryName( "diamond_leggings"));
		LoMaS_Utils.registerItem(IRONHEART.setRegistryName( "ironHeart"));
		LoMaS_Utils.registerItem(EMERALDHEART.setRegistryName( "emeraldHeart"));
		LoMaS_Utils.registerItem(SAPBOTTLE.setRegistryName( "sapBottle"));
		
		//GameRegistry.register(COMPACTPOTION.setRegistryName( "compactPotion"));
		LoMaS_Utils.registerItem(ARROW.setRegistryName( "arrow"), ItemArrow.arrowMeta, null, ItemArrow.arrowTextures);
		OreDictionary.registerOre("arrow", new ItemStack(this.ARROW, 1, OreDictionary.WILDCARD_VALUE));
		LoMaS_Utils.registerItem(BOW.setRegistryName( "bow"));
		OreDictionary.registerOre("bow", this.BOW);
	
        BlockDispenser.DISPENSE_BEHAVIOR_REGISTRY.putObject(this.ARROW, new BehaviorProjectileDispense()
        {
            public ItemStack dispenseStack(IBlockSource source, ItemStack stack)
            {
                World world = source.getWorld();
                IPosition iposition = BlockDispenser.getDispensePosition(source);               
                IProjectile iprojectile = this.getProjectileEntity(world, iposition, stack);
                world.spawnEntityInWorld((Entity)iprojectile);
                stack.splitStack(1);
                return stack;
            }
            
            /**
             * Return the projectile entity spawned by this dispense behavior.
             */
            protected IProjectile getProjectileEntity(World worldIn, IPosition position, ItemStack stackIn)
            {
            	EntityCustomArrow entityarrow = new EntityCustomArrow(worldIn, position.getX(), position.getY(), position.getZ(), stackIn.getMetadata());
                entityarrow.setPotionEffect(stackIn);
                entityarrow.pickupStatus = EntityArrow.PickupStatus.ALLOWED;
                return entityarrow;
            }
        });

		Items.ARROW.setCreativeTab(null);
		Items.TIPPED_ARROW.setCreativeTab(null);
		Items.BOW.setCreativeTab(null);
		Blocks.BREWING_STAND.setCreativeTab(null);
		Blocks.FURNACE.setCreativeTab(null);
		Blocks.CRAFTING_TABLE.setCreativeTab(null);
		Items.BREWING_STAND.setCreativeTab(null);

		Items.WOODEN_AXE.setCreativeTab(null);
		Items.STONE_AXE.setCreativeTab(null);
		Items.IRON_AXE.setCreativeTab(null);
		Items.GOLDEN_AXE.setCreativeTab(null);
		Items.DIAMOND_AXE.setCreativeTab(null);

		Items.WOODEN_PICKAXE.setCreativeTab(null);
		Items.STONE_PICKAXE.setCreativeTab(null);
		Items.IRON_PICKAXE.setCreativeTab(null);
		Items.GOLDEN_PICKAXE.setCreativeTab(null);
		Items.DIAMOND_PICKAXE.setCreativeTab(null);

		Items.WOODEN_SHOVEL.setCreativeTab(null);
		Items.STONE_SHOVEL.setCreativeTab(null);
		Items.IRON_SHOVEL.setCreativeTab(null);
		Items.GOLDEN_SHOVEL.setCreativeTab(null);
		Items.DIAMOND_SHOVEL.setCreativeTab(null);

		Items.WOODEN_HOE.setCreativeTab(null);
		Items.STONE_HOE.setCreativeTab(null);
		Items.IRON_HOE.setCreativeTab(null);
		Items.GOLDEN_HOE.setCreativeTab(null);
		Items.DIAMOND_HOE.setCreativeTab(null);

		Items.WOODEN_SWORD.setCreativeTab(null);
		Items.STONE_SWORD.setCreativeTab(null);
		Items.IRON_SWORD.setCreativeTab(null);
		Items.GOLDEN_SWORD.setCreativeTab(null);
		Items.DIAMOND_SWORD.setCreativeTab(null);

		Items.LEATHER_HELMET.setCreativeTab(null);
		Items.LEATHER_CHESTPLATE.setCreativeTab(null);
		Items.LEATHER_LEGGINGS.setCreativeTab(null);
		Items.LEATHER_BOOTS.setCreativeTab(null);

		Items.CHAINMAIL_HELMET.setCreativeTab(null);
		Items.CHAINMAIL_CHESTPLATE.setCreativeTab(null);
		Items.CHAINMAIL_LEGGINGS.setCreativeTab(null);
		Items.CHAINMAIL_BOOTS.setCreativeTab(null);

		Items.GOLDEN_HELMET.setCreativeTab(null);
		Items.GOLDEN_CHESTPLATE.setCreativeTab(null);
		Items.GOLDEN_LEGGINGS.setCreativeTab(null);
		Items.GOLDEN_BOOTS.setCreativeTab(null);

		Items.IRON_HELMET.setCreativeTab(null);
		Items.IRON_CHESTPLATE.setCreativeTab(null);
		Items.IRON_LEGGINGS.setCreativeTab(null);
		Items.IRON_BOOTS.setCreativeTab(null);

		Items.DIAMOND_HELMET.setCreativeTab(null);
		Items.DIAMOND_CHESTPLATE.setCreativeTab(null);
		Items.DIAMOND_LEGGINGS.setCreativeTab(null);
		Items.DIAMOND_BOOTS.setCreativeTab(null);

		Blocks.SAPLING.setCreativeTab(null);

		List<IRecipe> recipes = CraftingManager.getInstance().getRecipeList();
		Iterator<IRecipe> iter = recipes.iterator();
		while (iter.hasNext())
		{
			ItemStack is = iter.next().getRecipeOutput();
			if (is != null &&
					(is.getItem() instanceof ItemTool
							|| is.getItem() instanceof ItemHoe
							|| is.getItem() instanceof ItemSword
							|| is.getItem() instanceof ItemArmor
							|| is.getItem() == Items.BOW))
			{
				iter.remove();
			}
		}

		FoodInitializer.init();

		EntityRegistry.registerModEntity(EntityCustomArrow.class, "entitycustomarrow", LoMaS_Utils.getNextEntityID(), this, 80, 3, true);
		EntityRegistry.registerModEntity(EntityCustomXPOrb.class, "entitycustomxp", LoMaS_Utils.getNextEntityID(), this, 80, 3, true);

		proxy.regiterBlockStateMappers();
		doRecipes();

	}

	@EventHandler
	public void preInit(FMLPreInitializationEvent evt)
	{
        CapabilityManager.INSTANCE.register(IPlayerCapability.class, new PlayerCapabilityStorage(), IPlayerCapability.class);
		
		snw = NetworkRegistry.INSTANCE.newSimpleChannel(modid);
		snw.registerMessage(SpecializationMessageHandler.class, SpecializationMessage.class, 7778, Side.CLIENT);
		snw.registerMessage(AbilityMessageHandler.class, AbilityMessage.class, 7779, Side.CLIENT);
		snw.registerMessage(AnimalMessageHandler.class, AnimalMessage.class, 7780, Side.CLIENT);
		snw.registerMessage(CraftMessageHandler.class, CraftMessage.class, 7781, Side.CLIENT);
		snw.registerMessage(ArrowMessageHandler.class, ArrowMessage.class, 7782, Side.CLIENT);
		snw.registerMessage(TimedUsageMessageHandler.class, TimedUsageMessage.class, 7785, Side.CLIENT);
		snw.registerMessage(SpecializationMessageHandler.class, SpecializationMessage.class, 7778, Side.SERVER);
		snw.registerMessage(AbilityMessageHandler.class, AbilityMessage.class, 7779, Side.SERVER);
		snw.registerMessage(AnimalMessageHandler.class, AnimalMessage.class, 7780, Side.SERVER);
		snw.registerMessage(CraftMessageHandler.class, CraftMessage.class, 7781, Side.SERVER);
		snw.registerMessage(ArrowMessageHandler.class, ArrowMessage.class, 7782, Side.SERVER);
		snw.registerMessage(TimedUsageMessageHandler.class, TimedUsageMessage.class, 7785, Side.SERVER);
		proxy.registerRenderInformation();
	}

	@EventHandler
	public void serverStart(FMLServerStartingEvent event)
	{
		server = event.getServer();
		MinecraftForge.EVENT_BUS.register(this.serverEventHandler);
		this.serverEventHandler.getPlayerNameData();

		ServerCommandManager scm = (ServerCommandManager) event.getServer().getCommandManager();
		scm.registerCommand(new CommandSpecializations());
		scm.registerCommand(new CommandCustomChat());

		server.setAllowFlight(true);
	}

	public static void clientStart()
	{
		MinecraftForge.EVENT_BUS.register(new SCMClientEventContainerClass());
		/*RenderManager rm = Minecraft.getMinecraft().getRenderManager();
		try
		{
			Field skinF = rm.getClass().getDeclaredFields()[1];
			Field playerF = rm.getClass().getDeclaredFields()[2];
			skinF.setAccessible(true);
			playerF.setAccessible(true);
			playerF.set(rm, new RenderSpecPlayer(rm));
			((HashMap) skinF.get(rm)).put("default", playerF.get(rm));
			((HashMap) skinF.get(rm)).put("slim", new RenderSpecPlayer(rm, true));
			SpecItemRenderer newIR = new SpecItemRenderer(Minecraft.getMinecraft());
			Field ir = Minecraft.getMinecraft().getClass().getDeclaredFields()[23];
			ir.setAccessible(true);
			ir.set(Minecraft.getMinecraft(), newIR);
			ir = Minecraft.getMinecraft().entityRenderer.getClass().getDeclaredFields()[9];
			ir.setAccessible(true);
			ir.set(Minecraft.getMinecraft().entityRenderer, newIR);
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
		catch (IllegalAccessException e)
		{
			e.printStackTrace();
		}*/
	}

	public void doRecipes()
	{
		GameRegistry.addRecipe(new ItemStack(Items.SADDLE, 1), new Object[] { "###", "X X", "$ $", 'X', Items.STRING, '#', Items.LEATHER, '$', Items.IRON_INGOT });

		GameRegistry.addRecipe(new ItemStack(IRONHEART, 1), new Object[] { "$X$", "XXX", "$X$", Character.valueOf('X'), Items.IRON_INGOT, Character.valueOf('$'), SAPBOTTLE });
		GameRegistry.addRecipe(new ItemStack(EMERALDHEART, 1), new Object[] { "$X$", "X#X", "$X$", Character.valueOf('X'), Items.IRON_INGOT, Character.valueOf('$'), SAPBOTTLE, Character.valueOf('#'),
				Items.EMERALD });

		GameRegistry.addRecipe(new ItemStack(BOW, 1), new Object[] { " #X", "# X", " #X", 'X', Items.STRING, '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE) });
		GameRegistry.addRecipe(new ItemStack(Items.SPECTRAL_ARROW, 2), new Object[] {" # ", "#X#", " # ", 'X', new ItemStack(ARROW, 1, 3), '#', Items.GLOWSTONE_DUST});
		GameRegistry.addRecipe(new ItemStack(ARROW, 4, 0), new Object[] { "X", "#", "Y", 'Y', Items.FEATHER, 'X', Blocks.PLANKS, '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE) });
		GameRegistry.addRecipe(new ItemStack(ARROW, 4, 1), new Object[] { "X", "#", "Y", 'Y', Items.FEATHER, 'X', Items.FLINT, '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE) });
		GameRegistry.addRecipe(new ItemStack(ARROW, 4, 2),
				new Object[] { "X", "#", "Y", 'Y', Items.FEATHER, 'X', Items.IRON_INGOT, '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE) });
		GameRegistry.addRecipe(new ItemStack(ARROW, 8, 3),
				new Object[] { "X", "#", "Y", 'Y', Items.FEATHER, 'X', Items.GOLD_INGOT, '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE) });
		GameRegistry.addRecipe(new ItemStack(ARROW, 8, 4), new Object[] { "X", "#", "Y", 'Y', Items.FEATHER, 'X', Items.DIAMOND, '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE) });
		GameRegistry.addRecipe(new ItemArrow.RecipeTippedArrow());
		
		GameRegistry.addRecipe(new ItemStack(this.BREWING_STAND, 1), new Object[] { " B ", "###", '#', Blocks.COBBLESTONE, 'B', Items.BLAZE_ROD });
		GameRegistry.addRecipe(new ItemStack(this.FURNACE), new Object[] { "###", "# #", "###", '#', Blocks.COBBLESTONE });
		GameRegistry.addRecipe(new ItemStack(this.CRAFTING_TABLE), new Object[] { "##", "##", '#', Blocks.PLANKS });
		String[][] recipePatterns = new String[][] { { "X", "X", "#" } };
		Object[][] recipeItems = new Object[][] { { Blocks.LOG, Blocks.LOG2, Blocks.OBSIDIAN }, { this.SWORDHARDWOOD, this.SWORDHARDWOOD, this.SWORDOBSIDIAN } };

		for (int i = 0; i < recipeItems[0].length; ++i)
		{
			Object object = recipeItems[0][i];

			for (int j = 0; j < recipeItems.length - 1; ++j)
			{
				Item item = (Item) recipeItems[j + 1][i];
				GameRegistry.addRecipe(new ItemStack(item), new Object[] { recipePatterns[j], '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'X', object });
			}
		}

		recipeItems = new Object[][] { { Blocks.PLANKS, Blocks.COBBLESTONE, Items.IRON_INGOT, Items.DIAMOND, Items.GOLD_INGOT }, { this.WOODEN_SWORD, this.STONE_SWORD, this.IRON_SWORD, this.DIAMOND_SWORD, this.GOLDEN_SWORD } };

		for (int i = 0; i < recipeItems[0].length; ++i)
		{
			Object object = recipeItems[0][i];

			for (int j = 0; j < recipeItems.length - 1; ++j)
			{
				Item item = (Item) recipeItems[j + 1][i];
				GameRegistry.addRecipe(new ItemStack(item), new Object[] { recipePatterns[j], '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'X', object });
			}
		}

		recipePatterns = new String[][] { { "XXX", " # ", " # " }, { "X", "#", "#" }, { "XX", "X#", " #" }, { "XX", " #", " #" } };

		recipeItems = new Object[][] { { Blocks.PLANKS, Blocks.COBBLESTONE, Items.IRON_INGOT, Items.DIAMOND, Items.GOLD_INGOT },
				{ this.WOODEN_PICKAXE, this.STONE_PICKAXE, this.IRON_PICKAXE, this.DIAMOND_PICKAXE, this.GOLDEN_PICKAXE },
				{ this.WOODEN_SHOVEL, this.STONE_SHOVEL, this.IRON_SHOVEL, this.DIAMOND_SHOVEL, this.GOLDEN_SHOVEL },
				{ this.WOODEN_AXE, this.STONE_AXE, this.IRON_AXE, this.DIAMOND_AXE, this.GOLDEN_AXE },
				{ this.WOODEN_HOE, this.STONE_HOE, this.IRON_HOE, this.DIAMOND_HOE, this.GOLDEN_HOE } };

		for (int i = 0; i < recipeItems[0].length; ++i)
		{
			Object object = recipeItems[0][i];

			for (int j = 0; j < recipeItems.length - 1; ++j)
			{
				Item item = (Item) recipeItems[j + 1][i];
				GameRegistry.addRecipe(new ItemStack(item), new Object[] { recipePatterns[j], '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'X', object });
			}
		}

		recipePatterns = new String[][] { { "XXX", " # ", " # " }, { "X", "#", "#" }, { "XX", "X#", " #" }, { "XX", " #", " #" } };

		recipeItems = new Object[][] { { Blocks.LOG, Blocks.LOG2, Blocks.OBSIDIAN }, { this.PICKAXEHARDWOOD, this.PICKAXEHARDWOOD, this.PICKAXEOBSIDIAN },
				{ this.SHOVELHARDWOOD, this.SHOVELHARDWOOD, this.SHOVELOBSIDIAN }, { this.AXEHARDWOOD, this.AXEHARDWOOD, this.AXEOBSIDIAN }, { this.HOEHARDWOOD, this.HOEHARDWOOD, this.HOEOBSIDIAN } };

		for (int i = 0; i < recipeItems[0].length; ++i)
		{
			Object object = recipeItems[0][i];

			for (int j = 0; j < recipeItems.length - 1; ++j)
			{
				Item item = (Item) recipeItems[j + 1][i];
				GameRegistry.addRecipe(new ItemStack(item), new Object[] { recipePatterns[j], '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'X', object });
			}
		}

		recipePatterns = new String[][] { { "XXX", "X X" }, { "X X", "XXX", "XXX" }, { "XXX", "X X", "X X" }, { "X X", "X X" } };
		recipeItems = new Object[][] { { Blocks.OBSIDIAN }, { this.HELMETOBSIDIAN }, { this.PLATEOBSIDIAN }, { this.LEGSOBSIDIAN }, { this.BOOTSOBSIDIAN } };

		for (int i = 0; i < recipeItems[0].length; ++i)
		{
			Object object = recipeItems[0][i];

			for (int j = 0; j < recipeItems.length - 1; ++j)
			{
				Item item = (Item) recipeItems[j + 1][i];
				GameRegistry.addRecipe(new ItemStack(item), new Object[] { recipePatterns[j], 'X', object });
			}
		}

		recipeItems = new Item[][] { { Items.LEATHER, Items.IRON_INGOT, Items.DIAMOND, Items.GOLD_INGOT },
				{ this.LEATHER_HELMET, this.IRON_HELMET, this.DIAMOND_HELMET, this.GOLDEN_HELMET }, { this.LEATHER_CHESTPLATE, this.IRON_CHESTPLATE, this.DIAMOND_CHESTPLATE, this.GOLDEN_CHESTPLATE }, { this.LEATHER_LEGGINGS, this.IRON_LEGGINGS, this.DIAMOND_LEGGINGS, this.GOLDEN_LEGGINGS }, { this.LEATHER_BOOTS, this.IRON_BOOTS, this.DIAMOND_BOOTS, this.GOLDEN_BOOTS } };
		for (int i = 0; i < recipeItems[0].length; ++i)
		{
			Object object = recipeItems[0][i];

			for (int j = 0; j < recipeItems.length - 1; ++j)
			{
				Item item = (Item) recipeItems[j + 1][i];
				GameRegistry.addRecipe(new ItemStack(item), new Object[] { recipePatterns[j], 'X', object });
			}
		}

		CraftingManager.getInstance().getRecipeList().remove(new RecipeRepairItem());
	}

	public static boolean isTouchingLog(EntityPlayer player)
	{
		boolean ret = false;
		BlockPos pos = new BlockPos(player.posX, player.posY, player.posZ);
		ret = player.worldObj.getBlockState(pos.north()).getBlock() instanceof BlockLog ? true
				: player.worldObj.getBlockState(pos.south()).getBlock() instanceof BlockLog ? true
						: player.worldObj.getBlockState(pos.east()).getBlock() instanceof BlockLog ? true
								: player.worldObj.getBlockState(pos.west()).getBlock() instanceof BlockLog ? true : false;
		return ret;
	}

	public static boolean isTouchingLog(World worldObj, BlockPos pos)
	{
		boolean ret = false;
		ret = worldObj.getBlockState(pos.north()).getBlock() instanceof BlockLog ? true
				: worldObj.getBlockState(pos.south()).getBlock() instanceof BlockLog ? true
						: worldObj.getBlockState(pos.east()).getBlock() instanceof BlockLog ? true
								: worldObj.getBlockState(pos.west()).getBlock() instanceof BlockLog ? true : false;
		return ret;
	}

	public static float getStealthAttackerDamage(Float tempDamg, EntityPlayer player)
	{
		if (LoMaS_Player.getLoMaSPlayer(player).classlevel >= 3)
		{
			tempDamg = tempDamg * 1.10f;
		}
		if (LoMaS_Player.getLoMaSPlayer(player).classlevel >= 5)
		{
			tempDamg = tempDamg * 1.10f;
		}
		if (LoMaS_Player.getLoMaSPlayer(player).classlevel >= 6)
		{
			tempDamg = tempDamg * 1.10f;
		}
		return tempDamg;
	}

	/**
	 * Gathers and applies armor reduction to damage being dealt to a entity.
	 * 
	 * @param entity
	 *            The Entity being damage
	 * @param inventory
	 *            An array of armor items
	 * @param source
	 *            The damage source type
	 * @param damage
	 *            The total damage being done
	 * @return The left over damage that has not been absorbed by the armor
	 */
	public static float ApplyArmor(EntityLivingBase entity, ItemStack[] inventory, DamageSource source, double damage)
	{
		damage *= 25;
		ArrayList<ArmorProperties> dmgVals = new ArrayList<ArmorProperties>();
		for (int x = 0; x < inventory.length; x++)
		{
			ItemStack stack = inventory[x];
			if (stack == null)
			{
				continue;
			}
			ArmorProperties prop = null;
			if (stack.getItem() instanceof ISpecialArmor)
			{
				ISpecialArmor armor = (ISpecialArmor) stack.getItem();
				prop = armor.getProperties(entity, stack, source, damage / 25D, x).copy();
			}
			else if (stack.getItem() instanceof ItemArmor && !source.isUnblockable())
			{
				ItemArmor armor = (ItemArmor) stack.getItem();
				prop = new ArmorProperties(0, armor.damageReduceAmount / 25D, armor.getMaxDamage() + 1 - stack.getItemDamage());
			}
			if (prop != null)
			{
				prop.Slot = x;
				dmgVals.add(prop);
			}
		}
		if (dmgVals.size() > 0)
		{
			ArmorProperties[] props = dmgVals.toArray(new ArmorProperties[dmgVals.size()]);
			StandardizeList(props, damage);
			int level = props[0].Priority;
			double ratio = 0;
			for (ArmorProperties prop : props)
			{
				if (level != prop.Priority)
				{
					// ////////////////////////////CHANGES
					if (source.getEntity() != null && source.getEntity() instanceof EntityPlayer)
					{
						EntityPlayer attacker = (EntityPlayer) source.getEntity();
						if (LoMaS_Player.getLoMaSPlayer(attacker).specClass.equalsIgnoreCase("warrior"))
						{
							if (LoMaS_Player.getLoMaSPlayer(attacker).classlevel >= 4)
							{
								ratio = ratio * 0.975;
							}
							if (LoMaS_Player.getLoMaSPlayer(attacker).classlevel >= 8)
							{
								ratio = ratio * 0.975;
							}
						}
					}
					EntityPlayer player = (EntityPlayer) entity;
					if (LoMaS_Player.getLoMaSPlayer(player).specClass.equalsIgnoreCase("warrior"))
					{
						if (LoMaS_Player.getLoMaSPlayer(player).classlevel >= 2)
						{
							ratio = ratio * 1.02;
						}
						if (LoMaS_Player.getLoMaSPlayer(player).classlevel >= 3)
						{
							ratio = ratio * 1.03;
						}
						if (LoMaS_Player.getLoMaSPlayer(player).classlevel >= 7)
						{
							ratio = ratio * 1.05;
						}
						if (LoMaS_Player.getLoMaSPlayer(player).classlevel >= 9)
						{
							ratio = ratio * 1.05;
						}
					}
					// ///////////////////////////END CHANGES
					damage -= (damage * ratio);
					ratio = 0;
					level = prop.Priority;
				}
				ratio += prop.AbsorbRatio;

				double absorb = damage * prop.AbsorbRatio;
				if (absorb > 0)
				{
					ItemStack stack = inventory[prop.Slot];
					int itemDamage = (int) (absorb / 25D < 1 ? 1 : absorb / 25D);
					if (stack.getItem() instanceof ISpecialArmor)
					{
						((ISpecialArmor) stack.getItem()).damageArmor(entity, stack, source, itemDamage, prop.Slot);
					}
					else
					{
						stack.damageItem(itemDamage, entity);
					}
					if (stack.stackSize <= 0)
					{
						/*
						 * if (entity instanceof EntityPlayer) { stack.onItemDestroyedByUse((EntityPlayer)entity); }
						 */
						inventory[prop.Slot] = null;
					}
				}
			}
			damage -= (damage * ratio);
		}
		return (float) (damage / 25.0F);
	}

	/**
	 * Sorts and standardizes the distribution of damage over armor.
	 * 
	 * @param armor
	 *            The armor information
	 * @param damage
	 *            The total damage being received
	 */
	private static void StandardizeList(ArmorProperties[] armor, double damage)
	{
		Arrays.sort(armor);

		int start = 0;
		double total = 0;
		int priority = armor[0].Priority;
		int pStart = 0;
		boolean pChange = false;
		boolean pFinished = false;

		for (int x = 0; x < armor.length; x++)
		{
			total += armor[x].AbsorbRatio;
			if (x == armor.length - 1 || armor[x].Priority != priority)
			{
				if (armor[x].Priority != priority)
				{
					total -= armor[x].AbsorbRatio;
					x--;
					pChange = true;
				}
				if (total > 1)
				{
					for (int y = start; y <= x; y++)
					{
						double newRatio = armor[y].AbsorbRatio / total;
						if (newRatio * damage > armor[y].AbsorbMax)
						{
							armor[y].AbsorbRatio = (double) armor[y].AbsorbMax / damage;
							total = 0;
							for (int z = pStart; z <= y; z++)
							{
								total += armor[z].AbsorbRatio;
							}
							start = y + 1;
							x = y;
							break;
						}
						else
						{
							armor[y].AbsorbRatio = newRatio;
							pFinished = true;
						}
					}
					if (pChange && pFinished)
					{
						damage -= (damage * total);
						total = 0;
						start = x + 1;
						priority = armor[start].Priority;
						pStart = start;
						pChange = false;
						pFinished = false;
						if (damage <= 0)
						{
							for (int y = x + 1; y < armor.length; y++)
							{
								armor[y].AbsorbRatio = 0;
							}
							break;
						}
					}
				}
				else
				{
					for (int y = start; y <= x; y++)
					{
						total -= armor[y].AbsorbRatio;
						if (damage * armor[y].AbsorbRatio > armor[y].AbsorbMax)
						{
							armor[y].AbsorbRatio = (double) armor[y].AbsorbMax / damage;
						}
						total += armor[y].AbsorbRatio;
					}
					damage -= (damage * total);
					total = 0;
					if (x != armor.length - 1)
					{
						start = x + 1;
						priority = armor[start].Priority;
						pStart = start;
						pChange = false;
						if (damage <= 0)
						{
							for (int y = x + 1; y < armor.length; y++)
							{
								armor[y].AbsorbRatio = 0;
							}
							break;
						}
					}
				}
			}
		}
	}

	public static float getWalkSpeed(EntityPlayer player, LoMaS_Player lplayer)
	{
		float speed = (0.08f);

		if (lplayer.specClass.equalsIgnoreCase("stealth"))
		{
			speed = (0.082f);
			if (lplayer.classlevel == 2)
			{
				speed = (0.085f);
			}
			else if (lplayer.classlevel == 3)
			{
				speed = (0.09f);
			}
			else if (lplayer.classlevel == 6)
			{
				speed = (0.095f);
			}
			else if (lplayer.classlevel >= 7)
			{
				speed = (0.1f);
			}
		}
		else if (lplayer.specClass.equalsIgnoreCase("warrior"))
		{
			speed = (0.076f);
			if (lplayer.classlevel >= 6)
			{
				speed = (0.078f);
			}
		}

		int t = EnchantmentHelper.getMaxEnchantmentLevel(SpecializationsMod.haste, player);
		if (t == 1)
		{
			// player.capabilities.setPlayerWalkSpeed(player.capabilities.getWalkSpeed() * 1.10f);
			speed *= 1.10f;
		}
		else if (t == 2)
		{
			// player.capabilities.setPlayerWalkSpeed(player.capabilities.getWalkSpeed() * 1.20f);
			speed *= 1.20f;

		}

		return speed;
	}

	public static float getProgress(EntityPlayer player, LoMaS_Player lplayer, Block block, BlockEvent.HarvestDropsEvent event)
	{
		float progressEarned = -1.0f;

		if (lplayer.specClass.equalsIgnoreCase("miner"))
		{
			if (block instanceof BlockOre)
			{
				if (block.equals(Blocks.COAL_ORE))
				{
					progressEarned = 0.2f;
				}
				else if (block.equals(Blocks.QUARTZ_ORE))
				{
					progressEarned = 0.2f;
				}
				else if (block.equals(Blocks.IRON_ORE))
				{
					progressEarned = 0.4f;
				}
				else if (block.equals(Blocks.LAPIS_ORE))
				{
					progressEarned = 1.2f;
				}
				else if (block.equals(Blocks.GOLD_ORE))
				{
					progressEarned = 1.25f;
				}
				else if (block.equals(Blocks.EMERALD_ORE))
				{
					progressEarned = 6.0f;
				}
				else if (block.equals(Blocks.DIAMOND_ORE))
				{
					progressEarned = 10.0f;
				}
				else
				{
					progressEarned = 0.1f;
				}
			}
			else if (block.equals(Blocks.OBSIDIAN))
			{
				progressEarned = 4.5f;
			}
			else if (block.equals(Blocks.GLOWSTONE))
			{
				progressEarned = 0.5f;
			}
			else if (block.equals(Blocks.NETHERRACK))
			{
				progressEarned = 0.0075f;
			}
			else if (block.equals(Blocks.REDSTONE_ORE))
			{
				progressEarned = 0.45f;
			}
			else if (block.equals(Blocks.SANDSTONE))
			{
				progressEarned = 0.1f;
			}
			else if (block.equals(Blocks.STONE))
			{
				progressEarned = 0.015f;
			}
			else if (block.equals(Blocks.SOUL_SAND))
			{
				progressEarned = 0.25f;
			}
		}
		else if (lplayer.specClass.equalsIgnoreCase("farmer"))
		{
			// 'Grown' crops need metadata checks
			//
			if (block.equals(FoodInitializer.CACTUS))
			{
				progressEarned = 0.25f;
			}
			else if (block.equals(FoodInitializer.CARROTS) && block.getMetaFromState(event.getState()) == 7)
			{
				progressEarned = 0.4f;
			}
			else if (block.equals(FoodInitializer.WHEAT_BLOCK) && block.getMetaFromState(event.getState()) == 7)
			{
				progressEarned = 0.4f;
			}
			else if (block.equals(FoodInitializer.MELON_BLOCK))
			{
				progressEarned = 0.25f;
			}
			else if (block.equals(FoodInitializer.POTATOES) && block.getMetaFromState(event.getState()) == 7)
			{
				progressEarned = 0.4f;
			}
			else if (block.equals(FoodInitializer.PUMPKIN))
			{
				progressEarned = 0.25f;
			}
			else if (block.equals(FoodInitializer.REEDS_BLOCK) && block.getMetaFromState(event.getState()) > 2)
			{
				progressEarned = 0.05f;
				for (int i = 0; i < 3; i++)
				{
					if (event.getWorld().getBlockState(event.getPos().up(i)).equals(block))
					{
						progressEarned = progressEarned + 0.05f;
					}
				}
			}
		}
		else if (lplayer.specClass.equalsIgnoreCase("woodsman"))
		{
			if (block instanceof BlockLog)
			{
				progressEarned = 0.225f;
			}
			else if (block instanceof BlockLeaves)
			{
				progressEarned = 0.0225f;
			}
		}
		return progressEarned;
	}

	public static float getAttackerDamage(LivingHurtEvent event, EntityLivingBase target, EntityPlayer attacker)
	{
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(attacker);
		ItemStack held = attacker.inventory.getCurrentItem();
		float tempDamg = event.getAmount();
		tempDamg = event.getAmount() * 0.90f;
		if (lplayer.specClass.equalsIgnoreCase("farmer"))
		{
			if (lplayer.classlevel >= 2)
			{
				tempDamg = tempDamg * 1.02f;
			}

			if (lplayer.classlevel >= 3)
			{
				tempDamg = tempDamg * 1.02f;
			}

			if (lplayer.classlevel >= 4)
			{
				tempDamg = tempDamg * 1.02f;
			}

			if (lplayer.classlevel >= 5)
			{
				tempDamg = tempDamg * 1.01f;
			}

			if (lplayer.classlevel >= 6)
			{
				tempDamg = tempDamg * 1.02f;
				if (target instanceof EntityWolf)
				{
					tempDamg = tempDamg * 1.20f;
				}
			}
		}

		if (lplayer.specClass.equalsIgnoreCase("stealth"))
		{
			float attackerRot = MathHelper.clampAngle((int) attacker.rotationYaw) + 180;
			float defenderRot = MathHelper.clampAngle((int) target.rotationYaw) + 180;
			// System.out.println("attacker " + attackerRot);
			// System.out.println("defender " + defenderRot);
			if (attackerRot - 60.0f < defenderRot && attackerRot + 60.0f > defenderRot)
			{
				tempDamg = SpecializationsMod.getStealthAttackerDamage(tempDamg, attacker);
			}
			else if (Math.abs(defenderRot - attackerRot) > 60)
			{
				if (defenderRot <= 60)
				{
					defenderRot = defenderRot + 360;
					if (Math.abs(defenderRot - attackerRot) < 60)
					{
						tempDamg = SpecializationsMod.getStealthAttackerDamage(tempDamg, attacker);
						float progressEarned = 2.0f;
						if (!attacker.capabilities.isCreativeMode)
						{
							lplayer.addSpecProgress(progressEarned);
							attacker.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_PICKUP, 0.1F, 0.5F * ((attacker.worldObj.rand.nextFloat() - attacker.worldObj.rand.nextFloat()) * 0.7F + 1.8F));
						}
					}
				}
				else if (attackerRot <= 60)
				{
					attackerRot = attackerRot + 360;
					if (Math.abs(attackerRot - defenderRot) < 60)
					{
						tempDamg = SpecializationsMod.getStealthAttackerDamage(tempDamg, attacker);
						if (!attacker.capabilities.isCreativeMode)
						{
							float progressEarned = 2.0f;
							lplayer.addSpecProgress(progressEarned);
							attacker.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_PICKUP, 0.1F, 0.5F * ((attacker.worldObj.rand.nextFloat() - attacker.worldObj.rand.nextFloat()) * 0.7F + 1.8F));
						}
					}
				}
			}
		}

		if (lplayer.specClass.equalsIgnoreCase("warrior"))
		{
			if (lplayer.classlevel >= 3 && target instanceof EntityPlayer)
			{
				tempDamg += 1;
			}
			if (lplayer.classlevel >= 5 && !(target instanceof EntityPlayer))
			{
				tempDamg += 4;
			}
			if (lplayer.classlevel >= 7 && (target instanceof EntityPlayer))
			{
				tempDamg += 1;
			}
		}

		if (lplayer.specClass.equalsIgnoreCase("miner"))
		{
			if (lplayer.classlevel >= 4 && !(target instanceof EntityPlayer))
			{
				tempDamg += 1;
			}
			if (lplayer.classlevel >= 6)
			{
				tempDamg = tempDamg * 1.02f;
			}
			if (lplayer.classlevel >= 7 && !(target instanceof EntityPlayer))
			{
				tempDamg += 1;
			}
			if (lplayer.classlevel >= 9)
			{
				tempDamg = tempDamg * 1.03f;
			}
		}

		if (lplayer.specClass.equalsIgnoreCase("woodsman"))
		{
			if (lplayer.classlevel >= 2)
			{
				if (target instanceof EntityAnimal)
				{
					tempDamg += 1;
				}
				if (held != null && (held.getItem() instanceof ItemAxe))
				{
					tempDamg = tempDamg * 1.04f;
				}
			}
			if (lplayer.classlevel >= 4)
			{
				tempDamg = tempDamg * 1.03f;
			}
			if (lplayer.classlevel >= 5)
			{
				if (held != null && (held.getItem() instanceof ItemAxe))
				{
					tempDamg = tempDamg * 1.10f;
				}
			}
			if (lplayer.classlevel >= 6)
			{
				tempDamg = tempDamg * 1.02f;
			}
			if (lplayer.classlevel >= 7)
			{
				if (held != null && (held.getItem() instanceof ItemAxe))
				{
					tempDamg = tempDamg * 1.06f;
				}
			}

			if (lplayer.classlevel >= 8)
			{
				if (target instanceof EntityAnimal)
				{
					tempDamg += 1;
				}
			}

			if (lplayer.classlevel >= 9)
			{
				tempDamg = tempDamg * 1.05f;
			}
		}
		return tempDamg;
	}

	public static float applyPotionDamageCalculations(DamageSource p_70672_1_, float p_70672_2_, EntityLivingBase elb)
	{
		if (p_70672_1_.isDamageAbsolute())
		{
			return p_70672_2_;
		}
		else
		{
			int i;
			int j;
			float f1;

			if (elb.isPotionActive(MobEffects.RESISTANCE) && p_70672_1_ != DamageSource.outOfWorld)
			{
				i = (elb.getActivePotionEffect(MobEffects.RESISTANCE).getAmplifier() + 1) * 5;
				j = 25 - i;
				f1 = p_70672_2_ * (float) j;
				p_70672_2_ = f1 / 25.0F;
			}

			if (p_70672_2_ <= 0.0F)
			{
				return 0.0F;
			}
			else
			{
				i = EnchantmentHelper.getEnchantmentModifierDamage(elb.getArmorInventoryList(), p_70672_1_);

				if (i > 20)
				{
					i = 20;
				}

				if (i > 0 && i <= 20)
				{
					j = 25 - i;
					f1 = p_70672_2_ * (float) j;
					p_70672_2_ = f1 / 25.0F;
				}

				return p_70672_2_;
			}
		}
	}

	public static EntityPlayer getSpecClosestPlayer(World world, double x, double y, double z, double distance)
	{
		double d4 = -1.0D;
		EntityPlayer entityplayer = null;

		for (int i = 0; i < world.playerEntities.size(); ++i)
		{
			EntityPlayer entityplayer1 = (EntityPlayer) world.playerEntities.get(i);
			double d5 = entityplayer1.getDistanceSq(x, y, z);

			if ((distance < 0.0D || d5 < distance * distance) && (d4 == -1.0D || d5 < d4))
			{
				d4 = d5;
				entityplayer = entityplayer1;
			}
		}

		if (entityplayer != null && entityplayer.experienceLevel < 5)
		{
			return entityplayer;
		}
		else if (entityplayer != null)
		{
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(entityplayer);
			if (lplayer.specClass.equalsIgnoreCase("spellbinder"))
			{
				if (entityplayer.experienceLevel < 10)
				{
					return entityplayer;
				}
				else if (entityplayer.experienceLevel < 20 && lplayer.classlevel >= 4)
				{
					return entityplayer;
				}
				else if (entityplayer.experienceLevel < 30 && lplayer.classlevel >= 8)
				{
					return entityplayer;
				}
			}
		}
		return null;
	}

	public static boolean canSustainPlant(IBlockAccess world, BlockPos pos, EnumFacing direction, IPlantable plantable)
	{
		IBlockState state = world.getBlockState(pos);
		IBlockState plant = plantable.getPlant(world, pos.offset(direction));
		EnumPlantType plantType = plantable.getPlantType(world, pos.offset(direction));

		if (direction == EnumFacing.UP && plant.getBlock() == FoodInitializer.CACTUS && state.getBlock() == FoodInitializer.CACTUS)
		{
			return true;
		}

		if (direction == EnumFacing.UP && plant.getBlock() == FoodInitializer.REEDS_BLOCK && state.getBlock() == FoodInitializer.REEDS_BLOCK)
		{
			return true;
		}

		/*
		 * if (plantable instanceof BlockBush && (state.getBlock() == Blocks.grass || state.getBlock() == Blocks.dirt || state.getBlock() == FoodInitializer.farmland)) { return true; }
		 */

		switch (plantType)
		{
			case Desert:
				return state.getBlock() == Blocks.SAND || state.getBlock() == Blocks.HARDENED_CLAY || state.getBlock() == Blocks.STAINED_HARDENED_CLAY || state.getBlock() == Blocks.DIRT;
			case Nether:
				return state.getBlock() == Blocks.SOUL_SAND;
			case Crop:
				return state.getBlock() == FoodInitializer.FARMLAND;
			case Cave:
				return state.getBlock().isSideSolid(plant, world, pos, EnumFacing.UP);
			case Plains:
				return state.getBlock() == Blocks.GRASS || state.getBlock() == Blocks.DIRT || state.getBlock() == FoodInitializer.FARMLAND;
			case Water:
				return state.getMaterial() == Material.WATER && ((Integer) state.getValue(BlockLiquid.LEVEL)) == 0;
			case Beach:
				boolean isBeach = state.getBlock() == Blocks.GRASS || state.getBlock() == Blocks.DIRT || state.getBlock() == Blocks.SAND;
				boolean hasWater = false;
				int range = 1;
				if (world.getTileEntity(pos.offset(direction)) != null && world.getTileEntity(pos.offset(direction)) instanceof TileEntityPlant)
				{
					TileEntityPlant tilePlant = (TileEntityPlant) world.getTileEntity(pos.offset(direction));
					if (tilePlant.getFarmerLevel() >= 3)
					{
						range++;
					}
					if (tilePlant.getFarmerLevel() >= 5)
					{
						range++;
					}
					if (tilePlant.getFarmerLevel() >= 10)
					{
						hasWater = true;
					}

					Iterator iterator = BlockPos.getAllInBoxMutable(pos.add(-range, 0, -range), pos.add(range, 0, range)).iterator();
					BlockPos.MutableBlockPos mutableblockpos;

					do
					{
						mutableblockpos = (BlockPos.MutableBlockPos) iterator.next();
						if (world.getBlockState(mutableblockpos).getMaterial() == Material.WATER)
						{
							hasWater = true;
							break;
						}
					} while (iterator.hasNext());
				}
				else
				{
					hasWater = (world.getBlockState(pos.east()).getMaterial() == Material.WATER || world.getBlockState(pos.west()).getMaterial() == Material.WATER
							|| world.getBlockState(pos.north()).getMaterial() == Material.WATER || world.getBlockState(pos.south()).getMaterial() == Material.WATER);
				}
				return isBeach && hasWater;
		}

		return false;
	}

	public static int[] getAnimalParticleInfo(EntityAnimal animal, int status)
	{
		int[] ret = new int[2];
		if (animal instanceof EntityChicken)
		{
			ret[0] = 1;
		}
		else if (animal instanceof EntityWolf)
		{
			ret[0] = 2;
		}
		else if (animal instanceof EntityOcelot)
		{
			ret[0] = 3;
		}
		else if (animal instanceof EntityPig)
		{
			ret[0] = 4;
		}
		ret[1] = status;
		return ret;
	}

	public static int getQualityIndexFromDamage(int damage)
	{
		int ret = 0;
		if (damage < 75)
		{
			ret = 0;
		}
		else if (damage < 100)
		{
			ret = 1;
		}
		else
		{
			ret = 2;
		}
		return ret;
	}

	public static boolean isBreedingItem(EntityAnimal animal, ItemStack stack)
	{
		boolean ret = false;
		if (animal instanceof EntityChicken)
		{
			if (stack.getItem() == FoodInitializer.WHEAT_SEEDS)
			{
				ret = true;
			}
		}
		else if (animal instanceof EntityHorse)
		{
			ret = animal.isBreedingItem(stack);
		}
		else if (animal instanceof EntityOcelot)
		{
			ret = animal.isBreedingItem(stack);
		}
		else if (animal instanceof EntityPig)
		{
			if (stack.getItem() == FoodInitializer.CARROT)
			{
				ret = true;
			}
		}
		else if (animal instanceof EntityRabbit)
		{
			ret = animal.isBreedingItem(stack);
			if (stack.getItem() == FoodInitializer.CARROT || stack.getItem() == FoodInitializer.GOLDEN_CARROT)
			{
				ret = true;
			}
		}
		else if (animal instanceof EntityWolf)
		{
			ret = animal.isBreedingItem(stack);
		}
		else if (animal instanceof EntityAnimal)
		{
			if (stack.getItem() == FoodInitializer.WHEAT)
			{
				ret = true;
			}
		}
		return ret;
	}

	public static void setDamagedItem(ItemStack itemstack, EntityPlayer entityplayer, boolean displayMessage)
	{
		double newDur = itemstack.getItem().getMaxDamage();
		if (!LoMaS_Player.getLoMaSPlayer(entityplayer).specClass.equalsIgnoreCase("blacksmith"))
		{
			if (!(LoMaS_Player.getLoMaSPlayer(entityplayer).specClass.equalsIgnoreCase("woodsman") && (itemstack.getItem() == AXEHARDWOOD || itemstack.getItem() == PICKAXEHARDWOOD
					|| itemstack.getItem() == SHOVELHARDWOOD || itemstack.getItem() == HOEHARDWOOD || itemstack.getItem() == SWORDHARDWOOD)))
			{
				double durability = 0;
				Random rand = new Random();
				durability = itemstack.getMaxDamage();
				double randInt = ((double) rand.nextInt(20) + 30) / 100.0D;
				newDur = randInt * durability;
				itemstack.setItemDamage((int) (newDur));
				if (displayMessage)
				{
					entityplayer.addChatMessage(new TextComponentTranslation("\u00a7cYour unskilled hands have damaged the item."));
				}

				if (entityplayer.inventory.getItemStack() != null)
				{
					EntityPlayerMP ply = (EntityPlayerMP) entityplayer;
					snw.sendTo(new CraftMessage(itemstack.getItemDamage(), itemstack.getMaxDamage()), ply);
				}
			}
		}
		else
		{
			double durability = itemstack.getItem().getMaxDamage();
			// System.out.println("Durability " + durability);
			newDur = 0;
			if (LoMaS_Player.getLoMaSPlayer(entityplayer).classlevel >= 1)
			{
				newDur = 1.10 * durability;

				if (LoMaS_Player.getLoMaSPlayer(entityplayer).classlevel >= 3)
				{
					newDur = 1.05 * newDur;

					if (LoMaS_Player.getLoMaSPlayer(entityplayer).classlevel >= 4)
					{
						newDur = 1.15 * newDur;

						if (LoMaS_Player.getLoMaSPlayer(entityplayer).classlevel >= 5)
						{
							newDur = 1.10 * newDur;

							if (LoMaS_Player.getLoMaSPlayer(entityplayer).classlevel >= 8)
							{
								newDur = 1.10 * newDur;

								if (LoMaS_Player.getLoMaSPlayer(entityplayer).classlevel >= 9)
								{
									newDur = 1.05 * newDur;
								}
							}
						}
					}
				}
			}
			NBTTagCompound tagcomp = itemstack.getTagCompound();
			if (tagcomp == null)
			{
				tagcomp = new NBTTagCompound();
				itemstack.setTagCompound(tagcomp);
			}

			tagcomp.setInteger("maxDur", (int) newDur);
			if (entityplayer.inventory.getItemStack() != null)
			{
				EntityPlayerMP ply = (EntityPlayerMP) entityplayer;
				snw.sendTo(new CraftMessage(itemstack.getItemDamage(), itemstack.getTagCompound().getInteger("maxDur")), ply);
			}
		}

		entityplayer.inventoryContainer.detectAndSendChanges();
	}

	public static int getMaxDamage(ItemStack stack)
	{
		int max = stack.getItem().getMaxDamage();
		NBTTagCompound tagcomp = stack.getTagCompound();
		if (tagcomp == null)
		{
			tagcomp = new NBTTagCompound();
			stack.setTagCompound(tagcomp);
		}

		if (tagcomp.hasKey("maxDur"))
		{
			max = stack.getTagCompound().getInteger("maxDur");
		}
		return max;
	}

	@SideOnly(Side.CLIENT)
	public static void addInformation(ItemStack stack, EntityPlayer playerIn, List tooltip, boolean advanced)
	{
		NBTTagCompound nbtTagCompound = stack.getTagCompound();
		if (nbtTagCompound != null && nbtTagCompound.hasKey("maxDur"))
		{
			tooltip.add("Quality = "+nbtTagCompound.getInteger("maxDur"));
		}
		else
		{
			tooltip.add("Quality = "+stack.getItem().getMaxDamage());
		}
	}
}
