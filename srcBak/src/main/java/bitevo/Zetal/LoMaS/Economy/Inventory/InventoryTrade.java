package bitevo.Zetal.LoMaS.Economy.Inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;

public class InventoryTrade implements IInventory
{
	public ItemStack[] player1Trade = new ItemStack[9];
	public ItemStack[] player2Trade = new ItemStack[9];

	@Override
	public String getName()
	{
		return "Trade";
	}

	@Override
	public boolean hasCustomName()
	{
		return false;
	}

	@Override
	public ITextComponent getDisplayName()
	{
		return new TextComponentTranslation("Trade");
	}

	@Override
	public int getSizeInventory()
	{
		return 18;
	}

	@Override
	public ItemStack getStackInSlot(int index)
	{
		ItemStack[] aitemstack = this.player1Trade;

		if (index >= aitemstack.length)
		{
			index -= aitemstack.length;
			aitemstack = this.player2Trade;
		}

		return aitemstack[index];
	}

	@Override
	public ItemStack decrStackSize(int index, int count)
	{
		ItemStack[] aitemstack = this.player1Trade;

		if (index >= aitemstack.length)
		{
			index -= aitemstack.length;
			aitemstack = this.player2Trade;
		}

		if (aitemstack[index] != null)
		{
			ItemStack itemstack;

			if (aitemstack[index].stackSize <= count)
			{
				itemstack = aitemstack[index];
				aitemstack[index] = null;
				return itemstack;
			}
			else
			{
				itemstack = aitemstack[index].splitStack(count);

				if (aitemstack[index].stackSize == 0)
				{
					aitemstack[index] = null;
				}

				return itemstack;
			}
		}
		else
		{
			return null;
		}
	}

	public ItemStack removeStackFromSlot(int index)
	{
		ItemStack[] aitemstack = this.player1Trade;

		if (index >= aitemstack.length)
		{
			index -= aitemstack.length;
			aitemstack = this.player2Trade;
		}

		if (aitemstack[index] != null)
		{
			ItemStack itemstack = aitemstack[index];
			aitemstack[index] = null;
			return itemstack;
		}
		else
		{
			return null;
		}
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		ItemStack[] aitemstack = this.player1Trade;
		if (index >= aitemstack.length)
		{
			index -= aitemstack.length;
			aitemstack = this.player2Trade;
		}

		aitemstack[index] = stack;
	}

	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}

	@Override
	public void markDirty()
	{
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player)
	{
		return true;
	}

	@Override
	public void openInventory(EntityPlayer player)
	{
	}

	@Override
	public void closeInventory(EntityPlayer player)
	{
	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		return true;
	}

	@Override
	public int getField(int id)
	{
		return 0;
	}

	@Override
	public void setField(int id, int value)
	{
	}

	@Override
	public int getFieldCount()
	{
		return 0;
	}

	@Override
	public void clear()
	{
		for (int i = 0; i < this.player1Trade.length; i++)
		{
			this.player1Trade[i] = null;
		}
		for (int i = 0; i < this.player2Trade.length; i++)
		{
			this.player2Trade[i] = null;
		}
	}
}
