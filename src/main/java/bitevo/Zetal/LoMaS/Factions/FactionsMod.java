package bitevo.Zetal.LoMaS.Factions;

import bitevo.Zetal.LoMaS.Factions.Block.BlockObelisk;
import bitevo.Zetal.LoMaS.Factions.Block.BlockRoad;
import bitevo.Zetal.LoMaS.Factions.Block.BlockRoadSlab;
import bitevo.Zetal.LoMaS.Factions.Block.BlockRoadStairs;
import bitevo.Zetal.LoMaS.Factions.Command.CommandFaction;
import bitevo.Zetal.LoMaS.Factions.Entity.EntityPigmanWarrior;
import bitevo.Zetal.LoMaS.Factions.Entity.EntitySiegeGolem;
import bitevo.Zetal.LoMaS.Factions.Handler.FactionEventHandler;
import bitevo.Zetal.LoMaS.Factions.Handler.FactionsFMLEventHandler;
import bitevo.Zetal.LoMaS.Factions.Message.FactionMessage;
import bitevo.Zetal.LoMaS.Factions.Message.FactionMessageHandler;
import bitevo.Zetal.LoMaS.Factions.Message.WarriorMessage;
import bitevo.Zetal.LoMaS.Factions.Message.WarriorMessageHandler;
import bitevo.Zetal.LoMaS.Factions.TileEntity.TileEntityObelisk;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.command.ServerCommandManager;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSlab;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;

@Mod(modid = FactionsMod.modid, name = "Factions", version = "0.1")
public class FactionsMod
{
	public static final String modid = "lomas_factions";
	@SidedProxy(clientSide = "bitevo.Zetal.LoMaS.Factions.FactionsClientProxy", serverSide = "bitevo.Zetal.LoMaS.Factions.FactionsCommonProxy")
	public static FactionsCommonProxy proxy = new FactionsCommonProxy();
	@Instance(modid)
	public static FactionsMod instance;
	public static int dailySoldierFee = 5;

	public static final Block ROAD = new BlockRoad().setUnlocalizedName("road").setHardness(3.0F);
	public static final BlockObelisk OBELISK = (BlockObelisk) new BlockObelisk(Material.ROCK).setUnlocalizedName("obelisk").setHardness(-1.0F).setBlockUnbreakable().setResistance(6000000.0F);
	public static final BlockRoadStairs ROAD_STAIRS = (BlockRoadStairs) (new BlockRoadStairs(ROAD.getDefaultState())).setHardness(3.0F).setUnlocalizedName("road_stairs");
	public static final BlockRoadSlab ROAD_SLAB = (BlockRoadSlab) (new BlockRoadSlab.Half()).setHardness(3.0F).setUnlocalizedName("road_slab");
	public static final BlockRoadSlab ROAD_DOUBLE_SLAB = (BlockRoadSlab) (new BlockRoadSlab.Double()).setHardness(3.0F).setUnlocalizedName("road_double_slab");

	public static final Item OBELISK_CORE = new Item().setUnlocalizedName("obelisk_core").setCreativeTab(LoMaS_Utils.tabLoMaS);
	public static SimpleNetworkWrapper snw;

	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		NetworkRegistry.INSTANCE.registerGuiHandler(instance, proxy);
		MinecraftForge.EVENT_BUS.register(new FactionEventHandler());
		FMLCommonHandler.instance().bus().register(new FactionsFMLEventHandler());
		LoMaS_Utils.registerBlock(OBELISK.setRegistryName("obelisk"));
		LoMaS_Utils.registerBlock(ROAD.setRegistryName("road"));
		LoMaS_Utils.registerBlock(ROAD_STAIRS.setRegistryName("road_stairs"));
		GameRegistry.register(ROAD_DOUBLE_SLAB.setRegistryName("road_double_slab"));
		LoMaS_Utils.registerBlock(ROAD_SLAB.setRegistryName("road_slab"), new ItemSlab(ROAD_SLAB, ROAD_SLAB, ROAD_DOUBLE_SLAB).setUnlocalizedName("road_slab").setRegistryName("road_slab"));

		LoMaS_Utils.registerItem(OBELISK_CORE);

		GameRegistry.registerTileEntity(TileEntityObelisk.class, "TileEntityObelisk");

		this.registerModEntityWithEgg(EntityPigmanWarrior.class, LoMaS_Utils.getNextEntityID(), "Warrior", 0xCC9999, 0x33FFFF);
		this.registerModEntityWithEgg(EntitySiegeGolem.class, LoMaS_Utils.getNextEntityID(), "SiegeGolem", 0x33FFFF, 0xFF0000);

		doRecipes();
	}

	@EventHandler
	public void preInit(FMLPreInitializationEvent evt)
	{
		LoMaS_Utils.FAC = true;
		this.instance = this;
		snw = NetworkRegistry.INSTANCE.newSimpleChannel(modid);
		snw.registerMessage(FactionMessageHandler.class, FactionMessage.class, LoMaS_Utils.getNextPacketID(), Side.CLIENT);
		snw.registerMessage(FactionMessageHandler.class, FactionMessage.class, LoMaS_Utils.getCurrentPacketID(), Side.SERVER);
		snw.registerMessage(WarriorMessageHandler.class, WarriorMessage.class, LoMaS_Utils.getNextPacketID(), Side.CLIENT);
		snw.registerMessage(WarriorMessageHandler.class, WarriorMessage.class, LoMaS_Utils.getCurrentPacketID(), Side.SERVER);
		proxy.registerRenderInformation();
	}

	@EventHandler
	public void serverStart(FMLServerStartingEvent event)
	{
		ServerCommandManager scm = (ServerCommandManager) event.getServer().getCommandManager();
		scm.registerCommand(new CommandFaction());
	}

	public void doRecipes()
	{
		GameRegistry.addRecipe(new ItemStack(ROAD, 2), new Object[] { "$#", "#$", '$', Blocks.GRAVEL, '#', Blocks.COBBLESTONE });
		GameRegistry.addRecipe(new ItemStack(ROAD, 2), new Object[] { "$#", "#$", '$', Blocks.COBBLESTONE, '#', Blocks.GRAVEL });
		GameRegistry.addRecipe(new ItemStack(ROAD_STAIRS, 4), new Object[] { "  #", " ##", "###", '#', ROAD });
		GameRegistry.addRecipe(new ItemStack(ROAD_SLAB, 6), new Object[] { "###", '#', ROAD });
	}

	public static void clientStart()
	{
		// ClientCommandHandler scm = (ClientCommandHandler) Minecraft.getMinecraft().;
		// scm.registerCommand(new CommandFaction());
	}

	public void registerModEntityWithEgg(Class parEntityClass, int eID, String parEntityName, int parEggColor, int parEggSpotsColor)
	{
		EntityRegistry.registerModEntity(parEntityClass, parEntityName, eID, FactionsMod.instance, 80, 3, false);
		EntityRegistry.registerEgg(parEntityClass, parEggColor, parEggSpotsColor);
	}
}
