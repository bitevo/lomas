package bitevo.Zetal.LoMaS.Specializations.Renderer;

import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityFactionIronGolem;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.layers.LayerHeldItem;
import net.minecraft.util.ResourceLocation;

public class RenderIronGolem extends RenderLiving<EntityFactionIronGolem>
{
	public RenderIronGolem(RenderManager p_i46153_1_)
	{
		super(p_i46153_1_, new ModelIronGolem(), 0.5F);
		this.addLayer(new LayerHeldItem(this));
	}

    protected void rotateCorpse(EntityFactionIronGolem entityLiving, float p_77043_2_, float p_77043_3_, float partialTicks)
    {
        super.rotateCorpse(entityLiving, p_77043_2_, p_77043_3_, partialTicks);

        if ((double)entityLiving.limbSwingAmount >= 0.01D)
        {
            float f = 13.0F;
            float f1 = entityLiving.limbSwing - entityLiving.limbSwingAmount * (1.0F - partialTicks) + 6.0F;
            float f2 = (Math.abs(f1 % 13.0F - 6.5F) - 3.25F) / 3.25F;
            GlStateManager.rotate(6.5F * f2, 0.0F, 0.0F, 1.0F);
        }
    }

	@Override
	protected ResourceLocation getEntityTexture(EntityFactionIronGolem entity)
	{
		return new ResourceLocation(SpecializationsMod.MODID, "textures/entities/iron_golem.png");
	}
}
