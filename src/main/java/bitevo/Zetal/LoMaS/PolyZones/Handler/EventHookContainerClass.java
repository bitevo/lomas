package bitevo.Zetal.LoMaS.PolyZones.Handler;

import java.util.ArrayList;
import java.util.List;

import bitevo.Zetal.LoMaS.PolyZones.Access;
import bitevo.Zetal.LoMaS.PolyZones.BlockLoc;
import bitevo.Zetal.LoMaS.PolyZones.BlockPolygon;
import bitevo.Zetal.LoMaS.PolyZones.DataHandler;
import bitevo.Zetal.LoMaS.PolyZones.InvalidPolygonException;
import bitevo.Zetal.LoMaS.PolyZones.PolyZone;
import bitevo.Zetal.LoMaS.PolyZones.PolyZonePlayer;
import bitevo.Zetal.LoMaS.PolyZones.PolyZonesMod;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Animal;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockChest;
import net.minecraft.block.BlockDoor;
import net.minecraft.block.BlockFenceGate;
import net.minecraft.block.BlockFurnace;
import net.minecraft.block.BlockLever;
import net.minecraft.block.BlockTrapDoor;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.entity.passive.EntityTameable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.event.CommandEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.player.FillBucketEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.ExplosionEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

/**
 * Name and cast of this class are irrelevant
 */
public class EventHookContainerClass
{
	public BlockPolygon bPoly = null;
	public PolyZone temp = null;
	public PolyZone editingZone = null;
	public String isEditing = "";
	public String name = "";
	//public ArrayList<EntityPlayer> tempBlockers = new ArrayList<EntityPlayer>();
	//public ArrayList<Integer> tempCounters = new ArrayList<Integer>();
	public int x;
	public int y;
	public int z;

	@SubscribeEvent
	public void onJailedMouse(PlayerInteractEvent event)
	{
		Boolean thing = PolyZonesMod.playersMove.get(event.getEntityPlayer().getPersistentID());
		if (FMLCommonHandler.instance().getSide() == Side.SERVER && thing != null)
		{
			if (PolyZonesMod.playersMove.get(event.getEntityPlayer().getPersistentID()).booleanValue() == false)
			{
				event.setCanceled(true);
			}
		}
		else if (FMLCommonHandler.instance().getSide() == Side.SERVER && thing != null)
		{
			if (((event.getEntityPlayer().getHeldItemMainhand() != null && (event.getEntityPlayer().getHeldItemMainhand().getItemUseAction() != EnumAction.EAT && event.getEntityPlayer().getHeldItemMainhand().getItemUseAction() != EnumAction.DRINK))))
			{
				event.setCanceled(true);
			}
			if (((event.getEntityPlayer().getHeldItemOffhand() != null && (event.getEntityPlayer().getHeldItemOffhand().getItemUseAction() != EnumAction.EAT && event.getEntityPlayer().getHeldItemOffhand().getItemUseAction() != EnumAction.DRINK))))
			{
				event.setCanceled(true);
			}
		}
	}

	@SubscribeEvent
	public void onJailedMove(LivingUpdateEvent event)
	{
		if (FMLCommonHandler.instance().getSide() == Side.SERVER && event.getEntity() instanceof EntityPlayerMP)
		{
			EntityPlayer player = (EntityPlayer) event.getEntity();
			Boolean thing = PolyZonesMod.playersMove.get(player.getPersistentID());
			if (thing != null)
			{
				if (PolyZonesMod.playersMove.get(player.getPersistentID()).booleanValue() == false)
				{
					// addChatMessage(player, "Data files are out of sync.");
					player.setPositionAndUpdate(player.prevPosX, player.prevPosY, player.prevPosZ);
				}
			}
			else
			{
				// player.addChatMessage("Cheaters don't get wishes (Contact an Admin)");
				player.setPositionAndUpdate(player.prevPosX, player.prevPosY, player.prevPosZ);
			}
		}
	}

	@SubscribeEvent
	public void onEntityMove(LivingUpdateEvent event)
	{
		// System.out.println(event.getEntity());
		if (event.getEntity() instanceof EntityPlayer || event.getEntity() instanceof EntityPlayerMP)
		{
			EntityPlayer player = (EntityPlayer) event.getEntity();
			if ((((int) (player.posY - player.getEyeHeight()) == (int) player.posY) && FMLCommonHandler.instance().getSide() == Side.CLIENT) || ((int) (player.posY + player.getEyeHeight()) == (int) (player.posY + 1.62)))
			{
				if (PolyZonesMod.processor.getPlayer(((EntityPlayer) event.getEntity()).getPersistentID()) == null)
				{
					PolyZonesMod.processor.addPlayer((EntityPlayer) event.getEntity());
					System.out.println("Added a new CustomPlayer with the name " + ((EntityPlayer) event.getEntity()).getDisplayNameString());
					if (FMLCommonHandler.instance().getSide() != Side.CLIENT)
					{
						// player.addChatMessage("Added new CustomPlayer " + PolyZones.processor.getPlayer(player.username).getName());
					}
				}
				if (player.worldObj.provider.getDimension() == -1 || player.worldObj.provider.getDimension() == 1)
				{
					event.setCanceled(false);
					return;
				}
				PolyZonePlayer t = PolyZonesMod.processor.getPlayer(((EntityPlayer) event.getEntity()).getPersistentID());
				PolyZone temp = PolyZonesMod.processor.contained(player);
				if (temp != null)
				{
					if (!t.getIsInsideZone() && !temp.getPermissions().getWelcome().equals(""))
					{
						if (FMLCommonHandler.instance().getSide() == Side.SERVER)
						{
							LoMaS_Utils.addChatMessage(player, temp.getPermissions().getWelcome());
						}
					}

					t.insideNow(temp.getName());
					int p = t.getAccess(temp.getName()).getPermissions();

					if ((p & 2) == 2)
					{
						if (FMLCommonHandler.instance().getSide() == Side.SERVER)
						{
							LoMaS_Utils.addChatMessage(player, "A magical barrier prevents your entry. You do not have permission. You have been turned around.");
						}
						player.setPositionAndRotation(player.prevPosX, player.prevPosY, player.prevPosZ, (float) (player.prevCameraYaw + 2 * Math.PI), player.cameraPitch);
					}
					if ((p & 32) != 32)
					{
						if (player.getHealth() < 20 && !player.isDead)
						{
							player.heal(1);
						}
					}
				}
				else
				{
					if (t.getIsInsideZone() && !PolyZonesMod.processor.getZone(t.getCurrentZone()).getPermissions().getGoodbye().equals(""))
					{
						if (FMLCommonHandler.instance().getSide() == Side.SERVER)
						{
							LoMaS_Utils.addChatMessage(player, PolyZonesMod.processor.getZone(t.getCurrentZone()).getPermissions().getGoodbye());
						}
					}
					t.outsideNow();
				}
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				if (player.worldObj.provider.getDimension() != -1 && player.worldObj.provider.getDimension() != 1)
				{
					double x = player.posX;
					double z = player.posZ;
					/*
					 * if(x > -100 && x < 100 && z < 100 && z > -100 && !t.getIsInTown()) { addChatMessage(player, "You are now in the Central Town."); t.insideLandNow("Town"); t.setIsInTown(true); }
					 * if((x < -100 || x > 100 || z > 100 || z < -100) && t.getIsInTown()) { t.setIsInTown(false); } if (x >= 0.1 && !t.getIsPositiveX()) { t.setIsPositiveX(true); } else if (x <= -0.1
					 * && t.getIsPositiveX()) { t.setIsPositiveX(false); } if (z >= 0.1 && !t.getIsPositiveZ()) { t.setIsPositiveZ(true); } else if (z <= -0.1 && t.getIsPositiveZ()) {
					 * t.setIsPositiveZ(false); } if(t.getIsPositiveX() && t.getIsPositiveZ() && !t.getIsInTown() && (t.getCurrentLand() != "Mountains")) { addChatMessage(player,
					 * "You are now in the Land of Mountains."); t.insideLandNow("Mountains"); } else if(t.getIsPositiveX() && !t.getIsPositiveZ() && !t.getIsInTown() && (t.getCurrentLand() !=
					 * "Stone")) { addChatMessage(player, "You are now in the Land of Stone."); t.insideLandNow("Stone"); } else if(!t.getIsPositiveX() && t.getIsPositiveZ() && !t.getIsInTown() &&
					 * (t.getCurrentLand() != "Light")) { addChatMessage(player, "You are now in the Land of Light."); t.insideLandNow("Light"); } else if(!t.getIsPositiveX() && !t.getIsPositiveZ() &&
					 * !t.getIsInTown() && (t.getCurrentLand() != "Water")) { addChatMessage(player, "You are now in the Land of Water."); t.insideLandNow("Water"); }
					 */
					///////////////////////////////////////////////////////////////////////
					/*if ((int) x == PolyZonesMod.mapBoundaries && !this.tempBlockers.contains(player))// || (x < -PolyZones.mapBoundaries) || (z < -PolyZones.mapBoundaries) || (z >
																										// PolyZones.mapBoundaries)))
					{
						addChatMessage(player, "You cannot go this way. The magical fabric of the world is unstable.");// You have been turned around.");
						this.tempBlockers.add(player);
						this.tempCounters.add(50);
					}
					else if ((int) x == -PolyZonesMod.mapBoundaries && !this.tempBlockers.contains(player))// || (x > PolyZones.mapBoundaries) || (z < -PolyZones.mapBoundaries) || (z >
																											// PolyZones.mapBoundaries) )
					{
						addChatMessage(player, "You cannot go this way. The magical fabric of the world is unstable.");// You have been turned around.");
						this.tempBlockers.add(player);
						this.tempCounters.add(50);
					}
					else if ((int) z == -PolyZonesMod.mapBoundaries && !this.tempBlockers.contains(player))// || (x > PolyZones.mapBoundaries) || (x < -PolyZones.mapBoundaries) || (z >
																											// PolyZones.mapBoundaries))
					{
						addChatMessage(player, "You cannot go this way. The magical fabric of the world is unstable.");// You have been turned around.");
						this.tempBlockers.add(player);
						this.tempCounters.add(50);
					}
					else if ((int) z == PolyZonesMod.mapBoundaries && !this.tempBlockers.contains(player))// || (x > PolyZones.mapBoundaries) || (x < -PolyZones.mapBoundaries) || (z <
																											// -PolyZones.mapBoundaries))
					{
						addChatMessage(player, "You cannot go this way. The magical fabric of the world is unstable.");// You have been turned around.");
						this.tempBlockers.add(player);
						this.tempCounters.add(50);
					}
					else if (((int) z > PolyZonesMod.mapBoundaries || (int) z < -PolyZonesMod.mapBoundaries || (int) x > PolyZonesMod.mapBoundaries || (int) x < -PolyZonesMod.mapBoundaries) && !this.tempBlockers.contains(player))
					{
						addChatMessage(player, "You are outside of the boundaries of the normal world. Magical forces attempt to draw you home.");// You have been turned around.");
						this.tempBlockers.add(player);
						this.tempCounters.add(120);
					}
					if (this.tempBlockers.contains(player))
					{
						int index = this.tempBlockers.indexOf(player);
						this.tempCounters.set(index, this.tempCounters.get(index) - 1);
						if (this.tempCounters.get(index) + 1 <= 0)
						{
							this.tempBlockers.remove(index);
							this.tempCounters.remove(index);
						}
					}*/
				}
			}
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		}
		else if (EventHookContainerClass.shouldEntityDie(event))
		{
			if (event.getEntity().worldObj.provider.getDimension() == -1 || event.getEntity().worldObj.provider.getDimension() == 1)
			{
				return;
			}
			BlockLoc entLoc = new BlockLoc((int) event.getEntity().posX, (int) event.getEntity().posY, (int) event.getEntity().posZ);
			PolyZone temp = PolyZonesMod.processor.contained(entLoc, event.getEntity().worldObj);
			if (temp != null)
			{
				if (!temp.getPermissions().getMobs())
				{
					event.getEntity().setPosition(0, -3000, 0);
					if (!event.getEntity().isDead)
					{
						event.getEntity().setDead();
					}
				}
			}
		}
	}

	public static boolean shouldEntityDie(LivingUpdateEvent event)
	{
		if (event.getEntity() != null && (event.getEntity() instanceof EntityLiving))
		{
			if (LoMaS_Utils.CIT && event.getEntity() instanceof bitevo.Zetal.LoMaS.Citizens.Entity.EntityPigmanCitizen)
			{
				return false;
			}
			if (event.getEntity() instanceof EntityTameable || (event.getEntity() instanceof EntityAgeable && ((EntityLiving) event.getEntity()).getLeashed()))
			{
				if ((event.getEntity() instanceof EntityPig && (((EntityPig) event.getEntity()).getSaddled())) // If a saddled pig
						|| (event.getEntity() instanceof EntityHorse && (((EntityHorse) event.getEntity()).isHorseSaddled())) // if a saddled horse
						|| (event.getEntity() instanceof EntityTameable && ((EntityTameable) event.getEntity()).getOwner() != null) // if a tamed animal
						|| (event.getEntity() instanceof EntityLiving && ((EntityLiving) event.getEntity()).getLeashed()) // if a leashed animal
						|| (event.getEntity() instanceof EntityLiving && ((EntityLiving) event.getEntity()).hasCustomName())) // If has a nametag
				{
					return false;
				}
				else
				{
					LoMaS_Animal lanimal = LoMaS_Animal.getLoMaSAnimal(event.getEntity().getPersistentID());
					if (lanimal != null && lanimal.getBeenTouched())
					{
						return false;
					}
					return true;
				}
			}
			else
			{
				return true;
			}
		}
		else if (event.getEntity() != null && (event.getEntity() instanceof EntityItem))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	@SubscribeEvent
	public void entityAttacked(LivingAttackEvent event)
	{
		if (event.getEntity().worldObj.provider.getDimension() == -1 || event.getEntity().worldObj.provider.getDimension() == 1)
		{
			return;
		}
		BlockLoc lol = new BlockLoc((int) event.getEntity().posX, (int) event.getEntity().posY, (int) event.getEntity().posZ);
		PolyZone t = PolyZonesMod.processor.contained(lol, event.getEntity().worldObj);
		if (t != null && event.getSource().getSourceOfDamage() instanceof EntityPlayer)
		{
			EntityPlayer entityplayer = (EntityPlayer) event.getSource().getSourceOfDamage();
			if (t.getPermissions().getPvp() && event.getAmount() < 900 && (event.getEntity() instanceof EntityPlayer || (event.getSource().getSourceOfDamage() instanceof EntityPlayer && (!LoMaS_Utils.isPlayerAdmin(entityplayer)))))
			{
				event.setCanceled(true);
			}
		}
	}

	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void breakSpeedEvent(PlayerEvent.BreakSpeed event)
	{
		EntityPlayer player = event.getEntityPlayer();
		ItemStack itemstack = player.getHeldItemMainhand();// .getItem();
		Block block = event.getState().getBlock();

		if (event.getEntity().worldObj.provider.getDimension() == -1 || event.getEntity().worldObj.provider.getDimension() == 1)
		{
			return;
		}

		PolyZonePlayer temp = PolyZonesMod.processor.getPlayer(event.getEntityPlayer().getPersistentID());
		BlockLoc lol = new BlockLoc(event.getPos().getX(), event.getPos().getY(), event.getPos().getZ());
		PolyZone t = PolyZonesMod.processor.contained(lol, event.getEntityPlayer().worldObj);
		if (t != null)
		{
			int p = temp.getAccess(t.getName()).getPermissions();
			if ((p & 16) == 16)
			{
				event.setNewSpeed(0.0f);
				event.setCanceled(true);
			}
		}
	}

	/*
	 * @SubscribeEvent public void playerLeftClick(PlayerInteractEvent event) { if (event.getEntity().worldObj.provider.getDimensionId() == -1 || event.getEntity().worldObj.provider.getDimensionId()
	 * == 1) { return; } if(event.action != null && event.action.equals(event.action.LEFT_CLICK_BLOCK)) { CustomPlayer temp =
	 * PolyZonesMod.processor.getPlayer(event.getEntityPlayer().getPersistentID()); BlockLoc lol = new BlockLoc(event.getPos().getX(), event.getPos().getY(), event.getPos().getZ()); PolyZone t =
	 * PolyZonesMod.processor.contained(lol, event.getEntityPlayer().worldObj); if (t != null) { int p = temp.getAccess(t.getName()).getPermissions(); if ((p & 16) == 16) {
	 * System.out.println("playerLeftClick"); event.setCanceled(true); } } } }
	 */

	@SubscribeEvent
	public void playerRightClickOrInteract(PlayerInteractEvent.RightClickBlock event)
	{
		// System.out.println("rightClickOrInteract");
		if (event.getEntity().dimension != 0)
		{
			return;
		}
		if (isEditing == event.getEntityPlayer().getDisplayNameString() && (x != event.getPos().getX() || y != event.getPos().getY() || z != event.getPos().getZ()))
		{
			x = event.getPos().getX();
			y = event.getPos().getY();
			z = event.getPos().getZ();
			if (event.getEntityPlayer().getDisplayNameString() != null && isEditing == event.getEntityPlayer().getDisplayNameString())
			{
				if (FMLCommonHandler.instance().getSide() != Side.CLIENT)
				{
					LoMaS_Utils.addChatMessage(event.getEntityPlayer(), "This point has been set with X: " + event.getPos().getX() + " Y: " + event.getPos().getY() + " Z: " + event.getPos().getZ());
				}
				if (bPoly != null)
				{
					bPoly.addPoint(new BlockLoc(event.getPos().getX(), event.getPos().getZ()));
				}
				else
				{
					bPoly = new BlockPolygon();
					bPoly.addPoint(new BlockLoc(event.getPos().getX(), event.getPos().getZ()));
				}
			}
		}
		else if ((event.getEntityPlayer().getDisplayNameString() != null && PolyZonesMod.grabbing == true))
		{
			if ((PolyZonesMod.playerEditing == event.getEntityPlayer().getDisplayNameString()))
			{
				// they're modifying the HEIGHT
				if (PolyZonesMod.height == true)
				{
					if (FMLCommonHandler.instance().getSide() != Side.CLIENT)
					{
						LoMaS_Utils.addChatMessage(event.getEntityPlayer(), "This point has been set as the HEIGHT with Y: " + event.getPos().getY());
					}
					PolyZonesMod.processor.getZone(editingZone.getName()).getPolygon().setHighYAll(event.getPos().getY());
				}
				// they're modifying the FLOOR
				else if (PolyZonesMod.height == false)
				{
					if (FMLCommonHandler.instance().getSide() != Side.CLIENT)
					{
						LoMaS_Utils.addChatMessage(event.getEntityPlayer(), "This point has been set as the FLOOR with Y: " + event.getPos().getY());
					}
					PolyZonesMod.processor.getZone(editingZone.getName()).getPolygon().setLowYAll(event.getPos().getY());
				}
			}
		}
		else
		{
			Block b = event.getEntityPlayer().worldObj.getBlockState(event.getPos()).getBlock();
			if (b instanceof BlockLever || b.equals(Blocks.STONE_BUTTON) || b instanceof BlockFurnace || (LoMaS_Utils.SCM && b instanceof bitevo.Zetal.LoMaS.Specializations.Block.BlockSpecFurnace) || b instanceof BlockChest || b instanceof BlockDoor || b instanceof BlockTrapDoor || b instanceof BlockFenceGate)
			{
				PolyZonePlayer temp = PolyZonesMod.processor.getPlayer(event.getEntityPlayer().getPersistentID());
				BlockLoc lol = new BlockLoc(event.getPos().getX(), event.getPos().getY(), event.getPos().getZ());
				PolyZone t = PolyZonesMod.processor.contained(lol, event.getEntityPlayer().worldObj);
				if (t != null)
				{
					int p = temp.getAccess(t.getName()).getPermissions();
					if ((p & 4) == 4)
					{
						// System.out.println("Failed - rightClickOrInteract");
						event.setCanceled(true);
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void playerPlaceBlock(BlockEvent.PlaceEvent event)
	{
		EntityPlayer player = event.getPlayer();
		if (player.worldObj.provider.getDimension() == -1 || player.worldObj.provider.getDimension() == 1)
		{
			return;
		}
		if (event.getPlacedBlock() != null)
		{
			PolyZonePlayer temp = PolyZonesMod.processor.getPlayer(player.getPersistentID());
			BlockLoc lol = new BlockLoc(event.getPos().getX(), event.getPos().getY(), event.getPos().getZ());
			PolyZone t = PolyZonesMod.processor.contained(lol, player.worldObj);
			if (t != null)
			{
				int p = temp.getAccess(t.getName()).getPermissions();
				if ((p & 8) == 8)
				{
					// System.out.println("playerPlaceBlock");
					event.setCanceled(true);
				}
			}
		}
	}

	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void onExplosion(ExplosionEvent.Detonate event)
	{
		List<BlockPos> bps = event.getAffectedBlocks();
		List<Entity> es = event.getAffectedEntities();
		List<BlockPos> removeBP = new ArrayList(bps.size());
		List<Entity> removeE = new ArrayList(es.size());
		World world = event.getWorld();
		for (BlockPos bp : bps)
		{
			PolyZone zone = PolyZonesMod.processor.contained(new BlockLoc(bp.getX(), bp.getY(), bp.getZ()), world);
			if (zone != null)
			{
				EntityLivingBase detonator = event.getExplosion().getExplosivePlacedBy();
				if (detonator instanceof EntityPlayer)
				{
					EntityPlayer dPlayer = (EntityPlayer) detonator;
					PolyZonePlayer cus = PolyZonesMod.processor.getPlayer(dPlayer.getPersistentID());
					int p = cus.getAccess(zone.getName()).getPermissions();
					if ((p & 16) != 16)
					{
						removeBP.add(bp);
					}
				}
				else
				{
					removeBP.add(bp);
				}
			}
		}
		for (Entity e : es)
		{
			PolyZone zone = PolyZonesMod.processor.contained(new BlockLoc(e.getPosition().getX(), e.getPosition().getY(), e.getPosition().getZ()), world);
			if (zone != null)
			{
				EntityLivingBase detonator = event.getExplosion().getExplosivePlacedBy();
				if (detonator instanceof EntityPlayer && e instanceof EntityPlayer)
				{
					EntityPlayer dPlayer = (EntityPlayer) detonator;
					PolyZonePlayer cus = PolyZonesMod.processor.getPlayer(dPlayer.getPersistentID());
					if (!zone.getPermissions().getPvp())
					{
						removeE.add(e);
					}
				}
				else
				{
					removeE.add(e);
				}
			}
		}
		event.getAffectedBlocks().removeAll(removeBP);
		event.getAffectedEntities().removeAll(removeE);
	}

	@SubscribeEvent
	public void playerRightClickWithBucket(FillBucketEvent event)
	{
		// System.out.println("rightClickWithBucket");
		if (event.getEntity().worldObj.provider.getDimension() == -1 || event.getEntity().worldObj.provider.getDimension() == 1)
		{
			return;
		}
		PolyZonePlayer temp = PolyZonesMod.processor.getPlayer(event.getEntityPlayer().getPersistentID());
		if (event.getTarget() != null && event.getTarget().getBlockPos() != null)
		{
			BlockLoc lol = new BlockLoc(event.getTarget().getBlockPos().getX(), event.getTarget().getBlockPos().getY(), event.getTarget().getBlockPos().getZ());
			PolyZone t = PolyZonesMod.processor.contained(lol, event.getEntityPlayer().worldObj);
			if (t != null)
			{
				int p = temp.getAccess(t.getName()).getPermissions();
				if (event.getEmptyBucket().getItem() != Items.BUCKET && ((p & 8) == 8))
				{
					System.out.println("Failed - rightClickWithBucket");
					event.setCanceled(true);
				}
				else if (event.getEmptyBucket().getItem() == Items.BUCKET && ((p & 16) == 16))
				{
					System.out.println("Failed - rightClickWithBucket");
					event.setCanceled(true);
				}
			}
		}
	}

	/*
	 * @SubscribeEvent public void playerRightClickAir(PlayerInteractEvent event) { if (event.getEntity().worldObj.getWorldInfo().getDimension() == -1 ||
	 * event.getEntity().worldObj.getWorldInfo().getDimension() == 1) { return; } if(event.action != null && event.action.equals(event.action.RIGHT_CLICK_AIR)) { System.out.println("Block: "+
	 * event.useBlock); System.out.println("Item: "+ event.useItem); } if(event.action != null && event.action.equals(event.action.RIGHT_CLICK_AIR) && (event.getEntityPlayer().getHeldItem() != null &&
	 * (event.getEntityPlayer().getHeldItem().getItemUseAction() != EnumAction.eat && event.getEntityPlayer().getHeldItem().getItemUseAction() != EnumAction.drink &&
	 * event.getEntityPlayer().getHeldItem().getItemUseAction() != EnumAction.block && event.getEntityPlayer().getHeldItem().getItemUseAction() != EnumAction.bow &&
	 * PolyZones.rightClickAirHelper(event)))) { System.out.println("Failed - rightClickAir: " + event.getEntityPlayer().getHeldItem().itemID); event.setCanceled(true); } }
	 */

	@SubscribeEvent
	public void commandCreatePolyzoneEvent(CommandEvent event)
	{
		if (event.getCommand() instanceof CommandCreatePolyzone)
		{
			ICommandSender sender = event.getSender();
			String[] parameters = event.getParameters();
			if (LoMaS_Utils.isSenderAdmin(sender))
			{
				if (parameters.length > 0)
				{
					if (parameters[0].matches("done"))
					{
						if (FMLCommonHandler.instance().getSide() == Side.SERVER)
						{
							if (parameters.length > 1 && parameters[1] != null && !parameters[1].isEmpty())
							{
								PolyZone tempZone = PolyZonesMod.processor.getZone(parameters[1]);
								if (tempZone != null)
								{
									try
									{
										tempZone.getPolygon().recreate();
										DataHandler.writeObject(PolyZonesMod.processor);
										LoMaS_Utils.addChatMessage(sender, "Successfully recreated the polyzone " + tempZone.getName());
										this.isEditing = "";

										for (BlockLoc[] x : tempZone.getPolygon().getContainedPoints())
										{
											for (BlockLoc y : x)
											{
												if (event.getSender().getEntityWorld() == null)
												{
													System.out.println("World is null!!!");
													return;
												}

												if (y != null)
												{
													event.getSender().getEntityWorld().setBlockState(new BlockPos(y.getX(), event.getSender().getCommandSenderEntity().posY - 1.0, y.getZ()), Blocks.WOOL.getDefaultState(), 3);
												}
											}
										}

										this.bPoly = null;
										this.editingZone = null;
										this.x = 0;
										this.y = 0;
										this.z = 0;
										this.name = "";
										this.temp = null;
									}
									catch (InvalidPolygonException e)
									{
										LoMaS_Utils.addChatMessage(sender, "Failed recreating the polyzone " + editingZone.getName());
										e.printStackTrace();
									}
								}
							}
							else
							{
								try
								{
									bPoly.create();
									editingZone.setPolygon(bPoly);
									DataHandler.writeObject(PolyZonesMod.processor);
									LoMaS_Utils.addChatMessage(sender, "Successfully finalized the new polyzone " + editingZone.getName());
									this.isEditing = "";

									/*
									 * for (BlockLoc[] x : bPoly.getContainedPoints()) { for (BlockLoc y : x) { if (event.getSender().getEntityWorld() == null) {
									 * System.out.println("World is null!!!"); return; } if (y != null) { event.getSender().getEntityWorld().setBlockState(new BlockPos(y.getX(),
									 * event.getSender().getCommandSenderEntity().posY - 1.0, y.getZ()), Blocks.WOOL.getDefaultState(), 3); } } }
									 */

									this.bPoly = null;
									this.editingZone = null;
									this.x = 0;
									this.y = 0;
									this.z = 0;
									this.name = "";
									this.temp = null;
								}
								catch (Exception e)
								{
									LoMaS_Utils.addChatMessage(sender, "Failed finalizing the new polyzone " + editingZone.getName());
									e.printStackTrace();
								}
							}
						}
					}
					else if (parameters[0].matches("new"))
					{
						if (parameters[1] != null && !parameters[1].isEmpty())
						{
							PolyZonesMod.processor.addZone(new PolyZone(parameters[1]));
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "Successfully created the new polyzone " + parameters[1] + ". You should add points to it using '/pz modify <name>' asap.");
							}
							DataHandler.writeObject(PolyZonesMod.processor);
						}
						else
						{
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "Improper usage. See '/pz help' for more information.");
							}
						}
					}
					else if (parameters[0].matches("modify"))
					{
						if (parameters[1] != null && !parameters[1].isEmpty())
						{
							this.editingZone = PolyZonesMod.processor.getZone(parameters[1]);
							if (this.editingZone != null)
							{
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, "You are now editing a zone named " + this.editingZone.getName() + ". Right click the corners for your desired polyzone. When you are finished, type '/pz done'.");
								}
								this.isEditing = sender.getName();
							}
							else
							{
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, "Zone not found. Use '/pz list' for a list of all existing zones.");
								}
							}
						}
						else
						{
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "Improper usage. See '/pz help' for more information.");
							}
						}
					}
				}
				else
				{
					if (FMLCommonHandler.instance().getSide() != Side.CLIENT)
					{
						LoMaS_Utils.addChatMessage(sender, "Improper usage. Type '/pz help' for more information.");
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void commandPolyzoneGeneral(CommandEvent event)
	{
		if (event.getCommand() instanceof CommandCreatePolyzone)
		{
			ICommandSender sender = event.getSender();
			String[] parameters = event.getParameters();
			if (LoMaS_Utils.isSenderAdmin(sender))
			{
				if (parameters.length > 0)
				{
					if (parameters[0].matches("help"))
					{
						if (FMLCommonHandler.instance().getSide() == Side.SERVER)
						{
							LoMaS_Utils.addChatMessage(sender, "Syntax includes:");
							LoMaS_Utils.addChatMessage(sender, "/pz new <name>"); // done
							LoMaS_Utils.addChatMessage(sender, "/pz modify <name>"); // done
							LoMaS_Utils.addChatMessage(sender, "/pz done"); // done
							LoMaS_Utils.addChatMessage(sender, "/pz delete <name>"); // done
							LoMaS_Utils.addChatMessage(sender, "/pz edit permissions <name>"); // done
							LoMaS_Utils.addChatMessage(sender, "/pz s <player(s)> <allow/deny> <permission>"); // done
							LoMaS_Utils.addChatMessage(sender, "/pz finish"); // done
							LoMaS_Utils.addChatMessage(sender, "/pz setWelcome <name> <message>"); // done
							LoMaS_Utils.addChatMessage(sender, "/pz setGoodbye <name> <message>"); // done
							LoMaS_Utils.addChatMessage(sender, "/pz setMobs <on/off>"); // done
							LoMaS_Utils.addChatMessage(sender, "/pz setPVP <on/off>"); // done
							LoMaS_Utils.addChatMessage(sender, "/pz listZones"); // done
							LoMaS_Utils.addChatMessage(sender, "/pz setFloor <name>"); // done
							LoMaS_Utils.addChatMessage(sender, "/pz setHeight <name>"); // done
							LoMaS_Utils.addChatMessage(sender, "/pz setFloor <name> <ycoord>"); // done
							LoMaS_Utils.addChatMessage(sender, "/pz setHeight <name> <ycoord>"); // done
							LoMaS_Utils.addChatMessage(sender, "/pz listUsers"); // done
							LoMaS_Utils.addChatMessage(sender, "/pz listChunks <name>"); // done
						}
					}
					else if (parameters[0].matches("delete"))
					{
						if (parameters[1] != null && !parameters[1].isEmpty())
						{
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "The zone named " + parameters[1] + " was deleted.");
							}
							PolyZonesMod.processor.removeZone(PolyZonesMod.processor.getZone(parameters[1]));
							DataHandler.writeObject(PolyZonesMod.processor);
						}
						else
						{
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "Improper usage. See '/pz help' for more information.");
							}
						}
					}
					else if (parameters[0].equalsIgnoreCase("finish"))
					{
						PolyZonesMod.playerEditing = "";
						if (!PolyZonesMod.grabbing)
						{
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "Permissions set!");
							}
						}
						else
						{
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "Y value set.");
							}
						}
						temp = null;
						this.editingZone = null;
						PolyZonesMod.grabbing = false;
						PolyZonesMod.height = false;
						DataHandler.writeObject(PolyZonesMod.processor);
					}
					else if (parameters[0].equalsIgnoreCase("setWelcome"))
					{
						String zoneName = parameters[1];
						PolyZone lol = PolyZonesMod.processor.getZone(zoneName);
						String msg = "";
						for (int i = 2; i < parameters.length; i++)
						{
							msg = msg + parameters[i] + " ";
						}
						msg = msg.trim();
						lol.getPermissions().setWelcome(msg);
						if (FMLCommonHandler.instance().getSide() == Side.SERVER)
						{
							LoMaS_Utils.addChatMessage(sender, "Welcome message set!");
						}
						DataHandler.writeObject(PolyZonesMod.processor);
					}
					else if (parameters[0].equalsIgnoreCase("setGoodbye"))
					{
						String zoneName = parameters[1];
						PolyZone lol = PolyZonesMod.processor.getZone(zoneName);
						String msg = "";
						for (int i = 2; i < parameters.length; i++)
						{
							msg = msg + parameters[i] + " ";
						}
						msg = msg.trim();
						lol.getPermissions().setGoodbye(msg);
						if (FMLCommonHandler.instance().getSide() == Side.SERVER)
						{
							LoMaS_Utils.addChatMessage(sender, "Goodbye message set");
						}
						DataHandler.writeObject(PolyZonesMod.processor);
					}
					else if (parameters[0].equalsIgnoreCase("setMobs"))
					{
						String zoneName = parameters[1];
						PolyZone lol = PolyZonesMod.processor.getZone(zoneName);
						if (parameters[2].equalsIgnoreCase("off"))
						{
							lol.getPermissions().setMobs(false);
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "Mobs are now off in " + zoneName);
							}
						}
						else if (parameters[2].equalsIgnoreCase("on"))
						{
							lol.getPermissions().setMobs(true);
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "Mobs are now on in " + zoneName);
							}
						}
						else
						{
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "Incorrect format");
							}
						}
						DataHandler.writeObject(PolyZonesMod.processor);
					}
					else if (parameters[0].equalsIgnoreCase("setPVP"))
					{
						String zoneName = parameters[1];
						PolyZone lol = PolyZonesMod.processor.getZone(zoneName);
						if (parameters[2].equalsIgnoreCase("off"))
						{
							lol.getPermissions().setPvp(false);
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "PVP is now off in " + zoneName);
							}
						}
						else if (parameters[2].equalsIgnoreCase("on"))
						{
							lol.getPermissions().setPvp(true);
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "PVP is now on in " + zoneName);
							}
						}
						else
						{
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "Incorrect format");
							}
						}
						DataHandler.writeObject(PolyZonesMod.processor);
					}
					else if (parameters[0].equalsIgnoreCase("listZones"))
					{
						if (FMLCommonHandler.instance().getSide() == Side.SERVER)
						{
							LoMaS_Utils.addChatMessage(sender, PolyZonesMod.processor.getZones().toString());
						}
					}
					else if (parameters[0].equalsIgnoreCase("setHeight"))
					{
						String zoneName = parameters[1];
						if (PolyZonesMod.playerEditing.equals("") && this.editingZone == null)
						{
							this.editingZone = PolyZonesMod.processor.getZone(zoneName);
							if (this.editingZone != null && parameters[2] != null)
							{
								int height = Integer.parseInt(parameters[2]);
								LoMaS_Utils.addChatMessage(sender, "Height has been set to " + parameters[2]);
								PolyZonesMod.processor.getZone(this.editingZone.getName()).getPolygon().setHighYAll(height);
							}
							else if (editingZone != null)
							{
								PolyZonesMod.grabbing = true;
								PolyZonesMod.playerEditing = sender.getName();
								PolyZonesMod.height = true;
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, "Right click a block to set new ceiling.");
									LoMaS_Utils.addChatMessage(sender, "USE \"/pz FINISH\" TO END! NOT \"/pz DONE\"!");
								}
							}
							this.editingZone = null;
						}
						else
						{
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "Someone else is currently editing.  Please wait");
							}
						}
					}
					else if (parameters[0].equalsIgnoreCase("setFloor"))
					{
						String zoneName = parameters[1];
						if (PolyZonesMod.playerEditing.equals("") && this.editingZone == null)
						{
							this.editingZone = PolyZonesMod.processor.getZone(zoneName);
							if (this.editingZone != null && parameters[2] != null)
							{
								int floor = Integer.parseInt(parameters[2]);
								LoMaS_Utils.addChatMessage(sender, "Floor has been set to " + parameters[2]);
								PolyZonesMod.processor.getZone(this.editingZone.getName()).getPolygon().setLowYAll(floor);
							}
							else if (editingZone != null)
							{
								PolyZonesMod.grabbing = true;
								PolyZonesMod.playerEditing = sender.getName();
								PolyZonesMod.height = false;
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, "Right click a block to set new floor.");
									LoMaS_Utils.addChatMessage(sender, "USE \"/pz FINISH\" TO END! NOT \"/pz DONE\"!");
								}
							}
							this.editingZone = null;
						}
						else
						{
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "Someone else is currently editing.  Please wait");
							}
						}
					}
					else if (parameters[0].equalsIgnoreCase("listUsers"))
					{
						for (PolyZonePlayer x : PolyZonesMod.processor.getPlayers())
						{
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								String name = sender.getEntityWorld().getPlayerEntityByUUID(x.getUUID()).getDisplayNameString();
								LoMaS_Utils.addChatMessage(sender, name);
							}
						}
						if (FMLCommonHandler.instance().getSide() == Side.SERVER)
						{
							LoMaS_Utils.addChatMessage(sender, "Completed CustomPlayer listings.");
						}
					}
					else if (parameters[0].equalsIgnoreCase("listChunks"))
					{
						String zoneName = parameters[1];
						if (zoneName != null)
						{
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								for (BlockLoc x : PolyZonesMod.processor.getZone(zoneName).getPolygon().getContainedChunks())
								{
									LoMaS_Utils.addChatMessage(sender, x.toString());
								}
							}
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "Completed chunk listings.");
							}
						}
						else
						{
							LoMaS_Utils.addChatMessage(sender, "Invalid zone name.");
						}
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void commandSetPermissionsEvent(CommandEvent event)
	{
		if (event.getCommand() instanceof CommandCreatePolyzone) // Replace with new Command
		{
			ICommandSender sender = event.getSender();
			String[] parameters = event.getParameters();
			if (LoMaS_Utils.isSenderAdmin(sender))
			{
				if (parameters[0].equals("edit"))
				{
					if (parameters[1].equals("permissions"))
					{
						if (parameters[2] != null && !parameters[2].isEmpty())
						{
							String sName = parameters[2];
							if (PolyZonesMod.playerEditing.equals(""))
							{
								if (PolyZonesMod.processor.getZone(sName) != null)
								{
									temp = PolyZonesMod.processor.getZone(sName);
									PolyZonesMod.playerEditing = sender.getName();
									if (FMLCommonHandler.instance().getSide() == Side.SERVER)
									{
										LoMaS_Utils.addChatMessage(sender, "YOU MUST USE \"/pz FINISH\" TO END! DO NOT USE \"/pz DONE\"!!!");
										LoMaS_Utils.addChatMessage(sender, "Syntax for use: /pz s [player] [allow/deny] [permission]");
									}
								}
								else
								{
									if (FMLCommonHandler.instance().getSide() == Side.SERVER)
									{
										LoMaS_Utils.addChatMessage(sender, "Not a zone.");
									}
									return;
								}
							}
							else
							{
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, "Please wait. Someone else is currently editing.");
								}
							}
						}
						else
						{
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "Improper usage. See '/pz help' for more information.");
							}
						}
					}
				}
				else if (parameters[0].equals("s") && PolyZonesMod.playerEditing.equals(sender.getName()))
				{
					String name = parameters[1];
					String ad = parameters[2];
					String what = parameters[3];
					if (name.equals("all"))
					{
						Access tem = temp.getPermissions().getDefaultPermissions();
						if (ad.equalsIgnoreCase("allow"))
						{
							if (what.equalsIgnoreCase("all"))
							{
								tem.allowAll();
							}
							else if (what.equalsIgnoreCase("enter"))
							{
								tem.allowEnter();
							}
							else if (what.equalsIgnoreCase("interact"))
							{
								tem.allowInteract();
							}
							else if (what.equalsIgnoreCase("place"))
							{
								tem.allowPlace();
							}
							else if (what.equalsIgnoreCase("destroy"))
							{
								tem.allowDestroy();
							}
							else if (what.equalsIgnoreCase("healthregen"))
							{
								tem.allowHealthRegen();
							}
							else
							{
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, "Could not set permission " + what);
								}
							}
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, name + " players can now " + what);
							}
							PolyZonesMod.processor.updateDefaultPermissions(temp.getName());
							DataHandler.writeObject(PolyZonesMod.processor);

						}
						else if (ad.equalsIgnoreCase("deny"))
						{
							if (what.equalsIgnoreCase("all"))
							{
								tem.denyAll();
							}
							else if (what.equalsIgnoreCase("enter"))
							{
								tem.denyEnter();
							}
							else if (what.equalsIgnoreCase("interact"))
							{
								tem.denyInteract();
							}
							else if (what.equalsIgnoreCase("place"))
							{
								tem.denyPlace();
							}
							else if (what.equalsIgnoreCase("destroy"))
							{
								tem.denyDestroy();
							}
							else if (what.equalsIgnoreCase("healthregen"))
							{
								tem.denyHealthRegen();
							}
							else
							{
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, "Could not set permission " + what);
								}
							}

							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, name + " players can no longer " + what);
							}
							PolyZonesMod.processor.updateDefaultPermissions(temp.getName());
							DataHandler.writeObject(PolyZonesMod.processor);
						}
						else
						{
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "Incorrect format");
							}
						}
						DataHandler.writeObject(PolyZonesMod.processor);
					}
					else
					{
						Access tem = PolyZonesMod.processor.getPlayer(sender.getEntityWorld().getPlayerEntityByName(name).getPersistentID()).getAccess(temp.getName());
						if (ad.equalsIgnoreCase("allow"))
						{
							if (what.equalsIgnoreCase("all"))
							{
								tem.allowAll();
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, name + " now has all access.");
								}
							}
							else if (what.equalsIgnoreCase("enter"))
							{
								tem.allowEnter();
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, name + " now can enter.");
								}
							}
							else if (what.equalsIgnoreCase("interact"))
							{
								tem.allowInteract();
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, name + " now can interact.");
								}
							}
							else if (what.equalsIgnoreCase("place"))
							{
								tem.allowPlace();
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, name + " now can place blocks.");
								}
							}
							else if (what.equalsIgnoreCase("destroy"))
							{
								tem.allowDestroy();
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, name + " now can destroy blocks.");
								}
							}
							else if (what.equalsIgnoreCase("healthregen"))
							{
								tem.allowHealthRegen();
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, name + " now has health regen.");
								}
							}
							else
							{
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, "Could not set permission " + what);
								}
							}
							DataHandler.writeObject(PolyZonesMod.processor);

						}
						else if (ad.equalsIgnoreCase("deny"))
						{
							if (what.equalsIgnoreCase("all"))
							{
								tem.denyAll();
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, name + " now can't do anything.");
								}
							}
							else if (what.equalsIgnoreCase("enter"))
							{
								tem.denyEnter();
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, name + " now can't enter.");
								}
							}
							else if (what.equalsIgnoreCase("interact"))
							{
								tem.denyInteract();
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, name + " now can't interact.");
								}
							}
							else if (what.equalsIgnoreCase("place"))
							{
								tem.denyPlace();
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, name + " now can't place.");
								}
							}
							else if (what.equalsIgnoreCase("destroy"))
							{
								tem.denyDestroy();
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, name + " now can't destroy.");
								}
							}
							else if (what.equalsIgnoreCase("healthregen"))
							{
								tem.denyHealthRegen();
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, name + " no longer has Health Regen.");
								}
							}
							else
							{
								if (FMLCommonHandler.instance().getSide() == Side.SERVER)
								{
									LoMaS_Utils.addChatMessage(sender, "Could not set permission " + what);
								}
							}
						}
						else
						{
							if (FMLCommonHandler.instance().getSide() == Side.SERVER)
							{
								LoMaS_Utils.addChatMessage(sender, "Incorrect format");
							}
							DataHandler.writeObject(PolyZonesMod.processor);
						}
					}
					DataHandler.writeObject(PolyZonesMod.processor);
				}
			}
		}
	}
}