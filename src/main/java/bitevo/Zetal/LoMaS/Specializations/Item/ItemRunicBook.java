package bitevo.Zetal.LoMaS.Specializations.Item;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Message.TimedUsageMessage;
import bitevo.Zetal.LoMaS.Specializations.TileWrapper.TileWrapperHoverRune;
import bitevo.Zetal.LoMaS.Specializations.TileWrapper.TileWrapperRune;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionType;
import net.minecraft.potion.PotionUtils;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemRunicBook extends Item
{
	private static final HashMap<Integer, Class<? extends TileWrapperRune>> wrapperClasses = new HashMap();
	
	public ItemRunicBook()
	{
		this.setCreativeTab(LoMaS_Utils.tabLoMaS);
	}

	@Override
	public int getMaxItemUseDuration(ItemStack stack)
	{
		return 60;
	}

	@Override
	public EnumAction getItemUseAction(ItemStack stack)
	{
		return SpecializationsMod.farming;
	}

	@Override
	public String getItemStackDisplayName(ItemStack stack)
	{
		return super.getItemStackDisplayName(stack);
	}
	
    @SideOnly(Side.CLIENT)
	@Override
    public void getSubItems(Item itemIn, CreativeTabs tab, List<ItemStack> subItems)
    {
        for (Entry<Integer, Class<? extends TileWrapperRune>> wrapClass : this.getWrapperClasses().entrySet())
        {
            subItems.add(new ItemStack(itemIn, 1, wrapClass.getKey()));
        }
    }

	/**
	 * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
	 */
	@Override
	public EnumActionResult onItemUseFirst(ItemStack stack, EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ, EnumHand hand)
	{
		if (this.canPlaceRune(world, pos))
		{
			player.setActiveHand(hand);
			return EnumActionResult.PASS;
		}
		return EnumActionResult.FAIL;
	}

	@Override
	public void onUsingTick(ItemStack stack, EntityLivingBase player, int count)
	{
		World world = player.worldObj;
		if (count % 4 == 0)
		{
			player.playSound(SoundEvents.BLOCK_REDSTONE_TORCH_BURNOUT, 0.5F, 1.4F);
		}

		if (world.isRemote)
		{
			this.onClientTick(stack, player, count);
		}
	}

	@SideOnly(Side.CLIENT)
	public void onClientTick(ItemStack stack, EntityLivingBase player, int count)
	{
		World world = player.worldObj;
		BlockPos pos = Minecraft.getMinecraft().objectMouseOver.getBlockPos();
		if (!this.canPlaceRune(world, pos))
		{
			player.stopActiveHand();
		}
	}

	@Override
	public ItemStack onItemUseFinish(ItemStack stack, World worldIn, EntityLivingBase entityLiving)
	{
		if (worldIn.isRemote && entityLiving instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) entityLiving;
			BlockPos pos = Minecraft.getMinecraft().objectMouseOver.getBlockPos();
			EnumFacing facing = Minecraft.getMinecraft().objectMouseOver.sideHit;
			SpecializationsMod.snw.sendToServer(new TimedUsageMessage(player.getPersistentID(), pos, player.inventory.currentItem, facing));
		}
		return stack;
	}

	public static boolean canPlaceRune(World world, BlockPos pos)
	{
		if (world != null && pos != null)
		{
			IBlockState state = world.getBlockState(pos);
			if (state != null)
			{
				Block b = state.getBlock();
				if (b != null)
				{
					return true;
				}
			}
		}
		return false;
	}
	
	public static void addRuneWrapper(Class<? extends TileWrapperRune> rc, int id)
	{
		if(getWrapperClasses().get(id) == null)
		{
			if(!getWrapperClasses().containsValue(rc))
			{
				getWrapperClasses().put(id, rc);
			}
			else
			{
				System.err.println("The RuneWrapper " + rc.getSimpleName() + " has already been registered and could not be re-registered.");
			}
		}
		else
		{
			System.err.println("The RuneWrapper " + rc.getSimpleName() + " cannot be added with id " + id + " because this id is already in use by the RuneWrapper " + getWrapperClasses().get(id).getSimpleName());
		}
	}

	public static HashMap<Integer, Class<? extends TileWrapperRune>> getWrapperClasses()
	{
		return wrapperClasses;
	}
}
