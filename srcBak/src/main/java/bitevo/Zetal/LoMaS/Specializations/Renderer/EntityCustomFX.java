package bitevo.Zetal.LoMaS.Specializations.Renderer;

import net.minecraft.client.particle.IParticleFactory;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.entity.Entity;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class EntityCustomFX extends Particle
{
	float particleScaleOverTime;

	public EntityCustomFX(World par1World, double par2, double par4, double par6, double par8, double par10, double par12, int... info)
	{
		this(par1World, par2, par4, par6, par8, par10, par12, 2.0F, info);
	}

	public EntityCustomFX(World par1World, double par2, double par4, double par6, double par8, double par10, double par12, float par14, int... info)
	{
		super(par1World, par2, par4, par6, 0.0D, 0.0D, 0.0D);
		this.motionX *= 0.009999999776482582D;
		this.motionY *= 0.009999999776482582D;
		this.motionZ *= 0.009999999776482582D;
		this.motionY += 0.1D;
		this.particleScale *= 0.75F;
		this.particleScale *= par14;
		this.particleScaleOverTime = this.particleScale;
		this.particleMaxAge = 16;
		this.field_190017_n = false;

		int index = 0;
		if (info[0] == 1) //Chicken
		{
			index = 202;
		}
		else if (info[0] == 2) //Wolf
		{
			index = 204;
		}
		else if (info[0] == 3) //Ocelot
		{
			index = 220;
		}
		else if (info[0] == 4) //Pig
		{
			index = 222;
		}
		else //Other
		{
			index = 206;
		}

		if (info[1] == 1) //Fed
		{
			index = index + 1;
		}
		else if (info[1] == 2) //Oldage
		{
			index = 218;
		}
		else if (info[1] == 3) //Sickness
		{
			index = 219;
		}

		this.setParticleTextureIndex(index);
	}

	@Override
	public void renderParticle(VertexBuffer renderer, Entity entity, float par2, float par3, float par4, float par5, float par6, float par7)
	{
		float f6 = ((float) this.particleAge + par2) / (float) this.particleMaxAge * 32.0F;

		if (f6 < 0.0F)
		{
			f6 = 0.0F;
		}

		if (f6 > 1.0F)
		{
			f6 = 1.0F;
		}

		this.particleScale = this.particleScaleOverTime * f6;
		super.renderParticle(renderer, entity, par2, par3, par4, par5, par6, par7);
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	public void onUpdate()
	{
		this.prevPosX = this.posX;
		this.prevPosY = this.posY;
		this.prevPosZ = this.posZ;

		if (this.particleAge++ >= this.particleMaxAge)
		{
			this.setExpired();
		}

		this.moveEntity(this.motionX, this.motionY, this.motionZ);

		if (this.posY == this.prevPosY)
		{
			this.motionX *= 1.1D;
			this.motionZ *= 1.1D;
		}

		this.motionX *= 0.8600000143051147D;
		this.motionY *= 0.8600000143051147D;
		this.motionZ *= 0.8600000143051147D;

		if (this.isCollided)
		{
			this.motionX *= 0.699999988079071D;
			this.motionZ *= 0.699999988079071D;
		}
	}

	@Override
	public int getFXLayer()
	{
		return 0; // THE IMPORTANT PART
	}

	@SideOnly(Side.CLIENT)
	public static class Factory implements IParticleFactory
	{
		public Particle getEntityFX(int p_178902_1_, World worldIn, double p_178902_3_, double p_178902_5_, double p_178902_7_, double p_178902_9_, double p_178902_11_, double p_178902_13_, int... p_178902_15_)
		{
			int[] hi = p_178902_15_;
			return new EntityCustomFX(worldIn, p_178902_3_, p_178902_5_, p_178902_7_, p_178902_9_, p_178902_11_, p_178902_13_, hi);
		}
	}
}
