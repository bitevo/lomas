package bitevo.Zetal.LoMaS.Specializations.Items;

import java.util.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;

public class ItemCustomPickaxe extends ItemPickaxe {
	public ItemCustomPickaxe(ToolMaterial p_i45327_1_) {
		super(p_i45327_1_);
	}

	@Override
	public int getMaxDamage(ItemStack stack)
	{
		return SpecializationsMod.getMaxDamage(stack);
	}

	// adds 'tooltip' text
	@SideOnly(Side.CLIENT)
	@SuppressWarnings("unchecked")
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List tooltip, boolean advanced)
	{
		SpecializationsMod.addInformation(stack, playerIn, tooltip, advanced);
	}
}
