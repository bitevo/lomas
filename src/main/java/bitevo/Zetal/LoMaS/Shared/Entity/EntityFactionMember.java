package bitevo.Zetal.LoMaS.Shared.Entity;

import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.base.Optional;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Faction;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;

public abstract class EntityFactionMember extends EntityCreature
{
	private static DataParameter<Optional<UUID>> FACTION = EntityDataManager.<Optional<UUID>> createKey(EntityFactionMember.class, DataSerializers.OPTIONAL_UNIQUE_ID);
	private static DataParameter<Optional<UUID>> COMMANDER_ID = EntityDataManager.<Optional<UUID>> createKey(EntityFactionMember.class, DataSerializers.OPTIONAL_UNIQUE_ID);
	
	public EntityFactionMember(World worldIn)
	{
		super(worldIn);
	}

	public EntityFactionMember(World worldIn, UUID faction)
	{
		super(worldIn);
		this.setFaction(faction);
	}

	@Override
	@Nullable
	public IEntityLivingData onInitialSpawn(DifficultyInstance difficulty, @Nullable IEntityLivingData livingdata)
	{
		EntityLivingBase dummySpawner = this.worldObj.findNearestEntityWithinAABB(EntityPlayer.class, new AxisAlignedBB(this.getPosition().add(-8, -8, -8), this.getPosition().add(8, 8, 8)), this);
		if (dummySpawner != null && dummySpawner instanceof EntityPlayer)
		{
			EntityPlayer dummyPlayer = (EntityPlayer) dummySpawner;
			this.setFaction(LoMaS_Player.getLoMaSPlayer(dummyPlayer.getPersistentID()).faction);
			if(this.getCommanderId() == null)
			{
				this.setCommanderId(dummyPlayer.getPersistentID());
			}
		}
		return super.onInitialSpawn(difficulty, livingdata);
	}

	@Override
	protected void entityInit()
	{
		super.entityInit();
		this.dataManager.register(FACTION, Optional.<UUID> absent());
		this.dataManager.register(COMMANDER_ID, Optional.<UUID> absent());
	}

	@Override
	public void readEntityFromNBT(NBTTagCompound compound)
	{
		super.readEntityFromNBT(compound);

		if (compound.hasKey("faction", 8))
		{
			this.setFaction(UUID.fromString(compound.getString("faction")));
		}

		if (compound.hasKey("commander", 8))
		{
			this.setCommanderId(UUID.fromString(compound.getString("commander")));
		}
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(NBTTagCompound compound)
	{
		super.writeEntityToNBT(compound);
		
		if (this.getFaction() != null)
		{
			compound.setString("faction", this.getFaction().toString());
		}
		
		if (this.getCommanderId() != null)
		{
			compound.setString("commander", this.getCommanderId().toString());
		}
	}

	@Nullable
	public UUID getFaction()
	{
		return (UUID) ((Optional) this.dataManager.get(FACTION)).orNull();
	}

	public void setFaction(@Nullable UUID uniqueId)
	{
		LoMaS_Faction oldFaction = LoMaS_Faction.getLoMaSFaction(this.getFaction());
		if(oldFaction != null)
		{
			oldFaction.removeEntity(this.getPersistentID());
		}
		
		LoMaS_Faction newFaction = LoMaS_Faction.getLoMaSFaction(uniqueId);
		if(newFaction != null)
		{
			newFaction.addEntity(this.getPersistentID());
		}
		
		this.dataManager.set(FACTION, Optional.fromNullable(uniqueId));
	}


    @Nullable
    public UUID getCommanderId()
    {
        return (UUID)((Optional)this.dataManager.get(COMMANDER_ID)).orNull();
    }

    public void setCommanderId(@Nullable UUID p_184754_1_)
    {
        this.dataManager.set(COMMANDER_ID, Optional.fromNullable(p_184754_1_));
    }

    @Nullable
    public EntityLivingBase getCommander()
    {
        try
        {
            UUID uuid = this.getCommanderId();
            return uuid == null ? null : this.worldObj.getPlayerEntityByUUID(uuid);
        }
        catch (IllegalArgumentException var2)
        {
            return null;
        }
    }

    public boolean isCommander(EntityLivingBase entityIn)
    {
        return entityIn == this.getCommander();
    }
}
