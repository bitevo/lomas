package bitevo.Zetal.LoMaS.Factions.GUI;

import org.lwjgl.opengl.GL11;

import bitevo.Zetal.LoMaS.Factions.FactionsMod;
import bitevo.Zetal.LoMaS.Factions.Inventory.ContainerObelisk;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiObelisk extends GuiContainer
{
	ResourceLocation texture = new ResourceLocation(FactionsMod.modid, "gui/obelisk_gui.png");
	private static EntityPlayer thePlayer;
	private final InventoryPlayer playerInventory;
	private ContainerObelisk tileObelisk;

	public GuiObelisk(InventoryPlayer invPlayer, ContainerObelisk inventorySlotsIn)
	{
		super(inventorySlotsIn);
		this.thePlayer = invPlayer.player;
		this.playerInventory = invPlayer;
		this.tileObelisk = inventorySlotsIn;
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int par1, int par2)
	{
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		super.drawScreen(mouseX, mouseY, partialTicks);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(texture);
		ScaledResolution res = new ScaledResolution(this.mc);
		int width = res.getScaledWidth();
		int height = res.getScaledHeight();
		int k = (width - 256) / 2;
		int l = (height - 188) / 2;

		this.drawTexturedModalRect(k, l - 15, 0, 0, 256, 188);
	}

}
