package bitevo.Zetal.LoMaS.Factions.Block;

import net.minecraft.block.BlockStairs;
import net.minecraft.block.state.IBlockState;

public class BlockRoadStairs extends BlockStairs 
{
	 public BlockRoadStairs(IBlockState state) 
	 {
		 super(state);
		 this.useNeighborBrightness = true;
	     this.setHarvestLevel("pickaxe", 0);
	 }
}
