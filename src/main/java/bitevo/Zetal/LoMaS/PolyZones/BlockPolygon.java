package bitevo.Zetal.LoMaS.PolyZones;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class BlockPolygon implements Serializable
{

	private ArrayList<BlockLoc> intersects;
	private ArrayList<BlockLine> edges;
	private BlockLoc north, south, east, west;
	private BlockLoc[][] containedPoints; // [column][]=all points in that
											// column
	private BlockLoc[] intersectedChunks; // Chunk coord x + z

	/**
	 * Constructs a new BlockPolygon with all lists empty or null.
	 */
	public BlockPolygon()
	{
		intersects = new ArrayList<BlockLoc>();
		edges = new ArrayList<BlockLine>();
	}

	public void clear()
	{
		intersects.clear();
		edges.clear();
	}

	/**
	 * Adds a Block Location to this polygon to be processed when create() is called.
	 *
	 * @param x
	 *            The BlockLoc to add.
	 */
	public void addPoint(BlockLoc x)
	{
		intersects.add(x);
	}

	private void setPoints()
	{
		north = intersects.get(0);
		south = intersects.get(0);
		east = intersects.get(0);
		west = intersects.get(0);
		for (int i = 0; i < intersects.size(); i++)
		{
			if (intersects.get(i).getZ() > north.getZ()) north = intersects.get(i);
			if (intersects.get(i).getZ() < south.getZ()) south = intersects.get(i);
			if (intersects.get(i).getX() < west.getX()) west = intersects.get(i);
			if (intersects.get(i).getX() > east.getX()) east = intersects.get(i);
		}
	}

	private void createLines()
	{
		for (int i = 0; i < intersects.size() - 1; i++)
		{
			edges.add(new BlockLine(intersects.get(i), intersects.get(i + 1)));
		}
		edges.add((new BlockLine(intersects.get(intersects.size() - 1), intersects.get(0))));
	}

	private void createContainedPoints()
	{
		int size = (int) Math.abs((east.getX() - west.getX()));
		containedPoints = new BlockLoc[size][];
		int startingPoint = (int) west.getX();
		for (int i = 0; i < size; i++)
		{// scans all x starts
			containedPoints[i] = getColumnBlocks(startingPoint + i);
		}
	}

	private BlockLoc[] getColumnBlocks(int column)
	{
		BlockLoc temp = new BlockLoc(column, (int) this.south.getZ());
		int length = (int) Math.abs((this.north.getZ() - this.south.getZ()));
		ArrayList<BlockLoc> t = new ArrayList<BlockLoc>();
		for (int i = 0; i <= length; i++)
		{
			if (scanLane(temp.clone()))
			{
				t.add(temp.clone());
			}
			else
			{
				t.add(null);
			}
			temp.incrementZ();
		}
		return Arrays.copyOf(t.toArray(), t.toArray().length, BlockLoc[].class);
	}

	private boolean scanLane(BlockLoc x)
	{
		BlockLoc temp = x.clone();
		int length = (int) Math.abs((east.getX() - west.getX()));
		if (crossedEdge(temp))
		{// if its sitting on an edge
			return true;
		}
		BlockLoc lastPointCrossed = null;
		ArrayList<BlockLine> beenCrossed = new ArrayList<BlockLine>();
		int counter = 0;
		for (int i = 0; i < length; i++)
		{
			for (BlockLine h : edges)
			{
				if (h.crossThis(temp) && !beenCrossed.contains(h))
				{
					counter = counter + 1;
					lastPointCrossed = temp.clone();
					beenCrossed.add(h);
				}
			}
			temp.decrementX();
		}

		for (BlockLoc lol : intersects)
		{
			if (lol.equalsIgnoreY(lastPointCrossed))
			{
				counter--;
				break;
			}
		}
		return (counter % 2 != 0);
	}

	private void createContainedChunks()
	{
		int numChunks = ((int) (Math.abs(east.getX() - west.getX())) * (int) (Math.abs(north.getZ() - south.getZ())));
		int counter = 0;
		intersectedChunks = new BlockLoc[numChunks];
		for (int i = 0; i < this.getContainedPoints().length; i++)
		{
			for (int j = 0; j < this.getContainedPoints()[i].length; j++)
			{
				if (this.getContainedPoints()[i][j] != null)
				{
					int x = (int) this.getContainedPoints()[i][j].getX() >> 4;
					int z = (int) this.getContainedPoints()[i][j].getZ() >> 4;
					BlockLoc chunk = new BlockLoc(x, z);
					boolean contained = false;
					for (int k = 0; k < intersectedChunks.length; k++)
					{
						if (intersectedChunks[k] == null)
						{
							break;
						}

						if (intersectedChunks[k].equalsIgnoreY(chunk))
						{
							contained = true;
							break;
						}
					}

					if (!contained)
					{
						intersectedChunks[counter] = chunk;
						counter++;
					}
				}
			}
		}
	}

	public int getLowest(int column)
	{
		BlockLoc temp = new BlockLoc(column, (int) south.getZ());
		while (!crossedEdge(temp))
		{
			temp.incrementZ();
		}
		return (int) temp.getZ();
	}

	public int getHighest(int column)
	{
		BlockLoc temp = new BlockLoc(column, (int) north.getZ());
		while (!crossedEdge(temp))
		{
			temp.decrementZ();
		}
		return (int) temp.getZ();
	}

	private boolean crossedEdge(BlockLoc x)
	{
		for (BlockLine y : edges)
		{
			if (y.crossThis(x))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Creates this polygon by initializing all variables. This includes setting the farthest points, edges, and mapping all contained points.
	 */
	public void create() throws InvalidPolygonException
	{
		try
		{
			createLines();
		}
		catch (Exception p)
		{
			p.printStackTrace();
			throw new InvalidPolygonException("Error creating lines: " + p.getStackTrace());
		}
		try
		{
			setPoints();
		}
		catch (Exception p)
		{
			p.printStackTrace();
			throw new IllegalArgumentException("Error setting points: " + p.getStackTrace());
		}
		try
		{
			createContainedPoints();
		}
		catch (Exception p)
		{
			p.printStackTrace();
			throw new InvalidPolygonException("Error getting contained points: " + p.getStackTrace());
		}
		try
		{
			createContainedChunks();
		}
		catch (Exception p)
		{
			p.printStackTrace();
			throw new InvalidPolygonException("Error getting contained chunks: " + p.getStackTrace());
		}
	}

	public void recreate() throws InvalidPolygonException
	{
		edges = new ArrayList<BlockLine>();
		create();
	}

	/**
	 * Returns the edges of this object
	 *
	 * @return edges - ArrayList of edges of this polygon.
	 */
	public ArrayList<BlockLine> getEdges()
	{
		return edges;
	}

	/**
	 * Gets the matrix of contianed points of this polygon. {@Precondition } This polygon must have already been created.
	 *
	 * @return containedPoints - The matrix of points contained in this polygon.
	 */
	public BlockLoc[][] getContainedPoints()
	{
		return containedPoints;
	}

	public BlockLoc[] getContainedChunks()
	{
		return intersectedChunks;
	}

	/**
	 * Do not call this method. It has a run time of O(n^2) and is meant solely for testing purposes. It checks if the supplied BlockLoc is contained within this polygon.
	 *
	 * @param t
	 *            The Block Location to be checked.
	 * @return True if the BlockLoc given is contained.
	 */
	public boolean isContained(BlockLoc t)
	{
		for (BlockLoc[] x : this.getContainedPoints())
		{
			for (BlockLoc y : x)
			{
				if (t.equalsIgnoreY(y)) return true;
			}
		}
		return false;
	}

	public void setLowYAll(int z)
	{
		for (BlockLoc[] x : this.getContainedPoints())
		{
			for (BlockLoc y : x)
			{
				if (y != null) y.setY(z);
			}
		}
	}

	public void setHighYAll(int z)
	{
		for (BlockLoc[] x : this.getContainedPoints())
		{
			for (BlockLoc y : x)
			{
				if (y != null) y.setHighY(z);
			}
		}
	}

	public BlockLoc getEast()
	{
		return east;
	}

	public BlockLoc getWest()
	{
		return west;
	}

	public BlockLoc getNorth()
	{
		return north;
	}

	public BlockLoc getSouth()
	{
		return south;
	}

	@Override
	public String toString()
	{
		String temp = "";
		for (int i = 0; i < (int) (east.getX() - west.getX()); i++)
		{
			temp = temp + Arrays.toString(this.getContainedPoints()[i]) + "\n";
		}
		temp = temp + "---------------------------------\n";
		for (int i = 0; i < edges.size(); i++)
		{
			temp = temp + edges.get(i).toString() + "\n";
		}
		return temp;
	}
}
