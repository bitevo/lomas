package bitevo.Zetal.LoMaS.Citizens.Item;

import net.minecraft.block.Block;
import net.minecraft.block.BlockSnow;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import bitevo.Zetal.LoMaS.Citizens.CitizensMod;

public class ItemDeed extends Item
{
	public ItemDeed()
	{
		this.setCreativeTab(CreativeTabs.MATERIALS);
	}

	@Override
    public EnumActionResult onItemUse(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
        IBlockState iblockstate = worldIn.getBlockState(pos);
        Block block = iblockstate.getBlock();
        if (block == Blocks.SNOW_LAYER && ((Integer)iblockstate.getValue(BlockSnow.LAYERS)).intValue() < 1)
        {
            facing = EnumFacing.UP;
        }
        else if (!block.isReplaceable(worldIn, pos))
        {
            pos = pos.offset(facing);
        }

        if (!playerIn.canPlayerEdit(pos, facing, stack))
        {
            return EnumActionResult.FAIL;
        }
        else if (stack.stackSize == 0)
        {
            return EnumActionResult.FAIL;
        }
        else
        {
            if (CitizensMod.citizen_chest.canPlaceBlockAt(worldIn, pos))
            {
                IBlockState iblockstate1 = CitizensMod.citizen_chest.onBlockPlaced(worldIn, pos, facing, hitX, hitY, hitZ, 0, playerIn);
                if (worldIn.setBlockState(pos, iblockstate1, 3))
                {
                    iblockstate1 = worldIn.getBlockState(pos);

                    if (iblockstate1.getBlock() == CitizensMod.citizen_chest)
                    {
                        ItemBlock.setTileEntityNBT(worldIn, playerIn, pos, stack);
                        iblockstate1.getBlock().onBlockPlacedBy(worldIn, pos, iblockstate1, playerIn, stack);
                    }

                    worldIn.playSound(playerIn, pos, CitizensMod.citizen_chest.getSoundType().getPlaceSound(), SoundCategory.BLOCKS, (CitizensMod.citizen_chest.getSoundType().getVolume() + 1.0F) / 2.0F, CitizensMod.citizen_chest.getSoundType().getPitch() * 0.8F);
                    --stack.stackSize;
                    return EnumActionResult.SUCCESS;
                }
            }
        }
        return EnumActionResult.FAIL;
	}
}
