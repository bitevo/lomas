package bitevo.Zetal.LoMaS;

import java.util.HashMap;
import java.util.Map;

import com.mojang.authlib.GameProfile;

import net.minecraft.block.Block;
import net.minecraft.block.properties.IProperty;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.server.management.UserListOps;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class LoMaS_Utils
{
	public static int entityID = 7776;

	public static int getNextEntityID()
	{
		entityID = entityID + 1;
		return entityID;
	}

	public static boolean isSenderAdmin(ICommandSender sender)
	{
		if (!sender.getEntityWorld().isRemote)
		{
			net.minecraft.server.MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
			if (server.isSinglePlayer())
			{
				return true;
			}
			UserListOps ulo = server.getPlayerList().getOppedPlayers();
			if (sender.getCommandSenderEntity() instanceof EntityPlayer)
			{
				GameProfile gp = ((EntityPlayer) sender.getCommandSenderEntity()).getGameProfile();
				int perm = ulo.getPermissionLevel(gp);
				// System.out.println(perm + " " + gp);
				if (perm >= 3)
				{
					return true;
				}
			}
		}
		return false;
	}

	public static boolean isPlayerAdmin(EntityPlayer sender)
	{
		if (!sender.worldObj.isRemote)
		{
			net.minecraft.server.MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
			if (server.isSinglePlayer())
			{
				return true;
			}
			UserListOps ulo = server.getPlayerList().getOppedPlayers();
			GameProfile gp = sender.getGameProfile();
			int perm = ulo.getPermissionLevel(gp);
			// System.out.println(perm + " " + gp);
			if (perm >= 3)
			{
				return true;
			}
		}
		return false;
	}

	public static void registerItem(Item item)
	{
		LoMaS_Utils.registerItem(item, new int[1]);
	}

	public static void registerBlock(Block block, Item item)
	{
		LoMaS_Utils.registerBlock(block, item, new int[1]);
	}

	public static void registerItem(Item item, int[] meta)
	{
		if (item.getRegistryName() == null || item.getRegistryName().equals(""))
		{
			item.setRegistryName(item.getUnlocalizedName().replace("item.", "").replace("block.", "").replace("tile.", ""));
		}
		GameRegistry.register(item);

		if (FMLCommonHandler.instance().getSide() == Side.CLIENT)
		{
			for (int i : meta)
			{
				String mrl = item.getRegistryName().getResourceDomain() + ":" + item.getRegistryName().getResourcePath();
				registerItemModelMeshers(item, i, mrl);
			}
		}
	}

	public static void registerItem(Item item, int[] meta, IProperty<?> prefix)
	{
		if (item.getRegistryName() == null || item.getRegistryName().equals(""))
		{
			item.setRegistryName(item.getUnlocalizedName().replace("item.", "").replace("block.", "").replace("tile.", ""));
		}
		GameRegistry.register(item);
		if (FMLCommonHandler.instance().getSide() == Side.CLIENT)
		{
			for (int i : meta)
			{
				String mrl = item.getRegistryName().getResourceDomain() + ":" + prefix.getAllowedValues().toArray()[i] + "_" + item.getRegistryName().getResourcePath();
				registerItemModelMeshers(item, i, mrl);
				registerItemVariants(item, mrl);
			}
		}
	}

	public static void registerItem(Item item, HashMap<Integer, String> resources)
	{
		if (item.getRegistryName() == null || item.getRegistryName().equals(""))
		{
			item.setRegistryName(item.getUnlocalizedName().replace("item.", "").replace("block.", "").replace("tile.", ""));
		}
		GameRegistry.register(item);
		if (FMLCommonHandler.instance().getSide() == Side.CLIENT)
		{
			for (Map.Entry<Integer, String> entry : resources.entrySet())
			{
				String mrl = item.getRegistryName().getResourceDomain() + ":" + entry.getValue();
				registerItemModelMeshers(item, entry.getKey(), mrl);
				registerItemVariants(item, mrl);
			}
		}
	}

	public static void registerItem(Item item, int[] meta, String[] prefixes, String[] suffixes)
	{
		if (item.getRegistryName() == null || item.getRegistryName().equals(""))
		{
			item.setRegistryName(item.getUnlocalizedName().replace("item.", "").replace("block.", "").replace("tile.", ""));
		}
		GameRegistry.register(item);
		if (FMLCommonHandler.instance().getSide() == Side.CLIENT)
		{
			for (int i : meta)
			{
				String prefix = prefixes != null ? prefixes[i] + "_" : "";
				String suffix = suffixes != null ? "_" + suffixes[i] : "";
				String mrl = item.getRegistryName().getResourceDomain() + ":" + prefix + item.getRegistryName().getResourcePath() + suffix;
				registerItemModelMeshers(item, i, mrl);
				registerItemVariants(item, mrl);
			}
		}
	}

	public static void registerBlock(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		if (item == null)
		{
			item = new ItemBlock(block);
			if (block.getRegistryName() != null && !block.getRegistryName().equals(""))
			{
				item.setRegistryName(block.getRegistryName());
			}
			LoMaS_Utils.registerBlock(block, item, new int[1]);
		}
		else
		{
			if (block.getRegistryName() == null || block.getRegistryName().equals(""))
			{
				block.setRegistryName(block.getUnlocalizedName().replace("item.", "").replace("block.", "").replace("tile.", ""));
			}
			GameRegistry.register(block);
		}
	}

	public static void registerBlock(Block block, Item item, int[] meta)
	{
		if (block.getRegistryName() == null || block.getRegistryName().equals(""))
		{
			block.setRegistryName(block.getUnlocalizedName().replace("item.", "").replace("block.", "").replace("tile.", ""));
		}
		GameRegistry.register(block);
		if (item != null)
		{
			LoMaS_Utils.registerItem(item, meta);
		}
	}

	public static void registerBlock(Block block, Item item, int[] meta, IProperty<?> prefix)
	{
		if (block.getRegistryName() == null || block.getRegistryName().equals(""))
		{
			block.setRegistryName(block.getUnlocalizedName().replace("item.", "").replace("block.", "").replace("tile.", ""));
		}
		GameRegistry.register(block);

		if (FMLCommonHandler.instance().getSide() == Side.CLIENT)
		{
			registerBlockWithStateMapper(block, item, meta, prefix);
		}

		if (item != null)
		{
			LoMaS_Utils.registerItem(item, meta, prefix);
		}
	}

	@SideOnly(Side.CLIENT)
	public static void registerBlockWithStateMapper(Block block, Item item, int[] meta, IProperty<?> prefix)
	{
		net.minecraft.client.Minecraft.getMinecraft().getBlockRendererDispatcher().getBlockModelShapes().registerBlockWithStateMapper(block, (new net.minecraft.client.renderer.block.statemap.StateMap.Builder()).withName(prefix).withSuffix("_" + block.getRegistryName().getResourcePath()).build());
	}

	@SideOnly(Side.CLIENT)
	public static void registerItemModelMeshers(Item item, int meta, String mrls)
	{
		ModelResourceLocation mrl = new ModelResourceLocation(mrls, "inventory");
		net.minecraft.client.renderer.RenderItem itemRenderer = net.minecraft.client.Minecraft.getMinecraft().getRenderItem();
		itemRenderer.getItemModelMesher().register(item, meta, mrl);
	}

	@SideOnly(Side.CLIENT)
	public static void registerItemVariants(Item item, String mrls)
	{
		ModelResourceLocation mrl = new ModelResourceLocation(mrls, "inventory");
		net.minecraft.client.renderer.block.model.ModelBakery.registerItemVariants(item, mrl);
	}

	public static int getBlockID(World world, int x, int y, int z)
	{
		return Block.getIdFromBlock(world.getBlockState(new BlockPos(x, y, z)).getBlock());
	}

	public static Block getBlock(World world, int x, int y, int z)
	{
		return world.getBlockState(new BlockPos(x, y, z)).getBlock();
	}

	public static void setBlock(World world, int x, int y, int z, int blockID)
	{
		Block block = Block.getBlockById(blockID);
		world.setBlockState(new BlockPos(x, y, z), block.getDefaultState());
	}

	public static void setBlock(World world, int x, int y, int z, Block block)
	{
		world.setBlockState(new BlockPos(x, y, z), block.getDefaultState());
	}

	@SideOnly(Side.SERVER)
	public static int getBlockID(WorldServer world, int x, int y, int z)
	{
		return Block.getIdFromBlock(world.getBlockState(new BlockPos(x, y, z)).getBlock());
	}

	@SideOnly(Side.SERVER)
	public static Block getBlock(WorldServer world, int x, int y, int z)
	{
		return world.getBlockState(new BlockPos(x, y, z)).getBlock();
	}

	@SideOnly(Side.SERVER)
	public static void setBlock(WorldServer server, int x, int y, int z, int blockID)
	{
		Block block = Block.getBlockById(blockID);
		server.setBlockState(new BlockPos(x, y, z), block.getDefaultState());
	}

	@SideOnly(Side.SERVER)
	public static void setBlock(WorldServer server, int x, int y, int z, Block block)
	{
		server.setBlockState(new BlockPos(x, y, z), block.getDefaultState());
	}
}
