package bitevo.Zetal.LoMaS.Specializations.Block;

import java.util.Random;

import javax.annotation.Nullable;

import com.google.common.base.Predicate;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySapBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockSap extends BlockContainer
{
	public static final PropertyDirection FACING = PropertyDirection.create("facing", new Predicate<EnumFacing>()
	{
		@Override
		public boolean apply(@Nullable EnumFacing p_apply_1_)
		{
			return p_apply_1_ != EnumFacing.DOWN;
		}
	});
	protected static final AxisAlignedBB WEST_AABB = new AxisAlignedBB(0.0F, 0.2F, 0.4F, 0.01F, 0.7625F, 0.65F);
	protected static final AxisAlignedBB EAST_AABB = new AxisAlignedBB(.99F, 0.2F, 0.38F, 1.0F, 0.7625F, 0.63F);
	protected static final AxisAlignedBB NORTH_AABB = new AxisAlignedBB(0.38F, 0.2F, 0.0F, 0.63F, 0.7625F, 0.01F);
	protected static final AxisAlignedBB SOUTH_AABB = new AxisAlignedBB(0.4F, 0.2F, 0.99F, 0.65F, 0.7625F, 1.0F);

	public BlockSap()
	{
		super(Material.FIRE);
		this.setTickRandomly(true);
		this.setDefaultState(this.blockState.getBaseState().withProperty(FACING, EnumFacing.NORTH));
	}

	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		switch ((EnumFacing) state.getValue(FACING))
		{
			case EAST:
				return EAST_AABB;
			case WEST:
				return WEST_AABB;
			case SOUTH:
				return SOUTH_AABB;
			case NORTH:
				return NORTH_AABB;
			default:
				return NORTH_AABB;
		}
	}

	@Override
	@Nullable
	public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, World worldIn, BlockPos pos)
	{
		return NULL_AABB;
	}

	/**
	 * Used to determine ambient occlusion and culling when rebuilding chunks for render
	 */
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}

	@Override
	public boolean isReplaceable(IBlockAccess worldIn, BlockPos pos)
	{
		return true;
	}

	/**
	 * Ticks the block if it's been scheduled
	 */
	@Override
	public void updateTick(World par1World, BlockPos pos, IBlockState state, Random par5Random)
	{
		Random rand = new Random();
		if (!this.canBlockStay(par1World, pos))
		{
			// System.out.println("Sap tick -> Block Can't Be Here (How did you get there???)");
			par1World.setBlockToAir(pos);
		}
		else if (rand.nextInt(4) == 0)
		{
			// System.out.println("Sap tick -> Removal triggered");
			par1World.setBlockToAir(pos);
		}
	}

	/**
	 * Can this block stay at this position. Similar to canPlaceBlockAt except gets checked often with plants.
	 */
	public boolean canBlockStay(World world, BlockPos pos)
	{
		IBlockState state = world.getBlockState(pos);
		EnumFacing face = (EnumFacing) state.getValue(FACING);
		BlockPos p = pos.add(face.getFrontOffsetX(), 0, face.getFrontOffsetZ());
		Block i1 = world.getBlockState(p).getBlock();
		return (i1.equals(Blocks.LOG) || i1.equals(Blocks.LOG2)) && i1.getMetaFromState(world.getBlockState(p)) <= 1;
	}

	/**
	 * If this block doesn't render as an ordinary block it will return False (examples: signs, buttons, stairs, etc)
	 */
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public IBlockState onBlockPlaced(World par1World, BlockPos pos, EnumFacing facing, float par6, float par7, float par8, int par9, EntityLivingBase placer)
	{
		return this.getDefaultState().withProperty(FACING, facing.getOpposite());
	}

	@Override
	public boolean canPlaceBlockAt(World par1World, BlockPos pos)
	{
		if (SpecializationsMod.isTouchingLog(par1World, pos))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Lets the block know when one of its neighbor changes. Doesn't know which neighbor changed (coordinates passed are their own) Args: x, y, z, neighbor blockID
	 */
	@Override
	public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn)
	{
		if (!this.canBlockStay(worldIn, pos))
		{
			worldIn.setBlockToAir(pos);
		}
	}

	/**
	 * Drops the block items with a specified chance of dropping the specified items
	 */
	@Override
	public void dropBlockAsItemWithChance(World par1World, BlockPos pos, IBlockState state, float par6, int par7)
	{
	}

	@Override
	public Item getItemDropped(IBlockState par1, Random par2Random, int par3)
	{
		return null;
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, @Nullable ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		if (LoMaS_Player.getLoMaSPlayer(playerIn).getSkills().getMatchingNodesFromName("Sap Collection") >= 1)
		{
			if (worldIn.isRemote)
			{
				return true;
			}
			else
			{
				ItemStack itemstack = playerIn.inventory.getCurrentItem();

				if (itemstack == null)
				{
					return true;
				}
				else
				{
					if (itemstack.getItem() == Items.GLASS_BOTTLE)
					{
						Random rand = new Random();
						ItemStack itemstack1 = new ItemStack(SpecializationsMod.SAPBOTTLE, 1, 0);
						if (!playerIn.capabilities.isCreativeMode)
						{
							float progressEarned = 20.0f;
							LoMaS_Player.getLoMaSPlayer(playerIn).addSpecProgress(progressEarned);
							worldIn.playSound((EntityPlayer) null, playerIn.posX, playerIn.posY, playerIn.posZ, SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, SoundCategory.PLAYERS, 0.1F, 0.5F * ((this.RANDOM.nextFloat() - this.RANDOM.nextFloat()) * 0.7F + 1.8F));
						}

						if (!playerIn.inventory.addItemStackToInventory(itemstack1))
						{
							worldIn.spawnEntityInWorld(new EntityItem(worldIn, (double) pos.getX() + 0.5D, (double) pos.getY() + 1.5D, (double) pos.getZ() + 0.5D, itemstack1));
						}
						else if (playerIn instanceof EntityPlayerMP)
						{
							((EntityPlayerMP) playerIn).sendContainerToPlayer(playerIn.inventoryContainer);
						}

						--itemstack.stackSize;

						if (itemstack.stackSize <= 0)
						{
							playerIn.inventory.setInventorySlotContents(playerIn.inventory.currentItem, (ItemStack) null);
						}
						worldIn.setBlockToAir(pos);
					}
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		IBlockState iblockstate = this.getDefaultState();

		switch (meta)
		{
			case 1:
				iblockstate = iblockstate.withProperty(FACING, EnumFacing.EAST);
				break;
			case 2:
				iblockstate = iblockstate.withProperty(FACING, EnumFacing.WEST);
				break;
			case 3:
				iblockstate = iblockstate.withProperty(FACING, EnumFacing.SOUTH);
				break;
			case 4:
				iblockstate = iblockstate.withProperty(FACING, EnumFacing.NORTH);
				break;
			case 5:
			default:
				iblockstate = iblockstate.withProperty(FACING, EnumFacing.NORTH);
		}

		return iblockstate;
	}

	/**
	 * Convert the BlockState into the correct metadata value
	 */
	@Override
	public int getMetaFromState(IBlockState state)
	{
		int i = 0;

		switch ((EnumFacing) state.getValue(FACING))
		{
			case EAST:
				i = i | 1;
				break;
			case WEST:
				i = i | 2;
				break;
			case SOUTH:
				i = i | 3;
				break;
			case NORTH:
				i = i | 4;
				break;
			case DOWN:
			default:
				i = i | 5;
		}

		return i;
	}

	@Override
	public IBlockState withRotation(IBlockState state, Rotation rot)
	{
		return state.withProperty(FACING, rot.rotate((EnumFacing) state.getValue(FACING)));
	}

	/**
	 * Returns the blockstate with the given mirror of the passed blockstate. If inapplicable, returns the passed blockstate.
	 */
	@Override
	public IBlockState withMirror(IBlockState state, Mirror mirrorIn)
	{
		return state.withRotation(mirrorIn.toRotation((EnumFacing) state.getValue(FACING)));
	}

	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, new IProperty[] { FACING });
	}

	@Override
	public TileEntity createNewTileEntity(World world, int i)
	{
		return new TileEntitySapBlock();
	}
}
