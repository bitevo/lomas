package bitevo.Zetal.LoMaS.Specializations.Renderer;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class ModelDiamondShard extends ModelBase
{
    public ModelRenderer shard;

    public ModelDiamondShard()
    {
        this.textureWidth = 32;
        this.textureHeight = 32;
        this.shard = new ModelRenderer(this, 0, 0);
        this.shard.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.shard.addBox(-1.5F, -1.5F, -1.5F, 3, 3, 3, 0.0F);
    }

    /**
     * Sets the models various rotation angles then renders the model.
     */
    public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale)
    {
        GlStateManager.translate(this.shard.offsetX, this.shard.offsetY, this.shard.offsetZ);
        GlStateManager.translate(this.shard.rotationPointX * scale, this.shard.rotationPointY * scale, this.shard.rotationPointZ * scale);
        GlStateManager.scale(2.0D, 0.5D, 0.5D);
        GlStateManager.translate(-this.shard.offsetX, -this.shard.offsetY, -this.shard.offsetZ);
        GlStateManager.translate(-this.shard.rotationPointX * scale, -this.shard.rotationPointY * scale, -this.shard.rotationPointZ * scale);
        this.setRotationAngles(limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale, entityIn);
        this.shard.render(scale);
    }

    /**
     * Sets the model's various rotation angles. For bipeds, par1 and par2 are used for animating the movement of arms
     * and legs, where par1 represents the time(so that arms and legs swing back and forth) and par2 represents how
     * "far" arms and legs can swing at most.
     */
    public void setRotationAngles(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn)
    {
    	shard.rotateAngleX = (netHeadYaw * 0.017453292F);
        shard.rotateAngleY = 0.7853981633974483F;
        shard.rotateAngleZ = 0.7853981633974483F + (headPitch * 0.017453292F);
    }
}
