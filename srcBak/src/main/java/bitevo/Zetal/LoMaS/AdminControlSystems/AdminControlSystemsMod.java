package bitevo.Zetal.LoMaS.AdminControlSystems;

import net.minecraft.command.ServerCommandManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;

@Mod(modid = AdminControlSystemsMod.modid, name = "ACS", version = "0.1")

public class AdminControlSystemsMod 
{
	public static String shapeEditor = "";
	public static final String modid = "lomas_acs";
    public static SimpleNetworkWrapper snw;

	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		
	}
	
	public void preInit(FMLPreInitializationEvent event)
	{
		snw = NetworkRegistry.INSTANCE.newSimpleChannel(modid);
	}

	@EventHandler
	public void serverStart(FMLServerStartingEvent event)
	{
		ServerCommandManager scm = (ServerCommandManager) event.getServer().getCommandManager();
		scm.registerCommand(new CommandCheckInventory());
		scm.registerCommand(new CommandCheckChests());
		scm.registerCommand(new CommandCreateShape());
		scm.registerCommand(new CommandACS());

		MinecraftForge.EVENT_BUS.register(new EventHookContainerClass());
		FMLCommonHandler.instance().bus().register(new MotDConnectionHandler());
		WhitelistCheck hi = new WhitelistCheck();
		hi.server = event.getServer();
		Thread thread = new Thread(hi);
		thread.start();
	 }
}
