package bitevo.Zetal.LoMaS.BigFMod.TileEntity;

import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class TileEntityLeavesDecayed extends TileEntity
{
	/** The stored delay before a new spawn. */
	public int counter = 0;
	private int leafType = 0;

	/** The extra NBT data to add to spawned entities */

	@Override
	public void updateContainingBlockInfo()
	{
		super.updateContainingBlockInfo();
	}

	public int getLeafType()
	{
		return this.leafType;
	}

	public void setLeafType(int meta)
	{
		this.leafType = meta;
	}

	public void setCounter(int c)
	{
		this.counter = c;
	}

	public int getCounter()
	{
		return this.counter;
	}

	@Override
	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newState)
	{
		return !oldState.getBlock().equals(newState.getBlock());
	}

	/**
	 * Reads a tile entity from NBT.
	 */
	@Override
	public void readFromNBT(NBTTagCompound par1NBTTagCompound)
	{
		super.readFromNBT(par1NBTTagCompound);
		this.leafType = par1NBTTagCompound.getInteger("LeafType");
		this.counter = par1NBTTagCompound.getShort("Counter");
	}

	/**
	 * Writes a tile entity to NBT.
	 * 
	 * @return
	 */
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound par1NBTTagCompound)
	{
		super.writeToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setInteger("LeafType", this.leafType);
		par1NBTTagCompound.setShort("Counter", (short) this.counter);
		return par1NBTTagCompound;
	}
}
