package bitevo.Zetal.LoMaS.BigFMod;

import org.lwjgl.opengl.GL11;

import bitevo.Zetal.LoMaS.BigFMod.TileEntity.TileEntityBookstand;
import net.minecraft.client.model.ModelBook;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class TileEntityBookstandRenderer extends TileEntitySpecialRenderer
{
	private ModelBookstand standmodel;
	private ModelBook bookmodel = new ModelBook();

	public TileEntityBookstandRenderer()
	{
		standmodel = new ModelBookstand();
	}

	public void renderAModelAt(TileEntity tile, double d, double d1, double d2)
	{
		if (tile.getBlockMetadata() == 4)
		{
			// System.out.println("4");

			this.bindTexture(new ResourceLocation(BigFMod.modid, "textures/entities/bookstand.png"));
			GL11.glPushMatrix(); // start
			GL11.glTranslatef((float) d + 0.5F, (float) d1 + 1.5F, (float) d2 + 0.5F); // size
			GL11.glRotatef(0, 0.0F, 1.0F, 0.0F); // change the first 0 in like 90 to make it rotate 90 degrees.
			GL11.glScalef(1.0F, -1F, -1F); // to make your block have a normal positioning. comment out to see what happens
			standmodel.renderModel(0.0625F);
			GL11.glPushMatrix(); // start
			GL11.glEnable(GL11.GL_CULL_FACE);
			if (tile != null && tile instanceof TileEntityBookstand && ((TileEntityBookstand) tile).hasBook())
			{
				this.bindTexture(new ResourceLocation("textures/entity/enchanting_table_book.png"));
				GL11.glRotatef(180, -0.35F, 1.0F, 0.0F);
				// GL11.glRotatef(-36, 0.0F, 0.0F, 1.0F);
				GL11.glTranslatef(-0.75F, 0.95F, 0.0F);
				bookmodel.render((Entity) null, 1.0F, 0.0F, 0.0F, 1.25F, 0.0F, 0.0625F);
			}
			GL11.glPopMatrix();
			GL11.glPopMatrix(); // end
		}
		else if (tile.getBlockMetadata() == 3)
		{
			// System.out.println("3?");
			this.bindTexture(new ResourceLocation(BigFMod.modid, "textures/entities/bookstand.png"));
			GL11.glPushMatrix(); // start
			GL11.glTranslatef((float) d + 0.5F, (float) d1 + 1.5F, (float) d2 + 0.5F); // size
			GL11.glRotatef(90, 0.0F, 1.0F, 0.0F); // change the first 0 in like 90 to make it rotate 90 degrees.
			GL11.glScalef(1.0F, -1F, -1F); // to make your block have a normal positioning. comment out to see what happens
			standmodel.renderModel(0.0625F);
			GL11.glPushMatrix(); // start
			GL11.glEnable(GL11.GL_CULL_FACE);
			if (tile != null && tile instanceof TileEntityBookstand && ((TileEntityBookstand) tile).hasBook())
			{
				this.bindTexture(new ResourceLocation("textures/entity/enchanting_table_book.png"));
				GL11.glRotatef(180, -0.525F, 1.5F, 0.0F);
				// GL11.glRotatef(-36, 0.0F, 0.0F, 1.0F);
				GL11.glTranslatef(-0.75F, 0.95F, 0.0F);
				bookmodel.render((Entity) null, 1.0F, 0.0F, 0.0F, 1.25F, 0.0F, 0.0625F);
			}
			GL11.glPopMatrix();
			GL11.glPopMatrix(); // end
		}
		else if (tile.getBlockMetadata() == 2)
		{
			// System.out.println("2");
			this.bindTexture(new ResourceLocation(BigFMod.modid, "textures/entities/bookstand.png"));
			GL11.glPushMatrix(); // start
			GL11.glTranslatef((float) d + 0.5F, (float) d1 + 1.5F, (float) d2 + 0.5F); // size
			GL11.glRotatef(-90, 0.0F, 1.0F, 0.0F); // change the first 0 in like 90 to make it rotate 90 degrees.
			GL11.glScalef(1.0F, -1F, -1F); // to make your block have a normal positioning. comment out to see what happens
			standmodel.renderModel(0.0625F);
			GL11.glPushMatrix(); // start
			GL11.glEnable(GL11.GL_CULL_FACE);
			if (tile != null && tile instanceof TileEntityBookstand && ((TileEntityBookstand) tile).hasBook())
			{
				this.bindTexture(new ResourceLocation("textures/entity/enchanting_table_book.png"));
				GL11.glRotatef(180, -0.2F, .5F, 0.0F);
				// GL11.glRotatef(-36, 0.0F, 0.0F, 1.0F);
				GL11.glTranslatef(-.81F, 0.91F, 0.0F);
				bookmodel.render((Entity) null, 1.0F, 0.0F, 0.0F, 1.25F, 0.0F, 0.0625F);
			}
			GL11.glPopMatrix();
			GL11.glPopMatrix(); // end
		}
		else if (tile.getBlockMetadata() == 5)
		{
			// System.out.println("5?");
			this.bindTexture(new ResourceLocation(BigFMod.modid, "textures/entities/bookstand.png"));
			GL11.glPushMatrix(); // start
			GL11.glTranslatef((float) d + 0.5F, (float) d1 + 1.5F, (float) d2 + 0.5F); // size
			GL11.glRotatef(-180, 0.0F, 1.0F, 0.0F); // change the first 0 in like 90 to make it rotate 90 degrees.
			GL11.glScalef(1.0F, -1F, -1F); // to make your block have a normal positioning. comment out to see what happens
			standmodel.renderModel(0.0625F);
			GL11.glPushMatrix(); // start
			GL11.glEnable(GL11.GL_CULL_FACE);
			if (tile != null && tile instanceof TileEntityBookstand && ((TileEntityBookstand) tile).hasBook())
			{
				this.bindTexture(new ResourceLocation("textures/entity/enchanting_table_book.png"));
				GL11.glRotatef(180, -0.35F, 1.0F, 0.0F);
				// GL11.glRotatef(-36, 0.0F, 0.0F, 1.0F);
				GL11.glTranslatef(-0.75F, 0.95F, 0.0F);
				bookmodel.render((Entity) null, 1.0F, 0.0F, 0.0F, 1.25F, 0.0F, 0.0625F);
			}
			GL11.glPopMatrix();
			GL11.glPopMatrix(); // end
		}
		else
		{
			// System.out.println("Wwhaa?");
		}
	}

	@Override
	public void renderTileEntityAt(TileEntity tile, double posX, double posY, double posZ, float f1, int i1)
	{
		renderAModelAt((TileEntityBookstand) tile, posX, posY, posZ); // where to render
	}
}
