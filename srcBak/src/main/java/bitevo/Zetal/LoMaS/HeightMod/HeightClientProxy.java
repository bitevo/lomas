package bitevo.Zetal.LoMaS.HeightMod;

import net.minecraft.client.Minecraft;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class HeightClientProxy extends HeightCommonProxy
{
	public static Minecraft mc;

    @Override
    public void init() {
        MinecraftForge.EVENT_BUS.register(this);
    }

    @SubscribeEvent
    public void worldChange(WorldEvent.Load event) {
        if (event.getWorld().provider.getDimension() == 0) {
            event.getWorld().provider.setCloudRenderer(CloudHeight.INSTANCE);
        }
    }
}
