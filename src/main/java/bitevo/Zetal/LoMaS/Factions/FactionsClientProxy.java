package bitevo.Zetal.LoMaS.Factions;

import bitevo.Zetal.LoMaS.Factions.Entity.EntityPigmanWarrior;
import bitevo.Zetal.LoMaS.Factions.Entity.EntitySiegeGolem;
import bitevo.Zetal.LoMaS.Factions.Handler.FactionKeyBinds;
import bitevo.Zetal.LoMaS.Factions.Renderer.FactionRendererFactory;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class FactionsClientProxy extends FactionsCommonProxy
{
	public static Minecraft mc;

	@Override
	public void registerRenderInformation()
	{
		FactionRendererFactory fr = new FactionRendererFactory();
		RenderingRegistry.registerEntityRenderingHandler(EntityPigmanWarrior.class, fr.new WarriorFactory());
		RenderingRegistry.registerEntityRenderingHandler(EntitySiegeGolem.class, fr.new SiegeGolemFactory());

		init();
	}

	@Override
	public void init()
	{
		FactionsMod.clientStart();
		FMLCommonHandler.instance().bus().register(new FactionKeyBinds());
	}
}
