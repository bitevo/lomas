package bitevo.Zetal.LoMaS.Citizens.Entities.ai;

import net.minecraft.entity.ai.EntityAIBase;
import bitevo.Zetal.LoMaS.Citizens.Entities.EntityPigman;

public class EntityAIGoHome extends EntityAIBase
{
	public EntityPigman host;
	public int ticks = 0;

	public EntityAIGoHome(EntityPigman entityPigman)
	{
		this.host = entityPigman;
        this.setMutexBits(1);
	}

	@Override
	public boolean shouldExecute()
	{
		ticks = ticks + host.getRNG().nextInt(1) + 1;
		if (ticks % 10 == 0 && (this.host.getDistanceSqToCenter(this.host.getHomeChest()) > 48.0 || this.host.isWet()))
		{
			return true;
		}
		return false;
	}

	public boolean continueExecuting()
	{
		return false;
	}

	@Override
	public void startExecuting()
	{
		boolean success = this.host.getNavigator().tryMoveToXYZ(this.host.getHomeChest().getX(), this.host.getHomeChest().getY(), this.host.getHomeChest().getZ(), 0.3D);
		if(!success)
		{
			this.host.setHappiness(this.host.getHappiness() - 5);
			this.host.setUnhappyReason("I'm lost");
		}
	}
}
