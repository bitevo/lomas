package bitevo.Zetal.LoMaS.Specializations.Capabilities;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;

public class CapabilitiesProvider implements ICapabilitySerializable<NBTTagCompound>
{
    IPlayerCapability inst = LoMaS_Player.PLAYER_CAP.getDefaultInstance();
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return capability == LoMaS_Player.PLAYER_CAP;
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
        return capability == LoMaS_Player.PLAYER_CAP ? LoMaS_Player.PLAYER_CAP.<T>cast(inst) : null;
	}

	@Override
	public NBTTagCompound serializeNBT()
	{
		return (NBTTagCompound) LoMaS_Player.PLAYER_CAP.getStorage().writeNBT(LoMaS_Player.PLAYER_CAP, inst, null);
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt)
	{
		LoMaS_Player.PLAYER_CAP.getStorage().readNBT(LoMaS_Player.PLAYER_CAP, inst, null, nbt);
	}
}
