package bitevo.Zetal.LoMaS.Specializations.Inventory.Slot;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Inventory.SpecContainerRepair;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

//private
public class SlotCore extends Slot
{
	final World theWorld;

	final BlockPos blockPos;

	final int slotID;

	/** The anvil this slot belongs to. */
	final SpecContainerRepair anvil;
	
	public static ResourceLocation slotIronBG = new ResourceLocation(SpecializationsMod.MODID, "textures/items/ironHeartSlot.png");
	public static ResourceLocation slotEmeraldBG = new ResourceLocation(SpecializationsMod.MODID, "textures/items/emeraldHeartSlot.png");

	// private
	public SlotCore(SpecContainerRepair specContainerRepair, IInventory par2IInventory, int par3, int par4, int par5, World par6World, BlockPos pos)
	{
		super(par2IInventory, par3, par4, par5);
		this.anvil = specContainerRepair;
		this.theWorld = par6World;
		this.blockPos = pos;
		this.slotID = par3;
	}

	/**
	 * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
	 */
	@Override
	public boolean isItemValid(ItemStack par1ItemStack)
	{
		if (this.slotID == 0 && par1ItemStack.getItem().equals(SpecializationsMod.EMERALDHEART))
		{
			return true;
		}
		else if (this.slotID == 1 && par1ItemStack.getItem().equals(SpecializationsMod.IRONHEART))
		{
			return true;
		}
		return false;
	}

	/*@Override
    @Nullable
    @SideOnly(Side.CLIENT)
    public String getSlotTexture()
    {
		return this.slotID == 0 ? "lomas_scm:items/emeraldHeartSlot" : "lomas_scm:items/ironHeartSlot";
	}*/

	/**
	 * Return whether this slot's stack can be taken from this slot.
	 */
	@Override
	public boolean canTakeStack(EntityPlayer par1EntityPlayer)
	{
		return true;
	}

	@Override
	public void onPickupFromSlot(EntityPlayer par1EntityPlayer, ItemStack par2ItemStack)
	{
	}
}
