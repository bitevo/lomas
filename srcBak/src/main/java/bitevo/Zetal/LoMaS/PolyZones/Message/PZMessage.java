package bitevo.Zetal.LoMaS.PolyZones.Message;

import io.netty.buffer.ByteBuf;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.UUID;

import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import bitevo.Zetal.LoMaS.PolyZones.PolyProcessor;

public class PZMessage implements IMessage
{
	public UUID sender;
	public int piece;
	public ArrayList array;

	public PZMessage(){}

	public PZMessage(PolyProcessor processor, int piece, UUID sender)
	{
		this.sender = sender;
		this.piece = piece;
		switch (piece)
		{
			case 0:
				this.array = processor.players;
				break;
			case 1:
				this.array = processor.activePlayers;
				break;
			case 2:
				this.array = processor.defaultAccess;
				break;
			case 3:
				this.array = processor.zones;
				break;
		}
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		piece = buf.readInt();
		sender = UUID.fromString(ByteBufUtils.readUTF8String(buf));
		byte[] bytes = new byte[buf.readableBytes()];
		buf.readBytes(bytes);
		ArrayList a = this.bytesToArray(bytes);
		if(a != null)
		{
			this.array = a;
		}
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeInt(piece);
		ByteBufUtils.writeUTF8String(buf, sender.toString());
		byte[] bytes = this.arrayToBytes(array);
		if(bytes != null)
		{
			buf.writeBytes(bytes);
		}
	}

	public byte[] arrayToBytes(ArrayList yourObject)
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		try
		{
			out = new ObjectOutputStream(bos);
			out.writeObject(yourObject);
			byte[] yourBytes = bos.toByteArray();
			return yourBytes;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if (out != null)
				{
					out.close();
				}
			}
			catch (IOException ex)
			{
			}
			try
			{
				bos.close();
			}
			catch (IOException ex)
			{
			}
		}
		return null;
	}

	public ArrayList bytesToArray(byte[] yourBytes)
	{
		ByteArrayInputStream bis = new ByteArrayInputStream(yourBytes);
		ObjectInput in = null;
		try
		{
			in = new ObjectInputStream(bis);
			Object o = in.readObject();
			return (ArrayList) o;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				bis.close();
			}
			catch (IOException ex)
			{
			}
			try
			{
				if (in != null)
				{
					in.close();
				}
			}
			catch (IOException ex)
			{
			}
		}
		return null;
	}
}
