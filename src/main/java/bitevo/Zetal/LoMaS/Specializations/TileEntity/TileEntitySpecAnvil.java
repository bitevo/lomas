package bitevo.Zetal.LoMaS.Specializations.TileEntity;

import java.util.UUID;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEntitySpecAnvil extends TileEntity implements ITickable, ISidedInventory
{
	private EntityPlayer usingPlayer = null;
	private int curCraftTime;
	private int reqCraftTime;
	
	private UUID owner;
	private LoMaS_Player lPlayer;
	
	@Override
	public void update()
	{
		boolean flag = this.isCrafting();
		boolean flag1 = false;

		if (flag && this.curCraftTime < this.reqCraftTime)
		{
			this.curCraftTime++;
		}

		if (flag1)
		{
			this.markDirty();
		}

		if (!this.worldObj.isRemote)
		{
			if (this.usingPlayer == null)
			{
				if (!this.worldObj.isRemote)
				{
					for (int i = 1; i < this.getSizeInventory(); ++i)
					{
						ItemStack itemstack = this.removeStackFromSlot(i);

						if (itemstack != null)
						{
							this.worldObj.spawnEntityInWorld(new EntityItem(this.worldObj, this.pos.getX(), this.pos.getY(), this.pos.getZ(), itemstack));
						}
					}
				}
				this.clear();
			}
		}
	}

	@Override
	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newState)
	{
		return !oldState.getBlock().equals(newState.getBlock());
	}

	public boolean isCrafting()
	{
		return this.curCraftTime > 0;
	}

	@SideOnly(Side.CLIENT)
	public static boolean isCrafting(IInventory p_174903_0_)
	{
		return p_174903_0_.getField(0) > 0;
	}

	@Override
	public void readFromNBT(NBTTagCompound compound)
	{
		super.readFromNBT(compound);

		this.curCraftTime = compound.getShort("craftTime");
		this.reqCraftTime = compound.getShort("craftTimeTotal");
		if (compound.hasKey("owner"))
		{
			try
			{
				this.setOwner(UUID.fromString(compound.getString("owner")));
			}
			catch (Exception e)
			{
				this.setOwner(null);
			}
		}
		if(this.getOwner() != null)
		{
			this.lPlayer = LoMaS_Player.getLoMaSPlayer(getOwner());
		}
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound)
	{
		super.writeToNBT(compound);
		if (this.getOwner() != null)
		{
			try
			{
				compound.setString("owner", getOwner().toString());
			}
			catch (Exception e)
			{
			}
		}
		compound.setShort("craftTime", (short) this.curCraftTime);
		compound.setShort("craftTimeTotal", (short) this.reqCraftTime);

		return compound;
	}

	@Override
	public int getField(int id)
	{
		switch (id)
		{
			case 0:
				return this.curCraftTime;
			case 1:
				return this.reqCraftTime;
			default:
				return 0;
		}
	}

	@Override
	public void setField(int id, int value)
	{
		switch (id)
		{
			case 0:
				this.curCraftTime = value;
				break;
			case 1:
				this.reqCraftTime = value;
				break;
		}
	}

	@Override
	public int getFieldCount()
	{
		return 2;
	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		return false;
	}

	@Override
	public ITextComponent getDisplayName()
	{
		return new TextComponentTranslation("Workbench");
	}

	@Override
	public int[] getSlotsForFace(EnumFacing side)
	{
		return null;
	}

	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction)
	{
		return false;
	}

	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction)
	{
		return false;
	}

	public EntityPlayer getUsingPlayer()
	{
		return usingPlayer;
	}

	public void setUsingPlayer(EntityPlayer usingPlayer)
	{
		this.usingPlayer = usingPlayer;
	}

	public int getCurCraftTime()
	{
		return curCraftTime;
	}

	public void setCurCraftTime(int curCraftTime)
	{
		this.curCraftTime = curCraftTime;
	}

	public int getReqCraftTime()
	{
		return reqCraftTime;
	}

	public void setReqCraftTime(int reqCraftTime)
	{
		this.reqCraftTime = reqCraftTime;
	}

	public UUID getOwner()
	{
		return owner;
	}

	public void setOwner(UUID owner)
	{
		this.owner = owner;
		if(owner != null)
		{
			this.lPlayer = LoMaS_Player.getLoMaSPlayer(owner);
		}
	}

	public LoMaS_Player getlPlayer()
	{
		return lPlayer;
	}

	public void setlPlayer(LoMaS_Player lPlayer)
	{
		this.lPlayer = lPlayer;
	}

	@Override
	public int getSizeInventory()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ItemStack getStackInSlot(int index)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemStack decrStackSize(int index, int count)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemStack removeStackFromSlot(int index)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getInventoryStackLimit()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void openInventory(EntityPlayer player)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void closeInventory(EntityPlayer player)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clear()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasCustomName()
	{
		// TODO Auto-generated method stub
		return false;
	}
}
