package bitevo.Zetal.LoMaS.Specializations.Capabilities;

import bitevo.Zetal.LoMaS.Economy.Inventory.InventoryBank;
import bitevo.Zetal.LoMaS.Specializations.Inventory.InventoryPlayer;
import bitevo.Zetal.LoMaS.Specializations.Inventory.SpecPlayerContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;

public class PlayerCapabilityStorage implements IStorage<IPlayerCapability>
{
	public final static String BANK = "ExtendedPlayer_BANK";

	@Override
	public NBTBase writeNBT(Capability<IPlayerCapability> capability, IPlayerCapability instance, EnumFacing side)
	{
		NBTTagCompound compound = new NBTTagCompound();
		NBTTagCompound nbttagcompound;

		InventoryBank bank = instance.getInventoryBank();
		NBTTagList bankTag = new NBTTagList();
		for (int i = 0; i < bank.getSizeInventory(); ++i)
		{
			if (bank.getStackInSlot(i) != null)
			{
				nbttagcompound = new NBTTagCompound();
				nbttagcompound.setByte("Slot", (byte) i);
				bank.getStackInSlot(i).writeToNBT(nbttagcompound);
				bankTag.appendTag(nbttagcompound);
			}
		}
		compound.setTag(BANK, bankTag);
		return compound;
	}

	@Override
	public void readNBT(Capability<IPlayerCapability> capability, IPlayerCapability instance, EnumFacing side, NBTBase nbt)
	{
		// This is fired just before the player data is loaded from the NBT- so if we want to preserve coin data, we need to change the server
		// so that they use our inventory, otherwise coins will be lost. Fix-a-roo!
		EntityPlayer player = instance.getEntityPlayer();
		if (!(player.inventory instanceof InventoryPlayer))
		{
			InventoryPlayer newInv = new InventoryPlayer(player);
			newInv.readFromNBT(player.inventory.writeToNBT(new NBTTagList()));
			player.inventory = newInv;
		}

		if (!(player.inventoryContainer instanceof SpecPlayerContainer))
		{
			player.inventoryContainer = new SpecPlayerContainer((InventoryPlayer) player.inventory, !player.worldObj.isRemote, player);
			player.openContainer = player.inventoryContainer;
			player.inventory.markDirty();
		}

		NBTTagList bankTag = (NBTTagList) ((NBTTagCompound) nbt).getTag(BANK);
		InventoryBank bank = instance.getInventoryBank();
		for (int i = 0; i < bankTag.tagCount(); ++i)
		{
			NBTTagCompound nbttagcompound = bankTag.getCompoundTagAt(i);
			int j = nbttagcompound.getByte("Slot") & 255;
			ItemStack itemstack = ItemStack.loadItemStackFromNBT(nbttagcompound);

			if (itemstack != null)
			{
				if (j >= 0 && j < bank.getSizeInventory())
				{
					bank.setInventorySlotContents(j, itemstack);
				}
			}
		}
		instance.setInventoryBank(bank);
	}
}