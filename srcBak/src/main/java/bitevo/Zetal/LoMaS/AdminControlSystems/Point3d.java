package bitevo.Zetal.LoMaS.AdminControlSystems;

public class Point3d 
{
   public int x, y, z; 

   public Point3d(int x_value, int y_value, int z_value) 
   {
      x = x_value; 
      y = y_value; 
      z = z_value;
   }
}