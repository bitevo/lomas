package bitevo.Zetal.LoMaS.BigFMod.Handler;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.IFuelHandler;
import bitevo.Zetal.LoMaS.BigFMod.Items.ItemStick;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;

public class BigFFuelHandler implements IFuelHandler
{

	@Override
	public int getBurnTime(ItemStack fuel)
	{
		if(fuel.getItem() instanceof ItemStick)
		{
			return 100;
		}
		else if(fuel.getItem().equals(Item.getItemFromBlock(SpecializationsMod.SAPLING)))
		{
			return 100;
		}
		return 0;
	}

}
