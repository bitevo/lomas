package bitevo.Zetal.LoMaS.Citizens;

import bitevo.Zetal.LoMaS.Citizens.Entity.EntityPigmanCitizen;
import bitevo.Zetal.LoMaS.Citizens.Renderer.CitizenRendererFactory;
import bitevo.Zetal.LoMaS.Citizens.Renderer.TileEntityCitizenChestRenderer;
import bitevo.Zetal.LoMaS.Citizens.TileEntity.TileEntityCitizenChest;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class CitizensClientProxy extends CitizensCommonProxy
{
	public static Minecraft mc;

	@Override
	public void registerRenderInformation()
	{
		mc = Minecraft.getMinecraft();

		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCitizenChest.class, new TileEntityCitizenChestRenderer());
		RenderManager rm = mc.getRenderManager();
		RenderingRegistry.registerEntityRenderingHandler(EntityPigmanCitizen.class, new CitizenRendererFactory());
		init();
	}

	@Override
	public void init()
	{

	}
}
