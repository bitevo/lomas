package bitevo.Zetal.LoMaS.Specializations.Items;

import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockNetherWart;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockSapling;
import bitevo.Zetal.LoMaS.Specializations.Message.TimedUsageMessage;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntityPlant;
import net.minecraft.block.Block;
import net.minecraft.block.IGrowable;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemFertilizer extends Item
{
	public ItemFertilizer()
	{
		this.setCreativeTab(CreativeTabs.MATERIALS);
	}

	@Override
	public int getMaxItemUseDuration(ItemStack stack)
	{
		return 60;
	}

	@Override
	public EnumAction getItemUseAction(ItemStack stack)
	{
		return SpecializationsMod.farming;
	}

	/**
	 * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
	 */
	@Override
    public EnumActionResult onItemUseFirst(ItemStack stack, EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ, EnumHand hand)
	{
		if (ItemFertilizer.canBeFertilized(world, pos))
		{
			player.setActiveHand(hand);
			return EnumActionResult.PASS;
		}
		else if(ItemFertilizer.getFertilized(world, pos) != -1)
		{
			int fert = ItemFertilizer.getFertilized(world, pos);
			if(fert >= 5000)
			{
				player.addChatMessage(new TextComponentTranslation("It looks like this plant was just fertilized."));
			}
			else if(fert >= 3000)
			{
				player.addChatMessage(new TextComponentTranslation("It looks like this plant was fertilized recently."));
			}
			else if(fert >= 1000)
			{
				player.addChatMessage(new TextComponentTranslation("It looks like this plant was fertilized awhile ago."));
			}
			else if(fert >= 0)
			{
				player.addChatMessage(new TextComponentTranslation("It looks like this plant will need to be fertilized soon."));
			}
		}
		return EnumActionResult.FAIL;
	}

	@Override
    public void onUsingTick(ItemStack stack, EntityLivingBase player, int count)
	{
		World world = player.worldObj;
		if(count % 4 == 0)
		{
	    	player.playSound(SoundEvents.BLOCK_GRASS_STEP, 0.5F, 1.4F);
		}

		if (world.isRemote)
		{
			this.onClientTick(stack, player, count);
		}
	}

	@SideOnly(Side.CLIENT)
	public void onClientTick(ItemStack stack, EntityLivingBase player, int count)
	{
		World world = player.worldObj;
		BlockPos pos = Minecraft.getMinecraft().objectMouseOver.getBlockPos();
		if (!this.canBeFertilized(world, pos))
		{
			player.stopActiveHand();
		}
	}

	@Override
    public ItemStack onItemUseFinish(ItemStack stack, World worldIn, EntityLivingBase entityLiving)
	{
		// System.out.println("finish");
		if (worldIn.isRemote && entityLiving instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) entityLiving;
			BlockPos pos = Minecraft.getMinecraft().objectMouseOver.getBlockPos();
			SpecializationsMod.snw.sendToServer(new TimedUsageMessage(player.getPersistentID(), pos, player.inventory.currentItem));
		}
		return stack;
	}

	public static boolean canBeFertilized(World world, BlockPos pos)
	{
		if(world != null && pos != null)
		{
			IBlockState state = world.getBlockState(pos);
			if(state != null)
			{
				Block b = state.getBlock();
				if(b != null)
				{
					TileEntity entity = world.getTileEntity(pos);
					if (entity != null && b instanceof IGrowable && entity instanceof TileEntityPlant
					&& !(b instanceof BlockSapling) && !(b instanceof BlockNetherWart))
					{
						IGrowable growable = (IGrowable) b;
						TileEntityPlant tep = (TileEntityPlant) entity;
						if (!tep.isFertilized() && growable.canGrow(world, pos, state, false))
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public static int getFertilized(World world, BlockPos pos)
	{
		if(world != null && pos != null)
		{
			IBlockState state = world.getBlockState(pos);
			if(state != null)
			{
				Block b = state.getBlock();
				if(b != null)
				{
					TileEntity entity = world.getTileEntity(pos);
					if (entity != null && b instanceof IGrowable && entity instanceof TileEntityPlant
					&& !(b instanceof BlockSapling) && !(b instanceof BlockNetherWart))
					{
						IGrowable growable = (IGrowable) b;
						TileEntityPlant tep = (TileEntityPlant) entity;
						if (tep.isFertilized())
						{
							return tep.getFertTime();
						}
					}
				}
			}
		}
		return -1;
	}
}
