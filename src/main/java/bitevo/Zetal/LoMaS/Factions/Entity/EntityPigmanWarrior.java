package bitevo.Zetal.LoMaS.Factions.Entity;

import java.util.Arrays;
import java.util.UUID;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.Factions.FactionsMod;
import bitevo.Zetal.LoMaS.Factions.Entity.ai.EntityAIEngageEnemyFaction;
import bitevo.Zetal.LoMaS.Factions.Entity.ai.EntityAIFollowCommander;
import bitevo.Zetal.LoMaS.Factions.Entity.ai.EntityAIRangedAttack;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import bitevo.Zetal.LoMaS.Shared.Entity.EntityFactionMember;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAIMoveTowardsRestriction;
import net.minecraft.entity.ai.EntityAIOpenDoor;
import net.minecraft.entity.ai.EntityAIRestrictOpenDoor;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityTippedArrow;
import net.minecraft.init.Enchantments;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.inventory.IInventoryChangedListener;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.item.ItemArrow;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.pathfinding.PathNavigateGround;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityPigmanWarrior extends EntityFactionMember implements IInventoryChangedListener
{
	private static final DataParameter<Boolean> SWINGING_ARMS = EntityDataManager.<Boolean> createKey(EntityPigmanWarrior.class, DataSerializers.BOOLEAN);
	private final EntityAIRangedAttack aiArrowAttack = new EntityAIRangedAttack(this, 1.0D, 20, 15.0F);
	private final EntityAIAttackMelee aiAttackOnCollide = new EntityAIAttackMelee(this, 1.2D, false)
	{
		@Override
		public void resetTask()
		{
			super.resetTask();
			EntityPigmanWarrior.this.setSwingingArms(false);
		}

		@Override
		public void startExecuting()
		{
			super.startExecuting();
			EntityPigmanWarrior.this.setSwingingArms(true);
		}
	};
	public InventoryBasic inventory;
	private int state = 0;

	public EntityPigmanWarrior(World worldIn, UUID faction)
	{
		super(worldIn, faction);
		commonConstruct(worldIn);
	}

	public EntityPigmanWarrior(World worldIn)
	{
		super(worldIn);
		commonConstruct(worldIn);
	}

	public void commonConstruct(World worldIn)
	{
		this.setPathPriority(PathNodeType.WATER, -1.0F);
		((PathNavigateGround) this.getNavigator()).setEnterDoors(true);
		((PathNavigateGround) this.getNavigator()).setCanSwim(true);
		((PathNavigateGround) this.getNavigator()).setBreakDoors(true);
		this.tasks.addTask(0, new EntityAISwimming(this));
		this.tasks.addTask(5, new EntityAIFollowCommander(this, 1.0D, 10.0F, 2.0F));
		this.tasks.addTask(8, new EntityAIRestrictOpenDoor(this));
		this.tasks.addTask(9, new EntityAIOpenDoor(this, true));
		this.tasks.addTask(10, new EntityAIMoveTowardsRestriction(this, 0.3D));
		this.tasks.addTask(11, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
		this.tasks.addTask(12, new EntityAIEngageEnemyFaction(this, true));
		this.setCanPickUpLoot(true);
		this.setSize(0.6F, 1.95F);
		this.setCombatTask();
		this.initInventory();
		this.stepHeight = 1.0F;
	}

	@Override
	protected void applyEntityAttributes()
	{
		super.applyEntityAttributes();
		this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.35D);
		double baseHealth = 50.00D;
		if(this.getCommanderId() == null || LoMaS_Player.getLoMaSPlayer(this.getCommanderId()) == null)
		{
			this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(baseHealth);
		}
		else
		{
			float mod = 1.0f + LoMaS_Player.getLoMaSPlayer(this.getCommanderId()).getSkills().getPercentMagnitudeFromName("Soldier Health");
			this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(baseHealth * mod);
		}
		this.getAttributeMap().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE);
	}

	@Override
	public boolean attackEntityAsMob(Entity entityIn)
	{
		float f = (float) this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).getAttributeValue();
		int i = 0;

		if (entityIn instanceof EntityLivingBase)
		{
			f += EnchantmentHelper.getModifierForCreature(this.getHeldItemMainhand(), ((EntityLivingBase) entityIn).getCreatureAttribute());
			i += EnchantmentHelper.getKnockbackModifier(this);
		}

		boolean flag = entityIn.attackEntityFrom(DamageSource.causeMobDamage(this), f);

		if (flag)
		{
			if (i > 0 && entityIn instanceof EntityLivingBase)
			{
				((EntityLivingBase) entityIn).knockBack(this, (float) i * 0.5F, (double) MathHelper.sin(this.rotationYaw * 0.017453292F), (double) (-MathHelper.cos(this.rotationYaw * 0.017453292F)));
				this.motionX *= 0.6D;
				this.motionZ *= 0.6D;
			}

			int j = EnchantmentHelper.getFireAspectModifier(this);

			if (j > 0)
			{
				entityIn.setFire(j * 4);
			}

			if (entityIn instanceof EntityPlayer)
			{
				EntityPlayer entityplayer = (EntityPlayer) entityIn;
				ItemStack itemstack = this.getHeldItemMainhand();
				ItemStack itemstack1 = entityplayer.isHandActive() ? entityplayer.getActiveItemStack() : null;

				if (itemstack != null && itemstack1 != null && itemstack.getItem() instanceof ItemAxe && itemstack1.getItem() == Items.SHIELD)
				{
					float f1 = 0.25F + (float) EnchantmentHelper.getEfficiencyModifier(this) * 0.05F;

					if (this.rand.nextFloat() < f1)
					{
						entityplayer.getCooldownTracker().setCooldown(Items.SHIELD, 100);
						this.worldObj.setEntityState(entityplayer, (byte) 30);
					}
				}
			}

			this.applyEnchantments(this, entityIn);
		}

		return flag;
	}

	@Override
	protected void entityInit()
	{
		super.entityInit();
		this.dataManager.register(SWINGING_ARMS, Boolean.valueOf(false));
	}

	/**
	 * Attack the specified entity using a ranged attack.
	 */
	public void attackEntityWithRangedAttack(EntityLivingBase target, float p_82196_2_)
	{
		EntityTippedArrow entitytippedarrow = new EntityTippedArrow(this.worldObj, this);
		if (LoMaS_Utils.SCM)
		{
			int meta = 1;
			ItemStack stack = this.findAmmo();
			if (stack != null)
			{
				if (stack.getMetadata() > meta)
				{
					meta = stack.getMetadata();
					stack.stackSize--;
				}
			}
			entitytippedarrow = new bitevo.Zetal.LoMaS.Specializations.Entity.EntityCustomArrow(this.worldObj, this, meta);
		}
		double d0 = target.posX - this.posX;
		double d1 = target.getEntityBoundingBox().minY + (double) (target.height / 3.0F) - entitytippedarrow.posY;
		double d2 = target.posZ - this.posZ;
		double d3 = (double) MathHelper.sqrt_double(d0 * d0 + d2 * d2);
		entitytippedarrow.setThrowableHeading(d0, d1 + d3 * 0.20000000298023224D, d2, 1.6F, (float) (14 - this.worldObj.getDifficulty().getDifficultyId() * 4));
		int i = EnchantmentHelper.getMaxEnchantmentLevel(Enchantments.POWER, this);
		int j = EnchantmentHelper.getMaxEnchantmentLevel(Enchantments.PUNCH, this);
		DifficultyInstance difficultyinstance = this.worldObj.getDifficultyForLocation(new BlockPos(this));
		entitytippedarrow.setDamage((double) (p_82196_2_ * 2.0F) + this.rand.nextGaussian() * 0.25D + (double) ((float) this.worldObj.getDifficulty().getDifficultyId() * 0.11F));

		if (i > 0)
		{
			entitytippedarrow.setDamage(entitytippedarrow.getDamage() + (double) i * 0.5D + 0.5D);
		}

		if (j > 0)
		{
			entitytippedarrow.setKnockbackStrength(j);
		}

		boolean flag = EnchantmentHelper.getMaxEnchantmentLevel(Enchantments.FLAME, this) > 0;

		if (flag)
		{
			entitytippedarrow.setFire(100);
		}

		ItemStack itemstack = this.getHeldItem(EnumHand.OFF_HAND);

		if (itemstack != null && ((LoMaS_Utils.SCM && this.getHeldItemMainhand().getItem() == bitevo.Zetal.LoMaS.Specializations.SpecializationsMod.ARROW) || this.getHeldItemMainhand().getItem() == Items.TIPPED_ARROW))
		{
			entitytippedarrow.setPotionEffect(itemstack);
		}

		this.playSound(SoundEvents.ENTITY_ARROW_SHOOT, 1.0F, 1.0F / (this.getRNG().nextFloat() * 0.4F + 0.8F));
		this.worldObj.spawnEntityInWorld(entitytippedarrow);
	}

	@Override
	public void onLivingUpdate()
	{
		this.updateArmSwingProgress();
		if (this.getHealth() < this.getMaxHealth() && this.ticksExisted % 20 == 0)
		{
			this.heal(1.0F);
		}
		super.onLivingUpdate();
	}

	@Override
	public void fall(float distance, float damageMultiplier)
	{
		super.fall(distance / 2.0F, damageMultiplier);
	}

	@Override
	public void onInventoryChanged(InventoryBasic invBasic)
	{
		/*if (!this.worldObj.isRemote)
		{
			System.out.println("hey.");
			this.setCombatTask();
		}*/
	}

	@Nullable
	@Override
	public ItemStack getItemStackFromSlot(EntityEquipmentSlot slotIn)
	{
		ItemStack itemstack = null;

		switch (slotIn.getSlotType())
		{
			case HAND:
				itemstack = this.inventory.getStackInSlot(slotIn.getIndex());
				break;
			case ARMOR:
				itemstack = this.inventory.getStackInSlot(slotIn.getIndex() + 2);
				break;
		}

		return itemstack;
	}

	@Override
	public void setItemStackToSlot(EntityEquipmentSlot slotIn, @Nullable ItemStack stack)
	{
		//System.out.println("hey2.");
		switch (slotIn.getSlotType())
		{
			case HAND:
				this.inventory.setInventorySlotContents(slotIn.getIndex(), stack);
				this.inventory.markDirty();
				break;
			case ARMOR:
				this.inventory.setInventorySlotContents(slotIn.getIndex() + 2, stack);
				this.inventory.markDirty();
				break;
		}
		this.setCombatTask();
	}

	private void initInventory()
	{
		InventoryBasic inv = this.inventory;
		this.inventory = new InventoryBasic("Warrior", false, 6);
		this.inventory.setCustomName(this.getName());

		if (inv != null)
		{
			inv.removeInventoryChangeListener(this);
			int i = Math.min(inv.getSizeInventory(), this.inventory.getSizeInventory());

			for (int j = 0; j < i; ++j)
			{
				ItemStack itemstack = inv.getStackInSlot(j);

				if (itemstack != null)
				{
					this.inventory.setInventorySlotContents(j, itemstack.copy());
				}
			}
		}

		this.inventory.addInventoryChangeListener(this);
		this.onInventoryChanged(this.inventory);
		this.itemHandler = new net.minecraftforge.items.wrapper.InvWrapper(this.inventory);
	}

	@Override
	public ItemStack getHeldItem(EnumHand hand)
	{
		return this.inventory == null ? null : this.inventory.getStackInSlot(hand == EnumHand.MAIN_HAND ? 0 : 1);
	}

	@Override
	public ItemStack getHeldItemMainhand()
	{
		return this.inventory != null ? this.inventory.getStackInSlot(0) : null;
	}

	@Override
	public ItemStack getHeldItemOffhand()
	{
		return this.inventory != null ? this.inventory.getStackInSlot(1) : null;
	}

	public Iterable<ItemStack> getHeldEquipment()
	{
		return Arrays.<ItemStack> asList(this.inventory.getStackInSlot(0), this.inventory.getStackInSlot(1));
	}

	public Iterable<ItemStack> getArmorInventoryList()
	{
		return Arrays.<ItemStack> asList(this.inventory.getStackInSlot(2), this.inventory.getStackInSlot(3), this.inventory.getStackInSlot(4), this.inventory.getStackInSlot(5));
	}

	@Override
	public boolean attackEntityFrom(DamageSource source, float amount)
	{
		return this.isEntityInvulnerable(source) ? false : super.attackEntityFrom(source, amount);
	}

	protected boolean isBowInMainhand()
	{
		return this.findAmmo() != null && (this.getHeldItemMainhand() != null && ((LoMaS_Utils.SCM && this.getHeldItemMainhand().getItem() == bitevo.Zetal.LoMaS.Specializations.SpecializationsMod.BOW) || this.getHeldItemMainhand().getItem() == Items.BOW));
	}

	public void setCombatTask()
	{
		if (isBowInMainhand())
		{
			System.out.println("Arrows!");
			int i = 20;
			this.aiArrowAttack.setAttackCooldown(i);
			this.tasks.addTask(4, this.aiArrowAttack);
			this.tasks.removeTask(this.aiAttackOnCollide);
		}
		else
		{
			System.out.println("Melee!");
			this.tasks.addTask(4, this.aiAttackOnCollide);
			this.tasks.removeTask(this.aiArrowAttack);
		}
	}

	public ItemStack findAmmo()
	{
		ItemStack stackOff = this.getHeldItem(EnumHand.OFF_HAND);
		ItemStack stackMain = this.getHeldItem(EnumHand.MAIN_HAND);
		if (stackOff != null && stackOff.getItem() instanceof ItemArrow)
		{
			return this.getHeldItem(EnumHand.OFF_HAND);
		}
		else if (stackMain != null && stackMain.getItem() instanceof ItemArrow)
		{
			return this.getHeldItem(EnumHand.MAIN_HAND);
		}
		return null;
	}

	@Override
	public void readEntityFromNBT(NBTTagCompound compound)
	{
		super.readEntityFromNBT(compound);

		this.initInventory();
		NBTTagList nbttaglist = compound.getTagList("Items", 10);
		this.inventory = new InventoryBasic("Warrior", false, 6);
		for (int i = 0; i < nbttaglist.tagCount(); ++i)
		{
			NBTTagCompound nbttagcompound1 = (NBTTagCompound) nbttaglist.getCompoundTagAt(i);
			int j = nbttagcompound1.getByte("Slot") & 255;

			if (j >= 0 && j < this.inventory.getSizeInventory())
			{
				this.inventory.setInventorySlotContents(j, ItemStack.loadItemStackFromNBT(nbttagcompound1));
			}
		}

		this.state = compound.getInteger("state");

		this.setCombatTask();
	}

	@Override
	public void writeEntityToNBT(NBTTagCompound compound)
	{
		NBTTagList nbttaglist = new NBTTagList();
		for (int i = 0; i < this.inventory.getSizeInventory(); ++i)
		{
			if (this.inventory.getStackInSlot(i) != null)
			{
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte) i);
				this.inventory.getStackInSlot(i).writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}
		compound.setTag("Items", nbttaglist);
		compound.setInteger("state", this.state);
		super.writeEntityToNBT(compound);
	}

	@Override
	protected boolean processInteract(EntityPlayer player, EnumHand hand, @Nullable ItemStack stack)
	{
		//if (this.isCommander(player))
		//{
			player.openGui(FactionsMod.instance, 0, this.worldObj, this.getEntityId(), -1, -1);
		//}
		//else
		//{
		//	player.addChatMessage(new TextComponentTranslation("He does not recognize your authority."));
		//}
		return false;
	}

	@SideOnly(Side.CLIENT)
	public boolean isSwingingArms()
	{
		return ((Boolean) this.dataManager.get(SWINGING_ARMS)).booleanValue();
	}

	public void setSwingingArms(boolean swingingArms)
	{
		this.dataManager.set(SWINGING_ARMS, Boolean.valueOf(swingingArms));
	}

	@Override
	protected boolean canDespawn()
	{
		return false;
	}

	private net.minecraftforge.items.IItemHandler itemHandler = null; // Initialized by initHorseChest above.

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getCapability(net.minecraftforge.common.capabilities.Capability<T> capability, net.minecraft.util.EnumFacing facing)
	{
		if (capability == net.minecraftforge.items.CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) return (T) itemHandler;
		return super.getCapability(capability, facing);
	}

	@Override
	public boolean hasCapability(net.minecraftforge.common.capabilities.Capability<?> capability, net.minecraft.util.EnumFacing facing)
	{
		return capability == net.minecraftforge.items.CapabilityItemHandler.ITEM_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
	}

	public int getState()
	{
		return state;
	}

	public void setState(int state)
	{
		this.state = state;
	}

	public boolean isIdle()
	{
		return state == 0;
	}
}
