package bitevo.Zetal.LoMaS.HeightMod;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = HeightMod.MODID, name = "Height", version = HeightMod.VERSION)
public class HeightMod
{
    public static final String MODID = "lomas_height";
    public static final String VERSION = "0.1";
	@SidedProxy(clientSide = "bitevo.Zetal.LoMaS.HeightMod.HeightClientProxy", serverSide = "bitevo.Zetal.LoMaS.HeightMod.HeightCommonProxy")
	public static HeightCommonProxy proxy = new HeightCommonProxy();
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
    }
	
    @EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
    	proxy.init();
	}
	
    @EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
	}
}
