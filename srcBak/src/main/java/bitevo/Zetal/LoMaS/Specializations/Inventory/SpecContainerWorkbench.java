package bitevo.Zetal.LoMaS.Specializations.Inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Inventory.Slot.SlotCrafting;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySpecWorkbench;

public class SpecContainerWorkbench extends Container
{
	private TileEntitySpecWorkbench tileBench;
    private World worldObj;
    private BlockPos pos;
    
    private EntityPlayer player;

    public SpecContainerWorkbench(InventoryPlayer playerInventory, TileEntitySpecWorkbench bench, World worldIn, BlockPos pos)
    {
    	this.tileBench = bench;
        this.worldObj = worldIn;
        this.player = playerInventory.player;
        this.pos = pos;
        this.addSlotToContainer(new SlotCrafting(playerInventory.player, bench, 0, 124, 35));
        int i;
        int j;
        

        for (i = 0; i < 3; ++i)
        {
            for (j = 0; j < 3; ++j)
            {
                this.addSlotToContainer(new Slot(bench, 1 + j + i * 3, 30 + j * 18, 17 + i * 18));
            }
        }

        for (i = 0; i < 3; ++i)
        {
            for (j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(playerInventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        for (i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(playerInventory, i, 8 + i * 18, 142));
        }
        
        if(pos != null)
        {
            bench.setUsingPlayer(this.player);
            bench.onCraftMatrixChanged(bench, this.player);
        }
    }
    
    @Override
    public ItemStack slotClick(int slotId, int clickedButton, ClickType mode, EntityPlayer playerIn)
    {
    	if(slotId == 0)
    	{
        	Slot slotClicked = this.getSlot(slotId);
        	if(slotClicked instanceof SlotCrafting)
        	{
        		if(this.tileBench.getCurCraftTime() == 0)
        		{
        			ItemStack stack = slotClicked.getStack();
        			if(stack != null && stack.getItem() != null)
        			{
                		this.tileBench.setCurCraftTime(1);
        			}
        		}
        	}
    	}
    	return super.slotClick(slotId, clickedButton, mode, playerIn);
    }

    public void onContainerClosed(EntityPlayer playerIn)
    {
        super.onContainerClosed(playerIn);

        this.tileBench.setUsingPlayer(null);
        if (!this.worldObj.isRemote)
        {
            for (int i = 1; i < tileBench.getSizeInventory(); ++i)
            {
                ItemStack itemstack = this.tileBench.removeStackFromSlot(i);

                if (itemstack != null)
                {
                    playerIn.dropItem(itemstack, false);
                }
            }
        }
    }

    public boolean canInteractWith(EntityPlayer playerIn)
    {
        return this.worldObj.getBlockState(this.pos).getBlock() != SpecializationsMod.CRAFTING_TABLE ? false : playerIn.getDistanceSq((double)this.pos.getX() + 0.5D, (double)this.pos.getY() + 0.5D, (double)this.pos.getZ() + 0.5D) <= 64.0D;
    }

    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(index);

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if (index == 0)
            {
                if(!player.worldObj.isRemote && this.shouldDamage(itemstack1))
                {
        			SpecializationsMod.setDamagedItem(itemstack1, player, false);
                }
                
                if (!this.mergeItemStack(itemstack1, 10, 46, true))
                {
                    return null;
                }
                slot.onSlotChange(itemstack1, itemstack);
            }
            else if (index >= 10 && index < 37)
            {
                if (!this.mergeItemStack(itemstack1, 37, 46, false))
                {
                    return null;
                }
            }
            else if (index >= 37 && index < 46)
            {
                if (!this.mergeItemStack(itemstack1, 10, 37, false))
                {
                    return null;
                }
            }
            else if (!this.mergeItemStack(itemstack1, 10, 46, false))
            {
                return null;
            }

            if (itemstack1.stackSize == 0)
            {
                slot.putStack((ItemStack)null);
            }
            else
            {
                slot.onSlotChanged();
            }

            if (itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }

            slot.onPickupFromSlot(playerIn, itemstack1);
        }

        return itemstack;
    }

    public boolean canMergeSlot(ItemStack p_94530_1_, Slot p_94530_2_)
    {
        return p_94530_2_.inventory != this.tileBench && super.canMergeSlot(p_94530_1_, p_94530_2_);
    }	
    
    public static ItemStack findSpecRecipe(InventoryCrafting par1InventoryCrafting, World worldIn, EntityPlayer player)
	{
    	ItemStack finalStack = null;
    	if(player != null)
    	{
    		finalStack = CraftingManager.getInstance().findMatchingRecipe(par1InventoryCrafting, player.worldObj);
    		LoMaS_Player specEntityPlayer = LoMaS_Player.getLoMaSPlayer(player);

    		int i = 0;
    		ItemStack itemstack = null;
    		ItemStack itemstack1 = null;
    		int j;

    		for (j = 0; j < par1InventoryCrafting.getSizeInventory(); ++j)
    		{
    			ItemStack itemstack2 = par1InventoryCrafting.getStackInSlot(j);

    			if (itemstack2 != null)
    			{
    				if (i == 0)
    				{
    					itemstack = itemstack2;
    				}

    				if (i == 1)
    				{
    					itemstack1 = itemstack2;
    				}

    				++i;
    			}
    		}

    		if (i == 2 && itemstack != null && itemstack1 != null && itemstack.getItem().equals(itemstack1.getItem()) && itemstack.stackSize == 1 && itemstack1.stackSize == 1)
    		{
    			if(itemstack.getItem().isRepairable())
    			{
    				//System.out.println("It's a repair recipe! Kill it!");
    				// if in here, it's a repair recipe!!!
    				finalStack = null;
    			}
    		}

    		if (finalStack != null)
    		{
    			//System.out.println(finalStack.getItem() + " " + SpecializationsMod.axeHardWood);
    			if ((!(specEntityPlayer.specClass.equalsIgnoreCase("woodsman")) || (specEntityPlayer.specClass.equalsIgnoreCase("woodsman") && specEntityPlayer.classlevel < 2)) && finalStack != null)
    			{
    				if ((finalStack.getItem().equals(SpecializationsMod.AXEHARDWOOD) || finalStack.getItem().equals(SpecializationsMod.PICKAXEHARDWOOD)
    						|| finalStack.getItem().equals(SpecializationsMod.SHOVELHARDWOOD) || finalStack.getItem().equals(SpecializationsMod.HOEHARDWOOD) 
    						|| finalStack.getItem().equals(SpecializationsMod.SWORDHARDWOOD)))
    				{
    					finalStack = null;
    				}
    			}

    			if ((!(specEntityPlayer.specClass.equalsIgnoreCase("spellbinder")) || (specEntityPlayer.specClass.equalsIgnoreCase("spellbinder") && specEntityPlayer.classlevel < 2)) && finalStack != null)
    			{
    				if ((finalStack.getItem().equals(SpecializationsMod.ARROW) && finalStack.getItemDamage() == 3))
    				{
    					finalStack = null;
    				}
    			}

    			if ((!(specEntityPlayer.specClass.equalsIgnoreCase("blacksmith")) || (specEntityPlayer.specClass.equalsIgnoreCase("blacksmith") && specEntityPlayer.classlevel < 2)) && finalStack != null)
    			{
    				if (((finalStack.getItem().equals(SpecializationsMod.ARROW) && finalStack.getItemDamage() == 1)
    						|| (finalStack.getItem().equals(SpecializationsMod.ARROW) && finalStack.getItemDamage() == 2)
    						|| (finalStack.getItem().equals(SpecializationsMod.ARROW) && finalStack.getItemDamage() == 4) 
    						|| finalStack.getItem().equals(SpecializationsMod.STONE_SWORD)
    						|| finalStack.getItem().equals(SpecializationsMod.STONE_PICKAXE) 
    						|| finalStack.getItem().equals(SpecializationsMod.STONE_SHOVEL) 
    						|| finalStack.getItem().equals(SpecializationsMod.STONE_HOE)
    						|| finalStack.getItem().equals(SpecializationsMod.STONE_AXE) 
    						|| finalStack.getItem().equals(SpecializationsMod.IRON_SWORD) 
    						|| finalStack.getItem().equals(SpecializationsMod.IRON_HOE)
    						|| finalStack.getItem().equals(SpecializationsMod.IRON_AXE) 
    						|| finalStack.getItem().equals(SpecializationsMod.IRON_PICKAXE) 
    						|| finalStack.getItem().equals(SpecializationsMod.IRON_SHOVEL)
    						|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_SWORD) 
    						|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_HOE) 
    						|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_PICKAXE)
    						|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_SHOVEL) 
    						|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_AXE) 
    						|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_SWORD)
    						|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_HOE) 
    						|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_PICKAXE) 
    						|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_SHOVEL)
    						|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_AXE) 
    						|| finalStack.getItem().equals(SpecializationsMod.IRON_BOOTS) 
    						|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_BOOTS)
    						|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_BOOTS) 
    						|| finalStack.getItem().equals(SpecializationsMod.IRON_CHESTPLATE) 
    						|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_CHESTPLATE)
    						|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_CHESTPLATE) 
    						|| finalStack.getItem().equals(SpecializationsMod.IRON_HELMET) 
    						|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_HELMET)
    						|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_HELMET) 
    						|| finalStack.getItem().equals(SpecializationsMod.IRON_LEGGINGS) 
    						|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_LEGGINGS)
    						|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_LEGGINGS) 
    						|| finalStack.getItem().equals(SpecializationsMod.SWORDOBSIDIAN)
    						|| finalStack.getItem().equals(SpecializationsMod.HOEOBSIDIAN) 
    						|| finalStack.getItem().equals(SpecializationsMod.PICKAXEOBSIDIAN)
    						|| finalStack.getItem().equals(SpecializationsMod.SHOVELOBSIDIAN) 
    						|| finalStack.getItem().equals(SpecializationsMod.AXEOBSIDIAN)
    						|| finalStack.getItem().equals(SpecializationsMod.BOOTSOBSIDIAN) 
    						|| finalStack.getItem().equals(SpecializationsMod.PLATEOBSIDIAN)
    						|| finalStack.getItem().equals(SpecializationsMod.HELMETOBSIDIAN) 
    						|| finalStack.getItem().equals(SpecializationsMod.LEGSOBSIDIAN)))
    				{
    					finalStack = null;
    				}
    			}
    			else if (finalStack != null)
    			{
    				if (specEntityPlayer.classlevel < 4)
    				{
    					if (((finalStack.getItem().equals(SpecializationsMod.ARROW) && finalStack.getItemDamage() == 2) || (finalStack.getItem().equals(SpecializationsMod.ARROW) && finalStack.getItemDamage() == 4)
    							|| finalStack.getItem().equals(SpecializationsMod.IRON_SWORD) || finalStack.getItem().equals(SpecializationsMod.IRON_HOE) || finalStack.getItem().equals(SpecializationsMod.IRON_AXE)
    							|| finalStack.getItem().equals(SpecializationsMod.IRON_PICKAXE) || finalStack.getItem().equals(SpecializationsMod.IRON_SHOVEL) || finalStack.getItem().equals(SpecializationsMod.GOLDEN_SWORD)
    							|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_HOE) || finalStack.getItem().equals(SpecializationsMod.GOLDEN_PICKAXE) || finalStack.getItem().equals(SpecializationsMod.GOLDEN_SHOVEL)
    							|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_AXE) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_SWORD) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_HOE)
    							|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_PICKAXE) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_SHOVEL) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_AXE)
    							|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_BOOTS) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_BOOTS) || finalStack.getItem().equals(SpecializationsMod.IRON_CHESTPLATE)
    							|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_CHESTPLATE) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_CHESTPLATE) || finalStack.getItem().equals(SpecializationsMod.IRON_HELMET)
    							|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_HELMET) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_HELMET) || finalStack.getItem().equals(SpecializationsMod.IRON_LEGGINGS)
    							|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_LEGGINGS) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_LEGGINGS)
    							|| finalStack.getItem().equals(SpecializationsMod.SWORDOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.HOEOBSIDIAN)
    							|| finalStack.getItem().equals(SpecializationsMod.PICKAXEOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.SHOVELOBSIDIAN)
    							|| finalStack.getItem().equals(SpecializationsMod.AXEOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.BOOTSOBSIDIAN)
    							|| finalStack.getItem().equals(SpecializationsMod.PLATEOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.HELMETOBSIDIAN) || finalStack.getItem().equals(
    							SpecializationsMod.LEGSOBSIDIAN)))
    					{
    						finalStack = null;
    					}
    				}

    				else if (specEntityPlayer.classlevel < 8)
    				{
    					if (((finalStack.getItem().equals(SpecializationsMod.ARROW) && finalStack.getItemDamage() == 4)) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_SWORD)
    							|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_HOE) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_PICKAXE) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_SHOVEL)
    							|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_AXE) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_BOOTS) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_CHESTPLATE)
    							|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_HELMET) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_LEGGINGS)
    							|| finalStack.getItem().equals(SpecializationsMod.SWORDOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.HOEOBSIDIAN)
    							|| finalStack.getItem().equals(SpecializationsMod.PICKAXEOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.SHOVELOBSIDIAN)
    							|| finalStack.getItem().equals(SpecializationsMod.AXEOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.BOOTSOBSIDIAN)
    							|| finalStack.getItem().equals(SpecializationsMod.PLATEOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.HELMETOBSIDIAN)
    							|| finalStack.getItem().equals(SpecializationsMod.LEGSOBSIDIAN))
    					{
    						finalStack = null;
    					}
    				}

    				else if (specEntityPlayer.classlevel < 10)
    				{
    					if ((finalStack.getItem().equals(SpecializationsMod.SWORDOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.HOEOBSIDIAN)
    							|| finalStack.getItem().equals(SpecializationsMod.PICKAXEOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.SHOVELOBSIDIAN)
    							|| finalStack.getItem().equals(SpecializationsMod.AXEOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.BOOTSOBSIDIAN) || finalStack.getItem().equals(
    							SpecializationsMod.PLATEOBSIDIAN))
    							|| finalStack.getItem().equals(SpecializationsMod.HELMETOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.LEGSOBSIDIAN))
    					{
    						finalStack = null;
    					}
    				}
    			}
    		}
    	}
    	//System.out.println("stack: " + finalStack);
		return finalStack;
	}
    
    public boolean shouldDamage(ItemStack crafted)
    {
    	if (crafted.getItem() instanceof ItemTool)
		{
    		return true;
		}
		if (crafted.getItem() instanceof ItemHoe)
		{
			return true;
		}
		else if (crafted.getItem() instanceof ItemSword)
		{
			return true;
		}
		else if (crafted.getItem() instanceof ItemArmor)
		{
			return true;
		}
		else if(crafted.getItem() instanceof ItemBow)
		{
			return true;
		}
		return false;
    }
}