package bitevo.Zetal.LoMaS.Specializations;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.common.collect.Maps;

import bitevo.Zetal.LoMaS.Specializations.Block.BlockCactus;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockReed;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockStem;
import bitevo.Zetal.LoMaS.Specializations.Entities.EntityCustomArrow;
import bitevo.Zetal.LoMaS.Specializations.Entities.EntityCustomXPOrb;
import bitevo.Zetal.LoMaS.Specializations.Handler.AbilityKeyBind;
import bitevo.Zetal.LoMaS.Specializations.Handler.ChooseClassGUIKeyBind;
import bitevo.Zetal.LoMaS.Specializations.Handler.SpecInfoGUIKeyBind;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemFishFood;
import bitevo.Zetal.LoMaS.Specializations.Renderer.BlockSapRenderer;
import bitevo.Zetal.LoMaS.Specializations.Renderer.RenderCustomArrow;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySapBlock;
import net.minecraft.block.Block;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockModelShapes;
import net.minecraft.client.renderer.ItemMeshDefinition;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.StateMap;
import net.minecraft.client.renderer.block.statemap.StateMapperBase;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderXPOrb;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class SCMClientProxy extends SCMCommonProxy
{
	public static Minecraft mc;
	public static String modid;
	public static ResourceLocation[] bowTextures = new ResourceLocation[15];
	public static ResourceLocation[] arrowTextures = new ResourceLocation[5];

	public class ArrowRendererFactory implements IRenderFactory<EntityCustomArrow>
	{
		@Override
		public Render<? super EntityCustomArrow> createRenderFor(RenderManager manager)
		{
			return new RenderCustomArrow(manager);
		}
	}

	public class XPRendererFactory implements IRenderFactory<EntityCustomXPOrb>
	{
		@Override
		public Render<? super EntityCustomXPOrb> createRenderFor(RenderManager manager)
		{
			return new RenderXPOrb(manager);
		}
	}
	
	@Override
	public void registerRenderInformation()
	{
		mc = Minecraft.getMinecraft();
		modid = SpecializationsMod.modid;

		//mc.effectRenderer.registerParticle(77, new EntityCustomFX.Factory());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntitySapBlock.class, new BlockSapRenderer());
		RenderingRegistry.registerEntityRenderingHandler(EntityCustomArrow.class, new ArrowRendererFactory());
		RenderingRegistry.registerEntityRenderingHandler(EntityCustomXPOrb.class, new XPRendererFactory());


//
//		final String modid = SpecializationsMod.modid;
//
//		itemRenderer.getItemModelMesher().register(SpecializationsMod.BREWING_STAND_ITEM, 0, new ModelResourceLocation(modid + ":brewing_stand", "inventory"));
//

		init();
	}
	
	@Override
	public void regiterBlockStateMappers()
	{
		BlockModelShapes shapes = Minecraft.getMinecraft().getBlockRendererDispatcher().getBlockModelShapes();
		shapes.registerBlockWithStateMapper(FoodInitializer.PUMPKIN_STEM, new StateMapperBase()
		{
			protected ModelResourceLocation getModelResourceLocation(IBlockState p_178132_1_)
			{
				LinkedHashMap linkedhashmap = Maps.newLinkedHashMap(p_178132_1_.getProperties());

				if (p_178132_1_.getValue(BlockStem.FACING) != EnumFacing.UP)
				{
					linkedhashmap.remove(BlockStem.AGE);
				}

				return new ModelResourceLocation((ResourceLocation) Block.REGISTRY.getNameForObject(p_178132_1_.getBlock()), this.getPropertyString(linkedhashmap));
			}
		});
		shapes.registerBlockWithStateMapper(FoodInitializer.MELON_STEM, new StateMapperBase()
		{
			protected ModelResourceLocation getModelResourceLocation(IBlockState p_178132_1_)
			{
				LinkedHashMap linkedhashmap = Maps.newLinkedHashMap(p_178132_1_.getProperties());

				if (p_178132_1_.getValue(BlockStem.FACING) != EnumFacing.UP)
				{
					linkedhashmap.remove(BlockStem.AGE);
				}

				return new ModelResourceLocation((ResourceLocation) Block.REGISTRY.getNameForObject(p_178132_1_.getBlock()), this.getPropertyString(linkedhashmap));
			}
		});
		shapes.registerBlockWithStateMapper(FoodInitializer.REEDS_BLOCK, (new StateMap.Builder()).ignore(new IProperty[] { BlockReed.AGE }).build());
		shapes.registerBlockWithStateMapper(FoodInitializer.CACTUS, (new StateMap.Builder()).ignore(new IProperty[] { BlockCactus.AGE }).build());

		RenderItem itemRenderer = mc.getRenderItem();
		//itemRenderer.getItemModelMesher().register(FoodInitializer.FISH, this.fish);
		//itemRenderer.getItemModelMesher().register(FoodInitializer.COOKED_FISH, this.cooked_fish);
	}

	@Override
	public void init()
	{
		SpecializationsMod.clientStart();
		FMLCommonHandler.instance().bus().register(new AbilityKeyBind());
		FMLCommonHandler.instance().bus().register(new ChooseClassGUIKeyBind());
		FMLCommonHandler.instance().bus().register(new SpecInfoGUIKeyBind());
	}
}