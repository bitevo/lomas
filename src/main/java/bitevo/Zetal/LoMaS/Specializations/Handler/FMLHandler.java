package bitevo.Zetal.LoMaS.Specializations.Handler;

import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.util.Random;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.GUI.GuiIngameSpec;
import bitevo.Zetal.LoMaS.Specializations.Item.ItemArrow;
import io.netty.channel.local.LocalAddress;
import net.minecraft.block.Block;
import net.minecraft.block.BlockBeacon;
import net.minecraft.block.BlockCauldron;
import net.minecraft.block.BlockPistonBase;
import net.minecraft.block.BlockRailBase;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.util.FoodStats;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemFlintAndSteel;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemMinecart;
import net.minecraft.item.ItemShears;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.ItemCraftedEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerRespawnEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent.ClientConnectedToServerEvent;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class FMLHandler
{
	@SubscribeEvent
	public void playerConnected(PlayerLoggedInEvent event)
	{
		if (event.player.worldObj != null && !event.player.worldObj.isRemote)
		{
			EntityPlayer player = event.player;
			LoMaS_Player.addLoMaSPlayer(player);
			if (player != null && LoMaS_Player.getLoMaSPlayer(player) != null)
			{
				// this.setPlayerCurrentHealth(player);
				FMLHandler.setPlayerMaxHealth(player);
				// MoveSpeed
				if (player.capabilities != null)
				{
					SCMEventHandler.setPlayerWalkSpeed(player);
				}
				SpecializationsMod.sendPlayerAbilitiesToClients(LoMaS_Player.getLoMaSPlayer(player));
				LoMaS_Player.getLoMaSPlayer(player).notYet = true;
			}
		}
	}

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void clientPlayerConnected(ClientConnectedToServerEvent event)
	{
		Minecraft.getMinecraft().ingameGUI = new GuiIngameSpec(Minecraft.getMinecraft());
	}

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void playerClientRespawn(PlayerRespawnEvent event)
	{
		Minecraft.getMinecraft().ingameGUI = new GuiIngameSpec(Minecraft.getMinecraft());

		LoMaS_Player.addLoMaSPlayer(event.player);
		FMLHandler.setPlayerMaxHealth(event.player);
		event.player.getFoodStats().setFoodLevel(5);
		// this.setPlayerCurrentHealth(event.player);
	}

	@SubscribeEvent
	@SideOnly(Side.SERVER)
	public void playerServerRespawn(PlayerRespawnEvent event)
	{
		EntityPlayer player = event.player;
		player.getFoodStats().setFoodLevel(5);
	}

	@SubscribeEvent
	public void onCrafted(ItemCraftedEvent event)
	{
		ItemStack crafted = event.crafting;
		EntityPlayer player = event.player;
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
		if ((crafted.getItem().equals(SpecializationsMod.IRONHEART) || crafted.getItem().equals(SpecializationsMod.EMERALDHEART)) && !player.worldObj.isRemote)
		{
			ItemStack glassStack = new ItemStack(Items.GLASS_BOTTLE, 4, 0);
			if (!player.inventory.addItemStackToInventory(glassStack))
			{
				player.worldObj.spawnEntityInWorld(new EntityItem(player.worldObj, (double) player.posX + 0.5D, (double) player.posY + 1.5D, (double) player.posZ + 0.5D, glassStack));
			}
			else if (player instanceof EntityPlayerMP)
			{
				((EntityPlayerMP) player).sendContainerToPlayer(player.inventoryContainer);
			}
		}

		float progressEarned = this.getProgressForStack(crafted);
		float exhaustion = progressEarned > 0 ? this.getExhaustionForStack(crafted) : 0.0f;
		this.damageCraftedStack(crafted, player);

		player.addExhaustion(exhaustion);
		if (!player.capabilities.isCreativeMode && progressEarned > 0.00f)
		{
			lplayer.addSpecProgress(progressEarned);
			Random rand = new Random();
			player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
		}
	}
	
	public static boolean isTool(ItemStack crafted)
	{
		if (crafted.getItem() instanceof ItemTool)
		{
			return true;
		}
		if (crafted.getItem() instanceof ItemHoe)
		{
			return true;
		}
		else if (crafted.getItem() instanceof ItemSword)
		{
			return true;
		}
		else if (crafted.getItem().equals(SpecializationsMod.BOW))
		{
			return true;
		}
		else if(!(crafted.getItem() instanceof ItemArmor) && crafted.getItem().isItemTool(crafted))
		{
			return true;
		}
		return false;
	}

	public static void damageCraftedStack(ItemStack crafted, EntityPlayer player)
	{
		if (crafted.getItem() instanceof ItemTool)
		{
			if (!player.worldObj.isRemote)
			{
				SpecializationsMod.setDamagedItem(crafted, player, true);
			}
			player.inventory.inventoryChanged = true;
		}
		if (crafted.getItem() instanceof ItemHoe)
		{
			if (!player.worldObj.isRemote)
			{
				SpecializationsMod.setDamagedItem(crafted, player, true);
			}
			player.inventory.inventoryChanged = true;
		}
		else if (crafted.getItem() instanceof ItemSword)
		{
			if (!player.worldObj.isRemote)
			{
				SpecializationsMod.setDamagedItem(crafted, player, true);
			}
			player.inventory.inventoryChanged = true;
		}
		else if (crafted.getItem() instanceof ItemArmor)
		{
			if (!player.worldObj.isRemote)
			{
				SpecializationsMod.setDamagedItem(crafted, player, true);
			}
			player.inventory.inventoryChanged = true;
		}
		else if (crafted.getItem().equals(SpecializationsMod.BOW))
		{
			if (!player.worldObj.isRemote)
			{
				SpecializationsMod.setDamagedItem(crafted, player, true);
			}
			player.inventory.inventoryChanged = true;
		}
	}

	public static float getExhaustionForStack(ItemStack crafted)
	{
		float exhaustion = 0.2f;

		if (crafted.getItem() instanceof ItemTool)
		{
			exhaustion += 6.0f;
		}
		if (crafted.getItem() instanceof ItemHoe)
		{
			exhaustion += 6.0f;
		}
		else if (crafted.getItem() instanceof ItemSword)
		{
			exhaustion += 6.0f;
		}
		else if (crafted.getItem() instanceof ItemArmor)
		{
			exhaustion += 8.0f;
		}
		else if (crafted.getItem() instanceof ItemArrow)
		{
			exhaustion += 2.0f;
		}
		else if (crafted.getItem().equals(SpecializationsMod.BOW))
		{
			exhaustion += 6.0f;
		}
		else if (crafted.getItem() instanceof ItemMinecart)
		{
			exhaustion += 4.0f;
		}
		else if (crafted.getItem() instanceof ItemBlock)
		{
			exhaustion += 0.8f;
		}
		return exhaustion;
	}

	public static float getProgressForStack(ItemStack crafted)
	{
		float progressEarned = 0.0f;
		if (crafted.getItem() instanceof ItemTool)
		{
			ItemTool tool = (ItemTool) crafted.getItem();
			if (tool.getToolMaterialName().equals(ToolMaterial.WOOD.toString()))
			{
				progressEarned = 5.0f;
			}
			else if (tool.getToolMaterialName().equals(SpecializationsMod.HARDWOOD_TOOL.toString()))
			{
				progressEarned = 5.0f;
			}
			else if (tool.getToolMaterialName().equals(ToolMaterial.STONE.toString()))
			{
				progressEarned = 7.5f;
			}
			else if (tool.getToolMaterialName().equals(ToolMaterial.IRON.toString()))
			{
				progressEarned = 11.75f;
			}
			else if (tool.getToolMaterialName().equals(ToolMaterial.GOLD.toString()))
			{
				progressEarned = 15.5f;
			}
			else if (tool.getToolMaterialName().equals(ToolMaterial.DIAMOND.toString()))
			{
				progressEarned = 85.0f;
			}
			else if (tool.getToolMaterialName().equals(SpecializationsMod.OBSIDIAN_TOOL.toString()))
			{
				progressEarned = 50.0f;
			}

			if (!(tool instanceof ItemSpade))
			{
				progressEarned = progressEarned * 3.0f;
			}
		}
		else if (crafted.getItem() instanceof ItemHoe)
		{
			ItemHoe hoe = (ItemHoe) crafted.getItem();
			if (hoe.getMaterialName().equals(ToolMaterial.WOOD.toString()))
			{
				progressEarned = 10.0f;
			}
			else if (hoe.getMaterialName().equals(SpecializationsMod.HARDWOOD_TOOL.toString()))
			{
				progressEarned = 10.0f;
			}
			else if (hoe.getMaterialName().equals(ToolMaterial.STONE.toString()))
			{
				progressEarned = 15.0f;
			}
			else if (hoe.getMaterialName().equals(ToolMaterial.IRON.toString()))
			{
				progressEarned = 22.5f;
			}
			else if (hoe.getMaterialName().equals(ToolMaterial.GOLD.toString()))
			{
				progressEarned = 25.5f;
			}
			else if (hoe.getMaterialName().equals(ToolMaterial.DIAMOND.toString()))
			{
				progressEarned = 170.0f;
			}
			else if (hoe.getMaterialName().equals(SpecializationsMod.OBSIDIAN_TOOL.toString()))
			{
				progressEarned = 100.0f;
			}
		}
		else if (crafted.getItem() instanceof ItemSword)
		{
			ItemSword sword = (ItemSword) crafted.getItem();
			if (sword.getToolMaterialName().equals(ToolMaterial.WOOD.toString()))
			{
				progressEarned = 10.0f;
			}
			else if (sword.getToolMaterialName().equals(SpecializationsMod.HARDWOOD_TOOL.toString()))
			{
				progressEarned = 10.0f;
			}
			else if (sword.getToolMaterialName().equals(ToolMaterial.STONE.toString()))
			{
				progressEarned = 15.0f;
			}
			else if (sword.getToolMaterialName().equals(ToolMaterial.IRON.toString()))
			{
				progressEarned = 22.5f;
			}
			else if (sword.getToolMaterialName().equals(ToolMaterial.GOLD.toString()))
			{
				progressEarned = 25.5f;
			}
			else if (sword.getToolMaterialName().equals(ToolMaterial.DIAMOND.toString()))
			{
				progressEarned = 170.0f;
			}
			else if (sword.getToolMaterialName().equals(SpecializationsMod.OBSIDIAN_TOOL.toString()))
			{
				progressEarned = 100.0f;
			}
		}
		else if (crafted.getItem() instanceof ItemArmor)
		{
			ItemArmor armor = (ItemArmor) crafted.getItem();
			if (armor.getArmorMaterial() == ArmorMaterial.LEATHER)
			{
				progressEarned = 75.0f;
			}
			else if (armor.getArmorMaterial() == ArmorMaterial.IRON)
			{
				progressEarned = 125.0f;
			}
			else if (armor.getArmorMaterial() == ArmorMaterial.GOLD)
			{
				progressEarned = 150.0f;
			}
			else if (armor.getArmorMaterial() == ArmorMaterial.DIAMOND)
			{
				progressEarned = 350.0f;
			}
			else if (armor.getArmorMaterial() == SpecializationsMod.OBSIDIAN_ARMOR)
			{
				progressEarned = 250.0f;
			}
		}
		else if (crafted.getItem() instanceof ItemArrow)
		{
			if (crafted.getItemDamage() == 0)
			{
				progressEarned = 5.0f;
			}
			else if (crafted.getItemDamage() == 1)
			{
				progressEarned = 15.0f;
			}
			else if (crafted.getItemDamage() == 2)
			{
				progressEarned = 22.5f;
			}
			else if (crafted.getItemDamage() == 3)
			{
				progressEarned = 25.5f;
			}
			else if (crafted.getItemDamage() == 4)
			{
				progressEarned = 80.0f;
			}
		}
		else if (crafted.getItem().equals(SpecializationsMod.BOW))
		{
			progressEarned = 10.0f;
		}
		else if (crafted.getItem() instanceof ItemMinecart)
		{
			progressEarned = 50.0f;
		}
		else if (crafted.getItem() instanceof ItemShears)
		{
			progressEarned = 22.5f;
		}
		else if (crafted.getItem() instanceof ItemFlintAndSteel)
		{
			progressEarned = 11.25f;
		}
		else if (crafted.getItem() instanceof ItemBlock)
		{
			Block block = Block.getBlockFromItem(crafted.getItem());
			if (block instanceof BlockRailBase)
			{
				progressEarned = 70.0f;
			}
			else if (block instanceof BlockBeacon)
			{
				progressEarned = 250.0f;
			}
			else if (block instanceof BlockCauldron)
			{
				progressEarned = 82.25f;
			}
			else if (block instanceof BlockPistonBase)
			{
				progressEarned = 20.0f;
			}
			else
			{
				progressEarned = 0.0f;
			}
		}
		else
		{
			progressEarned = 0.0f;
		}
		return progressEarned;
	}

	public static void setPlayerCurrentHealth(EntityPlayer player)
	{
		double health = getMaxHealth(player);

		setPlayerMaxHealth(player);
		player.setHealth((float) health);
	}

	public static void setPlayerMaxHealth(EntityPlayer player)
	{
		double health = getMaxHealth(player);
		
		IAttributeInstance maxHpAttr = player.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH);
		double oldMax = maxHpAttr.getBaseValue();
		maxHpAttr.setBaseValue(health);

		if (player.getHealth() > health)
		{
			player.setHealth((float) health);
		}
		
		if(health > oldMax)
		{
			double diff = health - oldMax;
			player.heal((float) diff);
		}
	}

	public static double getMaxHealth(EntityPlayer player)
	{
		float health = 16;
		LoMaS_Player lPlayer = LoMaS_Player.getLoMaSPlayer(player);

		health += ((lPlayer.getSkills().getMagnitudeFromName("Health") * 2));
		
		return health;
	}
}
