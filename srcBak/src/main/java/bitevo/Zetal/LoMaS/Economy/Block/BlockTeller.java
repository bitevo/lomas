package bitevo.Zetal.LoMaS.Economy.Block;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.Economy.EconomyMod;
import net.minecraft.block.BlockPane;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockTeller extends BlockPane
{
	/**
	 * If this field is true, the pane block drops itself when destroyed (like the iron fences), otherwise, it's just destroyed (like glass panes)
	 */
	private final boolean canDropItself;

	public BlockTeller(Material par4Material)
	{
		super(par4Material, false);
		this.canDropItself = false;
		this.setHardness(0.3F);
		this.setSoundType(SoundType.GLASS);
		this.setHardness(5.0F);
		this.setResistance(10.0F);
	}

	@Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, @Nullable ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		if (worldIn.isRemote)
		{
			return true;
		}
		else
		{
			playerIn.openGui(EconomyMod.instance, 1, worldIn, pos.getX(), pos.getY(), pos.getZ());
			return true;
		}
	}
}
