package bitevo.Zetal.LoMaS.Specializations.Block;

import java.util.Iterator;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntityPlant;

public class BlockCactus extends BlockContainer implements net.minecraftforge.common.IPlantable
{
	public static final PropertyInteger AGE = PropertyInteger.create("age", 0, 15);
	private static final String __OBFID = "CL_00000210";

	public BlockCactus()
	{
		super(Material.CACTUS);
		this.setDefaultState(this.blockState.getBaseState().withProperty(AGE, Integer.valueOf(0)));
		this.setTickRandomly(true);
		this.setCreativeTab(CreativeTabs.DECORATIONS);
	}

	public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random rand)
	{
		BlockPos blockpos1 = pos.up();

		if ((worldIn.getLightFor(EnumSkyBlock.SKY, pos) > 13)
				&& worldIn.getLightFromNeighbors(pos.up()) >= 9
				&& worldIn.isDaytime())
		{
			if (worldIn.isAirBlock(blockpos1))
			{
				int i;

				for (i = 1; worldIn.getBlockState(pos.down(i)).getBlock() == this; ++i)
				{
					;
				}

				TileEntityPlant plant = null;
				boolean hasTileEntity = false;
				if (worldIn.getTileEntity(pos) != null && worldIn.getTileEntity(pos) instanceof TileEntityPlant)
				{
					hasTileEntity = true;
					plant = (TileEntityPlant) worldIn.getTileEntity(pos);
				}

				if (i < 3)
				{
					int j = ((Integer) state.getValue(AGE)).intValue();

					int timer = 15;
					if (hasTileEntity)
					{
						if (plant.getFarmerLevel() >= 4)
						{
							timer--;
						}
						if (plant.getFarmerLevel() >= 5)
						{
							timer--;
						}
						if (plant.getFarmerLevel() >= 6)
						{
							timer--;
						}
						if (plant.getFarmerLevel() >= 7)
						{
							timer--;
						}
					}

					if (j == timer)
					{
						worldIn.setBlockState(blockpos1, this.getDefaultState());
						IBlockState iblockstate1 = state.withProperty(AGE, Integer.valueOf(0));
						worldIn.setBlockState(pos, iblockstate1, 4);
						this.onNeighborBlockChange(worldIn, blockpos1, iblockstate1, this);
						if (worldIn.getTileEntity(pos.up()) != null && worldIn.getTileEntity(pos.up()) instanceof TileEntityPlant
								&& worldIn.getTileEntity(pos) != null && worldIn.getTileEntity(pos) instanceof TileEntityPlant)
						{
							TileEntityPlant upperPlant = (TileEntityPlant) worldIn.getTileEntity(pos.up());
							TileEntityPlant lowerPlant = (TileEntityPlant) worldIn.getTileEntity(pos);
							upperPlant.setFarmerLevel(lowerPlant.getFarmerLevel());
							upperPlant.setOwner(lowerPlant.getOwner());
						}
					}
					else
					{
						worldIn.setBlockState(pos, state.withProperty(AGE, Integer.valueOf(j + 1)), 4);
					}
				}
			}
		}
	}

	public AxisAlignedBB getCollisionBoundingBox(World worldIn, BlockPos pos, IBlockState state)
	{
		float f = 0.0625F;
		return new AxisAlignedBB((double) ((float) pos.getX() + f), (double) pos.getY(), (double) ((float) pos.getZ() + f), (double) ((float) (pos.getX() + 1) - f), (double) ((float) (pos.getY() + 1) - f), (double) ((float) (pos.getZ() + 1) - f));
	}

	@SideOnly(Side.CLIENT)
	public AxisAlignedBB getSelectedBoundingBox(World worldIn, BlockPos pos)
	{
		float f = 0.0625F;
		return new AxisAlignedBB((double) ((float) pos.getX() + f), (double) pos.getY(), (double) ((float) pos.getZ() + f), (double) ((float) (pos.getX() + 1) - f), (double) (pos.getY() + 1), (double) ((float) (pos.getZ() + 1) - f));
	}

	public boolean isFullCube()
	{
		return false;
	}

	public boolean isOpaqueCube()
	{
		return false;
	}

	public boolean canPlaceBlockAt(World worldIn, BlockPos pos)
	{
		return super.canPlaceBlockAt(worldIn, pos) ? this.canBlockStay(worldIn, pos) : false;
	}
	
    @Override
    public IBlockState onBlockPlaced(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
		if(world.getTileEntity(pos) != null && world.getTileEntity(pos) instanceof TileEntityPlant && placer instanceof EntityPlayer)
		{
			TileEntityPlant tile = (TileEntityPlant) world.getTileEntity(pos.up());
			tile.setOwner(placer.getPersistentID());
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer((EntityPlayer) placer);
			if (lplayer != null && lplayer.specClass.equalsIgnoreCase("farmer"))
			{
				tile.setFarmerLevel((byte) LoMaS_Player.getLoMaSPlayer((EntityPlayer) placer).classlevel);
			}
		}
        return this.getStateFromMeta(meta);
    }

	/**
	 * Called when a neighboring block changes.
	 */
	public void onNeighborBlockChange(World worldIn, BlockPos pos, IBlockState state, Block neighborBlock)
	{
		if (!this.canBlockStay(worldIn, pos))
		{
			worldIn.destroyBlock(pos, true);
		}
	}

	public Item getItemDropped(IBlockState state, Random rand, int fortune)
	{
		return Blocks.CACTUS.getItemDropped(Blocks.CACTUS.getDefaultState(), rand, fortune);
	}

	@SideOnly(Side.CLIENT)
	public Item getItem(World worldIn, BlockPos pos)
	{
		return Item.getItemFromBlock(Blocks.CACTUS);
	}

	public boolean canBlockStay(World worldIn, BlockPos pos)
	{
		Iterator iterator = EnumFacing.Plane.HORIZONTAL.iterator();

		while (iterator.hasNext())
		{
			EnumFacing enumfacing = (EnumFacing) iterator.next();

			if (worldIn.getBlockState(pos.offset(enumfacing)).getMaterial().isSolid())
			{
				return false;
			}
		}

		Block block = worldIn.getBlockState(pos.down()).getBlock();
		return SpecializationsMod.canSustainPlant(worldIn, pos.down(), EnumFacing.UP, this);
	}

	public int getRenderType()
	{
		return 3;
	}

	/**
	 * Called When an Entity Collided with the Block
	 */
	public void onEntityCollidedWithBlock(World worldIn, BlockPos pos, IBlockState state, Entity entityIn)
	{
		entityIn.attackEntityFrom(DamageSource.cactus, 1.0F);
	}

	/**
	 * Convert the given metadata into a BlockState for this Block
	 */
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty(AGE, Integer.valueOf(meta));
	}

    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }

	/**
	 * Convert the BlockState into the correct metadata value
	 */
	public int getMetaFromState(IBlockState state)
	{
		return ((Integer) state.getValue(AGE)).intValue();
	}

	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, new IProperty[] { AGE });
	}

	@Override
	public net.minecraftforge.common.EnumPlantType getPlantType(net.minecraft.world.IBlockAccess world, BlockPos pos)
	{
		return net.minecraftforge.common.EnumPlantType.Desert;
	}

	@Override
	public IBlockState getPlant(net.minecraft.world.IBlockAccess world, BlockPos pos)
	{
		return getDefaultState();
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileEntityPlant();
	}
}