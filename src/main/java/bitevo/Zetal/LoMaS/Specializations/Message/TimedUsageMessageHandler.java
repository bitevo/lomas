package bitevo.Zetal.LoMaS.Specializations.Message;

import java.util.UUID;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Item.ItemFertilizer;
import bitevo.Zetal.LoMaS.Specializations.Item.ItemSeedFood;
import bitevo.Zetal.LoMaS.Specializations.Item.ItemSeeds;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntityPlant;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TimedUsageMessageHandler implements IMessageHandler<TimedUsageMessage, IMessage>
{
	@Override
	public IMessage onMessage(TimedUsageMessage message, MessageContext ctx)
	{
		if (ctx.side == Side.CLIENT)
		{
			handleClientTimedUsageMessage(message);
		}
		else
		{
			handleServerTimedUsageMessage(message);
		}
		return null;
	}

	@SideOnly(Side.CLIENT)
	private void handleClientTimedUsageMessage(TimedUsageMessage packet)
	{
		
	}

	private void handleServerTimedUsageMessage(TimedUsageMessage packet)
	{
		EntityPlayer player = (EntityPlayer) SpecializationsMod.server.getEntityFromUuid(packet.username);
		BlockPos pos = packet.pos;
		ItemStack stack = player.inventory.getStackInSlot(packet.stack);
		World world = player.worldObj;
		int facingIndex = packet.facing;
		if (stack.getItem() instanceof ItemFertilizer && ItemFertilizer.canBeFertilized(world, pos))
		{
			TileEntityPlant tep = (TileEntityPlant) world.getTileEntity(pos);
			tep.setFertilized(true);
			int qualityBonus = 35 + (world.rand.nextInt(5) - 5);
			if (!player.capabilities.isCreativeMode)
			{
				stack.stackSize--;
				LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer((EntityPlayer) player);
				if (lplayer != null)
				{
					qualityBonus += (lplayer.getSkills().getMagnitudeFromName("Fertilizer quality"));
					lplayer.addSpecProgress(25.0f);
				}
			}
			tep.setQuality(tep.getQuality() + qualityBonus);
			tep.markDirty();
			world.notifyBlockUpdate(pos, world.getBlockState(pos), world.getBlockState(pos), 3);
		}
		else if ((stack.getItem() instanceof ItemSeeds || stack.getItem() instanceof ItemSeedFood) && SpecializationsMod.canSustainPlant(world, pos.down(), EnumFacing.UP, (IPlantable) stack.getItem()))
		{
			world.setBlockState(pos, ((IPlantable) stack.getItem()).getPlant(world, pos));
			world.notifyBlockUpdate(pos, world.getBlockState(pos), world.getBlockState(pos), 3);
			if (world.getTileEntity(pos) != null && world.getTileEntity(pos) instanceof TileEntityPlant && player instanceof EntityPlayer)
			{
				TileEntityPlant tile = (TileEntityPlant) world.getTileEntity(pos);
				tile.setOwner(player.getPersistentID());
			}
			if (!player.capabilities.isCreativeMode)
			{
				stack.stackSize--;
			}
		}
		else if (LoMaS_Utils.TIW && stack.getItem() instanceof bitevo.Zetal.LoMaS.Specializations.Item.ItemRunicBook)
		{
			if (player instanceof EntityPlayer)
			{
				bitevo.Zetal.LoMaS.Specializations.Item.ItemRunicBook runeBook = (bitevo.Zetal.LoMaS.Specializations.Item.ItemRunicBook) stack.getItem();
				int runeType = stack.getItemDamage();
            	try
				{
            		bitevo.Zetal.LoMaS.Specializations.TileWrapper.TileWrapperRune rune = runeBook.getWrapperClasses().get(runeType).getConstructor(String.class, World.class, BlockPos.class, EnumFacing.class, UUID.class).newInstance(null, Minecraft.getMinecraft().theWorld, pos, EnumFacing.VALUES[facingIndex], player.getPersistentID());
            		bitevo.Zetal.LoMaS.TileWrappers.TileWrapper.TileWrapperContainer.instance.setTileWrapper(pos, rune);
				}
				catch (Exception e)
				{
					System.err.println("Error receiving a TileWrapper message! Could not create a TileWrapper using class: " + runeBook.getWrapperClasses().get(runeType));
					e.printStackTrace();
				}
			}
			if (!player.capabilities.isCreativeMode)
			{
				stack.stackSize--;
			}
			world.notifyBlockUpdate(pos, world.getBlockState(pos), world.getBlockState(pos), 3);
		}
		else
		{
			player.stopActiveHand();
		}
	}
}
