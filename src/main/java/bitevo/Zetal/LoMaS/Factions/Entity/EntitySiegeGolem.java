package bitevo.Zetal.LoMaS.Factions.Entity;

import java.util.UUID;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.Shared.Entity.EntityFactionMember;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.World;

public class EntitySiegeGolem extends EntityFactionMember
{

	public EntitySiegeGolem(World worldIn)
	{
		super(worldIn);
		this.setSize(1.4F, 2.2F);
	}

	public EntitySiegeGolem(World worldIn, UUID faction)
	{
		super(worldIn, faction);
		this.setSize(1.4F, 2.2F);
	}

	@Override
	protected void entityInit()
	{
		super.entityInit();
	}

	@Override
	protected void applyEntityAttributes()
	{
		super.applyEntityAttributes();
		this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(100.0D);
		this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.25D);
		this.getEntityAttribute(SharedMonsterAttributes.KNOCKBACK_RESISTANCE).setBaseValue(1.0D);
	}

	@Override
	public void fall(float distance, float damageMultiplier)
	{
	}

	@Override
	@Nullable
	protected SoundEvent getAmbientSound()
	{
		return null;
	}

	@Override
	@Nullable
	protected SoundEvent getHurtSound()
	{
		return null;
	}

	@Override
	@Nullable
	protected SoundEvent getDeathSound()
	{
		return null;
	}

	/**
	 * Get number of ticks, at least during which the living entity will be silent.
	 */
	@Override
	public int getTalkInterval()
	{
		return 120;
	}

	/**
	 * Determines if an entity can be despawned, used on idle far away entities
	 */
	@Override
	protected boolean canDespawn()
	{
		return false;
	}
}
