package bitevo.Zetal.LoMaS.BigFMod;

import java.util.Iterator;
import java.util.List;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import bitevo.Zetal.LoMaS.BigFMod.Block.BlockBookstand;
import bitevo.Zetal.LoMaS.BigFMod.Block.BlockDoubleRoughStoneSlab;
import bitevo.Zetal.LoMaS.BigFMod.Block.BlockHalfRoughStoneSlab;
import bitevo.Zetal.LoMaS.BigFMod.Block.BlockLeavesDecayed;
import bitevo.Zetal.LoMaS.BigFMod.Block.BlockRoughStone;
import bitevo.Zetal.LoMaS.BigFMod.Block.BlockRoughStoneSlab;
import bitevo.Zetal.LoMaS.BigFMod.Block.BlockRoughStoneStairs;
import bitevo.Zetal.LoMaS.BigFMod.Entities.EntityBoat;
import bitevo.Zetal.LoMaS.BigFMod.Handler.BigFEventHandler;
import bitevo.Zetal.LoMaS.BigFMod.Handler.BigFFuelHandler;
import bitevo.Zetal.LoMaS.BigFMod.Items.ItemRoughSlab;
import bitevo.Zetal.LoMaS.BigFMod.Items.ItemStick;
import bitevo.Zetal.LoMaS.BigFMod.Message.BookstandMessage;
import bitevo.Zetal.LoMaS.BigFMod.Message.BookstandMessageHandler;
import bitevo.Zetal.LoMaS.BigFMod.TileEntity.TileEntityBookstand;
import bitevo.Zetal.LoMaS.BigFMod.TileEntity.TileEntityLeavesDecayed;
import bitevo.Zetal.LoMaS.Specializations.FoodInitializer;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockSapling;
import net.minecraft.block.Block;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.BlockStoneSlab;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.oredict.OreDictionary;

@Mod(modid = BigFMod.modid, name = "BigFMod", version = "0.1")
public class BigFMod
{
	public static final BlockLeavesDecayed LEAVESDECAYED = (BlockLeavesDecayed) (new BlockLeavesDecayed()).setHardness(0.0F).setCreativeTab(null).setUnlocalizedName("leavesDecayed");// .disableStats();
	public static final BlockBookstand BOOKSTAND = (BlockBookstand) (new BlockBookstand()).setHardness(1.2F).setCreativeTab(CreativeTabs.DECORATIONS).setUnlocalizedName("bookstand");
	public static final BlockRoughStone ROUGH_STONE = (BlockRoughStone) (new BlockRoughStone(Material.ROCK)).setUnlocalizedName("roughStone");
	public static final BlockRoughStoneStairs ROUGHSTAIRS = (BlockRoughStoneStairs) (new BlockRoughStoneStairs(ROUGH_STONE.getDefaultState())).setHardness(3.0F).setCreativeTab(CreativeTabs.BUILDING_BLOCKS).setUnlocalizedName("roughStairs");
	public static final BlockRoughStoneSlab ROUGH_SLAB = (BlockRoughStoneSlab) (new BlockHalfRoughStoneSlab()).setHardness(3.0F).setUnlocalizedName("rough_slab");
	public static final BlockRoughStoneSlab ROUGH_DOUBLE_SLAB = (BlockRoughStoneSlab) (new BlockDoubleRoughStoneSlab()).setHardness(3.0F).setUnlocalizedName("rough_double_slab");
	public static final ItemStick STICK = (ItemStick) (new ItemStick()).setFull3D().setUnlocalizedName("stick").setCreativeTab(CreativeTabs.MATERIALS);
	public static final ItemBlock ITEM_ROUGH_SLAB = new ItemRoughSlab(ROUGH_SLAB, ROUGH_SLAB, ROUGH_DOUBLE_SLAB).setUnlocalizedName("ROUGH_SLAB");

	public static final String modid = "lomas_bigf";
	public static SimpleNetworkWrapper snw;

	@SidedProxy(clientSide = "bitevo.Zetal.LoMaS.BigFMod.BigFClientProxy", serverSide = "bitevo.Zetal.LoMaS.BigFMod.BigFCommonProxy")
	public static BigFCommonProxy proxy = new BigFCommonProxy();

	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		MinecraftForge.EVENT_BUS.register(new BigFEventHandler());

		LoMaS_Utils.registerBlock(ROUGH_SLAB.setRegistryName( "rough_slab"), ITEM_ROUGH_SLAB.setRegistryName("rough_slab"));
		LoMaS_Utils.registerBlock(ROUGH_DOUBLE_SLAB.setRegistryName( "rough_double_slab"));
		LoMaS_Utils.registerBlock(LEAVESDECAYED.setRegistryName( "leavesDecayed"));
		LoMaS_Utils.registerBlock(BOOKSTAND.setRegistryName( "bookstand"));
		LoMaS_Utils.registerBlock(ROUGHSTAIRS.setRegistryName( "roughStairs"));
		LoMaS_Utils.registerItem(this.STICK.setRegistryName( "stick"), this.STICK.stickMetas, BlockSapling.TYPE);
		OreDictionary.registerOre("stickWood", new ItemStack(this.STICK, 1, OreDictionary.WILDCARD_VALUE));

		ItemRoughSlab.singleSlab = (BlockSlab) ROUGH_SLAB;
		ItemRoughSlab.doubleSlab = (BlockSlab) ROUGH_DOUBLE_SLAB;


		GameRegistry.registerTileEntity(TileEntityLeavesDecayed.class, "TileEntityLeavesDecayed");
		GameRegistry.registerTileEntity(TileEntityBookstand.class, "TileEntityBookstand");

		GameRegistry.registerFuelHandler(new BigFFuelHandler());

		EntityRegistry.registerModEntity(EntityBoat.class, "boat", LoMaS_Utils.getNextEntityID(), this, 80, 1, true);

		Items.STICK.setCreativeTab(null);


		List<IRecipe> recipes = CraftingManager.getInstance().getRecipeList();
		Iterator<IRecipe> iter = recipes.iterator();
		while (iter.hasNext())
		{
			ItemStack is = iter.next().getRecipeOutput();
			if (is != null && is.getItem() == Items.STICK)
			{
				iter.remove();
			}
		}

		registerStickRecipes();

		for (int k = 0; k <= 5; k++)
		{
			GameRegistry.addRecipe(new ItemStack(Blocks.PLANKS, 1, k), new Object[] { "##", "##", '#', new ItemStack(BigFMod.STICK, 1, k) });
		}
		GameRegistry.addRecipe(new ItemStack(this.BOOKSTAND, 1), new Object[] { " # ", "###", '#', Blocks.PLANKS });
		GameRegistry.addRecipe(new ItemStack(this.ROUGHSTAIRS, 1), new Object[] { "  &", " &#", "&##", '#', Blocks.STONE, '&', Blocks.COBBLESTONE });
		GameRegistry.addRecipe(new ItemStack(this.ROUGH_SLAB, 1), new Object[] { "#&#", '#', Blocks.STONE, '&', Blocks.COBBLESTONE });
	}

	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		snw = NetworkRegistry.INSTANCE.newSimpleChannel(modid);
		snw.registerMessage(BookstandMessageHandler.class, BookstandMessage.class, 7784, Side.CLIENT);
		snw.registerMessage(BookstandMessageHandler.class, BookstandMessage.class, 7784, Side.SERVER);
		proxy.registerRenderInformation();
	}

	/**
	 * This list of block ID exceptions are defined as "breakable" and should only be added to the list if they DO NOT have a 'proper tool' that is already defined.
	 **/
	public static boolean requiresProperTool(Block block)
	{
		if ((block != Blocks.DIRT) && (block != Blocks.GRASS) && (block != Blocks.SAND) && (block != Blocks.GRAVEL) && (block != Blocks.LEAVES) && (block != Blocks.LEAVES2) && (block != Blocks.WOOL)
		&& (block != FoodInitializer.HAY_BLOCK) && (block != Blocks.REDSTONE_LAMP) && (block != Blocks.LADDER) && (block != Blocks.LEVER) && (block != Blocks.PISTON) && (block != Blocks.STICKY_PISTON)
		&& (block != Blocks.STONE_BUTTON) && (block != Blocks.REDSTONE_BLOCK) && (block != Blocks.SNOW_LAYER) && (block != Blocks.VINE) && (block != Blocks.SNOW)
		&& (block != Blocks.BROWN_MUSHROOM_BLOCK) && (block != Blocks.RED_MUSHROOM_BLOCK) && (block != Blocks.GLASS) && (block != Blocks.GLASS_PANE) && (block != Blocks.FARMLAND)
		&& (block != FoodInitializer.FARMLAND) && (block != FoodInitializer.PUMPKIN_STEM) && (block != FoodInitializer.REEDS_BLOCK) && (block != FoodInitializer.MELON_STEM)
		&& (block != Blocks.CACTUS) && (block != FoodInitializer.CACTUS) && (block != FoodInitializer.PUMPKIN) && (block != Blocks.PUMPKIN_STEM) && (block != Blocks.GLOWSTONE)
		&& (block != SpecializationsMod.BREWING_STAND) && (block != FoodInitializer.MELON_BLOCK) && (block != Blocks.COCOA) && (block != Blocks.BEACON) && (block != Blocks.SKULL)
		&& (block != Blocks.MYCELIUM) && (block != Blocks.WEB) && (block != Blocks.REEDS) && (block != FoodInitializer.MELON_STEM) && (block != Blocks.WOODEN_BUTTON)
		&& (block != Blocks.DAYLIGHT_DETECTOR) && (block != Blocks.COMMAND_BLOCK) && (block != Blocks.BED) && (block != FoodInitializer.CAKE_BLOCK) && (block != Blocks.ICE)
		&& (block != SpecializationsMod.LIT_FURNACE) && /*(block != FoodInitializer.PUMPKINROTBLOCK) && (block != FoodInitializer.MELONROTBLOCK) &&*/ (block != Blocks.LIT_PUMPKIN)
		&& (block != Blocks.CARPET) && (block != Blocks.WALL_BANNER) && (block != Blocks.STANDING_BANNER) && (block != Blocks.FLOWER_POT) && (block != Blocks.HAY_BLOCK)
		&& (block != Blocks.SEA_LANTERN))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public static ItemStack getInventorySlotContainItem(EntityPlayer entityplayer, Item par1)
	{
		for (int j = 0; j < entityplayer.inventory.mainInventory.length; ++j)
		{
			if (entityplayer.inventory.mainInventory[j] != null && entityplayer.inventory.mainInventory[j].getItem() == par1)
			{
				return entityplayer.inventory.mainInventory[j];
			}
		}

		return null;
	}

	public static void registerStickRecipes()
	{
		// CraftingManager.java
		for (int k = 0; k <= 5; k++)
		{
			GameRegistry.addRecipe(new ItemStack(BigFMod.STICK, 4, k), new Object[] { "#", "#", '#', new ItemStack(Blocks.PLANKS, 1, k) });
		}

		GameRegistry.addRecipe(new ItemStack(Blocks.OAK_FENCE, 3), new Object[] { "W#W", "W#W", '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'W', new ItemStack(Blocks.PLANKS, 1, BlockPlanks.EnumType.OAK.getMetadata()) });
		GameRegistry.addRecipe(new ItemStack(Blocks.BIRCH_FENCE, 3), new Object[] { "W#W", "W#W", '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'W', new ItemStack(Blocks.PLANKS, 1, BlockPlanks.EnumType.BIRCH.getMetadata()) });
		GameRegistry.addRecipe(new ItemStack(Blocks.SPRUCE_FENCE, 3), new Object[] { "W#W", "W#W", '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'W', new ItemStack(Blocks.PLANKS, 1, BlockPlanks.EnumType.SPRUCE.getMetadata()) });
		GameRegistry.addRecipe(new ItemStack(Blocks.JUNGLE_FENCE, 3), new Object[] { "W#W", "W#W", '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'W', new ItemStack(Blocks.PLANKS, 1, BlockPlanks.EnumType.JUNGLE.getMetadata()) });
		GameRegistry.addRecipe(new ItemStack(Blocks.ACACIA_FENCE, 3), new Object[] { "W#W", "W#W", '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'W', new ItemStack(Blocks.PLANKS, 1, 4 + BlockPlanks.EnumType.ACACIA.getMetadata() - 4) });
		GameRegistry.addRecipe(new ItemStack(Blocks.DARK_OAK_FENCE, 3), new Object[] { "W#W", "W#W", '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'W', new ItemStack(Blocks.PLANKS, 1, 4 + BlockPlanks.EnumType.DARK_OAK.getMetadata() - 4) });
		GameRegistry.addRecipe(new ItemStack(Blocks.OAK_FENCE_GATE, 1), new Object[] { "#W#", "#W#", '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'W', new ItemStack(Blocks.PLANKS, 1, BlockPlanks.EnumType.OAK.getMetadata()) });
		GameRegistry.addRecipe(new ItemStack(Blocks.BIRCH_FENCE_GATE, 1), new Object[] { "#W#", "#W#", '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'W', new ItemStack(Blocks.PLANKS, 1, BlockPlanks.EnumType.BIRCH.getMetadata()) });
		GameRegistry.addRecipe(new ItemStack(Blocks.SPRUCE_FENCE_GATE, 1), new Object[] { "#W#", "#W#", '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'W', new ItemStack(Blocks.PLANKS, 1, BlockPlanks.EnumType.SPRUCE.getMetadata()) });
		GameRegistry.addRecipe(new ItemStack(Blocks.JUNGLE_FENCE_GATE, 1), new Object[] { "#W#", "#W#", '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'W', new ItemStack(Blocks.PLANKS, 1, BlockPlanks.EnumType.JUNGLE.getMetadata()) });
		GameRegistry.addRecipe(new ItemStack(Blocks.ACACIA_FENCE_GATE, 1), new Object[] { "#W#", "#W#", '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'W', new ItemStack(Blocks.PLANKS, 1, 4 + BlockPlanks.EnumType.ACACIA.getMetadata() - 4) });
		GameRegistry.addRecipe(new ItemStack(Blocks.DARK_OAK_FENCE_GATE, 1), new Object[] { "#W#", "#W#", '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'W', new ItemStack(Blocks.PLANKS, 1, 4 + BlockPlanks.EnumType.DARK_OAK.getMetadata() - 4) });
		GameRegistry.addRecipe(new ItemStack(Blocks.LADDER, 3), new Object[] { "# #", "###", "# #", '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE) });
		GameRegistry.addRecipe(new ItemStack(Items.SIGN, 3), new Object[] { "###", "###", " X ", '#', Blocks.PLANKS, 'X', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE) });

		GameRegistry.addRecipe(new ItemStack(Blocks.TORCH, 4), new Object[] { "X", "#", 'X', Items.COAL, '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE) });
		GameRegistry.addRecipe(new ItemStack(Blocks.TORCH, 4), new Object[] { "X", "#", 'X', new ItemStack(Items.COAL, 1, 1), '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE) });
		GameRegistry.addRecipe(new ItemStack(Blocks.RAIL, 16), new Object[] { "X X", "X#X", "X X", 'X', Items.IRON_INGOT, '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE) });
		GameRegistry.addRecipe(new ItemStack(Blocks.GOLDEN_RAIL, 6), new Object[] { "X X", "X#X", "XRX", 'X', Items.GOLD_INGOT, 'R', Items.REDSTONE, '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE) });
		GameRegistry.addRecipe(new ItemStack(Blocks.ACTIVATOR_RAIL, 6), new Object[] { "XSX", "X#X", "XSX", 'X', Items.IRON_INGOT, '#', Blocks.REDSTONE_TORCH, 'S', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE) });
		GameRegistry.addRecipe(new ItemStack(Items.FISHING_ROD, 1), new Object[] { "  #", " #X", "# X", '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'X', Items.STRING });
		GameRegistry.addRecipe(new ItemStack(Items.PAINTING, 1), new Object[] { "###", "#X#", "###", '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'X', Blocks.WOOL });
		GameRegistry.addRecipe(new ItemStack(Items.ITEM_FRAME, 1), new Object[] { "###", "#X#", "###", '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'X', Items.LEATHER });
		GameRegistry.addRecipe(new ItemStack(Blocks.LEVER, 1), new Object[] { "X", "#", '#', Blocks.COBBLESTONE, 'X', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE) });
		GameRegistry.addRecipe(new ItemStack(Blocks.TRIPWIRE_HOOK, 2), new Object[] { "I", "S", "#", '#', Blocks.PLANKS, 'S', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'I', Items.IRON_INGOT });
		GameRegistry.addRecipe(new ItemStack(Blocks.REDSTONE_TORCH, 1), new Object[] { "X", "#", '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'X', Items.REDSTONE });
		GameRegistry.addRecipe(new ItemStack(Items.ARMOR_STAND, 1), new Object[] { "///", " / ", "/_/", '/', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), '_', new ItemStack(Blocks.STONE_SLAB, 1, BlockStoneSlab.EnumType.STONE.getMetadata()) });

		// RecipesBanner.java
		EnumDyeColor[] aenumdyecolor = EnumDyeColor.values();
		int i = aenumdyecolor.length;

		for (int j = 0; j < i; ++j)
		{
			EnumDyeColor enumdyecolor = aenumdyecolor[j];
			GameRegistry.addRecipe(new ItemStack(Items.BANNER, 1, enumdyecolor.getDyeDamage()), new Object[] { "###", "###", " | ", '#', new ItemStack(Blocks.WOOL, 1, enumdyecolor.getMetadata()), '|', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE) });
		}

		// RecipesTools.java (excluding Axes)
		String[][] recipePatterns = new String[][] { { "XXX", " # ", " # " }, { "X", "#", "#" }, { "XX", " #", " #" } };
		Object[][] recipeItems = new Object[][] { { Blocks.PLANKS, Blocks.COBBLESTONE, Items.IRON_INGOT, Items.DIAMOND, Items.GOLD_INGOT }, 
		{ SpecializationsMod.WOODEN_PICKAXE, SpecializationsMod.STONE_PICKAXE, SpecializationsMod.IRON_PICKAXE, SpecializationsMod.DIAMOND_PICKAXE, SpecializationsMod.GOLDEN_PICKAXE }, { SpecializationsMod.WOODEN_SHOVEL, SpecializationsMod.STONE_SHOVEL, SpecializationsMod.IRON_SHOVEL, SpecializationsMod.DIAMOND_SHOVEL, SpecializationsMod.GOLDEN_SHOVEL }, { SpecializationsMod.WOODEN_HOE, SpecializationsMod.STONE_HOE, SpecializationsMod.IRON_HOE, SpecializationsMod.DIAMOND_HOE, SpecializationsMod.GOLDEN_HOE } };
		for (i = 0; i < recipeItems[0].length; ++i)
		{
			Object object = recipeItems[0][i];

			for (int j = 0; j < recipeItems.length - 1; ++j)
			{
				Item item = (Item) recipeItems[j + 1][i];
				GameRegistry.addRecipe(new ItemStack(item), new Object[] { recipePatterns[j], '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'X', object });
			}
		}

		// RecipeWeapons.java (excluding Bow and Arrows)
		recipePatterns = new String[][] { { "X", "X", "#" } };
		recipeItems = new Object[][] { { Blocks.PLANKS, Blocks.COBBLESTONE, Items.IRON_INGOT, Items.DIAMOND, Items.GOLD_INGOT }, 
		{ SpecializationsMod.WOODEN_SWORD, SpecializationsMod.STONE_SWORD, SpecializationsMod.IRON_SWORD, SpecializationsMod.DIAMOND_SWORD, SpecializationsMod.GOLDEN_SWORD } };
		for (i = 0; i < recipeItems[0].length; ++i)
		{
			Object object = recipeItems[0][i];

			for (int j = 0; j < recipeItems.length - 1; ++j)
			{
				Item item = (Item) recipeItems[j + 1][i];
				GameRegistry.addRecipe(new ItemStack(item), new Object[] { recipePatterns[j], '#', new ItemStack(BigFMod.STICK, 1, OreDictionary.WILDCARD_VALUE), 'X', object });
			}
		}
	}
}