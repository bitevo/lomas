package bitevo.Zetal.LoMaS.Factions.Inventory;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.Factions.Entity.EntityPigmanWarrior;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemFishingRod;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemShears;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ContainerWarriorInventory extends Container
{
	private static final EntityEquipmentSlot[] VALID_EQUIPMENT_SLOTS = new EntityEquipmentSlot[] { EntityEquipmentSlot.HEAD, EntityEquipmentSlot.CHEST, EntityEquipmentSlot.LEGS, EntityEquipmentSlot.FEET };
	private IInventory warriorInventory;
	private EntityPigmanWarrior warrior;

	public ContainerWarriorInventory(IInventory playerInventory, IInventory warInventory, final EntityPigmanWarrior war, EntityPlayer player)
	{
		this.warrior = war;
		this.warriorInventory = warInventory;
		byte b0 = 3;

		this.addSlotToContainer(new Slot(warriorInventory, 0, 80, 8));
		this.addSlotToContainer(new Slot(warriorInventory, 1, 98, 8));
		
		for (int k = 0; k < 4; ++k)
		{
			final EntityEquipmentSlot entityequipmentslot = VALID_EQUIPMENT_SLOTS[k];
			this.addSlotToContainer(new Slot(warriorInventory, 2 + (3 - k), 8, 8 + k * 18)
			{
				/**
				 * Returns the maximum stack size for a given slot (usually the same as getInventoryStackLimit(), but 1 in the case of armor slots)
				 */
				@Override
				public int getSlotStackLimit()
				{
					return 1;
				}

				/**
				 * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
				 */
				@Override
				public boolean isItemValid(@Nullable ItemStack stack)
				{
					if (stack == null)
					{
						return false;
					}
					else
					{
						return stack.getItem().isValidArmor(stack, entityequipmentslot, warrior);
					}
				}

				@Override
				@Nullable
				@SideOnly(Side.CLIENT)
				public String getSlotTexture()
				{
					return ItemArmor.EMPTY_SLOT_NAMES[entityequipmentslot.getIndex()];
				}
			});
		}
		
		int i = (b0 - 4) * 18;
		int j;
		int k;

		for (j = 0; j < 3; ++j)
		{
			for (k = 0; k < 9; ++k)
			{
				this.addSlotToContainer(new Slot(playerInventory, k + j * 9 + 9, 8 + k * 18, 102 + j * 18 + i));
			}
		}

		for (j = 0; j < 9; ++j)
		{
			this.addSlotToContainer(new Slot(playerInventory, j, 8 + j * 18, 160 + i));
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn)
	{
		return this.warriorInventory.isUseableByPlayer(playerIn) && this.warrior.isEntityAlive() && this.warrior.getDistanceToEntity(playerIn) < 8.0F;
	}

	/**
	 * Take a stack from the specified inventory slot.
	 */
	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index)
	{
		ItemStack itemstack = null;
		Slot slot = (Slot) this.inventorySlots.get(index);

		if (slot != null && slot.getHasStack())
		{
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (index < this.warriorInventory.getSizeInventory())
			{
				if (!this.mergeItemStack(itemstack1, this.warriorInventory.getSizeInventory(), this.inventorySlots.size(), true))
				{
					return null;
				}
			}
			else if (this.getSlot(1).isItemValid(itemstack1) && !this.getSlot(1).getHasStack())
			{
				if (!this.mergeItemStack(itemstack1, 1, 2, false))
				{
					return null;
				}
			}
			else if (this.getSlot(0).isItemValid(itemstack1))
			{
				if (!this.mergeItemStack(itemstack1, 0, 1, false))
				{
					return null;
				}
			}
			else if (this.warriorInventory.getSizeInventory() <= 2 || !this.mergeItemStack(itemstack1, 2, this.warriorInventory.getSizeInventory(), false))
			{
				return null;
			}

			if (itemstack1.stackSize == 0)
			{
				slot.putStack((ItemStack) null);
			}
			else
			{
				slot.onSlotChanged();
			}
		}

		return itemstack;
	}

	/**
	 * Called when the container is closed.
	 */
	@Override
	public void onContainerClosed(EntityPlayer playerIn)
	{
		super.onContainerClosed(playerIn);
		this.warriorInventory.closeInventory(playerIn);
	}
}