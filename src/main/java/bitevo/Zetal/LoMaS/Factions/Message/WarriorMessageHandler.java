package bitevo.Zetal.LoMaS.Factions.Message;

import bitevo.Zetal.LoMaS.Factions.Entity.EntityPigmanWarrior;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Faction;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Shared.Entity.EntityFactionMember;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

public class WarriorMessageHandler implements IMessageHandler<WarriorMessage, IMessage>
{
	@Override
	public IMessage onMessage(WarriorMessage message, MessageContext ctx)
	{
		if (ctx.side == Side.CLIENT)
		{
			handleCWarriorMessage(message);
		}
		else
		{
			handleSWarriorMessage(message);
		}
		return null;
	}

	private void handleCWarriorMessage(WarriorMessage packet)
	{
	}

	private void handleSWarriorMessage(WarriorMessage packet)
	{
		Entity entity = FMLCommonHandler.instance().getMinecraftServerInstance().getEntityFromUuid(packet.commanderID);
		if(entity != null && entity instanceof EntityPlayerMP)
		{
			EntityPlayerMP sender = (EntityPlayerMP) entity;
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(sender);
			LoMaS_Faction faction = LoMaS_Faction.getLoMaSFaction(lplayer.faction);
			entity = FMLCommonHandler.instance().getMinecraftServerInstance().getEntityFromUuid(packet.warriorID);
			if(lplayer != null && entity != null && entity instanceof EntityPigmanWarrior)
			{
				EntityPigmanWarrior warrior = (EntityPigmanWarrior) entity;
				LoMaS_Faction warriorFaction = LoMaS_Faction.getLoMaSFaction(warrior.getFaction());
				if((warriorFaction == null) || (faction != null && faction.equals(warriorFaction)))
				{
					double dis = sender.getDistanceSqToEntity(warrior);
					if(dis <= 16.0D)
					{
						warrior.setState(packet.command);
						warrior.setCommanderId(packet.commanderID);
					}
				}
			}
		}
	}
}
