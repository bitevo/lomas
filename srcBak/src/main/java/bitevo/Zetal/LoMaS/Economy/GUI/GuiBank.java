package bitevo.Zetal.LoMaS.Economy.GUI;

import java.text.DecimalFormat;

import net.minecraft.client.renderer.InventoryEffectRenderer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Economy.EconomyMod;
import bitevo.Zetal.LoMaS.Economy.Inventory.ContainerBank;

public class GuiBank extends InventoryEffectRenderer
{
    private static final ResourceLocation BANK_TEXTURE = new ResourceLocation(EconomyMod.modid, "gui/bank.png");
    private EntityPlayer thePlayer;
    protected final LoMaS_Player lPlayer;
    private final DecimalFormat formatter = new DecimalFormat();

    public GuiBank(EntityPlayer par1EntityPlayer)
    {
        super(new ContainerBank(par1EntityPlayer.inventory, par1EntityPlayer));
        this.lPlayer = LoMaS_Player.getLoMaSPlayer(par1EntityPlayer);
        this.thePlayer = par1EntityPlayer;
        this.xSize = 255;
        this.ySize = 193;
        this.guiLeft = (this.width - this.xSize) / 2;
        this.guiTop = (this.height - this.ySize) / 2;
        this.allowUserInput = true;
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
    }

    /**
     * Draw the foreground layer for the GuiContainer (everything in front of the items)
     */
    protected void drawGuiContainerForegroundLayer(int par1, int par2)
    {
		this.mc.thePlayer.inventoryContainer.detectAndSendChanges();
    	int coins = 0;
    	for(int i = 0; i < 3; i++)
    	{
    		if(this.mc.thePlayer.inventory.getStackInSlot(41 + i) instanceof ItemStack)
    		{
    			coins += ((ItemStack) this.mc.thePlayer.inventory.getStackInSlot(41 + i)).stackSize;
    		}
    	}
    	if(coins > 64)
    	{
    		coins = 64;
    	}
        this.fontRendererObj.drawString(coins + "/64", 200, 21, 4210752);
        
        this.fontRendererObj.drawString("Input", 200, 67, 4210752);
        this.fontRendererObj.drawString("Output", 200, 103, 4210752);
        this.fontRendererObj.drawString("Currency", 184, 8, 4210752);
        this.fontRendererObj.drawString(formatter.format(LoMaS_Player.getLoMaSPlayer(thePlayer).getBankCC()), 27, 6, 4210752);
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int par1, int par2, float par3)
    {
        super.drawScreen(par1, par2, par3);
    }

    /**
     * Draw the background layer for the GuiContainer (everything behind the items)
     */
    protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3)
    {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(BANK_TEXTURE);
        this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
    }
}
