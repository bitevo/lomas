package bitevo.Zetal.LoMaS.Specializations.Message;

import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;

public class AbilityMessageHandler implements IMessageHandler<AbilityMessage, IMessage>
{
	@Override
	public IMessage onMessage(AbilityMessage message, MessageContext ctx) 
	{
		if(ctx.side == Side.CLIENT)
		{
			handleCAbilityMessage(message);
		}
		else
		{
			handleSAbilityMessage(message);
		}
		return null;
	}

	@SideOnly(Side.CLIENT)
	private void handleCAbilityMessage(AbilityMessage packet) 
	{
		LoMaS_Player playerEntity = LoMaS_Player.getLoMaSPlayer(packet.uuid);
   	 	playerEntity.isActive = packet.isActive;
   	 	playerEntity.counter = packet.counter;
	}

	private void handleSAbilityMessage(AbilityMessage packet) 
	{
		LoMaS_Player playerEntity = LoMaS_Player.getLoMaSPlayer(packet.uuid);
   	 	if(playerEntity.isActive == false && playerEntity.counter == 0)
   	 	{
   	 		playerEntity.isActive = true;
   	 	}
	}
}
