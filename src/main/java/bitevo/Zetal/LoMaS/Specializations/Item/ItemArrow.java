package bitevo.Zetal.LoMaS.Specializations.Item;

import java.util.List;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityCustomArrow;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.potion.PotionType;
import net.minecraft.potion.PotionUtils;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemArrow extends net.minecraft.item.ItemArrow
{
	/** List of dye color names */
	public static final int[] arrowMeta = new int[] { 0, 1, 2, 3, 4 };// , "Obsidian"};
	public static final String[] arrowNames = new String[] { "Wooden", "Flint", "Iron", "Golden", "Diamond" };// , "Obsidian"};
	public static final String[] arrowTextures = new String[] { "Wood", "Stone", "Iron", "Gold", "Diamond" };// , "arrow_Obsidian"};

	public ItemArrow()
	{
		super();
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
		this.setCreativeTab(CreativeTabs.COMBAT);
	}

	/**
	 * returns a list of items with the same ID, but different meta (eg: dye returns 16 items)
	 */
	@Override
	public void getSubItems(Item itemIn, CreativeTabs par2CreativeTabs, List subItems)
	{
		for (int j = 0; j < 5; ++j)
		{
			for (PotionType potiontype : PotionType.REGISTRY)
			{
				subItems.add(PotionUtils.addPotionToItemStack(new ItemStack(SpecializationsMod.ARROW, 1, j), potiontype));
			}
		}
	}

	/**
	 * allows items to add custom lines of information to the mouseover description
	 */
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced)
	{
		super.addInformation(stack, playerIn, tooltip, advanced);
		PotionUtils.addPotionTooltip(stack, tooltip, 0.125F);
	}

	@Override
	public String getItemStackDisplayName(ItemStack stack)
	{
		return arrowNames[stack.getMetadata()] + " " + I18n.translateToLocal(PotionUtils.getPotionFromItem(stack).getNamePrefixed("tipped_arrow.effect."));
	}

	public EntityCustomArrow createArrow(World worldIn, ItemStack stack, EntityLivingBase shooter, int type)
	{
		EntityCustomArrow entitytippedarrow = new EntityCustomArrow(worldIn, shooter, type);
		entitytippedarrow.setPotionEffect(stack);
		return entitytippedarrow;
	}

	/**
	 * Returns the unlocalized name of this item. This version accepts an ItemStack so different stacks can have different names based on their damage or NBT.
	 */
	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack)
	{
		int i = MathHelper.clamp_int(par1ItemStack.getItemDamage(), 0, 4);
		return super.getUnlocalizedName() + "." + arrowNames[i];
	}

	public static class RecipeTippedArrow implements IRecipe
	{
		private final ItemStack[] EMPTY_ITEMS = new ItemStack[9];

		/**
		 * Used to check if a recipe matches current crafting inventory
		 */
		@Override
		public boolean matches(InventoryCrafting inv, World worldIn)
		{
			if (inv.getWidth() == 3 && inv.getHeight() == 3)
			{
				for (int i = 0; i < inv.getWidth(); ++i)
				{
					for (int j = 0; j < inv.getHeight(); ++j)
					{
						ItemStack itemstack = inv.getStackInRowAndColumn(i, j);

						if (itemstack == null)
						{
							return false;
						}

						Item item = itemstack.getItem();

						if (i == 1 && j == 1)
						{
							if (item != Items.LINGERING_POTION)
							{
								return false;
							}
						}
						else if (item != SpecializationsMod.ARROW)
						{
							return false;
						}
					}
				}

				return true;
			}
			else
			{
				return false;
			}
		}

		/**
		 * Returns an Item that is the result of this recipe
		 */
		@Override
		@Nullable
		public ItemStack getCraftingResult(InventoryCrafting inv)
		{
			ItemStack itemstack = inv.getStackInRowAndColumn(1, 1);

			if (itemstack != null && itemstack.getItem() == Items.LINGERING_POTION)
			{
				ItemStack itemstack1 = new ItemStack(SpecializationsMod.ARROW, 8);
				PotionUtils.addPotionToItemStack(itemstack1, PotionUtils.getPotionFromItem(itemstack));
				PotionUtils.appendEffects(itemstack1, PotionUtils.getFullEffectsFromItem(itemstack));
				return itemstack1;
			}
			else
			{
				return null;
			}
		}

		/**
		 * Returns the size of the recipe area
		 */
		@Override
		public int getRecipeSize()
		{
			return 9;
		}

		@Override
		@Nullable
		public ItemStack getRecipeOutput()
		{
			return null;
		}

		@Override
		public ItemStack[] getRemainingItems(InventoryCrafting inv)
		{
			return EMPTY_ITEMS;
		}
	}
}
