package bitevo.Zetal.LoMaS.PolyZones;

import java.io.Serializable;

public class BlockLoc implements Serializable {

	private double x;
	private double y;
	private double z;
	private double hy;

	/**
	 * Creates a standard Block Location with the given coordinates in the order
	 * x, y, z
	 * 
	 * @param xt
	 *            The x coordinate of this object.
	 * @param yt
	 *            The y coordinate of this object.
	 * @param zt
	 *            The z coordinate of this object.
	 */
	public BlockLoc(int xt, int yt, int zt) {
		x = xt;
		y = yt;
		z = zt;
		hy = y + 3.0;
	}

	public BlockLoc() {
		
	}

	/**
	 * Returns the x value of this object as a double.
	 * 
	 * @return x - This objects x coordinate.
	 */
	public double getX() {
		return x;
	}

	/**
	 * Sets this objects x value to the given double.
	 * 
	 * @param nx
	 *            The new x value.
	 */
	public void setX(double nx) {
		x = nx;
	}

	/**
	 * Returns the y value of this object as a double.
	 * 
	 * @return y - This objects y coordinate.
	 */
	public double getY() {
		return y;
	}

	/**
	 * Sets this objects y value to the given double.
	 * 
	 * @param ny
	 *            The new y value.
	 */
	public void setY(double ny) {
		y = ny;
		hy = y + 3;
	}

	public void setHighY(double nhy) {
		hy = nhy;
	}

	/**
	 * Returns the z value of this object as a double.
	 * 
	 * @return z - This objects z coordinate.
	 */
	public double getZ() {
		return z;
	}

	/**
	 * Sets this objects z value to the given double.
	 * 
	 * @param nz
	 *            The new z value.
	 */
	public void setZ(double nz) {
		z = nz;
	}

	/**
	 * Increments the z value by one.
	 */
	public void incrementZ() {
		z = z + 1;
	}

	/**
	 * Decrements the x value by one.
	 */
	public void decrementX() {
		x = x - 1;
	}

	/**
	 * Decrements the z value by one.
	 */
	public void decrementZ() {
		z = z - 1;
	}

	@Override
	public String toString() {
		return "X: " + x + " Y: " + y + " Z: " + z;
	}

	/**
	 * Checks if the given object is equal to this by checking if it is an
	 * instance of BlockLoc and if all coordinates match.
	 */
	public boolean equals(Object obj) {
		if (!(obj instanceof BlockLoc)) {
			System.out.println("NOT A BLOCKLOC!");
			return false;
		} else {
			BlockLoc temp = (BlockLoc) obj;
			return temp.getX() == this.getX()
					&& ((temp.getY() >= this.y-1) && (temp.getY() < this.hy))
					&& temp.getZ() == this.getZ();
		}
	}

	public boolean equalsIgnoreY(Object obj) {
		if (!(obj instanceof BlockLoc)) {
			return false;
		} else {
			BlockLoc temp = (BlockLoc) obj;
			return temp.getX() == this.getX() && temp.getZ() == this.getZ();
		}
	}

	/**
	 * Custom cloning method which returns a new instance of this objects type
	 * rather than a normal object.
	 */
	public BlockLoc clone() {
		return new BlockLoc((int) this.x, (int) this.y, (int) this.z);
	}
}
