package bitevo.Zetal.LoMaS.Citizens.Entity;

import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.Citizens.CitizensMod;
import bitevo.Zetal.LoMaS.Citizens.Entity.ai.EntityAIGoHome;
import bitevo.Zetal.LoMaS.Citizens.Entity.ai.EntityAIGoOutside;
import bitevo.Zetal.LoMaS.Citizens.Entity.ai.EntityAIGoToDesire;
import bitevo.Zetal.LoMaS.Citizens.Entity.ai.EntityAIRandomlyLook;
import bitevo.Zetal.LoMaS.Citizens.TileEntity.TileEntityCitizenChest;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import bitevo.Zetal.LoMaS.Shared.Entity.EntityFactionMember;
import net.minecraft.block.Block;
import net.minecraft.entity.ai.EntityAIMoveTowardsRestriction;
import net.minecraft.entity.ai.EntityAIOpenDoor;
import net.minecraft.entity.ai.EntityAIRestrictOpenDoor;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.passive.HorseType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.AnimalChest;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.inventory.IInventoryChangedListener;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.item.Item;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemFishingRod;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemShears;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.pathfinding.PathNavigateGround;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class EntityPigmanCitizen extends EntityFactionMember implements IInventoryChangedListener
{
	/**
	 * Happiness is a base of 0. Finding desires increases happiness to a maximum of 100. Failing to find desires decreases happiness to a minimum of -100. At 100 happiness, citizens will produce the
	 * highest possible amount of goods and income. At -100 happiness, citizens will produce no goods, and no income.
	 */
	private int happiness = 0;
	/**
	 * ID for what the Citizen currently desires, using the desires block array and task ID's.
	 */
	private int desire = 0;
	public static final Block[] desires = { Blocks.BOOKSHELF, Blocks.JUKEBOX, Blocks.FLOWER_POT, Blocks.RED_FLOWER, Blocks.YELLOW_FLOWER, Blocks.GLASS_PANE, Blocks.GLASS, Blocks.CARPET, Blocks.DOUBLE_PLANT, Blocks.BED };
	public static final Block[] unwanted = { Blocks.DIRT, Blocks.GRAVEL, Blocks.STONE, Blocks.GRASS, Blocks.BEDROCK, Blocks.CLAY, Blocks.ICE, Blocks.SAND, Blocks.SNOW_LAYER, Blocks.SNOW, Blocks.NETHERRACK, Blocks.SOUL_SAND, Blocks.MYCELIUM, Blocks.LEAVES, Blocks.LEAVES2, Blocks.LOG, Blocks.LOG2, Blocks.DIAMOND_ORE, Blocks.IRON_ORE, Blocks.COAL_ORE, Blocks.EMERALD_ORE, Blocks.GOLD_ORE, Blocks.LAPIS_ORE, Blocks.QUARTZ_ORE, Blocks.REDSTONE_ORE, Blocks.AIR };
	public BlockPos currentDesire;
	public int incomeTicks = 0;
	public InventoryBasic inventory;
	private BlockPos homeChest;
	private String unhappyReason = "";
	public static final DataParameter<Integer> HOME_X = EntityDataManager.<Integer> createKey(EntityPigmanCitizen.class, DataSerializers.VARINT);
	public static final DataParameter<Integer> HOME_Y = EntityDataManager.<Integer> createKey(EntityPigmanCitizen.class, DataSerializers.VARINT);
	public static final DataParameter<Integer> HOME_Z = EntityDataManager.<Integer> createKey(EntityPigmanCitizen.class, DataSerializers.VARINT);
	public static final DataParameter<Integer> DESIRE = EntityDataManager.<Integer> createKey(EntityPigmanCitizen.class, DataSerializers.VARINT);
	public static final DataParameter<Integer> HAPPINESS = EntityDataManager.<Integer> createKey(EntityPigmanCitizen.class, DataSerializers.VARINT);
	public static final DataParameter<String> UNHAPPY = EntityDataManager.<String> createKey(EntityPigmanCitizen.class, DataSerializers.STRING);

	public EntityPigmanCitizen(World worldIn)
	{
		super(worldIn);
		this.setPathPriority(PathNodeType.WATER, -1.0F);
		((PathNavigateGround) this.getNavigator()).setEnterDoors(true);
		((PathNavigateGround) this.getNavigator()).setCanSwim(true);
		((PathNavigateGround) this.getNavigator()).setBreakDoors(true);
		this.tasks.addTask(0, new EntityAISwimming(this));
		this.tasks.addTask(6, new EntityAIGoToDesire(this));
		this.tasks.addTask(7, new EntityAIRandomlyLook(this));
		this.tasks.addTask(8, new EntityAIRestrictOpenDoor(this));
		this.tasks.addTask(9, new EntityAIOpenDoor(this, true));
		this.tasks.addTask(10, new EntityAIMoveTowardsRestriction(this, 0.3D));
		this.tasks.addTask(11, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
		this.tasks.addTask(12, new EntityAIGoHome(this));
		this.tasks.addTask(11, new EntityAIGoOutside(this, 0.3D));
		this.setCanPickUpLoot(false);
		this.setSize(0.6F, 1.95F);
		this.initInventory();
	}

	public EntityPigmanCitizen(World worldIn, BlockPos homeChest)
	{
		this(worldIn);
		this.setHomeChest(homeChest);
	}

	@Override
	protected void entityInit()
	{
		super.entityInit();
		this.dataManager.register(this.HOME_X, 0);
		this.dataManager.register(this.HOME_Y, 0);
		this.dataManager.register(this.HOME_Z, 0);
		this.dataManager.register(this.DESIRE, 0);
		this.dataManager.register(this.HAPPINESS, 0);
		this.dataManager.register(this.UNHAPPY, "");
	}

	@Override
	public void onUpdate()
	{
		super.onUpdate();
		if (this.ticksExisted % 800 == 0)
		{
			List pigfriends = this.worldObj.getEntitiesWithinAABB(this.getClass(), this.getEntityBoundingBox().expand(8, 8, 8));
			List playerfriends = this.worldObj.getEntitiesWithinAABB(EntityPlayer.class, this.getEntityBoundingBox().expand(8, 8, 8));
			// System.out.println(list.size());
			if (pigfriends.size() > 4 || playerfriends.size() > 4)
			{
				this.setHappiness(this.getHappiness() - 10);
				this.setUnhappyReason("I'm crowded");
			}
			else if (pigfriends.size() <= 1 && playerfriends.size() <= 1)
			{
				this.setHappiness(this.getHappiness() - 8);
				this.setUnhappyReason("I'm lonely");
			}

			if (!this.getJob().equalsIgnoreCase("None"))
			{
				this.setHappiness(this.getHappiness() - 5);
			}
		}
	}

	@Override
	public void updateAITasks()
	{
		if (currentDesire != null)
		{
			//System.out.println(currentDesire.getDistance(this.getPosition().getX(), this.getPosition().getY(), this.getPosition().getZ()));
			if (currentDesire.getDistance(this.getPosition().getX(), this.getPosition().getY(), this.getPosition().getZ()) < 5)
			{
				//System.out.println("[Update] Successfully pathed to desire: " + desires[desire] + " at Blockpos " + currentDesire);
				this.currentDesire = null;
				this.setDesire(this.getDesire() + 1);
				this.setHappiness(this.getHappiness() + 7);
			}
		}
	}

	@Override
	public boolean canBeLeashedTo(EntityPlayer player)
	{
		return false;
	}

	@Override
	public void writeEntityToNBT(NBTTagCompound tagCompound)
	{
		super.writeEntityToNBT(tagCompound);
		tagCompound.setInteger("desire", this.getDesire());
		tagCompound.setInteger("happiness", this.getHappiness());
		tagCompound.setInteger("xHome", this.getHomeChest().getX());
		tagCompound.setInteger("yHome", this.getHomeChest().getY());
		tagCompound.setInteger("zHome", this.getHomeChest().getZ());
		tagCompound.setInteger("incomeTick", this.incomeTicks);
		tagCompound.setString("unhappy", this.getUnhappyReason());

		NBTTagList nbttaglist = new NBTTagList();
		for (int i = 0; i < this.inventory.getSizeInventory(); ++i)
		{
			if (this.inventory.getStackInSlot(i) != null)
			{
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte) i);
				this.inventory.getStackInSlot(i).writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}
		tagCompound.setTag("Items", nbttaglist);
	}

	@Override
	public void readEntityFromNBT(NBTTagCompound tagCompound)
	{
		super.readEntityFromNBT(tagCompound);
		this.setDesire(tagCompound.getInteger("desire"));
		this.setHappiness(tagCompound.getInteger("happiness"));
		this.setHomeChest(new BlockPos(tagCompound.getInteger("xHome"), tagCompound.getInteger("yHome"), tagCompound.getInteger("zHome")));
		this.incomeTicks = tagCompound.getInteger("incomeTick");
		this.setUnhappyReason(tagCompound.getString("unhappy"));

		this.initInventory();
		NBTTagList nbttaglist = tagCompound.getTagList("Items", 10);
		this.inventory = new InventoryBasic("Citizen", false, 1);
		for (int i = 0; i < nbttaglist.tagCount(); ++i)
		{
			NBTTagCompound nbttagcompound1 = (NBTTagCompound) nbttaglist.getCompoundTagAt(i);
			int j = nbttagcompound1.getByte("Slot") & 255;

			if (j >= 0 && j < this.inventory.getSizeInventory())
			{
				this.inventory.setInventorySlotContents(j, ItemStack.loadItemStackFromNBT(nbttagcompound1));
			}
		}
	}

	@Override
	public boolean canBePushed()
	{
		return true;
	}

	@Override
	protected boolean canDespawn()
	{
		return false;
	}

	public String getJob()
	{
		String ret = "None";
		ItemStack heldStack = this.getHeldItem(EnumHand.MAIN_HAND);
		if (heldStack != null)
		{
			Item heldItem = heldStack.getItem();
			if (heldItem != null)
			{
				if (heldItem instanceof ItemPickaxe)
				{
					ret = "Miner";
				}
				else if (heldItem instanceof ItemHoe)
				{
					ret = "Farmer";
				}
				else if (heldItem instanceof ItemFishingRod)
				{
					ret = "Fisher";
				}
				else if (heldItem instanceof ItemAxe)
				{
					ret = "Woodsman";
				}
				else if (heldItem instanceof ItemShears)
				{
					ret = "Caretaker";
				}
				else if (heldItem instanceof ItemSpade)
				{
					ret = "Landscaper";
				}
			}
		}
		return ret;
	}

	public int getIncomeForJob(String job)
	{
		Random rand = new Random();
		int ret = 0;
		int toolBonus = 0;
		if (this.getHeldItem(EnumHand.MAIN_HAND).getItem() instanceof ItemTool)
		{
			toolBonus = (int) Math.pow(((ItemTool) this.getHeldItem(EnumHand.MAIN_HAND).getItem()).getToolMaterial().getHarvestLevel(), 2) * 10;
		}
		int mod = ((happiness + toolBonus + 100) / 200);
		if (job.equalsIgnoreCase("Miner"))
		{
			ret = 3 * mod;
		}
		else if (job.equalsIgnoreCase("Farmer"))
		{
			ret = 4 * mod;
		}
		else if (job.equalsIgnoreCase("Fisher"))
		{
			ret = 4 * mod;
		}
		else if (job.equalsIgnoreCase("Woodsman"))
		{
			ret = 5 * mod;
		}
		else if (job.equalsIgnoreCase("Caretaker"))
		{
			ret = 5 * mod;
		}
		else if (job.equalsIgnoreCase("Landscaper"))
		{
			ret = 5 * mod;
		}
		return ret;
	}

	public ItemStack getStackForJob(String job)
	{
		Random rand = new Random();
		ItemStack ret = null;
		Item item = null;
		int toolBonus = 0;
		if (this.getHeldItem(EnumHand.MAIN_HAND).getItem() instanceof ItemTool)
		{
			toolBonus = (int) Math.pow(((ItemTool) this.getHeldItem(EnumHand.MAIN_HAND).getItem()).getToolMaterial().getHarvestLevel(), 2) * 5;
		}
		int mod = ((happiness + toolBonus + 100) / 200);
		boolean scm = LoMaS_Utils.SCM;
		if (job.equalsIgnoreCase("Miner"))
		{
			if (this.getHeldItem(EnumHand.MAIN_HAND).getItem() instanceof ItemTool)
			{
				toolBonus = (int) Math.pow(((ItemTool) this.getHeldItem(EnumHand.MAIN_HAND).getItem()).getToolMaterial().getHarvestLevel(), 2) * 5;
				toolBonus = toolBonus - 45;
			}
			ret = new ItemStack(CitizensMod.getRandomOre(toolBonus));
		}
		else if (job.equalsIgnoreCase("Farmer"))
		{
			if (rand.nextInt(10) >= 3)
			{
				item = scm ? bitevo.Zetal.LoMaS.Specializations.FoodInitializer.WHEAT : Items.WHEAT;
				ret = new ItemStack(item, 1 + toolBonus, rand.nextInt(3));
			}
			else
			{
				item = scm ? bitevo.Zetal.LoMaS.Specializations.FoodInitializer.WHEAT_SEEDS : Items.WHEAT_SEEDS;
				ret = new ItemStack(item, 1);
			}
		}
		else if (job.equalsIgnoreCase("Fisher"))
		{
			item = scm ? bitevo.Zetal.LoMaS.Specializations.FoodInitializer.FISH : Items.FISH;
			int meta = scm ? rand.nextInt(12) : rand.nextInt(4);
			ret = new ItemStack(item, 1 + toolBonus, meta);
		}
		else if (job.equalsIgnoreCase("Woodsman"))
		{
			if (rand.nextInt(10) > 3)
			{
				ret = new ItemStack(Blocks.LOG, 1 + toolBonus, rand.nextInt(4));
			}
			else
			{
				ret = new ItemStack(Blocks.LOG2, 1 + toolBonus, rand.nextInt(2));
			}
		}
		else if (job.equalsIgnoreCase("Caretaker"))
		{
			int i = rand.nextInt(10);
			if (i <= 3)
			{
				ret = new ItemStack(Items.EGG, 1);
			}
			else if (i <= 6)
			{
				ret = new ItemStack(Blocks.WOOL, 1);
			}
			else if (i <= 9)
			{
				ret = new ItemStack(Items.STRING, 1);
			}
			else if (i <= 10)
			{
				ret = new ItemStack(Items.SLIME_BALL, 1);
			}
		}
		else if (job.equalsIgnoreCase("Landscaper"))
		{
			int i = rand.nextInt(10);
			if (i <= 3)
			{
				ret = new ItemStack(Items.FLINT, 1 + toolBonus);
			}
			else if (i <= 6)
			{
				ret = new ItemStack(Items.CLAY_BALL, 1 + toolBonus);
			}
			else if (i <= 8)
			{
				ret = new ItemStack(Blocks.DIRT, 1 + toolBonus);
			}
			else if (i <= 9)
			{
				ret = new ItemStack(Blocks.SAND, 1 + toolBonus);
			}
			else if (i <= 10)
			{
				ret = new ItemStack(Items.SLIME_BALL, 1 + toolBonus);
			}
		}
		if (this.happiness >= 75)
		{
			ret.stackSize *= 2;
		}
		return ret;
	}

	/**
	 * Returns the sound this mob makes while it's alive.
	 */
	@Override
	protected SoundEvent getAmbientSound()
	{
		return CitizensMod.citizen_say;
	}

	/**
	 * Returns the sound this mob makes when it is hurt.
	 */
	@Override
	protected SoundEvent getHurtSound()
	{
		return CitizensMod.citizen_say;
	}

	/**
	 * Returns the sound this mob makes on death.
	 */
	@Override
	protected SoundEvent getDeathSound()
	{
		return CitizensMod.citizen_death;
	}

	@Override
	protected void playStepSound(BlockPos p_180429_1_, Block p_180429_2_)
	{
		this.playSound(SoundEvents.ENTITY_PIG_STEP, 0.25F, 0.5F);
	}

	@Override
	protected boolean processInteract(EntityPlayer player, EnumHand hand, @Nullable ItemStack stack)
	{
		// System.out.println(this.getHomeChest());
		if (this.getHomeChest() != null && this.worldObj.getTileEntity(this.getHomeChest()) instanceof TileEntityCitizenChest)
		{
			TileEntityCitizenChest chest = (TileEntityCitizenChest) this.worldObj.getTileEntity(this.getHomeChest());
			if (chest.owner != null && chest.owner.equals(player.getPersistentID()))
			{
				player.openGui(CitizensMod.instance, 0, this.worldObj, this.getEntityId(), -1, -1);
			}
		}
		return false;
	}

	public int getHappiness()
	{
		this.happiness = this.dataManager.get(this.HAPPINESS);
		return happiness;
	}

	@Nullable
	@Override
	public ItemStack getItemStackFromSlot(EntityEquipmentSlot slotIn)
	{
		ItemStack itemstack = null;
		itemstack = this.inventory.getStackInSlot(0);
		return itemstack;
	}

	@Override
	public void setItemStackToSlot(EntityEquipmentSlot slotIn, @Nullable ItemStack stack)
	{
		this.inventory.setInventorySlotContents(0, stack);
		this.inventory.markDirty();
	}

	@Override
	public ItemStack getHeldItem(EnumHand hand)
	{
		return hand == EnumHand.MAIN_HAND ? this.inventory.getStackInSlot(0) : null;
	}

	@Override
	public ItemStack getHeldItemMainhand()
	{
		return this.inventory.getStackInSlot(0);
	}

	@Override
	public ItemStack getHeldItemOffhand()
	{
		return null;
	}

	@Override
	public void onInventoryChanged(InventoryBasic invBasic)
	{
		if (!this.worldObj.isRemote)
		{
		}
	}

	private void initInventory()
	{
		InventoryBasic inv = this.inventory;
		this.inventory = new InventoryBasic("Citizen", false, 1);
		this.inventory.setCustomName(this.getName());

		if (inv != null)
		{
			inv.removeInventoryChangeListener(this);
			int i = Math.min(inv.getSizeInventory(), this.inventory.getSizeInventory());

			for (int j = 0; j < i; ++j)
			{
				ItemStack itemstack = inv.getStackInSlot(j);

				if (itemstack != null)
				{
					this.inventory.setInventorySlotContents(j, itemstack.copy());
				}
			}
		}

		this.inventory.addInventoryChangeListener(this);
		this.onInventoryChanged(this.inventory);
		this.itemHandler = new net.minecraftforge.items.wrapper.InvWrapper(this.inventory);
	}

	public void setHappiness(int happiness)
	{
		this.happiness = happiness;
		if (happiness >= 100)
		{
			this.happiness = 100;
		}
		else if (happiness <= -100)
		{
			this.happiness = -100;
		}
		this.dataManager.set(this.HAPPINESS, this.happiness);
	}

	public int getDesire()
	{
		this.desire = this.dataManager.get(this.DESIRE);
		return desire;
	}

	public void setDesire(int desire)
	{
		if (desire >= this.desires.length)
		{
			this.desire = 0;
		}
		else
		{
			this.desire = desire;
		}
		this.dataManager.set(this.DESIRE, this.desire);
	}

	public BlockPos getHomeChest()
	{
		if (homeChest == null)
		{
			return new BlockPos(this.dataManager.get(this.HOME_X), this.dataManager.get(this.HOME_Y), this.dataManager.get(this.HOME_Z));
		}
		return homeChest;
	}

	public void setHomeChest(BlockPos homeChest)
	{
		this.homeChest = homeChest;
		this.dataManager.set(this.HOME_X, homeChest.getX());
		this.dataManager.set(this.HOME_Y, homeChest.getY());
		this.dataManager.set(this.HOME_Z, homeChest.getZ());
	}

	public String getUnhappyReason()
	{
		this.unhappyReason = this.dataManager.get(this.UNHAPPY);
		return unhappyReason;
	}

	public void setUnhappyReason(String unhappyReason)
	{
		this.dataManager.set(this.UNHAPPY, unhappyReason);
		this.unhappyReason = unhappyReason;
	}

	private net.minecraftforge.items.IItemHandler itemHandler = null; // Initialized by initHorseChest above.

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getCapability(net.minecraftforge.common.capabilities.Capability<T> capability, net.minecraft.util.EnumFacing facing)
	{
		if (capability == net.minecraftforge.items.CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) return (T) itemHandler;
		return super.getCapability(capability, facing);
	}

	@Override
	public boolean hasCapability(net.minecraftforge.common.capabilities.Capability<?> capability, net.minecraft.util.EnumFacing facing)
	{
		return capability == net.minecraftforge.items.CapabilityItemHandler.ITEM_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
	}
}
