package bitevo.Zetal.LoMaS.PolyZones;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

import net.minecraft.entity.player.EntityPlayer;

public class CustomPlayer implements Serializable {

	private UUID uuid;
	private String name = "";
	private ArrayList<Access> zoneAccess = new ArrayList<Access>();
	private boolean isInsideZone = false;
	private String currentZone = "";
	private String currentLand = "";
	private boolean posX;
	private boolean posZ;
	private boolean inTown;

	public CustomPlayer(UUID n) {
		uuid = n;
	}

	public CustomPlayer(EntityPlayer p) {
		uuid = p.getPersistentID();
	}

	public String getName() {
		return name;
	}

	public void setZoneAccessArray(ArrayList<Access> x) {
		zoneAccess.clear();
		for (int i = 0; i < x.size(); i++) {
			zoneAccess.add((Access) x.get(i).clone());
		}
	}

	public void addZone(String zoneName, int permissions) {
		zoneAccess.add(new Access(zoneName, permissions));
	}

	public int getPermission(String zoneName) {
		for (Access l : zoneAccess) {
			if (l.getPolyZoneName().equals("zoneName")) {
				return l.getPermissions();
			}
		}
		return -1;
	}

	public Access getAccess(String zoneName) {
		for (Access l : zoneAccess) {
			if (l.getPolyZoneName().equals(zoneName)) {
				return l;
			}
		}
		return null;
	}

	public void insideNow(String zoneName) {
		currentZone = zoneName;
		isInsideZone = true;
	}

	public void outsideNow() {
		currentZone = "";
		isInsideZone = false;
	}
	
	public void insideLandNow(String landName) {
		currentLand = landName;
	}
	
	public String getCurrentLand(){
		return currentLand;
	}

	public boolean getIsInsideZone() {
		return isInsideZone;
	}
	
	public String getCurrentZone(){
		return currentZone;
	}
	
	public void setCurrentZone(String zone){
		currentZone = zone;
	}
	
	public void setIsPositiveX(boolean positiveX){
		this.posX = positiveX;
	}
	
	public void setIsPositiveZ(boolean positiveZ){
		this.posZ = positiveZ;
	}	
	
	public void setIsInTown(boolean inTown){
		this.inTown = inTown;
	}
	
	public boolean getIsPositiveX(){
		return this.posX;
	}
	
	public boolean getIsPositiveZ(){
		return this.posZ;
	}	
	
	public boolean getIsInTown(){
		return this.inTown;
	}
	
	public UUID getUUID()
	{
		return uuid;
	}

	public void setUUID(UUID uuid)
	{
		this.uuid = uuid;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof CustomPlayer)
		{
			CustomPlayer in = (CustomPlayer) o;
			if(in.uuid.equals(this.uuid))
			{
				return true;
			}
		}
		return false;
	}
}
