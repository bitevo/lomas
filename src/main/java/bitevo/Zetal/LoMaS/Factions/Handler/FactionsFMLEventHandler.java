package bitevo.Zetal.LoMaS.Factions.Handler;

import bitevo.Zetal.LoMaS.Factions.FactionsMod;
import bitevo.Zetal.LoMaS.Factions.Message.FactionMessage;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Faction;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;

public class FactionsFMLEventHandler
{
	@SubscribeEvent(priority = EventPriority.LOW)
	public void playerConnected(PlayerLoggedInEvent event)
	{
		if (event.player.worldObj != null && !event.player.worldObj.isRemote)
		{
			EntityPlayer player = event.player;
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
			if (player != null && lplayer != null)
			{
				LoMaS_Faction faction = LoMaS_Faction.getLoMaSFaction(lplayer.faction);
				if (faction != null)
				{
					FactionsMod.snw.sendToAll(new FactionMessage(player.getPersistentID(), faction));
				}
			}
		}
	}
}
