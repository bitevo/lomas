package bitevo.Zetal.LoMaS.Specializations.Item.DamageItems;

import java.util.List;

import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemCustomPickaxe extends ItemPickaxe
{
	public ItemCustomPickaxe(ToolMaterial p_i45327_1_)
	{
		super(p_i45327_1_);
	}

	@Override
	public int getMaxDamage(ItemStack stack)
	{
		return SpecializationsMod.getMaxDamage(stack);
	}
}
