package bitevo.Zetal.LoMaS.Specializations.Item;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemIronHeart extends Item
{

	public ItemIronHeart()
	{
		super();
		this.maxStackSize = 16;
		this.setCreativeTab(LoMaS_Utils.tabLoMaS);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean hasEffect(ItemStack par1ItemStack)
	{
		return false;
	}
}
