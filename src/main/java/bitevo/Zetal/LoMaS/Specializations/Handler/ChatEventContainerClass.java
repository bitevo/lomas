package bitevo.Zetal.LoMaS.Specializations.Handler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Skills;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.management.PlayerList;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.event.CommandEvent;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * Name and cast of this class are irrelevant
 */
public class ChatEventContainerClass
{
	public static ArrayList<String> nameFormatList = new ArrayList<String>();
	public static ArrayList<String> messageFormatList = new ArrayList<String>();
	public static ArrayList<String> userList = new ArrayList<String>();
	public static ArrayList<String> tagList = new ArrayList<String>();
	public static ArrayList<String> rankList = new ArrayList<String>();
	public static boolean loaded = false;

	/*
	 * Formatting Codes \u00a70 Black 0 0 0 #000000 0 0 0 #000000 \u00a71 Dark Blue 0 0 170 #0000AA 0 0 42 #00002A \u00a72 Dark Green 0 170 0 #00AA00 0 42 0 #002A00 \u00a73 Dark Aqua 0 170 170 #00AAAA
	 * 0 42 42 #002A2A \u00a74 Dark Red 170 0 0 #AA0000 42 0 0 #2A0000 \u00a75 Purple 170 0 170 #AA00AA 42 0 42 #2A002A \u00a76 Gold 255 170 0 #FFAA00 42 42 0 #2A2A00 \u00a77 Gray 170 170 170 #AAAAAA
	 * 42 42 42 #2A2A2A \u00a78 Dark Gray 85 85 85 #555555 21 21 21 #151515 \u00a79 Blue 85 85 255 #5555FF 21 21 63 #15153F \u00a7a Green 85 255 85 #55FF55 21 63 21 #153F15 \u00a7b Aqua 85 255 255
	 * #55FFFF 21 63 63 #153F3F \u00a7c Red 255 85 85 #FF5555 63 21 21 #3F1515 \u00a7d Light Purple 255 85 255 #FF55FF 63 21 63 #3F153F \u00a7e Yellow 255 255 85 #FFFF55 63 63 21 #3F3F15 \u00a7f White
	 * 255 255 255 #FFFFFF 63 63 63 #3F3F3F --------------- \u00a7k Obfuscated \u00a7l Bold \u00a7m Strikethrough \u00a7n Underline \u00a7o Italic \u00a7r Reset
	 */

	@SubscribeEvent
	public void onWorldSave(WorldEvent.Save event)
	{
		storePlayerNameData(userList, nameFormatList, messageFormatList, tagList, rankList);
	}

	@SubscribeEvent
	public void onWorldLoad(WorldEvent.Load event)
	{
		if (!event.getWorld().isRemote && !loaded)
		{
			getPlayerNameData();
			this.scanExistingPlayers();
			loaded = true;
		}
	}

	public void scanExistingPlayers()
	{
		MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
		PlayerList list = server.getPlayerList();
		for (String username : list.getAllUsernames())
		{
			this.addPlayerToChatList(username);
		}
	}

	@SubscribeEvent
	public void onPlayerJoins(EntityJoinWorldEvent event)
	{
		if (!event.getWorld().isRemote && event.getEntity() instanceof EntityPlayer)
		{
			if (!loaded)
			{
				getPlayerNameData();
				loaded = true;
			}

			if (loaded)
			{
				EntityPlayer player = (EntityPlayer) event.getEntity();
				this.addPlayerToChatList(player.getDisplayNameString());
			}
		}
	}

	@SubscribeEvent
	public void onPlayerSave(PlayerEvent.SaveToFile event)
	{
		File saveDir = new File(event.getPlayerDirectory().getPath() + File.separator + "Specializations" + File.separator);
		saveDir.mkdir();
		File saveFile = new File(saveDir.getPath() + File.separator + event.getEntityPlayer().getUniqueID() + ".spec");
		try
		{
			FileOutputStream saveOut = new FileOutputStream(saveFile);
			ObjectOutputStream saveObj = new ObjectOutputStream(saveOut);
			saveObj.writeObject(LoMaS_Player.getLoMaSPlayer(event.getEntityPlayer()));
			saveObj.close();
		}
		catch (IOException e1)
		{
			System.err.println("Failed to save Specializations file at " + saveFile.getPath() + " for player " + event.getEntityPlayer());
			e1.printStackTrace();
		}
	}

	@SubscribeEvent
	public void onPlayerLoad(PlayerEvent.LoadFromFile event)
	{
		File loadDir = new File(event.getPlayerDirectory().getPath() + File.separator + "Specializations" + File.separator);
		File loadFile = new File(loadDir.getPath() + File.separator + event.getEntityPlayer().getUniqueID() + ".spec");
		try
		{
			FileInputStream loadOut = new FileInputStream(loadFile);
			ObjectInputStream loadObj = new ObjectInputStream(loadOut);
			LoMaS_Player temp = (LoMaS_Player) loadObj.readObject();
			if (LoMaS_Player.getLoMaSPlayer(event.getEntityPlayer()) == null)
			{
				LoMaS_Player.addLoMaSPlayer(event.getEntityPlayer());
				LoMaS_Player.getLoMaSPlayer(event.getEntityPlayer()).copyTraitsFromLoMaSPlayer(temp);
			}
			else
			{
				LoMaS_Player.getLoMaSPlayer(event.getEntityPlayer()).copyTraitsFromLoMaSPlayer(temp);
			}
			loadObj.close();
		}
		catch (IOException e1)
		{
			System.err.println("Failed to load Specializations file at " + loadFile.getPath() + " for player " + event.getEntityPlayer());
			e1.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			System.err.println("Failed to load Specializations file at " + loadFile.getPath() + " for player " + event.getEntityPlayer());
			e.printStackTrace();
		}
	}

	public static String getNameAttribute(String username, int type)
	{
		boolean foundUser = false;
		if (userList.size() > 0)
		{
			for (int i = 0; i < userList.size(); i++)
			{
				if (username.equalsIgnoreCase(userList.get(i)))
				{
					foundUser = true;
					String guildtag = tagList.get(i);
					String rank = rankList.get(i);
					if (type == 0)
					{
						// System.out.println(rank);
						return rank;
					}
					else if (type == 1)
					{
						// System.out.println(guildtag);
						return guildtag;
					}
				}
			}
		}
		// System.out.println("No user data was found for user " + type + " " + username + " Array:" + userList);
		if (type == 0)
		{
			return "No Rank";
		}
		else if (type == 1)
		{
			return "[Renegade]";
		}
		return "";
	}

	public static void addPlayerToChatList(String username)
	{
		boolean foundUser = false;
		if (userList.size() > 0)
		{
			for (int i = 0; i < userList.size(); i++)
			{
				if (username.equalsIgnoreCase(userList.get(i)))
				{
					foundUser = true;
				}
			}
		}
		if (foundUser == false)
		{
			userList.add(username);
			nameFormatList.add("\u00a7f");
			messageFormatList.add("\u00a7r");
			tagList.add("[Renegade]");
			rankList.add("No Rank");
		}
	}

	public int getUserID(EntityPlayer player)
	{
		if (userList.size() > 0)
		{
			for (int i = 0; i < userList.size(); i++)
			{
				if (player.getDisplayNameString().equalsIgnoreCase(userList.get(i)))
				{
					return i;
				}
			}
		}
		return -1;
	}

	@SubscribeEvent
	public void chatHandler(ServerChatEvent event)
	{
		boolean foundUser = false;
		if (userList.size() > 0)
		{
			for (int i = 0; i < userList.size(); i++)
			{
				if (event.getUsername().equalsIgnoreCase(userList.get(i)))
				{
					foundUser = true;
					String nameFormatter = nameFormatList.get(i);
					String messageFormatter = messageFormatList.get(i);
					String guildtag = tagList.get(i);
					TextComponentTranslation fuck = new TextComponentTranslation(nameFormatter + guildtag + "<" + event.getUsername() + ">" + messageFormatter + " ");
					fuck.appendText(event.getMessage());
					event.setComponent(fuck);
				}
			}
		}
		// System.out.println(event.getDisplayNameString());
		if (foundUser == false)
		{
			this.addPlayerToChatList(event.getUsername());
			TextComponentTranslation fuck = new TextComponentTranslation("\u00a7f" + "" + "<" + event.getUsername() + ">" + "\u00a7r" + " ");
			fuck.appendText(event.getMessage());
			event.setComponent(fuck);
		}
	}

	@SubscribeEvent
	public void commandSetChatEvent(CommandEvent event)
	{
		if (event.getCommand() instanceof CommandCustomChat) // Replace with new Command
		{
			ICommandSender sender = event.getSender();
			String[] parameters = event.getParameters();
			EntityPlayerMP entityplayermp = null;
			entityplayermp = (EntityPlayerMP) sender.getCommandSenderEntity();
			if (entityplayermp != null && LoMaS_Utils.isSenderAdmin(sender))
			{
				WorldServer server = entityplayermp.getServerWorld();
				if (parameters.length > 0)
				{
					if (parameters[0].matches("set"))
					{
						if (parameters[2].matches(("namecode")))
						{
							boolean foundUser = false;
							if (userList.size() > 0)
							{
								for (int i = 0; i < userList.size(); i++)
								{
									if (parameters[1].equalsIgnoreCase(userList.get(i)))
									{
										foundUser = true;
										nameFormatList.set(i, this.getAppropriateCode(event, parameters[3]));
										EntityPlayerMP target = (EntityPlayerMP) server.getPlayerEntityByName(parameters[1]);
										if (target != null)
										{
											if (LoMaS_Player.getLoMaSPlayer(target) == null)
											{
												LoMaS_Player.addLoMaSPlayer(target);
											}
											SpecializationsMod.sendPlayerAbilitiesToClients(LoMaS_Player.getLoMaSPlayer(target));
										}
									}
								}
							}
							if (foundUser = false)
							{
								LoMaS_Utils.addChatMessage  (sender, "Could not find player with username: " + parameters[1]);
							}
							storePlayerNameData(userList, nameFormatList, messageFormatList, tagList, rankList);
						}
						if (parameters[2].matches(("messagecode")))
						{
							boolean foundUser = false;
							if (userList.size() > 0)
							{
								for (int i = 0; i < userList.size(); i++)
								{
									if (parameters[1].equalsIgnoreCase(userList.get(i)))
									{
										foundUser = true;
										messageFormatList.set(i, this.getAppropriateCode(event, parameters[3]));
										EntityPlayerMP target = (EntityPlayerMP) server.getPlayerEntityByName(parameters[1]);
										if (target != null)
										{
											if (LoMaS_Player.getLoMaSPlayer(target) == null)
											{
												LoMaS_Player.addLoMaSPlayer(target);
											}
											SpecializationsMod.sendPlayerAbilitiesToClients(LoMaS_Player.getLoMaSPlayer(target));
										}
									}
								}
							}
							if (foundUser = false)
							{
								LoMaS_Utils.addChatMessage  (sender, "Could not find player with username: " + parameters[1]);
							}
							storePlayerNameData(userList, nameFormatList, messageFormatList, tagList, rankList);
						}
						if (parameters[2].matches(("guildtag")))
						{
							boolean foundUser = false;
							if (userList.size() > 0)
							{
								for (int i = 0; i < userList.size(); i++)
								{
									if (parameters[1].equalsIgnoreCase(userList.get(i)))
									{
										foundUser = true;
										tagList.set(i, this.getAppropriateCode(event, parameters[3]));
										EntityPlayerMP target = (EntityPlayerMP) server.getPlayerEntityByName(parameters[1]);
										if (target != null)
										{
											if (LoMaS_Player.getLoMaSPlayer(target) == null)
											{
												LoMaS_Player.addLoMaSPlayer(target);
											}
											LoMaS_Player.getLoMaSPlayer(target).guildtag = parameters[3];
											SpecializationsMod.sendPlayerAbilitiesToClients(LoMaS_Player.getLoMaSPlayer(target));
										}
									}
								}
							}
							if (foundUser = false)
							{
								LoMaS_Utils.addChatMessage  (sender, "Could not find player with username: " + parameters[1]);
							}
							storePlayerNameData(userList, nameFormatList, messageFormatList, tagList, rankList);
						}
						if (parameters[2].matches(("rank")))
						{
							boolean foundUser = false;
							if (userList.size() > 0)
							{
								for (int i = 0; i < userList.size(); i++)
								{
									if (parameters[1].equalsIgnoreCase(userList.get(i)))
									{
										foundUser = true;
										rankList.set(i, this.getAppropriateCode(event, parameters[3]));
										EntityPlayerMP target = (EntityPlayerMP) server.getPlayerEntityByName(parameters[1]);
										if (target != null)
										{
											if (LoMaS_Player.getLoMaSPlayer(target) == null)
											{
												LoMaS_Player.addLoMaSPlayer(target);
											}
											LoMaS_Player.getLoMaSPlayer(target).rank = parameters[3];
											SpecializationsMod.sendPlayerAbilitiesToClients(LoMaS_Player.getLoMaSPlayer(target));
										}
									}
								}
							}
							if (foundUser = false)
							{
								LoMaS_Utils.addChatMessage  (sender, "Could not find player with username: " + parameters[1]);
							}
							storePlayerNameData(userList, nameFormatList, messageFormatList, tagList, rankList);
						}
					}
					else if (parameters[0].matches("help"))
					{
						if (server.isRemote == false)
						{
							LoMaS_Utils.addChatMessage  (sender, "Syntax includes:");
							LoMaS_Utils.addChatMessage  (sender, "/ccs set <targetname> namecode <code>"); // done
							LoMaS_Utils.addChatMessage  (sender, "/ccs set <targetname> messagecode <code>"); // done
							LoMaS_Utils.addChatMessage  (sender, "/ccs set <targetname> guildtag <tag>"); // done
							LoMaS_Utils.addChatMessage  (sender, "/ccs set <targetname> rank <rank>"); // done
						}
					}
				}
				else
				{
					LoMaS_Utils.addChatMessage  (sender, "Incorrect syntax. Type '/ccs help' for help.");
				}
			}
			else
			{
				LoMaS_Utils.addChatMessage  (sender, "You do not have permission to use this command.");
			}
		}
	}

	@SubscribeEvent
	public void commandSetClassEvent(CommandEvent event)
	{
		if (event.getCommand() instanceof CommandSpecializations) // Replace with new Command
		{
			ICommandSender sender = event.getSender();
			String[] parameters = event.getParameters();
			EntityPlayer entityplayer = null;
			entityplayer = (EntityPlayer) sender.getCommandSenderEntity();
			if (entityplayer != null && LoMaS_Utils.isPlayerAdmin(entityplayer))
			{
				World world = entityplayer.worldObj;
				if (parameters.length > 0)
				{
					if (parameters[0].matches("help"))
					{
						LoMaS_Utils.addChatMessage (entityplayer, "Commands for SCM include: ");
						LoMaS_Utils.addChatMessage (entityplayer, "/scm view <attribute> <player>");
						LoMaS_Utils.addChatMessage (entityplayer, "/scm set <attribute> <player>");
						LoMaS_Utils.addChatMessage (entityplayer, "/scm add <attribute> <player>");
						LoMaS_Utils.addChatMessage (entityplayer, "/scm reset <player>");
						LoMaS_Utils.addChatMessage (entityplayer, "/scm view <attribute>");
						LoMaS_Utils.addChatMessage (entityplayer, "/scm set <attribute>");
						LoMaS_Utils.addChatMessage (entityplayer, "/scm add <attribute>");
						LoMaS_Utils.addChatMessage (entityplayer, " ");
						LoMaS_Utils.addChatMessage (entityplayer, "Attributes Include: ");
						LoMaS_Utils.addChatMessage (entityplayer, "progress");
						LoMaS_Utils.addChatMessage (entityplayer, "level");
						LoMaS_Utils.addChatMessage (entityplayer, "required (view only)");
					}
					else if ((!parameters[0].matches("view") && (parameters.length <= 3)) || (parameters[0].matches("view") && (parameters.length <= 2)))
					{
						if (parameters[0].matches("reset"))
						{
							EntityPlayerMP entityplayermp = (EntityPlayerMP) entityplayer;
							WorldServer server = entityplayermp.getServerWorld();
							EntityPlayerMP target = (EntityPlayerMP) server.getPlayerEntityByName(parameters[1]);
							if (target != null)
							{
								LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(target);
								if (lplayer == null)
								{
									LoMaS_Player.addLoMaSPlayer(target);
									lplayer = LoMaS_Player.getLoMaSPlayer(target);
								}
								System.out.println("Skills reset! Old skills were: " + lplayer.getSkills().toString() + " Player was level " + lplayer.getClassLevel());
								lplayer.setSkills(new LoMaS_Skills());
								SpecializationsMod.sendPlayerAbilitiesToClients(lplayer);
							}
						}
						if (parameters[0].matches("set"))
						{
							if (parameters[1].matches(("level")))
							{
								// entityplayer.setSpecClassLevel(Integer.parseInt(parameters[2]));
								LoMaS_Utils.addChatMessage (entityplayer, "Successfully changed your current level to " + parameters[2]);
								EntityPlayerMP emp = (EntityPlayerMP) entityplayer;
								if (LoMaS_Player.getLoMaSPlayer(emp) == null)
								{
									LoMaS_Player.addLoMaSPlayer(emp);
								}
								LoMaS_Player.getLoMaSPlayer(emp).setSpecClassLevel(Integer.parseInt(parameters[2]));
								SpecializationsMod.sendPlayerAbilitiesToClients(LoMaS_Player.getLoMaSPlayer(emp));
							}
							if (parameters[1].matches(("progress")))
							{
								// entityplayer.setSpecProgress(Float.parseFloat(parameters[2]));
								LoMaS_Utils.addChatMessage (entityplayer, "Successfully changed your current progress to " + parameters[2]);
								EntityPlayerMP emp = (EntityPlayerMP) entityplayer;
								if (LoMaS_Player.getLoMaSPlayer(emp) == null)
								{
									LoMaS_Player.addLoMaSPlayer(emp);
								}
								LoMaS_Player.getLoMaSPlayer(emp).setSpecProgress(Float.parseFloat(parameters[2]));
								SpecializationsMod.sendPlayerAbilitiesToClients(LoMaS_Player.getLoMaSPlayer(emp));
							}
						}
						if (parameters[0].matches("add"))
						{
							if (parameters[1].matches(("level")))
							{
								// entityplayer.addSpecClassLevel(Integer.parseInt(parameters[2]));
								EntityPlayerMP emp = (EntityPlayerMP) entityplayer;
								if (LoMaS_Player.getLoMaSPlayer(emp) == null)
								{
									LoMaS_Player.addLoMaSPlayer(emp);
								}
								LoMaS_Player.getLoMaSPlayer(emp).addSpecClassLevel(Integer.parseInt(parameters[2]));
								SpecializationsMod.sendPlayerAbilitiesToClients(LoMaS_Player.getLoMaSPlayer(emp));
							}
							if (parameters[1].matches(("progress")))
							{
								// entityplayer.addSpecProgress(Float.parseFloat(parameters[2]));
								EntityPlayerMP emp = (EntityPlayerMP) entityplayer;
								if (LoMaS_Player.getLoMaSPlayer(emp) == null)
								{
									LoMaS_Player.addLoMaSPlayer(emp);
								}
								LoMaS_Player.getLoMaSPlayer(emp).addSpecProgress(Float.parseFloat(parameters[2]));
								SpecializationsMod.sendPlayerAbilitiesToClients(LoMaS_Player.getLoMaSPlayer(emp));
							}
						}
						if (parameters[0].matches("view"))
						{
							if (parameters[1].matches(("level")))
							{
								LoMaS_Utils.addChatMessage (entityplayer, "Your current level is (Default): " + LoMaS_Player.getLoMaSPlayer(entityplayer).getClassLevel());
							}
							else if (parameters[1].matches(("progress")))
							{
								LoMaS_Utils.addChatMessage (entityplayer, "Your current progress is (Default): " + LoMaS_Player.getLoMaSPlayer(entityplayer).progress);
							}
							else if (parameters[1].matches(("required")))
							{
								LoMaS_Utils.addChatMessage (entityplayer, "Your required progress is: " + LoMaS_Player.getLoMaSPlayer(entityplayer).getReqProgress());
							}
						}
					}
					else
					{
						EntityPlayerMP entityplayermp = (EntityPlayerMP) entityplayer;
						WorldServer server = entityplayermp.getServerWorld();
						EntityPlayerMP target = (EntityPlayerMP) server.getPlayerEntityByName(parameters[3]);
						if (parameters[0].matches("view"))
						{
							target = (EntityPlayerMP) server.getPlayerEntityByName(parameters[2]);
						}
						if (target != null)
						{
							if (parameters[0].matches("set"))
							{
								if (parameters[1].matches(("level")))
								{
									// entityplayer.setSpecClassLevel(Integer.parseInt(parameters[2]));
									LoMaS_Utils.addChatMessage (entityplayer, "Successfully changed " + target.getDisplayNameString() + "'s current level to " + parameters[2]);
									if (LoMaS_Player.getLoMaSPlayer(target) == null)
									{
										LoMaS_Player.addLoMaSPlayer(target);
									}
									LoMaS_Player.getLoMaSPlayer(target).setSpecClassLevel(Integer.parseInt(parameters[2]));
									SpecializationsMod.sendPlayerAbilitiesToClients(LoMaS_Player.getLoMaSPlayer(target));
								}
								if (parameters[1].matches(("progress")))
								{
									// entityplayer.setSpecProgress(Float.parseFloat(parameters[2]));
									LoMaS_Utils.addChatMessage (entityplayer, "Successfully changed " + target.getDisplayNameString() + "'s current progress to " + parameters[2]);
									if (LoMaS_Player.getLoMaSPlayer(target) == null)
									{
										LoMaS_Player.addLoMaSPlayer(target);
									}
									LoMaS_Player.getLoMaSPlayer(target).setSpecProgress(Float.parseFloat(parameters[2]));
									SpecializationsMod.sendPlayerAbilitiesToClients(LoMaS_Player.getLoMaSPlayer(target));
								}
							}
							if (parameters[0].matches("add"))
							{
								if (parameters[1].matches(("level")))
								{
									// entityplayer.addSpecClassLevel(Integer.parseInt(parameters[2]));
									LoMaS_Utils.addChatMessage (entityplayer, "Successfully added " + parameters[2] + " to " + target.getDisplayNameString() + "'s current level.");
									if (LoMaS_Player.getLoMaSPlayer(target) == null)
									{
										LoMaS_Player.addLoMaSPlayer(target);
									}
									LoMaS_Player.getLoMaSPlayer(target).addSpecClassLevel(Integer.parseInt(parameters[2]));
									SpecializationsMod.sendPlayerAbilitiesToClients(LoMaS_Player.getLoMaSPlayer(target));
								}
								if (parameters[1].matches(("progress")))
								{
									// entityplayer.addSpecProgress(Float.parseFloat(parameters[2]));
									LoMaS_Utils.addChatMessage (entityplayer, "Successfully added " + parameters[2] + " to " + target.getDisplayNameString() + "'s current progress.");
									if (LoMaS_Player.getLoMaSPlayer(target) == null)
									{
										LoMaS_Player.addLoMaSPlayer(target);
									}
									LoMaS_Player.getLoMaSPlayer(target).addSpecProgress(Float.parseFloat(parameters[2]));
									SpecializationsMod.sendPlayerAbilitiesToClients(LoMaS_Player.getLoMaSPlayer(target));
								}
							}
							if (parameters[0].matches("view"))
							{
								if (parameters[1].matches(("level")))
								{
									if (LoMaS_Player.getLoMaSPlayer(target) == null)
									{
										LoMaS_Player.addLoMaSPlayer(target);
									}
									LoMaS_Utils.addChatMessage (entityplayer, target.getDisplayNameString() + "'s current level is (Default): " + LoMaS_Player.getLoMaSPlayer(target).getClassLevel());
								}
								else if (parameters[1].matches(("progress")))
								{
									if (LoMaS_Player.getLoMaSPlayer(target) == null)
									{
										LoMaS_Player.addLoMaSPlayer(target);
									}
									LoMaS_Utils.addChatMessage (entityplayer, target.getDisplayNameString() + "'s current progress is (Default): " + LoMaS_Player.getLoMaSPlayer(target).progress);
								}
								else if (parameters[1].matches(("required")))
								{
									if (LoMaS_Player.getLoMaSPlayer(target) == null)
									{
										LoMaS_Player.addLoMaSPlayer(target);
									}
									LoMaS_Utils.addChatMessage (entityplayer, target.getDisplayNameString() + "'s required progress is: " + LoMaS_Player.getLoMaSPlayer(target).getReqProgress());
								}
							}
						}
						else
						{
							LoMaS_Utils.addChatMessage (entityplayer, "Unknown player: " + parameters[3]);
						}
					}
				}
			}
			else
			{
				LoMaS_Utils.addChatMessage (entityplayer, "You do not have permission to use this command.");
			}
		}
	}

	public String getAppropriateCode(CommandEvent event, String input)
	{
		if (event.getParameters()[3].equalsIgnoreCase("white"))
		{
			return "\u00a7f";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("black"))
		{
			return "\u00a70";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("dark blue") || event.getParameters()[3].equalsIgnoreCase("darkblue"))
		{
			return "\u00a71";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("dark green") || event.getParameters()[3].equalsIgnoreCase("darkgreen"))
		{
			return "\u00a72";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("dark aqua") || event.getParameters()[3].equalsIgnoreCase("darkaqua"))
		{
			return "\u00a73";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("dark red") || event.getParameters()[3].equalsIgnoreCase("darkred"))
		{
			return "\u00a74";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("purple"))
		{
			return "\u00a75";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("gold"))
		{
			return "\u00a76";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("gray"))
		{
			return "\u00a77";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("dark gray") || event.getParameters()[3].equalsIgnoreCase("darkgray"))
		{
			return "\u00a78";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("blue"))
		{
			return "\u00a79";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("green"))
		{
			return "\u00a7a";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("aqua"))
		{
			return "\u00a7b";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("red"))
		{
			return "\u00a7c";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("light purple") || event.getParameters()[3].equalsIgnoreCase("lightpurple"))
		{
			return "\u00a7d";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("yellow"))
		{
			return "\u00a7e";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("chaos"))
		{
			return "\u00a7k";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("bold"))
		{
			return "\u00a7l";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("strike") || event.getParameters()[3].equalsIgnoreCase("strikethrough"))
		{
			return "\u00a7m";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("underline"))
		{
			return "\u00a7n";
		}
		else if (event.getParameters()[3].equalsIgnoreCase("italic"))
		{
			return "\u00a7o";
		}
		else if (event.getParameters()[2].equalsIgnoreCase("guildtag") || event.getParameters()[2].equalsIgnoreCase("rank"))
		{
			if (event.getParameters()[3] == "" || event.getParameters()[3] == null || event.getParameters()[3] == "normal" || event.getParameters()[3] == "default" || event.getParameters()[3] == "none")
			{
				return "";
			}
			else
			{
				return "[" + event.getParameters()[3] + "]";
			}
		}
		else if (event.getParameters()[3].equalsIgnoreCase("default") || event.getParameters()[3].equalsIgnoreCase("normal"))
		{
			return "\u00a7r";
		}
		else
		{
			LoMaS_Utils.addChatMessage(event.getSender(), "This is not a valid codeformat. Setting to default.");
			return "\u00a7r";
		}
	}

	public static void storePlayerNameData(ArrayList<String> nameList, ArrayList<String> colorList, ArrayList<String> messageList, ArrayList<String> tagList, ArrayList<String> rankList)
	{
		String fileName = "chatconfig.txt";
		ArrayList<String> output = new ArrayList<String>();
		for (int i = 0; i < nameList.size(); i++)
		{
			output.add(nameList.get(i) + ", " + colorList.get(i) + ", " + messageList.get(i) + ", " + tagList.get(i) + ", " + rankList.get(i) + "\r\n");
		}
		BufferedWriter out;
		try
		{
			out = new BufferedWriter(new FileWriter(fileName, false));
			for (String lol : output)
			{
				out.write(lol);
			}
			out.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	public static void getPlayerNameData()
	{
		File fileName = new File("chatconfig.txt");
		ArrayList<String> value = new ArrayList<String>();
		userList.clear();
		nameFormatList.clear();
		messageFormatList.clear();
		tagList.clear();
		rankList.clear();
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line;
			int i = 0;
			while ((line = br.readLine()) != null)
			{
				String[] info = line.split(",");
				String lineName = info[0].trim();
				String lineNameCode = info[1].trim();
				String lineMessageCode = info[2].trim();
				String lineGuildTag = info[3].trim();
				String lineRank = info[4].trim();
				userList.add(i, lineName);
				nameFormatList.add(i, lineNameCode);
				messageFormatList.add(i, lineMessageCode);
				tagList.add(i, lineGuildTag);
				rankList.add(i, lineRank);
				i++;
			}
			br.close();
		}
		catch (IOException e)
		{
			System.err.println("Couldn't find chatconfig.txt at " + fileName.getAbsolutePath());
		}
	}
}