package bitevo.Zetal.LoMaS.Specializations.Inventory.Slot;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Inventory.SpecContainerRepair;

//private
public class SlotCore extends Slot
{
    final World theWorld;

    final BlockPos blockPos;
    
    final int slotID;

    /** The anvil this slot belongs to. */
    final SpecContainerRepair anvil;

    //private
    public SlotCore(SpecContainerRepair specContainerRepair, IInventory par2IInventory, int par3, int par4, int par5, World par6World, BlockPos pos)
    {
        super(par2IInventory, par3, par4, par5);
        this.anvil = specContainerRepair;
        this.theWorld = par6World;
        this.blockPos = pos;
        this.slotID = par3;
    }

    /**
     * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
     */
    public boolean isItemValid(ItemStack par1ItemStack)
    {
        if(this.slotID == 0 && par1ItemStack.getItem().equals(SpecializationsMod.EMERALDHEART))
        {
        	return true;
        }
        else if(this.slotID == 1 && par1ItemStack.getItem().equals(SpecializationsMod.IRONHEART))
        {
        	return true;
        }
        return false;
    }

    /**
     * Return whether this slot's stack can be taken from this slot.
     */
    @Override
    public boolean canTakeStack(EntityPlayer par1EntityPlayer)
    {
		return true;
    }

    public void onPickupFromSlot(EntityPlayer par1EntityPlayer, ItemStack par2ItemStack)
    {
    }
}
