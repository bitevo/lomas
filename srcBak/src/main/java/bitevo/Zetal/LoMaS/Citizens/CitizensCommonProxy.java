package bitevo.Zetal.LoMaS.Citizens;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import bitevo.Zetal.LoMaS.Citizens.Entities.EntityPigman;
import bitevo.Zetal.LoMaS.Citizens.GUI.GuiCitizenInventory;
import bitevo.Zetal.LoMaS.Citizens.Inventory.ContainerCitizenInventory;

public class CitizensCommonProxy implements IGuiHandler
{
	public CitizensCommonProxy()
	{
	}

	public void init()
	{
	}

	public void registerRenderInformation()
	{
		// unused server side. -- see ClientProxy for implementation
	}

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		switch(ID)
		{
			case 0:
				return new ContainerCitizenInventory(player.inventory, ((EntityPigman) world.getEntityByID(x)).inventory, ((EntityPigman) world.getEntityByID(x)), player);
		}
		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		switch(ID)
		{
			case 0:
				return new GuiCitizenInventory(player.inventory, ((EntityPigman) world.getEntityByID(x)).inventory, ((EntityPigman) world.getEntityByID(x)));
		}
		return null;
	}
}
