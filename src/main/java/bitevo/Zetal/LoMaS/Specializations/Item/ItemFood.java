package bitevo.Zetal.LoMaS.Specializations.Item;

import java.util.List;

import bitevo.Zetal.LoMaS.Specializations.FoodInitializer;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;

public class ItemFood extends net.minecraft.item.ItemFood
{
	/** If this field is true, the food can be consumed even if the player don't need to eat. */
	private boolean alwaysEdible;

	public ItemFood(int amount, float saturation, boolean isWolfFood)
	{
		super(amount, saturation, isWolfFood);
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
	}

	public ItemFood(int amount, boolean isWolfFood)
	{
		this(amount, 0.6F, isWolfFood);
	}

	@Override
	public String getItemStackDisplayName(ItemStack stack)
	{
		int i = stack.getItemDamage() & 3;
		if (i < 3)
		{
			return (FoodInitializer.qualities[i] + " " + super.getItemStackDisplayName(stack));
		}
		else
		{
			return super.getItemStackDisplayName(stack) + ".ERR";
		}
	}

	@Override
	public void getSubItems(Item par1, CreativeTabs par2CreativeTabs, List par3List)
	{
		for (int j = 0; j < FoodInitializer.qualities.length; ++j)
		{
			par3List.add(new ItemStack(this, 1, j));
		}
	}

	/**
	 * Called when the player finishes using this Item (E.g. finishes eating.). Not called when the player stops using the Item before the action is complete.
	 */
	@Override
	public ItemStack onItemUseFinish(ItemStack stack, World worldIn, EntityLivingBase entityLiving)
	{
		--stack.stackSize;

		if (entityLiving instanceof EntityPlayer)
		{
			EntityPlayer entityplayer = (EntityPlayer) entityLiving;
			int healAmount = this.getHealAmount(stack);
			float satMod = this.getSaturationModifier(stack);
			switch (stack.getItemDamage())
			{
				case 0:
					healAmount = Math.min(healAmount, Math.max(4 - entityplayer.getFoodStats().getFoodLevel(), 0));
					break;
				case 1:
					healAmount = Math.min(healAmount, Math.max(12 - entityplayer.getFoodStats().getFoodLevel(), 0));
					break;
				case 2:
					break;
			}
			entityplayer.getFoodStats().addStats(healAmount, satMod);
			worldIn.playSound((EntityPlayer) null, entityplayer.posX, entityplayer.posY, entityplayer.posZ, SoundEvents.ENTITY_PLAYER_BURP, SoundCategory.PLAYERS, 0.5F, worldIn.rand.nextFloat() * 0.1F + 0.9F);
			this.onFoodEaten(stack, worldIn, entityplayer);
			entityplayer.addStat(StatList.getObjectUseStats(this));
		}

		return stack;
	}

	/**
	 * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
	 */
	@Override
	public ActionResult<ItemStack> onItemRightClick(ItemStack itemStackIn, World worldIn, EntityPlayer playerIn, EnumHand hand)
	{
		if (playerIn.canEat(this.alwaysEdible))
		{
			switch (itemStackIn.getMetadata())
			{
				case 0:
					if (playerIn.getFoodStats().getFoodLevel() >= 4)
					{
						return new ActionResult(EnumActionResult.FAIL, itemStackIn);
					}
					break;
				case 1:
					if (playerIn.getFoodStats().getFoodLevel() >= 12)
					{
						return new ActionResult(EnumActionResult.FAIL, itemStackIn);
					}
					break;
				case 2:
					break;
			}
			playerIn.setActiveHand(hand);
			return new ActionResult(EnumActionResult.SUCCESS, itemStackIn);
		}
		return new ActionResult(EnumActionResult.FAIL, itemStackIn);
	}
}