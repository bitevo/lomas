package bitevo.Zetal.LoMaS.Specializations.Items.ItemBlock;

import net.minecraft.block.Block;
import net.minecraft.block.BlockPlanks;
import net.minecraft.item.ItemMultiTexture;
import net.minecraft.item.ItemStack;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;

import com.google.common.base.Function;

public class ItemSapling extends ItemMultiTexture
{
	public ItemSapling(Block block)
	{
		super(block, SpecializationsMod.SAPLING, new Function()
		{
            public String apply(ItemStack stack)
            {
                return BlockPlanks.EnumType.byMetadata(stack.getMetadata()).getUnlocalizedName();
            }
            public Object apply(Object p_apply_1_)
            {
                return this.apply((ItemStack)p_apply_1_);
            }
		});
		this.setUnlocalizedName("sapling");
	}
	
	public String getUnlocalizedName(ItemStack stack)
    {
        return super.getUnlocalizedName() + "." + (String)this.nameFunction.apply(stack);
    }
}
