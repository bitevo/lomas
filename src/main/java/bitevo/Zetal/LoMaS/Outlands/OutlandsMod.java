package bitevo.Zetal.LoMaS.Outlands;

import bitevo.Zetal.LoMaS.Outlands.Block.BlockWrath;
import bitevo.Zetal.LoMaS.Outlands.Handler.EventHookContainerClass;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = OutlandsMod.MODID, name = "Outlands", version = OutlandsMod.VERSION)
public class OutlandsMod
{
	public static final String MODID = "lomas_outlands";
	public static final String VERSION = "0.1";
	@SidedProxy(clientSide = "bitevo.Zetal.LoMaS.Outlands.OutlandsClientProxy", serverSide = "bitevo.Zetal.LoMaS.Outlands.OutlandsCommonProxy")
	public static OutlandsCommonProxy proxy = new OutlandsCommonProxy();

	public static BlockWrath WRATH = (BlockWrath) (new BlockWrath()).setUnlocalizedName("wrath").setHardness(0.0F).setCreativeTab(CreativeTabs.DECORATIONS);

	/**
	 * Measured in blocks. /16 to get chunks.
	 * 12800x12800 blocks = 800x800 chunks
	 * Extents of the map are actually 2xmapSizex2xmapSize, since the map extends into both negative and positive in equal amounts
	 */
	public static int mapSize = 12800 / 2;
	
	@EventHandler
	public void init(FMLInitializationEvent event)
	{
		MinecraftForge.EVENT_BUS.register(new EventHookContainerClass());
		
		LoMaS_Utils.registerBlock(this.WRATH);
	}

	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		LoMaS_Utils.OUT = true;
		proxy.init();
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
	}
}
