package bitevo.Zetal.LoMaS.TileWrappers;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import bitevo.Zetal.LoMaS.TileWrappers.Command.CommandWrapper;
import bitevo.Zetal.LoMaS.TileWrappers.Handler.TWEventHandler;
import bitevo.Zetal.LoMaS.TileWrappers.Handler.TWTickHandler;
import bitevo.Zetal.LoMaS.TileWrappers.Message.TileWrapperMessage;
import bitevo.Zetal.LoMaS.TileWrappers.Message.TileWrapperMessageHandler;
import net.minecraft.command.ServerCommandManager;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

@Mod(modid = TileWrapperMod.MODID, name = "TileWrappers", version = TileWrapperMod.VERSION)
public class TileWrapperMod
{
	public static final String MODID = "lomas_tilewrappers";
	public static final String VERSION = "0.1";
	@SidedProxy(clientSide = "bitevo.Zetal.LoMaS.TileWrappers.TWClientProxy", serverSide = "bitevo.Zetal.LoMaS.TileWrappers.TWCommonProxy")
	public static TWCommonProxy proxy = new TWCommonProxy();
	public static SimpleNetworkWrapper snw;
	public static MinecraftServer server;

	@EventHandler
	public void init(FMLInitializationEvent event)
	{
		FMLCommonHandler.instance().bus().register(new TWTickHandler());
		MinecraftForge.EVENT_BUS.register(new TWEventHandler());
	}

	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		LoMaS_Utils.TIW = true;
		proxy.init();
		snw = NetworkRegistry.INSTANCE.newSimpleChannel(MODID);
		snw.registerMessage(TileWrapperMessageHandler.class, TileWrapperMessage.class, LoMaS_Utils.getNextPacketID(), Side.CLIENT);
		snw.registerMessage(TileWrapperMessageHandler.class, TileWrapperMessage.class, LoMaS_Utils.getCurrentPacketID(), Side.SERVER);
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
	}

	@EventHandler
	public void serverStart(FMLServerStartingEvent event)
	{
		this.server = event.getServer();
		ServerCommandManager scm = (ServerCommandManager) event.getServer().getCommandManager();
		scm.registerCommand(new CommandWrapper());
	}
}
