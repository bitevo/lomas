package bitevo.Zetal.LoMaS.Economy.Inventory;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import bitevo.Zetal.LoMaS.Economy.EconomyMod;
import bitevo.Zetal.LoMaS.Economy.Item.ItemCoin;

//private
public class SlotCoin extends Slot
{
	public static ResourceLocation slotBG = new ResourceLocation(EconomyMod.modid, "textures/items/CC.png");
	//private
    public SlotCoin(IInventory inventory, int id, int x, int y)
    {
        super(inventory, id, x, y);
    }

    /**
     * Returns the maximum stack size for a given slot (usually the same as getInventoryStackLimit(), but 1 in the case
     * of armor slots)
     */
    public int getSlotStackLimit()
    {
        return 64;
    }
    
    @SideOnly(Side.CLIENT)
    public ResourceLocation getBackgroundLocation()
    {
        return this.slotBG;
    }

    /**
     * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
     */
    public boolean isItemValid(ItemStack par1ItemStack)
    {
        Item item = (par1ItemStack == null ? null : par1ItemStack.getItem());
        boolean isCoin = item instanceof ItemCoin;
        return isCoin;
    }
}
