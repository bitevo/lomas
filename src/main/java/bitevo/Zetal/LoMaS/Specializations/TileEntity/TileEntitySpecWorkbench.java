package bitevo.Zetal.LoMaS.Specializations.TileEntity;

import java.util.UUID;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.Handler.FMLHandler;
import bitevo.Zetal.LoMaS.Specializations.Inventory.SpecContainerWorkbench;
import bitevo.Zetal.LoMaS.Specializations.Inventory.Slot.SlotCrafting;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEntitySpecWorkbench extends TileEntity implements ITickable, ISidedInventory
{
	/**
	 * The ItemStacks that hold the items currently being used in the workbench 0 is the result
	 */
	private EntityPlayer usingPlayer = null;
	private ItemStack[] workbenchSlots = new ItemStack[9 + 1];
	private int curCraftTime;
	private int reqCraftTime;
	private String workbenchCustomName;
	
	private UUID owner;
	private LoMaS_Player lPlayer;

	/**
	 * Returns the number of slots in the inventory.
	 */
	@Override
	public int getSizeInventory()
	{
		return this.workbenchSlots.length;
	}

	/**
	 * Returns the stack in slot i
	 */
	@Override
	public ItemStack getStackInSlot(int index)
	{
		return this.workbenchSlots[index];
	}

	public void setStackInSlot(ItemStack stack, int index)
	{
		this.workbenchSlots[index] = stack;
	}

	@Override
	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newState)
	{
		return !oldState.getBlock().equals(newState.getBlock());
	}

	/**
	 * Removes from an inventory slot (first arg) up to a specified number (second arg) of items and returns them in a new stack.
	 */
	@Override
	public ItemStack decrStackSize(int index, int count)
	{
		if (this.getStackInSlot(index) != null)
		{
			ItemStack itemstack;

			if (this.getStackInSlot(index).stackSize <= count)
			{
				itemstack = this.getStackInSlot(index);
				this.setStackInSlot(null, index);
				if (this.getUsingPlayer() != null && index != 0)
				{
					this.onCraftMatrixChanged(this, this.getUsingPlayer());
				}
				return itemstack;
			}
			else
			{
				itemstack = this.getStackInSlot(index).splitStack(count);

				if (this.getStackInSlot(index).stackSize == 0)
				{
					this.setStackInSlot(null, index);
				}

				if (this.getUsingPlayer() != null && index != 0)
				{
					this.onCraftMatrixChanged(this, this.getUsingPlayer());
				}
				return itemstack;
			}
		}
		else
		{
			return null;
		}
	}

	/**
	 * When some containers are closed they call this on each slot, then drop whatever it returns as an EntityItem - like when you close a workbench GUI.
	 */
	@Override
	public ItemStack removeStackFromSlot(int index)
	{
		if (this.getStackInSlot(index) != null)
		{
			ItemStack itemstack = this.getStackInSlot(index);
			this.setStackInSlot(null, index);
			return itemstack;
		}
		else
		{
			return null;
		}
	}

	/**
	 * Sets the given item stack to the specified slot in the inventory (can be crafting or armor sections).
	 */
	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		boolean flag = stack != null && stack.isItemEqual(this.getStackInSlot(index));// && ItemStack.areItemStackTagsEqual(stack, this.getStackInSlot(index));
		this.setStackInSlot(stack, index);

		if (stack != null && stack.stackSize > this.getInventoryStackLimit())
		{
			stack.stackSize = this.getInventoryStackLimit();
		}

		if (stack != null && stack.getItem() != null && index == 0 && !flag)
		{
			this.reqCraftTime = 80;
			float magMod = 1.0f;
			magMod -= this.lPlayer.getSkills().getPercentMagnitudeFromName("Crafting spd");
			if(FMLHandler.isTool(stack) || stack.getItem() instanceof ItemArmor)
			{
				magMod -= this.lPlayer.getSkills().getPercentMagnitudeFromName("Tool and Armor spd");
				if(FMLHandler.isTool(stack))
				{
					magMod -= this.lPlayer.getSkills().getPercentMagnitudeFromName("Tool craft spd");
				}
			}
			else
			{
				magMod -= this.lPlayer.getSkills().getPercentMagnitudeFromName("Non-tool/armor craft spd");
			}
			this.reqCraftTime = (int) (this.reqCraftTime * magMod);
			this.curCraftTime = 0;
			this.markDirty();
		}

		if (this.getUsingPlayer() != null && index != 0)
		{
			this.onCraftMatrixChanged(this, this.getUsingPlayer());
		}
	}

	/**
	 * Gets the name of this command sender (usually username, but possibly "Rcon")
	 */
	@Override
	public String getName()
	{
		return this.hasCustomName() ? this.workbenchCustomName : "container.workbench";
	}

	/**
	 * Returns true if this thing is named
	 */
	@Override
	public boolean hasCustomName()
	{
		return this.workbenchCustomName != null && this.workbenchCustomName.length() > 0;
	}

	public void setCustomInventoryName(String p_145951_1_)
	{
		this.workbenchCustomName = p_145951_1_;
	}

	@Override
	public void readFromNBT(NBTTagCompound compound)
	{
		super.readFromNBT(compound);
		NBTTagList nbttaglist = compound.getTagList("Items", 10);
		this.workbenchSlots = new ItemStack[this.getSizeInventory()];

		for (int i = 0; i < nbttaglist.tagCount(); ++i)
		{
			NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
			byte b0 = nbttagcompound1.getByte("Slot");

			if (b0 >= 0 && b0 < this.workbenchSlots.length)
			{
				this.setStackInSlot(ItemStack.loadItemStackFromNBT(nbttagcompound1), b0);
			}
		}

		this.curCraftTime = compound.getShort("craftTime");
		this.reqCraftTime = compound.getShort("craftTimeTotal");
		if (compound.hasKey("owner"))
		{
			try
			{
				this.setOwner(UUID.fromString(compound.getString("owner")));
			}
			catch (Exception e)
			{
				this.setOwner(null);
			}
		}
		if(this.getOwner() != null)
		{
			this.lPlayer = LoMaS_Player.getLoMaSPlayer(getOwner());
		}

		if (compound.hasKey("CustomName", 8))
		{
			this.workbenchCustomName = compound.getString("CustomName");
		}
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound)
	{
		super.writeToNBT(compound);
		if (this.getOwner() != null)
		{
			try
			{
				compound.setString("owner", getOwner().toString());
			}
			catch (Exception e)
			{
			}
		}
		compound.setShort("craftTime", (short) this.curCraftTime);
		compound.setShort("craftTimeTotal", (short) this.reqCraftTime);
		NBTTagList nbttaglist = new NBTTagList();

		for (int i = 0; i < this.workbenchSlots.length; ++i)
		{
			if (this.getStackInSlot(i) != null)
			{
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte) i);
				this.getStackInSlot(i).writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}

		compound.setTag("Items", nbttaglist);

		if (this.hasCustomName())
		{
			compound.setString("CustomName", this.workbenchCustomName);
		}
		return compound;
	}

	public void onCraftMatrixChanged(IInventory inventoryIn, EntityPlayer player)
	{
		this.setInventorySlotContents(0, SpecContainerWorkbench.findSpecRecipe(SlotCrafting.getDummyInventory(player, this, this.worldObj), this.worldObj, player));
	}

	/**
	 * Returns the maximum stack size for a inventory slot. Seems to always be 64, possibly will be extended. *Isn't this more of a set than a get?*
	 */
	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}

	public boolean isCrafting()
	{
		return this.curCraftTime > 0;
	}

	@SideOnly(Side.CLIENT)
	public static boolean isCrafting(IInventory p_174903_0_)
	{
		return p_174903_0_.getField(0) > 0;
	}

	/**
	 * Updates the JList with a new model.
	 */
	@Override
	public void update()
	{
		boolean flag = this.isCrafting();
		boolean flag1 = false;

		if (flag && this.curCraftTime < this.reqCraftTime)
		{
			this.curCraftTime++;
		}

		if (flag1)
		{
			this.markDirty();
		}

		if (!this.worldObj.isRemote)
		{
			if (this.usingPlayer == null)
			{
				if (!this.worldObj.isRemote)
				{
					for (int i = 1; i < this.getSizeInventory(); ++i)
					{
						ItemStack itemstack = this.removeStackFromSlot(i);

						if (itemstack != null)
						{
							this.worldObj.spawnEntityInWorld(new EntityItem(this.worldObj, this.pos.getX(), this.pos.getY(), this.pos.getZ(), itemstack));
						}
					}
				}
				this.clear();
			}
		}
	}

	/**
	 * Do not make give this method the name canInteractWith because it clashes with Container
	 */
	@Override
	public boolean isUseableByPlayer(EntityPlayer player)
	{
		return this.worldObj.getTileEntity(this.pos) != this ? false : player.getDistanceSq((double) this.pos.getX() + 0.5D, (double) this.pos.getY() + 0.5D, (double) this.pos.getZ() + 0.5D) <= 64.0D;
	}

	@Override
	public void openInventory(EntityPlayer player)
	{
	}

	@Override
	public void closeInventory(EntityPlayer player)
	{
	}

	public String getGuiID()
	{
		return "minecraft:workbench";
	}

	@Override
	public int getField(int id)
	{
		switch (id)
		{
			case 0:
				return this.curCraftTime;
			case 1:
				return this.reqCraftTime;
			default:
				return 0;
		}
	}

	@Override
	public void setField(int id, int value)
	{
		switch (id)
		{
			case 0:
				this.curCraftTime = value;
				break;
			case 1:
				this.reqCraftTime = value;
				break;
		}
	}

	@Override
	public int getFieldCount()
	{
		return 2;
	}

	@Override
	public void clear()
	{
		for (int i = 0; i < this.workbenchSlots.length; ++i)
		{
			this.setStackInSlot(null, i);
		}
	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		return false;
	}

	@Override
	public ITextComponent getDisplayName()
	{
		return new TextComponentTranslation("Workbench");
	}

	@Override
	public int[] getSlotsForFace(EnumFacing side)
	{
		return null;
	}

	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction)
	{
		return false;
	}

	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction)
	{
		return false;
	}

	public EntityPlayer getUsingPlayer()
	{
		return usingPlayer;
	}

	public void setUsingPlayer(EntityPlayer usingPlayer)
	{
		this.usingPlayer = usingPlayer;
	}

	public int getCurCraftTime()
	{
		return curCraftTime;
	}

	public void setCurCraftTime(int curCraftTime)
	{
		this.curCraftTime = curCraftTime;
	}

	public int getReqCraftTime()
	{
		return reqCraftTime;
	}

	public void setReqCraftTime(int reqCraftTime)
	{
		this.reqCraftTime = reqCraftTime;
	}

	public UUID getOwner()
	{
		return owner;
	}

	public void setOwner(UUID owner)
	{
		this.owner = owner;
		if(owner != null)
		{
			this.lPlayer = LoMaS_Player.getLoMaSPlayer(owner);
		}
	}

	public LoMaS_Player getlPlayer()
	{
		return lPlayer;
	}

	public void setlPlayer(LoMaS_Player lPlayer)
	{
		this.lPlayer = lPlayer;
	}
}