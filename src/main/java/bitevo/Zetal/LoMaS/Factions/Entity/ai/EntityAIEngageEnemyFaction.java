package bitevo.Zetal.LoMaS.Factions.Entity.ai;

import java.util.List;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Faction;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Shared.Entity.EntityFactionMember;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IEntityOwnable;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.pathfinding.Path;
import net.minecraft.pathfinding.PathPoint;
import net.minecraft.scoreboard.Team;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;

public class EntityAIEngageEnemyFaction extends EntityAIBase
{
	/** The entity that this task belongs to */
	protected final EntityFactionMember taskOwner;
	/** If true, EntityAI targets must be able to be seen (cannot be blocked by walls) to be suitable targets. */
	protected boolean shouldCheckSight;
	/** When nearbyOnly is true: 0 -> No target, but OK to search; 1 -> Nearby target found; 2 -> Target too far. */
	private int targetSearchStatus;
	/** When nearbyOnly is true, this throttles target searching to avoid excessive pathfinding. */
	private int targetSearchDelay;
	/**
	 * If @shouldCheckSight is true, the number of ticks before the interruption of this AITask when the entity does't see the target
	 */
	private int targetUnseenTicks;
	protected EntityLivingBase target;
	protected int unseenMemoryTicks;

	public EntityAIEngageEnemyFaction(EntityFactionMember creature, boolean checkSight)
	{
		this.unseenMemoryTicks = 60;
		this.taskOwner = creature;
		this.shouldCheckSight = checkSight;
	}

	/**
	 * Returns whether an in-progress EntityAIBase should continue executing
	 */
	@Override
	public boolean continueExecuting()
	{
		EntityLivingBase entitylivingbase = this.taskOwner.getAttackTarget();

		if (entitylivingbase == null)
		{
			entitylivingbase = this.target;
		}

		if (entitylivingbase == null)
		{
			return false;
		}
		else if (!entitylivingbase.isEntityAlive())
		{
			return false;
		}
		else
		{
			Team team = this.taskOwner.getTeam();
			Team team1 = entitylivingbase.getTeam();

			if (team != null && team1 == team)
			{
				return false;
			}
			else
			{
				double d0 = this.getTargetDistance();

				if (this.taskOwner.getDistanceSqToEntity(entitylivingbase) > d0 * d0)
				{
					return false;
				}
				else
				{
					if (this.shouldCheckSight)
					{
						if (this.taskOwner.getEntitySenses().canSee(entitylivingbase))
						{
							this.targetUnseenTicks = 0;
						}
						else if (++this.targetUnseenTicks > this.unseenMemoryTicks)
						{
							return false;
						}
					}

					if (entitylivingbase instanceof EntityPlayer && ((EntityPlayer) entitylivingbase).capabilities.disableDamage)
					{
						return false;
					}
					else
					{
						this.taskOwner.setAttackTarget(entitylivingbase);
						return true;
					}
				}
			}
		}
	}

	protected double getTargetDistance()
	{
		IAttributeInstance iattributeinstance = this.taskOwner.getEntityAttribute(SharedMonsterAttributes.FOLLOW_RANGE);
		return iattributeinstance == null ? 16.0D : iattributeinstance.getAttributeValue();
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute()
	{
		List<EntityLivingBase> list = this.taskOwner.worldObj.<EntityLivingBase> getEntitiesWithinAABB(EntityLivingBase.class, this.getTargetableArea(this.getTargetDistance()));

		if (list.isEmpty())
		{
			return false;
		}
		else
		{
			for (EntityLivingBase target : list)
			{
				if (this.isSuitableTarget(taskOwner, target, false, true))
				{
					this.target = target;
					return true;
				}
			}
			return false;
		}
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting()
	{
		this.targetSearchStatus = 0;
		this.targetSearchDelay = 0;
		this.targetUnseenTicks = 0;
		this.taskOwner.setAttackTarget(this.target);
	}

	/**
	 * Resets the task
	 */
	@Override
	public void resetTask()
	{
		this.taskOwner.setAttackTarget((EntityLivingBase) null);
		this.target = null;
	}

	/**
	 * A static method used to see if an entity is a suitable target through a number of checks.
	 */
	public static boolean isSuitableTarget(EntityFactionMember attacker, EntityLivingBase target, boolean includeInvincibles, boolean checkSight)
	{
		if (target == null)
		{
			return false;
		}
		else if (target == attacker)
		{
			return false;
		}
		else if (!target.isEntityAlive())
		{
			return false;
		}
		else if (!attacker.canAttackClass(target.getClass()))
		{
			return false;
		}
		else if (attacker.isOnSameTeam(target))
		{
			return false;
		}
		else
		{
			// If target is owned by taskOwner's owner (currently useless)
			if (attacker instanceof IEntityOwnable && ((IEntityOwnable) attacker).getOwnerId() != null)
			{
				if (target instanceof IEntityOwnable && ((IEntityOwnable) attacker).getOwnerId().equals(target.getUniqueID()))
				{
					return false;
				}

				if (target == ((IEntityOwnable) attacker).getOwner())
				{
					return false;
				}
			} // If target is an invincible player, return false
			else if (target instanceof EntityPlayer && !includeInvincibles && ((EntityPlayer) target).capabilities.disableDamage)
			{
				return false;
			} // If target is a player
			else if (target instanceof EntityPlayer)
			{
				EntityPlayer playerTarget = (EntityPlayer) target;
				LoMaS_Faction targetFaction = LoMaS_Faction.getLoMaSFaction(LoMaS_Player.getLoMaSPlayer(playerTarget).faction);
				if (targetFaction != null)
				{
					LoMaS_Faction attackerFaction = LoMaS_Faction.getLoMaSFaction(attacker.getFaction());
					if (attackerFaction != null)
					{
						if (attackerFaction.getId().equals(targetFaction.getId()) || attackerFaction.getAlliedFactions().contains(targetFaction.getId()))
						{
							return false;
						}
						else if (attackerFaction.getEnemyFactions().contains(targetFaction.getId()))
						{
							return !checkSight || attacker.getEntitySenses().canSee(target);
						}
						else
						{
							return false;
						}
					}
				}
			} // If target is a FactionMember
			else if (target instanceof EntityFactionMember)
			{
				EntityFactionMember factionTarget = (EntityFactionMember) target;
				LoMaS_Faction targetFaction = LoMaS_Faction.getLoMaSFaction(factionTarget.getFaction());
				if (targetFaction != null)
				{
					LoMaS_Faction attackerFaction = LoMaS_Faction.getLoMaSFaction(attacker.getFaction());
					if (attackerFaction != null)
					{
						if (attackerFaction.getId().equals(targetFaction.getId()) || attackerFaction.getAlliedFactions().contains(targetFaction.getId()))
						{
							return false;
						}
						else if (attackerFaction.getEnemyFactions().contains(targetFaction.getId()))
						{
							return !checkSight || attacker.getEntitySenses().canSee(target);
						}
						else
						{
							return false;
						}
					}
				}
			}
			else if (target instanceof IMob)
			{
				return !checkSight || attacker.getEntitySenses().canSee(target);
			}

			return false;
		}
	}

	/**
	 * A method used to see if an entity is a suitable target through a number of checks. Args : entity, canTargetInvinciblePlayer
	 */
	protected boolean isSuitableTarget(EntityFactionMember target, boolean includeInvincibles)
	{
		if (!isSuitableTarget(this.taskOwner, target, includeInvincibles, this.shouldCheckSight))
		{
			return false;
		}
		else if (!this.taskOwner.isWithinHomeDistanceFromPosition(new BlockPos(target)))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	/**
	 * Checks to see if this entity can find a short path to the given target.
	 */
	private boolean canEasilyReach(EntityLivingBase target)
	{
		this.targetSearchDelay = 10 + this.taskOwner.getRNG().nextInt(5);
		Path path = this.taskOwner.getNavigator().getPathToEntityLiving(target);

		if (path == null)
		{
			return false;
		}
		else
		{
			PathPoint pathpoint = path.getFinalPathPoint();

			if (pathpoint == null)
			{
				return false;
			}
			else
			{
				int i = pathpoint.xCoord - MathHelper.floor_double(target.posX);
				int j = pathpoint.zCoord - MathHelper.floor_double(target.posZ);
				return (double) (i * i + j * j) <= 2.25D;
			}
		}
	}

	protected AxisAlignedBB getTargetableArea(double targetDistance)
	{
		return this.taskOwner.getEntityBoundingBox().expand(targetDistance, 4.0D, targetDistance);
	}
}
