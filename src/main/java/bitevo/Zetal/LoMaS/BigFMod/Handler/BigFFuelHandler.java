package bitevo.Zetal.LoMaS.BigFMod.Handler;

import bitevo.Zetal.LoMaS.BigFMod.Item.ItemStick;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.IFuelHandler;

public class BigFFuelHandler implements IFuelHandler
{

	@Override
	public int getBurnTime(ItemStack fuel)
	{
		if (fuel.getItem() instanceof ItemStick)
		{
			return 100;
		}
		else if (LoMaS_Utils.SCM && fuel.getItem().equals(Item.getItemFromBlock(bitevo.Zetal.LoMaS.Specializations.SpecializationsMod.SAPLING)))
		{
			return 100;
		}
		return 0;
	}

}
