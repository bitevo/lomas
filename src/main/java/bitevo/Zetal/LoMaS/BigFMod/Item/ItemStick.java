package bitevo.Zetal.LoMaS.BigFMod.Item;

import java.util.List;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemStick extends Item
{
	public static final int[] stickMetas = new int[] { 0, 1, 2, 3, 4, 5 };
	public static final String[] stickNames = new String[] { "Oak", "Spruce", "Birch", "Jungle", "Acacia", "Dark Oak" };
	public static final String[] imageNames = new String[] { "oakStick", "spruceStick", "birchStick", "jungleStick", "acaciaStick", "darkoakStick" };

	public ItemStick()
	{
		super();
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
		this.setCreativeTab(CreativeTabs.MATERIALS);
	}

	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack)
	{
		int i = MathHelper.clamp_int(par1ItemStack.getItemDamage(), 0, 5);
		return super.getUnlocalizedName() + "." + stickNames[i];
	}

	@Override
	public String getItemStackDisplayName(ItemStack par1ItemStack)
	{
		int i = MathHelper.clamp_int(par1ItemStack.getItemDamage(), 0, 5);
		return ("" + stickNames[i] + " Wood Stick");
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void getSubItems(Item par1, CreativeTabs par2CreativeTabs, List par3List)
	{
		par3List.add(new ItemStack(par1, 1, 0));
		par3List.add(new ItemStack(par1, 1, 1));
		par3List.add(new ItemStack(par1, 1, 2));
		par3List.add(new ItemStack(par1, 1, 3));
		par3List.add(new ItemStack(par1, 1, 4));
		par3List.add(new ItemStack(par1, 1, 5));
	}
}
