package bitevo.Zetal.LoMaS.Specializations.Message;

import io.netty.buffer.ByteBuf;

import java.util.UUID;

import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class SpecializationMessage implements IMessage
{
	public float progress = 0.0f;
	public String specClass = "basic";
	public int classlevel = 0;
	public UUID username;
	public String rank;
	public String guildtag;
	public boolean levelDisplay;

	public SpecializationMessage()
	{
	}

	public SpecializationMessage(float progress, String specClass, int classlevel, UUID username, String rank, String guildtag, boolean levelDisplay)
	{
		this.progress = progress;
		this.specClass = specClass;
		this.classlevel = classlevel;
		this.username = username;
		this.rank = rank;
		this.guildtag = guildtag;
		this.levelDisplay = levelDisplay;
		if(this.specClass == null || this.username == null || this.rank == null || this.guildtag == null)
		{
			System.err.println("There was a null value in the packet!");
		}
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.progress = buf.readFloat();
		this.specClass = ByteBufUtils.readUTF8String(buf);
		this.classlevel = buf.readInt();
		this.username = new UUID(buf.readLong(), buf.readLong());
		//this.username = UUID.fromString(ByteBufUtils.readUTF8String(buf));
		this.rank = ByteBufUtils.readUTF8String(buf);
		this.guildtag = ByteBufUtils.readUTF8String(buf);
		this.levelDisplay = buf.readBoolean();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeFloat(this.progress);
		ByteBufUtils.writeUTF8String(buf, this.specClass);
		buf.writeInt(this.classlevel);
		buf.writeLong(this.username.getMostSignificantBits());
		buf.writeLong(this.username.getLeastSignificantBits());
		//ByteBufUtils.writeUTF8String(buf, this.username.toString());
		ByteBufUtils.writeUTF8String(buf, this.rank);
		ByteBufUtils.writeUTF8String(buf, this.guildtag);
		buf.writeBoolean(this.levelDisplay);
	}
}
