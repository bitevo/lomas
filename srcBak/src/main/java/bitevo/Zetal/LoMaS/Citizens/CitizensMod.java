package bitevo.Zetal.LoMaS.Citizens;

import java.util.Random;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import bitevo.Zetal.LoMaS.Citizens.Block.BlockCitizenChest;
import bitevo.Zetal.LoMaS.Citizens.Entities.EntityPigman;
import bitevo.Zetal.LoMaS.Citizens.Handler.CitizenTickHandler;
import bitevo.Zetal.LoMaS.Citizens.Item.ItemDeed;
import bitevo.Zetal.LoMaS.Citizens.TileEntity.TileEntityCitizenChest;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = CitizensMod.modid, name = "Citizens", version = "0.1")
public class CitizensMod
{
	public static final String modid = "lomas_citizens";
	@SidedProxy(clientSide = "bitevo.Zetal.LoMaS.Citizens.CitizensClientProxy", serverSide = "bitevo.Zetal.LoMaS.Citizens.CitizensCommonProxy")
	public static CitizensCommonProxy proxy = new CitizensCommonProxy();
	@Instance(modid)
	public static CitizensMod instance;
	public static Item deed = new ItemDeed().setUnlocalizedName("deed");
	public static BlockCitizenChest citizen_chest = (BlockCitizenChest) new BlockCitizenChest().setUnlocalizedName("citizen_chest");
	public static SoundEvent citizen_say, citizen_death;

	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		NetworkRegistry.INSTANCE.registerGuiHandler(instance, proxy);
		registerModEntityWithEgg(EntityPigman.class, LoMaS_Utils.getNextEntityID(), "Pigman", 0x996633, 0xFF33CC);
		LoMaS_Utils.registerItem(deed);
		LoMaS_Utils.registerBlock(citizen_chest);
		GameRegistry.registerTileEntity(TileEntityCitizenChest.class, "TileEntityCitizenChest");
		doRecipes();
		FMLCommonHandler.instance().bus().register(new CitizenTickHandler());
		citizen_say = registerSound("mob.citizen.say");
		citizen_death = registerSound("mob.citizen.death");
	}
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent evt)
	{
		proxy.registerRenderInformation();
	}
	
	private static SoundEvent registerSound(String soundName) 
	{
		final ResourceLocation soundID = new ResourceLocation(CitizensMod.modid, soundName);
		return GameRegistry.register(new SoundEvent(soundID).setRegistryName(soundID));
	}

	public void doRecipes()
	{
		GameRegistry.addShapelessRecipe(new ItemStack(this.deed), Items.PAPER, Items.FLINT, Items.GOLD_NUGGET);
	}

	public void registerModEntityWithEgg(Class parEntityClass, int eID, String parEntityName, int parEggColor, int parEggSpotsColor)
	{
		EntityRegistry.registerModEntity(parEntityClass, parEntityName, eID, CitizensMod.instance, 80, 3, false);
		EntityRegistry.registerEgg(parEntityClass, parEggColor, parEggSpotsColor);
	}
	
	/**
	 * Default offset should be 0.
	 * @param offset
	 * @return
	 */
	public static Item getRandomOre(float offset)
	{
		Item i = Item.getItemFromBlock(Blocks.COBBLESTONE);
		Random rand = new Random();
		float f = (rand.nextFloat() * 100) + offset;
		if(f <= 30)
		{
			return Item.getItemFromBlock(Blocks.COBBLESTONE);
		}
		else if(f <= 65)
		{
			return Items.COAL;
		}
		else if(f <= 75)
		{
			return Item.getItemFromBlock(Blocks.IRON_ORE);
		}
		else if(f <= 85)
		{
			return Item.getItemFromBlock(Blocks.GOLD_ORE);
		}
		else if(f <= 95)
		{
			return Item.getItemFromBlock(Blocks.REDSTONE_ORE);
		}
		else if(f < 100)
		{
			return Item.getItemFromBlock(Blocks.DIAMOND_ORE);
		}
		else if(f >= 100)
		{
			return Item.getItemFromBlock(Blocks.EMERALD_ORE);
		}
		return i;
	}
}
