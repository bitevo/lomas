package bitevo.Zetal.LoMaS.Shared.SkillTree;

public class SkillNode
{
	private String id;
	private String name;
	private float positionX;
	private float positionY;
	private String shape;
	private float size;
	private float effect;
	private String image;
	private String description;
	
	public SkillNode(String id)
	{
		this.id = id;
	}
	
	public SkillNode(String id, String name, float positionX, float positionY)
	{
		this(id);
		this.name = name;
		this.positionX = positionX;
		this.positionY = positionY;
	}

	public SkillNode(String id, String name, float positionX, float positionY, String shape, float size, String image)
	{
		this(id, name, positionX, positionY);
		this.shape = shape;
		this.size = size;
		this.image = image;
	}

	@Override
	public String toString()
	{
		return "SkillNode [getId()=" + getId() + ", getName()=" + getName() + ", getPositionX()=" + getPositionX() + ", getPositionY()=" + getPositionY() + ", getShape()=" + getShape() + ", getSize()=" + getSize() + ", getImage()=" + getImage() + ", getEffect()=" + getEffect() + ", getDescription()=" + getDescription() + "]";
	}

	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public float getPositionX()
	{
		return positionX;
	}
	public void setPositionX(float positionX)
	{
		this.positionX = positionX;
	}
	public float getPositionY()
	{
		return positionY;
	}
	public void setPositionY(float positionY)
	{
		this.positionY = positionY;
	}
	public String getShape()
	{
		return shape;
	}
	public void setShape(String shape)
	{
		this.shape = shape;
	}
	public float getSize()
	{
		return size;
	}
	public void setSize(float size)
	{
		this.size = size;
	}
	public String getImage()
	{
		return image;
	}
	public void setImage(String image)
	{
		this.image = image;
	}

	public float getEffect()
	{
		return effect;
	}

	public void setEffect(float effect)
	{
		this.effect = effect;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}
}
