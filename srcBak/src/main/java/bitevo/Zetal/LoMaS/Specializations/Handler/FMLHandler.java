package bitevo.Zetal.LoMaS.Specializations.Handler;

import io.netty.channel.local.LocalAddress;

import java.net.InetSocketAddress;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockBeacon;
import net.minecraft.block.BlockCauldron;
import net.minecraft.block.BlockPistonBase;
import net.minecraft.block.BlockRailBase;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemFlintAndSteel;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemMinecart;
import net.minecraft.item.ItemShears;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.ItemCraftedEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerRespawnEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent.ClientConnectedToServerEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.PolyZones.PolyZonesMod;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.GUI.GuiIngameSpec;
import bitevo.Zetal.LoMaS.Specializations.Inventory.InventoryPlayer;
import bitevo.Zetal.LoMaS.Specializations.Inventory.SpecPlayerContainer;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemArrow;

public class FMLHandler
{
	@SubscribeEvent
	public void playerConnected(PlayerLoggedInEvent event)
	{
		if (event.player.worldObj != null && !event.player.worldObj.isRemote)
		{
			EntityPlayer player = event.player;
			LoMaS_Player.addLoMaSPlayer(player);
			if (player != null && LoMaS_Player.getLoMaSPlayer(player) != null)
			{
				// this.setPlayerCurrentHealth(player);
				FMLHandler.setPlayerMaxHealth(player);
				// MoveSpeed
				if (player.capabilities != null)
				{
					SCMEventHandler.setPlayerWalkSpeed(player);
				}
				LoMaS_Player.getLoMaSPlayer(player).sendPlayerAbilities();
				LoMaS_Player.getLoMaSPlayer(player).notYet = true;
			}
		}
	}

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void clientPlayerConnected(ClientConnectedToServerEvent event)
	{
		if (event.getManager().getRemoteAddress() instanceof LocalAddress)
		{

			PolyZonesMod.ip = "localhost";
		}
		else
		{
			InetSocketAddress s = (InetSocketAddress) event.getManager().getRemoteAddress();
			PolyZonesMod.ip = s.getAddress().getHostAddress();
		}
		
		if(Minecraft.getMinecraft().thePlayer != null)
		{
			Minecraft.getMinecraft().ingameGUI = new GuiIngameSpec(Minecraft.getMinecraft());
		}
		System.out.println(PolyZonesMod.ip);
	}

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void playerClientRespawn(PlayerRespawnEvent event)
	{
		if (event.player.worldObj.isRemote)
		{
			Minecraft.getMinecraft().ingameGUI = new GuiIngameSpec(Minecraft.getMinecraft());
		}

		LoMaS_Player.addLoMaSPlayer(event.player);
		FMLHandler.setPlayerMaxHealth(event.player);
		event.player.getFoodStats().setFoodLevel(5);
		// this.setPlayerCurrentHealth(event.player);
	}

	@SubscribeEvent
	@SideOnly(Side.SERVER)
	public void playerServerRespawn(PlayerRespawnEvent event)
	{
		EntityPlayer player = event.player;

		if (!(player.inventory instanceof InventoryPlayer))
		{
			InventoryPlayer newInv = new InventoryPlayer(player);
			player.inventory = newInv;
		}

		player.getFoodStats().setFoodLevel(5);
	}

	@SubscribeEvent
	public void onCrafted(ItemCraftedEvent event)
	{
		ItemStack crafted = event.crafting;
		EntityPlayer player = event.player;
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
		if ((crafted.getItem().equals(SpecializationsMod.IRONHEART) || crafted.getItem().equals(SpecializationsMod.EMERALDHEART)) && !player.worldObj.isRemote)
		{
			ItemStack glassStack = new ItemStack(Items.GLASS_BOTTLE, 4, 0);
			if (!player.inventory.addItemStackToInventory(glassStack))
			{
				player.worldObj.spawnEntityInWorld(new EntityItem(player.worldObj, (double) player.posX + 0.5D, (double) player.posY + 1.5D, (double) player.posZ + 0.5D, glassStack));
			}
			else if (player instanceof EntityPlayerMP)
			{
				((EntityPlayerMP) player).sendContainerToPlayer(player.inventoryContainer);
			}
		}

		float progressEarned = this.getProgressForStack(crafted);
		float exhaustion = this.getExhaustionForStack(crafted);
		this.damageCraftedStack(crafted, player);

		player.addExhaustion(exhaustion);
		if (lplayer.specClass.equalsIgnoreCase("blacksmith"))
		{
			if (!player.capabilities.isCreativeMode && progressEarned > 0.00f)
			{
				lplayer.addSpecProgress(progressEarned);
				Random rand = new Random();
				player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
			}
		}
	}

	public static void damageCraftedStack(ItemStack crafted, EntityPlayer player)
	{
		if (crafted.getItem() instanceof ItemTool)
		{
			if (!player.worldObj.isRemote)
			{
				SpecializationsMod.setDamagedItem(crafted, player, true);
			}
			player.inventory.inventoryChanged = true;
		}
		if (crafted.getItem() instanceof ItemHoe)
		{
			if (!player.worldObj.isRemote)
			{
				SpecializationsMod.setDamagedItem(crafted, player, true);
			}
			player.inventory.inventoryChanged = true;
		}
		else if (crafted.getItem() instanceof ItemSword)
		{
			if (!player.worldObj.isRemote)
			{
				SpecializationsMod.setDamagedItem(crafted, player, true);
			}
			player.inventory.inventoryChanged = true;
		}
		else if (crafted.getItem() instanceof ItemArmor)
		{
			if (!player.worldObj.isRemote)
			{
				SpecializationsMod.setDamagedItem(crafted, player, true);
			}
			player.inventory.inventoryChanged = true;
		}
		else if (crafted.getItem().equals(SpecializationsMod.BOW))
		{
			if (!player.worldObj.isRemote)
			{
				SpecializationsMod.setDamagedItem(crafted, player, true);
			}
			player.inventory.inventoryChanged = true;
		}
	}

	public static float getExhaustionForStack(ItemStack crafted)
	{
		float exhaustion = 0.25f;

		if (crafted.getItem() instanceof ItemTool)
		{
			exhaustion += 12.0f;
		}
		if (crafted.getItem() instanceof ItemHoe)
		{
			exhaustion += 12.0f;
		}
		else if (crafted.getItem() instanceof ItemSword)
		{
			exhaustion += 12.0f;
		}
		else if (crafted.getItem() instanceof ItemArmor)
		{
			exhaustion += 16.0f;
		}
		else if (crafted.getItem() instanceof ItemArrow)
		{
			exhaustion += 3.0f;
		}
		else if (crafted.getItem().equals(SpecializationsMod.BOW))
		{
			exhaustion += 12.0f;
		}
		else if (crafted.getItem() instanceof ItemMinecart)
		{
			exhaustion += 6.0f;
		}
		else if (crafted.getItem() instanceof ItemBlock)
		{
			exhaustion += 0.25f;
		}
		return exhaustion;
	}

	public static float getProgressForStack(ItemStack crafted)
	{
		float progressEarned = 0.0f;
		if (crafted.getItem() instanceof ItemTool)
		{
			ItemTool tool = (ItemTool) crafted.getItem();
			if (tool.getToolMaterialName().equals(ToolMaterial.WOOD.toString()))
			{
				progressEarned = 0.25f;
			}
			else if (tool.getToolMaterialName().equals(SpecializationsMod.HARDWOOD_TOOL.toString()))
			{
				progressEarned = 0.25f;
			}
			else if (tool.getToolMaterialName().equals(ToolMaterial.STONE.toString()))
			{
				progressEarned = 0.75f;
			}
			else if (tool.getToolMaterialName().equals(ToolMaterial.IRON.toString()))
			{
				progressEarned = 1.175f;
			}
			else if (tool.getToolMaterialName().equals(ToolMaterial.GOLD.toString()))
			{
				progressEarned = 1.55f;
			}
			else if (tool.getToolMaterialName().equals(ToolMaterial.DIAMOND.toString()))
			{
				progressEarned = 10.0f;
			}
			else if (tool.getToolMaterialName().equals(SpecializationsMod.OBSIDIAN_TOOL.toString()))
			{
				progressEarned = 10.0f;
			}

			if (!(tool instanceof ItemSpade))
			{
				progressEarned = progressEarned * 3.0f;
			}
		}
		else if (crafted.getItem() instanceof ItemHoe)
		{
			ItemHoe hoe = (ItemHoe) crafted.getItem();
			if (hoe.getMaterialName().equals(ToolMaterial.WOOD.toString()))
			{
				progressEarned = 0.5f;
			}
			else if (hoe.getMaterialName().equals(SpecializationsMod.HARDWOOD_TOOL.toString()))
			{
				progressEarned = 0.25f;
			}
			else if (hoe.getMaterialName().equals(ToolMaterial.STONE.toString()))
			{
				progressEarned = 1.5f;
			}
			else if (hoe.getMaterialName().equals(ToolMaterial.IRON.toString()))
			{
				progressEarned = 2.25f;
			}
			else if (hoe.getMaterialName().equals(ToolMaterial.GOLD.toString()))
			{
				progressEarned = 2.55f;
			}
			else if (hoe.getMaterialName().equals(ToolMaterial.DIAMOND.toString()))
			{
				progressEarned = 20.0f;
			}
			else if (hoe.getMaterialName().equals(SpecializationsMod.OBSIDIAN_TOOL.toString()))
			{
				progressEarned = 20.0f;
			}
		}
		else if (crafted.getItem() instanceof ItemSword)
		{
			ItemSword sword = (ItemSword) crafted.getItem();
			if (sword.getToolMaterialName().equals(ToolMaterial.WOOD.toString()))
			{
				progressEarned = 0.5f;
			}
			else if (sword.getToolMaterialName().equals(SpecializationsMod.HARDWOOD_TOOL.toString()))
			{
				progressEarned = 0.25f;
			}
			else if (sword.getToolMaterialName().equals(ToolMaterial.STONE.toString()))
			{
				progressEarned = 1.5f;
			}
			else if (sword.getToolMaterialName().equals(ToolMaterial.IRON.toString()))
			{
				progressEarned = 2.25f;
			}
			else if (sword.getToolMaterialName().equals(ToolMaterial.GOLD.toString()))
			{
				progressEarned = 2.55f;
			}
			else if (sword.getToolMaterialName().equals(ToolMaterial.DIAMOND.toString()))
			{
				progressEarned = 20.0f;
			}
			else if (sword.getToolMaterialName().equals(SpecializationsMod.OBSIDIAN_TOOL.toString()))
			{
				progressEarned = 20.0f;
			}
		}
		else if (crafted.getItem() instanceof ItemArmor)
		{
			ItemArmor armor = (ItemArmor) crafted.getItem();
			if (armor.getArmorMaterial() == ArmorMaterial.LEATHER)
			{
				progressEarned = 7.5f;
			}
			else if (armor.getArmorMaterial() == ArmorMaterial.IRON)
			{
				progressEarned = 12.5f;
			}
			else if (armor.getArmorMaterial() == ArmorMaterial.GOLD)
			{
				progressEarned = 15.0f;
			}
			else if (armor.getArmorMaterial() == ArmorMaterial.DIAMOND)
			{
				progressEarned = 35.0f;
			}
			else if (armor.getArmorMaterial() == SpecializationsMod.OBSIDIAN_ARMOR)
			{
				progressEarned = 35.0f;
			}
		}
		else if (crafted.getItem() instanceof ItemArrow)
		{
			if (crafted.getItemDamage() == 0)
			{
				progressEarned = 0.5f;
			}
			else if (crafted.getItemDamage() == 1)
			{
				progressEarned = 1.5f;
			}
			else if (crafted.getItemDamage() == 2)
			{
				progressEarned = 2.25f;
			}
			else if (crafted.getItemDamage() == 3)
			{
				progressEarned = 2.55f;
			}
			else if (crafted.getItemDamage() == 4)
			{
				progressEarned = 20.0f;
			}
		}
		else if (crafted.getItem().equals(SpecializationsMod.BOW))
		{
			progressEarned = 1.5f;
		}
		else if (crafted.getItem() instanceof ItemMinecart)
		{
			progressEarned = 5.0f;
		}
		else if (crafted.getItem() instanceof ItemShears)
		{
			progressEarned = 2.25f;
		}
		else if (crafted.getItem() instanceof ItemFlintAndSteel)
		{
			progressEarned = 1.125f;
		}
		else if (crafted.getItem() instanceof ItemBlock)
		{
			Block block = Block.getBlockFromItem(crafted.getItem());
			if (block instanceof BlockRailBase)
			{
				progressEarned = 7.0f;
			}
			else if (block instanceof BlockBeacon)
			{
				progressEarned = 25.0f;
			}
			else if (block instanceof BlockCauldron)
			{
				progressEarned = 8.225f;
			}
			else if (block instanceof BlockPistonBase)
			{
				progressEarned = 2.0f;
			}
			else
			{
				progressEarned = 0.0f;
			}
		}
		else
		{
			progressEarned = 0.0f;
		}
		return progressEarned;
	}

	public static void setPlayerCurrentHealth(EntityPlayer player)
	{
		float health = getMaxHealth(player);

		setPlayerMaxHealth(player);
		player.setHealth(health);
	}

	public static void setPlayerMaxHealth(EntityPlayer player)
	{
		float health = getMaxHealth(player);

		player.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(health);

		if (player.getHealth() > health)
		{
			player.setHealth(health);
		}
	}

	public static float getMaxHealth(EntityPlayer player)
	{
		float health = 16;
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
		if (lplayer.specClass.equalsIgnoreCase("warrior"))
		{
			health = health + 1;
			if (lplayer.classlevel >= 2)
			{
				health = health + 1;
			}
			if (lplayer.classlevel >= 4)
			{
				health = health + 1;
			}
			if (lplayer.classlevel >= 8)
			{
				health = health + 1;
			}
		}
		else if (lplayer.specClass.equalsIgnoreCase("stealth"))
		{
			health = health - 4;
			if (lplayer.classlevel >= 7)
			{
				health = health + 2;
			}
		}
		return health;
	}
}
