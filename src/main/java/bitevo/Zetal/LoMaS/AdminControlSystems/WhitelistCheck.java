package bitevo.Zetal.LoMaS.AdminControlSystems;

import java.util.Calendar;
import java.util.GregorianCalendar;

import net.minecraft.server.MinecraftServer;

/**
 * @author pwebbz
 */
public class WhitelistCheck implements Runnable
{
	public boolean doneAlready = false;
	public MinecraftServer server;

	@Override
	public void run()
	{
		while (true)
		{
			Calendar calendar = new GregorianCalendar();
			int minute = calendar.get(Calendar.MINUTE);
			int second = calendar.get(Calendar.SECOND);
			int millisecond = calendar.get(Calendar.MILLISECOND);
			if (minute == 0 && second == 0 && millisecond <= 20)
			{ // 1:00, 2:00, 3:00, 4:00 (every hour)
				if (!doneAlready)
				{
					server.getCommandManager().executeCommand(server, "/whitelist reload");
					doneAlready = true;
				}
			}
			else
			{
				doneAlready = false;
			}
		}
	}
}