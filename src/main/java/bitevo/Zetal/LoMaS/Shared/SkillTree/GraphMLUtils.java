package bitevo.Zetal.LoMaS.Shared.SkillTree;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

public class GraphMLUtils
{
	public static HashMap<String, SkillNode> nodes = new HashMap<String, SkillNode>();
	public static HashMap<String, SkillConnection> edges = new HashMap<String, SkillConnection>();
	public static InputStream graphFile = GraphMLUtils.class.getResourceAsStream("/assets/lomas_scm/treeFinal.graphml");
	
	public static int[] minMaxX = new int[]{Integer.MAX_VALUE,Integer.MIN_VALUE};
	public static int[] minMaxY = new int[]{Integer.MAX_VALUE,Integer.MIN_VALUE};
	
	private static String magnitudeKey = "";
	private static String descriptionKey = "";
	
	public static void main(String[] args)
	{
		readGraphML(graphFile);
		System.out.println(nodes);
		System.out.println(edges);
		System.out.println(getConnectedNodes(nodes.get("n135")));
	}
	
	public static void readGraphML(InputStream graphFile)
	{
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder;
		/**
		 * The coordinates for this need to be positive, otherwise the GUI fucks up.
		 */
		minMaxX = new int[]{Integer.MAX_VALUE,Integer.MIN_VALUE};
		minMaxY = new int[]{Integer.MAX_VALUE,Integer.MIN_VALUE};
		try
		{
			documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = documentBuilder.parse(graphFile);

			Element childNode = (Element) GraphMLUtils.getChildNodeFromKey(document, "key", "attr.name", "Magnitude");
			magnitudeKey = childNode.getAttribute("id");
			childNode = (Element) GraphMLUtils.getChildNodeFromKey(document, "key", "attr.name", "Descript");
			descriptionKey = childNode.getAttribute("id");
			
			NodeList nList = document.getElementsByTagName("node");
			for(int i = 0; i < nList.getLength(); i++)
			{
				Node node = nList.item(i);
				SkillNode temp = getSkillNodeFromTreeNode(node);
				if(!temp.getShape().equalsIgnoreCase("triangle"))
				{
					nodes.put(temp.getId(), temp);
				}
				
				if(temp.getPositionX() < minMaxX[0])
				{
					minMaxX[0] = (int) temp.getPositionX();
				}
				
				if(temp.getPositionX() > minMaxX[1])
				{
					minMaxX[1] = (int) temp.getPositionX();
				}
				
				if(temp.getPositionY() < minMaxY[0])
				{
					minMaxY[0] = (int) temp.getPositionY();
				}
				
				if(temp.getPositionY() > minMaxY[1])
				{
					minMaxY[1] = (int) temp.getPositionY();
				}
			}
			
			NodeList edgeList = document.getElementsByTagName("edge");
			for(int i = 0; i < edgeList.getLength(); i++)
			{
				Node node = edgeList.item(i);
				SkillConnection temp = getConnectionFromTreeNode(node);
				edges.put(temp.getId(), temp);
			}
		}
		catch (ParserConfigurationException e)
		{
			e.printStackTrace();
		}
		catch (SAXException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static SkillNode getSkillNodeFromTreeNode(Node node)
	{
		// Create the SkillNode object using the provided ID
		SkillNode skillNode = new SkillNode(((Element) node).getAttribute("id"));
		
		// Add Geometry information to the SkillNode
		Element childNode = (Element) getChildNode(node, "y:Geometry");
		float size = Float.parseFloat(childNode.getAttribute("height"));
		skillNode.setSize(size);
		float x = Float.parseFloat(childNode.getAttribute("x"));
		float y = Float.parseFloat(childNode.getAttribute("y"));
		skillNode.setPositionX(x);
		skillNode.setPositionY(y);
		
		// Add shape information to the SkillNode
		childNode = (Element) getChildNode(node, "y:Shape");
		skillNode.setShape(childNode.getAttribute("type"));
		
		// Add detail information to the SkillNode
		childNode = (Element) getChildNode(node, "y:NodeLabel");
		skillNode.setName(childNode.getTextContent().trim());
		
		childNode = (Element) GraphMLUtils.getDataChildNodeFromKey(node, magnitudeKey);
		float magnitude = 0.0f;
		if(childNode != null)
		{
			magnitude = Float.parseFloat(childNode.getTextContent().trim());
			skillNode.setEffect(magnitude);
		}
		
		childNode = (Element) GraphMLUtils.getDataChildNodeFromKey(node, descriptionKey);
		if(childNode != null)
		{
			skillNode.setDescription(childNode.getTextContent().trim().replace("$_Magnitude_$", ("" + magnitude).replaceFirst("\\.0*$|(\\.\\d*?)0+$", "$1")));
		}
		
		return skillNode;
	}
	
	public static SkillConnection getConnectionFromTreeNode(Node node)
	{
		Element ele = (Element) node;
		
		// Create the SkillConnection object using the provided ID
		SkillConnection skillCon = new SkillConnection(ele.getAttribute("id"), ele.getAttribute("source"), ele.getAttribute("target"));
		
		// Add style information to the SkillConnection
		Element childNode = (Element) getChildNode(node, "y:LineStyle");
		skillCon.setStyle("color:"+childNode.getAttribute("color"));
		skillCon.setStyle(skillCon.getStyle()+"|type:"+childNode.getAttribute("type"));
		skillCon.setStyle(skillCon.getStyle()+"|width:"+childNode.getAttribute("width"));
		childNode = (Element) getChildNode(node, "y:BendStyle");
		skillCon.setStyle(skillCon.getStyle()+"|smoothed:"+childNode.getAttribute("smoothed"));
		
		return skillCon;
	}
	
	public static Node getDataChildNodeFromKey(Node node, String key)
	{
		return GraphMLUtils.getChildNodeFromKey(node, "data", "key", key);
	}
	
	public static Node getChildNodeFromKey(Document node, String tag, String keyName, String keyValue)
	{
		NodeList tempList = node.getElementsByTagName(tag);
		for(int i = 0; i < tempList.getLength(); i++)
		{
			if(((Element) tempList.item(i)).getAttribute(keyName).equals(keyValue))
			{
				return tempList.item(i);
			}
		}
		return null;
	}
	
	public static Node getChildNodeFromKey(Node node, String tag, String keyName, String keyValue)
	{
		Element e = (Element) node;
		NodeList tempList = e.getElementsByTagName(tag);
		for(int i = 0; i < tempList.getLength(); i++)
		{
			if(((Element) tempList.item(i)).getAttribute(keyName).equals(keyValue))
			{
				return tempList.item(i);
			}
		}
		return null;
	}
	
	public static Node getChildNode(Node node, String tag)
	{
		Element e = (Element) node;
		NodeList tempList = e.getElementsByTagName(tag);
		return tempList.item(0);
	}
	
	public static ArrayList<SkillNode> getConnectedNodes(SkillNode node)
	{
		String nodeID = node.getId();
		ArrayList<SkillNode> ret = new ArrayList<SkillNode>();
		for(SkillConnection edge : edges.values())
		{
			String sourceId = edge.getSourceNode();
			String targetId = edge.getTargetNode();
			if(nodeID.equals(sourceId))
			{
				ret.add(nodes.get(targetId));
			}
			else if(nodeID.equals(targetId))
			{
				ret.add(nodes.get(sourceId));
			}
		}
		return ret;
	}
	
	public static SkillNode getNodeFromName(String name)
	{
		for(SkillNode node : GraphMLUtils.nodes.values())
		{
			if(node.getName().equalsIgnoreCase(name))
			{
				return node;
			}
		}
		return null;
	}
	
	public static ArrayList<SkillNode> getNodesFromName(String name)
	{
		ArrayList<SkillNode> nodes = new ArrayList<SkillNode>();
		for(SkillNode node : GraphMLUtils.nodes.values())
		{
			if(node.getName().equalsIgnoreCase(name))
			{
				nodes.add(node);
			}
		}
		return nodes;
	}
	
	public static ArrayList<SkillNode> getNodesNameContainingString(String str)
	{
		ArrayList<SkillNode> nodes = new ArrayList<SkillNode>();
		for(SkillNode node : GraphMLUtils.nodes.values())
		{
			if(node.getName().toLowerCase().contains(str.toLowerCase()))
			{
				nodes.add(node);
			}
		}
		return nodes;
	}
}
