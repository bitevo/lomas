package bitevo.Zetal.LoMaS.Citizens.Entities.ai;

import java.util.Random;

import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import bitevo.Zetal.LoMaS.Citizens.Entities.EntityPigman;

public class EntityAIGoToDesire extends EntityAIBase
{
	public EntityPigman host;
	public Vec3d vec3;
	public int ticks = 0;

	public EntityAIGoToDesire(EntityPigman entityPigman)
	{
		this.host = entityPigman;
        this.setMutexBits(1);
	}

	@Override
	public boolean shouldExecute()
	{
		ticks = ticks + host.getRNG().nextInt(1) + 1;
		if (this.ticks % 120 == 0)
		{
			vec3 = this.findCurrentDesire();
			return true;
		}
		return false;
	}

	public boolean continueExecuting()
	{
		if(this.host.getNavigator().noPath() && this.host.currentDesire != null)
		{
			this.host.updateAITasks();
			if(this.host.currentDesire != null)
			{
				//System.out.println("[Continue] Failed to find the current desire: " + this.host.desires[this.host.getDesire()]);
				this.host.setUnhappyReason("Want " + this.host.desires[this.host.getDesire()].getLocalizedName());
				this.host.setDesire(this.host.getDesire() + 1);
				this.host.setHappiness(this.host.getHappiness() - 3);
				//System.out.println("Giving up :C");
				this.host.currentDesire = null;
			}
			return false;
		}
		else if(this.host.getNavigator().noPath())
		{
			//System.out.println("No Penalty Stoppage: " + this.host.desires[this.host.getDesire()]);
			return false;
		}
		return true;
	}

	@Override
	public void startExecuting()
	{
		if (vec3 == null)
		{
			//System.out.println("[Start] Failed to find the current desire: " + this.host.desires[this.host.getDesire()]);
			this.host.setUnhappyReason("Want " + this.host.desires[this.host.getDesire()].getLocalizedName());
			this.host.setDesire(this.host.getDesire() + 1);
			this.host.setHappiness(this.host.getHappiness() - 3);
			//System.out.println("WTF AM ANGRY");
			this.host.currentDesire = null;
			return;
		}
		else
		{
			//System.out.println("Pathing to desire: " + this.host.desires[this.host.getDesire()]);
			this.host.getNavigator().tryMoveToXYZ(vec3.xCoord, vec3.yCoord, vec3.zCoord, 0.3D);
			this.host.currentDesire = new BlockPos(vec3.xCoord, vec3.yCoord, vec3.zCoord);
		}
	}

	private Vec3d findCurrentDesire()
	{
		Random random = this.host.getRNG();
		BlockPos blockpos = new BlockPos(this.host.posX, this.host.posY, this.host.posZ);

		for (int y = -16; y < 16; ++y)
		{
			for (int x = -16; x < 16; ++x)
			{
				for (int z = -16; z < 16; ++z)
				{
					BlockPos searchPos = blockpos.add(x, y, z);
					if (this.host.worldObj.getBlockState(searchPos).getBlock() == this.host.desires[this.host.getDesire()])
					{
						return new Vec3d((double) searchPos.getX(), (double) searchPos.getY(), (double) searchPos.getZ());
					}
				}
			}
		}

		return null;
	}
}
