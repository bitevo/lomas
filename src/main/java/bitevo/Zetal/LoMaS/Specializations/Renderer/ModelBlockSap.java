package bitevo.Zetal.LoMaS.Specializations.Renderer;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelBlockSap extends ModelBase
{
	ModelRenderer sap;

	public ModelBlockSap()
	{
		textureWidth = 4;
		textureHeight = 9;

		sap = new ModelRenderer(this, 0, 0);
		sap.addBox(-2F, 5F, 8.01F, 4, 9, 0);
		sap.setRotationPoint(0F, 0F, 0F);
		sap.setTextureSize(4, 9);
		sap.mirror = true;
		setRotation(sap, 0F, 0F, 0F);
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		super.render(entity, f, f1, f2, f3, f4, f5);
		setRotationAngles(f, f1, f2, f3, f4, f5, entity);
		sap.render(f5);
	}

	public void renderModel(float f5)
	{
		sap.render(f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
	{
		super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
	}

}
