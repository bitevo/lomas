package bitevo.Zetal.LoMaS.TileWrappers.Handler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Random;

import bitevo.Zetal.LoMaS.TileWrappers.TileWrapper.TileWrapperContainer;
import net.minecraft.world.World;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class TWEventHandler
{
	private Random rand = new Random();
	
	@SubscribeEvent
	public void onWorldSave(WorldEvent.Save event)
	{
		World world = event.getWorld();
		if (!world.isRemote)
		{
			File f = world.getMinecraftServer().getDataDirectory();
			File saveDir = new File(f.getPath() + File.separator + "world" + File.separator + "LoMaS" + File.separator);
			saveDir.mkdir();
			File wrapperFile = new File(saveDir.getPath() + File.separator + event.getWorld().provider.getDimensionType().getName() + ".wrappers");
			try
			{
				wrapperFile.delete();
				wrapperFile.createNewFile();
				FileOutputStream saveOut = new FileOutputStream(wrapperFile);
				ObjectOutputStream saveObj = new ObjectOutputStream(saveOut);

				saveObj.writeObject(TileWrapperContainer.instance);

				saveObj.close();
				saveOut.close();
			}
			catch (Exception e1)
			{
				System.err.println("Failed to save wrapper file at " + wrapperFile.getPath() + " for world " + event.getWorld().provider.getDimensionType() + " " + e1.getMessage());
				e1.printStackTrace();
			}
		}
	}

	@SubscribeEvent
	public void onLoadWorld(WorldEvent.Load event)
	{
		World world = event.getWorld();
		if (!world.isRemote)
		{
			File f = world.getMinecraftServer().getDataDirectory();
			File loadDir = new File(f.getPath() + File.separator + "world" + File.separator + "LoMaS" + File.separator);
			loadDir.mkdir();
			File wrapperFile = new File(loadDir.getPath() + File.separator + event.getWorld().provider.getDimensionType().getName() + ".wrappers");
			try
			{
				FileInputStream loadOut = new FileInputStream(wrapperFile);
				ObjectInputStream loadObj = new ObjectInputStream(loadOut);

				TileWrapperContainer.instance = (TileWrapperContainer) loadObj.readObject();

				loadObj.close();
				loadOut.close();
			}
			catch (Exception e1)
			{
				System.err.println("Failed to load wrapper file at " + wrapperFile.getPath() + " for world " + event.getWorld().provider.getDimensionType() + " " + e1.getMessage());
				e1.printStackTrace();
			}
		}
	}
}
