package bitevo.Zetal.LoMaS.Outlands.Handler;

import java.util.ArrayList;
import java.util.Random;

import bitevo.Zetal.LoMaS.Outlands.OutlandsMod;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.client.event.EntityViewRenderEvent.FogColors;
import net.minecraftforge.client.event.EntityViewRenderEvent.FogDensity;
import net.minecraftforge.client.event.EntityViewRenderEvent.RenderFogEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Name and cast of this class are irrelevant
 */
public class EventHookContainerClass
{
	public ArrayList<EntityPlayer> tempBlockers = new ArrayList<EntityPlayer>();
	public ArrayList<Integer> tempCounters = new ArrayList<Integer>();
	public float enforcedRender = 12;
	public boolean enforceEdges = false;
	
	@SideOnly(Side.CLIENT)
	@SubscribeEvent(priority=EventPriority.NORMAL, receiveCanceled=true)
	public void fogDensity(RenderFogEvent event)
	{
		float xDens = (float) Math.min(Math.pow((Math.abs(event.getEntity().posX) / OutlandsMod.mapSize), 2) - 0.9f, 0.75f);
		float zDens = (float) Math.min(Math.pow((Math.abs(event.getEntity().posZ) / OutlandsMod.mapSize), 2) - 0.9f, 0.75f);
		float dens = Math.max(xDens, zDens);
		if(dens > 0.1f)
		{
			float d = Math.max((0.1f / dens) * (this.enforcedRender * 16), 16);
			//System.out.println(dens + " " + d);
			GlStateManager.setFogDensity(dens);
			GlStateManager.setFogStart(0);
			GlStateManager.setFogEnd(d);
		}
	}
	
	@SideOnly(Side.CLIENT)
	@SubscribeEvent(priority=EventPriority.NORMAL, receiveCanceled=true)
	public void fogColor(FogColors event)
	{
		EntityPlayerSP thePlayer = Minecraft.getMinecraft().thePlayer;
		WorldClient theWorld = Minecraft.getMinecraft().theWorld;
		if(Math.abs(thePlayer.posX) > OutlandsMod.mapSize || Math.abs(thePlayer.posZ) > OutlandsMod.mapSize)
		{
			long packedColor = (int) (Math.abs(thePlayer.chunkCoordX) * 4 + Math.abs(thePlayer.chunkCoordZ) * 4) + 16777216;
			Random rand = new Random(packedColor);
			float r = rand.nextFloat();
			float g = rand.nextFloat();
			float b = rand.nextFloat();
			event.setRed(r);
			event.setGreen(g);
			event.setBlue(b); 
		}
	}
	
	@SubscribeEvent
	public void onEntityMove(LivingUpdateEvent event)
	{
		// System.out.println(event.getEntity());
		if (enforceEdges && (event.getEntity() instanceof EntityPlayer || event.getEntity() instanceof EntityPlayerMP))
		{
			EntityPlayer player = (EntityPlayer) event.getEntity();
			if ((((int) (player.posY - player.getEyeHeight()) == (int) player.posY) && FMLCommonHandler.instance().getSide() == Side.CLIENT) || ((int) (player.posY + player.getEyeHeight()) == (int) (player.posY + 1.62)))
			{
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				if (player.worldObj.provider.getDimension() != -1 && player.worldObj.provider.getDimension() != 1)
				{
					double x = player.posX;
					double z = player.posZ;
					///////////////////////////////////////////////////////////////////////
					if (this.tempBlockers.contains(player))
					{
						int index = this.tempBlockers.indexOf(player);
						this.tempCounters.set(index, this.tempCounters.get(index) - 1);
						double xVel = (x > OutlandsMod.mapSize ? -0.2F : x < -OutlandsMod.mapSize ? 0.2F : 0.0F);
						double yVel = x > OutlandsMod.mapSize || x < -OutlandsMod.mapSize || z > OutlandsMod.mapSize || z < -OutlandsMod.mapSize ? 0.1F : 0.0F;
						double zVel = (z > OutlandsMod.mapSize ? -0.2F : z < -OutlandsMod.mapSize ? 0.2F : 0.0F);
						// player.addVelocity(xVel, 0.0F, zVel);
						player.motionX = xVel != 0 ? xVel : player.motionX;
						player.motionY = yVel != 0 ? yVel : player.motionY;
						player.motionZ = zVel != 0 ? zVel : player.motionZ;
						if (this.tempCounters.get(index) + 1 <= 0)
						{
							this.tempBlockers.remove(index);
							this.tempCounters.remove(index);
						}
					}
					else if ((int) x == OutlandsMod.mapSize)
					{
						LoMaS_Utils.addChatMessage(player, "You cannot go this way. The magical fabric of the world is unstable.");// You have been turned around.");
						this.tempBlockers.add(player);
						this.tempCounters.add(50);
					}
					else if ((int) x == -OutlandsMod.mapSize)
					{
						LoMaS_Utils.addChatMessage(player, "You cannot go this way. The magical fabric of the world is unstable.");// You have been turned around.");
						this.tempBlockers.add(player);
						this.tempCounters.add(50);
					}
					else if ((int) z == -OutlandsMod.mapSize)
					{
						LoMaS_Utils.addChatMessage(player, "You cannot go this way. The magical fabric of the world is unstable.");// You have been turned around.");
						this.tempBlockers.add(player);
						this.tempCounters.add(50);
					}
					else if ((int) z == OutlandsMod.mapSize)
					{
						LoMaS_Utils.addChatMessage(player, "You cannot go this way. The magical fabric of the world is unstable.");// You have been turned around.");
						this.tempBlockers.add(player);
						this.tempCounters.add(50);
					}
					else if (((int) z > OutlandsMod.mapSize || (int) z < -OutlandsMod.mapSize || (int) x > OutlandsMod.mapSize || (int) x < -OutlandsMod.mapSize))
					{
						LoMaS_Utils.addChatMessage(player, "You are outside of the boundaries of the normal world. Magical forces attempt to draw you home.");// You have been turned around.");
						this.tempBlockers.add(player);
						this.tempCounters.add(120);
					}
				}
			}
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		}
	}
}