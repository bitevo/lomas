package bitevo.Zetal.LoMaS.Specializations;

import java.util.LinkedHashMap;

import javax.annotation.Nullable;

import com.google.common.collect.Maps;

import bitevo.Zetal.LoMaS.Specializations.Block.BlockCactus;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockReed;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockStem;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityCustomArrow;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityCustomXPOrb;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityDiamondShard;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityFactionDiamondGolem;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityFactionIronGolem;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntitySpecialFishHook;
import bitevo.Zetal.LoMaS.Specializations.Handler.SpecializationKeyBinds;
import bitevo.Zetal.LoMaS.Specializations.Renderer.BlockSapRenderer;
import bitevo.Zetal.LoMaS.Specializations.Renderer.EntityCustomFX;
import bitevo.Zetal.LoMaS.Specializations.Renderer.SpecializationsRendererFactory;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySapBlock;
import net.minecraft.block.Block;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockModelShapes;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.StateMap;
import net.minecraft.client.renderer.block.statemap.StateMapperBase;
import net.minecraft.client.renderer.color.BlockColors;
import net.minecraft.client.renderer.color.IBlockColor;
import net.minecraft.client.renderer.color.IItemColor;
import net.minecraft.client.renderer.color.ItemColors;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionUtils;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.biome.BiomeColorHelper;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class SCMClientProxy extends SCMCommonProxy
{
	public static Minecraft mc;
	public static String modid;
	public static ResourceLocation[] bowTextures = new ResourceLocation[15];
	public static ResourceLocation[] arrowTextures = new ResourceLocation[5];

	@Override
	public void registerRenderInformation()
	{
		mc = Minecraft.getMinecraft();
		modid = SpecializationsMod.MODID;

		SpecializationsRendererFactory srf = new SpecializationsRendererFactory();

		ClientRegistry.bindTileEntitySpecialRenderer(TileEntitySapBlock.class, new BlockSapRenderer());
		RenderingRegistry.registerEntityRenderingHandler(EntityCustomArrow.class, srf.new ArrowRendererFactory());
		RenderingRegistry.registerEntityRenderingHandler(EntityCustomXPOrb.class, srf.new XPRendererFactory());
		RenderingRegistry.registerEntityRenderingHandler(EntitySpecialFishHook.class, srf.new FishHookRendererFactory());
		RenderingRegistry.registerEntityRenderingHandler(EntityFactionIronGolem.class, srf.new IronGolemRendererFactory());
		RenderingRegistry.registerEntityRenderingHandler(EntityFactionDiamondGolem.class, srf.new DiamondGolemRendererFactory());
		RenderingRegistry.registerEntityRenderingHandler(EntityDiamondShard.class, srf.new DiamondShardRendererFactory());

		init();
	}

	@Override
	public void regiterBlockStateMappers()
	{
		mc.effectRenderer.registerParticle(77, new EntityCustomFX.Factory());
		BlockModelShapes shapes = Minecraft.getMinecraft().getBlockRendererDispatcher().getBlockModelShapes();
		shapes.registerBlockWithStateMapper(FoodInitializer.PUMPKIN_STEM, new StateMapperBase()
		{
			@Override
			protected ModelResourceLocation getModelResourceLocation(IBlockState p_178132_1_)
			{
				LinkedHashMap linkedhashmap = Maps.newLinkedHashMap(p_178132_1_.getProperties());

				if (p_178132_1_.getValue(BlockStem.FACING) != EnumFacing.UP)
				{
					linkedhashmap.remove(BlockStem.AGE);
				}

				return new ModelResourceLocation((ResourceLocation) Block.REGISTRY.getNameForObject(p_178132_1_.getBlock()), this.getPropertyString(linkedhashmap));
			}
		});
		shapes.registerBlockWithStateMapper(FoodInitializer.MELON_STEM, new StateMapperBase()
		{
			@Override
			protected ModelResourceLocation getModelResourceLocation(IBlockState p_178132_1_)
			{
				LinkedHashMap linkedhashmap = Maps.newLinkedHashMap(p_178132_1_.getProperties());

				if (p_178132_1_.getValue(BlockStem.FACING) != EnumFacing.UP)
				{
					linkedhashmap.remove(BlockStem.AGE);
				}

				return new ModelResourceLocation((ResourceLocation) Block.REGISTRY.getNameForObject(p_178132_1_.getBlock()), this.getPropertyString(linkedhashmap));
			}
		});
		shapes.registerBlockWithStateMapper(FoodInitializer.REEDS_BLOCK, (new StateMap.Builder()).ignore(new IProperty[] { BlockReed.AGE }).build());
		shapes.registerBlockWithStateMapper(FoodInitializer.CACTUS, (new StateMap.Builder()).ignore(new IProperty[] { BlockCactus.AGE }).build());
		

		BlockColors blockcolors = Minecraft.getMinecraft().getBlockColors();
        blockcolors.registerBlockColorHandler(new IBlockColor()
        {
            public int colorMultiplier(IBlockState state, @Nullable IBlockAccess worldIn, @Nullable BlockPos pos, int tintIndex)
            {
                return worldIn != null && pos != null ? BiomeColorHelper.getGrassColorAtPos(worldIn, pos) : -1;
            }
        }, new Block[] {FoodInitializer.REEDS_BLOCK});
        blockcolors.registerBlockColorHandler(new IBlockColor()
        {
            public int colorMultiplier(IBlockState state, @Nullable IBlockAccess worldIn, @Nullable BlockPos pos, int tintIndex)
            {
                int i = ((Integer)state.getValue(BlockStem.AGE)).intValue();
                int j = i * 32;
                int k = 255 - i * 8;
                int l = i * 4;
                return j << 16 | k << 8 | l;
            }
        }, new Block[] {FoodInitializer.MELON_STEM, FoodInitializer.PUMPKIN_STEM});
        

		ItemColors itemcolors = Minecraft.getMinecraft().getItemColors();
		itemcolors.registerItemColorHandler(new IItemColor()
        {
            public int getColorFromItemstack(ItemStack stack, int tintIndex)
            {
                return tintIndex > 0 ? -1 : PotionUtils.getPotionColorFromEffectList(PotionUtils.getEffectsFromStack(stack));
            }
        }, new Item[] {SpecializationsMod.COMPACT_POTION, SpecializationsMod.COMPACT_SPLASH_POTION, SpecializationsMod.COMPACT_LINGERING_POTION});
	}

	@Override
	public void init()
	{
		SpecializationsMod.clientStart();
		FMLCommonHandler.instance().bus().register(new SpecializationKeyBinds());
	}
}