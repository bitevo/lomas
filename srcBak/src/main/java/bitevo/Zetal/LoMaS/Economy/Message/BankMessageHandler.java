package bitevo.Zetal.LoMaS.Economy.Message;

import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;

public class BankMessageHandler implements IMessageHandler<BankMessage, IMessage>
{
	@Override
	public IMessage onMessage(BankMessage message, MessageContext ctx) 
	{
		if(ctx.side == Side.CLIENT)
		{
			handleCBankMessage(message);
		}
		return null;
	}

	@SideOnly(Side.CLIENT)
	private void handleCBankMessage(BankMessage packet) 
	{
		LoMaS_Player ep = LoMaS_Player.getLoMaSPlayer(Minecraft.getMinecraft().thePlayer);
		ep.setBankCC(packet.cc);
	}

	private void handleSBankMessage(BankMessage packet) 
	{
	}
}
