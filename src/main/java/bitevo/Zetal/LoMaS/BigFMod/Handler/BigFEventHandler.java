package bitevo.Zetal.LoMaS.BigFMod.Handler;

import java.util.List;
import java.util.Random;

import bitevo.Zetal.LoMaS.BigFMod.BigFMod;
import bitevo.Zetal.LoMaS.BigFMod.Entity.EntityBoat;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockPlanks;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class BigFEventHandler
{
	public static final String[] arrowNames = new String[] { "_wood", "_stone", "_iron", "_gold", "_diamond" };// , "_obsidian"};

	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void breakSpeedEvent(PlayerEvent.BreakSpeed event)
	{
		EntityPlayer player = event.getEntityPlayer();
		ItemStack itemstack = player.getHeldItemMainhand();
		Block block = event.getState().getBlock();
		if (block != null)
		{
			if (BigFMod.requiresProperTool(event.getState()) && block.getBlockHardness(event.getState(), event.getEntity().worldObj, event.getPos()) != 0)
			{
				event.setNewSpeed(0.0f);
			}

			if (itemstack != null)
			{
				Item item = itemstack.getItem();
				if (item instanceof ItemTool)
				{
					ItemTool it = (ItemTool) item;
					if (it.getStrVsBlock(itemstack, block.getDefaultState()) != 1.0f)
					{
						event.setNewSpeed(event.getOriginalSpeed());
					}
				}
			}
		}
	}

	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void harvestCheck(PlayerEvent.HarvestCheck event)
	{
		event.setCanHarvest(true);
	}

	@SubscribeEvent
	public void onDrops(BlockEvent.HarvestDropsEvent event)
	{
		Random rand = new Random();
		if (event.getState().getBlock() == Blocks.LEAVES)
		{
			int meta = Blocks.LEAVES.getMetaFromState(event.getState());
			BlockPlanks.EnumType type = Blocks.LEAVES.getWoodType(meta);
			if (rand.nextInt(30) <= 100)
			{
				event.getDrops().add(new ItemStack(BigFMod.STICK, rand.nextInt(2) + 1, type.getMetadata()));
			}
			if (event.getHarvester() != null)
			{
				// System.out.println("hi" + " " + BigFMod.LEAVESDECAYED.getDefaultState().withProperty(BlockPlanks.VARIANT, type));
				event.getWorld().setBlockState(event.getPos(), BigFMod.LEAVESDECAYED.getDefaultState().withProperty(BlockPlanks.VARIANT, type));
			}
		}
		else if (event.getState().getBlock() == Blocks.LEAVES2)
		{
			int meta = Blocks.LEAVES2.getMetaFromState(event.getState());
			BlockPlanks.EnumType type = Blocks.LEAVES2.getWoodType(meta);
			if (rand.nextInt(30) <= 100)
			{
				event.getDrops().add(new ItemStack(BigFMod.STICK, rand.nextInt(2) + 1, type.getMetadata()));
			}
			if (event.getHarvester() != null)
			{
				// System.out.println("hi" + " " + BigFMod.LEAVESDECAYED.getDefaultState().withProperty(BlockPlanks.VARIANT, type));
				event.getWorld().setBlockState(event.getPos(), BigFMod.LEAVESDECAYED.getDefaultState().withProperty(BlockPlanks.VARIANT, type));
			}
		}
	}

	@SubscribeEvent
	public void playerRightClickOrInteract(PlayerInteractEvent.RightClickBlock event)
	{
		if (event.getWorld().getBlockState(event.getPos()).getBlock().equals(Blocks.BED) && event.getWorld().isDaytime() && event.getEntityPlayer() != null)
		{
			double d0 = 8.0D;
			double d1 = 5.0D;
			List list = event.getWorld().getEntitiesWithinAABB(EntityMob.class, new AxisAlignedBB((double) event.getPos().getX() - d0, (double) event.getPos().getY() - d1, (double) event.getPos().getZ() - d0, (double) event.getPos().getX() + d0, (double) event.getPos().getY() + d1, (double) event.getPos().getZ() + d0));
			if (list.isEmpty())
			{
				if (!event.getWorld().isRemote)
				{
					LoMaS_Utils.addChatMessage(event.getEntityPlayer(), "Your spawn point has been set!");
					event.getEntityPlayer().setSpawnPoint(event.getPos(), false);
					event.setCanceled(true);
				}
			}
		}
	}

	@SubscribeEvent
	public void entityJoinsWorld(EntityJoinWorldEvent event)
	{
		Entity entity = event.getEntity();
		World world = event.getWorld();
		if (entity instanceof EntityItem)
		{
			EntityItem entItem = (EntityItem) entity;
			ItemStack stack = entItem.getEntityItem();
			if (stack != null && stack.getItem().equals(Items.STICK))
			{
				entItem.setEntityItemStack(new ItemStack(BigFMod.STICK, stack.stackSize));
			}
		}
		else if (entity instanceof net.minecraft.entity.item.EntityBoat && !(entity instanceof EntityBoat))
		{
			net.minecraft.entity.item.EntityBoat entBoat = (net.minecraft.entity.item.EntityBoat) entity;
			EntityBoat newBoat = new EntityBoat(entBoat.worldObj, entBoat.posX, entBoat.posY, entBoat.posZ);
			newBoat.copyLocationAndAnglesFrom(entBoat);
			NBTTagCompound nbttagcompound = entBoat.writeToNBT(new NBTTagCompound());
			newBoat.readFromNBT(nbttagcompound);
			world.spawnEntityInWorld(newBoat);
			event.setCanceled(true);
		}
	}

	/*
	 * @SubscribeEvent public void onBreak(BlockEvent.BreakEvent event) { if (event.block == Blocks.leaves) { event.setCanceled(true); event.world.setBlock(event.x, event.y, event.z, BigFMod.leavesDecayed, event.blockMetadata, 2); } }
	 */
}
