package bitevo.Zetal.LoMaS.Specializations.Inventory;

import java.util.Iterator;
import java.util.Map;

import net.minecraft.block.BlockAnvil;
import net.minecraft.block.state.IBlockState;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.inventory.InventoryCraftResult;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.IInteractionObject;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Inventory.Slot.SlotCore;

public class SpecContainerRepair extends Container
{
	private static final Logger logger = LogManager.getLogger();
	/** Here comes out item you merged and/or renamed. */
	public IInventory outputSlot;
	/** The 2slots where you put your items in that you want to merge and/or rename. */
	public IInventory inputSlots;
	public IInventory coreSlots = new InventoryRepairCore(this, "RepairCore", true, 2);
	private World theWorld;
	private BlockPos selfPosition;
	/** The maximum cost of repairing/renaming in the anvil. */
	public int maximumCost;
	/** determined by damage of input item and stackSize of repair materials */
	public int materialCost;
	private String repairedItemName;
	/** The player that has this container open. */
	private final EntityPlayer thePlayer;

	@SideOnly(Side.CLIENT)
	public SpecContainerRepair(InventoryPlayer playerInventory, World worldIn, EntityPlayer player)
	{
		this(playerInventory, worldIn, BlockPos.ORIGIN, player);
	}

	public SpecContainerRepair(InventoryPlayer playerInventory, final World worldIn, final BlockPos blockPosIn, EntityPlayer player)
	{
		this.outputSlot = new InventoryCraftResult();
		this.inputSlots = new InventoryBasic("Repair", true, 2)
		{
			private static final String __OBFID = "CL_00001733";

			/**
			 * For tile entities, ensures the chunk containing the tile entity is saved to disk later - the game won't think it hasn't changed and skip it.
			 */
			public void markDirty()
			{
				super.markDirty();
				SpecContainerRepair.this.onCraftMatrixChanged(this);
			}
		};
		this.selfPosition = blockPosIn;
		this.theWorld = worldIn;
		this.thePlayer = player;
		this.addSlotToContainer(new Slot(this.inputSlots, 0, 27, 30));
		this.addSlotToContainer(new Slot(this.inputSlots, 1, 76, 30));
		this.addSlotToContainer(new Slot(this.outputSlot, 2, 134, 30)
		{
			private SpecContainerRepair anvil = SpecContainerRepair.this;
			private static final String __OBFID = "CL_00001734";

			/**
			 * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
			 */
			public boolean isItemValid(ItemStack stack)
			{
				return false;
			}

			/**
			 * Return whether this slot's stack can be taken from this slot.
			 */
			public boolean canTakeStack(EntityPlayer playerIn)
			{
				int emeraldCores;
				int ironCores;
				if (this.anvil.coreSlots.getStackInSlot(0) == null)
				{
					emeraldCores = 0;
				}
				else
				{
					emeraldCores = this.anvil.coreSlots.getStackInSlot(0).stackSize;
				}

				if (this.anvil.coreSlots.getStackInSlot(1) == null)
				{
					ironCores = 0;
				}
				else
				{
					ironCores = this.anvil.coreSlots.getStackInSlot(1).stackSize;
				}
				int ironCost = this.anvil.maximumCost;
				int emeraldCost = 0;
				while (ironCost - 10 >= 0)
				{
					ironCost = ironCost - 10;
					emeraldCost++;
				}
				int coreTotal = (emeraldCores * 10) + ironCores;
				return (playerIn.capabilities.isCreativeMode || (ironCost <= ironCores && emeraldCost <= emeraldCores)) && this.anvil.maximumCost > 0 && this.getHasStack();
			}

			public void onPickupFromSlot(EntityPlayer playerIn, ItemStack stack)
			{
				if (!playerIn.capabilities.isCreativeMode)
				{
					int ironCost = SpecContainerRepair.this.maximumCost;
					int emeraldCost = 0;
					while (ironCost - 10 >= 0)
					{
						ironCost = ironCost - 10;
						emeraldCost++;
					}
					anvil.coreSlots.decrStackSize(0, emeraldCost);
					anvil.coreSlots.decrStackSize(1, ironCost);
					// playerIn.addExperienceLevel(-SpecContainerRepair.this.maximumCost);
				}

				float breakChance = net.minecraftforge.common.ForgeHooks.onAnvilRepair(playerIn, stack, SpecContainerRepair.this.inputSlots.getStackInSlot(0),
						SpecContainerRepair.this.inputSlots.getStackInSlot(1));

				SpecContainerRepair.this.inputSlots.setInventorySlotContents(0, (ItemStack) null);

				if (SpecContainerRepair.this.materialCost > 0)
				{
					ItemStack itemstack1 = SpecContainerRepair.this.inputSlots.getStackInSlot(1);

					if (itemstack1 != null && itemstack1.stackSize > SpecContainerRepair.this.materialCost)
					{
						itemstack1.stackSize -= SpecContainerRepair.this.materialCost;
						SpecContainerRepair.this.inputSlots.setInventorySlotContents(1, itemstack1);
					}
					else
					{
						SpecContainerRepair.this.inputSlots.setInventorySlotContents(1, (ItemStack) null);
					}
				}
				else
				{
					SpecContainerRepair.this.inputSlots.setInventorySlotContents(1, (ItemStack) null);
				}

				SpecContainerRepair.this.maximumCost = 0;
				IBlockState iblockstate = worldIn.getBlockState(blockPosIn);

                if (!playerIn.capabilities.isCreativeMode && !worldIn.isRemote && iblockstate.getBlock() == Blocks.ANVIL && playerIn.getRNG().nextFloat() < breakChance)
                {
                    int l = ((Integer)iblockstate.getValue(BlockAnvil.DAMAGE)).intValue();
                    ++l;

                    if (l > 2)
                    {
                        worldIn.setBlockToAir(blockPosIn);
                        worldIn.playEvent(1029, blockPosIn, 0);
                    }
                    else
                    {
                        worldIn.setBlockState(blockPosIn, iblockstate.withProperty(BlockAnvil.DAMAGE, Integer.valueOf(l)), 2);
                        worldIn.playEvent(1030, blockPosIn, 0);
                    }
                }
                else if (!worldIn.isRemote)
                {
                    worldIn.playEvent(1030, blockPosIn, 0);
                }

				// ///////////SPECIALIZATIONS
				float progressEarned = 0;
				progressEarned = (this.anvil.maximumCost / 2) + 1.0f;

				LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(playerIn);
				if (lplayer.specClass.equalsIgnoreCase("blacksmith"))
				{
					if (!playerIn.capabilities.isCreativeMode)
					{
						lplayer.addSpecProgress(progressEarned);
						playerIn.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((playerIn.worldObj.rand.nextFloat() - playerIn.worldObj.rand.nextFloat()) * 0.7F + 1.8F));
					}
				}
			}
		});
		this.addSlotToContainer(new SlotCore(this, this.coreSlots, 0, 94, 51, theWorld, blockPosIn));
		this.addSlotToContainer(new SlotCore(this, this.coreSlots, 1, 116, 51, theWorld, blockPosIn));
		int i;

		for (i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 9; ++j)
			{
				this.addSlotToContainer(new Slot(playerInventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
			}
		}

		for (i = 0; i < 9; ++i)
		{
			this.addSlotToContainer(new Slot(playerInventory, i, 8 + i * 18, 142));
		}
	}

	/**
	 * Callback for when the crafting matrix is changed.
	 */
	public void onCraftMatrixChanged(IInventory inventoryIn)
	{
		super.onCraftMatrixChanged(inventoryIn);

		if (inventoryIn == this.inputSlots)
		{
			this.updateRepairOutput();
		}
	}

	/**
	 * called when the Anvil Input Slot changes, calculates the new result and puts it in the output slot
	 */
	public void updateRepairOutput()
	{
		boolean flag = false;
		boolean flag1 = true;
		boolean flag2 = true;
		boolean flag3 = true;
		boolean flag4 = true;
		boolean flag5 = true;
		boolean flag6 = true;
		ItemStack itemstack = this.inputSlots.getStackInSlot(0);
		this.maximumCost = 1;
		int i = 0;
		byte b0 = 0;
		byte b1 = 0;

		if (itemstack == null)
		{
			this.outputSlot.setInventorySlotContents(0, (ItemStack) null);
			this.maximumCost = 0;
		}
		else
		{
			ItemStack itemstack1 = itemstack.copy();
			ItemStack itemstack2 = this.inputSlots.getStackInSlot(1);
			Map map = EnchantmentHelper.getEnchantments(itemstack1);
			boolean flag7 = false;
			int i2 = b0 + itemstack.getRepairCost() + (itemstack2 == null ? 0 : itemstack2.getRepairCost());
			this.materialCost = 0;
			int j;

			if (itemstack2 != null)
			{
				// if (!net.minecraftforge.common.ForgeHooks.onAnvilChange(this, itemstack, itemstack2, outputSlot, repairedItemName, i2)) return;
				flag7 = itemstack2.getItem() == Items.ENCHANTED_BOOK && Items.ENCHANTED_BOOK.getEnchantments(itemstack2).tagCount() > 0;
				int k;
				int l;

				if (itemstack1.isItemStackDamageable() && itemstack1.getItem().getIsRepairable(itemstack, itemstack2))
				{
					j = Math.min(itemstack1.getItemDamage(), itemstack1.getMaxDamage() / 4);

					if (j <= 0)
					{
						this.outputSlot.setInventorySlotContents(0, (ItemStack) null);
						this.maximumCost = 0;
						return;
					}

					for (k = 0; j > 0 && k < itemstack2.stackSize; ++k)
					{
						l = itemstack1.getItemDamage() - j;
						itemstack1.setItemDamage(l);
						++i;
						j = Math.min(itemstack1.getItemDamage(), itemstack1.getMaxDamage() / 4);
					}

					this.materialCost = k;
				}
				else
				{
					if (!flag7 && (itemstack1.getItem() != itemstack2.getItem() || !itemstack1.isItemStackDamageable()))
					{
						this.outputSlot.setInventorySlotContents(0, (ItemStack) null);
						this.maximumCost = 0;
						return;
					}

					int j1;

					if (itemstack1.isItemStackDamageable() && !flag7)
					{
						j = itemstack.getMaxDamage() - itemstack.getItemDamage();
						k = itemstack2.getMaxDamage() - itemstack2.getItemDamage();
						l = k + itemstack1.getMaxDamage() * 12 / 100;
						int i1 = j + l;
						j1 = itemstack1.getMaxDamage() - i1;

						if (j1 < 0)
						{
							j1 = 0;
						}

						if (j1 < itemstack1.getMetadata())
						{
							itemstack1.setItemDamage(j1);
							i += 2;
						}
					}

					Map map1 = EnchantmentHelper.getEnchantments(itemstack2);
					Iterator iterator1 = map1.keySet().iterator();

					while (iterator1.hasNext())
					{
						l = ((Integer) iterator1.next()).intValue();
						Enchantment enchantment = Enchantment.getEnchantmentByID(l);

						if (enchantment != null)
						{
							j1 = map.containsKey(Integer.valueOf(l)) ? ((Integer) map.get(Integer.valueOf(l))).intValue() : 0;
							int k1 = ((Integer) map1.get(Integer.valueOf(l))).intValue();
							int k2;

							if (j1 == k1)
							{
								++k1;
								k2 = k1;
							}
							else
							{
								k2 = Math.max(k1, j1);
							}

							k1 = k2;
							boolean flag8 = enchantment.canApply(itemstack);

							if (this.thePlayer.capabilities.isCreativeMode || itemstack.getItem() == Items.ENCHANTED_BOOK)
							{
								flag8 = true;
							}

							Iterator iterator = map.keySet().iterator();

							while (iterator.hasNext())
							{
								int l1 = ((Integer) iterator.next()).intValue();

								Enchantment e2 = Enchantment.getEnchantmentByID(l1);
								if (l1 != l && !(enchantment.canApplyTogether(e2) && e2.canApplyTogether(enchantment))) // Forge BugFix: Let Both enchantments veto being together
								{
									flag8 = false;
									++i;
								}
							}

							if (flag8)
							{
								if (k1 > enchantment.getMaxLevel())
								{
									k1 = enchantment.getMaxLevel();
								}

								map.put(Integer.valueOf(l), Integer.valueOf(k1));
								int k3 = 0;

								switch (enchantment.getRarity())
								{
                                    case COMMON:
                                        k3 = 1;
                                        break;
                                    case UNCOMMON:
                                        k3 = 2;
                                        break;
                                    case RARE:
                                        k3 = 4;
                                        break;
                                    case VERY_RARE:
                                        k3 = 8;
								}

								if (flag7)
								{
									k3 = Math.max(1, k3 / 2);
								}

								i += k3 * k1;
							}
						}
					}
				}
			}

			if (flag7 && !itemstack1.getItem().isBookEnchantable(itemstack1, itemstack2)) itemstack1 = null;

			if (StringUtils.isBlank(this.repairedItemName))
			{
				if (itemstack.hasDisplayName())
				{
					b1 = 1;
					i += b1;
					itemstack1.clearCustomName();
				}
			}
			else if (!this.repairedItemName.equals(itemstack.getDisplayName()))
			{
				b1 = 1;
				i += b1;
				itemstack1.setStackDisplayName(this.repairedItemName);
			}

			this.maximumCost = i2 + i;

			if (i <= 0)
			{
				itemstack1 = null;
			}

			if (b1 == i && b1 > 0 && this.maximumCost >= 40)
			{
				this.maximumCost = 39;
			}

			if (this.maximumCost >= 40 && !this.thePlayer.capabilities.isCreativeMode)
			{
				itemstack1 = null;
			}

			if (itemstack1 != null)
			{
				j = itemstack1.getRepairCost();

				if (itemstack2 != null && j < itemstack2.getRepairCost())
				{
					j = itemstack2.getRepairCost();
				}

				j = j * 2 + 1;
				itemstack1.setRepairCost(j);
				EnchantmentHelper.setEnchantments(map, itemstack1);
			}

			this.outputSlot.setInventorySlotContents(0, itemstack1);
			this.detectAndSendChanges();
		}
	}

	/**
	 * Add the given Listener to the list of Listeners. Method name is for legacy.
	 */
	public void addListener(IContainerListener listener)
	{
		super.addListener(listener);
		listener.sendProgressBarUpdate(this, 0, this.maximumCost);
	}

	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int id, int data)
	{
		if (id == 0)
		{
			this.maximumCost = data;
		}
	}

	/**
	 * Called when the container is closed.
	 */
	public void onContainerClosed(EntityPlayer playerIn)
	{
		super.onContainerClosed(playerIn);

		if (!this.theWorld.isRemote)
		{
			for (int i = 0; i < this.inputSlots.getSizeInventory(); ++i)
			{
				ItemStack itemstack = this.inputSlots.removeStackFromSlot(i);

				if (itemstack != null)
				{
					playerIn.dropItem(itemstack, false);
				}
			}
			
			for (int i = 0; i < this.coreSlots.getSizeInventory(); ++i)
			{
				ItemStack itemstack = this.coreSlots.removeStackFromSlot(i);

				if (itemstack != null)
				{
					playerIn.dropItem(itemstack, false);
				}
			}
		}
	}

	public boolean canInteractWith(EntityPlayer playerIn)
	{
		return this.theWorld.getBlockState(this.selfPosition).getBlock() != Blocks.ANVIL ? false : playerIn.getDistanceSq((double) this.selfPosition.getX() + 0.5D,
				(double) this.selfPosition.getY() + 0.5D, (double) this.selfPosition.getZ() + 0.5D) <= 64.0D;
	}

	/**
	 * Take a stack from the specified inventory slot.
	 */
	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index)
	{
		ItemStack itemstack = null;
		Slot slot = (Slot) this.inventorySlots.get(index);
		//System.out.println(index);

		if (slot != null && slot.getHasStack())
		{
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (index == 2)
			{
				if (!this.mergeItemStack(itemstack1, 5, 41, true))
				{
					return null;
				}

				slot.onSlotChange(itemstack1, itemstack);
			}
			else if (index != 0 && index != 1 && index != 3 && index != 4)
			{
				if (index >= 5 && index < 41 && (itemstack1.getItem().equals(SpecializationsMod.EMERALDHEART)))
				{
					if ((!this.mergeItemStack(itemstack1, 3, 4, true)))
					{
						return null;
					}
				}
				else if (index >= 5 && index < 41 && (itemstack1.getItem().equals(SpecializationsMod.IRONHEART)))
				{
					if ((!this.mergeItemStack(itemstack1, 4, 5, true)))
					{
						return null;
					}
				}
				else if (index >= 5 && index < 41 && !this.mergeItemStack(itemstack1, 0, 2, false))
				{
					return null;
				}
			}
			else if (!this.mergeItemStack(itemstack1, 5, 41, false))
			{
				return null;
			}

			if (itemstack1.stackSize == 0)
			{
				slot.putStack((ItemStack) null);
			}
			else
			{
				slot.onSlotChanged();
			}

			if (itemstack1.stackSize == itemstack.stackSize)
			{
				return null;
			}

			slot.onPickupFromSlot(playerIn, itemstack1);
		}

		return itemstack;
	}

	/**
	 * used by the Anvil GUI to update the Item Name being typed by the player
	 */
	public void updateItemName(String newName)
	{
		this.repairedItemName = newName;

		if (this.getSlot(2).getHasStack())
		{
			ItemStack itemstack = this.getSlot(2).getStack();

			if (StringUtils.isBlank(newName))
			{
				itemstack.clearCustomName();
			}
			else
			{
				itemstack.setStackDisplayName(this.repairedItemName);
			}
		}

		this.updateRepairOutput();
	}

	// private
	public static IInventory getRepairInputInventory(SpecContainerRepair par0ContainerRepair)
	{
		return par0ContainerRepair.inputSlots;
	}

	// private
	public static int getStackSizeUsedInRepair(SpecContainerRepair par0ContainerRepair)
	{
		return par0ContainerRepair.materialCost;
	}

	public static class Anvil implements IInteractionObject
	{
		private final World world;
		private final BlockPos position;

		public Anvil(World worldIn, BlockPos pos)
		{
			this.world = worldIn;
			this.position = pos;
		}

		/**
		 * Gets the name of this command sender (usually username, but possibly "Rcon")
		 */
		public String getName()
		{
			return "anvil";
		}

		/**
		 * Returns true if this thing is named
		 */
		public boolean hasCustomName()
		{
			return false;
		}

		public ITextComponent getDisplayName()
		{
			return new TextComponentTranslation(Blocks.ANVIL.getUnlocalizedName() + ".name", new Object[0]);
		}

		public Container createContainer(InventoryPlayer playerInventory, EntityPlayer playerIn)
		{
			return new SpecContainerRepair(playerInventory, this.world, this.position, playerIn);
		}

		public String getGuiID()
		{
			return "minecraft:anvil";
		}
	}
}
