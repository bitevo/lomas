package bitevo.Zetal.LoMaS.Specializations.Message;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class CraftMessageHandler implements IMessageHandler<CraftMessage, IMessage>
{
	static World world;

	@Override
	@SideOnly(Side.CLIENT)
	public IMessage onMessage(CraftMessage message, MessageContext ctx)
	{
		world = Minecraft.getMinecraft().theWorld;
		handleCraftArrowMessage(message);
		return null;
	}

	@SideOnly(Side.CLIENT)
	private void handleCraftArrowMessage(CraftMessage packet)
	{
		int damage;
		int maxDamage;
		EntityPlayerSP player = Minecraft.getMinecraft().thePlayer;
		if (player.inventory.getItemStack() != null)
		{
			NBTTagCompound tagcomp = player.inventory.getItemStack().getTagCompound();
			if(tagcomp == null)
			{
				tagcomp = new NBTTagCompound();
				player.inventory.getItemStack().setTagCompound(tagcomp);
			}
			tagcomp.setInteger("maxDur", (int) packet.maxDamage);
			player.inventory.getItemStack().setItemDamage(packet.damage);
			player.inventoryContainer.detectAndSendChanges();
		}
	}
}
