package bitevo.Zetal.LoMaS.Economy;

import bitevo.Zetal.LoMaS.Economy.Entity.EntityItemCoin;
import bitevo.Zetal.LoMaS.Economy.Renderer.RenderEntityItemCoin;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class EconClientProxy extends EconCommonProxy
{
	public class CoinRendererFactory implements IRenderFactory<EntityItemCoin>
	{
		@Override
		public Render<? super EntityItemCoin> createRenderFor(RenderManager manager)
		{
			return new RenderEntityItemCoin(manager, Minecraft.getMinecraft().getRenderItem());
		}
	}

	@Override
	public void registerRenderInformation()
	{
		RenderingRegistry.registerEntityRenderingHandler(EntityItemCoin.class, new CoinRendererFactory());

		init();
	}

	@Override
	public void init()
	{
	}
}