package bitevo.Zetal.LoMaS.Factions.GUI;

import java.io.IOException;
import java.util.Random;

import org.lwjgl.input.Mouse;

import bitevo.Zetal.LoMaS.Shared.GUI.GuiScrollable;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiOptionButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.IProgressMeter;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.resources.I18n;
import net.minecraft.init.Blocks;
import net.minecraft.network.play.client.CPacketClientStatus;
import net.minecraft.stats.AchievementList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;

public class GuiMap extends GuiScrollable
{
	public GuiMap(GuiScreen parentScreenIn)
	{
		super(parentScreenIn, new int[]{0,0}, new int[]{255,255});
		this.title = "Faction Map";
	}	
	
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		super.drawScreen(mouseX, mouseY, partialTicks);
		
	}
}
