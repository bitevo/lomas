package bitevo.Zetal.LoMaS.Factions.GUI;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import bitevo.Zetal.LoMaS.Factions.FactionsMod;
import bitevo.Zetal.LoMaS.Factions.Handler.FactionKeyBinds;
import bitevo.Zetal.LoMaS.Factions.Message.FactionMessage;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Faction;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiListButton;
import net.minecraft.client.gui.GuiOptionButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiFaction extends GuiScreen
{
	private static EntityPlayer thePlayer;
	private GuiTextField inputField;
	ResourceLocation texture = new ResourceLocation(FactionsMod.modid, "gui/faction_gui.png");
	public LoMaS_Faction faction;
	public boolean inFaction = false;
	public boolean isLeader = false;
	public boolean isOwner = false;
	
	private boolean isConfirming = false;
	private boolean isCreating = false;
	private boolean isLeaving = false;
	
	public HashMap<Integer, UUID> memberMap;

	public GuiFaction(EntityPlayer player)
	{
		super();
		this.thePlayer = player;
		this.updateFactionStatus();
	}

	public void updateFactionStatus()
	{
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(thePlayer);
		if (lplayer != null)
		{
			this.faction = LoMaS_Faction.getLoMaSFaction(lplayer.faction);
			if (faction != null)
			{
				this.inFaction = true;
				if (faction.getLeaders().contains(lplayer.uuid))
				{
					this.isLeader = true;
					if (faction.getOwner().equals(lplayer.uuid))
					{
						this.isOwner = true;
					}
				}
			}
		}
	}

	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}

	@Override
	public void initGui()
	{
		ScaledResolution res = new ScaledResolution(this.mc);
		int width = res.getScaledWidth();
		int height = res.getScaledHeight();
		int k = (width - 256) / 2;
		int l = (height - 227) / 2;
		this.buttonList.clear();
		this.updateFactionStatus();

		if (!isCreating && !isConfirming)
		{
			if (inFaction)
			{
				if (!LoMaS_Faction.canLeaveFaction(this.thePlayer.getPersistentID(), this.faction.getId()))
				{
					GuiButton leavebutton = new GuiButton(4, k + 126, l + 144, 99, 20, "Change Owner");
					leavebutton.enabled = true;
					leavebutton.displayString = "Change Owner";
					this.buttonList.add(leavebutton);
				}
				else
				{
					GuiButton leavebutton = new GuiButton(1, k + 126, l + 144, 99, 20, "Leave Faction");
					leavebutton.enabled = true;
					leavebutton.displayString = "Leave Faction";
					this.buttonList.add(leavebutton);
				}
				GuiButton mapbutton = new GuiButton(2, k + 126, l + 119, 99, 20, "Faction Map");
				mapbutton.enabled = true;
				mapbutton.displayString = "Faction Map";
				this.buttonList.add(mapbutton);
				if (isLeader)
				{
					GuiButton diplobutton = new GuiButton(3, k + 126, l + 94, 99, 20, "Diplomacy");
					diplobutton.enabled = true;
					diplobutton.displayString = "Diplomacy";
					this.buttonList.add(diplobutton);
				}

				for(int i = 0; i < this.faction.getMembers().size(); i++)
				{
					GuiOptionButton memberbutton = new GuiOptionButton(20 + i, k + 14, l + 24 + (12 * i), 12, 12, "");
					memberbutton.enabled = true;
					memberbutton.displayString = "";
					this.buttonList.add(memberbutton);
				}
			}
			else
			{
				GuiButton createbutton = new GuiButton(0, k + 126, l + 144, 99, 20, "Create Faction");
				createbutton.enabled = true;
				createbutton.displayString = "Create Faction";
				this.buttonList.add(createbutton);
			}
		}
		else if (isCreating && !isConfirming)
		{
			Keyboard.enableRepeatEvents(true);
			this.inputField = new GuiTextField(0, this.fontRendererObj, this.width / 2, this.height / 6 + 56, 89, this.fontRendererObj.FONT_HEIGHT);
			this.inputField.setMaxStringLength(15);
			this.inputField.setEnableBackgroundDrawing(true);
			this.inputField.setVisible(true);
			this.inputField.setTextColor(16777215);
			this.buttonList.add(new GuiOptionButton(5, this.width / 2 - 155, this.height / 6 + 86, "Create"));
			this.buttonList.add(new GuiOptionButton(6, this.width / 2 - 155 + 160, this.height / 6 + 86, "Cancel"));
		}
		else if (isConfirming)
		{
			this.buttonList.add(new GuiOptionButton(9, this.width / 2 - 155, this.height / 6 + 86, "I'm sure!"));
			this.buttonList.add(new GuiOptionButton(10, this.width / 2 - 155 + 160, this.height / 6 + 86, "Nevermind"));
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		this.drawGuiContainerBackgroundLayer(partialTicks, mouseX, mouseY);
		super.drawScreen(mouseX, mouseY, partialTicks);
		this.drawGuiContainerForegroundLayer(mouseX, mouseY);

		if (this.isCreating && !this.isConfirming)
		{
			GlStateManager.disableLighting();
			GlStateManager.disableBlend();
			this.inputField.drawTextBox();
		}
	}

	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		ScaledResolution res = new ScaledResolution(this.mc);
		int width = res.getScaledWidth();
		int height = res.getScaledHeight();
		int k = (width - 256) / 2;
		int l = (height - 227) / 2;

		if (!isConfirming && !isCreating)
		{
			if (inFaction)
			{
				this.fontRendererObj.drawString("Members", k + 32, l + 12, 4210752);
				for(int i = 0; i < this.faction.getMembers().size(); i++)
				{
					this.fontRendererObj.drawString(LoMaS_Player.getLoMaSPlayer(this.faction.getMembers().get(i)).name, k + 30, l + 26 + (12 * i), 4210752);
				}
			}
			else
			{
				this.fontRendererObj.drawString("Invites", k + 32, l + 12, 4210752);
				
			}
		}
		else if (isCreating && !isConfirming)
		{
			this.drawCenteredString(this.fontRendererObj, "Name", this.width / 2 - 20,  this.height / 6 + 56, 16777215);
		}
		else if (isConfirming)
		{
			this.drawCenteredString(this.fontRendererObj, "Are you sure?", this.width / 2, 70, 16777215);
			this.drawCenteredString(this.fontRendererObj, "This is permanent!", this.width / 2, 90, 16777215);
		}
	}

	/**
	 * Draw the background layer for the GuiContainer (everything behind the items)
	 */
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(texture);
		ScaledResolution res = new ScaledResolution(this.mc);
		int width = res.getScaledWidth();
		int height = res.getScaledHeight();
		int k = (width - 256) / 2;
		int l = (height - 227) / 2;

		if (!isConfirming && !isCreating)
		{
			this.drawTexturedModalRect(k, l, 0, 0, 256, 197);
		}
		else if (isCreating && !isConfirming)
		{
			super.drawDefaultBackground();
		}
		else if (isConfirming)
		{
			super.drawDefaultBackground();
		}
	}

	@Override
	protected void actionPerformed(GuiButton button)
	{
		// Creating
		if (button.id == 0)
		{
			this.isCreating = true;
			this.initGui();
		}
		// Leaving
		else if (button.id == 1)
		{
			System.out.println("Leaving");
			this.isLeaving = true;
			this.isConfirming = true;
			this.initGui();
		}
		// Leaving
		else if (button.id == 2)
		{
			System.out.println("Faction Map");
			this.thePlayer.openGui(FactionsMod.instance, 1, null, 0, 0, 0);
		}
		// Leaving
		else if (button.id == 3)
		{
			System.out.println("Diplomacy");
			this.thePlayer.openGui(FactionsMod.instance, 2, null, 0, 0, 0);
		}
		// Leaving
		else if (button.id == 4)
		{
			System.out.println("Change Owner");
			this.initGui();
		}
		// Create
		else if (button.id == 5)
		{
			if (this.inputField.getText() != null && !this.inputField.getText().equals(""))
			{
				this.isCreating = false;
				LoMaS_Faction newFaction = new LoMaS_Faction(this.inputField.getText(), this.thePlayer);
				FactionsMod.snw.sendToServer(new FactionMessage(this.thePlayer.getPersistentID(), newFaction));
				this.initGui();
			}
		}
		// Cancel
		else if (button.id == 6)
		{
			this.isCreating = false;
			this.initGui();
		}
		// Confirm Yes
		else if (button.id == 9)
		{
			System.out.println("Yes");
			if(this.isLeaving)
			{
				FactionsMod.snw.sendToServer(new FactionMessage(this.thePlayer.getPersistentID(), null));
			}
			this.isConfirming = false;
			this.isLeaving = false;
			this.initGui();
		}
		// Confirm Cancel
		else if (button.id == 10)
		{
			System.out.println("Cancel");
			this.isConfirming = false;
			this.isLeaving = false;
			this.initGui();
		}
		// Member modifier button
		else if (button.id >= 20)
		{
			System.out.println("Member button");
		}
	}

	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);
		if (this.inputField != null)
		{
			this.inputField.mouseClicked(mouseX, mouseY, mouseButton);
		}
	}

	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		if (this.inputField == null || !this.inputField.textboxKeyTyped(typedChar, keyCode))
		{
			if (keyCode == 1 || keyCode == this.mc.gameSettings.keyBindInventory.getKeyCode() || keyCode == FactionKeyBinds.keys[0].getKeyCode())
			{
				this.mc.thePlayer.closeScreen();
			}
		}
	}
}
