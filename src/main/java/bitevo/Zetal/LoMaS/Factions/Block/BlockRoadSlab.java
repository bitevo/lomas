package bitevo.Zetal.LoMaS.Factions.Block;

import java.util.Random;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.Factions.FactionsMod;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public abstract class BlockRoadSlab extends BlockSlab
{
    public static final PropertyEnum<BlockRoadSlab.Variant> VARIANT = PropertyEnum.<BlockRoadSlab.Variant>create("variant", BlockRoadSlab.Variant.class);

	public BlockRoadSlab()
	{
		super(Material.ROCK);
		IBlockState iblockstate = this.blockState.getBaseState();

		if (!this.isDouble())
		{
			iblockstate = iblockstate.withProperty(HALF, BlockSlab.EnumBlockHalf.BOTTOM);
			this.setCreativeTab(LoMaS_Utils.tabLoMaS);
		}

        this.setDefaultState(iblockstate.withProperty(VARIANT, BlockRoadSlab.Variant.DEFAULT));
		this.setHarvestLevel("pickaxe", 0);

		this.useNeighborBrightness = !this.isDouble();
	}

    /**
     * Get the Item that this Block should drop when harvested.
     */
    @Nullable
    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune)
    {
        return Item.getItemFromBlock(FactionsMod.ROAD_SLAB);
    }

    @Override
    public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state)
    {
        return new ItemStack(FactionsMod.ROAD_SLAB);
    }

	@Override
	public String getUnlocalizedName(int meta)
	{
		return this.getUnlocalizedName();
	}

	/**
	 * Creates the block state object.
	 * 
	 * @return the block state with properties defined.
	 */
	@Override
    protected BlockStateContainer createBlockState()
    {
        return this.isDouble() ? new BlockStateContainer(this, new IProperty[] {VARIANT}): new BlockStateContainer(this, new IProperty[] {HALF, VARIANT});
    }

    /**
     * Convert the given metadata into a BlockState for this Block
     */
	@Override
    public IBlockState getStateFromMeta(int meta)
    {
        IBlockState iblockstate = this.getDefaultState().withProperty(VARIANT, BlockRoadSlab.Variant.DEFAULT);

        if (!this.isDouble())
        {
            iblockstate = iblockstate.withProperty(HALF, (meta & 8) == 0 ? BlockSlab.EnumBlockHalf.BOTTOM : BlockSlab.EnumBlockHalf.TOP);
        }

        return iblockstate;
    }

    /**
     * Convert the BlockState into the correct metadata value
     */
	@Override
    public int getMetaFromState(IBlockState state)
    {
        int i = 0;

        if (!this.isDouble() && state.getValue(HALF) == BlockSlab.EnumBlockHalf.TOP)
        {
            i |= 8;
        }

        return i;
    }

	@Override
    public IProperty<?> getVariantProperty()
    {
        return VARIANT;
    }

	@Override
    public Comparable<?> getTypeForItem(ItemStack stack)
    {
        return BlockRoadSlab.Variant.DEFAULT;
    }

    public static class Double extends BlockRoadSlab
        {
    		@Override
            public boolean isDouble()
            {
                return true;
            }
        }

    public static class Half extends BlockRoadSlab
        {
    		@Override
            public boolean isDouble()
            {
                return false;
            }
        }

    public static enum Variant implements IStringSerializable
    {
        DEFAULT;

    	@Override
        public String getName()
        {
            return "default";
        }
    }
}
