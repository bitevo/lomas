package bitevo.Zetal.LoMaS.Specializations.Inventory.Slot;

import java.util.Random;

import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.stats.AchievementList;
import net.minecraft.util.math.MathHelper;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;

public class SlotFurnaceOutput extends Slot
{
    /** The player that is using the GUI where this slot resides. */
    private EntityPlayer thePlayer;
    private int field_75228_b;
    private static final String __OBFID = "CL_00002183";

    public SlotFurnaceOutput(EntityPlayer player, IInventory p_i45793_2_, int slotIndex, int xPosition, int yPosition)
    {
        super(p_i45793_2_, slotIndex, xPosition, yPosition);
        this.thePlayer = player;
    }

    /**
     * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
     */
    public boolean isItemValid(ItemStack stack)
    {
        return false;
    }

    /**
     * Decrease the size of the stack in slot (first int arg) by the amount of the second int arg. Returns the new
     * stack.
     */
    public ItemStack decrStackSize(int amount)
    {
        if (this.getHasStack())
        {
            this.field_75228_b += Math.min(amount, this.getStack().stackSize);
        }

        return super.decrStackSize(amount);
    }

    public void onPickupFromSlot(EntityPlayer playerIn, ItemStack stack)
    {
        this.onCrafting(stack);
        super.onPickupFromSlot(playerIn, stack);
    }

    /**
     * the itemStack passed in is the output - ie, iron ingots, and pickaxes, not ore and wood. Typically increases an
     * internal count then calls onCrafting(item).
     */
    protected void onCrafting(ItemStack stack, int amount)
    {
        this.field_75228_b += amount;
        this.onCrafting(stack);
    }

    /**
     * the itemStack passed in is the output - ie, iron ingots, and pickaxes, not ore and wood.
     */
    protected void onCrafting(ItemStack stack)
    {
        stack.onCrafting(this.thePlayer.worldObj, this.thePlayer, this.field_75228_b);

        if (!this.thePlayer.worldObj.isRemote)
        {
            int i = this.field_75228_b;
            float f = FurnaceRecipes.instance().getSmeltingExperience(stack);
            int j;

            if (f == 0.0F)
            {
                i = 0;
            }
            else if (f < 1.0F)
            {
                j = MathHelper.floor_float((float)i * f);

                if (j < MathHelper.ceiling_float_int((float)i * f) && Math.random() < (double)((float)i * f - (float)j))
                {
                    ++j;
                }

                i = j;
            }

            while (i > 0)
            {
                j = EntityXPOrb.getXPSplit(i);
                i -= j;
                this.thePlayer.worldObj.spawnEntityInWorld(new EntityXPOrb(this.thePlayer.worldObj, this.thePlayer.posX, this.thePlayer.posY + 0.5D, this.thePlayer.posZ + 0.5D, j));
            }
        }
        
        float progressEarned = 0;
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(thePlayer);
        //System.out.println(par1ItemStack.getDisplayName());
        if(stack.getItem().equals(Items.IRON_INGOT)
        || stack.getItem().equals(Items.GOLD_INGOT))
        {
        	progressEarned = 1.5f;
        }
        else if (stack.getDisplayName().equalsIgnoreCase("glass"))
        {
        	progressEarned = 0.5f;
        }
        else
        {
        	progressEarned = 0.2f;
        }
        
        if(lplayer.specClass.equalsIgnoreCase("blacksmith"))
        {
        	progressEarned = progressEarned * this.field_75228_b;
            if (!this.thePlayer.capabilities.isCreativeMode && progressEarned > 0.0f)
        	{
                System.out.println("Smelting xp gain: " + progressEarned);
            	lplayer.addSpecProgress(progressEarned);
            	Random rand = new Random();
				this.thePlayer.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
        	}
        }

        this.field_75228_b = 0;

        net.minecraftforge.fml.common.FMLCommonHandler.instance().firePlayerSmeltedEvent(thePlayer, stack);

        if (stack.getItem() == Items.IRON_INGOT)
        {
            this.thePlayer.hasAchievement(AchievementList.ACQUIRE_IRON);
        }

        if (stack.getItem() == Items.COOKED_FISH)
        {
            this.thePlayer.hasAchievement(AchievementList.COOK_FISH);
        }
    }
}