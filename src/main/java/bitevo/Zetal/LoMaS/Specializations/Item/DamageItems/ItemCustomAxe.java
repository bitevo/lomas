package bitevo.Zetal.LoMaS.Specializations.Item.DamageItems;

import java.util.List;

import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemCustomAxe extends ItemAxe
{
	private static final float[] ATTACK_DAMAGES = new float[] { 6.0F, 8.0F, 8.0F, 8.0F, 6.0F, 6.0F, 6.0F };
	private static final float[] ATTACK_SPEEDS = new float[] { -3.2F, -3.2F, -3.1F, -3.0F, -3.0F, -3.0F, -3.5F };

	public ItemCustomAxe(ToolMaterial mat)
	{
		super(mat, ATTACK_DAMAGES[mat.ordinal()], ATTACK_SPEEDS[mat.ordinal()]);
	}

	@Override
	public int getMaxDamage(ItemStack stack)
	{
		return SpecializationsMod.getMaxDamage(stack);
	}
}
