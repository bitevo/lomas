package bitevo.Zetal.LoMaS.Specializations.Item.DamageItems;

import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import net.minecraft.item.Item;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;

public class ItemCustomHoe extends ItemHoe
{
	public ItemCustomHoe(Item.ToolMaterial material)
	{
		super(material);
	}

	@Override
	public int getMaxDamage(ItemStack stack)
	{
		return SpecializationsMod.getMaxDamage(stack);
	}
}
