package bitevo.Zetal.LoMaS.Specializations.Handler;

import java.lang.reflect.Field;

import bitevo.Zetal.LoMaS.Specializations.GUI.GuiSpecEnchantment;
import bitevo.Zetal.LoMaS.Specializations.GUI.GuiSpecFurnace;
import bitevo.Zetal.LoMaS.Specializations.GUI.GuiSpecInventory;
import bitevo.Zetal.LoMaS.Specializations.GUI.GuiSpecRepair;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiEnchantment;
import net.minecraft.client.gui.GuiRepair;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.inventory.GuiFurnace;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.world.IWorldNameable;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class SCMClientEventContainerClass
{
	@SubscribeEvent
	public void guiOpen(GuiOpenEvent event)
	{
		GuiScreen gui = event.getGui();
		EntityPlayer player = Minecraft.getMinecraft().thePlayer;
		if (player != null)
		{
			boolean foundOne = false;
			if (gui instanceof GuiInventory && !(gui instanceof GuiSpecInventory))
			{
				event.setGui(new GuiSpecInventory(player));
			}
			else if (gui instanceof GuiFurnace)
			{
				GuiFurnace gbs = (GuiFurnace) gui;
				IInventory i;
				Field f;
				try
				{
					f = gbs.getClass().getDeclaredFields()[2];
					f.setAccessible(true);
					i = (IInventory) f.get(gbs);
					event.setGui(new GuiSpecFurnace(player.inventory, i));
				}
				catch (IllegalArgumentException e)
				{
					e.printStackTrace();
				}
				catch (IllegalAccessException e)
				{
					e.printStackTrace();
				}
				catch (SecurityException e)
				{
					e.printStackTrace();
				}
			}
			else if (gui instanceof GuiEnchantment)
			{
				GuiEnchantment gbs = (GuiEnchantment) gui;
				IWorldNameable i;
				Field f;
				try
				{
					f = gbs.getClass().getDeclaredFields()[14]; // f = gbs.getClass().getDeclaredField("field_175380_I");
					f.setAccessible(true);
					i = (IWorldNameable) f.get(gbs);
					event.setGui(new GuiSpecEnchantment(player.inventory, player.worldObj, i));
				}
				catch (IllegalArgumentException e)
				{
					e.printStackTrace();
				}
				catch (IllegalAccessException e)
				{
					e.printStackTrace();
				}
				catch (SecurityException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
}