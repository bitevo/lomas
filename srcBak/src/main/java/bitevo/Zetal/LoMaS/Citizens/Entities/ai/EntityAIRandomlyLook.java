package bitevo.Zetal.LoMaS.Citizens.Entities.ai;

import java.util.Random;

import net.minecraft.block.BlockAir;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import bitevo.Zetal.LoMaS.Citizens.Entities.EntityPigman;

public class EntityAIRandomlyLook extends EntityAIBase
{
	public EntityPigman host;
	public BlockPos target;
	public int ticks = 0;
	private float pitch;
	private float yaw;
	private int lookTime;

	public EntityAIRandomlyLook(EntityPigman entityPigman)
	{
		this.host = entityPigman;
	}

	@Override
	public boolean shouldExecute()
	{
		ticks = ticks + host.getRNG().nextInt(1) + 1;
		if (this.ticks % 30 == 0 && this.lookTime == 0)
		{
			target = this.findLookTarget();
			return true;
		}
		return false;
	}

	public boolean continueExecuting()
	{
		return target != null;
	}

	@Override
	public void startExecuting()
	{
		// System.out.println(target);
		if (target == null)
		{
			return;
		}
		else
		{
			this.lookTime = 40 + this.host.getRNG().nextInt(40);
			//System.out.println(this.host.worldObj.getBlockState(target) + " selected!");
		}
	}

	/**
	 * Resets the task
	 */
	public void resetTask()
	{
		this.target = null;
		this.pitch = 0;
		this.yaw = 0;
	}

	/**
	 * Updates the task
	 */
	public void updateTask()
	{
		this.host.getLookHelper().setLookPosition(this.target.getX(), this.target.getY(), this.target.getZ(), 10.0F, (float) this.host.getVerticalFaceSpeed());
		--this.lookTime;
		if (this.lookTime == 0)
		{
			//System.out.println(this.host.worldObj.getBlockState(target) + " done staring!");
			IBlockState state = this.host.worldObj.getBlockState(target);
			boolean isUnwanted = false;
			if (state != null && state.getBlock() != null)
			{
				for (int i = 0; i < EntityPigman.unwanted.length; i++)
				{
					if (state.getBlock() == EntityPigman.unwanted[i])
					{
						isUnwanted = true;
					}
				}
			}
			if (!isUnwanted)
			{
				//System.out.println(this.host.worldObj.getBlockState(target) + " recognized!");
				this.host.setHappiness(this.host.getHappiness() + 1);
			}
			else
			{
				//System.out.println(this.host.worldObj.getBlockState(target) + " disliked!");
				this.host.setHappiness(this.host.getHappiness() - 1);
				this.host.setUnhappyReason("Hate " + this.host.worldObj.getBlockState(target).getBlock().getLocalizedName());
			}
			this.target = null;
		}
	}

	private BlockPos findLookTarget()
	{
		Random random = this.host.getRNG();
		BlockPos ret = null;
		while (ret == null || this.host.worldObj.getBlockState(ret).getBlock() instanceof BlockAir)
		{
			this.pitch = (random.nextFloat() - 1.0f) * 90;
			this.yaw = ((random.nextFloat() - 1.0f) * 360);
			RayTraceResult mop = this.rayTrace(16, this.pitch, this.yaw);
			if (mop != null)
			{
				ret = mop.getBlockPos();
			}
			else
			{
				ret = null;
			}
		}
		return ret;
	}

	private RayTraceResult rayTrace(double distance, float pitch, float yaw)
	{
		Vec3d vec3 = new Vec3d(this.host.posX, this.host.posY + this.host.getEyeHeight(), this.host.posZ);
		Vec3d vec31 = this.getVectorForRotation(pitch, yaw);
		Vec3d vec32 = vec3.addVector(vec31.xCoord * distance, vec31.yCoord * distance, vec31.zCoord * distance);
		// System.out.println(vec3 + " to " + vec32);
		return this.host.worldObj.rayTraceBlocks(vec3, vec32, false, false, true);
	}

	protected final Vec3d getVectorForRotation(float pitch, float yaw)
	{
		float f2 = MathHelper.cos(-yaw * 0.017453292F - (float) Math.PI);
		float f3 = MathHelper.sin(-yaw * 0.017453292F - (float) Math.PI);
		float f4 = -MathHelper.cos(-pitch * 0.017453292F);
		float f5 = MathHelper.sin(-pitch * 0.017453292F);
		return new Vec3d((double) (f3 * f4), (double) f5, (double) (f2 * f4));
	}
}
