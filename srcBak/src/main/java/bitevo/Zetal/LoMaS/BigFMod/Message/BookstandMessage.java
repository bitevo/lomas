package bitevo.Zetal.LoMaS.BigFMod.Message;

import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class BookstandMessage implements IMessage
{
	public BlockPos pos;
	public boolean hasBook;
	public ItemStack book;
	
    public BookstandMessage(){}
	
    public BookstandMessage(BlockPos pos, ItemStack book, boolean hasBook)
    {
    	this.pos = pos;
    	this.book = book;
    	this.hasBook = hasBook;
    }
    
	@Override
	public void fromBytes(ByteBuf buf)
	{
		int x = buf.readInt();
		int y = buf.readInt();
		int z = buf.readInt();
		this.pos = new BlockPos(x, y, z);
		this.book = ByteBufUtils.readItemStack(buf);
		this.hasBook = buf.readBoolean();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeInt(this.pos.getX());
		buf.writeInt(this.pos.getY());
		buf.writeInt(this.pos.getZ());
		ByteBufUtils.writeItemStack(buf, book);
		buf.writeBoolean(hasBook);
	}

}
