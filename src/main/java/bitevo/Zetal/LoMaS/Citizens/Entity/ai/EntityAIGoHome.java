package bitevo.Zetal.LoMaS.Citizens.Entity.ai;

import bitevo.Zetal.LoMaS.Citizens.Entity.EntityPigmanCitizen;
import net.minecraft.entity.ai.EntityAIBase;

public class EntityAIGoHome extends EntityAIBase
{
	public EntityPigmanCitizen host;
	public int ticks = 0;

	public EntityAIGoHome(EntityPigmanCitizen entityPigman)
	{
		this.host = entityPigman;
		this.setMutexBits(1);
	}

	@Override
	public boolean shouldExecute()
	{
		ticks = ticks + host.getRNG().nextInt(1) + 1;
		if (ticks % 10 == 0 && (Math.sqrt(this.host.getDistanceSqToCenter(this.host.getHomeChest())) > 48.0 || this.host.isWet()))
		{
			return true;
		}
		return false;
	}

	@Override
	public boolean continueExecuting()
	{
		return false;
	}

	@Override
	public void startExecuting()
	{
		boolean success = this.host.getNavigator().tryMoveToXYZ(this.host.getHomeChest().getX(), this.host.getHomeChest().getY(), this.host.getHomeChest().getZ(), 0.3D)
						|| this.host.getDistance(this.host.getHomeChest().getX(), this.host.getHomeChest().getY(), this.host.getHomeChest().getZ()) < 5;
		if (!success)
		{
			this.host.setHappiness(this.host.getHappiness() - 5);
			this.host.setUnhappyReason("I'm lost");
		}
	}
}
