package bitevo.Zetal.LoMaS.BigFMod.Block;

import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.BigFMod.TileEntity.TileEntityLeavesDecayed;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockLeavesDecayed extends BlockContainer
{
	public static final PropertyEnum VARIANT = PropertyEnum.create("variant", BlockPlanks.EnumType.class);
	int[] surroundings;

	public BlockLeavesDecayed()
	{
		super(Material.AIR);
		this.disableStats();
		this.setCreativeTab(CreativeTabs.DECORATIONS);
		this.setTickRandomly(true);
		this.setDefaultState(this.blockState.getBaseState().withProperty(VARIANT, BlockPlanks.EnumType.OAK));
		// System.out.println("created decay");
	}

	@Override
	public int tickRate(World par1World)
	{
		Random r = new Random();
		return (r.nextInt(120) + 30);
	}

	/**
	 * adjacent blocks and also whether the player can attach torches, redstone wire, etc to this block.
	 */
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, World worldIn, BlockPos pos)
	{
		return null;
	}

	@Override
	public boolean isCollidable()
	{
		return false;
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing the block.
	 */
	@Override
	public TileEntity createNewTileEntity(World par1World, int i)
	{
		// System.out.println("placed");
		return new TileEntityLeavesDecayed();
	}

	private Block getLeavesBlockFromMeta(int meta)
	{
		if (meta >= 4)
		{
			return Blocks.LEAVES2;
		}
		else
		{
			return Blocks.LEAVES;
		}
	}

	/** Ticks the block if it's been scheduled */
	@Override
	public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random rand)
	{
		int x;
		int y;
		int z;
		int blockx = pos.getX();
		int blocky = pos.getY();
		int blockz = pos.getZ();

		Block adjBlock;
		TileEntity tileblock = worldIn.getTileEntity(pos);
		// System.out.println("tick");
		if (!worldIn.isRemote)
		{
			if (tileblock != null && tileblock instanceof TileEntityLeavesDecayed && this.RANDOM.nextInt(5) == 0)
			{
				TileEntityLeavesDecayed tileconfirmed = (TileEntityLeavesDecayed) tileblock;
				// System.out.println("confirmed tick");
				int counter = ((TileEntityLeavesDecayed) tileblock).getCounter();
				tileconfirmed.setCounter(tileconfirmed.getCounter() + 1);
				if (counter < 5)
				{
					// System.out.println("tock");
					int i = 4;
					int j = 5;
					int k = pos.getX();
					int l = pos.getY();
					int i1 = pos.getZ();

					if (this.surroundings == null)
					{
						this.surroundings = new int[32768];
					}

					if (worldIn.isAreaLoaded(new BlockPos(k - 5, l - 5, i1 - 5), new BlockPos(k + 5, l + 5, i1 + 5)))
					{
						BlockPos.MutableBlockPos blockpos$mutableblockpos = new BlockPos.MutableBlockPos();

						for (int i2 = -4; i2 <= 4; ++i2)
						{
							for (int j2 = -4; j2 <= 4; ++j2)
							{
								for (int k2 = -4; k2 <= 4; ++k2)
								{
									IBlockState iblockstate = worldIn.getBlockState(blockpos$mutableblockpos.setPos(k + i2, l + j2, i1 + k2));
									Block block = iblockstate.getBlock();

									if (block.canSustainLeaves(iblockstate, worldIn, blockpos$mutableblockpos.setPos(k + i2, l + j2, i1 + k2)))
									{
										// System.out.println("IT'S LEAVES");
										worldIn.setBlockState(pos, this.getLeavesBlockFromMeta(tileblock.getBlockMetadata()).getStateFromMeta(tileblock.getBlockMetadata()));
										return;
									}
								}
							}
						}
					}
				}
				else
				{
					// System.out.println("killing decay");
					worldIn.removeTileEntity(pos);
					worldIn.setBlockToAir(pos);
				}
			}
		}
	}

	@Override
	public void breakBlock(World par1World, BlockPos pos, IBlockState state)
	{
		super.breakBlock(par1World, pos, state);
		if (par1World.getTileEntity(pos) != null && par1World.getTileEntity(pos) instanceof TileEntityLeavesDecayed)
		{
			par1World.getTileEntity(pos).invalidate();
			par1World.setTileEntity(pos, null);
			par1World.setBlockToAir(pos);
		}
	}

	@SideOnly(Side.CLIENT)
	/**
	 * A randomly called display update to be able to add particles or other items for display
	 */
	@Override
	public void randomDisplayTick(IBlockState stateIn, World worldIn, BlockPos pos, Random rand)
	{
	}

	/** Returns the quantity of items to drop on block destruction. */
	@Override
	public int quantityDropped(Random par1Random)
	{
		return 0;
	}

	/** Returns the ID of the items to drop on destruction. */
	public int idDropped(int par1, Random par2Random, int par3)
	{
		return Block.getIdFromBlock(LoMaS_Utils.SCM ? bitevo.Zetal.LoMaS.Specializations.SpecializationsMod.SAPLING : Blocks.SAPLING);
	}

	/**
	 * Drops the block items with a specified chance of dropping the specified items
	 */
	@Override
	public void dropBlockAsItemWithChance(World par1World, BlockPos pos, IBlockState state, float par6, int par7)
	{
	}

	/**
	 * Called when the player destroys a block with an item that can harvest it. (i, j, k) are the coordinates of the block and l is the block's subtype/damage.
	 */
	@Override
	public void harvestBlock(World worldIn, EntityPlayer player, BlockPos pos, IBlockState state, @Nullable TileEntity te, @Nullable ItemStack stack)
	{
	}

	/**
	 * Determines the damage on the item the block drops. Used in cloth and wood.
	 */
	@Override
	public int damageDropped(IBlockState state)
	{
		return (Integer) state.getValue(VARIANT) & 5;
	}

	@Override
	@SideOnly(Side.CLIENT)
	/**
	 * returns a list of blocks with the same ID, but different meta (eg: wood returns 4 blocks)
	 */
	public void getSubBlocks(Item par1, CreativeTabs par2CreativeTabs, List par3List)
	{
		par3List.add(new ItemStack(par1, 1, 0));
		par3List.add(new ItemStack(par1, 1, 1));
		par3List.add(new ItemStack(par1, 1, 2));
		par3List.add(new ItemStack(par1, 1, 3));
		par3List.add(new ItemStack(par1, 1, 4));
		par3List.add(new ItemStack(par1, 1, 5));
	}

	public void setBlock(World world, int x, int y, int z, Block block, int meta)
	{
		world.setBlockState(new BlockPos(x, y, z), block.getDefaultState().withProperty(VARIANT, BlockPlanks.EnumType.byMetadata(meta)));
	}

	public int getBlockMetadata(World world, int x, int y, int z)
	{
		if (world.getBlockState(new BlockPos(x, y, z)).getProperties().keySet().contains(VARIANT))
		{
			return (Integer) ((BlockPlanks.EnumType) world.getBlockState(new BlockPos(x, y, z)).getValue(VARIANT)).getMetadata();
		}
		return -1;
	}

	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty(VARIANT, BlockPlanks.EnumType.byMetadata(meta));
	}

	@Override
	public int getMetaFromState(IBlockState state)
	{
		return ((BlockPlanks.EnumType) state.getValue(VARIANT)).getMetadata();
	}

	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, new IProperty[] { VARIANT });
	}

	/*
	 * @Override public boolean isShearable(ItemStack item, IBlockAccess world, int x, int y, int z) { return false; }
	 * @Override public ArrayList<ItemStack> onSheared(ItemStack item, IBlockAccess world, int x, int y, int z, int fortune) { return null; }
	 */
}
