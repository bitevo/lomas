package bitevo.Zetal.LoMaS.Specializations.Block;

import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import com.google.common.base.Predicate;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.FoodInitializer;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityFactionDiamondGolem;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityFactionIronGolem;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntityQualityBlock;
import net.minecraft.block.BlockHorizontal;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.BlockWorldState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.block.state.pattern.BlockMaterialMatcher;
import net.minecraft.block.state.pattern.BlockPattern;
import net.minecraft.block.state.pattern.BlockStateMatcher;
import net.minecraft.block.state.pattern.FactoryBlockPattern;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntitySnowman;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Enchantments;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockPumpkin extends BlockHorizontal implements ITileEntityProvider
{
	private BlockPattern snowmanBasePattern;
	private BlockPattern snowmanPattern;
	private BlockPattern ironGolemBasePattern;
	private BlockPattern ironGolemPattern;
	private BlockPattern diamondGolemBasePattern;
	private BlockPattern diamondGolemPattern;
	private static final Predicate<IBlockState> IS_PUMPKIN = new Predicate<IBlockState>()
	{
		@Override
		public boolean apply(@Nullable IBlockState p_apply_1_)
		{
			return p_apply_1_ != null && (p_apply_1_.getBlock() == FoodInitializer.PUMPKIN || p_apply_1_.getBlock() == Blocks.PUMPKIN || p_apply_1_.getBlock() == Blocks.LIT_PUMPKIN);
		}
	};

	public BlockPumpkin()
	{
		super(Material.GOURD, MapColor.ADOBE);
		this.setDefaultState(this.blockState.getBaseState().withProperty(FACING, EnumFacing.NORTH));
		this.setTickRandomly(true);
		this.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
		this.isBlockContainer = true;
	}

	@Override
	public void harvestBlock(World worldIn, EntityPlayer player, BlockPos pos, IBlockState state, @Nullable TileEntity te, @Nullable ItemStack stack)
	{
		if (!worldIn.isRemote) // Forge: Noop this
		{
			player.addStat(StatList.getBlockStats(this));
			player.addExhaustion(0.025F);

			if (this.canSilkHarvest(worldIn, pos, state, player) && EnchantmentHelper.getEnchantmentLevel(Enchantments.SILK_TOUCH, stack) > 0)
			{
				java.util.List<ItemStack> items = new java.util.ArrayList<ItemStack>();
				ItemStack itemstack = this.createStackedBlock(state);

				if (itemstack != null)
				{
					items.add(itemstack);
				}

				net.minecraftforge.event.ForgeEventFactory.fireBlockHarvesting(items, worldIn, pos, state, 0, 1.0f, true, player);
				for (ItemStack item : items)
				{
					spawnAsEntity(worldIn, pos, item);
				}
			}
			else
			{
				int i = EnchantmentHelper.getEnchantmentLevel(Enchantments.FORTUNE, stack);
				Random rand = worldIn instanceof World ? ((World) worldIn).rand : new Random();
				spawnAsEntity(worldIn, pos, new ItemStack(this.getItemDropped(state, rand, i), this.quantityDropped(state, i, rand), ((TileEntityQualityBlock) te).getQuality()));
			}
		}
		else
		{
			super.harvestBlock(worldIn, player, pos, state, te, stack);
		}
	}

	@Override
	public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state)
	{
		return new ItemStack(FoodInitializer.PUMPKIN, 1, ((TileEntityQualityBlock) worldIn.getTileEntity(pos)).getQuality());
	}

	@Override
	public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state)
	{
		super.onBlockAdded(worldIn, pos, state);
	}

	public boolean canDispenserPlace(World worldIn, BlockPos pos)
	{
		return this.getSnowmanBasePattern().match(worldIn, pos) != null || this.getIronGolemBasePattern().match(worldIn, pos) != null || this.getDiamondGolemBasePattern().match(worldIn, pos) != null;
	}

	private void trySpawnGolem(World worldIn, BlockPos pos, EntityLivingBase placer)
	{
		BlockPattern.PatternHelper blockpattern$patternhelper = this.getSnowmanPattern().match(worldIn, pos);

		if (blockpattern$patternhelper != null)
		{
			for (int i = 0; i < this.getSnowmanPattern().getThumbLength(); ++i)
			{
				BlockWorldState blockworldstate = blockpattern$patternhelper.translateOffset(0, i, 0);
				worldIn.setBlockState(blockworldstate.getPos(), Blocks.AIR.getDefaultState(), 2);
			}

			EntitySnowman entitysnowman = new EntitySnowman(worldIn);
			BlockPos blockpos1 = blockpattern$patternhelper.translateOffset(0, 2, 0).getPos();
			entitysnowman.setLocationAndAngles((double) blockpos1.getX() + 0.5D, (double) blockpos1.getY() + 0.05D, (double) blockpos1.getZ() + 0.5D, 0.0F, 0.0F);
			worldIn.spawnEntityInWorld(entitysnowman);

			for (int j = 0; j < 120; ++j)
			{
				worldIn.spawnParticle(EnumParticleTypes.SNOW_SHOVEL, (double) blockpos1.getX() + worldIn.rand.nextDouble(), (double) blockpos1.getY() + worldIn.rand.nextDouble() * 2.5D, (double) blockpos1.getZ() + worldIn.rand.nextDouble(), 0.0D, 0.0D, 0.0D, new int[0]);
			}

			for (int i1 = 0; i1 < this.getSnowmanPattern().getThumbLength(); ++i1)
			{
				BlockWorldState blockworldstate1 = blockpattern$patternhelper.translateOffset(0, i1, 0);
				worldIn.notifyNeighborsRespectDebug(blockworldstate1.getPos(), Blocks.AIR);
			}
		}
		else
		{
			blockpattern$patternhelper = this.getIronGolemPattern().match(worldIn, pos);

			if (blockpattern$patternhelper != null)
			{
				for (int k = 0; k < this.getIronGolemPattern().getPalmLength(); ++k)
				{
					for (int l = 0; l < this.getIronGolemPattern().getThumbLength(); ++l)
					{
						worldIn.setBlockState(blockpattern$patternhelper.translateOffset(k, l, 0).getPos(), Blocks.AIR.getDefaultState(), 2);
					}
				}

				BlockPos blockpos = blockpattern$patternhelper.translateOffset(1, 2, 0).getPos();
				EntityFactionIronGolem entityirongolem = new EntityFactionIronGolem(worldIn);
				entityirongolem.setPlayerCreated(true);
				entityirongolem.setLocationAndAngles((double) blockpos.getX() + 0.5D, (double) blockpos.getY() + 0.05D, (double) blockpos.getZ() + 0.5D, 0.0F, 0.0F);
				worldIn.spawnEntityInWorld(entityirongolem);

				for (int j1 = 0; j1 < 120; ++j1)
				{
					worldIn.spawnParticle(EnumParticleTypes.SNOWBALL, (double) blockpos.getX() + worldIn.rand.nextDouble(), (double) blockpos.getY() + worldIn.rand.nextDouble() * 3.9D, (double) blockpos.getZ() + worldIn.rand.nextDouble(), 0.0D, 0.0D, 0.0D, new int[0]);
				}

				for (int k1 = 0; k1 < this.getIronGolemPattern().getPalmLength(); ++k1)
				{
					for (int l1 = 0; l1 < this.getIronGolemPattern().getThumbLength(); ++l1)
					{
						BlockWorldState blockworldstate2 = blockpattern$patternhelper.translateOffset(k1, l1, 0);
						worldIn.notifyNeighborsRespectDebug(blockworldstate2.getPos(), Blocks.AIR);
					}
				}
			}
			/**
			 * If it's not snowman or iron, it's diamond
			 */
			else
			{
				/**
				 * If placer has diamond golems skill
				 */
				if(placer != null && placer instanceof EntityPlayer && LoMaS_Player.getLoMaSPlayer((EntityPlayer) placer) != null && LoMaS_Player.getLoMaSPlayer((EntityPlayer) placer).getSkills().getMatchingNodesFromName("Diamond Golems") >= 1)
				{
					blockpattern$patternhelper = this.getDiamondGolemPattern().match(worldIn, pos);

					if (blockpattern$patternhelper != null)
					{
						for (int k = 0; k < this.getDiamondGolemPattern().getPalmLength(); ++k)
						{
							for (int l = 0; l < this.getDiamondGolemPattern().getThumbLength(); ++l)
							{
								worldIn.setBlockState(blockpattern$patternhelper.translateOffset(k, l, 0).getPos(), Blocks.AIR.getDefaultState(), 2);
							}
						}

						BlockPos blockpos = blockpattern$patternhelper.translateOffset(1, 2, 0).getPos();
						EntityFactionDiamondGolem entitydiamondgolem = new EntityFactionDiamondGolem(worldIn);
						entitydiamondgolem.setPlayerCreated(true);
						entitydiamondgolem.setLocationAndAngles((double) blockpos.getX() + 0.5D, (double) blockpos.getY() + 0.05D, (double) blockpos.getZ() + 0.5D, 0.0F, 0.0F);
						worldIn.spawnEntityInWorld(entitydiamondgolem);

						for (int j1 = 0; j1 < 120; ++j1)
						{
							worldIn.spawnParticle(EnumParticleTypes.SNOWBALL, (double) blockpos.getX() + worldIn.rand.nextDouble(), (double) blockpos.getY() + worldIn.rand.nextDouble() * 3.9D, (double) blockpos.getZ() + worldIn.rand.nextDouble(), 0.0D, 0.0D, 0.0D, new int[0]);
						}

						for (int k1 = 0; k1 < this.getDiamondGolemPattern().getPalmLength(); ++k1)
						{
							for (int l1 = 0; l1 < this.getDiamondGolemPattern().getThumbLength(); ++l1)
							{
								BlockWorldState blockworldstate2 = blockpattern$patternhelper.translateOffset(k1, l1, 0);
								worldIn.notifyNeighborsRespectDebug(blockworldstate2.getPos(), Blocks.AIR);
							}
						}
					}
				}
			}
		}
	}

	@Override
	public boolean canPlaceBlockAt(World worldIn, BlockPos pos)
	{
		return worldIn.getBlockState(pos).getBlock().isReplaceable(worldIn, pos) && worldIn.getBlockState(pos.down()).isSideSolid(worldIn, pos, EnumFacing.UP);
	}

	@Override
	public IBlockState onBlockPlaced(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
	{
		return this.getDefaultState().withProperty(FACING, placer.getHorizontalFacing().getOpposite());
	}

	@Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
    {
		this.trySpawnGolem(worldIn, pos, placer);
    }

	@SideOnly(Side.CLIENT)
	@Override
	public void getSubBlocks(Item itemIn, CreativeTabs tab, List list)
	{
		list.add(new ItemStack(itemIn, 1, 0));
		list.add(new ItemStack(itemIn, 1, 1));
		list.add(new ItemStack(itemIn, 1, 2));
	}

	/**
	 * Convert the given metadata into a BlockState for this Block
	 */
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty(FACING, EnumFacing.getHorizontal(meta));
	}

	/**
	 * Convert the BlockState into the correct metadata value
	 */
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return ((EnumFacing) state.getValue(FACING)).getHorizontalIndex();
	}

	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, new IProperty[] { FACING });
	}

	/**
	 * Returns the blockstate with the given rotation from the passed blockstate. If inapplicable, returns the passed blockstate.
	 */
	@Override
	public IBlockState withRotation(IBlockState state, Rotation rot)
	{
		return state.withProperty(FACING, rot.rotate((EnumFacing) state.getValue(FACING)));
	}

	/**
	 * Returns the blockstate with the given mirror of the passed blockstate. If inapplicable, returns the passed blockstate.
	 */
	@Override
	public IBlockState withMirror(IBlockState state, Mirror mirrorIn)
	{
		return state.withRotation(mirrorIn.toRotation((EnumFacing) state.getValue(FACING)));
	}

	protected BlockPattern getSnowmanBasePattern()
	{
		if (this.snowmanBasePattern == null)
		{
			this.snowmanBasePattern = FactoryBlockPattern.start().aisle(new String[] { " ", "#", "#" }).where('#', BlockWorldState.hasState(BlockStateMatcher.forBlock(Blocks.SNOW))).build();
		}

		return this.snowmanBasePattern;
	}

	protected BlockPattern getSnowmanPattern()
	{
		if (this.snowmanPattern == null)
		{
			this.snowmanPattern = FactoryBlockPattern.start().aisle(new String[] { "^", "#", "#" }).where('^', BlockWorldState.hasState(IS_PUMPKIN)).where('#', BlockWorldState.hasState(BlockStateMatcher.forBlock(Blocks.SNOW))).build();
		}

		return this.snowmanPattern;
	}

	protected BlockPattern getIronGolemBasePattern()
	{
		if (this.ironGolemBasePattern == null)
		{
			this.ironGolemBasePattern = FactoryBlockPattern.start().aisle(new String[] { "~ ~", "###", "~#~" }).where('#', BlockWorldState.hasState(BlockStateMatcher.forBlock(Blocks.IRON_BLOCK))).where('~', BlockWorldState.hasState(BlockMaterialMatcher.func_189886_a(Material.AIR))).build();
		}

		return this.ironGolemBasePattern;
	}

	protected BlockPattern getIronGolemPattern()
	{
		if (this.ironGolemPattern == null)
		{
			this.ironGolemPattern = FactoryBlockPattern.start().aisle(new String[] { "~^~", "###", "~#~" }).where('^', BlockWorldState.hasState(IS_PUMPKIN)).where('#', BlockWorldState.hasState(BlockStateMatcher.forBlock(Blocks.IRON_BLOCK))).where('~', BlockWorldState.hasState(BlockMaterialMatcher.func_189886_a(Material.AIR))).build();
		}

		return this.ironGolemPattern;
	}

	protected BlockPattern getDiamondGolemBasePattern()
	{
		if (this.diamondGolemBasePattern == null)
		{
			this.diamondGolemBasePattern = FactoryBlockPattern.start().aisle(new String[] { "~ ~", "###", "~#~" }).where('#', BlockWorldState.hasState(BlockStateMatcher.forBlock(Blocks.DIAMOND_BLOCK))).where('~', BlockWorldState.hasState(BlockMaterialMatcher.func_189886_a(Material.AIR))).build();
		}

		return this.diamondGolemBasePattern;
	}

	protected BlockPattern getDiamondGolemPattern()
	{
		if (this.diamondGolemPattern == null)
		{
			this.diamondGolemPattern = FactoryBlockPattern.start().aisle(new String[] { "~^~", "###", "~#~" }).where('^', BlockWorldState.hasState(IS_PUMPKIN)).where('#', BlockWorldState.hasState(BlockStateMatcher.forBlock(Blocks.DIAMOND_BLOCK))).where('~', BlockWorldState.hasState(BlockMaterialMatcher.func_189886_a(Material.AIR))).build();
		}

		return this.diamondGolemPattern;
	}

	/**
	 * Called on both Client and Server when World#addBlockEvent is called
	 */
	@Override
	public boolean eventReceived(IBlockState state, World worldIn, BlockPos pos, int eventID, int eventParam)
	{
		super.eventReceived(state, worldIn, pos, eventID, eventParam);
		TileEntity tileentity = worldIn.getTileEntity(pos);
		return tileentity == null ? false : tileentity.receiveClientEvent(eventID, eventParam);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileEntityQualityBlock();
	}
}
