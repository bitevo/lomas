package bitevo.Zetal.LoMaS.Specializations.TileWrapper;

import java.util.UUID;

import bitevo.Zetal.LoMaS.TileWrappers.TileWrapperMod;
import bitevo.Zetal.LoMaS.TileWrappers.Message.TileWrapperMessage;
import bitevo.Zetal.LoMaS.TileWrappers.TileWrapper.TileWrapper;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public abstract class TileWrapperRune extends TileWrapper
{
	private UUID owner;
	private int secondCounter = 0;

	public TileWrapperRune(String name, World world, BlockPos pos, EnumFacing facing)
	{
		super(name, world, pos, facing);
		if(this.name == null)
		{
			this.name = this.getDefaultName();
		}
	}

	public abstract String getDefaultName();

	public TileWrapperRune(String name, World world, BlockPos pos, EnumFacing facing, UUID owner)
	{
		this(name, world, pos, facing);
		this.owner = owner;
	}

	@Override
	public void update()
	{
		if(this.secondCounter % 20 == 0)
		{
			this.secondCounter = 0;
			if(this.shouldRefresh())
			{
				this.invalidate();
			}
			
			if(!this.getWorld().isRemote)
			{
				TileWrapperMod.snw.sendToAll(new TileWrapperMessage(this.getPos(), this.index, this.getUpdateTag(), this.getClass()));
			}
			//System.out.println("Hi there! " + this.getPos() + " " + this.blockType + " " + this.name + " " + this.getIndex());
		}
		this.secondCounter++;
	}

    public void readFromNBT(NBTTagCompound compound)
    {
    	if(compound.hasKey("owner"))
    	{
        	this.owner = UUID.fromString(compound.getString("owner"));
    	}
    	super.readFromNBT(compound);
    }

    public NBTTagCompound writeToNBT(NBTTagCompound compound)
    {
    	if(this.owner != null)
    	{
        	compound.setString("owner", this.owner.toString());
    	}
        return super.writeToNBT(compound);
    }

	public UUID getOwner()
	{
		return owner;
	}

	public void setOwner(UUID owner)
	{
		this.owner = owner;
	}
	
}
