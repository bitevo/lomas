package bitevo.Zetal.LoMaS.Specializations.Entities;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.entity.projectile.EntityTippedArrow;
import net.minecraft.init.Items;
import net.minecraft.init.PotionTypes;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.network.play.server.SPacketChangeGameState;
import net.minecraft.potion.PotionEffect;
import net.minecraft.potion.PotionType;
import net.minecraft.potion.PotionUtils;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntitySelectors;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.datafix.DataFixer;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.IThrowableEntity;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Sets;

public class EntityCustomArrow extends EntityTippedArrow
{
	/*
	 * Arrow Types: 0 = Wooden 1 = Stone 2 = Iron 3 = Gold 4 = Diamond 5 = Obsidian
	 */
	///// SPECIALIZATIONS
	public int arrowType = -1;
	private double damage;
	private static final DataParameter<Integer> COLOR = EntityDataManager.<Integer> createKey(EntityTippedArrow.class, DataSerializers.VARINT);
	private PotionType potion = PotionTypes.EMPTY;
	private final Set<PotionEffect> customPotionEffects = Sets.<PotionEffect> newHashSet();

	public EntityCustomArrow(World worldIn)
	{
		super(worldIn);
	}

	public EntityCustomArrow(World worldIn, double x, double y, double z, int arrowType)
	{
		super(worldIn, x, y, z);
		// Wood = 0.5
		// Stone = 1.0;
		// Iron = 1.5;
		// Gold = 0.5;
		// Diamond = 2.5;
		this.arrowType = arrowType;
		int arrowMod = 0;
		switch (this.arrowType)
		{
			case 0:
				arrowMod = 0;
				break;
			case 1:
				arrowMod = 1;
				break;
			case 2:
				arrowMod = 2;
				break;
			case 3:
				arrowMod = 0;
				break;
			case 4:
				arrowMod = 4;
				break;
		}
		this.damage = (this.damage * arrowMod) + this.damage;
	}

	public EntityCustomArrow(World worldIn, EntityLivingBase shooter, int arrowType)
	{
		super(worldIn, shooter);
		// Wood = 0.5
		// Stone = 1.0;
		// Iron = 1.5;
		// Gold = 0.5;
		// Diamond = 2.5;
		this.arrowType = arrowType;
		int arrowMod = 0;
		switch (this.arrowType)
		{
			case 0:
				arrowMod = 0;
				break;
			case 1:
				arrowMod = 1;
				break;
			case 2:
				arrowMod = 2;
				break;
			case 3:
				arrowMod = 0;
				break;
			case 4:
				arrowMod = 4;
				break;
		}
		this.damage = (this.damage * arrowMod) + this.damage;
	}

	protected void entityInit()
	{
		super.entityInit();
	}

	protected void arrowHit(EntityLivingBase living)
	{
		super.arrowHit(living);
		if (this.shootingEntity != null && this.shootingEntity instanceof EntityPlayer && LoMaS_Player.getLoMaSPlayer((EntityPlayer) this.shootingEntity) != null && LoMaS_Player.getLoMaSPlayer((EntityPlayer) this.shootingEntity).specClass.equalsIgnoreCase("warrior"))
		{
			EntityPlayer player = ((EntityPlayer) this.shootingEntity);
			EntityLivingBase entityhit = (EntityLivingBase) living;

            float f = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionY * this.motionY + this.motionZ * this.motionZ);
            int i = MathHelper.ceiling_double_int((double)f * this.getDamage());
			
			if ((this.isBurning() || this.arrowType == 3) && !(living instanceof EntityEnderman))
            {
                living.setFire(5);
            }
			
			if (i >= entityhit.getHealth() && i >= 3)
			{
				float progressEarned = 1.0f;
				if (entityhit instanceof EntityMob)
				{
					progressEarned = 1.5f;
				}
				if (entityhit instanceof EntityAnimal)
				{
					progressEarned = 0.25f;
					if (entityhit instanceof EntityWolf)
					{
						progressEarned = 1.5f;
					}
				}
				if (!player.capabilities.isCreativeMode)
				{
					LoMaS_Player.getLoMaSPlayer(player).addSpecProgress(progressEarned);
					worldObj.playSound((EntityPlayer) null, player.posX, player.posY, player.posZ, SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, SoundCategory.PLAYERS, 0.1F, 0.5F * ((this.rand.nextFloat() - this.rand.nextFloat()) * 0.7F + 1.8F));
				}
			}
		}
	}
	
    protected ItemStack getArrowStack()
    {
        if (this.customPotionEffects.isEmpty() && this.potion == PotionTypes.EMPTY)
        {
            return new ItemStack(SpecializationsMod.ARROW);
        }
        else
        {
            ItemStack itemstack = new ItemStack(SpecializationsMod.ARROW);
            PotionUtils.addPotionToItemStack(itemstack, this.potion);
            PotionUtils.appendEffects(itemstack, this.customPotionEffects);
            return itemstack;
        }
    }

	@Override
	public void writeEntityToNBT(NBTTagCompound compound)
	{
		super.writeEntityToNBT(compound);
		///// SPECIALIZATIONS
		compound.setByte("arrowType", (byte) this.arrowType);
	}

	@Override
	public void readEntityFromNBT(NBTTagCompound compound)
	{
		super.readEntityFromNBT(compound);
		///// SPECIALIZATIONS
		this.arrowType = compound.getByte("arrowType");
	}
}
