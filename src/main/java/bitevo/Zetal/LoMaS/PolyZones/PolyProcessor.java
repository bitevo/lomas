package bitevo.Zetal.LoMaS.PolyZones;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class PolyProcessor implements Serializable
{

	private static final long serialVersionUID = 1L;
	private ArrayList<PolyZone> zones = new ArrayList<PolyZone>();
	private ArrayList<PolyZonePlayer> players = new ArrayList<PolyZonePlayer>();
	private ArrayList<PolyZonePlayer> activePlayers = new ArrayList<PolyZonePlayer>();
	private HashMap<String, Access> defaultAccess = new HashMap<String, Access>();

	public PolyProcessor()
	{
	}

	public PolyZonePlayer getPlayer(UUID uuid)
	{
		for (PolyZonePlayer x : players)
		{
			if (uuid != null && x.getUUID().equals(uuid))
			{
				return x;
			}
		}
		return null;
	}

	/**
	 * Adds a player to the active players list. If the player was not on the list of all players before hand then the player is added. If the player is brand new default permissions for all zones are
	 * assigned.
	 *
	 * @param p
	 *            The player to be added to this game.
	 */
	public void addPlayer(EntityPlayer p)
	{
		UUID uuid = p.getPersistentID();
		for (PolyZonePlayer x : players)
		{
			if (x.getUUID().equals(uuid))
			{
				return;
			}
		}
		PolyZonePlayer temp = new PolyZonePlayer(p);
		temp.setZoneAccessArray(defaultAccess);
		players.add(temp);
	}

	public void addPlayer(UUID p)
	{
		for (PolyZonePlayer x : players)
		{
			if (x.getUUID().equals(p))
			{
				return;
			}
		}
		PolyZonePlayer temp = new PolyZonePlayer(p);
		temp.setZoneAccessArray(defaultAccess);
		players.add(temp);
	}

	public void removePlayer(EntityPlayer p)
	{
		UUID uuid = p.getPersistentID();
		for (PolyZonePlayer y : activePlayers)
		{
			if (y.getUUID().equals(uuid))
			{
				activePlayers.remove(y);
			}
		}
	}

	public PolyZone getZone(String name)
	{
		for (PolyZone x : getZones())
		{
			if (x.getName().equals(name)) return x;
		}
		return null;
	}

	/**
	 * This is called after changing the default permissions of a certain zone. We say update all players permissions for this zone and also update the defaultAcess array with the new permissions.
	 *
	 * @param polyname
	 */
	public void updateDefaultPermissions(String polyname)
	{
		PolyZone temp = this.getZone(polyname);
		for (PolyZonePlayer y : players)
		{
			y.getAccess(polyname).setPermissionValue(temp.getPermissions().getDefaultPermissions().getPermissions());
		}
	}

	/**
	 * Adds a new polyZone to this processor object. The zones default permissions are loaded to the database.
	 *
	 * @param x
	 *            The zone to be added.
	 */
	public void addZone(PolyZone x)
	{
		getZones().add(x);
		defaultAccess.put(x.getName(), x.getPermissions().getDefaultPermissions());
		for (PolyZonePlayer g : players)
		{
			g.addZone(x.getName(), x.getPermissions().getDefaultPermissions().getPermissions());
		}
		/*
		 * if(x.getName().equalsIgnoreCase("sedrons")) { Access def = x.getPermissions().getDefaultPermissions(); def.denyAll(); def.allowEnter(); def.allowHealthRegen(); Access tem =
		 * PolyZonesMod.processor.getPlayer("Sedrons").getAccess(x.getName()); tem.allowAll(); } else if(x.getName().equalsIgnoreCase("dylan")) { Access def =
		 * x.getPermissions().getDefaultPermissions(); def.denyAll(); def.allowEnter(); def.allowHealthRegen(); Access tem = PolyZonesMod.processor.getPlayer("dylanyates92").getAccess(x.getName());
		 * tem.allowAll(); } else if(x.getName().equalsIgnoreCase("zetal")) { Access def = x.getPermissions().getDefaultPermissions(); def.denyAll(); def.allowEnter(); def.allowHealthRegen(); Access
		 * tem = PolyZonesMod.processor.getPlayer("Zetal").getAccess(x.getName()); tem.allowAll(); } else if(x.getName().equalsIgnoreCase("pwebbz")) { Access def =
		 * x.getPermissions().getDefaultPermissions(); def.denyAll(); def.allowEnter(); def.allowHealthRegen(); Access tem = PolyZonesMod.processor.getPlayer("pwebbz").getAccess(x.getName());
		 * tem.allowAll(); }
		 */
	}

	/**
	 * Called when a polyZone is removed from the map.
	 *
	 * @param x
	 *            The zone to be removed.
	 */
	public boolean removeZone(PolyZone x)
	{
		if (getZones().contains(x))
		{
			getZones().remove(x);
			defaultAccess.remove(x.getPermissions().getDefaultPermissions());
			DataHandler.writeObject(this);
			return true;
		}
		return false;
	}

	/**
	 * Called at the start up of the server. Clears the active player list.
	 */
	public void newGame()
	{
		activePlayers.clear();
	}

	/**
	 * Returns true if the given player is inside a polygon. Operation time O(n).
	 *
	 * @param p
	 *            The player to be checked.
	 * @return True or False.
	 */
	public PolyZone contained(Entity p)
	{
		for (PolyZone x : getZones())
		{
			if (x.isInside(p))
			{
				return x;
			}
		}
		return null;
	}

	/**
	 * Returns true if the given blockloc is inside a polygon. Operation time O(n).
	 *
	 * @param p
	 *            The blockloc to be checked.
	 * @return True or False.
	 */
	public PolyZone contained(BlockLoc p, World w)
	{
		for (PolyZone x : getZones())
		{
			if (x.isInside(p, w))
			{
				return x;
			}
		}
		return null;
	}

	public PolyZone contained(World w, Chunk c)
	{
		for (PolyZone x : getZones())
		{
			if (x.isInside(w, c))
			{
				return x;
			}
		}
		return null;
	}

	public ArrayList<PolyZone> getZones()
	{
		return zones;
	}

	public void setZones(ArrayList<PolyZone> zones)
	{
		this.zones = zones;
	}

	public ArrayList<PolyZonePlayer> getPlayers()
	{
		return players;
	}

	public void setPlayers(ArrayList<PolyZonePlayer> players)
	{
		this.players = players;
	}

	public ArrayList<PolyZonePlayer> getActivePlayers()
	{
		return activePlayers;
	}

	public void setActivePlayers(ArrayList<PolyZonePlayer> activePlayers)
	{
		this.activePlayers = activePlayers;
	}

	public HashMap<String, Access> getDefaultAccess()
	{
		return defaultAccess;
	}

	public void setDefaultAccess(HashMap<String, Access> defaultAccess)
	{
		this.defaultAccess = defaultAccess;
	}

	@Override
	public String toString()
	{
		String retVal = "";
		for (PolyZone x : getZones())
		{
			retVal = retVal + x.toString();
		}
		for (Access s : defaultAccess.values())
		{
			retVal = retVal + s.getPermissions();
		}
		return retVal;
	}

	@Override
	public boolean equals(Object o)
	{
		if (o instanceof PolyProcessor)
		{
			PolyProcessor in = (PolyProcessor) o;
			if ((in.getZones().containsAll(this.getZones()) && this.getZones().containsAll(in.getZones())) && (in.players.containsAll(this.players) && this.players.containsAll(in.players)) && (in.activePlayers.containsAll(this.activePlayers) && this.activePlayers.containsAll(in.activePlayers)) && (in.defaultAccess.equals(this.defaultAccess)))
			{
				return true;
			}
		}
		return false;
	}
}
