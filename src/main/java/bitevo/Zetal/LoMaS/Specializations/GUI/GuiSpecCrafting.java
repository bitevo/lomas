package bitevo.Zetal.LoMaS.Specializations.GUI;

import bitevo.Zetal.LoMaS.Specializations.Inventory.SpecContainerWorkbench;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySpecWorkbench;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiSpecCrafting extends GuiContainer
{
	private static final ResourceLocation craftingTableGuiTextures = new ResourceLocation("textures/gui/container/crafting_table.png");
	private static final ResourceLocation furnaceGuiTextures = new ResourceLocation("textures/gui/container/furnace.png");
	private static TileEntitySpecWorkbench bench;
	private static final String __OBFID = "CL_00000750";

	public GuiSpecCrafting(InventoryPlayer playerInv, World worldIn)
	{
		this(playerInv, worldIn, BlockPos.ORIGIN);
	}

	public GuiSpecCrafting(InventoryPlayer playerInv, World worldIn, BlockPos blockPosition)
	{
		super(new SpecContainerWorkbench(playerInv, (TileEntitySpecWorkbench) worldIn.getTileEntity(blockPosition), worldIn, blockPosition));
		bench = (TileEntitySpecWorkbench) worldIn.getTileEntity(blockPosition);
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		this.fontRendererObj.drawString(I18n.format("container.crafting", new Object[0]), 28, 6, 4210752);
		this.fontRendererObj.drawString(I18n.format("container.inventory", new Object[0]), 8, this.ySize - 96 + 2, 4210752);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(craftingTableGuiTextures);
		int k = (this.width - this.xSize) / 2;
		int l = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);

		this.mc.getTextureManager().bindTexture(furnaceGuiTextures);

		int i1 = this.func_175381_h(24);
		this.drawTexturedModalRect(k + 89, l + 34, 176, 14, i1 + 1, 16);
	}

	private int func_175381_h(int p_175381_1_)
	{
		int j = this.bench.getField(0);
		int k = this.bench.getField(1);
		return k != 0 && j != 0 ? j * p_175381_1_ / k : 0;
	}
}