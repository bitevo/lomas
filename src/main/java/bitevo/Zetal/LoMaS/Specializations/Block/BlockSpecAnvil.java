package bitevo.Zetal.LoMaS.Specializations.Block;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySpecAnvil;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySpecWorkbench;
import net.minecraft.block.BlockAnvil;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.SoundType;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;

public class BlockSpecAnvil extends BlockAnvil implements ITileEntityProvider
{
	public static String[] stringTypes = new String[] {"intact", "slightlyDamaged", "veryDamaged"};
	
	public BlockSpecAnvil()
	{
		super();
		this.setCreativeTab(CreativeTabs.DECORATIONS);
		this.isBlockContainer = true;
		this.setSoundType(SoundType.ANVIL);
	}

	@Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, @Nullable ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		if (worldIn.isRemote)
		{
			return true;
		}
		else
		{
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(playerIn);
			if (lplayer == null || lplayer.getSkills().getMatchingNodesFromName("Anvil repair") < 1)
			{
				LoMaS_Utils.addChatMessage(playerIn, ("You aren't trained to use this."));
				return true;
			}
			
			TileEntity tileentity = worldIn.getTileEntity(pos);
			if (tileentity instanceof TileEntitySpecAnvil)
			{
				TileEntitySpecAnvil tew = (TileEntitySpecAnvil) worldIn.getTileEntity(pos);
				if (tew.getUsingPlayer() == null)
				{
					playerIn.openGui(SpecializationsMod.instance, 3, worldIn, pos.getX(), pos.getY(), pos.getZ());
				}
				else
				{
					LoMaS_Utils.addChatMessage(playerIn, ("Someone is already using this!"));
				}
			}
			return true;
		}
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileEntitySpecAnvil();
	}

	@Override
	public boolean eventReceived(IBlockState state, World worldIn, BlockPos pos, int eventID, int eventParam)
	{
		super.eventReceived(state, worldIn, pos, eventID, eventParam);
		TileEntity tileentity = worldIn.getTileEntity(pos);
		return tileentity == null ? false : tileentity.receiveClientEvent(eventID, eventParam);
	}

	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
	{
		TileEntity tileentity = worldIn.getTileEntity(pos);

		if (tileentity instanceof TileEntitySpecAnvil)
		{
			InventoryHelper.dropInventoryItems(worldIn, pos, (TileEntitySpecAnvil) tileentity);
			worldIn.updateComparatorOutputLevel(pos, this);
		}

		super.breakBlock(worldIn, pos, state);
	}
}
