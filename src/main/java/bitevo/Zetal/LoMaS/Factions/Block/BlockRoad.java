package bitevo.Zetal.LoMaS.Factions.Block;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BlockRoad extends Block
{
	public BlockRoad()
	{
		super(Material.ROCK);
		this.setCreativeTab(LoMaS_Utils.tabLoMaS);
		this.setHardness(4.0f);
	}
}
