package bitevo.Zetal.LoMaS.Factions;

import bitevo.Zetal.LoMaS.Factions.Entity.EntityPigmanWarrior;
import bitevo.Zetal.LoMaS.Factions.GUI.GuiDiplomacy;
import bitevo.Zetal.LoMaS.Factions.GUI.GuiMap;
import bitevo.Zetal.LoMaS.Factions.GUI.GuiWarriorInventory;
import bitevo.Zetal.LoMaS.Factions.Inventory.ContainerWarriorInventory;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class FactionsCommonProxy implements IGuiHandler
{
	public FactionsCommonProxy()
	{
	}

	public void init()
	{
	}

	public void registerRenderInformation()
	{
		// unused server side. -- see ClientProxy for implementation
	}

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		switch (ID)
		{
			case 0:
				return new ContainerWarriorInventory(player.inventory, ((EntityPigmanWarrior) world.getEntityByID(x)).inventory, ((EntityPigmanWarrior) world.getEntityByID(x)), player);
			case 1:
				return null;
			case 2:
				return null;
		}
		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		switch (ID)
		{
			case 0:
				return new GuiWarriorInventory(player.inventory, ((EntityPigmanWarrior) world.getEntityByID(x)).inventory, ((EntityPigmanWarrior) world.getEntityByID(x)));
			case 1:
				return new GuiMap(Minecraft.getMinecraft().currentScreen);
			case 2:
				return new GuiDiplomacy(Minecraft.getMinecraft().currentScreen, player);
		}
		return null;
	}
}
