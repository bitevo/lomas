package bitevo.Zetal.LoMaS.PolyZones;

import java.io.Serializable;
import java.util.HashMap;
import java.util.UUID;

import net.minecraft.entity.player.EntityPlayer;

public class PolyZonePlayer implements Serializable
{
	private UUID uuid;
	private HashMap<String, Access> zoneAccess = new HashMap<String, Access>();
	private boolean isInsideZone = false;
	private String currentZone = "";

	public PolyZonePlayer(UUID n)
	{
		uuid = n;
	}

	public PolyZonePlayer(EntityPlayer p)
	{
		uuid = p.getPersistentID();
	}

	public void setZoneAccessArray(HashMap<String, Access> x)
	{
		zoneAccess.clear();
		for (HashMap.Entry<String, Access> l : x.entrySet())
		{
			zoneAccess.put(l.getKey(), (Access) l.getValue().clone());
		}
	}

	public void addZone(String zoneName, byte permissions)
	{
		zoneAccess.put(zoneName, new Access(permissions));
	}

	public Access getAccess(String zoneName)
	{
		for (HashMap.Entry<String, Access> l : zoneAccess.entrySet())
		{
			if (l.getKey().equals(zoneName))
			{
				return l.getValue();
			}
		}
		return null;
	}

	public void insideNow(String zoneName)
	{
		currentZone = zoneName;
		isInsideZone = true;
	}

	public void outsideNow()
	{
		currentZone = "";
		isInsideZone = false;
	}

	public boolean getIsInsideZone()
	{
		return isInsideZone;
	}

	public String getCurrentZone()
	{
		return currentZone;
	}

	public void setCurrentZone(String zone)
	{
		currentZone = zone;
	}

	public UUID getUUID()
	{
		return uuid;
	}

	public void setUUID(UUID uuid)
	{
		this.uuid = uuid;
	}

	@Override
	public boolean equals(Object o)
	{
		if (o instanceof PolyZonePlayer)
		{
			PolyZonePlayer in = (PolyZonePlayer) o;
			if (in.uuid.equals(this.uuid))
			{
				return true;
			}
		}
		return false;
	}
}
