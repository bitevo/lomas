package bitevo.Zetal.LoMaS.Factions.Handler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import bitevo.Zetal.LoMaS.Factions.FactionsMod;
import bitevo.Zetal.LoMaS.Factions.Entity.EntityPigmanWarrior;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Faction;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class FactionEventHandler
{
	public AttributeModifier roadMod = new AttributeModifier(new UUID(77777777L, 88888888L), "roadmod", 0.035D, 0);

	@SubscribeEvent
	public void onPlayerRoad(LivingUpdateEvent event)
	{
		if (event.getEntityLiving() instanceof EntityPlayer && event.getEntityLiving().ticksExisted % 20 == 0)
		{
			EntityPlayer player = (EntityPlayer) event.getEntityLiving();
			World world = player.worldObj;
			BlockPos pos = new BlockPos(MathHelper.floor_double(player.posX), player.posY, MathHelper.floor_double(player.posZ));
			IBlockState state = world.getBlockState(pos.down());
			boolean isRoad = state.getBlock() == FactionsMod.ROAD || state.getBlock() == FactionsMod.ROAD_SLAB || state.getBlock() == FactionsMod.ROAD_STAIRS || state.getBlock() == FactionsMod.ROAD_DOUBLE_SLAB;
			if (!isRoad)
			{
				state = world.getBlockState(pos);
				isRoad = state.getBlock() == FactionsMod.ROAD || state.getBlock() == FactionsMod.ROAD_SLAB || state.getBlock() == FactionsMod.ROAD_STAIRS || state.getBlock() == FactionsMod.ROAD_DOUBLE_SLAB;
			}
			if (isRoad && player.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getModifier(this.roadMod.getID()) == null)
			{
				player.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).applyModifier(roadMod);
			}
			else if (!isRoad && player.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getModifier(this.roadMod.getID()) != null)
			{
				player.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).removeModifier(roadMod);
			}
		}
	}

	@SubscribeEvent
	public void onSaveWorld(WorldEvent.Save event)
	{
		World world = event.getWorld();
		if (!world.isRemote && event.getWorld().provider.getDimension() == 0)
		{
			File f = world.getMinecraftServer().getDataDirectory();
			File saveDir = new File(f.getPath() + File.separator + "world" + File.separator + "LoMaS" + File.separator);
			saveDir.mkdir();
			File factionsFile = new File(saveDir.getPath() + File.separator + event.getWorld().provider.getDimensionType().getName() + ".fac");
			try
			{
				factionsFile.delete();
				factionsFile.createNewFile();
				FileOutputStream saveOut = new FileOutputStream(factionsFile);
				ObjectOutputStream saveObj = new ObjectOutputStream(saveOut);

				saveObj.writeObject(LoMaS_Faction.getFactionList());

				saveObj.close();
				saveOut.close();
			}
			catch (Exception e1)
			{
				System.err.println("Failed to save factions file at " + factionsFile.getPath() + " for world " + event.getWorld().provider.getDimensionType());
			}
		}
	}

	@SubscribeEvent
	public void onLoadWorld(WorldEvent.Load event)
	{
		World world = event.getWorld();
		if (!world.isRemote && event.getWorld().provider.getDimension() == 0)
		{
			File f = world.getMinecraftServer().getDataDirectory();
			File loadDir = new File(f.getPath() + File.separator + "world" + File.separator + "LoMaS" + File.separator);
			loadDir.mkdir();
			File factionsFile = new File(loadDir.getPath() + File.separator + event.getWorld().provider.getDimensionType().getName() + ".fac");
			try
			{
				FileInputStream loadOut = new FileInputStream(factionsFile);
				ObjectInputStream loadObj = new ObjectInputStream(loadOut);

				HashMap<UUID, LoMaS_Faction> factions = (HashMap<UUID, LoMaS_Faction>) loadObj.readObject();
				LoMaS_Faction.setFactionList(factions);
				
				loadObj.close();
				loadOut.close();
			}
			catch (Exception e1)
			{
				System.err.println("Failed to load factions file at " + factionsFile.getPath() + " for world " + event.getWorld().provider.getDimensionType());
			}
		}
	}
}
