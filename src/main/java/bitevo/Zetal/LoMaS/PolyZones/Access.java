package bitevo.Zetal.LoMaS.PolyZones;

import java.io.Serializable;

public class Access implements Serializable
{
	private byte permission = 62;
	private byte lastpermission = permission;
	private byte defaultPermission = 0;

	public Access()
	{
	}

	public Access(byte l)
	{
		permission = l;
		lastpermission = permission;
	}

	/**
	 * Gets the permissions for this object.
	 * 
	 * @return permission - the integer for permissions
	 */
	public byte getPermissions()
	{
		return permission;
	}

	/**
	 * Deny all access to this zone.
	 */
	public void denyAll()
	{
		lastpermission = permission;
		denyEnter();
		denyInteract();
		denyPlace();
		denyDestroy();
		denyHealthRegen();
	}

	/**
	 * Allow full access to this zone.
	 */
	public void allowAll()
	{
		lastpermission = permission;
		allowEnter(); // done
		allowInteract();
		allowPlace(); // done
		allowDestroy(); // done
		allowHealthRegen(); // done
	}

	/**
	 * Revert the permissions to one before.
	 */
	public void revert()
	{
		byte temp = permission;
		permission = lastpermission;
		lastpermission = temp;
	}

	/**
	 * Allow entering permissions.
	 */
	public void allowEnter()
	{
		lastpermission = permission;
		permission = (byte) (permission & ~2);
	}

	/**
	 * Allow entering permissions.
	 */
	public void denyEnter()
	{
		lastpermission = permission;
		permission = (byte) (permission | 2);
	}

	/**
	 * Deny interaction.
	 */
	public void allowInteract()
	{
		lastpermission = permission;
		permission = (byte) (permission & ~4);
	}

	/**
	 * Allow interaction.
	 */
	public void denyInteract()
	{
		lastpermission = permission;
		permission = (byte) (permission | 4);
	}

	/**
	 * Deny placing of blocks.
	 */
	public void allowPlace()
	{
		lastpermission = permission;
		permission = (byte) (permission & ~8);
	}

	/**
	 * Allow placing of blocks.
	 */
	public void denyPlace()
	{
		lastpermission = permission;
		permission = (byte) (permission | 8);
	}

	/**
	 * Deny destruction of blocks.
	 */
	public void allowDestroy()
	{
		lastpermission = permission;
		permission = (byte) (permission & ~16);
	}

	/**
	 * Allow destruction of blocks.
	 */
	public void denyDestroy()
	{
		lastpermission = permission;
		permission = (byte) (permission | 16);
	}

	/**
	 * Deny health regeneration.
	 */
	public void allowHealthRegen()
	{
		lastpermission = permission;
		permission = (byte) (permission & ~32);
	}

	/**
	 * Allow health regeneration.
	 */
	public void denyHealthRegen()
	{
		lastpermission = permission;
		permission = (byte) (permission | 32);
	}

	/**
	 * Returns a clone of this object with matching zone name and permissions.
	 */
	@Override
	public Object clone()
	{
		Access temp = new Access();
		temp.permission = this.permission;
		return temp;
	}

	public void setPermissionValue(byte x)
	{
		lastpermission = permission;
		permission = x;
	}

	@Override
	public boolean equals(Object o)
	{
		if (o instanceof Access)
		{
			Access in = (Access) o;
			if (in.permission == this.permission)
			{
				return true;
			}
		}
		return false;
	}
}
