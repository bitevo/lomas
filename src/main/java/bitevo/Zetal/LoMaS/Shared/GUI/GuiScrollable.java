package bitevo.Zetal.LoMaS.Shared.GUI;

import java.io.IOException;
import java.util.Random;

import org.lwjgl.input.Mouse;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiOptionButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.resources.I18n;
import net.minecraft.init.Blocks;
import net.minecraft.stats.AchievementList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;

public class GuiScrollable extends GuiScreen
{
    public static int X_MIN = AchievementList.minDisplayColumn * 24 - 112;
    public static int Y_MIN = AchievementList.minDisplayRow * 24 - 112;
    public static int X_MAX = AchievementList.maxDisplayColumn * 24 - 77;
    public static int Y_MAX = AchievementList.maxDisplayRow * 24 - 77;
    protected Block[] backgroundBlocks = new Block[]{Blocks.OBSIDIAN};
	protected static ResourceLocation BACKGROUND = new ResourceLocation("textures/gui/achievement/achievement_background.png");
	protected GuiScreen parentScreen;
	protected int imageWidth = 256;
	protected int imageHeight = 202;
	protected int xLastScroll;
	protected int yLastScroll;
	protected float zoom = 1.0F;
	protected double xScrollO;
	protected double yScrollO;
	protected double xScrollP;
	protected double yScrollP;
	protected double xScrollTarget;
	protected double yScrollTarget;
	private int scrolling;
	public String title;
	public int scrollXDiff, scrollYDiff;
	public static int extraOffsetX = 152;
	public static int extraOffsetY = 192;

	protected boolean isConfirming = false;
	public static int clickStartX, clickStartY;

	public GuiScrollable(GuiScreen parentScreenIn, int[] minMaxX, int[] minMaxY)
	{
		this.X_MIN = minMaxX[0] - 256;
		this.X_MAX = minMaxX[1] - 256;
		this.Y_MIN = minMaxY[0] - 256;
		this.Y_MAX = minMaxY[1] - 256;
		
		//System.out.println(X_MIN + " " + X_MAX + " " + Y_MIN + " " + Y_MAX);
		this.parentScreen = parentScreenIn;
		this.xScrollTarget = (double) ((X_MIN + X_MAX)/2) + extraOffsetX;
		this.xScrollO = this.xScrollTarget;
		this.xScrollP = this.xScrollTarget;
		this.yScrollTarget = (double) ((Y_MIN + Y_MAX)/2) + extraOffsetY;
		this.yScrollO = this.yScrollTarget;
		this.yScrollP = this.yScrollTarget;
	}
	
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
    {
    	super.mouseClicked(mouseX, mouseY, mouseButton);
        if (mouseButton == 0)
        {
        	this.clickStartX = mouseX;
        	this.clickStartY = mouseY;
        }
    }

    /**
     * Called when a mouse button is released.
     */
    protected void mouseReleased(int mouseX, int mouseY, int state)
    {
    	super.mouseReleased(mouseX, mouseY, state);
        if (!isConfirming && state == 0)
        {
        	if(Math.abs(this.clickStartX - mouseX) <= 5 && Math.abs(this.clickStartY - mouseY) <= 5)
        	{
        		trueMouseClick(mouseX, mouseY);
        	}
        }
    }

	protected void trueMouseClick(int mouseX, int mouseY)
	{
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question. Called when the GUI is displayed and when the window resizes, the buttonList is cleared beforehand.
	 */
	public void initGui()
	{
		this.buttonList.clear();
		this.buttonList.add(new GuiOptionButton(1, this.width / 2 + 44, this.height / 2 + 74, 60, 20, I18n.format("gui.done", new Object[0])));
	}

	/**
	 * Called by the controls from the buttonList when activated. (Mouse pressed for buttons)
	 */
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if (button.id == 1)
		{
			this.mc.displayGuiScreen(this.parentScreen);
		}
	}

	/**
	 * Fired when a key is typed (except F11 which toggles full screen). This is the equivalent of KeyListener.keyTyped(KeyEvent e). Args : character (character on the key), keyCode (lwjgl Keyboard
	 * key code)
	 */
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		if (this.mc.gameSettings.keyBindInventory.isActiveAndMatches(keyCode))
		{
			this.mc.displayGuiScreen((GuiScreen) null);
			this.mc.setIngameFocus();
		}
		else
		{
			super.keyTyped(typedChar, keyCode);
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		if (!isConfirming && Mouse.isButtonDown(0))
		{
			int i = (this.width - this.imageWidth) / 2;
			int j = (this.height - this.imageHeight) / 2;
			int k = i + 8;
			int l = j + 17;

			if ((this.scrolling == 0 || this.scrolling == 1) && mouseX >= k && mouseX < k + 224 && mouseY >= l && mouseY < l + 155)
			{
				if (this.scrolling == 0)
				{
					this.scrolling = 1;
				}
				else
				{
					this.xScrollP -= (double) ((float) (mouseX - this.xLastScroll) * this.zoom);
					this.yScrollP -= (double) ((float) (mouseY - this.yLastScroll) * this.zoom);

			        if (this.xScrollP < (double)X_MIN)
			        {
			            this.xScrollP = (double)X_MIN;
			        }

			        if (this.yScrollP < (double)Y_MIN)
			        {
			            this.yScrollP = (double)Y_MIN;
			        }

			        if (this.xScrollP >= (double)X_MAX)
			        {
			            this.xScrollP = (double)(X_MAX - 1);
			        }

			        if (this.yScrollP >= (double)Y_MAX)
			        {
			            this.yScrollP = (double)(Y_MAX - 1);
			        }
			        
					this.xScrollO = this.xScrollP;
					this.yScrollO = this.yScrollP;
					this.xScrollTarget = this.xScrollP;
					this.yScrollTarget = this.yScrollP;
				}

				this.xLastScroll = mouseX;
				this.yLastScroll = mouseY;
			}
		}
		else
		{
			this.scrolling = 0;
		}

		int i1 = Mouse.getDWheel();
		float prevZoom = this.zoom;

		if (!isConfirming && i1 < 0)
		{
			this.zoom += 0.25F;
		}
		else if (!isConfirming && i1 > 0)
		{
			this.zoom -= 0.25F;
		}

		this.zoom = MathHelper.clamp_float(this.zoom, 1.5F, 3.0F);

		if (this.zoom != prevZoom)
		{
			float prevZoomWidth = prevZoom * (float) this.imageWidth;
			float prevZoomHeight = prevZoom * (float) this.imageHeight;
			float zoomWidth = this.zoom * (float) this.imageWidth;
			float zoomHeight = this.zoom * (float) this.imageHeight;
			this.xScrollP -= (double) ((zoomWidth - prevZoomWidth) * 0.5F);
			this.yScrollP -= (double) ((zoomHeight - prevZoomHeight) * 0.5F);
			this.xScrollO = this.xScrollP;
			this.yScrollO = this.yScrollP;
			this.xScrollTarget = this.xScrollP;
			this.yScrollTarget = this.yScrollP;
		}
		
        if (this.xScrollTarget < (double)X_MIN)
        {
            this.xScrollTarget = (double)X_MIN;
        }

        if (this.yScrollTarget < (double)Y_MIN)
        {
            this.yScrollTarget = (double)Y_MIN;
        }

        if (this.xScrollTarget >= (double)X_MAX)
        {
            this.xScrollTarget = (double)(X_MAX - 1);
        }

        if (this.yScrollTarget >= (double)Y_MAX)
        {
            this.yScrollTarget = (double)(Y_MAX - 1);
        }

		this.updateScrollDiffs(partialTicks);
		this.drawDefaultBackground();
		this.drawScrollableScreen(mouseX, mouseY, partialTicks);
		GlStateManager.disableLighting();
		GlStateManager.disableDepth();
		this.drawTitle();
		GlStateManager.enableLighting();
		GlStateManager.enableDepth();
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	public void updateScreen()
	{
		this.xScrollO = this.xScrollP;
		this.yScrollO = this.yScrollP;
		double d0 = this.xScrollTarget - this.xScrollP;
		double d1 = this.yScrollTarget - this.yScrollP;

		if (d0 * d0 + d1 * d1 < 4.0D)
		{
			this.xScrollP += d0;
			this.yScrollP += d1;
		}
		else
		{
			this.xScrollP += d0 * 0.85D;
			this.yScrollP += d1 * 0.85D;
		}
	}

	protected void drawTitle()
	{
		int i = (this.width - this.imageWidth) / 2;
		int j = (this.height - this.imageHeight) / 2;
		this.fontRendererObj.drawString(title, i + 15, j + 5, 4210752);
	}

	protected void drawScrollableScreen(int mouseX, int mouseY, float partialTicks)
	{
		int i = MathHelper.floor_double(this.xScrollO + (this.xScrollP - this.xScrollO) * (double) partialTicks);
		int j = MathHelper.floor_double(this.yScrollO + (this.yScrollP - this.yScrollO) * (double) partialTicks);

        if (i < X_MIN)
        {
            i = X_MIN;
        }

        if (j < Y_MIN)
        {
            j = Y_MIN;
        }

        if (i >= X_MAX)
        {
            i = X_MAX - 1;
        }

        if (j >= Y_MAX)
        {
            j = Y_MAX - 1;
        }

		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		int i1 = k + 16;
		int j1 = l + 17;
		this.zLevel = 0.0F;
		GlStateManager.depthFunc(518);
		GlStateManager.pushMatrix();
		GlStateManager.translate((float) i1, (float) j1, -200.0F);
		GlStateManager.scale(1.0F / this.zoom, 1.0F / this.zoom, 1.0F);
		GlStateManager.enableTexture2D();
		GlStateManager.disableLighting();
		GlStateManager.enableRescaleNormal();
		GlStateManager.enableColorMaterial();
		int xShift = i + 288 >> 4;
		int yShift = j + 288 >> 4;
		int xMod = (i + 288) % 16;
		int yMod = (j + 288) % 16;
		Random random = new Random();
		float zoomScalar = 16.0F / this.zoom;

		for (int yOffset = 0; (float) yOffset * zoomScalar - (float) yMod < 155.0F; ++yOffset)
		{
			for (int xOffset = 0; (float) xOffset * zoomScalar - (float) xMod < 224.0F; ++xOffset)
			{
				random.setSeed((long) (this.mc.getSession().getPlayerID().hashCode() + xShift + xOffset + (yShift + yOffset) * 16));
				int j4 = random.nextInt(1 + yShift + yOffset + Math.abs(this.Y_MAX) + Math.abs(this.X_MAX)) + (yShift + yOffset) / 2;
				int len = this.backgroundBlocks.length;
				
				TextureAtlasSprite textureatlassprite = this.getTexture(this.backgroundBlocks[0]);
				if(j4 % 2 == 0)
				{
					textureatlassprite = this.getTexture(this.backgroundBlocks[j4 % len]);
				}
				else if (j4 % 3 == 0)
				{
					textureatlassprite = this.getTexture(this.backgroundBlocks[j4 % len]);
				}
				else if (j4 % 5 == 0)
				{
					textureatlassprite = this.getTexture(this.backgroundBlocks[j4 % len]);
				}

				this.mc.getTextureManager().bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
				this.drawTexturedModalRect(xOffset * 16 - xMod, yOffset * 16 - yMod, textureatlassprite, 16, 16);
			}
		}
		
		GlStateManager.enableDepth();
		GlStateManager.depthFunc(515);
		
		this.drawScrollableContent(mouseX, mouseY, partialTicks);
		
		this.mc.getTextureManager().bindTexture(BACKGROUND);
		
		float f3 = (float) (mouseX - i1) * this.zoom;
		float f4 = (float) (mouseY - j1) * this.zoom;
		RenderHelper.enableGUIStandardItemLighting();
		GlStateManager.disableLighting();
		GlStateManager.enableRescaleNormal();
		GlStateManager.enableColorMaterial();
		GlStateManager.disableDepth();
		GlStateManager.enableBlend();
		GlStateManager.popMatrix();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(BACKGROUND);
		this.drawTexturedModalRect(k, l, 0, 0, this.imageWidth, this.imageHeight);
		this.zLevel = 0.0F;
		GlStateManager.depthFunc(515);
		GlStateManager.disableDepth();
		GlStateManager.enableTexture2D();
		super.drawScreen(mouseX, mouseY, partialTicks);

		this.drawScrollableOverlay(mouseX, mouseY, partialTicks);
		
		GlStateManager.enableDepth();
		GlStateManager.enableLighting();
		RenderHelper.disableStandardItemLighting();
	}

	public void drawScrollableContent(int mouseX, int mouseY, float partialTicks){}

	public void drawScrollableOverlay(int mouseX, int mouseY, float partialTicks){}

	private TextureAtlasSprite getTexture(Block blockIn)
	{
		return Minecraft.getMinecraft().getBlockRendererDispatcher().getBlockModelShapes().getTexture(blockIn.getDefaultState());
	}

	/**
	 * Returns true if this GUI should pause the game when it is displayed in single-player
	 */
	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
	
	public boolean isMouseWithinRect(int mouseX, int mouseY, int x, int y, int width)
	{
        int k = (this.width - this.imageWidth) / 2;
        int l = (this.height - this.imageHeight) / 2;
        int i1 = k + 16;
        int j1 = l + 17;
        float f3 = (float)(mouseX - i1) * this.zoom;
        float f4 = (float)(mouseY - j1) * this.zoom;
        
        if (f3 >= (float)x && f3 <= (float)(x + width) && f4 >= (float)y && f4 <= (float)(y + width))
        {
        	return true;
        }
		return false;
	}
	
	public void updateScrollDiffs(float partialTicks)
	{
        int i = MathHelper.floor_double(this.xScrollO + (this.xScrollP - this.xScrollO) * (double)partialTicks);
        int j = MathHelper.floor_double(this.yScrollO + (this.yScrollP - this.yScrollO) * (double)partialTicks);
        this.scrollXDiff = i;
        this.scrollYDiff = j;
	}
	
	public boolean areCoordsVisible(int x, int y)
	{
		if(x >= -16 && y >= -16 && (float)x <= 224.0F * this.zoom && (float)y <= 155.0F * this.zoom)
		{
			return true;
		}
		return false;
	}
	
	public boolean areCoordsVisible(int x, int y, int width, int height)
	{
		if(((x >= -16 && y >= -16) || (x + width >= -16 && y + height >= -16)) && (float)x <= 224.0F * this.zoom && (float)y <= 155.0F * this.zoom)
		{
			return true;
		}
		return false;
	}
	
	public int[] getCoordsFromPixels(int x, int y)
	{
		return new int[]{x - this.scrollXDiff, y - this.scrollYDiff};
	}
	
	/**
	 * @param column
	 * @param partialTicks
	 * @return x coord
	 */
	public int getCoordFromColumn(int column)
	{
		return column * 24 - this.scrollXDiff;
	}

	/**
	 * @param row
	 * @param partialTicks
	 * @return y coord
	 */
	public int getCoordFromRow(int row)
	{
		return row * 24 - this.scrollYDiff;
	}
}
