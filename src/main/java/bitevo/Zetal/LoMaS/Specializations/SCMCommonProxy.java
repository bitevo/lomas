package bitevo.Zetal.LoMaS.Specializations;

import bitevo.Zetal.LoMaS.Specializations.GUI.GuiSpecBrewingStand;
import bitevo.Zetal.LoMaS.Specializations.GUI.GuiSpecCrafting;
import bitevo.Zetal.LoMaS.Specializations.GUI.GuiSpecFurnace;
import bitevo.Zetal.LoMaS.Specializations.GUI.GuiSpecInventory;
import bitevo.Zetal.LoMaS.Specializations.GUI.GuiSpecRepair;
import bitevo.Zetal.LoMaS.Specializations.Inventory.SpecContainerBrewingStand;
import bitevo.Zetal.LoMaS.Specializations.Inventory.SpecContainerFurnace;
import bitevo.Zetal.LoMaS.Specializations.Inventory.SpecContainerRepair;
import bitevo.Zetal.LoMaS.Specializations.Inventory.SpecContainerWorkbench;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySpecAnvil;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySpecFurnace;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySpecWorkbench;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class SCMCommonProxy implements IGuiHandler
{
	public SCMCommonProxy()
	{
	}

	public void init()
	{
	}

	public void registerRenderInformation()
	{
		// unused server side. -- see ClientProxy for implementation
	}

	public void regiterBlockStateMappers()
	{
	}

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		switch (ID)
		{
			case 0:
				return new SpecContainerBrewingStand(player.inventory, (IInventory) world.getTileEntity(new BlockPos(x, y, z)));
			case 1:
				return new SpecContainerWorkbench(player.inventory, (TileEntitySpecWorkbench) world.getTileEntity(new BlockPos(x, y, z)), world, new BlockPos(x, y, z));
			case 3:
				return new SpecContainerRepair(player.inventory, world, new BlockPos(x, y, z), player, (TileEntitySpecAnvil) world.getTileEntity(new BlockPos(x, y, z)));
			case 4:
				return new SpecContainerFurnace(player.inventory, (TileEntitySpecFurnace) world.getTileEntity(new BlockPos(x, y, z)));
		}
		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		switch (ID)
		{
			case 0:
				return new GuiSpecBrewingStand(player.inventory, (IInventory) world.getTileEntity(new BlockPos(x, y, z)));
			case 1:
				return new GuiSpecCrafting(player.inventory, world, new BlockPos(x, y, z));
			case 2:
				return new GuiSpecInventory(player);
			case 3:
				return new GuiSpecRepair(player.inventory, new BlockPos(x, y, z), world, (TileEntitySpecAnvil) world.getTileEntity(new BlockPos(x, y, z)));
			case 4:
				return new GuiSpecFurnace(player.inventory, (TileEntitySpecFurnace) world.getTileEntity(new BlockPos(x, y, z)));
		}
		return null;
	}
}
