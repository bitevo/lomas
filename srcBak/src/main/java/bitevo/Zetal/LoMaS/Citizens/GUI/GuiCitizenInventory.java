package bitevo.Zetal.LoMaS.Citizens.GUI;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import bitevo.Zetal.LoMaS.Citizens.CitizensMod;
import bitevo.Zetal.LoMaS.Citizens.Entities.EntityPigman;
import bitevo.Zetal.LoMaS.Citizens.Inventory.ContainerCitizenInventory;

@SideOnly(Side.CLIENT)
public class GuiCitizenInventory extends GuiContainer
{
    private static final ResourceLocation citizenGuiTexture = new ResourceLocation(CitizensMod.modid, "gui/citizen.png");
    /** The player inventory bound to this GUI. */
    private IInventory playerInventory;
    /** The horse inventory bound to this GUI. */
    private IInventory citizenInventory;
    private EntityPigman citizenEntity;
    /** The mouse x-position recorded during the last rendered frame. */
    private float mousePosx;
    /** The mouse y-position recorded during the last renderered frame. */
    private float mousePosY;
	public static final String[] desiresTranslated = { "Bookshelf", "Jukebox", "Flowerpot", "Poppy", "Dandelion",
		"Glasspane", "Glass", "Carpet", "Bush", "Bed" };

    public GuiCitizenInventory(IInventory playerInv, IInventory citInv, EntityPigman cit)
    {
        super(new ContainerCitizenInventory(playerInv, citInv, cit, Minecraft.getMinecraft().thePlayer));
        this.playerInventory = playerInv;
        this.citizenInventory = citInv;
        this.citizenEntity = cit;
        this.allowUserInput = false;
    }

    /**
     * Draw the foreground layer for the GuiContainer (everything in front of the items). Args : mouseX, mouseY
     */
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
    {
        this.fontRendererObj.drawString(this.citizenInventory.getDisplayName().getUnformattedText(), 8, 6, 4210752);
        this.fontRendererObj.drawString(this.playerInventory.getDisplayName().getUnformattedText(), 8, this.ySize - 96 + 2, 4210752);
        this.fontRendererObj.drawString("Job: " + this.citizenEntity.getJob(), 83, 21, 4210752);
        this.fontRendererObj.drawString("Happiness: " + this.citizenEntity.getHappiness(), 83, 33, 4210752);
        this.fontRendererObj.drawString("Want: " + this.desiresTranslated[this.citizenEntity.getDesire()], 83, 45, 4210752);
        this.fontRendererObj.drawString("Hate: " + this.citizenEntity.getUnhappyReason(), 83, 57, 4210752);
        if(this.citizenEntity.getHomeChest() != null)
        {
            //this.fontRendererObj.drawString("Home:" + (int) this.citizenEntity.getHomeChest().getX() + "x" + (int) this.citizenEntity.getHomeChest().getY() + "y" + (int) this.citizenEntity.getHomeChest().getZ() + "z", 83, 57, 4210752);
        }
    }

    /**
     * Args : renderPartialTicks, mouseX, mouseY
     */
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
    {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(citizenGuiTexture);
        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
        GuiInventory.drawEntityOnScreen(k + 51, l + 68, 23, (float)(k + 51) - this.mousePosx, (float)(l + 75 - 50) - this.mousePosY, this.citizenEntity);
    }

    /**
     * Draws the screen and all the components in it. Args : mouseX, mouseY, renderPartialTicks
     */
    public void drawScreen(int mouseX, int mouseY, float partialTicks)
    {
        this.mousePosx = (float)mouseX;
        this.mousePosY = (float)mouseY;
        super.drawScreen(mouseX, mouseY, partialTicks);
    }
}