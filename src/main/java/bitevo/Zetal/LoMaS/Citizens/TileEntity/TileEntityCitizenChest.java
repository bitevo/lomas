package bitevo.Zetal.LoMaS.Citizens.TileEntity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import bitevo.Zetal.LoMaS.Citizens.Entity.EntityPigmanCitizen;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import net.minecraft.block.Block;
import net.minecraft.block.BlockChest;
import net.minecraft.block.BlockChest.Type;
import net.minecraft.block.state.IBlockState;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ContainerChest;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryLargeChest;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityLockable;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.ReportedException;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEntityCitizenChest extends TileEntityLockable implements ITickable, IInventory
{
	public UUID owner;
	/** Determines if the check for adjacent chests has taken place. */
	public boolean adjacentChestChecked;
	/** Contains the chest tile located adjacent to this one (if any) */
	public TileEntityCitizenChest adjacentChestZNeg;
	/** Contains the chest tile located adjacent to this one (if any) */
	public TileEntityCitizenChest adjacentChestXPos;
	/** Contains the chest tile located adjacent to this one (if any) */
	public TileEntityCitizenChest adjacentChestXNeg;
	/** Contains the chest tile located adjacent to this one (if any) */
	public TileEntityCitizenChest adjacentChestZPos;
	/** The current angle of the lid (between 0 and 1) */
	public float lidAngle;
	/** The angle of the lid last tick */
	public float prevLidAngle;
	/** The number of players currently using this chest */
	public int numPlayersUsing;
	/** Server sync counter (once per 20 ticks) */
	private int ticksSinceSync;
	private Type cachedChestType;
	public ArrayList<UUID> citizens = new ArrayList();
	private ItemStack[] chestContents = new ItemStack[27];
	private String customName;

	public void updateBlock()
	{
		if (citizens.isEmpty())
		{
			if (owner != null)
			{
				EntityPlayer player = this.worldObj.getPlayerEntityByUUID(owner);
				if (player != null)
				{
					LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
					if (lplayer != null)
					{
						System.out.println("!!!!!!!!!!!!!!!!!!!!!!Spawning Citizen!!!!!!!!!!!!!!!!!!!!!!!");
						EntityPigmanCitizen cit = new EntityPigmanCitizen(this.worldObj, this.pos);
						cit.getDataManager().set(EntityPigmanCitizen.HOME_X, this.pos.getX());
						cit.getDataManager().set(EntityPigmanCitizen.HOME_Y, this.pos.getY());
						cit.getDataManager().set(EntityPigmanCitizen.HOME_Z, this.pos.getZ());
						cit.setPosition(this.pos.getX(), this.pos.getY(), this.pos.getZ());
						cit.motionX = this.worldObj.rand.nextDouble() - 1.0D;
						cit.motionY = this.worldObj.rand.nextDouble() - 1.0D;
						cit.motionZ = this.worldObj.rand.nextDouble() - 1.0D;
						this.citizens.add(cit.getPersistentID());
						this.worldObj.spawnEntityInWorld(cit);
						lplayer.getCitizens().add(cit.getPersistentID());
					}
				}
			}
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public AxisAlignedBB getRenderBoundingBox()
	{
		AxisAlignedBB bb = new AxisAlignedBB(getPos().add(-1, 0, -1), getPos().add(2, 2, 2));
		return bb;
	}

	/**
	 * Returns the stack in slot i
	 */
	@Override
	public ItemStack getStackInSlot(int index)
	{
		return this.chestContents[index];
	}

	/**
	 * Removes from an inventory slot (first arg) up to a specified number (second arg) of items and returns them in a new stack.
	 */
	@Override
	public ItemStack decrStackSize(int index, int count)
	{
		if (this.chestContents[index] != null)
		{
			ItemStack itemstack;

			if (this.chestContents[index].stackSize <= count)
			{
				itemstack = this.chestContents[index];
				this.chestContents[index] = null;
				return itemstack;
			}
			else
			{
				itemstack = this.chestContents[index].splitStack(count);

				if (this.chestContents[index].stackSize == 0)
				{
					this.chestContents[index] = null;
				}
				return itemstack;
			}
		}
		else
		{
			return null;
		}
	}

	/**
	 * When some containers are closed they call this on each slot, then drop whatever it returns as an EntityItem - like when you close a workbench GUI.
	 */
	@Override
	public ItemStack removeStackFromSlot(int index)
	{
		if (this.chestContents[index] != null)
		{
			ItemStack itemstack = this.chestContents[index];
			this.chestContents[index] = null;
			return itemstack;
		}
		else
		{
			return null;
		}
	}

	/**
	 * Sets the given item stack to the specified slot in the inventory (can be crafting or armor sections).
	 */
	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		this.chestContents[index] = stack;

		if (stack != null && stack.stackSize > this.getInventoryStackLimit())
		{
			stack.stackSize = this.getInventoryStackLimit();
		}
	}

	/**
	 * Gets the name of this command sender (usually username, but possibly "Rcon")
	 */
	@Override
	public String getName()
	{
		return this.hasCustomName() ? this.customName : "container.chest";
	}

	/**
	 * Returns true if this thing is named
	 */
	@Override
	public boolean hasCustomName()
	{
		return this.customName != null && this.customName.length() > 0;
	}

	public void setCustomName(String name)
	{
		this.customName = name;
	}

	public boolean addItemStackToInventory(final ItemStack p_70441_1_)
	{
		if (p_70441_1_ != null && p_70441_1_.stackSize != 0 && p_70441_1_.getItem() != null)
		{
			try
			{
				int i;

				if (p_70441_1_.isItemDamaged())
				{
					i = this.getFirstEmptyStack();

					if (i >= 0)
					{
						this.chestContents[i] = ItemStack.copyItemStack(p_70441_1_);
						this.chestContents[i].animationsToGo = 5;
						p_70441_1_.stackSize = 0;
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					do
					{
						i = p_70441_1_.stackSize;
						p_70441_1_.stackSize = this.storePartialItemStack(p_70441_1_);
					} while (p_70441_1_.stackSize > 0 && p_70441_1_.stackSize < i);
					return p_70441_1_.stackSize < i;
				}
			}
			catch (Throwable throwable)
			{
				CrashReport crashreport = CrashReport.makeCrashReport(throwable, "Adding item to inventory");
				CrashReportCategory crashreportcategory = crashreport.makeCategory("Item being added");
				crashreportcategory.addCrashSection("Item ID", Integer.valueOf(Item.getIdFromItem(p_70441_1_.getItem())));
				crashreportcategory.addCrashSection("Item data", Integer.valueOf(p_70441_1_.getMetadata()));
				crashreportcategory.addCrashSectionThrowable("Item name", new Throwable()
				{
					private static final String __OBFID = "CL_00001710";

					public String call()
					{
						return p_70441_1_.getDisplayName();
					}
				});
				throw new ReportedException(crashreport);
			}
		}
		else
		{
			return false;
		}
	}

	private int storePartialItemStack(ItemStack p_70452_1_)
	{
		Item item = p_70452_1_.getItem();
		int i = p_70452_1_.stackSize;
		int j = this.storeItemStack(p_70452_1_);

		if (j < 0)
		{
			j = this.getFirstEmptyStack();
		}

		if (j < 0)
		{
			return i;
		}
		else
		{
			if (this.chestContents[j] == null)
			{
				this.chestContents[j] = new ItemStack(item, 0, p_70452_1_.getMetadata());

				if (p_70452_1_.hasTagCompound())
				{
					this.chestContents[j].setTagCompound((NBTTagCompound) p_70452_1_.getTagCompound().copy());
				}
			}

			int k = i;

			if (i > this.chestContents[j].getMaxStackSize() - this.chestContents[j].stackSize)
			{
				k = this.chestContents[j].getMaxStackSize() - this.chestContents[j].stackSize;
			}

			if (k > this.getInventoryStackLimit() - this.chestContents[j].stackSize)
			{
				k = this.getInventoryStackLimit() - this.chestContents[j].stackSize;
			}

			if (k == 0)
			{
				return i;
			}
			else
			{
				i -= k;
				this.chestContents[j].stackSize += k;
				this.chestContents[j].animationsToGo = 5;
				return i;
			}
		}
	}

	private int storeItemStack(ItemStack p_70432_1_)
	{
		for (int i = 0; i < this.chestContents.length; ++i)
		{
			if (this.chestContents[i] != null && this.chestContents[i].getItem() == p_70432_1_.getItem() && this.chestContents[i].isStackable() && this.chestContents[i].stackSize < this.chestContents[i].getMaxStackSize() && this.chestContents[i].stackSize < this.getInventoryStackLimit() && (!this.chestContents[i].getHasSubtypes() || this.chestContents[i].getMetadata() == p_70432_1_.getMetadata()) && ItemStack.areItemStackTagsEqual(this.chestContents[i], p_70432_1_))
			{
				return i;
			}
		}

		return -1;
	}

	/**
	 * Returns the first item stack that is empty.
	 */
	public int getFirstEmptyStack()
	{
		for (int i = 0; i < this.chestContents.length; ++i)
		{
			if (this.chestContents[i] == null)
			{
				return i;
			}
		}

		return -1;
	}

	@Override
	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newSate)
	{
		return oldState.getBlock() != newSate.getBlock();
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound)
	{
		super.writeToNBT(compound);
		NBTTagList nbttaglist = new NBTTagList();

		for (int i = 0; i < this.chestContents.length; ++i)
		{
			if (this.chestContents[i] != null)
			{
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte) i);
				this.chestContents[i].writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}

		compound.setTag("Items", nbttaglist);

		if (this.hasCustomName())
		{
			compound.setString("CustomName", this.customName);
		}

		NBTTagList nbttaglist2 = new NBTTagList();
		for (int i = 0; i < this.citizens.size(); ++i)
		{
			if (this.citizens.get(i) != null)
			{
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setString("Citizen", this.citizens.get(i).toString());
				nbttaglist2.appendTag(nbttagcompound1);
			}
		}
		compound.setTag("Citizens", nbttaglist2);
		if (this.owner != null)
		{
			compound.setString("owner", this.owner.toString());
			// System.out.println("Setting owner UUID " + this.owner);
		}
		else
		{
			// System.out.println("Null writing owner UUID " + this.owner);
		}
		// System.out.println("Finished writing");
		return compound;
	}

	@Override
	public void readFromNBT(NBTTagCompound compound)
	{
		super.readFromNBT(compound);
		NBTTagList nbttaglist = compound.getTagList("Items", 10);
		this.chestContents = new ItemStack[this.getSizeInventory()];

		if (compound.hasKey("CustomName", 8))
		{
			this.customName = compound.getString("CustomName");
		}

		for (int i = 0; i < nbttaglist.tagCount(); ++i)
		{
			NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
			int j = nbttagcompound1.getByte("Slot") & 255;

			if (j >= 0 && j < this.chestContents.length)
			{
				this.chestContents[j] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
			}
		}

		NBTTagList nbttaglist2 = compound.getTagList("Citizens", 10);
		for (int i = 0; i < nbttaglist2.tagCount(); ++i)
		{
			NBTTagCompound nbttagcompound1 = nbttaglist2.getCompoundTagAt(i);
			this.citizens.add(UUID.fromString(nbttagcompound1.getString("Citizen")));
		}
		try
		{
			// System.out.println("reading owner UUID " + this.owner);
			this.owner = UUID.fromString(compound.getString("owner"));
		}
		catch (Exception e)
		{
			// System.out.println("failed reading owner UUID " + this.owner);
		}
		// System.out.println("Finished reading");
	}

	@Override
	public String getGuiID()
	{
		return "minecraft:chest";
	}

	@Override
	public Container createContainer(InventoryPlayer playerInventory, EntityPlayer playerIn)
	{
		return new ContainerChest(playerInventory, this, playerIn);
	}

	@Override
	public int getField(int id)
	{
		return 0;
	}

	@Override
	public void setField(int id, int value)
	{
	}

	@Override
	public int getFieldCount()
	{
		return 0;
	}

	@Override
	public void clear()
	{
		for (int i = 0; i < this.chestContents.length; ++i)
		{
			this.chestContents[i] = null;
		}
	}

	private void func_174910_a(TileEntityCitizenChest p_174910_1_, EnumFacing p_174910_2_)
	{
		if (p_174910_1_.isInvalid())
		{
			this.adjacentChestChecked = false;
		}
		else if (this.adjacentChestChecked)
		{
			switch (TileEntityCitizenChest.SwitchEnumFacing.field_177366_a[p_174910_2_.ordinal()])
			{
				case 1:
					if (this.adjacentChestZNeg != p_174910_1_)
					{
						this.adjacentChestChecked = false;
					}

					break;
				case 2:
					if (this.adjacentChestZPos != p_174910_1_)
					{
						this.adjacentChestChecked = false;
					}

					break;
				case 3:
					if (this.adjacentChestXPos != p_174910_1_)
					{
						this.adjacentChestChecked = false;
					}

					break;
				case 4:
					if (this.adjacentChestXNeg != p_174910_1_)
					{
						this.adjacentChestChecked = false;
					}
			}
		}
	}

	/**
	 * Performs the check for adjacent chests to determine if this chest is double or not.
	 */
	public void checkForAdjacentChests()
	{
		if (!this.adjacentChestChecked)
		{
			this.adjacentChestChecked = true;
			this.adjacentChestXNeg = this.func_174911_a(EnumFacing.WEST);
			this.adjacentChestXPos = this.func_174911_a(EnumFacing.EAST);
			this.adjacentChestZNeg = this.func_174911_a(EnumFacing.NORTH);
			this.adjacentChestZPos = this.func_174911_a(EnumFacing.SOUTH);
		}
	}

	protected TileEntityCitizenChest func_174911_a(EnumFacing p_174911_1_)
	{
		BlockPos blockpos = this.pos.offset(p_174911_1_);

		if (this.func_174912_b(blockpos))
		{
			TileEntity tileentity = this.worldObj.getTileEntity(blockpos);

			if (tileentity instanceof TileEntityCitizenChest)
			{
				TileEntityCitizenChest tileentitychest = (TileEntityCitizenChest) tileentity;
				tileentitychest.func_174910_a(this, p_174911_1_.getOpposite());
				return tileentitychest;
			}
		}

		return null;
	}

	private boolean func_174912_b(BlockPos p_174912_1_)
	{
		if (this.worldObj == null)
		{
			return false;
		}
		else
		{
			Block block = this.worldObj.getBlockState(p_174912_1_).getBlock();
			return block instanceof BlockChest && ((BlockChest) block).chestType == this.getChestType();
		}
	}

	/**
	 * invalidates a tile entity
	 */
	@Override
	public void invalidate()
	{
		super.invalidate();
		this.updateContainingBlockInfo();
		this.checkForAdjacentChests();
	}

	public BlockChest.Type getChestType()
	{
		if (this.cachedChestType == null)
		{
			if (this.worldObj == null || !(this.getBlockType() instanceof BlockChest))
			{
				return BlockChest.Type.BASIC;
			}

			this.cachedChestType = ((BlockChest) this.getBlockType()).chestType;
		}

		return this.cachedChestType;
	}

	/**
	 * Updates the JList with a new model.
	 */
	@Override
	public void update()
	{
		this.checkForAdjacentChests();
		int i = this.pos.getX();
		int j = this.pos.getY();
		int k = this.pos.getZ();
		++this.ticksSinceSync;
		float f;

		if (!this.worldObj.isRemote && this.numPlayersUsing != 0 && (this.ticksSinceSync + i + j + k) % 200 == 0)
		{
			this.numPlayersUsing = 0;
			f = 5.0F;
			List list = this.worldObj.getEntitiesWithinAABB(EntityPlayer.class, new AxisAlignedBB((double) ((float) i - f), (double) ((float) j - f), (double) ((float) k - f), (double) ((float) (i + 1) + f), (double) ((float) (j + 1) + f), (double) ((float) (k + 1) + f)));
			Iterator iterator = list.iterator();

			while (iterator.hasNext())
			{
				EntityPlayer entityplayer = (EntityPlayer) iterator.next();

				if (entityplayer.openContainer instanceof ContainerChest)
				{
					IInventory iinventory = ((ContainerChest) entityplayer.openContainer).getLowerChestInventory();

					if (iinventory == this || iinventory instanceof InventoryLargeChest && ((InventoryLargeChest) iinventory).isPartOfLargeChest(this))
					{
						++this.numPlayersUsing;
					}
				}
			}
		}

		this.prevLidAngle = this.lidAngle;
		f = 0.1F;
		double d2;

		if (this.numPlayersUsing > 0 && this.lidAngle == 0.0F && this.adjacentChestZNeg == null && this.adjacentChestXNeg == null)
		{
			double d1 = (double) i + 0.5D;
			d2 = (double) k + 0.5D;

			if (this.adjacentChestZPos != null)
			{
				d2 += 0.5D;
			}

			if (this.adjacentChestXPos != null)
			{
				d1 += 0.5D;
			}

			this.worldObj.playSound((EntityPlayer) null, d1, (double) j + 0.5D, d2, SoundEvents.BLOCK_CHEST_OPEN, SoundCategory.BLOCKS, 0.5F, this.worldObj.rand.nextFloat() * 0.1F + 0.9F);
		}

		if (this.numPlayersUsing == 0 && this.lidAngle > 0.0F || this.numPlayersUsing > 0 && this.lidAngle < 1.0F)
		{
			float f1 = this.lidAngle;

			if (this.numPlayersUsing > 0)
			{
				this.lidAngle += f;
			}
			else
			{
				this.lidAngle -= f;
			}

			if (this.lidAngle > 1.0F)
			{
				this.lidAngle = 1.0F;
			}

			float f2 = 0.5F;

			if (this.lidAngle < f2 && f1 >= f2 && this.adjacentChestZNeg == null && this.adjacentChestXNeg == null)
			{
				d2 = (double) i + 0.5D;
				double d0 = (double) k + 0.5D;

				if (this.adjacentChestZPos != null)
				{
					d0 += 0.5D;
				}

				if (this.adjacentChestXPos != null)
				{
					d2 += 0.5D;
				}

				this.worldObj.playSound((EntityPlayer) null, d2, (double) j + 0.5D, d0, SoundEvents.BLOCK_CHEST_CLOSE, SoundCategory.BLOCKS, 0.5F, this.worldObj.rand.nextFloat() * 0.1F + 0.9F);
			}

			if (this.lidAngle < 0.0F)
			{
				this.lidAngle = 0.0F;
			}
		}
	}

	@Override
	public boolean receiveClientEvent(int id, int type)
	{
		if (id == 1)
		{
			this.numPlayersUsing = type;
			return true;
		}
		else
		{
			return super.receiveClientEvent(id, type);
		}
	}

	@Override
	public void openInventory(EntityPlayer player)
	{
		if (!player.isSpectator())
		{
			if (this.numPlayersUsing < 0)
			{
				this.numPlayersUsing = 0;
			}

			++this.numPlayersUsing;
			this.worldObj.addBlockEvent(this.pos, this.getBlockType(), 1, this.numPlayersUsing);
			this.worldObj.notifyNeighborsOfStateChange(this.pos, this.getBlockType());
			this.worldObj.notifyNeighborsOfStateChange(this.pos.down(), this.getBlockType());
		}
	}

	@Override
	public void closeInventory(EntityPlayer player)
	{
		if (!player.isSpectator() && this.getBlockType() instanceof BlockChest)
		{
			--this.numPlayersUsing;
			this.worldObj.addBlockEvent(this.pos, this.getBlockType(), 1, this.numPlayersUsing);
			this.worldObj.notifyNeighborsOfStateChange(this.pos, this.getBlockType());
			this.worldObj.notifyNeighborsOfStateChange(this.pos.down(), this.getBlockType());
		}
	}

	/**
	 * Returns true if automation is allowed to insert the given stack (ignoring stack size) into the given slot.
	 */
	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		return true;
	}

	@Override
	public int getSizeInventory()
	{
		return 27;
	}

	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player)
	{
		return this.worldObj.getTileEntity(this.pos) != this ? false : player.getDistanceSq((double) this.pos.getX() + 0.5D, (double) this.pos.getY() + 0.5D, (double) this.pos.getZ() + 0.5D) <= 64.0D;
	}

	@Override
	public void updateContainingBlockInfo()
	{
		super.updateContainingBlockInfo();
		this.adjacentChestChecked = false;
	}

	@Override
	public SPacketUpdateTileEntity getUpdatePacket()
	{
		return new SPacketUpdateTileEntity(pos, 1, this.getUpdateTag());
	}

	@Override
	public NBTTagCompound getUpdateTag()
	{
		return this.writeToNBT(new NBTTagCompound());
	}

	static final class SwitchEnumFacing
	{
		static final int[] field_177366_a = new int[EnumFacing.values().length];
		private static final String __OBFID = "CL_00002041";

		static
		{
			try
			{
				field_177366_a[EnumFacing.NORTH.ordinal()] = 1;
			}
			catch (NoSuchFieldError var4)
			{
				;
			}

			try
			{
				field_177366_a[EnumFacing.SOUTH.ordinal()] = 2;
			}
			catch (NoSuchFieldError var3)
			{
				;
			}

			try
			{
				field_177366_a[EnumFacing.EAST.ordinal()] = 3;
			}
			catch (NoSuchFieldError var2)
			{
				;
			}

			try
			{
				field_177366_a[EnumFacing.WEST.ordinal()] = 4;
			}
			catch (NoSuchFieldError var1)
			{
				;
			}
		}
	}
}
