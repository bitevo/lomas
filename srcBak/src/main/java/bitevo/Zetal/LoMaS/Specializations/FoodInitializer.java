package bitevo.Zetal.LoMaS.Specializations;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockBeetroot;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockCactus;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockCake;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockCarrot;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockCrops;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockFarmland;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockHay;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockMelon;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockNetherWart;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockPotato;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockPumpkin;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockReed;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockStem;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemFertilizer;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemFishFood;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemFood;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemSeedFood;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemSeeds;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemSoup;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemWheat;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemBlock.ItemQualityBlock;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntityPlant;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntityQualityBlock;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.MobEffects;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlockSpecial;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;

public class FoodInitializer
{
	public static String[] qualities = new String[] { "Poor", "Decent", "Superb" };
	public static int[] qualities_meta = new int[] { 0, 1, 2 };

	public static Block CACTUS = (new BlockCactus()).setHardness(0.4F).setUnlocalizedName("cactus");
	public static Block CARROTS = (new BlockCarrot()).setUnlocalizedName("carrots");
	public static Block POTATOES = (new BlockPotato()).setUnlocalizedName("potatoes");
	public static Block FARMLAND = (new BlockFarmland()).setHardness(0.6F).setUnlocalizedName("farmland");
	public static Block WHEAT_BLOCK = (new BlockCrops()).setUnlocalizedName("crops");
	public static Block NETHER_WART_BLOCK = (new BlockNetherWart()).setUnlocalizedName("netherStalk");
	public static Block MELON_STEM = (new BlockStem(FoodInitializer.MELON_BLOCK)).setHardness(0.0F).setUnlocalizedName("pumpkinStem");
	public static Block PUMPKIN_STEM = (new BlockStem(FoodInitializer.PUMPKIN)).setHardness(0.0F).setUnlocalizedName("pumpkinStem");
	public static Block REEDS_BLOCK = (new BlockReed()).setHardness(0.0F).setUnlocalizedName("reeds");
	public static Block HAY_BLOCK = (new BlockHay()).setHardness(0.5F).setUnlocalizedName("hayBlock").setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
	public static Block MELON_BLOCK = (new BlockMelon()).setHardness(1.0F).setUnlocalizedName("melon");
	public static Block PUMPKIN = (new BlockPumpkin()).setHardness(1.0F).setUnlocalizedName("pumpkin");
	//public static BlockMelonRotted MELONROTBLOCK = (BlockMelonRotted) (new BlockMelonRotted()).setHardness(1.2F).setCreativeTab(CreativeTabs.MISC).setUnlocalizedName("melonRot");
	//public static BlockPumpkinRotted PUMPKINROTBLOCK = (BlockPumpkinRotted) (new BlockPumpkinRotted()).setHardness(1.2F).setCreativeTab(CreativeTabs.MISC).setUnlocalizedName("pumpkinRot");
	public static Block CAKE_BLOCK = (new BlockCake()).setHardness(0.5F).setUnlocalizedName("cake");
	public static Block BEETROOTS = (new BlockBeetroot()).setUnlocalizedName("beetroots").setCreativeTab(CreativeTabs.FOOD);

	public static Item REEDS = (new ItemBlockSpecial(FoodInitializer.REEDS_BLOCK)).setUnlocalizedName("reeds").setCreativeTab(CreativeTabs.MATERIALS);
	public static Item WHEAT_SEEDS = (new ItemSeeds(FoodInitializer.WHEAT_BLOCK, FoodInitializer.FARMLAND)).setUnlocalizedName("seeds");
	public static Item NETHER_WART = (new ItemSeeds(FoodInitializer.NETHER_WART_BLOCK, Blocks.SOUL_SAND)).setUnlocalizedName("netherStalkSeeds");
	public static Item MELON_SEEDS = (new ItemSeeds(FoodInitializer.MELON_STEM, FoodInitializer.FARMLAND)).setUnlocalizedName("seeds_melon");
	public static Item PUMPKIN_SEEDS = (new ItemSeeds(FoodInitializer.PUMPKIN_STEM, FoodInitializer.FARMLAND)).setUnlocalizedName("seeds_pumpkin");
	public static Item CARROT_SEEDS = (new ItemSeeds(FoodInitializer.CARROTS, FoodInitializer.FARMLAND)).setUnlocalizedName("seeds_carrots");
	public static Item BEETROOT_SEEDS = (new ItemSeeds(FoodInitializer.BEETROOTS, FoodInitializer.FARMLAND)).setUnlocalizedName("beetroot_seeds");

	public static Item CARROT = (new ItemFood(3, 0.6F, false)).setUnlocalizedName("carrots");
	public static Item POTATO = (new ItemSeedFood(0, 0.0F, FoodInitializer.POTATOES, FoodInitializer.FARMLAND)).setUnlocalizedName("potato").setCreativeTab(CreativeTabs.MATERIALS);
	public static Item BEETROOT = (new ItemFood(1, 0.6F, false)).setUnlocalizedName("beetroot");

	public static Item WHEAT = (new ItemWheat()).setUnlocalizedName("wheat").setCreativeTab(CreativeTabs.MATERIALS);
	public static Item BREAD = (new ItemFood(5, 0.6F, false)).setUnlocalizedName("bread");
	public static Item PORKCHOP = (new ItemFood(3, 0.3F, true)).setUnlocalizedName("porkchopRaw");
	public static Item COOKED_PORKCHOP = (new ItemFood(8, 0.8F, true)).setUnlocalizedName("porkchopCooked");
	public static Item COOKIE = (new ItemFood(2, 0.1F, false)).setUnlocalizedName("cookie");
	public static Item MELON = (new ItemFood(2, 0.3F, false)).setUnlocalizedName("melon");
	public static Item BEEF = (new ItemFood(3, 0.3F, true)).setUnlocalizedName("beefRaw");
	public static Item COOKED_BEEF = (new ItemFood(8, 0.8F, true)).setUnlocalizedName("beefCooked");
	public static Item CHICKEN = (new ItemFood(2, 0.3F, true)).setPotionEffect(new PotionEffect(MobEffects.HUNGER, 600, 0), 0.3F).setUnlocalizedName("chickenRaw");
	public static Item COOKED_CHICKEN = (new ItemFood(6, 0.6F, true)).setUnlocalizedName("chickenCooked");
	public static Item BAKED_POTATO = (new ItemFood(5, 0.6F, false)).setUnlocalizedName("potatoBaked");
	public static Item GOLDEN_CARROT = (new ItemFood(6, 1.2F, false)).setUnlocalizedName("carrotGolden").setCreativeTab(CreativeTabs.BREWING);
	public static Item PUMPKIN_PIE = (new ItemFood(8, 0.3F, false)).setUnlocalizedName("pumpkinPie").setCreativeTab(CreativeTabs.FOOD);
	public static Item RABBIT = (new ItemFood(3, 0.3F, true)).setUnlocalizedName("rabbitRaw");
	public static Item COOKED_RABBIT = (new ItemFood(5, 0.6F, true)).setUnlocalizedName("rabbitCooked");
	public static Item MUTTON = (new ItemFood(2, 0.3F, true)).setUnlocalizedName("muttonRaw");
	public static Item COOKED_MUTTON = (new ItemFood(6, 0.8F, true)).setUnlocalizedName("muttonCooked");
	public static Item FISH = (new ItemFishFood(false)).setUnlocalizedName("fish").setHasSubtypes(true);
	public static Item COOKED_FISH = (new ItemFishFood(true)).setUnlocalizedName("fish").setHasSubtypes(true);
	public static Item MUSHROOM_STEW = (new ItemSoup(6)).setUnlocalizedName("mushroomStew");
	public static Item RABBIT_STEW = (new ItemSoup(10)).setUnlocalizedName("rabbitStew");
	public static Item APPLE = (new ItemFood(4, 0.3F, false)).setUnlocalizedName("apple");
	public static Item ROTTEN_FLESH = (new ItemFood(4, 0.1F, true)).setPotionEffect(new PotionEffect(MobEffects.HUNGER, 600, 0), 0.8F).setUnlocalizedName("rottenFlesh");
	public static Item POISONOUS_POTATO = (new ItemFood(2, 0.3F, false)).setPotionEffect(new PotionEffect(MobEffects.HUNGER, 600, 0), 0.3F).setUnlocalizedName("potatoPoisonous");
	public static Item SPIDER_EYE = (new ItemFood(2, 0.8F, false)).setPotionEffect(new PotionEffect(MobEffects.POISON, 100, 0), 1.0F).setUnlocalizedName("spiderEye");
	public static Item CAKE = (new ItemQualityBlock(FoodInitializer.CAKE_BLOCK)).setMaxStackSize(1).setUnlocalizedName("cake");
	public static Item BEETROOT_SOUP = (new ItemSoup(6)).setUnlocalizedName("beetroot_soup");

	public static Item FERTILIZER = (new ItemFertilizer()).setUnlocalizedName("fertilizer");

	public static void init()
	{
		GameRegistry.registerTileEntity(TileEntityPlant.class, "TileEntityPlant");
		GameRegistry.registerTileEntity(TileEntityQualityBlock.class, "TileEntityQualityBlock");

		MinecraftForge.addGrassSeed(new ItemStack(FoodInitializer.CARROT_SEEDS, 1), 2);
		MinecraftForge.addGrassSeed(new ItemStack(FoodInitializer.MELON_SEEDS, 1), 2);
		MinecraftForge.addGrassSeed(new ItemStack(FoodInitializer.PUMPKIN_SEEDS, 1), 2);
		MinecraftForge.addGrassSeed(new ItemStack(FoodInitializer.POTATO, 1), 2);

		LoMaS_Utils.registerItem(WHEAT_SEEDS.setRegistryName( "wheat_seeds"));
		LoMaS_Utils.registerItem(CARROT.setRegistryName( "carrot"), qualities_meta);
		OreDictionary.registerOre("cropCarrot", new ItemStack(CARROT, 1, OreDictionary.WILDCARD_VALUE));
		LoMaS_Utils.registerItem(POTATO.setRegistryName( "potato"), qualities_meta);
		OreDictionary.registerOre("cropPotato", new ItemStack(POTATO, 1, OreDictionary.WILDCARD_VALUE));
		LoMaS_Utils.registerItem(BEETROOT.setRegistryName( "beetroot"), qualities_meta);
		OreDictionary.registerOre("cropBeetroot", new ItemStack(BEETROOT, 1, OreDictionary.WILDCARD_VALUE));
		LoMaS_Utils.registerItem(MELON_SEEDS.setRegistryName( "melon_seeds"));
		LoMaS_Utils.registerItem(PUMPKIN_SEEDS.setRegistryName( "pumpkin_seeds"));
		LoMaS_Utils.registerItem(CARROT_SEEDS.setRegistryName( "carrot_seeds"));
		LoMaS_Utils.registerItem(BEETROOT_SEEDS.setRegistryName( "beetroot_seeds"));
		
		LoMaS_Utils.registerBlock(CAKE_BLOCK.setRegistryName( "cake"), CAKE.setRegistryName( "cake"), qualities_meta);
		LoMaS_Utils.registerBlock(CACTUS.setRegistryName( "cactus"));
		OreDictionary.registerOre("blockCactus", CACTUS);
		LoMaS_Utils.registerBlock(CARROTS.setRegistryName( "carrots"), null);
		LoMaS_Utils.registerBlock(POTATOES.setRegistryName( "potatoes"), null);
		LoMaS_Utils.registerBlock(BEETROOTS.setRegistryName( "beetroots"), null);
		LoMaS_Utils.registerBlock(WHEAT_BLOCK.setRegistryName( "wheat"), WHEAT.setRegistryName( "wheat"), qualities_meta);
		OreDictionary.registerOre("cropWheat", new ItemStack(WHEAT, 1, OreDictionary.WILDCARD_VALUE));
		LoMaS_Utils.registerBlock(FARMLAND.setRegistryName( "farmland"), null);
		LoMaS_Utils.registerBlock(REEDS_BLOCK.setRegistryName( "reeds"), REEDS.setRegistryName( "reeds"), qualities_meta);
		OreDictionary.registerOre("sugarcane", new ItemStack(REEDS, 1, OreDictionary.WILDCARD_VALUE));
		LoMaS_Utils.registerBlock(NETHER_WART_BLOCK.setRegistryName( "nether_wart"), NETHER_WART.setRegistryName( "nether_wart"));
		OreDictionary.registerOre("cropNetherWart", NETHER_WART);
		LoMaS_Utils.registerBlock(MELON_STEM.setRegistryName( "melon_stem"), null);
		LoMaS_Utils.registerBlock(PUMPKIN_STEM.setRegistryName( "pumpkin_stem"), null);
		LoMaS_Utils.registerBlock(HAY_BLOCK.setRegistryName( "hay_block"), new ItemQualityBlock(HAY_BLOCK).setRegistryName("hay_block"), qualities_meta);
		LoMaS_Utils.registerBlock(PUMPKIN.setRegistryName( "pumpkin"), new ItemQualityBlock(PUMPKIN).setRegistryName("pumpkin"),  qualities_meta);
		LoMaS_Utils.registerBlock(MELON_BLOCK.setRegistryName( "melon_block"), new ItemQualityBlock(MELON_BLOCK).setRegistryName("melon_block"),  qualities_meta);

		LoMaS_Utils.registerItem(BREAD.setRegistryName( "bread"), qualities_meta);
		LoMaS_Utils.registerItem(PORKCHOP.setRegistryName( "porkchop"), qualities_meta);
		LoMaS_Utils.registerItem(COOKED_PORKCHOP.setRegistryName( "cooked_porkchop"), qualities_meta);
		LoMaS_Utils.registerItem(COOKIE.setRegistryName( "cookie"), qualities_meta);
		LoMaS_Utils.registerItem(MELON.setRegistryName( "melon"), qualities_meta);
		LoMaS_Utils.registerItem(BEEF.setRegistryName( "beef"), qualities_meta);
		LoMaS_Utils.registerItem(COOKED_BEEF.setRegistryName( "cooked_beef"), qualities_meta);
		LoMaS_Utils.registerItem(CHICKEN.setRegistryName( "chicken"), qualities_meta);
		LoMaS_Utils.registerItem(COOKED_CHICKEN.setRegistryName( "cooked_chicken"), qualities_meta);
		LoMaS_Utils.registerItem(BAKED_POTATO.setRegistryName( "baked_potato"), qualities_meta);
		LoMaS_Utils.registerItem(GOLDEN_CARROT.setRegistryName( "golden_carrot"), qualities_meta);
		LoMaS_Utils.registerItem(PUMPKIN_PIE.setRegistryName( "pumpkin_pie"), qualities_meta);
		LoMaS_Utils.registerItem(RABBIT.setRegistryName( "rabbit"), qualities_meta);
		LoMaS_Utils.registerItem(COOKED_RABBIT.setRegistryName( "cooked_rabbit"), qualities_meta);
		LoMaS_Utils.registerItem(MUTTON.setRegistryName( "mutton"), qualities_meta);
		LoMaS_Utils.registerItem(COOKED_MUTTON.setRegistryName( "cooked_mutton"), qualities_meta);
		LoMaS_Utils.registerItem(FERTILIZER.setRegistryName( "fertilizer"), qualities_meta);

		LoMaS_Utils.registerItem(FISH.setRegistryName( "fish"), getFishTextureMap());
		LoMaS_Utils.registerItem(COOKED_FISH.setRegistryName( "cooked_fish"), getCookedFishTextureMap());
		LoMaS_Utils.registerItem(MUSHROOM_STEW.setRegistryName( "mushroom_stew"), qualities_meta);
		LoMaS_Utils.registerItem(RABBIT_STEW.setRegistryName( "rabbit_stew"), qualities_meta);
		LoMaS_Utils.registerItem(APPLE.setRegistryName( "apple"), qualities_meta);
		LoMaS_Utils.registerItem(ROTTEN_FLESH.setRegistryName( "rotten_flesh"), qualities_meta);
		LoMaS_Utils.registerItem(POISONOUS_POTATO.setRegistryName( "poisonous_potato"), qualities_meta);
		LoMaS_Utils.registerItem(SPIDER_EYE.setRegistryName( "spider_eye"), qualities_meta);
		LoMaS_Utils.registerItem(BEETROOT_SOUP.setRegistryName( "beetroot_soup"), qualities_meta);

		Blocks.FIRE.setFireInfo(FoodInitializer.HAY_BLOCK, 60, 20);

		Blocks.CACTUS.setCreativeTab(null);
		Blocks.FARMLAND.setCreativeTab(null);
		Blocks.CARROTS.setCreativeTab(null);
		Blocks.WHEAT.setCreativeTab(null);
		Blocks.NETHER_WART.setCreativeTab(null);
		Blocks.POTATOES.setCreativeTab(null);
		Blocks.REEDS.setCreativeTab(null);
		Blocks.MELON_STEM.setCreativeTab(null);
		Blocks.PUMPKIN_STEM.setCreativeTab(null);
		Blocks.HAY_BLOCK.setCreativeTab(null);
		Blocks.MELON_BLOCK.setCreativeTab(null);
		Blocks.PUMPKIN.setCreativeTab(null);
		Blocks.CAKE.setCreativeTab(null);

		Items.CARROT.setCreativeTab(null);
		Items.WHEAT.setCreativeTab(null);
		Items.WHEAT_SEEDS.setCreativeTab(null);
		Items.NETHER_WART.setCreativeTab(null);
		Items.POTATO.setCreativeTab(null);
		Items.REEDS.setCreativeTab(null);
		Items.MELON_SEEDS.setCreativeTab(null);
		Items.PUMPKIN_SEEDS.setCreativeTab(null);
		Items.BREAD.setCreativeTab(null);
		Items.PORKCHOP.setCreativeTab(null);
		Items.COOKED_PORKCHOP.setCreativeTab(null);
		Items.COOKIE.setCreativeTab(null);
		Items.MELON.setCreativeTab(null);
		Items.BEEF.setCreativeTab(null);
		Items.COOKED_BEEF.setCreativeTab(null);
		Items.CHICKEN.setCreativeTab(null);
		Items.COOKED_CHICKEN.setCreativeTab(null);
		Items.BAKED_POTATO.setCreativeTab(null);
		Items.GOLDEN_CARROT.setCreativeTab(null);
		Items.PUMPKIN_PIE.setCreativeTab(null);
		Items.RABBIT.setCreativeTab(null);
		Items.COOKED_RABBIT.setCreativeTab(null);
		Items.MUTTON.setCreativeTab(null);
		Items.COOKED_MUTTON.setCreativeTab(null);

		Items.FISH.setCreativeTab(null);
		Items.COOKED_FISH.setCreativeTab(null);
		Items.MUSHROOM_STEW.setCreativeTab(null);
		Items.RABBIT_STEW.setCreativeTab(null);
		Items.APPLE.setCreativeTab(null);
		Items.ROTTEN_FLESH.setCreativeTab(null);
		Items.POISONOUS_POTATO.setCreativeTab(null);
		Items.SPIDER_EYE.setCreativeTab(null);
		Items.CAKE.setCreativeTab(null);
		Items.BEETROOT.setCreativeTab(null);
		Items.BEETROOT_SOUP.setCreativeTab(null);
		Items.BEETROOT_SEEDS.setCreativeTab(null);

		List<IRecipe> recipes = CraftingManager.getInstance().getRecipeList();
		Iterator<IRecipe> iter = recipes.iterator();
		while (iter.hasNext())
		{
			ItemStack is = iter.next().getRecipeOutput();
			if (is != null &&
					(is.getItem() == Items.BREAD
							|| is.getItem() == Items.COOKIE
							|| is.getItem() == Items.GOLDEN_CARROT
							|| is.getItem() == Items.PUMPKIN_PIE
							|| is.getItem() == Items.WHEAT
							|| is.getItem() == Items.CAKE
							|| is.getItem() == Items.MUSHROOM_STEW
							|| is.getItem() == Items.RABBIT_STEW
							|| is.getItem() == Items.BEETROOT_SOUP
							|| is.getItem() == Item.getItemFromBlock(Blocks.MELON_BLOCK)
							|| is.getItem() == Item.getItemFromBlock(Blocks.HAY_BLOCK)))
			{
				iter.remove();
			}
		}

		FurnaceRecipes.instance().getSmeltingList().remove(Items.POTATO);
		FurnaceRecipes.instance().getSmeltingList().remove(Items.CHICKEN);
		FurnaceRecipes.instance().getSmeltingList().remove(Items.MUTTON);
		FurnaceRecipes.instance().getSmeltingList().remove(Items.PORKCHOP);
		FurnaceRecipes.instance().getSmeltingList().remove(Items.RABBIT);
		FurnaceRecipes.instance().getSmeltingList().remove(Items.BEEF);
		FurnaceRecipes.instance().getSmeltingList().remove(Items.FISH);

		GameRegistry.addShapelessRecipe(new ItemStack(FoodInitializer.FERTILIZER, 1), new Object[] {Blocks.DIRT, new ItemStack(Items.DYE, 1, 15) });
		//GameRegistry.addShapelessRecipe(new ItemStack(FoodInitializer.fertilizer, 1), new Object[] {Blocks.dirt, new ItemStack(FoodInitializer.wheat, 1, OreDictionary.WILDCARD_VALUE)});
		GameRegistry.addRecipe(new ItemStack(Items.SPECKLED_MELON, 1), new Object[] { "XXX", "X#X", "XXX", 'X', Items.GOLD_NUGGET, '#', new ItemStack(FoodInitializer.MELON, 1, OreDictionary.WILDCARD_VALUE) });
		GameRegistry.addRecipe(new ItemStack(Blocks.LIT_PUMPKIN, 1), new Object[] { "A", "B", 'A', new ItemStack(FoodInitializer.PUMPKIN, 1, OreDictionary.WILDCARD_VALUE), 'B', Blocks.TORCH });
		GameRegistry.addRecipe(new ItemStack(FoodInitializer.PUMPKIN_SEEDS, 4), new Object[] { "X", 'X', new ItemStack(FoodInitializer.PUMPKIN, 1, OreDictionary.WILDCARD_VALUE) });
		GameRegistry.addRecipe(new ItemStack(FoodInitializer.MELON_SEEDS, 1), new Object[] { "X", 'X', new ItemStack(FoodInitializer.MELON, 1, OreDictionary.WILDCARD_VALUE) });
		GameRegistry.addShapelessRecipe(new ItemStack(FoodInitializer.MUSHROOM_STEW, 1, 0), new Object[] { Blocks.BROWN_MUSHROOM, Blocks.RED_MUSHROOM, Items.BOWL });
		GameRegistry.addShapelessRecipe(new ItemStack(Items.FERMENTED_SPIDER_EYE), new Object[] { FoodInitializer.SPIDER_EYE, Blocks.BROWN_MUSHROOM, Items.SUGAR });
		GameRegistry.addShapelessRecipe(new ItemStack(Items.DYE, 1, EnumDyeColor.RED.getDyeDamage()), new Object[] {new ItemStack(FoodInitializer.BEETROOT, 1)});

		for (int j = 0; j < FoodInitializer.qualities.length; ++j)
		{
			GameRegistry.addRecipe(new ItemStack(FoodInitializer.BEETROOT_SOUP), new Object[] {"OOO", "OOO", " B ", 'O', FoodInitializer.BEETROOT, 'B', Items.BOWL});
			GameRegistry.addRecipe(new ItemStack(FoodInitializer.RABBIT_STEW, 1, j), new Object[] { " R ", "CPM", " B ", 'R', new ItemStack(FoodInitializer.COOKED_RABBIT, 1, j), 'C', new ItemStack(FoodInitializer.CARROT, 1, j), 'P', new ItemStack(FoodInitializer.BAKED_POTATO, 1, j), 'M', Blocks.BROWN_MUSHROOM, 'B', Items.BOWL });
			GameRegistry.addRecipe(new ItemStack(FoodInitializer.RABBIT_STEW, 1, j), new Object[] { " R ", "CPD", " B ", 'R', new ItemStack(FoodInitializer.COOKED_RABBIT, 1, j), 'C', new ItemStack(FoodInitializer.CARROT, 1, j), 'P', new ItemStack(FoodInitializer.BAKED_POTATO, 1, j), 'D', Blocks.RED_MUSHROOM, 'B', Items.BOWL });
			GameRegistry.addRecipe(new ItemStack(FoodInitializer.COOKIE, 8, j), new Object[] { "#X#", 'X', new ItemStack(Items.DYE, 1, EnumDyeColor.BROWN.getDyeDamage()), '#', new ItemStack(FoodInitializer.WHEAT, 1, j) });
			GameRegistry.addShapelessRecipe(new ItemStack(FoodInitializer.PUMPKIN_PIE, 1, j), new Object[] { new ItemStack(FoodInitializer.PUMPKIN, 1, j), Items.SUGAR, Items.EGG });
			GameRegistry.addRecipe(new ItemStack(FoodInitializer.CAKE, 1, j), new Object[] { "AAA", "BEB", "CCC", 'A', Items.MILK_BUCKET, 'B', Items.SUGAR, 'C', new ItemStack(FoodInitializer.WHEAT, 1, j), 'E', Items.EGG });
			// GameRegistry.addRecipe(new ItemStack(FoodInitializer.sugar, 1, j), new Object[] {"#", '#', new ItemStack(FoodInitializer.reeds, 1, j)});
			GameRegistry.addRecipe(new ItemStack(FoodInitializer.GOLDEN_CARROT, 1, j), new Object[] { "###", "#X#", "###", '#', Items.GOLD_NUGGET, 'X', new ItemStack(FoodInitializer.CARROT, 1, j) });
			GameRegistry.addRecipe(new ItemStack(FoodInitializer.MELON_BLOCK, 1, j), new Object[] { "###", "###", "###", '#', new ItemStack(FoodInitializer.MELON, 1, j) });
			GameRegistry.addRecipe(new ItemStack(FoodInitializer.HAY_BLOCK, 1, j), new Object[] { "###", "###", "###", '#', new ItemStack(FoodInitializer.WHEAT, 1, j) });
			GameRegistry.addRecipe(new ItemStack(FoodInitializer.WHEAT, 9, j), new Object[] { "#", '#', new ItemStack(FoodInitializer.HAY_BLOCK, 1, j) });
			GameRegistry.addRecipe(new ItemStack(FoodInitializer.BREAD, 1, j), new Object[] { "###", '#', new ItemStack(FoodInitializer.WHEAT, 1, j) });
			FurnaceRecipes.instance().addSmeltingRecipe(new ItemStack(FoodInitializer.FISH, 1, j), new ItemStack(FoodInitializer.COOKED_FISH, 1, j), 0.35F);
			FurnaceRecipes.instance().addSmeltingRecipe(new ItemStack(FoodInitializer.FISH, 1, j+3), new ItemStack(FoodInitializer.COOKED_FISH, 1, j+3), 0.35F);
			FurnaceRecipes.instance().addSmeltingRecipe(new ItemStack(FoodInitializer.POTATO, 1, j), new ItemStack(FoodInitializer.BAKED_POTATO, 1, j), 0.35F);
			FurnaceRecipes.instance().addSmeltingRecipe(new ItemStack(FoodInitializer.CHICKEN, 1, j), new ItemStack(FoodInitializer.COOKED_CHICKEN, 1, j), 0.35F);
			FurnaceRecipes.instance().addSmeltingRecipe(new ItemStack(FoodInitializer.MUTTON, 1, j), new ItemStack(FoodInitializer.COOKED_MUTTON, 1, j), 0.35F);
			FurnaceRecipes.instance().addSmeltingRecipe(new ItemStack(FoodInitializer.PORKCHOP, 1, j), new ItemStack(FoodInitializer.COOKED_PORKCHOP, 1, j), 0.35F);
			FurnaceRecipes.instance().addSmeltingRecipe(new ItemStack(FoodInitializer.RABBIT, 1, j), new ItemStack(FoodInitializer.COOKED_RABBIT, 1, j), 0.35F);
			FurnaceRecipes.instance().addSmeltingRecipe(new ItemStack(FoodInitializer.BEEF, 1, j), new ItemStack(FoodInitializer.COOKED_BEEF, 1, j), 0.35F);
		}
	}
	
	public static HashMap getFishTextureMap()
	{
		HashMap map = new HashMap();
        ItemFishFood.FishType[] afishtype = ItemFishFood.FishType.values();
        int i = afishtype.length;

        int data = 0;
        for (int j = 0; j < i; ++j)
        {
            ItemFishFood.FishType fishtype = afishtype[j];
            for (int k = 0; k < FoodInitializer.qualities.length; ++k)
            {
            	//int data = (fishtype.getMetadata() << 2) | k;
                map.put(data, fishtype.getUnlocalizedName().toLowerCase());
                data++;
            }
        }
		return map;
	}
	
	public static HashMap getCookedFishTextureMap()
	{
		HashMap map = new HashMap();
        ItemFishFood.FishType[] afishtype = ItemFishFood.FishType.values();
        int i = afishtype.length;

        int data = 0;
        for (int j = 0; j < i; ++j)
        {
            ItemFishFood.FishType fishtype = afishtype[j];
            if(fishtype.canCook())
            {
                for (int k = 0; k < FoodInitializer.qualities.length; ++k)
                {
                	//int data = (fishtype.getMetadata() << 2) | k;
                    map.put(data, "cooked_" + fishtype.getUnlocalizedName().toLowerCase());
                    data++;
                }
            }
        }
		return map;
	}
}
