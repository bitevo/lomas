package bitevo.Zetal.LoMaS.Specializations.Message;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemFertilizer;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemSeedFood;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemSeeds;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntityPlant;

public class TimedUsageMessageHandler implements IMessageHandler<TimedUsageMessage, IMessage>
{
	@Override
	public IMessage onMessage(TimedUsageMessage message, MessageContext ctx)
	{
		if (ctx.side == Side.CLIENT)
		{
			handleClientTimedUsageMessage(message);
		}
		else
		{
			handleServerTimedUsageMessage(message);
		}
		return null;
	}

	@SideOnly(Side.CLIENT)
	private void handleClientTimedUsageMessage(TimedUsageMessage packet)
	{
	}

	private void handleServerTimedUsageMessage(TimedUsageMessage packet)
	{
		EntityPlayer player = (EntityPlayer) SpecializationsMod.server.getEntityFromUuid(packet.username);
		BlockPos pos = packet.pos;
		ItemStack stack = player.inventory.getStackInSlot(packet.stack);
		World world = player.worldObj;
		if (stack.getItem() instanceof ItemFertilizer && ItemFertilizer.canBeFertilized(world, pos))
		{
			TileEntityPlant tep = (TileEntityPlant) world.getTileEntity(pos);
			tep.setFertilized(true);
			tep.setQuality(tep.getQuality() + 20 + (world.rand.nextInt(5) - 5));
			tep.markDirty();
			world.notifyBlockUpdate(pos, world.getBlockState(pos), world.getBlockState(pos), 3);
			if(!player.capabilities.isCreativeMode)
			{
		    	stack.stackSize--;
				LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer((EntityPlayer) player);
				if (lplayer != null && lplayer.specClass.equalsIgnoreCase("farmer"))
				{
					lplayer.addSpecProgress(1.0f);
				}
			}
		}
		else if ((stack.getItem() instanceof ItemSeeds || stack.getItem() instanceof ItemSeedFood) 
		&& SpecializationsMod.canSustainPlant(world, pos.down(), EnumFacing.UP, (IPlantable) stack.getItem()))
		{
			world.setBlockState(pos, ((IPlantable)stack.getItem()).getPlant(world, pos));
			if(world.getTileEntity(pos) != null && world.getTileEntity(pos) instanceof TileEntityPlant && player instanceof EntityPlayer)
			{
				TileEntityPlant tile = (TileEntityPlant) world.getTileEntity(pos);
				tile.setOwner(player.getPersistentID());
				LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer((EntityPlayer) player);
				if (lplayer != null && lplayer.specClass.equalsIgnoreCase("farmer"))
				{
					tile.setFarmerLevel((byte) LoMaS_Player.getLoMaSPlayer((EntityPlayer) player).classlevel);
				}
			}
			world.notifyBlockUpdate(pos, world.getBlockState(pos), world.getBlockState(pos), 3);
			if(!player.capabilities.isCreativeMode)
			{
		    	stack.stackSize--;
			}
		}
		else
		{
			player.stopActiveHand();
		}
	}
}
