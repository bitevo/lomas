package bitevo.Zetal.LoMaS.Specializations.Message;
import io.netty.buffer.ByteBuf;

import java.util.UUID;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class AbilityMessage implements IMessage
{
	public UUID uuid;
	public int counter = -1;
	public boolean isActive = false;
	
    public AbilityMessage(){}
	
    public AbilityMessage(EntityPlayerSP player)
    {
    	uuid = player.getPersistentID();
    }
    
    public AbilityMessage(EntityPlayerSP player, int count, boolean active)
    {
    	uuid = player.getPersistentID();
    	counter = count;
    	isActive = active;
    }

	@Override
	public void fromBytes(ByteBuf buf) 
	{
		this.uuid = UUID.fromString(ByteBufUtils.readUTF8String(buf));
		//this.uuid = new UUID(buf.readLong(), buf.readLong());
		this.counter = buf.readInt();
		this.isActive = buf.readBoolean();
	}

	@Override
	public void toBytes(ByteBuf buf) 
	{
		ByteBufUtils.writeUTF8String(buf, this.uuid.toString());
		//buf.writeLong(this.uuid.getMostSignificantBits());
		//buf.writeLong(this.uuid.getLeastSignificantBits());
		buf.writeInt(counter);
		buf.writeBoolean(isActive);
	}
}
