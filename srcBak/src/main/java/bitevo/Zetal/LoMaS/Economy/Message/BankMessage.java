package bitevo.Zetal.LoMaS.Economy.Message;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class BankMessage implements IMessage
{
	public int cc = -1;
	
    public BankMessage(){}
    public BankMessage(int cc)
    {
    	this.cc = cc;
    }

	@Override
	public void fromBytes(ByteBuf buf) 
	{
		this.cc = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) 
	{
		buf.writeInt(cc);
	}
}
