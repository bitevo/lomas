package bitevo.Zetal.LoMaS.Specializations.Item;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.FoodInitializer;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Message.TimedUsageMessage;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemSeeds extends Item implements net.minecraftforge.common.IPlantable
{
	private Block crops;
	/** BlockID of the block the seeds can be planted on. */
	private Block soilBlockID;

	public ItemSeeds(Block crops, Block soil)
	{
		this.crops = crops;
		this.soilBlockID = soil;
		this.setCreativeTab(CreativeTabs.MATERIALS);
	}
	
	public static int getUseDuration()
	{
		return FMLCommonHandler.instance().getSide() == Side.CLIENT ? getClientUseDuration() : getServerUseDuration();
	}
	
	@SideOnly(Side.SERVER)
	public static int getServerUseDuration()
	{
		return 60;
	}
	
	@SideOnly(Side.CLIENT)
	public static int getClientUseDuration()
	{
		int dur = 60;
		/**
		 * While this is a fine solution for now, it's a bit of a problem that you can increase planting speed entirely on the client... 
		 */
		if(FMLCommonHandler.instance().getSide() == Side.CLIENT)
		{
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(Minecraft.getMinecraft().thePlayer);
			dur = (int) (dur * (1.0f - lplayer.getSkills().getPercentMagnitudeFromName("Planting spd")));
		}
		return dur;
	}

	/**
	 * How long it takes to use or consume an item
	 */
	@Override
	public int getMaxItemUseDuration(ItemStack stack)
	{
		return getUseDuration();
	}

	/**
	 * returns the action that specifies what animation to play when the items is being used
	 */
	@Override
	public EnumAction getItemUseAction(ItemStack stack)
	{
		return SpecializationsMod.farming;
	}

	@Override
	public EnumActionResult onItemUseFirst(ItemStack stack, EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ, EnumHand hand)
	{
		if (side != EnumFacing.UP)
		{
			return EnumActionResult.FAIL;
		}
		else if (!player.canPlayerEdit(pos.offset(side), side, stack))
		{
			return EnumActionResult.FAIL;
		}
		else if (SpecializationsMod.canSustainPlant(world, pos, EnumFacing.UP, this) && world.isAirBlock(pos.up()))
		{
			player.setActiveHand(hand);
			return EnumActionResult.PASS;
		}
		else
		{
			return EnumActionResult.FAIL;
		}
	}

	@Override
	public void onUsingTick(ItemStack stack, EntityLivingBase entityLiving, int count)
	{
		if (entityLiving instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) entityLiving;
			World world = player.worldObj;
			if (count % 4 == 0)
			{
				player.playSound(SoundEvents.BLOCK_GRASS_STEP, 0.5F, 1.4F);
			}

			if (world.isRemote)
			{
				this.onClientTick(stack, player, count);
			}
		}
	}

	@SideOnly(Side.CLIENT)
	public void onClientTick(ItemStack stack, EntityPlayer player, int count)
	{
		World world = player.worldObj;
		BlockPos pos = Minecraft.getMinecraft().objectMouseOver.getBlockPos();
		if (!this.canBePlanted(world, pos, this))
		{
			player.stopActiveHand();
		}
	}

	@Override
	public ItemStack onItemUseFinish(ItemStack stack, World worldIn, EntityLivingBase entityLiving)
	{
		if (worldIn.isRemote && entityLiving instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) entityLiving;
			BlockPos pos = Minecraft.getMinecraft().objectMouseOver.getBlockPos();
			SpecializationsMod.snw.sendToServer(new TimedUsageMessage(player.getPersistentID(), pos.up(), player.inventory.currentItem));
		}
		return stack;
	}

	public static boolean canBePlanted(World world, BlockPos pos, IPlantable plantable)
	{
		if (pos != null && world.getBlockState(pos) != null && world.getBlockState(pos).getBlock() != null)
		{
			if (SpecializationsMod.canSustainPlant(world, pos, EnumFacing.UP, plantable) && world.isAirBlock(pos.up()))
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public net.minecraftforge.common.EnumPlantType getPlantType(net.minecraft.world.IBlockAccess world, BlockPos pos)
	{
		return this.crops == FoodInitializer.NETHER_WART_BLOCK ? EnumPlantType.Nether : EnumPlantType.Crop;
	}

	@Override
	public net.minecraft.block.state.IBlockState getPlant(net.minecraft.world.IBlockAccess world, BlockPos pos)
	{
		return this.crops.getDefaultState();
	}
}