package bitevo.Zetal.LoMaS.Specializations.Inventory;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySpecBrewingStand;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.PotionTypes;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionUtils;
import net.minecraft.stats.AchievementList;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class SpecContainerBrewingStand extends Container
{
	private TileEntitySpecBrewingStand tileBrewingStand;
	/** Instance of Slot. */
	private final Slot theSlot;
	private int brewTime;

	public SpecContainerBrewingStand(InventoryPlayer playerInventory, IInventory tileBrewingStandIn)
    {
        this.tileBrewingStand = (TileEntitySpecBrewingStand) tileBrewingStandIn;
        this.addSlotToContainer(new SpecContainerBrewingStand.Potion(playerInventory.player, tileBrewingStandIn, 0, 56, 51));
        this.addSlotToContainer(new SpecContainerBrewingStand.Potion(playerInventory.player, tileBrewingStandIn, 1, 79, 58));
        this.addSlotToContainer(new SpecContainerBrewingStand.Potion(playerInventory.player, tileBrewingStandIn, 2, 102, 51));
        this.theSlot = this.addSlotToContainer(new SpecContainerBrewingStand.Ingredient(tileBrewingStandIn, 3, 79, 17));
        this.addSlotToContainer(new SpecContainerBrewingStand.Fuel(tileBrewingStandIn, 4, 17, 17));

        for (int i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(playerInventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        for (int k = 0; k < 9; ++k)
        {
            this.addSlotToContainer(new Slot(playerInventory, k, 8 + k * 18, 142));
        }
	}

	/**
	 * Add the given Listener to the list of Listeners. Method name is for legacy.
	 */
	@Override
	public void addListener(IContainerListener listener)
	{
		super.addListener(listener);
		listener.sendAllWindowProperties(this, this.tileBrewingStand);
	}

	/**
	 * Looks for changes made in the container, sends them to every listener.
	 */
	@Override
	public void detectAndSendChanges()
	{
		super.detectAndSendChanges();

		for (int i = 0; i < this.listeners.size(); ++i)
		{
			IContainerListener icrafting = (IContainerListener) this.listeners.get(i);

			if (this.brewTime != this.tileBrewingStand.getField(0))
			{
				icrafting.sendProgressBarUpdate(this, 0, this.tileBrewingStand.getField(0));
			}
		}

		this.brewTime = this.tileBrewingStand.getField(0);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int id, int data)
	{
		this.tileBrewingStand.setField(id, data);
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn)
	{
		return this.tileBrewingStand.isUseableByPlayer(playerIn);
	}

	@Override
	public ItemStack slotClick(int slotId, int clickedButton, ClickType mode, EntityPlayer playerIn)
	{
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(playerIn);
		this.tileBrewingStand.setOwner(lplayer.uuid);
		return super.slotClick(slotId, clickedButton, mode, playerIn);
	}

    /**
     * Take a stack from the specified inventory slot.
     */
    @Nullable
    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(index);

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if ((index < 0 || index > 2) && index != 3 && index != 4)
            {
                if (!this.theSlot.getHasStack() && this.theSlot.isItemValid(itemstack1))
                {
                    if (!this.mergeItemStack(itemstack1, 3, 4, false))
                    {
                        return null;
                    }
                }
                else if (SpecContainerBrewingStand.Potion.canHoldPotion(itemstack))
                {
                    if (!this.mergeItemStack(itemstack1, 0, 3, false))
                    {
                        return null;
                    }
                }
                else if (SpecContainerBrewingStand.Fuel.isValidBrewingFuel(itemstack))
                {
                    if (!this.mergeItemStack(itemstack1, 4, 5, false))
                    {
                        return null;
                    }
                }
                else if (index >= 5 && index < 32)
                {
                    if (!this.mergeItemStack(itemstack1, 32, 41, false))
                    {
                        return null;
                    }
                }
                else if (index >= 32 && index < 41)
                {
                    if (!this.mergeItemStack(itemstack1, 5, 32, false))
                    {
                        return null;
                    }
                }
                else if (!this.mergeItemStack(itemstack1, 5, 41, false))
                {
                    return null;
                }
            }
            else
            {
                if (!this.mergeItemStack(itemstack1, 5, 41, true))
                {
                    return null;
                }

                slot.onSlotChange(itemstack1, itemstack);
            }

            if (itemstack1.stackSize == 0)
            {
                slot.putStack((ItemStack)null);
            }
            else
            {
                slot.onSlotChanged();
            }

            if (itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }

            slot.onPickupFromSlot(playerIn, itemstack1);
        }

        return itemstack;
    }

    static class Ingredient extends Slot
    {
        public Ingredient(IInventory iInventoryIn, int index, int xPosition, int yPosition)
        {
            super(iInventoryIn, index, xPosition, yPosition);
        }

        /**
         * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
         */
        @Override
        public boolean isItemValid(@Nullable ItemStack stack)
        {
            return stack != null && net.minecraftforge.common.brewing.BrewingRecipeRegistry.isValidIngredient(stack);
        }

        /**
         * Returns the maximum stack size for a given slot (usually the same as getInventoryStackLimit(), but 1 in
         * the case of armor slots)
         */
        @Override
        public int getSlotStackLimit()
        {
            return 64;
        }
    }

    static class Fuel extends Slot
        {
            public Fuel(IInventory iInventoryIn, int index, int xPosition, int yPosition)
            {
                super(iInventoryIn, index, xPosition, yPosition);
            }

            /**
             * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
             */
            @Override
            public boolean isItemValid(@Nullable ItemStack stack)
            {
                /**
                 * Returns true if the given ItemStack is usable as a fuel in the brewing stand.
                 */
                return isValidBrewingFuel(stack);
            }

            /**
             * Returns true if the given ItemStack is usable as a fuel in the brewing stand.
             */
            public static boolean isValidBrewingFuel(@Nullable ItemStack itemStackIn)
            {
                return itemStackIn != null && itemStackIn.getItem() == Items.BLAZE_POWDER;
            }

            /**
             * Returns the maximum stack size for a given slot (usually the same as getInventoryStackLimit(), but 1 in
             * the case of armor slots)
             */
            @Override
            public int getSlotStackLimit()
            {
                return 64;
            }
        }

	static class Potion extends Slot
	{
		/** The player that has this container open. */
		private EntityPlayer player;

		 public Potion(EntityPlayer playerIn, IInventory inventoryIn, int index, int xPosition, int yPosition)
         {
             super(inventoryIn, index, xPosition, yPosition);
             this.player = playerIn;
         }

		/**
		 * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
		 */
		@Override
		public boolean isItemValid(@Nullable ItemStack stack)
        {
            /**
             * Returns true if this itemstack can be filled with a potion
             */
            return canHoldPotion(stack);
        }

		/**
		 * Returns the maximum stack size for a given slot (usually the same as getInventoryStackLimit(), but 1 in the case of armor slots)
		 */
		@Override
		public int getSlotStackLimit()
		{
			/*if(this.player != null)
			{
				LoMaS_Player lPlayer = LoMaS_Player.getLoMaSPlayer(player);
				if(lPlayer != null)
				{
					
				}
			}*/
			return 1;
		}

		@Override
		public void onPickupFromSlot(EntityPlayer playerIn, ItemStack stack)
		{
			if (stack.getItem() instanceof net.minecraft.item.ItemPotion && PotionUtils.getPotionFromItem(stack) != PotionTypes.WATER)
			{
				this.player.hasAchievement(AchievementList.POTION);
				//System.out.println("hi!");
				//////////// SPECIALIZATIONS
				LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(playerIn);
				if (PotionUtils.getPotionFromItem(stack) != PotionTypes.WATER && PotionUtils.getPotionFromItem(stack) != PotionTypes.AWKWARD 
				&& PotionUtils.getPotionFromItem(stack) != PotionTypes.MUNDANE && PotionUtils.getPotionFromItem(stack) != PotionTypes.THICK)
				{
					//System.out.println("yo!");
					float progressEarned = 5.0f;
					if ((stack.getItemDamage() >= 8225))
					{
						progressEarned = progressEarned + 20.0f;
					}
					if (!playerIn.capabilities.isCreativeMode)
					{
						TileEntitySpecBrewingStand.printArray((TileEntitySpecBrewingStand.intToBooleans(this.inventory.getField(2), 4)));
						boolean[] freshArray = (TileEntitySpecBrewingStand.intToBooleans(this.inventory.getField(2), 4)).clone();
						boolean isFresh = freshArray[this.slotNumber];
						if (isFresh)
						{
							//System.out.println("isFresh!");
							lplayer.addSpecProgress(progressEarned);
							freshArray[this.slotNumber] = false;
							this.inventory.setField(2, TileEntitySpecBrewingStand.booleansToInt(freshArray));
							this.player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((this.player.worldObj.rand.nextFloat() - this.player.worldObj.rand.nextFloat()) * 0.7F + 1.8F));
						}
					}
				}
			}
			
			/*if(stack.getMaxStackSize() == 1)
			{
				stack.splitStack(1);
			}*/

			super.onPickupFromSlot(playerIn, stack);
		}

		/**
		 * Returns true if this itemstack can be filled with a potion
		 */
		public static boolean canHoldPotion(@Nullable ItemStack stack)
        {
            if (stack == null)
            {
                return false;
            }
            else
            {
                return TileEntitySpecBrewingStand.isValidInput(stack);
            }
        }
	}
}
