package bitevo.Zetal.LoMaS.Specializations.Renderer;

import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityFactionDiamondGolem;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.layers.LayerHeldItem;
import net.minecraft.util.ResourceLocation;

public class RenderDiamondGolem extends RenderLiving<EntityFactionDiamondGolem>
{
	public RenderDiamondGolem(RenderManager p_i46153_1_)
	{
		super(p_i46153_1_, new ModelDiamondGolem(), 0.5F);
		this.addLayer(new LayerHeldItem(this));
	}

    protected void rotateCorpse(EntityFactionDiamondGolem entityLiving, float p_77043_2_, float p_77043_3_, float partialTicks)
    {
        super.rotateCorpse(entityLiving, p_77043_2_, p_77043_3_, partialTicks);
    }

	@Override
	protected ResourceLocation getEntityTexture(EntityFactionDiamondGolem entity)
	{
		return new ResourceLocation(SpecializationsMod.MODID, "textures/entities/diamond_golem.png");
	}
}
