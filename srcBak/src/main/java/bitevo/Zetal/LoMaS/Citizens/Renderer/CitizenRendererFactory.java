package bitevo.Zetal.LoMaS.Citizens.Renderer;

import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import bitevo.Zetal.LoMaS.Citizens.Entities.EntityPigman;

public class CitizenRendererFactory implements IRenderFactory<EntityPigman>
{
	@Override
	public Render<? super EntityPigman> createRenderFor(RenderManager manager)
	{
		return new RenderPigman(manager, new ModelPigman(), 0.5F);
	}
}
