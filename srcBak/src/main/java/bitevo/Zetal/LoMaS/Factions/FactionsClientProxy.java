package bitevo.Zetal.LoMaS.Factions;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.item.Item;

public class FactionsClientProxy extends FactionsCommonProxy
{
	public static Minecraft mc;

	@Override
	public void registerRenderInformation()
	{
		mc = Minecraft.getMinecraft();
		final String modid = FactionsMod.modid;
		RenderItem itemRenderer = mc.getRenderItem();
		RenderManager rm = Minecraft.getMinecraft().getRenderManager();
		
		init();
	}

	@Override
	public void init()
	{

	}
}
