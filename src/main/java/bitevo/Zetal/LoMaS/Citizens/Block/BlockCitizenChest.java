package bitevo.Zetal.LoMaS.Citizens.Block;

import java.util.Iterator;
import java.util.Random;

import bitevo.Zetal.LoMaS.Citizens.TileEntity.TileEntityCitizenChest;
import net.minecraft.block.Block;
import net.minecraft.block.BlockChest;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.passive.EntityOcelot;
import net.minecraft.inventory.InventoryLargeChest;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.ILockableContainer;
import net.minecraft.world.World;

public class BlockCitizenChest extends BlockChest
{
	public BlockCitizenChest()
	{
		super(BlockChest.Type.BASIC);
		this.setCreativeTab(null);
		this.setTickRandomly(true);
		this.setHardness(2.5F);
	}

	@Override
	public boolean canPlaceBlockAt(World worldIn, BlockPos pos)
	{
		if (worldIn.isAirBlock(pos.down()))
		{
			return false;
		}
		int i = 0;
		BlockPos blockpos = pos.west();
		BlockPos blockpos1 = pos.east();
		BlockPos blockpos2 = pos.north();
		BlockPos blockpos3 = pos.south();

		if (worldIn.getBlockState(blockpos).getBlock() == this)
		{
			if (this.isDoubleChest(worldIn, blockpos))
			{
				return false;
			}

			++i;
		}

		if (worldIn.getBlockState(blockpos1).getBlock() == this)
		{
			if (this.isDoubleChest(worldIn, blockpos1))
			{
				return false;
			}

			++i;
		}

		if (worldIn.getBlockState(blockpos2).getBlock() == this)
		{
			if (this.isDoubleChest(worldIn, blockpos2))
			{
				return false;
			}

			++i;
		}

		if (worldIn.getBlockState(blockpos3).getBlock() == this)
		{
			if (this.isDoubleChest(worldIn, blockpos3))
			{
				return false;
			}

			++i;
		}

		return i <= 1;
	}

	private boolean isDoubleChest(World worldIn, BlockPos pos)
	{
		if (worldIn.getBlockState(pos).getBlock() != this)
		{
			return false;
		}
		else
		{
			for (EnumFacing enumfacing : EnumFacing.Plane.HORIZONTAL)
			{
				if (worldIn.getBlockState(pos.offset(enumfacing)).getBlock() == this)
				{
					return true;
				}
			}

			return false;
		}
	}

	@Override
	public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state)
	{
		this.checkForSurroundingChests(worldIn, pos, state);
		Iterator iterator = EnumFacing.Plane.HORIZONTAL.iterator();

		while (iterator.hasNext())
		{
			EnumFacing enumfacing = (EnumFacing) iterator.next();
			BlockPos blockpos1 = pos.offset(enumfacing);
			IBlockState iblockstate1 = worldIn.getBlockState(blockpos1);

			if (iblockstate1.getBlock() == this)
			{
				this.checkForSurroundingChests(worldIn, blockpos1, iblockstate1);
			}
		}
	}

	@Override
	public int getWeakPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
	{
		if (!this.canProvidePower(blockState))
		{
			return 0;
		}
		else
		{
			int i = 0;
			TileEntity tileentity = blockAccess.getTileEntity(pos);

			if (tileentity instanceof TileEntityCitizenChest)
			{
				i = ((TileEntityCitizenChest) tileentity).numPlayersUsing;
			}

			return MathHelper.clamp_int(i, 0, 15);
		}
	}

	@Override
	public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn)
	{
		super.neighborChanged(state, worldIn, pos, blockIn);
		TileEntity tileentity = worldIn.getTileEntity(pos);

		if (tileentity instanceof TileEntityCitizenChest)
		{
			tileentity.updateContainingBlockInfo();
		}
	}

	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		return source.getBlockState(pos.north()).getBlock() == this ? NORTH_CHEST_AABB : (source.getBlockState(pos.south()).getBlock() == this ? SOUTH_CHEST_AABB : (source.getBlockState(pos.west()).getBlock() == this ? WEST_CHEST_AABB : (source.getBlockState(pos.east()).getBlock() == this ? EAST_CHEST_AABB : NOT_CONNECTED_AABB)));
	}

	@Override
	public IBlockState onBlockPlaced(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
	{
		return this.getDefaultState().withProperty(FACING, placer.getHorizontalFacing());
	}

	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
	{
		EnumFacing enumfacing = EnumFacing.getHorizontal(MathHelper.floor_double((double) (placer.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3).getOpposite();
		state = state.withProperty(FACING, enumfacing);
		BlockPos blockpos1 = pos.north();
		BlockPos blockpos2 = pos.south();
		BlockPos blockpos3 = pos.west();
		BlockPos blockpos4 = pos.east();
		boolean flag = this == worldIn.getBlockState(blockpos1).getBlock();
		boolean flag1 = this == worldIn.getBlockState(blockpos2).getBlock();
		boolean flag2 = this == worldIn.getBlockState(blockpos3).getBlock();
		boolean flag3 = this == worldIn.getBlockState(blockpos4).getBlock();

		if (!flag && !flag1 && !flag2 && !flag3)
		{
			worldIn.setBlockState(pos, state, 3);
		}
		else if (enumfacing.getAxis() == EnumFacing.Axis.X && (flag || flag1))
		{
			if (flag)
			{
				worldIn.setBlockState(blockpos1, state, 3);
			}
			else
			{
				worldIn.setBlockState(blockpos2, state, 3);
			}

			worldIn.setBlockState(pos, state, 3);
		}
		else if (enumfacing.getAxis() == EnumFacing.Axis.Z && (flag2 || flag3))
		{
			if (flag2)
			{
				worldIn.setBlockState(blockpos3, state, 3);
			}
			else
			{
				worldIn.setBlockState(blockpos4, state, 3);
			}

			worldIn.setBlockState(pos, state, 3);
		}

		TileEntity tileentity = worldIn.getTileEntity(pos);

		if (tileentity instanceof TileEntityCitizenChest)
		{
			((TileEntityCitizenChest) tileentity).owner = placer.getPersistentID();
		}
		if (stack.hasDisplayName())
		{
			tileentity = worldIn.getTileEntity(pos);

			if (tileentity instanceof TileEntityCitizenChest)
			{
				((TileEntityCitizenChest) tileentity).setCustomName(stack.getDisplayName());
			}
		}
	}

	@Override
	public ILockableContainer getLockableContainer(World worldIn, BlockPos pos)
	{
		TileEntity tileentity = worldIn.getTileEntity(pos);

		if (!(tileentity instanceof TileEntityCitizenChest))
		{
			return null;
		}
		else
		{
			Object object = (TileEntityCitizenChest) tileentity;

			if (this.isBlocked(worldIn, pos))
			{
				return null;
			}
			else
			{
				Iterator iterator = EnumFacing.Plane.HORIZONTAL.iterator();

				while (iterator.hasNext())
				{
					EnumFacing enumfacing = (EnumFacing) iterator.next();
					BlockPos blockpos1 = pos.offset(enumfacing);
					Block block = worldIn.getBlockState(blockpos1).getBlock();

					if (block == this)
					{
						if (this.isBlocked(worldIn, blockpos1))
						{
							return null;
						}

						TileEntity tileentity1 = worldIn.getTileEntity(blockpos1);

						if (tileentity1 instanceof TileEntityCitizenChest)
						{
							if (enumfacing != EnumFacing.WEST && enumfacing != EnumFacing.NORTH)
							{
								object = new InventoryLargeChest("container.chestDouble", (ILockableContainer) object, (TileEntityCitizenChest) tileentity1);
							}
							else
							{
								object = new InventoryLargeChest("container.chestDouble", (TileEntityCitizenChest) tileentity1, (ILockableContainer) object);
							}
						}
					}
				}

				return (ILockableContainer) object;
			}
		}
	}

	@Override
	public void randomTick(World world, BlockPos pos, IBlockState state, Random rand)
	{
		if (world.getTileEntity(pos) instanceof TileEntityCitizenChest)
		{
			TileEntityCitizenChest tecc = (TileEntityCitizenChest) world.getTileEntity(pos);
			tecc.updateBlock();
		}
	}

	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune)
	{
		return null;
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileEntityCitizenChest();
	}

	private boolean isBlocked(World worldIn, BlockPos pos)
	{
		return this.isBelowSolidBlock(worldIn, pos) || this.isOcelotSittingOnChest(worldIn, pos);
	}

	private boolean isBelowSolidBlock(World worldIn, BlockPos pos)
	{
		return worldIn.getBlockState(pos.up()).isSideSolid(worldIn, pos.up(), EnumFacing.DOWN);
	}

	private boolean isOcelotSittingOnChest(World worldIn, BlockPos pos)
	{
		for (Entity entity : worldIn.getEntitiesWithinAABB(EntityOcelot.class, new AxisAlignedBB((double) pos.getX(), (double) (pos.getY() + 1), (double) pos.getZ(), (double) (pos.getX() + 1), (double) (pos.getY() + 2), (double) (pos.getZ() + 1))))
		{
			EntityOcelot entityocelot = (EntityOcelot) entity;

			if (entityocelot.isSitting())
			{
				return true;
			}
		}

		return false;
	}
}
