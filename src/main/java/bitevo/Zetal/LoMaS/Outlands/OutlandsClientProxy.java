package bitevo.Zetal.LoMaS.Outlands;

import bitevo.Zetal.LoMaS.Outlands.Block.BlockWrath;
import net.minecraft.block.properties.IProperty;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockModelShapes;
import net.minecraft.client.renderer.block.statemap.StateMap;
import net.minecraftforge.common.MinecraftForge;

public class OutlandsClientProxy extends OutlandsCommonProxy
{
	public static Minecraft mc;

	@Override
	public void init()
	{
		MinecraftForge.EVENT_BUS.register(this);
	}

	@Override
	public void regiterBlockStateMappers()
	{
		BlockModelShapes shapes = Minecraft.getMinecraft().getBlockRendererDispatcher().getBlockModelShapes();
		shapes.registerBlockWithStateMapper(OutlandsMod.WRATH, (new StateMap.Builder()).ignore(new IProperty[] { BlockWrath.AGE }).build());
	}
}
