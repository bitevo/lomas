package bitevo.Zetal.LoMaS.Specializations.Item;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class ItemSapBottle extends Item
{
	public ItemSapBottle()
	{
		super();
		this.setCreativeTab(LoMaS_Utils.tabLoMaS);
	}

	/**
	 * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
	 */
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
	{
		RayTraceResult movingobjectposition = this.rayTrace(par2World, par3EntityPlayer, true);
		if (movingobjectposition == null)
		{
		}
		else
		{
		}
		return par1ItemStack;
	}
}
