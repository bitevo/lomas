package bitevo.Zetal.LoMaS.Specializations.GUI;

import java.text.DecimalFormat;

import org.lwjgl.opengl.GL11;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Handler.SpecializationKeyBinds;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiSpecInfoBranching extends GuiScreen
{
	private float oldMouseX;
	private float oldMouseY;
	ResourceLocation specInfo = new ResourceLocation(SpecializationsMod.MODID, "gui/specinfo.png");
	private float xSize_lo;
	private float ySize_lo;

	private static EntityPlayer thePlayer;
	private float progress = 0.0f;
	private float endProgress = 0.0f;
	private int classlevel = 0;
	private String playerName;
	private String guildName;
	private String rankName;
	private int bankCC;

	public GuiSpecInfoBranching(EntityPlayer par1EntityPlayer)
	{
		this.allowUserInput = false;
		this.thePlayer = par1EntityPlayer;
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(par1EntityPlayer);
		this.progress = lplayer.progress;
		this.endProgress = lplayer.getReqProgress();
		this.classlevel = lplayer.getClassLevel();
		this.playerName = lplayer.name;
		this.rankName = lplayer.rank;
		this.guildName = lplayer.guildtag;
		this.bankCC = lplayer.getBankCC();
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui()
	{
		this.buttonList.clear();
	}

	/**
	 * Draw the foreground layer for the GuiContainer (everything in front of the items)
	 */
	protected void drawGuiContainerForegroundLayer(int par1, int par2)
	{
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		super.drawScreen(mouseX, mouseY, partialTicks);
		this.drawGuiContainerBackgroundLayer(partialTicks, mouseX, mouseY);
		this.drawGuiContainerForegroundLayer(mouseX, mouseY);
		this.oldMouseX = (float) mouseX;
		this.oldMouseY = (float) mouseY;
	}

	/**
	 * Draw the background layer for the GuiContainer (everything behind the items)
	 */
	protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(specInfo);
		ScaledResolution res = new ScaledResolution(this.mc);
		int width = res.getScaledWidth();
		int height = res.getScaledHeight();
		int k = (width - 256) / 2;
		int l = (height - 188) / 2;
		int p = (width - 182) / 2;
		int o = (height - 5) / 2;
		// Width + 51
		this.drawTexturedModalRect(k, l - 15, 0, 0, 256, 188);
		// Complete Bar: 0 -> 182, 193 -> 198
		// Empty Bar: 0 -> 182, 188 -> 193
		this.drawTexturedModalRect(p, o + 72, 0, 188, 182, 5);
		if (this.progress > 0 && this.classlevel > 0)
		{
			float fractionProgress = ((this.progress - LoMaS_Player.getReqProgressFromLevel(classlevel - 1)) / this.endProgress);
			DecimalFormat df = new DecimalFormat("0.00");
			String percentProgress = "" + df.format(fractionProgress * 100.0) + "%";
			this.fontRendererObj.drawString(percentProgress, p + 84, o + 63 + 1, 10881);
			this.fontRendererObj.drawString(percentProgress, p + 84 + 1, o + 63, 10881);
			this.fontRendererObj.drawString(percentProgress, p + 84 + 1, o + 63 + 1, 10881);
			this.fontRendererObj.drawString(percentProgress, p + 84, o + 63, 1600226);
			this.mc.getTextureManager().bindTexture(specInfo);
			int pixel = (int) (fractionProgress * 182);
			this.drawTexturedModalRect(p, o + 72, 0, 193, pixel, 5);
		}
		int x = 9;
		int y = 74;
		this.fontRendererObj.drawString("Level " + this.classlevel, k + x + 1, l + y, 10881);
		this.fontRendererObj.drawString("Level " + this.classlevel, k + x, l + y, 1600226);
		y = y + 15;
		this.fontRendererObj.drawString(this.guildName, k + x + 1, l + y, 10881);
		this.fontRendererObj.drawString(this.guildName, k + x, l + y, 1600226);
		y = y + 15;
		this.fontRendererObj.drawString(this.rankName, k + x + 1, l + y, 10881);
		this.fontRendererObj.drawString(this.rankName, k + x, l + y, 1600226);
		y = y + 15;
		this.fontRendererObj.drawString(this.bankCC + "CC", k + x + 1, l + y, 10881);
		this.fontRendererObj.drawString(this.bankCC + "CC", k + x, l + y, 1600226);
		x = 7;
		y = 155;
		this.fontRendererObj.drawString("" + classlevel, k + x + 1, l + y, 10881);
		this.fontRendererObj.drawString("" + classlevel, k + x, l + y, 1600226);
		x = 70;
		y = -5;
		this.fontRendererObj.drawString(this.playerName, k + x + 1, l + y, 10881);
		this.fontRendererObj.drawString(this.playerName, k + x, l + y, 1600226);
		y = y + 10;
		displayStatInfo(x, y, k, l);
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		drawEntityOnScreen(k + 30, l + 63, 30, (float) (k + 51) - this.oldMouseX, (float) (l + 75 - 50) - this.oldMouseY, this.mc.thePlayer);
	}

	public void displayStatInfo(int x, int y, int k, int l)
	{
		
	}

	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}

	public static void drawEntityOnScreen(int p_147046_0_, int p_147046_1_, int p_147046_2_, float p_147046_3_, float p_147046_4_, EntityLivingBase p_147046_5_)
	{
		GlStateManager.enableColorMaterial();
		GlStateManager.pushMatrix();
		GlStateManager.translate((float) p_147046_0_, (float) p_147046_1_, 50.0F);
		GlStateManager.scale((float) (-p_147046_2_), (float) p_147046_2_, (float) p_147046_2_);
		GlStateManager.rotate(180.0F, 0.0F, 0.0F, 1.0F);
		float f2 = p_147046_5_.renderYawOffset;
		float f3 = p_147046_5_.rotationYaw;
		float f4 = p_147046_5_.rotationPitch;
		float f5 = p_147046_5_.prevRotationYawHead;
		float f6 = p_147046_5_.rotationYawHead;
		GlStateManager.rotate(135.0F, 0.0F, 1.0F, 0.0F);
		RenderHelper.enableStandardItemLighting();
		GlStateManager.rotate(-135.0F, 0.0F, 1.0F, 0.0F);
		GlStateManager.rotate(-((float) Math.atan((double) (p_147046_4_ / 40.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
		p_147046_5_.renderYawOffset = (float) Math.atan((double) (p_147046_3_ / 40.0F)) * 20.0F;
		p_147046_5_.rotationYaw = (float) Math.atan((double) (p_147046_3_ / 40.0F)) * 40.0F;
		p_147046_5_.rotationPitch = -((float) Math.atan((double) (p_147046_4_ / 40.0F))) * 20.0F;
		p_147046_5_.rotationYawHead = p_147046_5_.rotationYaw;
		p_147046_5_.prevRotationYawHead = p_147046_5_.rotationYaw;
		GlStateManager.translate(0.0F, 0.0F, 0.0F);
		RenderManager rendermanager = Minecraft.getMinecraft().getRenderManager();
		rendermanager.setPlayerViewY(180.0F);
		rendermanager.setRenderShadow(false);
		rendermanager.doRenderEntity(p_147046_5_, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F, false);
		rendermanager.setRenderShadow(true);
		p_147046_5_.renderYawOffset = f2;
		p_147046_5_.rotationYaw = f3;
		p_147046_5_.rotationPitch = f4;
		p_147046_5_.prevRotationYawHead = f5;
		p_147046_5_.rotationYawHead = f6;
		GlStateManager.popMatrix();
		RenderHelper.disableStandardItemLighting();
		GlStateManager.disableRescaleNormal();
		GlStateManager.setActiveTexture(OpenGlHelper.lightmapTexUnit);
		GlStateManager.disableTexture2D();
		GlStateManager.setActiveTexture(OpenGlHelper.defaultTexUnit);
	}

	@Override
	protected void keyTyped(char par1, int par2)
	{
		if (par2 == 1 || par2 == this.mc.gameSettings.keyBindInventory.getKeyCode() || par2 == SpecializationKeyBinds.keys[2].getKeyCode())
		{
			this.mc.thePlayer.closeScreen();
		}
	}

	@Override
	protected void actionPerformed(GuiButton par1GuiButton)
	{
	}

	@Override
	public void updateScreen()
	{
		if (!this.mc.thePlayer.isEntityAlive() || this.mc.thePlayer.isDead)
		{
			this.mc.thePlayer.closeScreen();
		}
	}
}
