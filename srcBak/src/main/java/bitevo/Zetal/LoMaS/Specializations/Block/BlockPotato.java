package bitevo.Zetal.LoMaS.Specializations.Block;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import bitevo.Zetal.LoMaS.Specializations.FoodInitializer;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntityPlant;

public class BlockPotato extends BlockCrops
{

    @Override
    protected Item getSeed()
    {
        return FoodInitializer.POTATO;
    }

    @Override
    protected Item getCrop()
    {
        return FoodInitializer.POTATO;
    }

    /**
     * Spawns this Block's drops into the World as EntityItems.
     *  
     * @param chance The chance that each Item is actually spawned (1.0 = always, 0.0 = never)
     * @param fortune The player's fortune level
     */
    @Override
    public void dropBlockAsItemWithChance(World worldIn, BlockPos pos, IBlockState state, float chance, int fortune)
    {
        super.dropBlockAsItemWithChance(worldIn, pos, state, chance, fortune);
    }
    
    @Override
    public java.util.List<net.minecraft.item.ItemStack> getDrops(net.minecraft.world.IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
    {
        List<ItemStack> ret = new ArrayList<ItemStack>();
    	if(world.getTileEntity(pos) != null && world.getTileEntity(pos) instanceof TileEntityPlant)
    	{
            ret = super.getDrops(world, pos, state, fortune);
            java.util.Random rand = world instanceof World ? ((World)world).rand : new java.util.Random();
            if (this.getAge(state) >= 7 && rand.nextInt(50) == 0)
            {
                ret.add(new ItemStack(FoodInitializer.POISONOUS_POTATO));
            }
            
            int quality = ((TileEntityPlant) world.getTileEntity(pos)).getQuality();
            for(ItemStack stack : ret)
            {
            	if(stack.getItem().equals(this.getCrop()))
            	{
            		stack.setItemDamage(SpecializationsMod.getQualityIndexFromDamage(quality));
            	}
            }
    	}
        return ret;
    }
}