package bitevo.Zetal.LoMaS.BigFMod.Message;

import bitevo.Zetal.LoMaS.BigFMod.Block.BlockBookstand;
import bitevo.Zetal.LoMaS.BigFMod.TileEntity.TileEntityBookstand;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BookstandMessageHandler implements IMessageHandler<BookstandMessage, IMessage>
{
	@Override
	public IMessage onMessage(BookstandMessage message, MessageContext ctx)
	{
		if (ctx.side == Side.CLIENT)
		{
			handleCBookstandMessage(message);
		}
		return null;
	}

	@SideOnly(Side.CLIENT)
	private void handleCBookstandMessage(BookstandMessage packet)
	{
		BlockPos pos = packet.pos;
		ItemStack book = packet.book;
		boolean hasBook = packet.hasBook;
		World world = Minecraft.getMinecraft().theWorld;
		IBlockState state = world.getBlockState(pos);
		if (state.getBlock() instanceof BlockBookstand && world.getTileEntity(pos) != null && world.getTileEntity(pos) instanceof TileEntityBookstand)
		{
			TileEntityBookstand tebs = (TileEntityBookstand) world.getTileEntity(pos);
			if (!hasBook && tebs.hasBook())
			{
				tebs.removeBook();
			}
			else if (hasBook && !tebs.hasBook())
			{
				tebs.addBook(book);
			}
			else if (hasBook && tebs.hasBook() && !tebs.getStoredBook().equals(book))
			{
				tebs.addBook(book);
			}
			world.markBlockRangeForRenderUpdate(pos, pos);
			world.notifyBlockUpdate(pos, world.getBlockState(pos), world.getBlockState(pos), 3);
		}
	}
}
