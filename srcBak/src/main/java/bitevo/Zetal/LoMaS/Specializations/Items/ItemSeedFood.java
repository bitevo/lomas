package bitevo.Zetal.LoMaS.Specializations.Items;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Message.TimedUsageMessage;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemSeedFood extends ItemFood implements net.minecraftforge.common.IPlantable
{
    private Block crops;
    /** Block ID of the soil this seed food should be planted on. */
    private Block soilId;

    public ItemSeedFood(int healAmount, float saturation, Block crops, Block soil)
    {
        super(healAmount, saturation, false);
        this.crops = crops;
        this.soilId = soil;
    }

	/**
	 * How long it takes to use or consume an item
	 */
	@Override
	public int getMaxItemUseDuration(ItemStack stack)
	{
		return 60;
	}

	/**
	 * returns the action that specifies what animation to play when the items is being used
	 */
	@Override
	public EnumAction getItemUseAction(ItemStack stack)
	{
		return SpecializationsMod.farming;
	}

	@Override
    public EnumActionResult onItemUseFirst(ItemStack stack, EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ, EnumHand hand)
    {
		if (side != EnumFacing.UP)
		{
			return EnumActionResult.FAIL;
		}
		else if (!player.canPlayerEdit(pos.offset(side), side, stack))
		{
			return EnumActionResult.FAIL;
		}
		else if (SpecializationsMod.canSustainPlant(world, pos, EnumFacing.UP, this) && world.isAirBlock(pos.up()))
		{
			player.setActiveHand(hand);
			return EnumActionResult.PASS;
		}
		else
		{
			return EnumActionResult.FAIL;
		}
    }

	@Override
    public void onUsingTick(ItemStack stack, EntityLivingBase entityLiving, int count)
	{
		if(entityLiving instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) entityLiving;
			World world = player.worldObj;
			if(count % 4 == 0)
			{
		    	player.playSound(SoundEvents.BLOCK_GRASS_STEP, 0.5F, 1.4F);
			}
	
			if (world.isRemote)
			{
				this.onClientTick(stack, player, count);
			}
		}
	}

	@SideOnly(Side.CLIENT)
	public void onClientTick(ItemStack stack, EntityPlayer player, int count)
	{
		World world = player.worldObj;
		BlockPos pos = Minecraft.getMinecraft().objectMouseOver.getBlockPos();
		if (!ItemSeeds.canBePlanted(world, pos, this))
		{
			player.stopActiveHand();
		}
	}

	@Override
    public ItemStack onItemUseFinish(ItemStack stack, World worldIn, EntityLivingBase entityLiving)
	{
		if (worldIn.isRemote && entityLiving instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) entityLiving;
			BlockPos pos = Minecraft.getMinecraft().objectMouseOver.getBlockPos();
			SpecializationsMod.snw.sendToServer(new TimedUsageMessage(player.getPersistentID(), pos.up(), player.inventory.currentItem));
		}
		return stack;
	}

    @Override
    public net.minecraftforge.common.EnumPlantType getPlantType(net.minecraft.world.IBlockAccess world, BlockPos pos)
    {
        return net.minecraftforge.common.EnumPlantType.Crop;
    }

    @Override
    public net.minecraft.block.state.IBlockState getPlant(net.minecraft.world.IBlockAccess world, BlockPos pos)
    {
        return this.crops.getDefaultState();
    }
}