package bitevo.Zetal.LoMaS.Economy.Item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemCoin extends Item
{
	public ItemCoin() 
	{
		super();
        this.maxStackSize = 64;
        this.setCreativeTab(CreativeTabs.MATERIALS);
	}
}
