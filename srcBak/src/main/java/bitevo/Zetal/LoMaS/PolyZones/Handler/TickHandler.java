package bitevo.Zetal.LoMaS.PolyZones.Handler;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockFire;
import net.minecraft.block.state.IBlockState;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.storage.ExtendedBlockStorage;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.WorldTickEvent;
import net.minecraftforge.fml.relauncher.Side;
import bitevo.Zetal.LoMaS.PolyZones.PolyZone;
import bitevo.Zetal.LoMaS.PolyZones.PolyZonesMod;

public class TickHandler
{

	@SubscribeEvent(priority = EventPriority.HIGH)
	public void serverTickEnd(WorldTickEvent event)
	{
		if (event.side == Side.SERVER)
		{
			if (event.phase == TickEvent.Phase.END && event.world.getMinecraftServer().getTickCounter() % 10 == 0)
			{
				WorldServer world = (WorldServer) event.world;
				Collection<Chunk> chunks = world.getChunkProvider().getLoadedChunks();
				try
				{
					for (Iterator iterator = chunks.iterator(); iterator.hasNext(); world.theProfiler.endSection())
					{
						Chunk chunk = (Chunk) iterator.next();
						PolyZone zone = PolyZonesMod.processor.contained(world, chunk);
						
						if(zone != null)
						{
							//System.out.println(zone);
							int k = chunk.xPosition * 16;
							int l = chunk.zPosition * 16;
							world.theProfiler.startSection("getChunk");
							world.theProfiler.endStartSection("tickChunk");
							BlockPos blockpos;
							IBlockState iblockstate;
							Block block;

							world.theProfiler.endStartSection("tickBlocks");

							ExtendedBlockStorage[] aextendedblockstorage = chunk.getBlockStorageArray();
							int l2 = aextendedblockstorage.length;
							int x, y, z;
							int posY, posX;

							for (int j1 = 0; j1 < l2; ++j1)
							{
								ExtendedBlockStorage extendedblockstorage = aextendedblockstorage[j1];
								if (extendedblockstorage != null && extendedblockstorage.getNeedsRandomTick())
								{
									for (y = 0; y < 16; ++y)
									{
										posY = y + extendedblockstorage.getYLocation();
										for (x = 0; x < 16; ++x)
										{
											posX = x + k;
											for (z = 0; z < 16; ++z)
											{
												blockpos = new BlockPos(posX, posY, z + l);
												iblockstate = extendedblockstorage.get(x, y, z);
												block = iblockstate.getBlock();

												if (block instanceof BlockFire)
												{
													world.setBlockToAir(blockpos);
												}
											}
										}
									}
								}
							}
						}
					}
				}
				catch (ConcurrentModificationException e)
				{
					System.out.println("There was a concurrent modification exception... again. (Fire)");
				}

			}
		}
	}
}
