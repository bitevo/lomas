package bitevo.Zetal.LoMaS.AdminControlSystems;

import java.util.ArrayList;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;

public class MotDConnectionHandler
{
	@SubscribeEvent
	public void playerLoggedIn(PlayerLoggedInEvent event) 
	{
		ArrayList<String> motd = EventHookContainerClass.readTextData("motd.txt");
		for(int i = 0; i < motd.size(); i++)
		{
			EntityPlayerMP ply = (EntityPlayerMP) event.player;
			if(ply != null)
			{
				if(motd.get(i) != null && motd.get(i).length() > 0)
				{
					ply.addChatMessage(new TextComponentTranslation("\u00a75\u00a7l" + motd.get(i).substring(0, motd.get(i).length())));
				}
				else
				{
					ply.addChatMessage(new TextComponentTranslation(""));
				}
			}
		}
	}
}
