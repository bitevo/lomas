package bitevo.Zetal.LoMaS.Specializations.Item.DamageItems;

import java.util.List;

import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemCustomArmor extends ItemArmor
{
	public ItemCustomArmor(ArmorMaterial p_i45325_1_, int p_i45325_2_, EntityEquipmentSlot p_i45325_3_)
	{
		super(p_i45325_1_, p_i45325_2_, p_i45325_3_);
	}

	@Override
	public int getMaxDamage(ItemStack stack)
	{
		return SpecializationsMod.getMaxDamage(stack);
	}
}
