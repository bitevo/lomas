package bitevo.Zetal.LoMaS.Specializations.Items.ItemBlock;

import java.util.UUID;

import com.google.common.base.Function;
import com.mojang.authlib.GameProfile;

import bitevo.Zetal.LoMaS.Specializations.FoodInitializer;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntityQualityBlock;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemMultiTexture;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

public class ItemQualityBlock extends ItemMultiTexture
{
	public ItemQualityBlock(Block block)
	{
		super(block, block, new Function()
		{
            public String apply(ItemStack stack)
            {
                return FoodInitializer.qualities[stack.getItemDamage()];
            }
            public Object apply(Object p_apply_1_)
            {
                return this.apply((ItemStack)p_apply_1_);
            }
		});
	}
	
	public String getUnlocalizedName(ItemStack stack)
    {
        return super.getUnlocalizedName() + "." + ((String)this.nameFunction.apply(stack)).toLowerCase();
    }
	
	@Override
    public boolean placeBlockAt(ItemStack stack, EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ, IBlockState newState)
    {
        if (!world.setBlockState(pos, this.block.getDefaultState(), 3)) return false;

        IBlockState state = world.getBlockState(pos);
        if (state.getBlock() == this.block)
        {
            setTileEntityNBT(world, player, pos, stack);
            TileEntity te = world.getTileEntity(pos);
            if(te != null && te instanceof TileEntityQualityBlock)
            {
            	((TileEntityQualityBlock) te).setQuality(stack.getMetadata());
            }
            this.block.onBlockPlacedBy(world, pos, state, player, stack);
        }

        return true;
    }
}
