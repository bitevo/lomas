package bitevo.Zetal.LoMaS.Specializations.GUI;

import java.text.DecimalFormat;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import org.lwjgl.opengl.GL11;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Handler.SpecInfoGUIKeyBind;

@SideOnly(Side.CLIENT)
public class GuiSpecInfo extends GuiScreen
{
    private float oldMouseX;
    private float oldMouseY;
	ResourceLocation specInfo = new ResourceLocation(SpecializationsMod.modid, "gui/specinfo.png");
    private float xSize_lo;
    private float ySize_lo;
    
    private static EntityPlayer thePlayer;
    private float progress = 0.0f;
    private float endProgress = 0.0f;
    private int classlevel = 0;
    private String playerName;
    private String className;
    private String guildName;
    private String rankName;
    private int bankCC;

    public GuiSpecInfo(EntityPlayer par1EntityPlayer)
    {
        this.allowUserInput = false;
        this.thePlayer = par1EntityPlayer;
        LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(par1EntityPlayer);
        this.progress = lplayer.progress;
        this.endProgress = lplayer.getReqProgress();
        this.classlevel = lplayer.classlevel;
        this.playerName = lplayer.name;
        this.rankName = lplayer.rank;
        this.guildName = lplayer.guildtag;
        this.bankCC = lplayer.getBankCC();
        if(lplayer.specClass.length() >= 2)
        {
            this.className = lplayer.specClass.substring(0, 1).toUpperCase() + lplayer.specClass.substring(1);
        }
        else
        {
        	this.className = "Basic";
        }
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        this.buttonList.clear();
    }

    /**
     * Draw the foreground layer for the GuiContainer (everything in front of the items)
     */
    protected void drawGuiContainerForegroundLayer(int par1, int par2)
    {
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTicks)
    {
        super.drawScreen(mouseX, mouseY, partialTicks);
        this.drawGuiContainerBackgroundLayer(partialTicks, mouseX, mouseY);
        this.drawGuiContainerForegroundLayer(mouseX, mouseY);
        this.oldMouseX = (float)mouseX;
        this.oldMouseY = (float)mouseY;
    }

    /**
     * Draw the background layer for the GuiContainer (everything behind the items)
     */
    protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3)
    {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(specInfo);
		ScaledResolution res = new ScaledResolution(this.mc);
		int width = res.getScaledWidth();
		int height = res.getScaledHeight();
		int k = (width - 256) / 2;
		int l = (height - 188) / 2;
		int p = (width - 182) / 2;
		int o = (height - 5) / 2;
		//Width + 51
        this.drawTexturedModalRect(k, l - 15, 0, 0, 256, 188);
        //Complete Bar: 0 -> 182, 193 -> 198
        //Empty Bar: 0 -> 182, 188 -> 193
        this.drawTexturedModalRect(p, o + 72, 0, 188, 182, 5);
        if(this.progress > 0 && this.classlevel > 0)
        {
        	float fractionProgress = (this.progress / this.endProgress);
        	DecimalFormat df = new DecimalFormat("0.00");
        	String percentProgress = "" + df.format(fractionProgress * 100.0) + "%";
            this.fontRendererObj.drawString(percentProgress, p + 84, o + 63 + 1, 10881);
            this.fontRendererObj.drawString(percentProgress, p + 84 + 1, o + 63, 10881);
            this.fontRendererObj.drawString(percentProgress, p + 84 + 1,o + 63 + 1, 10881);
            this.fontRendererObj.drawString(percentProgress, p + 84, o + 63, 1600226);
            this.mc.getTextureManager().bindTexture(specInfo);
        	int pixel = (int) ((this.progress / this.endProgress) * 182);
            this.drawTexturedModalRect(p, o + 72, 0, 193, pixel, 5);
        }
        int x = 9;
        int y = 74;
        this.fontRendererObj.drawString(this.className, k + x + 1, l + y, 10881);
        this.fontRendererObj.drawString(this.className, k + x, l + y, 1600226);
        y = y + 15;
        this.fontRendererObj.drawString("Level " + this.classlevel, k + x + 1, l + y, 10881);
        this.fontRendererObj.drawString("Level " + this.classlevel, k + x, l + y, 1600226);
        y = y + 15;
        this.fontRendererObj.drawString(this.guildName, k + x + 1, l + y, 10881);
        this.fontRendererObj.drawString(this.guildName, k + x, l + y, 1600226);
        y = y + 15;
        this.fontRendererObj.drawString(this.rankName, k + x + 1, l + y, 10881);
        this.fontRendererObj.drawString(this.rankName, k + x, l + y, 1600226);
        y = y + 15;
        this.fontRendererObj.drawString(this.bankCC + "CC", k + x + 1, l + y, 10881);
        this.fontRendererObj.drawString(this.bankCC + "CC", k + x, l + y, 1600226);
        x = 7;
        y = 155;
        this.fontRendererObj.drawString("" + classlevel, k + x + 1, l + y, 10881);
        this.fontRendererObj.drawString("" + classlevel, k + x, l + y, 1600226);
        x = 70;
        y = -5;
        this.fontRendererObj.drawString(this.playerName, k + x + 1, l + y, 10881);
        this.fontRendererObj.drawString(this.playerName, k + x, l + y, 1600226);
        y = y + 10;
        displayClassInfo(this.className, x, y, k, l);
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        drawEntityOnScreen(k + 30, l + 63, 30, (float)(k + 51) - this.oldMouseX, (float)(l + 75 - 50) - this.oldMouseY, this.mc.thePlayer);
    }
    
    public void displayClassInfo(String specClass, int x, int y, int k, int l)
    {
		String level1 = "";
		String level2 = "";
		String level3 = "";
		String level4 = "";
		String level5 = "";
		String level6 = "";
		String level7 = "";
		String level8 = "";
		String level9 = "";
		String level10 = "";
		int spacer = 15;
		x = x - 3;
    	if(specClass.equalsIgnoreCase("woodsman"))
    	{
    		level1 = "1: Skilled woodcutting. Axe blocking.";
    		level2 = "2: Logcraft. +Axe dmg. +Animal dmg.";
    		level3 = "3: +Tree growth and harvesting.";
    		level4 = "4: Sap collection. +Woodcutting skill. ";
    		level5 = "5: +Axe dmg. +Tree Harvesting.";
    		level6 = "6: +Melee Dmg. +Woodcutting skill. ";
    		level7 = "7: +Overall axe usability.";
    		level8 = "8: +Dmg vs. animals. +Sap Collection.";
    		level9 = "9: +Melee Dmg. +Woodcutting skill.";
    		level10 = "10: Tree Climbing Unlocked";
    		String[] myStringArray = new String[]{level1, level2, level3, level4, level5, level6, level7, level8, level9, level10};
    		for(int i = 0; i < myStringArray.length; i++)
    		{
        		int color0 = -16777216;
        		int color1 = 16736352;
    			if(this.classlevel >= i + 1)
    			{
    				color1 = 8453920;
    			}
				color0 = -16777216 | (color1 & 16579836) >> 2 | color1 & -16777216;;
                this.fontRendererObj.drawString(myStringArray[i], k + x, l + y + 1, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x + 1, l + y, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x + 1, l + y + 1, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x, l + y, color1);
                y = y + spacer;
    		}
    	}
    	else if(specClass.equalsIgnoreCase("miner"))
    	{
    		level1 = "1: Lower hunger rate. Skilled mining.";
    		level2 = "2: Improved ore harvesting.";
    		level3 = "3: More efficient mining.";
    		level4 = "4: +Dmg vs. Monsters. +Mining speed.";
    		level5 = "5: +Ore Harvesting. -Hunger Rate";
    		level6 = "6: +Melee dmg. +Mining speed.";
    		level7 = "7: +Dmg vs. Monsters. +Mining speed.";
    		level8 = "8: Better mining efficiency.";
    		level9 = "9: +Melee dmg. +Mining speed.";
    		level10 = "10: Diamond Spirit: Instant mine. ('F')";
    		String[] myStringArray = new String[]{level1, level2, level3, level4, level5, level6, level7, level8, level9, level10};
    		for(int i = 0; i < myStringArray.length; i++)
    		{
        		int color0 = -16777216;
        		int color1 = 16736352;
    			if(this.classlevel >= i + 1)
    			{
    				color1 = 8453920;
    			}
				color0 = -16777216 | (color1 & 16579836) >> 2 | color1 & -16777216;;
                this.fontRendererObj.drawString(myStringArray[i], k + x, l + y + 1, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x + 1, l + y, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x + 1, l + y + 1, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x, l + y, color1);
                y = y + spacer;
    		}
    	}
    	else if(specClass.equalsIgnoreCase("warrior"))
    	{
    		level1 = "1: Improved HP. Lowered speed.";
    		level2 = "2: Higher HP. Better armor usage.";
    		level3 = "3: +PVP dmg. Better armor usage.";
    		level4 = "4: +Dmg vs. Armor. Higher HP.";
    		level5 = "5: +Bow Skill. +Dmg vs. Monsters.";
    		level6 = "6: +Speed. Improved blocking.";
    		level7 = "7: +PVP dmg. Better armor usage.";
    		level8 = "8: +Dmg vs. Armor. Higher HP.";
    		level9 = "9: -Potion Damage. +Armor Usage.";
    		level10 = "10: Adrenaline Rush: HP Regen ('F')";
    		String[] myStringArray = new String[]{level1, level2, level3, level4, level5, level6, level7, level8, level9, level10};
    		for(int i = 0; i < myStringArray.length; i++)
    		{
        		int color0 = -16777216;
        		int color1 = 16736352;
    			if(this.classlevel >= i + 1)
    			{
    				color1 = 8453920;
    			}
				color0 = -16777216 | (color1 & 16579836) >> 2 | color1 & -16777216;;
                this.fontRendererObj.drawString(myStringArray[i], k + x, l + y + 1, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x + 1, l + y, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x + 1, l + y + 1, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x, l + y, color1);
                y = y + spacer;
    		}
    	}
    	else if(specClass.equalsIgnoreCase("stealth"))
    	{
    		level1 = "1: Increased speed. Reduced HP.";
    		level2 = "2: +Speed. +Sneaking speed.";
    		level3 = "3: +Overall Speed. +Backstab dmg.";
    		level4 = "4: +Sneak speed. +Pickpocket speed.";
    		level5 = "5: +Sneaking speed. +Backstab dmg.";
    		level6 = "6: +Speed. +Backstab dmg.";
    		level7 = "7: +HP. +Speed. +Pickpocket speed.";
    		level8 = "8: Movement is silenced.";
    		level9 = "9: Hidden while sneaking.";
    		level10 = "10: Ender Arts: Short teleport. ('F')";
    		String[] myStringArray = new String[]{level1, level2, level3, level4, level5, level6, level7, level8, level9, level10};
    		for(int i = 0; i < myStringArray.length; i++)
    		{
        		int color0 = -16777216;
        		int color1 = 16736352;
    			if(this.classlevel >= i + 1)
    			{
    				color1 = 8453920;
    			}
				color0 = -16777216 | (color1 & 16579836) >> 2 | color1 & -16777216;;
                this.fontRendererObj.drawString(myStringArray[i], k + x, l + y + 1, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x + 1, l + y, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x + 1, l + y + 1, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x, l + y, color1);
                y = y + spacer;
    		}
    	}
    	else if(specClass.equalsIgnoreCase("spellbinder"))
    	{
    		level1 = "1: +XP Gain. Enchant lvl 10.";
    		level2 = "2: Better enchanting. +Brewing.";
    		level3 = "3: +Brewing. Haste Enchantment.";
    		level4 = "4: Enchant lvl 20. +Potion creation.";
    		level5 = "5: +Enchanting. +Potion creation.";
    		level6 = "6: +XP Gain. +Brewing efficiency.";
    		level7 = "7: +Brewing speed and efficiency.";
    		level8 = "8: Enchant lvl 30. +Brewing speed.";
    		level9 = "9: +XP Gain. Skill Enchantment.";
    		level10 = "10: Brewed potions are stackable.";
    		String[] myStringArray = new String[]{level1, level2, level3, level4, level5, level6, level7, level8, level9, level10};
    		for(int i = 0; i < myStringArray.length; i++)
    		{
        		int color0 = -16777216;
        		int color1 = 16736352;
    			if(this.classlevel >= i + 1)
    			{
    				color1 = 8453920;
    			}
				color0 = -16777216 | (color1 & 16579836) >> 2 | color1 & -16777216;;
                this.fontRendererObj.drawString(myStringArray[i], k + x, l + y + 1, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x + 1, l + y, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x + 1, l + y + 1, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x, l + y, color1);
                y = y + spacer;
    		}
    	}
    	else if(specClass.equalsIgnoreCase("blacksmith"))
    	{
    		level1 = "1: +Smelting. +Crafting.";
    		level2 = "2: Stonecraft. +Smelting. +Crafting.";
    		level3 = "3: +Tool durability. +Smelting bonus.";
    		level4 = "4: Ingotcraft. +Tool durability. ";
    		level5 = "5: +Tool durability. +Smelting bonus.";
    		level6 = "6: +Crafting. +Smelting.";
    		level7 = "7: +Smelting bonus.";
    		level8 = "8: Diamondcraft. +Toolcrafting.";
    		level9 = "9: +Tool durability. +Smelting.";
    		level10 = "10: Obsidian crafting unlocked.";
    		String[] myStringArray = new String[]{level1, level2, level3, level4, level5, level6, level7, level8, level9, level10};
    		for(int i = 0; i < myStringArray.length; i++)
    		{
        		int color0 = -16777216;
        		int color1 = 16736352;
    			if(this.classlevel >= i + 1)
    			{
    				color1 = 8453920;
    			}
				color0 = -16777216 | (color1 & 16579836) >> 2 | color1 & -16777216;;
                this.fontRendererObj.drawString(myStringArray[i], k + x, l + y + 1, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x + 1, l + y, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x + 1, l + y + 1, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x, l + y, color1);
                y = y + spacer;
    		}
    	}
    	else if(specClass.equalsIgnoreCase("farmer"))
    	{
    		level1 = "1: Can collect seeds from grass.";
    		level2 = "2: +Seed chance. +Melee dmg.";
    		level3 = "3: +Seed.  +1 Block from water.";
    		level4 = "4: +Growth speed. +Melee dmg.";
    		level5 = "5: +Growth. +1 Block from water.";
    		level6 = "6: +Dmg vs. Wolves. +Growth speed.";
    		level7 = "7: Chance for twin animal birth.";
    		level8 = "8: Improved nether farming.";
    		level9 = "9: +Nether farming. +Nether growth.";
    		level10 = "10: No water required for growth.";
    		String[] myStringArray = new String[]{level1, level2, level3, level4, level5, level6, level7, level8, level9, level10};
    		for(int i = 0; i < myStringArray.length; i++)
    		{
        		int color0 = -16777216;
        		int color1 = 16736352;
    			if(this.classlevel >= i + 1)
    			{
    				color1 = 8453920;
    			}
				color0 = -16777216 | (color1 & 16579836) >> 2 | color1 & -16777216;;
                this.fontRendererObj.drawString(myStringArray[i], k + x, l + y + 1, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x + 1, l + y, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x + 1, l + y + 1, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x, l + y, color1);
                y = y + spacer;
    		}
    	}
    	else
    	{

    		level1 = "You should really choose a class!";
    		String[] myStringArray = new String[]{level1};
    		for(int i = 0; i < myStringArray.length; i++)
    		{
        		int color0 = -16777216;
        		int color1 = 16736352;
    			if(this.classlevel >= i + 1)
    			{
    				color1 = 8453920;
    			}
				color0 = -16777216 | (color1 & 16579836) >> 2 | color1 & -16777216;;
                this.fontRendererObj.drawString(myStringArray[i], k + x, l + y + 1, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x + 1, l + y, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x + 1, l + y + 1, color0);
                this.fontRendererObj.drawString(myStringArray[i], k + x, l + y, color1);
                y = y + spacer;
    		}
    	}
    }
    
    @Override
    public boolean doesGuiPauseGame()
    {
        return false;
    }

    public static void drawEntityOnScreen(int p_147046_0_, int p_147046_1_, int p_147046_2_, float p_147046_3_, float p_147046_4_, EntityLivingBase p_147046_5_)
    {
        GlStateManager.enableColorMaterial();
        GlStateManager.pushMatrix();
        GlStateManager.translate((float)p_147046_0_, (float)p_147046_1_, 50.0F);
        GlStateManager.scale((float)(-p_147046_2_), (float)p_147046_2_, (float)p_147046_2_);
        GlStateManager.rotate(180.0F, 0.0F, 0.0F, 1.0F);
        float f2 = p_147046_5_.renderYawOffset;
        float f3 = p_147046_5_.rotationYaw;
        float f4 = p_147046_5_.rotationPitch;
        float f5 = p_147046_5_.prevRotationYawHead;
        float f6 = p_147046_5_.rotationYawHead;
        GlStateManager.rotate(135.0F, 0.0F, 1.0F, 0.0F);
        RenderHelper.enableStandardItemLighting();
        GlStateManager.rotate(-135.0F, 0.0F, 1.0F, 0.0F);
        GlStateManager.rotate(-((float)Math.atan((double)(p_147046_4_ / 40.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
        p_147046_5_.renderYawOffset = (float)Math.atan((double)(p_147046_3_ / 40.0F)) * 20.0F;
        p_147046_5_.rotationYaw = (float)Math.atan((double)(p_147046_3_ / 40.0F)) * 40.0F;
        p_147046_5_.rotationPitch = -((float)Math.atan((double)(p_147046_4_ / 40.0F))) * 20.0F;
        p_147046_5_.rotationYawHead = p_147046_5_.rotationYaw;
        p_147046_5_.prevRotationYawHead = p_147046_5_.rotationYaw;
        GlStateManager.translate(0.0F, 0.0F, 0.0F);
        RenderManager rendermanager = Minecraft.getMinecraft().getRenderManager();
        rendermanager.setPlayerViewY(180.0F);
        rendermanager.setRenderShadow(false);
        rendermanager.doRenderEntity(p_147046_5_, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F, false);
        rendermanager.setRenderShadow(true);
        p_147046_5_.renderYawOffset = f2;
        p_147046_5_.rotationYaw = f3;
        p_147046_5_.rotationPitch = f4;
        p_147046_5_.prevRotationYawHead = f5;
        p_147046_5_.rotationYawHead = f6;
        GlStateManager.popMatrix();
        RenderHelper.disableStandardItemLighting();
        GlStateManager.disableRescaleNormal();
        GlStateManager.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GlStateManager.disableTexture2D();
        GlStateManager.setActiveTexture(OpenGlHelper.defaultTexUnit);
    }
    
    @Override
    protected void keyTyped(char par1, int par2)
    {
        if (par2 == 1 || par2 == this.mc.gameSettings.keyBindInventory.getKeyCode() || par2 == SpecInfoGUIKeyBind.keys[0].getKeyCode())
        {
            this.mc.thePlayer.closeScreen();
        }
    }

    protected void actionPerformed(GuiButton par1GuiButton)
    {
    }
    
    @Override
    public void updateScreen()
    {
        if (!this.mc.thePlayer.isEntityAlive() || this.mc.thePlayer.isDead)
        {
            this.mc.thePlayer.closeScreen();
        }
    }
}
