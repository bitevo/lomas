package bitevo.Zetal.LoMaS.PolyZones;

import java.io.Serializable;

public class Permissions implements Serializable
{

	private String welcomeMessage;
	private String goodbyeMessage;
	private boolean mobs = false;
	private boolean pvp = true;
	private Access access = new Access();

	public Permissions()
	{
		welcomeMessage = "";
		goodbyeMessage = "";
		access.allowAll();
		access.denyDestroy();
		access.denyPlace();
	}

	public Access getDefaultPermissions()
	{
		return access;
	}

	public void setWelcome(String w)
	{
		welcomeMessage = w;
	}

	public void setGoodbye(String g)
	{
		goodbyeMessage = g;
	}

	public void setMobs(boolean x)
	{
		mobs = x;
	}

	public void setPvp(boolean x)
	{
		pvp = x;
	}

	public String getWelcome()
	{
		return welcomeMessage;
	}

	public String getGoodbye()
	{
		return goodbyeMessage;
	}

	public boolean getMobs()
	{
		return mobs;
	}

	public boolean getPvp()
	{
		return pvp;
	}
}
