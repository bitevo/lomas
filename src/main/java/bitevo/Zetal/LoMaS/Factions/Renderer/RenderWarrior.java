package bitevo.Zetal.LoMaS.Factions.Renderer;

import bitevo.Zetal.LoMaS.Factions.FactionsMod;
import bitevo.Zetal.LoMaS.Factions.Entity.EntityPigmanWarrior;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.layers.LayerBipedArmor;
import net.minecraft.client.renderer.entity.layers.LayerHeldItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class RenderWarrior extends RenderLiving<EntityPigmanWarrior>
{
	public RenderWarrior(RenderManager p_i46153_1_, ModelBase p_i46153_2_, float p_i46153_3_)
	{
		super(p_i46153_1_, p_i46153_2_, p_i46153_3_);
		this.addLayer(new LayerHeldItem(this));
		this.addLayer(new LayerBipedArmor(this)
		{
			@Override
			protected void initArmor()
			{
				this.modelLeggings = new ModelWarrior(0.5F, true);
				this.modelArmor = new ModelWarrior(1.0F, true);
			}
		});
	}

	@Override
	public void doRender(EntityPigmanWarrior entity, double x, double y, double z, float entityYaw, float partialTicks)
	{
		if (entity != null && entity instanceof EntityPigmanWarrior)
		{
			ModelWarrior modelpigman = (ModelWarrior) super.getMainModel();
			EntityPigmanWarrior pigman = (EntityPigmanWarrior) entity;
			ItemStack itemstackM = pigman.getHeldItemMainhand();
			ItemStack itemstackO = pigman.getHeldItemOffhand();

			if (itemstackM == null)
			{
				modelpigman.rightArmPose = ModelBiped.ArmPose.EMPTY;
			}
			else
			{
				modelpigman.rightArmPose = ModelBiped.ArmPose.ITEM;
			}

			if (itemstackO == null)
			{
				modelpigman.leftArmPose = ModelBiped.ArmPose.EMPTY;
			}
			else
			{
				modelpigman.leftArmPose = ModelBiped.ArmPose.ITEM;
			}
			super.doRender(entity, x, y, z, entityYaw, partialTicks);
		}
	}

	@Override
	public void transformHeldFull3DItemLayer()
	{
		GlStateManager.translate(0.0F, 0.1875F, 0.0F);
	}

	@Override
	protected ResourceLocation getEntityTexture(EntityPigmanWarrior entity)
	{
		return new ResourceLocation(FactionsMod.modid, "textures/entity/warrior.png");
	}
}
