package bitevo.Zetal.LoMaS.Shared;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.lwjgl.opengl.GL11;

import com.google.common.collect.Lists;
import com.mojang.authlib.GameProfile;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils.OreDictStuff;
import net.minecraft.block.Block;
import net.minecraft.block.properties.IProperty;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.command.ICommandSender;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraft.server.management.UserListOps;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.WorldServer;
import net.minecraft.world.storage.loot.LootTable;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

public class LoMaS_Utils
{
	public static int entityID = 7776;
	public static int packetID = 7776;

	public static boolean ACS = false;
	public static boolean BFM = false;
	public static boolean CIT = false;
	public static boolean ECO = false;
	public static boolean FAC = false;
	public static boolean HEI = false;
	public static boolean PZM = false;
	public static boolean SCM = false;
	public static boolean OUT = false;
	public static boolean TIW = false;

	public static CreativeTabs tabLoMaS = new CreativeTabs("LoMaS")
	{
		@Override
		public Item getTabIconItem()
		{
			return SCM ? bitevo.Zetal.LoMaS.Specializations.SpecializationsMod.EMERALDHEART : 
				CIT ? bitevo.Zetal.LoMaS.Citizens.CitizensMod.deed :
				FAC ? bitevo.Zetal.LoMaS.Factions.FactionsMod.OBELISK_CORE :
				Items.RECORD_13;
		}

		@Override
		@SideOnly(Side.CLIENT)
		public String getTranslatedTabLabel()
		{
			return this.getTabLabel();
		}
	};

	public static int getNextEntityID()
	{
		entityID = entityID + 1;
		return entityID;
	}

	public static int getNextPacketID()
	{
		packetID = packetID + 1;
		return packetID;
	}

	public static int getCurrentPacketID()
	{
		return packetID;
	}

	public static boolean isSenderAdmin(ICommandSender sender)
	{
		if (!sender.getEntityWorld().isRemote)
		{
			net.minecraft.server.MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
			if (server.isSinglePlayer())
			{
				return true;
			}
			UserListOps ulo = server.getPlayerList().getOppedPlayers();
			if (sender.getCommandSenderEntity() instanceof EntityPlayer)
			{
				GameProfile gp = ((EntityPlayer) sender.getCommandSenderEntity()).getGameProfile();
				int perm = ulo.getPermissionLevel(gp);
				// System.out.println(perm + " " + gp);
				if (perm >= 3)
				{
					return true;
				}
			}
		}
		return false;
	}

	public static boolean isPlayerAdmin(EntityPlayer sender)
	{
		if (!sender.worldObj.isRemote)
		{
			net.minecraft.server.MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
			if (server.isSinglePlayer())
			{
				return true;
			}
			UserListOps ulo = server.getPlayerList().getOppedPlayers();
			GameProfile gp = sender.getGameProfile();
			int perm = ulo.getPermissionLevel(gp);
			// System.out.println(perm + " " + gp);
			if (perm >= 3)
			{
				return true;
			}
		}
		return false;
	}

	public static void registerItem(Item item)
	{
		LoMaS_Utils.registerItem(item, new int[1]);
	}

	public static void registerBlock(Block block, Item item)
	{
		LoMaS_Utils.registerBlock(block, item, new int[1]);
	}

	public static void registerItem(Item item, int[] meta)
	{
		if (item.getRegistryName() == null || item.getRegistryName().equals(""))
		{
			item.setRegistryName(item.getUnlocalizedName().replace("item.", "").replace("block.", "").replace("tile.", ""));
		}
		GameRegistry.register(item);

		if (FMLCommonHandler.instance().getSide() == Side.CLIENT)
		{
			for (int i : meta)
			{
				String mrl = item.getRegistryName().getResourceDomain() + ":" + item.getRegistryName().getResourcePath();
				registerItemModelMeshers(item, i, mrl);
			}
		}
	}

	public static void registerItem(Item item, int[] meta, IProperty<?> prefix)
	{
		if (item.getRegistryName() == null || item.getRegistryName().equals(""))
		{
			item.setRegistryName(item.getUnlocalizedName().replace("item.", "").replace("block.", "").replace("tile.", ""));
		}
		GameRegistry.register(item);
		if (FMLCommonHandler.instance().getSide() == Side.CLIENT)
		{
			for (int i : meta)
			{
				String mrl = item.getRegistryName().getResourceDomain() + ":" + prefix.getAllowedValues().toArray()[i] + "_" + item.getRegistryName().getResourcePath();
				registerItemModelMeshers(item, i, mrl);
				registerItemVariants(item, mrl);
			}
		}
	}

	public static void registerItem(Item item, HashMap<Integer, String> resources)
	{
		if (item.getRegistryName() == null || item.getRegistryName().equals(""))
		{
			item.setRegistryName(item.getUnlocalizedName().replace("item.", "").replace("block.", "").replace("tile.", ""));
		}
		GameRegistry.register(item);
		if (FMLCommonHandler.instance().getSide() == Side.CLIENT)
		{
			for (Map.Entry<Integer, String> entry : resources.entrySet())
			{
				String mrl = item.getRegistryName().getResourceDomain() + ":" + entry.getValue();
				registerItemModelMeshers(item, entry.getKey(), mrl);
				registerItemVariants(item, mrl);
			}
		}
	}

	public static void registerItem(Item item, int[] meta, String[] prefixes, String[] suffixes)
	{
		if (item.getRegistryName() == null || item.getRegistryName().equals(""))
		{
			item.setRegistryName(item.getUnlocalizedName().replace("item.", "").replace("block.", "").replace("tile.", ""));
		}
		GameRegistry.register(item);
		if (FMLCommonHandler.instance().getSide() == Side.CLIENT)
		{
			for (int i : meta)
			{
				String prefix = prefixes != null ? prefixes[i] + "_" : "";
				String suffix = suffixes != null ? "_" + suffixes[i] : "";
				String mrl = item.getRegistryName().getResourceDomain() + ":" + prefix + item.getRegistryName().getResourcePath() + suffix;
				registerItemModelMeshers(item, i, mrl);
				registerItemVariants(item, mrl);
			}
		}
	}

	public static void registerBlock(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		if (item == null)
		{
			item = new ItemBlock(block);
			if (block.getRegistryName() != null && !block.getRegistryName().equals(""))
			{
				item.setRegistryName(block.getRegistryName());
			}
			LoMaS_Utils.registerBlock(block, item, new int[1]);
		}
		else
		{
			if (block.getRegistryName() == null || block.getRegistryName().equals(""))
			{
				block.setRegistryName(block.getUnlocalizedName().replace("item.", "").replace("block.", "").replace("tile.", ""));
			}
			GameRegistry.register(block);
		}
	}

	public static void registerBlock(Block block, Item item, int[] meta)
	{
		if (block.getRegistryName() == null || block.getRegistryName().equals(""))
		{
			block.setRegistryName(block.getUnlocalizedName().replace("item.", "").replace("block.", "").replace("tile.", ""));
		}
		GameRegistry.register(block);
		if (item != null)
		{
			LoMaS_Utils.registerItem(item, meta);
		}
	}

	public static void registerBlock(Block block, Item item, int[] meta, IProperty<?> prefix)
	{
		if (block.getRegistryName() == null || block.getRegistryName().equals(""))
		{
			block.setRegistryName(block.getUnlocalizedName().replace("item.", "").replace("block.", "").replace("tile.", ""));
		}
		GameRegistry.register(block);

		if (FMLCommonHandler.instance().getSide() == Side.CLIENT)
		{
			registerBlockWithStateMapper(block, item, meta, prefix);
		}

		if (item != null)
		{
			LoMaS_Utils.registerItem(item, meta, prefix);
		}
	}

	@SideOnly(Side.CLIENT)
	public static void registerBlockWithStateMapper(Block block, Item item, int[] meta, IProperty<?> prefix)
	{
		net.minecraft.client.Minecraft.getMinecraft().getBlockRendererDispatcher().getBlockModelShapes().registerBlockWithStateMapper(block, (new net.minecraft.client.renderer.block.statemap.StateMap.Builder()).withName(prefix).withSuffix("_" + block.getRegistryName().getResourcePath()).build());
	}

	@SideOnly(Side.CLIENT)
	public static void registerItemModelMeshers(Item item, int meta, String mrls)
	{
		ModelResourceLocation mrl = new ModelResourceLocation(mrls, "inventory");
		net.minecraft.client.renderer.RenderItem itemRenderer = net.minecraft.client.Minecraft.getMinecraft().getRenderItem();
		itemRenderer.getItemModelMesher().register(item, meta, mrl);
	}

	@SideOnly(Side.CLIENT)
	public static void registerItemVariants(Item item, String mrls)
	{
		ModelResourceLocation mrl = new ModelResourceLocation(mrls, "inventory");
		net.minecraft.client.renderer.block.model.ModelBakery.registerItemVariants(item, mrl);
	}

	@SideOnly(Side.SERVER)
	public static void setBlock(WorldServer server, int x, int y, int z, Block block)
	{
		server.setBlockState(new BlockPos(x, y, z), block.getDefaultState());
	}

	public static void drawLine(int x1, int y1, int x2, int y2, int color, int width)
	{
		float f3 = (float) (color >> 24 & 255) / 255.0F;
		float f = (float) (color >> 16 & 255) / 255.0F;
		float f1 = (float) (color >> 8 & 255) / 255.0F;
		float f2 = (float) (color & 255) / 255.0F;
		GlStateManager.enableBlend();
		GlStateManager.disableTexture2D();
		GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
		GlStateManager.color(f, f1, f2, f3);
		
		Tessellator tessellator = Tessellator.getInstance();
		VertexBuffer vertexbuffer = tessellator.getBuffer();
		
		float dx = (x2 - x1);
		float dy = (y2 - y1);
		float sigdx = Math.signum(dx);
		float sigdy = Math.signum(dy);
		
		float widthX = width / 2;
		float widthY = width / 2;
		
		if(dx == 0)
		{
			widthX = width;
			widthY = 0;
		}
		else if(dy == 0)
		{
			widthY = width;
			widthX = 0;
		}
		else
		{
			widthX = (widthX * Math.abs(dx / dy)) * 2;
			widthY = (widthY * Math.abs(dy / dx)) * 2;
		}

		vertexbuffer.begin(7, DefaultVertexFormats.POSITION);
		vertexbuffer.pos(x1 - widthX*(-sigdy), y1 - widthY*(sigdx), 0.0D).endVertex();
		vertexbuffer.pos((x1 + widthX*(-sigdy)), (y1 + widthY*(sigdx)), 0.0D).endVertex();
		vertexbuffer.pos(x2 + widthX*(-sigdy), y2 + widthY*(sigdx), 0.0D).endVertex();
		vertexbuffer.pos((x2 - widthX*(-sigdy)), (y2 - widthY*(sigdx)), 0.0D).endVertex();
		tessellator.draw();
		
        GlStateManager.enableTexture2D();
        GlStateManager.disableBlend();
	}

	/**
	 * Draws a solid color rectangle with the specified coordinates and color.
	 */
	public static void drawRect(int[] topLeft, int[] topRight, int[] botLeft, int[] botRight, int color)
	{
		float f3 = (float) (color >> 24 & 255) / 255.0F;
		float f = (float) (color >> 16 & 255) / 255.0F;
		float f1 = (float) (color >> 8 & 255) / 255.0F;
		float f2 = (float) (color & 255) / 255.0F;
		Tessellator tessellator = Tessellator.getInstance();
		VertexBuffer vertexbuffer = tessellator.getBuffer();
		GlStateManager.enableBlend();
		GlStateManager.disableTexture2D();
		//GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
		//GlStateManager.color(f, f1, f2, f3);
		vertexbuffer.begin(7, DefaultVertexFormats.POSITION);
		vertexbuffer.pos((double) topLeft[0], (double) topLeft[1], 0.0D).endVertex();
		vertexbuffer.pos((double) topRight[0], (double) topRight[1], 0.0D).endVertex();
		vertexbuffer.pos((double) botLeft[0], (double) botLeft[1], 0.0D).endVertex();
		vertexbuffer.pos((double) botRight[0], (double) botRight[1], 0.0D).endVertex();
		tessellator.draw();
		GlStateManager.enableTexture2D();
		GlStateManager.disableBlend();
	}
	
	public static void addChatMessage(ICommandSender player, String message)
	{
		TextComponentTranslation mes = new TextComponentTranslation(message);
		player.addChatMessage(mes);
	}

	public static class RecipeReplacer
	{

		/**
		 * Replaces all recipes that have {@code originalItem} as an input or output and aren't in {@code exclusions} with an equivalent ore recipe, using {@code replacementOutputItem} as the new
		 * output and {@code replacementInputOreName} as the new input.
		 * <p>
		 * Only replaces recipes that use {@link ShapedRecipes}, {@link ShapelessRecipes}, {@link ShapedOreRecipe} or {@link ShapelessOreRecipe} because any other {@link IRecipe} implementations or
		 * subclasses of these classes could have arbitrary behaviour that can't be replicated by a standard ore recipe.
		 *
		 * @param exclusions
		 *            A list of recipes to exclude from replacement
		 * @param originalItem
		 *            The original item to replace recipes for
		 * @param replacementOutputItem
		 *            The new output item
		 * @param replacementInputOreName
		 *            The new input ore name
		 * @return A map of replaced recipes, with the original recipes as keys and the replacements as values
		 */
		public static Map<IRecipe, IRecipe> replaceInputAndOutput(Set<IRecipe> exclusions, ItemStack originalItem, ItemStack replacementOutputItem, String replacementInputOreName)
		{
			// originalRecipe -> replacementRecipe
			Map<IRecipe, IRecipe> replacedRecipes = new HashMap<>();

			@SuppressWarnings("unchecked")
			List<IRecipe> recipes = CraftingManager.getInstance().getRecipeList();
			recipes.replaceAll(recipe -> {
				if (!exclusions.contains(recipe))
				{
					try
					{
						IRecipe replacement = null;

						// Compare classes instead of using instanceof so we don't replace subclasses
						Class<? extends IRecipe> recipeClass = recipe.getClass();
						if (recipeClass == ShapedRecipes.class)
						{
							replacement = convertShapedRecipe((ShapedRecipes) recipe, originalItem, replacementOutputItem, replacementInputOreName);
						}
						else if (recipeClass == ShapedOreRecipe.class)
						{
							replacement = convertShapedRecipe((ShapedOreRecipe) recipe, originalItem, replacementOutputItem, replacementInputOreName);
						}
						else if (recipeClass == ShapelessRecipes.class)
						{
							replacement = convertShapelessRecipe((ShapelessRecipes) recipe, originalItem, replacementOutputItem, replacementInputOreName);
						}
						else if (recipeClass == ShapelessOreRecipe.class)
						{
							replacement = convertShapelessRecipe((ShapelessOreRecipe) recipe, originalItem, replacementOutputItem, replacementInputOreName);
						}

						if (replacement != null)
						{
							replacedRecipes.put(recipe, replacement);
							return replacement;
						}
					}
					catch (UnableToReplaceRecipeException e)
					{
						// Log it at DEBUG (log file only) rather than ERROR (console and log file) because the most common cause is an empty ore list ingredient.
						// If this is the case, the player can't craft the original recipe anyway; so they probably don't care that it wasn't replaced.
						System.err.println("Error replacing recipe for original item " + originalItem + " " + e.getMessage());
					}
				}

				return recipe;
			});

			return replacedRecipes;
		}

		// [row][width - 1]
		private static final String[][] SHAPE_STRINGS = { { "A", "AB", "ABC" }, { "D", "DE", "DEF" }, { "G", "GH", "GHI" } };

		/**
		 * Gets an array of shape strings for the specified recipe width and height.
		 *
		 * @param recipeHeight
		 *            The recipe height
		 * @param recipeWidth
		 *            The recipe width
		 * @return The shape strings
		 */
		private static String[] getShapeStrings(Object[] ingredients, int recipeHeight, int recipeWidth)
		{
			String[] result = new String[recipeHeight];
			for (int row = 0; row < recipeHeight; row++)
			{
				char[] shapeString = SHAPE_STRINGS[row][recipeWidth - 1].toCharArray();

				for (int col = 0; col < recipeWidth; col++)
				{
					if (ingredients[row * recipeWidth + col] == null)
					{ // If this ingredient is null,
						shapeString[col] = ' '; // Replace it with a space in the shape string
					}
				}

				result[row] = String.valueOf(shapeString);
			}
			return result;
		}

		/**
		 * Replaces the {@code ingredient} with {@code replacementInputOreName} if it's {@code originalItem} or the corresponding ore name if it's a list of ores.
		 *
		 * @param ingredient
		 *            The ingredient
		 * @param originalItem
		 *            The item to replace
		 * @param replacementInputOreName
		 *            The new input ore name
		 * @return The replacement, or {@code ingredient} if there was no replacement needed
		 * @throws OreDictStuff.InvalidOreException
		 *             When no matching ore name is found for an ore list
		 */
		private static Object replaceIngredient(Object ingredient, ItemStack originalItem, String replacementInputOreName) throws OreDictStuff.InvalidOreException
		{
			if (ingredient instanceof ItemStack && OreDictionary.itemMatches(originalItem, (ItemStack) ingredient, true))
			{ // If the ingredient is the item to replace,
				return replacementInputOreName; // Return the replacement ore name
			}
			else if (ingredient instanceof ArrayList)
			{ // If the ingredient is a list of ores,
				@SuppressWarnings("unchecked")
				List<ItemStack> oreList = (List<ItemStack>) ingredient;
				return OreDictStuff.getOreNameForOreList(oreList); // Return the ore name
			}
			else
			{
				return ingredient; // Else return the ingredient
			}
		}

		/**
		 * Creates an input list for a {@link ShapedOreRecipe}, using {@link #replaceIngredient(Object, ItemStack, String)} to process each ingredient.
		 *
		 * @param ingredients
		 *            The original recipe's ingredient list
		 * @param recipeHeight
		 *            The original recipe's height
		 * @param recipeWidth
		 *            The original recipe's width
		 * @param originalItem
		 *            The item to replace
		 * @param replacementInputOreName
		 *            The new input ore name
		 * @return The input list
		 * @throws OreDictStuff.InvalidOreException
		 *             When no matching ore name is found for an ore list ingredient
		 */
		private static List<Object> createShapedInputs(Object[] ingredients, int recipeHeight, int recipeWidth, ItemStack originalItem, String replacementInputOreName) throws OreDictStuff.InvalidOreException
		{
			String[] shapeStrings = getShapeStrings(ingredients, recipeHeight, recipeWidth);
			String fullShapeString = StringUtils.join(shapeStrings);

			List<Object> input = new ArrayList<>();
			input.add(shapeStrings); // Add the shape strings

			for (int i = 0; i < ingredients.length; i++)
			{ // For each ingredient,
				Object ingredient = ingredients[i];
				if (ingredient != null)
				{
					char inputChar = fullShapeString.charAt(i);
					input.add(inputChar); // Add the character for the ingredient
					input.add(replaceIngredient(ingredient, originalItem, replacementInputOreName)); // Add the replacement ingredient
				}
			}

			return input;
		}

		/**
		 * Creates an input list for a {@link ShapelessOreRecipe}, using {@link #replaceIngredient(Object, ItemStack, String)} to process each ingredient.
		 *
		 * @param ingredients
		 *            The original recipe's ingredient list
		 * @param originalItem
		 *            The item to replace
		 * @param replacementInputOreName
		 *            The new input ore name
		 * @return The input list
		 * @throws OreDictStuff.InvalidOreException
		 *             When no matching ore name is found for an ore list ingredient
		 */
		private static List<Object> createShapelessInputs(List<Object> ingredients, ItemStack originalItem, String replacementInputOreName) throws OreDictStuff.InvalidOreException
		{
			List<Object> input = new ArrayList<>();

			for (Object ingredient : ingredients)
			{ // For each ingredient,
				input.add(replaceIngredient(ingredient, originalItem, replacementInputOreName)); // Add the replacement ingredient
			}

			return input;
		}

		/**
		 * Creates a {@link ShapedOreRecipe} from a {@link ShapedRecipes}, replacing {@code originalItem} with {@code replacementOutputItem} or {@code replacementInputOreName} as appropriate.
		 *
		 * @param recipe
		 *            The original recipe
		 * @param originalItem
		 *            The item to replace
		 * @param replacementOutputItem
		 *            The new output item
		 * @param replacementInputOreName
		 *            The new input ore name
		 * @return The new recipe, or {@code null} if no replacement was needed
		 * @throws UnableToReplaceRecipeException
		 *             When the recipe couldn't be replaced
		 */
		private static IRecipe convertShapedRecipe(ShapedRecipes recipe, ItemStack originalItem, ItemStack replacementOutputItem, String replacementInputOreName) throws UnableToReplaceRecipeException
		{
			try
			{
				List<Object> input = createShapedInputs(recipe.recipeItems, recipe.recipeHeight, recipe.recipeWidth, originalItem, replacementInputOreName);

				boolean inputMatches = input.contains(replacementInputOreName);

				boolean outputMatches = OreDictionary.itemMatches(originalItem, recipe.getRecipeOutput(), true);
				ItemStack output = outputMatches ? replacementOutputItem : recipe.getRecipeOutput();

				return (inputMatches || outputMatches) ? new ShapedOreRecipe(output, input.toArray()) : null;
			}
			catch (OreDictStuff.InvalidOreException e)
			{
				throw new UnableToReplaceRecipeException(recipe, recipe.recipeItems, e);
			}
		}

		// We need access to the height and width of the recipe to build the shape strings for the replacement, but there are no getters so we have to use reflection.
		private static final Field SHAPED_ORE_RECIPE_HEIGHT = ReflectionHelper.findField(ShapedOreRecipe.class, "height");
		private static final Field SHAPED_ORE_RECIPE_WIDTH = ReflectionHelper.findField(ShapedOreRecipe.class, "width");

		/**
		 * Creates a {@link ShapedOreRecipe} from a {@link ShapedOreRecipe}, replacing {@code originalItem} with {@code replacementOutputItem} or {@code replacementInputOreName} as appropriate.
		 *
		 * @param recipe
		 *            The original recipe
		 * @param originalItem
		 *            The item to replace
		 * @param replacementOutputItem
		 *            The new output item
		 * @param replacementInputOreName
		 *            The new input ore name
		 * @return The new recipe, or {@code null} if no replacement was needed
		 * @throws UnableToReplaceRecipeException
		 *             When the recipe couldn't be replaced
		 */
		private static IRecipe convertShapedRecipe(ShapedOreRecipe recipe, ItemStack originalItem, ItemStack replacementOutputItem, String replacementInputOreName) throws UnableToReplaceRecipeException
		{
			try
			{
				List<Object> input = createShapedInputs(recipe.getInput(), SHAPED_ORE_RECIPE_HEIGHT.getInt(recipe), SHAPED_ORE_RECIPE_WIDTH.getInt(recipe), originalItem, replacementInputOreName);
				boolean inputMatches = input.contains(replacementInputOreName);

				boolean outputMatches = OreDictionary.itemMatches(originalItem, recipe.getRecipeOutput(), true);
				ItemStack output = outputMatches ? replacementOutputItem : recipe.getRecipeOutput();

				return (inputMatches || outputMatches) ? new ShapedOreRecipe(output, input.toArray()) : null;
			}
			catch (OreDictStuff.InvalidOreException | IllegalAccessException e)
			{
				throw new UnableToReplaceRecipeException(recipe, recipe.getInput(), e);
			}
		}

		/**
		 * Creates a {@link ShapelessOreRecipe} from a {@link ShapelessRecipes}, replacing {@code originalItem} with {@code replacementOutputItem} or {@code replacementInputOreName} as appropriate.
		 *
		 * @param recipe
		 *            The original recipe
		 * @param originalItem
		 *            The item to replace
		 * @param replacementOutputItem
		 *            The new output item
		 * @param replacementInputOreName
		 *            The new input ore name
		 * @return The new recipe, or {@code null} if no replacement was needed
		 * @throws UnableToReplaceRecipeException
		 *             When the recipe couldn't be replaced
		 */
		@SuppressWarnings("unchecked")
		private static IRecipe convertShapelessRecipe(ShapelessRecipes recipe, ItemStack originalItem, ItemStack replacementOutputItem, String replacementInputOreName) throws UnableToReplaceRecipeException
		{
			try
			{
				List<Object> input = createShapelessInputs((List<Object>) (Object) recipe.recipeItems, originalItem, replacementInputOreName);
				boolean inputMatches = input.contains(replacementInputOreName);

				boolean outputMatches = OreDictionary.itemMatches(originalItem, recipe.getRecipeOutput(), true);
				ItemStack output = outputMatches ? replacementOutputItem : recipe.getRecipeOutput();

				return (inputMatches || outputMatches) ? new ShapelessOreRecipe(output, input.toArray()) : null;
			}
			catch (OreDictStuff.InvalidOreException e)
			{
				throw new UnableToReplaceRecipeException(recipe, (List<Object>) (Object) recipe.recipeItems, e);
			}
		}

		/**
		 * Creates a {@link ShapelessOreRecipe} from a {@link ShapelessOreRecipe}, replacing {@code originalItem} with {@code replacementOutputItem} or {@code replacementInputOreName} as appropriate.
		 *
		 * @param recipe
		 *            The original recipe
		 * @param originalItem
		 *            The item to replace
		 * @param replacementOutputItem
		 *            The new output item
		 * @param replacementInputOreName
		 *            The new input ore name
		 * @return The new recipe, or {@code null} if no replacement was needed
		 * @throws UnableToReplaceRecipeException
		 *             When the recipe couldn't be replaced
		 */
		private static IRecipe convertShapelessRecipe(ShapelessOreRecipe recipe, ItemStack originalItem, ItemStack replacementOutputItem, String replacementInputOreName) throws UnableToReplaceRecipeException
		{
			try
			{
				List<Object> input = createShapelessInputs(recipe.getInput(), originalItem, replacementInputOreName);
				boolean inputMatches = input.contains(replacementInputOreName);

				boolean outputMatches = OreDictionary.itemMatches(originalItem, recipe.getRecipeOutput(), true);
				ItemStack output = outputMatches ? replacementOutputItem : recipe.getRecipeOutput();

				return (inputMatches || outputMatches) ? new ShapelessOreRecipe(output, input.toArray()) : null;
			}
			catch (OreDictStuff.InvalidOreException e)
			{
				throw new UnableToReplaceRecipeException(recipe, recipe.getInput(), e);
			}
		}

		/**
		 * Thrown when a recipe can't be replaced for some reason. Creates a human-readable message from the original recipe's class, output and inputs.
		 */
		public static class UnableToReplaceRecipeException extends Exception
		{
			public UnableToReplaceRecipeException(IRecipe recipe, List<Object> input, Throwable cause)
			{
				super(getMessageFromRecipe(recipe, input), cause);
			}

			public UnableToReplaceRecipeException(IRecipe recipe, Object[] input, Throwable cause)
			{
				this(recipe, Lists.newArrayList(input), cause);
			}

			private static String getMessageFromRecipe(IRecipe recipe, List<Object> input)
			{
				return "Class: " + recipe.getClass().getSimpleName() + " - Output: " + recipe.getRecipeOutput() + " - Input: " + input;
			}
		}
	}

	public static class OreDictStuff
	{

		/**
		 * Gets an array of ore names for the specified ore.
		 *
		 * @param itemStack
		 *            The ore
		 * @return An array of ore names
		 */
		public static String[] getOreNames(ItemStack itemStack)
		{
			int[] ids = OreDictionary.getOreIDs(itemStack);
			String[] names = new String[ids.length];

			for (int i = 0; i < ids.length; i++)
			{
				names[i] = OreDictionary.getOreName(ids[i]);
			}

			return names;
		}

		/**
		 * Gets the ore name corresponding to the specified list of ores.
		 *
		 * @param ores
		 *            The list of ores
		 * @return The corresponding ore name
		 * @throws InvalidOreException
		 *             When no matching ore name is found
		 */
		public static String getOreNameForOreList(List<ItemStack> ores) throws InvalidOreException
		{
			// OreDictionary.getOres returns an empty list if no ores have been registered for the specified name
			if (ores.isEmpty())
			{
				throw new InvalidOreException("Ore list is empty");
			}
			else
			{
				for (String possibleOreName : getOreNames(ores.get(0)))
				{
					if (ores.equals(OreDictionary.getOres(possibleOreName)))
					{
						return possibleOreName;
					}
				}
			}

			throw new InvalidOreException("Unable to find name for ores " + ores.toString());
		}

		public static class InvalidOreException extends Exception
		{
			public InvalidOreException()
			{
			}

			public InvalidOreException(String message)
			{
				super(message);
			}

			public InvalidOreException(String message, Throwable cause)
			{
				super(message, cause);
			}
		}
	}

	public static class RecipeRemover
	{
		public static void removeShapedRecipes(List<ItemStack> removelist)
		{
			removelist.forEach(RecipeRemover::removeShapedRecipe);
		}

		public static void removeAnyRecipe(ItemStack resultItem)
		{
			List<IRecipe> recipes = CraftingManager.getInstance().getRecipeList();
			for (int i = 0; i < recipes.size(); i++)
			{
				IRecipe tmpRecipe = recipes.get(i);
				ItemStack recipeResult = tmpRecipe.getRecipeOutput();
				if (ItemStack.areItemStacksEqual(resultItem, recipeResult))
				{
					recipes.remove(i--);
				}
			}
		}

		public static void removeAnyRecipe(Item resultItem)
		{
			List<IRecipe> recipes = CraftingManager.getInstance().getRecipeList();
			for (int i = 0; i < recipes.size(); i++)
			{
				IRecipe tmpRecipe = recipes.get(i);
				ItemStack recipeResult = tmpRecipe.getRecipeOutput();
				if (recipeResult != null && recipeResult.getItem() == resultItem)
				{
					recipes.remove(i--);
				}
			}
		}

		public static void removeShapedRecipe(ItemStack resultItem)
		{
			List<IRecipe> recipes = CraftingManager.getInstance().getRecipeList();
			for (int i = 0; i < recipes.size(); i++)
			{
				IRecipe tmpRecipe = recipes.get(i);
				if (tmpRecipe instanceof ShapedRecipes)
				{
					ShapedRecipes recipe = (ShapedRecipes) tmpRecipe;
					ItemStack recipeResult = recipe.getRecipeOutput();

					if (ItemStack.areItemStacksEqual(resultItem, recipeResult))
					{
						recipes.remove(i--);
					}
				}
			}
		}

		public static void removeShapelessRecipe(ItemStack resultItem)
		{
			List<IRecipe> recipes = CraftingManager.getInstance().getRecipeList();
			for (int i = 0; i < recipes.size(); i++)
			{
				IRecipe tmpRecipe = recipes.get(i);
				if (tmpRecipe instanceof ShapelessRecipes)
				{
					ShapelessRecipes recipe = (ShapelessRecipes) tmpRecipe;
					ItemStack recipeResult = recipe.getRecipeOutput();

					if (ItemStack.areItemStacksEqual(resultItem, recipeResult))
					{
						recipes.remove(i--);
					}
				}
			}
		}

		public static void removeFurnaceRecipe(ItemStack resultItem)
		{
			Map<ItemStack, ItemStack> recipes = FurnaceRecipes.instance().getSmeltingList();
			for (Iterator<Map.Entry<ItemStack, ItemStack>> entries = recipes.entrySet().iterator(); entries.hasNext();)
			{
				Map.Entry<ItemStack, ItemStack> entry = entries.next();
				ItemStack result = entry.getValue();
				if (ItemStack.areItemStacksEqual(result, resultItem))
				{ // If the output matches the specified ItemStack,
					entries.remove(); // Remove the recipe
				}
			}
		}

		public static void removeFurnaceRecipe(Item i, int metadata)
		{
			removeFurnaceRecipe(new ItemStack(i, 1, metadata));
		}

		public static void removeFurnaceRecipe(Item i)
		{
			removeFurnaceRecipe(new ItemStack(i, 1, 32767));
		}
	}
}
