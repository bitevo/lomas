package bitevo.Zetal.LoMaS.Shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;

public class LoMaS_Faction implements Serializable
{
	private static HashMap<UUID, LoMaS_Faction> factionList = new HashMap<UUID, LoMaS_Faction>();
	private static final long serialVersionUID = 1L;

	private UUID id;
	private UUID owner;
	private Random rand;
	private String name = "";
	private ArrayList<UUID> leaders = new ArrayList<UUID>();
	private ArrayList<UUID> members = new ArrayList<UUID>();
	private ArrayList<UUID> alliedFactions = new ArrayList<UUID>();
	private ArrayList<UUID> enemyFactions = new ArrayList<UUID>();
	private ArrayList<BlockPos> obelisks = new ArrayList<BlockPos>();
	private ArrayList<UUID> entities = new ArrayList<UUID>();

	@Deprecated
	public LoMaS_Faction(String name, EntityPlayer owner)
	{
		this.setName(name);
		this.rand = new Random();
		this.setId(MathHelper.getRandomUuid(rand));
		UUID creator = owner.getPersistentID();
		this.setOwner(creator);
		this.addLeader(creator);
		this.addMember(creator);
	}

	public LoMaS_Faction(String name, EntityPlayer owner, BlockPos firstObelisk)
	{
		this.setName(name);
		this.rand = new Random();
		this.setId(MathHelper.getRandomUuid(rand));
		UUID creator = owner.getPersistentID();
		this.setOwner(creator);
		this.addLeader(creator);
		this.addMember(creator);
		this.addObelisk(firstObelisk);
	}

	public LoMaS_Faction(String name, UUID creator, BlockPos firstObelisk)
	{
		this.setName(name);
		this.rand = new Random();
		this.setId(MathHelper.getRandomUuid(rand));
		this.setOwner(creator);
		this.addLeader(creator);
		this.addMember(creator);
		this.addObelisk(firstObelisk);
	}

	@Deprecated
	public LoMaS_Faction(String name, UUID creator)
	{
		this.setName(name);
		this.rand = new Random();
		this.setId(MathHelper.getRandomUuid(rand));
		this.setOwner(creator);
		this.addLeader(creator);
		this.addMember(creator);
	}

	public static LoMaS_Faction getLoMaSFaction(UUID pid)
	{
		LoMaS_Faction ret = null;
		if (pid != null)
		{
			//System.out.println(factionList);
			for (int i = 0; i < factionList.size(); i++)
			{
				if (factionList.get(i) != null && factionList.get(i).getId() != null && pid.equals(factionList.get(i).getId()))
				{
					ret = factionList.get(i);
				}
			}
		}
		return ret;
	}
	
	/**
	 * This should only be used after verification is done server-side.
	 */
	public static void removeFaction(UUID id)
	{
		if(LoMaS_Faction.factionList.get(id) != null)
		{
			LoMaS_Faction.factionList.remove(id);
		}
	}
	
	/**
	 * This can be used whenever, since the verification logic is built-in.
	 */
	public static void removeFaction(UUID requester, UUID id)
	{
		if(LoMaS_Faction.factionList.get(id) != null)
		{
			LoMaS_Faction faction = LoMaS_Faction.getLoMaSFaction(id);
			if(faction.owner.equals(requester))
			{
				if(faction.members.size() <= 1)
				{
					LoMaS_Faction.factionList.remove(id);
				}
			}
		}
	}
	
	public static boolean canLeaveFaction(UUID requester, UUID id)
	{
		//System.out.println(requester + " " + id);
		if(LoMaS_Faction.getLoMaSFaction(id) != null)
		{
			//System.out.println("hi1");
			LoMaS_Faction faction = LoMaS_Faction.getLoMaSFaction(id);
			if(faction.owner.equals(requester))
			{
				//System.out.println(faction + " " + faction.members);
				if(faction.members.size() <= 1)
				{
					return true;
				}
				return false;
			}
			return true;
		}
		return false;
	}

	public static void addFaction(LoMaS_Faction faction)
	{
		LoMaS_Faction.factionList.put(faction.getId(), faction);
	}

	@Override
	public LoMaS_Faction clone()
	{
		LoMaS_Faction clone = new LoMaS_Faction(this.name, this.owner);
		clone.setId(this.getId());
		clone.setAlliedFactions(this.getAlliedFactions());
		clone.setEnemyFactions(this.getEnemyFactions());
		clone.setLeaders(this.getLeaders());
		clone.setMembers(this.getMembers());
		clone.setObelisks(this.getObelisks());
		clone.setEntities(this.getEntities());
		return clone;
	}

	public UUID getId()
	{
		return id;
	}

	public void setId(UUID id)
	{
		this.id = id;
	}

	public UUID getOwner()
	{
		return owner;
	}

	public void setOwner(UUID owner)
	{
		this.owner = owner;
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(owner);
		if (lplayer != null)
		{
			lplayer.faction = this.getId();
		}

		if (!this.members.contains(owner))
		{
			this.members.add(owner);
		}
	}

	public ArrayList<UUID> getLeaders()
	{
		return leaders;
	}

	public void setLeaders(ArrayList<UUID> leaders)
	{
		this.leaders = leaders;
	}

	public void addLeader(UUID leader)
	{
		if (!this.leaders.contains(leader))
		{
			this.leaders.add(leader);
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(leader);
			if (lplayer != null)
			{
				lplayer.faction = this.getId();
			}
		}

		if (!this.members.contains(leader))
		{
			this.members.add(leader);
		}
	}

	public void removeLeader(UUID leader)
	{
		if (this.leaders.contains(leader))
		{
			this.leaders.remove(leader);
		}
	}

	public void addLeader(EntityPlayer player)
	{
		UUID leader = player.getPersistentID();
		if (!this.leaders.contains(leader))
		{
			this.leaders.add(leader);
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(leader);
			if (lplayer != null)
			{
				lplayer.faction = this.getId();
			}
		}

		if (!this.members.contains(leader))
		{
			this.members.add(leader);
		}
	}

	public void removeLeader(EntityPlayer player)
	{
		UUID leader = player.getPersistentID();
		if (this.leaders.contains(leader))
		{
			this.leaders.remove(leader);
		}
	}

	public ArrayList<UUID> getMembers()
	{
		return members;
	}

	public void setMembers(ArrayList<UUID> members)
	{
		this.members = members;
	}

	public void addMember(UUID member)
	{
		if (!this.members.contains(member))
		{
			this.members.add(member);
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(member);
			if (lplayer != null)
			{
				lplayer.faction = this.getId();
			}
		}
	}

	public void removeMember(UUID member)
	{
		if (this.members.contains(member))
		{
			this.members.remove(member);
		}
		if(this.leaders.contains(member))
		{
			this.leaders.remove(member);
		}
		if(this.members.size() <= 0)
		{
			this.removeFaction(this.getId());
		}
	}

	public void addMember(EntityPlayer player)
	{
		UUID member = player.getPersistentID();
		if (!this.members.contains(member))
		{
			this.members.add(member);
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(member);
			if (lplayer != null)
			{
				lplayer.faction = this.getId();
			}
		}
	}

	public void removeMember(EntityPlayer player)
	{
		UUID member = player.getPersistentID();
		if (this.members.contains(member))
		{
			this.members.remove(member);
		}
		if(this.leaders.contains(member))
		{
			this.leaders.remove(member);
		}
	}

	public ArrayList<UUID> getAlliedFactions()
	{
		return alliedFactions;
	}

	public void setAlliedFactions(ArrayList<UUID> alliedFactions)
	{
		this.alliedFactions = alliedFactions;
	}

	public void addAlliedFaction(UUID allyFaction)
	{
		if (!this.alliedFactions.contains(allyFaction))
		{
			this.alliedFactions.add(allyFaction);
		}

		if (this.enemyFactions.contains(allyFaction))
		{
			this.enemyFactions.remove(allyFaction);
		}
	}

	public void removeAlliedFaction(UUID allyFaction)
	{
		if (this.alliedFactions.contains(allyFaction))
		{
			this.alliedFactions.remove(allyFaction);
		}
	}

	public ArrayList<UUID> getEnemyFactions()
	{
		return enemyFactions;
	}

	public void setEnemyFactions(ArrayList<UUID> enemyFactions)
	{
		this.enemyFactions = enemyFactions;
	}

	public void addEnemyFaction(UUID enemyFaction)
	{
		if (!this.enemyFactions.contains(enemyFaction))
		{
			this.enemyFactions.add(enemyFaction);
		}

		if (this.alliedFactions.contains(enemyFaction))
		{
			this.alliedFactions.remove(enemyFaction);
		}
	}

	public void removeEnemyFaction(UUID enemyFaction)
	{
		if (this.enemyFactions.contains(enemyFaction))
		{
			this.enemyFactions.remove(enemyFaction);
		}
	}

	public ArrayList<BlockPos> getObelisks()
	{
		return obelisks;
	}

	public void setObelisks(ArrayList<BlockPos> obelisks)
	{
		this.obelisks = obelisks;
	}

	public void addObelisk(BlockPos pos)
	{
		if (!this.obelisks.contains(pos))
		{
			this.obelisks.add(pos);
		}
	}

	public void removeObelisk(BlockPos pos)
	{
		if (this.obelisks.contains(pos))
		{
			this.obelisks.remove(pos);
		}
	}

	public ArrayList<UUID> getEntities()
	{
		return entities;
	}

	public void setEntities(ArrayList<UUID> entities)
	{
		this.entities = entities;
	}

	public void addEntity(UUID ent)
	{
		if (!this.entities.contains(ent))
		{
			this.entities.add(ent);
		}
	}

	public void removeEntity(UUID ent)
	{
		if (this.entities.contains(ent))
		{
			this.entities.remove(ent);
		}
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
	
	@Override
	public String toString()
	{
		return this.name + " " + this.members + " " + this.id;
	}

	public static HashMap<UUID, LoMaS_Faction> getFactionList()
	{
		return factionList;
	}

	public static void setFactionList(HashMap<UUID, LoMaS_Faction> factionList)
	{
		LoMaS_Faction.factionList = factionList;
	}
}
