package bitevo.Zetal.LoMaS.Specializations.Message;

import java.util.UUID;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class ArrowMessage implements IMessage
{
	public UUID playerUUID;
	public int arrowType = 0;

	public ArrowMessage()
	{
	}

	public ArrowMessage(int arrowType, EntityPlayer entity)
	{
		this.arrowType = arrowType;
		this.playerUUID = entity.getUniqueID();
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.arrowType = buf.readInt();
		// this.playerUUID = new UUID(buf.readLong(), buf.readLong());
		this.playerUUID = UUID.fromString(ByteBufUtils.readUTF8String(buf));
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeInt(this.arrowType);
		// buf.writeLong(this.playerUUID.getMostSignificantBits());
		// buf.writeLong(this.playerUUID.getLeastSignificantBits());
		ByteBufUtils.writeUTF8String(buf, this.playerUUID.toString());
	}
}
