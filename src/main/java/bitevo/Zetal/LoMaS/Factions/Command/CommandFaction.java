package bitevo.Zetal.LoMaS.Factions.Command;

import bitevo.Zetal.LoMaS.Factions.Entity.EntityPigmanWarrior;
import bitevo.Zetal.LoMaS.Factions.Entity.EntitySiegeGolem;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;

public class CommandFaction extends CommandBase
{
	@Override
	public String getCommandName()
	{
		return "faction";
	}

	@Override
	public int getRequiredPermissionLevel()
	{
		return 0;
	}

	@Override
	public String getCommandUsage(ICommandSender icommandsender)
	{
		return "/faction help";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		EntityPlayer player = null;
		try
		{
			player = this.getCommandSenderAsPlayer(sender);
		}
		catch (PlayerNotFoundException e)
		{
			e.printStackTrace();
		}
		World world = player.worldObj;
		if (player != null && LoMaS_Utils.isPlayerAdmin(player))
		{
			if (args.length > 0)
			{
				if (args[0].matches("help"))
				{
					LoMaS_Utils.addChatMessage(player, "Commands for Factions include: ");
					LoMaS_Utils.addChatMessage(player, ("/faction spawn warrior"));
					LoMaS_Utils.addChatMessage(player, ("/faction spawn siegegolem"));
					return;
				}
				else if ((args[0].matches("spawn") && (args.length == 2)))
				{
					if (args[1].equalsIgnoreCase("warrior"))
					{
						LoMaS_Utils.addChatMessage(player, ("Spawned entity at your location."));
						EntityPigmanWarrior warrior = new EntityPigmanWarrior(player.worldObj, LoMaS_Player.getLoMaSPlayer(player).faction);
						warrior.setPosition(player.posX, player.posY, player.posZ);
						player.worldObj.spawnEntityInWorld(warrior);
						return;
					}
					else if (args[1].equalsIgnoreCase("siegegolem"))
					{
						LoMaS_Utils.addChatMessage(player, ("Spawned entity at your location."));
						EntitySiegeGolem golem = new EntitySiegeGolem(player.worldObj, LoMaS_Player.getLoMaSPlayer(player).faction);
						golem.setPosition(player.posX, player.posY, player.posZ);
						player.worldObj.spawnEntityInWorld(golem);
						return;
					}
				}
				LoMaS_Utils.addChatMessage(player, ("Malformed command. Try '/faction help'"));
			}
		}
	}
}
