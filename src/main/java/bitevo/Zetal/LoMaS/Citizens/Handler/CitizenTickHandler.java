package bitevo.Zetal.LoMaS.Citizens.Handler;

import java.util.ArrayList;
import java.util.UUID;

import bitevo.Zetal.LoMaS.Citizens.Entity.EntityPigmanCitizen;
import bitevo.Zetal.LoMaS.Citizens.TileEntity.TileEntityCitizenChest;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.WorldTickEvent;
import net.minecraftforge.fml.relauncher.Side;

public class CitizenTickHandler
{
	public int ticker = 0;
	public static final int tickFinal = 60;

	@SubscribeEvent
	public void serverTickEnd(WorldTickEvent event)
	{
		if (event.side == Side.SERVER)
		{
			if (event.phase == TickEvent.Phase.END)
			{
				if (ticker % 1440 == 0)
				{
					WorldServer world = (WorldServer) event.world;

					for (LoMaS_Player lplayer : LoMaS_Player.playerList.values())
					{
						for (UUID u : lplayer.getCitizens())
						{
							Entity ent = world.getMinecraftServer().getEntityFromUuid(u);
							if (ent instanceof EntityPigmanCitizen)
							{
								EntityPigmanCitizen cit = (EntityPigmanCitizen) ent;
								if (!cit.getJob().equalsIgnoreCase("None") && cit.getHomeChest() != null && world.getTileEntity(cit.getHomeChest()) instanceof TileEntityCitizenChest)
								{
									cit.incomeTicks = cit.incomeTicks + 1;
									//System.out.println("income tick");
									if (cit.incomeTicks >= tickFinal && cit.worldObj.getTileEntity(cit.getHomeChest()) instanceof TileEntityCitizenChest)
									{
										cit.incomeTicks = 0;
										TileEntityCitizenChest home = (TileEntityCitizenChest) cit.worldObj.getTileEntity(cit.getHomeChest());
										ItemStack product = cit.getStackForJob(cit.getJob());
										product.stackSize = (int) (product.stackSize * (1.0f + lplayer.getSkills().getPercentMagnitudeFromName("Citizen items")));
										if (product != null)
										{
											home.addItemStackToInventory(product);
										}
										int income = cit.getIncomeForJob(cit.getJob());
										income = (int) (income * (1.0f + lplayer.getSkills().getPercentMagnitudeFromName("Citizen income")));
										System.out.println("Citizen Giving Income: " + home + " to player " + lplayer.name);
										lplayer.setBankCC(lplayer.getBankCC() + income);
										EntityPlayer player = world.getPlayerEntityByUUID(lplayer.uuid);
										if (LoMaS_Utils.ECO && income > 0 && player != null)
										{
											bitevo.Zetal.LoMaS.Economy.EconomyMod.snw.sendTo(new bitevo.Zetal.LoMaS.Economy.Message.BankMessage(lplayer.getBankCC()), (EntityPlayerMP) player);
											LoMaS_Utils.addChatMessage(player, "You received income from a citizen! They paid you " + income + "CC in taxes!");
										}
									}
								}
								else
								{
									//System.out.println(cit.getJob() + " " + cit.getHomeChest() + " " + world.getTileEntity(cit.getHomeChest()));
								}
							}
						}
					}
					ticker = 0;
				}
				ticker++;
			}
		}
	}
}
