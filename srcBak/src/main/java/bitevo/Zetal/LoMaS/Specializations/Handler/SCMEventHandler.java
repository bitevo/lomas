package bitevo.Zetal.LoMaS.Specializations.Handler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Animal;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.BigFMod.BigFMod;
import bitevo.Zetal.LoMaS.Economy.Entities.EntityItemCoin;
import bitevo.Zetal.LoMaS.Economy.Inventory.InventoryBank;
import bitevo.Zetal.LoMaS.Economy.Item.ItemCoin;
import bitevo.Zetal.LoMaS.Specializations.FoodInitializer;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Capabilities.CapabilitiesProvider;
import bitevo.Zetal.LoMaS.Specializations.Entities.EntityCustomArrow;
import bitevo.Zetal.LoMaS.Specializations.Entities.EntityCustomXPOrb;
import bitevo.Zetal.LoMaS.Specializations.Inventory.InventoryPlayer;
import bitevo.Zetal.LoMaS.Specializations.Inventory.SpecContainerRepair;
import bitevo.Zetal.LoMaS.Specializations.Inventory.SpecPlayerContainer;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemFood;
import bitevo.Zetal.LoMaS.Specializations.Message.AnimalMessage;
import bitevo.Zetal.LoMaS.Specializations.Message.ArrowMessage;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntityPlant;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySpecBrewingStand;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySpecFurnace;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAnvil;
import net.minecraft.block.BlockFlower;
import net.minecraft.block.BlockGrass;
import net.minecraft.block.BlockLog;
import net.minecraft.block.BlockObsidian;
import net.minecraft.block.BlockOre;
import net.minecraft.block.BlockRedstoneOre;
import net.minecraft.block.BlockTallGrass;
import net.minecraft.block.BlockWorkbench;
import net.minecraft.block.IGrowable;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIMate;
import net.minecraft.entity.ai.EntityAITasks.EntityAITaskEntry;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.entity.passive.EntityMooshroom;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemSaddle;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.stats.StatList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityBrewingStand;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraftforge.common.IShearable;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.ArrowNockEvent;
import net.minecraftforge.event.entity.player.FillBucketEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.EntityInteract;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.ExplosionEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class SCMEventHandler
{
	private Random rand = new Random();

	@SubscribeEvent(priority = EventPriority.HIGH)
	public void breakSpeedEvent(PlayerEvent.BreakSpeed event)
	{
		EntityPlayer harvester = event.getEntityPlayer();
		float f = event.getOriginalSpeed();
		Block par1Block = event.getState().getBlock();
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(harvester);
		Material blockMat = event.getState().getMaterial();

		if ((par1Block instanceof BlockLog || blockMat == Material.WOOD) && lplayer.specClass.equalsIgnoreCase("woodsman"))
		{
			f *= 1.15F;
			if (lplayer.classlevel >= 4)
			{
				f *= 1.10F;
				if (lplayer.classlevel >= 6)
				{
					f *= 1.05F;
					if (lplayer.classlevel >= 7)
					{
						f *= 1.05F;
						if (lplayer.classlevel >= 8)
						{
							f *= 1.10F;
							if (lplayer.classlevel >= 9)
							{
								f *= 1.10F;
							}
						}
					}
				}
			}
		}
		else if (lplayer.specClass.equalsIgnoreCase("miner"))
		{
			if (par1Block instanceof BlockOre || blockMat == Material.ROCK || blockMat == Material.ANVIL || blockMat == Material.IRON || blockMat == Material.GROUND || blockMat == Material.CLAY || blockMat == Material.GRASS || blockMat == Material.ICE || blockMat == Material.SAND)
			{
				f *= 1.05F;
				if (lplayer.classlevel >= 3)
				{
					f *= 1.10F;
					if (lplayer.classlevel >= 4)
					{
						f *= 1.05F;
						if (lplayer.classlevel >= 6)
						{
							f *= 1.04F;
							if (lplayer.classlevel >= 7)
							{
								f *= 1.05F;
								if (lplayer.classlevel >= 9)
								{
									f *= 1.06F;
									if (lplayer.classlevel >= 10 && lplayer.isActive)
									{
										f = 9999999.99F;
									}
								}
							}
						}
					}
				}
			}
		}
		else
		{
			f *= 0.8F;
		}

		event.setNewSpeed(f);
	}

	@SubscribeEvent
	public void onHarvest(BlockEvent.HarvestDropsEvent event)
	{
		if (event.getHarvester() != null && !event.getHarvester().capabilities.isCreativeMode)
		{
			EntityPlayer player = event.getHarvester();
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
			Block block = event.getState().getBlock();
			World world = event.getWorld();
			BlockPos pos = event.getPos();
			IBlockState state = event.getState();
			int fortune = event.getFortuneLevel();
			float progressEarned = SpecializationsMod.getProgress(player, lplayer, block, event);
			if (progressEarned > 0.0f)
			{
				lplayer.addSpecProgress(progressEarned);
				player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
			}

			if (block instanceof BlockOre || block instanceof BlockRedstoneOre || block instanceof BlockObsidian)
			{
				Random rand = new Random();
				int randy = rand.nextInt(100);
				if (lplayer.specClass.equalsIgnoreCase("miner"))
				{
					// Chance for a completely extra drop!!!
					int chance = 0;
					if (lplayer.classlevel >= 2)
					{
						chance = chance + 3;
						if (lplayer.classlevel >= 5)
						{
							chance = chance + 3;
							if (lplayer.classlevel >= 8)
							{
								chance = chance + 4;
							}
						}
						if (player.getRNG().nextInt(100) < chance)
						{
							player.addChatMessage(new TextComponentTranslation("\u00A72Your honed abilities improved the yield of that ore!"));
							event.getDrops().addAll(block.getDrops(world, pos, state, fortune));
						}
					}
				}
				else if (randy < 50)
				{
					int amt = world.rand.nextInt(2) + 1;
					ItemStack itemstack = null;
					if (!(block instanceof BlockObsidian) && !(block.equals(Blocks.QUARTZ_ORE)))
					{
						itemstack = new ItemStack(Blocks.COBBLESTONE, amt);
					}
					else if (block.equals(Blocks.QUARTZ_ORE))
					{
						itemstack = new ItemStack(Blocks.NETHERRACK, amt);
					}
					player.addChatMessage(new TextComponentTranslation("\u00a7cYour unskilled hands have destroyed the valuable ore."));
					if (itemstack != null)
					{
						event.getDrops().clear();
						event.getDrops().add(itemstack);
					}
				}
			}
			else if (block instanceof BlockLog)
			{
				Random rand = new Random();
				int randy = rand.nextInt(100);
				if (lplayer.specClass.equalsIgnoreCase("woodsman"))
				{
					// Chance for a completely extra drop!!!
					int chance = 0;
					if (lplayer.classlevel >= 3)
					{
						chance = chance + 3;
						if (lplayer.classlevel >= 5)
						{
							chance = chance + 3;
							if (lplayer.classlevel >= 7)
							{
								chance = chance + 4;
							}
						}
						if (player.getRNG().nextInt(100) < chance)
						{
							player.addChatMessage(new TextComponentTranslation("\u00A72Your honed abilities improved the yield of that log!"));
							event.getDrops().addAll(block.getDrops(world, pos, state, fortune));
						}
					}
				}
				else if (randy < 50)
				{
					int amt = world.rand.nextInt(5) + 4;
					ItemStack itemstack = new ItemStack(BigFMod.STICK, amt);// , block.damageDropped(state));
					player.addChatMessage(new TextComponentTranslation("\u00a7cYour unskilled hands have destroyed the log."));
					event.getDrops().clear();
					event.getDrops().add(itemstack);
				}
			}
		}
	}

	@SubscribeEvent
	public void onEntityMove(LivingUpdateEvent event)
	{
		if (event.getEntity() != null && event.getEntity() instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) event.getEntity();
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);

			if (player != null && lplayer != null)
			{
				if (player.isWet() && player.ticksExisted % 120 == 0 && player.getRidingEntity() == null)
				{
					player.addExhaustion(0.1f);
				}

				if (lplayer.isActive && lplayer.specClass.equalsIgnoreCase("stealth"))
				{
					if (player.worldObj.isRemote)
					{
						EntityPlayerSP p = (EntityPlayerSP) player;
						p.onUpdateWalkingPlayer();
					}
					int distance = 5;
					float f1 = MathHelper.cos(-player.rotationYaw * 0.017453292F - (float) Math.PI);
					float f2 = MathHelper.sin(-player.rotationYaw * 0.017453292F - (float) Math.PI);
					float f3 = -MathHelper.cos(-player.rotationPitch * 0.017453292F);
					float f4 = MathHelper.sin(-player.rotationPitch * 0.017453292F);
					double i = player.posX;
					double j = player.posY;
					double k = player.posZ;
					player.moveEntity(distance * f2 * f3, distance * f4, distance * f1 * f3);
					short short1 = 128;

					for (int l = 0; l < short1; ++l)
					{
						Random rand = event.getEntity().worldObj.rand;
						double d6 = (double) l / ((double) short1 - 1.0D);
						float s = (rand.nextFloat() - 0.5F) * 0.2F;
						float s1 = (rand.nextFloat() - 0.5F) * 0.2F;
						float s2 = (rand.nextFloat() - 0.5F) * 0.2F;
						double d7 = i + (player.posX - i) * d6 + (rand.nextDouble() - 0.5D) * (double) player.width * 2.0D;
						double d8 = j + (player.posY - j) * d6 + rand.nextDouble() - 1.5;// * (double)player.height;
						double d9 = k + (player.posZ - k) * d6 + (rand.nextDouble() - 0.5D) * (double) player.width * 2.0D;
						player.worldObj.spawnParticle(EnumParticleTypes.PORTAL, d7, d8, d9, (double) s, (double) s1, (double) s2);
					}

					player.worldObj.playSound((EntityPlayer) null, player.prevPosX, player.prevPosY, player.prevPosZ, SoundEvents.ENTITY_ENDERMEN_TELEPORT, player.getSoundCategory(), 1.0F, 1.0F);
					player.playSound(SoundEvents.ENTITY_ENDERMEN_TELEPORT, 1.0F, 1.0F);
					// System.out.println("Stealth isActive = " + player.isActive);
					lplayer.isActive = false;
					lplayer.counter = 21;
				}
				lplayer.secCounter = lplayer.counter / 20;
				if ((lplayer.secCounter <= LoMaS_Player.abilCooldown && lplayer.secCounter != 0) || (lplayer.secCounter == 0 && lplayer.isActive == true))
				{
					lplayer.counter++;
					// System.out.println(player.counter);
					if (lplayer.counter % 20 == 0)
					{
						// System.out.println((player.secCounter));
						if (lplayer.specClass.equalsIgnoreCase("warrior") && lplayer.secCounter > 5)
						{
							lplayer.isActive = false;
						}
						if (lplayer.specClass.equalsIgnoreCase("miner") && lplayer.secCounter > 5)
						{
							lplayer.isActive = false;
						}
						if (lplayer.secCounter >= LoMaS_Player.abilCooldown)
						{
							lplayer.counter = 0;
						}
					}
				}

				if (lplayer.specClass.equalsIgnoreCase("warrior") && lplayer.isActive && player.getHealth() < player.getMaxHealth() && player.ticksExisted % 5 * 12 == 0)
				{
					player.heal(1);
				}

				if (lplayer.progress > lplayer.getReqProgress())
				{
					lplayer.addSpecProgress(0);
				}

				if (!player.worldObj.isRemote && lplayer.notYet && ((EntityPlayerMP) player).connection != null)
				{
					((EntityPlayerMP) player).sendContainerToPlayer(player.inventoryContainer);
					// I don't think this 'openGui' call is required, but I'm keeping it because things are working right now and I don't want to touch it. :(
					player.openGui(SpecializationsMod.class, 2, player.worldObj, 0, 0, 0);
					player.inventoryContainer.detectAndSendChanges();
					lplayer.notYet = false;
				}

				if (lplayer.specClass.equalsIgnoreCase("woodsman") && lplayer.classlevel >= 10 && SpecializationsMod.isTouchingLog(player))
				{
					float f5 = 0.15F;

					if (player.motionY < -0.15D)
					{
						player.motionY = -0.15D;
					}

					boolean flag = player.isSneaking();

					if (flag && player.motionY < 0.0D)
					{
						player.motionY = 0.0D;
					}

					if (player.isCollidedHorizontally)
					{
						player.motionY = 0.1D;
					}

					player.motionX = MathHelper.clamp_double(player.motionX, (double) (-f5), (double) f5);
					player.motionZ = MathHelper.clamp_double(player.motionZ, (double) (-f5), (double) f5);
					player.fallDistance = 0.0F;

					player.moveEntity(player.motionX, player.motionY, player.motionZ);
				}

				float foodExhaustionLevel = this.getExhaustion(player);
				if (lplayer.prevfoodExhaustionLevel != -1.0f && foodExhaustionLevel != lplayer.prevfoodExhaustionLevel)
				{
					// If diff > 0, exhaustion increased. If diff < 0, it decreased.
					float diff = foodExhaustionLevel - lplayer.prevfoodExhaustionLevel;
					if (diff > 0)
					{
						if (lplayer.specClass.equalsIgnoreCase("miner"))
						{
							float reduction = 5.25f;
							if (lplayer.classlevel >= 5)
							{
								reduction = 4.0f;
							}
							diff = diff * reduction;
						}
						else
						{
							diff = diff * 6.0f;
						}
						player.addExhaustion(diff);
						lplayer.prevfoodExhaustionLevel = this.getExhaustion(player);
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onFallDamage(LivingFallEvent event)
	{
		if (event.getEntityLiving() instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) event.getEntityLiving();
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
			if (lplayer != null)
			{
				if (lplayer.specClass.equalsIgnoreCase("woodsman") && lplayer.classlevel >= 10 && SpecializationsMod.isTouchingLog(player))
				{
					event.setDamageMultiplier(0.0f);
				}
			}
		}
	}

	@SubscribeEvent
	public void onPlace(BlockEvent.PlaceEvent event)
	{
		EntityPlayer player = event.getPlayer();
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
		player.addExhaustion(0.1f);
		ItemStack stack = event.getItemInHand();
		World world = event.getWorld();
		if (event.getPlacedBlock().getBlock().equals(Blocks.FARMLAND))
		{
			world.setBlockState(event.getPos(), FoodInitializer.FARMLAND.getDefaultState());
		}
	}

	@SubscribeEvent(priority = EventPriority.HIGH)
	public void rightClick(PlayerInteractEvent.RightClickBlock event)
	{
		BlockPos pos = event.getPos();
		World world = event.getWorld();
		Block b = world.getBlockState(pos).getBlock();
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(event.getEntityPlayer());
		// System.out.println(world.getBlockState(pos) + " " + world.getTileEntity(pos));
		if (event.getWorld().getTileEntity(event.getPos()) != null)
		{
			TileEntity tileentity = world.getTileEntity(pos);
			if (tileentity instanceof TileEntityBrewingStand && !(tileentity instanceof TileEntitySpecBrewingStand))
			{
				world.setBlockState(pos, SpecializationsMod.BREWING_STAND.getDefaultState());
				System.out.println("Converted old: " + world.getTileEntity(pos));
			}

			if (tileentity instanceof TileEntityFurnace && !(tileentity instanceof TileEntitySpecFurnace))
			{
				world.setBlockState(pos, SpecializationsMod.FURNACE.getDefaultState());
				System.out.println("Converted old: " + world.getTileEntity(pos));
			}

			if (tileentity instanceof TileEntityPlant)
			{
				TileEntityPlant tep = (TileEntityPlant) tileentity;
				if(lplayer.specClass.equalsIgnoreCase("Farmer") && lplayer.classlevel >= 2)
				{
					if(tep.getQuality() < 75)
					{
						lplayer.player.addChatMessage(new TextComponentTranslation("This plant is looking pretty poor... " + tep.getQuality()));
					}
					else if(tep.getQuality() <= 100)
					{
						lplayer.player.addChatMessage(new TextComponentTranslation("This plant is starting to look pretty decent. " + tep.getQuality()));
					}
					else if(tep.getQuality() <= 150)
					{
						lplayer.player.addChatMessage(new TextComponentTranslation("This plant looks absolutely superb! " + tep.getQuality()));
					}
				}
			}
		}

		if (world.getBlockState(pos).getBlock() instanceof BlockAnvil)
		{
			if (!world.isRemote)
			{
				if (lplayer != null && lplayer.specClass.equalsIgnoreCase("blacksmith"))
				{
					((EntityPlayerMP) event.getEntityPlayer()).displayGui(new SpecContainerRepair.Anvil(world, pos));
				}
				else
				{
					event.getEntityPlayer().addChatMessage(new TextComponentTranslation("You aren't trained to use this."));
				}
				event.setCanceled(true);
			}
		}

		if (world.getBlockState(pos).getBlock() instanceof BlockWorkbench)
		{
			world.setBlockState(pos, SpecializationsMod.CRAFTING_TABLE.getDefaultState());
		}

		ItemStack stack = event.getEntityPlayer().getHeldItemOffhand() != null ? event.getEntityPlayer().getHeldItemOffhand() : event.getEntityPlayer().getHeldItemMainhand();
		if (stack != null && event.getUseItem() != Event.Result.DENY)
		{
			if (stack.getItem() instanceof ItemDye && b instanceof IGrowable && stack.getItemDamage() == 15 && !(b instanceof BlockFlower) && !(b instanceof BlockGrass) && !(b instanceof BlockTallGrass))
			{
				event.setCanceled(true);
			}
		}
	}

	@SubscribeEvent
	public void entityAttacked(LivingHurtEvent event)
	{
		Entity targetEntity = event.getEntity();
		Entity attackerEntity = event.getSource().getEntity();
		if (attackerEntity instanceof EntityCustomArrow)
		{
			attackerEntity = ((EntityCustomArrow) attackerEntity).shootingEntity;
		}

		if (attackerEntity instanceof EntityPlayer && targetEntity instanceof EntityLivingBase)
		{
			EntityLivingBase target = (EntityLivingBase) targetEntity;
			EntityPlayer attacker = (EntityPlayer) attackerEntity;
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(attacker);
			ItemStack held = attacker.inventory.getCurrentItem();

			float tempDamg = SpecializationsMod.getAttackerDamage(event, target, attacker);

			float progressEarned = 0.0f;
			if (lplayer.specClass.equalsIgnoreCase("warrior"))
			{ // getHealth()
				if (target instanceof EntityLivingBase && tempDamg >= ((EntityLivingBase) target).getHealth() && tempDamg >= 3)
				{
					progressEarned = 1.0f;
					if (target instanceof EntityMob)
					{
						progressEarned = 1.5f;
					}
					if (target instanceof EntityAnimal)
					{
						progressEarned = 0.25f;
						if (target instanceof EntityWolf)
						{
							progressEarned = 1.5f;
						}
					}
					if (!attacker.capabilities.isCreativeMode)
					{
						lplayer.addSpecProgress(progressEarned);
						attacker.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
					}
				}
			}

			if (lplayer.specClass.equalsIgnoreCase("stealth"))
			{
				// getHealth()
				if (target instanceof EntityLivingBase && tempDamg >= ((EntityLivingBase) target).getHealth() && tempDamg >= 2)
				{
					if (target instanceof EntityPlayer)
					{
						progressEarned = 3.0f;
					}
					else if (target instanceof EntityMob && !(target instanceof EntityAnimal))
					{
						progressEarned = 1.0f;
					}
					if (!attacker.capabilities.isCreativeMode)
					{
						lplayer.addSpecProgress(progressEarned);
						attacker.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
					}
				}
			}

			if (lplayer.specClass.equalsIgnoreCase("farmer"))
			{
				// getHealth()
				if (target instanceof EntityLivingBase && tempDamg >= ((EntityLivingBase) target).getHealth())
				{
					if (target instanceof EntityAnimal)
					{
						progressEarned = 4.0f;
						if (target instanceof EntityWolf)
						{
							progressEarned = 6.0f;
						}
					}
					if (!attacker.capabilities.isCreativeMode)
					{
						lplayer.addSpecProgress(progressEarned);
						attacker.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
					}
				}
			}

			// Damage Reduction
			DamageSource damageSrc = event.getSource();

			if (target instanceof EntityPlayer)
			{
				EntityPlayer tp = (EntityPlayer) target;
				if (tempDamg <= 0) return;

				float blockMod = 1.0f;
				if (!damageSrc.isUnblockable() && tp.getActiveItemStack().getItem() == Items.SHIELD && tempDamg > 0.0F)
				{
					LoMaS_Player tplayer = LoMaS_Player.getLoMaSPlayer(tp);
					if (tplayer.specClass.equalsIgnoreCase("warrior"))
					{
						if (tplayer.classlevel >= 6)
						{
							blockMod = 2.0f;
						}
					}
					tempDamg = (1.0F + tempDamg) * (0.5F / blockMod);
				}

				// tempDamg = net.minecraftforge.common.ISpecialArmor.ArmorProperties.applyArmor(tp, tp.inventory.armorInventory, damageSrc, tempDamg);
				SpecializationsMod.ApplyArmor(target, ((EntityPlayer) target).inventory.armorInventory, damageSrc, tempDamg);
				if (tempDamg <= 0) return;
				tempDamg = SpecializationsMod.applyPotionDamageCalculations(damageSrc, tempDamg, tp);
				float f1 = tempDamg;
				tempDamg = Math.max(tempDamg - tp.getAbsorptionAmount(), 0.0F);
				tp.setAbsorptionAmount(tp.getAbsorptionAmount() - (f1 - tempDamg));

				if (tempDamg != 0.0F)
				{
					tp.addExhaustion(damageSrc.getHungerDamage());
					float f2 = tp.getHealth();
					tp.setHealth(tp.getHealth() - tempDamg);
					tp.getCombatTracker().trackDamage(damageSrc, f2, tempDamg);

					if (tempDamg < 3.4028235E37F)
					{
						tp.addStat(StatList.DAMAGE_TAKEN, Math.round(tempDamg * 10.0F));
					}
				}
				event.setCanceled(true);
			}
			else
			{
				event.setAmount(tempDamg);// target.attackEntityFrom(event.source, tempDamg);
			}
		}
	}

	@SubscribeEvent
	public void livingDrops(LivingDropsEvent event)
	{
		if (event.getSource().getEntity() instanceof EntityPlayer && event.getEntityLiving() instanceof EntityAnimal)
		{
			EntityAnimal animal = (EntityAnimal) event.getEntityLiving();
			LoMaS_Animal lAnimal = LoMaS_Animal.getLoMaSAnimal(animal);
			System.out.println(lAnimal + " " + animal);
			EntityPlayer attacker = (EntityPlayer) event.getSource().getEntity();
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(attacker);
			int failureChance = 0;
			if (!event.getDrops().isEmpty() && lplayer != null)
			{
				if (!lplayer.specClass.equalsIgnoreCase("farmer"))
				{
					failureChance = failureChance + 50;
				}

				if (attacker.worldObj.rand.nextInt(100) < failureChance)
				{
					attacker.addChatMessage(new TextComponentTranslation("\u00a7cYour unskilled hands have caused most of the animal to be unusable."));
					for (EntityItem i : event.getDrops())
					{
						if (i.getEntityItem().getItem() instanceof ItemSaddle)
						{
							animal.entityDropItem(i.getEntityItem(), 1);
						}
					}
					event.setCanceled(true);
				}
				else if (lAnimal != null && lAnimal.getHunger() < 50 && lAnimal.getSickness() <= 0 && lAnimal.getAge() < 600000)
				{
					for (EntityItem i : event.getDrops())
					{
						if (i.getEntityItem().getItem() instanceof net.minecraft.item.ItemFood)
						{
							i.getEntityItem().setItemDamage(SpecializationsMod.getQualityIndexFromDamage(lAnimal.getQuality()));
						}
					}
					return;
				}
				else
				{
					attacker.addChatMessage(new TextComponentTranslation("\u00a7cThe animal was not fit enough to produce anything useful."));
					event.setCanceled(true);
				}

				if (lAnimal != null)
				{
					LoMaS_Animal.animalList.remove(LoMaS_Animal.getLoMaSAnimal(animal.getPersistentID()));
				}
			}
		}
		else if (event.getEntityLiving() instanceof EntityAnimal)
		{
			EntityAnimal animal = (EntityAnimal) event.getEntityLiving();
			LoMaS_Animal lAnimal = LoMaS_Animal.getLoMaSAnimal(animal);
			if (!event.getDrops().isEmpty())
			{
				if (lAnimal != null && lAnimal.getHunger() < 50 && lAnimal.getSickness() <= 0 && lAnimal.getAge() < 600000)
				{
					for (EntityItem i : event.getDrops())
					{
						if (i.getEntityItem().getItem() instanceof net.minecraft.item.ItemFood)
						{
							i.getEntityItem().setItemDamage(SpecializationsMod.getQualityIndexFromDamage(lAnimal.getQuality()));
						}
					}
					return;
				}
				else
				{
					event.setCanceled(true);
				}

				if (lAnimal != null)
				{
					LoMaS_Animal.animalList.remove(LoMaS_Animal.getLoMaSAnimal(animal.getPersistentID()));
				}
			}
		}
	}

	@SubscribeEvent
	public void playerRightClickWithBucket(FillBucketEvent event)
	{
		EntityPlayer player = event.getEntityPlayer();
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
		ItemStack c = event.getEmptyBucket();
		if (c.getItem().equals(Items.MILK_BUCKET))
		{
			float progressEarned = 0.0f;
			if (lplayer.specClass.equalsIgnoreCase("farmer"))
			{
				progressEarned = 1.0f;
				if (!player.capabilities.isCreativeMode)
				{
					lplayer.addSpecProgress(progressEarned);
					player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
				}
			}
		}
	}

	@SubscribeEvent
	public void onDrops(BlockEvent.HarvestDropsEvent event)
	{
		EntityPlayer player = event.getHarvester();
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
		BlockPos pos = event.getPos();
		World world = event.getWorld();
		IBlockState state = event.getState();

		if (player != null && lplayer != null)
		{
			if (state.getBlock() instanceof BlockTallGrass)
			{
				if (event.isSilkTouching())
				{
					return;
				}

				if (lplayer.specClass.equalsIgnoreCase("farmer"))
				{
					float seedChance = 16.0f;
					if (lplayer.classlevel >= 2)
					{
						seedChance = seedChance + 10.0f;
					}
					if (lplayer.classlevel >= 3)
					{
						seedChance = seedChance + 20.0f;
					}
					if (world.rand.nextInt(100) > seedChance)
					{
						event.getDrops().clear();
					}
				}
				else
				{
					event.getDrops().clear();
				}
			}
		}
	}

	@SubscribeEvent
	public void onItemUse(LivingEntityUseItemEvent.Finish event)
	{
		ItemStack stack = event.getItem();
		EntityLivingBase ent = event.getEntityLiving();
		if (ent instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) ent;
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
			if (stack.getItem() instanceof ItemPotion)
			{
				if (((stack.getItemDamage() != 0) && (stack.getItemDamage() != 16) && (stack.getItemDamage() != 32) && (stack.getItemDamage() != 64) && (stack.getItemDamage() != 8192)) && (lplayer.specClass.equalsIgnoreCase("spellbinder")))
				{
					float progressEarned = 0.75f;
					if (!player.capabilities.isCreativeMode)
					{
						lplayer.addSpecProgress(progressEarned);
						player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void bowNock(ArrowNockEvent event)
	{
		if (event.getEntity() instanceof EntityPlayer)
		{
			EntityPlayer player = event.getEntityPlayer();
			ItemStack itemstack = event.getBow();
			World world = player.worldObj;
			if (itemstack != null && itemstack.getItem() != null)
			{
				Item item = itemstack.getItem();
				if (BigFMod.getInventorySlotContainItem(player, SpecializationsMod.ARROW) != null)
				{
					LoMaS_Player.getLoMaSPlayer(player).arrowStack = BigFMod.getInventorySlotContainItem(player, SpecializationsMod.ARROW);
					if (!world.isRemote)
					{
						SpecializationsMod.snw.sendToAll(new ArrowMessage(LoMaS_Player.getLoMaSPlayer(player).arrowStack.getItemDamage(), player));
					}
					// System.out.println(LoMaS_Player.getLoMaSPlayer(player).arrowStack);
				}
			}
		}
	}

	@SubscribeEvent(priority = EventPriority.HIGH)
	public void onExplosion(ExplosionEvent.Detonate event)
	{
		Explosion explosion = event.getExplosion();
		World world = event.getWorld();
		List<Entity> e = event.getAffectedEntities();
		List<BlockPos> b = event.getAffectedBlocks();
		if (explosion.getExplosivePlacedBy() instanceof EntityCreeper)
		{
			EntityCreeper creeper = (EntityCreeper) explosion.getExplosivePlacedBy();
			float creepMult = 1.0f;
			if (creeper.getPowered())
			{
				creepMult = 2.0f;
			}
			Iterator iterator = b.iterator();
			/*
			 * while (iterator.hasNext()) { BlockPos blockpos = (BlockPos) iterator.next(); if (world.getBlockState(blockpos).getBlock().getMaterial() == Material.air &&
			 * world.getBlockState(blockpos.down()).getBlock().isFullBlock() && world.rand.nextInt(3) == 0) { world.setBlockState(blockpos, Blocks.fire.getDefaultState()); } }
			 */

			b.clear();

			float f3 = (3.0f * creepMult) * 3.0F;
			for (Entity ent : e)
			{
				Vec3d Vec3d = explosion.getPosition();
				double d12 = ent.getDistance(explosion.getPosition().xCoord, explosion.getPosition().yCoord, explosion.getPosition().zCoord) / (double) f3;
				double d14 = (double) world.getBlockDensity(Vec3d, ent.getEntityBoundingBox());
				double d10 = (1.0D - d12) * d14;
				ent.attackEntityFrom(DamageSource.causeExplosionDamage(explosion), (float) ((int) ((d10 * d10 + d10) / 2.0D * 7.0D * (double) f3 + 1.0D)));
			}
		}
	}

	@SubscribeEvent
	public void onPlayerSpawn(EntityJoinWorldEvent event)
	{
		Entity e = event.getEntity();
		if (e instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) e;
			LoMaS_Player.addLoMaSPlayer(player);
			if (player != null && LoMaS_Player.getLoMaSPlayer(player) != null)
			{
				// FMLHandler.setPlayerCurrentHealth(player);
				FMLHandler.setPlayerMaxHealth(player);
				if (player.capabilities != null)
				{
					SCMEventHandler.setPlayerWalkSpeed(player);
				}
				LoMaS_Player.getLoMaSPlayer(player).sendPlayerAbilities();
			}
		}
	}

	/*
	 * @SubscribeEvent public void onAnimalDies(LivingDeathEvent event) { if (event.getEntityLiving() instanceof EntityAnimal) {
	 * LoMaS_Animal.animalList.remove(LoMaS_Animal.getLoMaSAnimal(event.getEntityLiving().getEntityId())); } }
	 */

	@SubscribeEvent
	public void onAnimalUpdate(LivingUpdateEvent event)
	{
		if (event.getEntityLiving() instanceof EntityAnimal)
		{
			World world = event.getEntity().worldObj;
			EntityAnimal animal = (EntityAnimal) event.getEntityLiving();
			LoMaS_Animal lAnimal = LoMaS_Animal.getLoMaSAnimal(animal);
			if (lAnimal != null)
			{
				// should be 40. is 0 for testing
				if (lAnimal.getHunger() > 40)
				{
					lAnimal.setReady(false);
					// String s = "AnimalInfo:" + animal + ":" + "starve";

					if (animal.ticksExisted % 10 == 0 && world.isRemote)
					{
						double d0 = world.rand.nextGaussian() * 0.02D;
						double d1 = world.rand.nextGaussian() * 0.02D;
						double d2 = world.rand.nextGaussian() * 0.02D;
						int[] info = SpecializationsMod.getAnimalParticleInfo(animal, 0);
						Minecraft.getMinecraft().renderGlobal.spawnParticle(77, false, animal.posX + (double) (world.rand.nextFloat() * animal.width * 2.0F) - (double) animal.width, animal.posY + 0.5D + (double) (world.rand.nextFloat() * animal.height), animal.posZ + (double) (world.rand.nextFloat() * animal.width * 2.0F) - (double) animal.width, d0, d1, d2, info);
					}
				}

				if (lAnimal.getAge() > 600000)
				{
					lAnimal.setReady(false);
					// String s = "AnimalInfo:" + animal + ":" + "oldage";

					if (animal.ticksExisted % 10 == 0 && world.isRemote)
					{
						double d0 = world.rand.nextGaussian() * 0.02D;
						double d1 = world.rand.nextGaussian() * 0.02D;
						double d2 = world.rand.nextGaussian() * 0.02D;
						int[] info = SpecializationsMod.getAnimalParticleInfo(animal, 2);
						Minecraft.getMinecraft().renderGlobal.spawnParticle(77, false, animal.posX + (double) (world.rand.nextFloat() * animal.width * 2.0F) - (double) animal.width, animal.posY + 0.5D + (double) (world.rand.nextFloat() * animal.height), animal.posZ + (double) (world.rand.nextFloat() * animal.width * 2.0F) - (double) animal.width, d0, d1, d2, info);
					}
				}

				if (lAnimal.getSickness() > 0)
				{
					lAnimal.setReady(false);
					// String s = "AnimalInfo:" + animal + ":" + "sickness";

					if (animal.ticksExisted % 10 == 0 && world.isRemote)
					{
						double d0 = world.rand.nextGaussian() * 0.02D;
						double d1 = world.rand.nextGaussian() * 0.02D;
						double d2 = world.rand.nextGaussian() * 0.02D;
						int[] info = SpecializationsMod.getAnimalParticleInfo(animal, 3);
						Minecraft.getMinecraft().renderGlobal.spawnParticle(77, false, animal.posX + (double) (world.rand.nextFloat() * animal.width * 2.0F) - (double) animal.width, animal.posY + 0.5D + (double) (world.rand.nextFloat() * animal.height), animal.posZ + (double) (world.rand.nextFloat() * animal.width * 2.0F) - (double) animal.width, d0, d1, d2, info);
					}
				}

				if (!world.isRemote)
				{
					if (animal.ticksExisted % 80 == 0 && lAnimal.getHunger() >= 100)
					{
						animal.attackEntityFrom(DamageSource.starve, 1.0F);
					}

					if (animal.ticksExisted % (36 * 20) == 0)
					{
						EntityPlayer owner = null;
						if (lAnimal.getLastToucher() != null && !lAnimal.getLastToucher().equals(""))
						{
							owner = animal.worldObj.getPlayerEntityByUUID(lAnimal.getLastToucher());
						}

						if (world.rand.nextInt(100) >= 20 && (owner != null || lAnimal.getLastToucher() == null || lAnimal.getLastToucher().equals("")))
						{
							if (lAnimal.getHunger() < 0)
							{
								lAnimal.setAge(lAnimal.getAge() - 126);
								if (lAnimal.getAge() < 300)
								{
									lAnimal.setAge(300);
								}
							}

							lAnimal.setHunger(lAnimal.getHunger() + 1);

							if (lAnimal.getHunger() > 100)
							{
								lAnimal.setHunger(100);
							}
						}
						int sicknessChance = 400;
						if (lAnimal.getHunger() < 10)
						{
							sicknessChance = 700;
						}
						if ((world.rand.nextInt(sicknessChance) == 0 || lAnimal.getSickness() > 0) && (owner != null || lAnimal.getLastToucher() == null || lAnimal.getLastToucher().equals("")))
						{
							lAnimal.setAge(lAnimal.getAge() + 2520);
							lAnimal.setSickness(lAnimal.getSickness() + 1);

							List list = animal.worldObj.getEntitiesWithinAABB(animal.getClass(), animal.getEntityBoundingBox().expand((double) 4.0, (double) 4.0, (double) 4.0));
							EntityAnimal entityanimal = null;
							for (int i = 0; i < list.size(); ++i)
							{
								entityanimal = (EntityAnimal) list.get(i);
								if (entityanimal != animal && world.rand.nextInt(100) < 5)
								{
									lAnimal.setSickness(1);
								}
							}
						}
					}

					if (lAnimal.getAge() >= 604800)
					{
						animal.setDead();
						EntityPlayer owner = null;
						if (lAnimal.getLastToucher() != null && !lAnimal.getLastToucher().equals(""))
						{
							owner = animal.worldObj.getPlayerEntityByUUID(lAnimal.getLastToucher());
						}

						if (owner != null)
						{
							owner.addChatComponentMessage(new TextComponentTranslation("\u00a7cOne of your animals has passed away due to old age. Life goes on."));
						}
					}

					if (animal.ticksExisted % 100 == 0)
					{
						Long secFactor = 1000L;
						Long timeDiff = ((System.currentTimeMillis() - lAnimal.getLastSystemTime()) / secFactor);
						lAnimal.setAge((int) (timeDiff + lAnimal.getAge()));
						SpecializationsMod.snw.sendToAll(new AnimalMessage(animal.getEntityId(), lAnimal.getAge(), lAnimal.getHunger(), lAnimal.getSickness(), lAnimal.isReady()));
						lAnimal.setLastSystemTime(System.currentTimeMillis());
					}

					if (!(lAnimal.getSickness() <= 0 && lAnimal.getHunger() < 40))
					{
						if (animal instanceof EntityChicken)
						{
							((EntityChicken) animal).timeUntilNextEgg = world.rand.nextInt(6000) + 6000;
						}
						else if (animal instanceof EntitySheep)
						{
							((EntitySheep) animal).setSheared(true);
						}
					}

					if (lAnimal.getAge() >= 604800 || lAnimal.getSickness() > 0 || lAnimal.getHunger() > 40)
					{
						animal.resetInLove();
					}

					if (lAnimal.getRemoveTouchFlagTimer() > 0 && animal.ticksExisted % 20 == 0)
					{
						lAnimal.setRemoveTouchFlagTimer(lAnimal.getRemoveTouchFlagTimer() - 1);
					}

					if (lAnimal.getBeenTouched() && lAnimal.getRemoveTouchFlagTimer() <= 0 && animal.ticksExisted % 20 == 0)
					{
						lAnimal.setBeenTouched(false);
					}
				}

				if (animal.getGrowingAge() == 0 && !animal.isInLove() && lAnimal.getAge() >= 300 && lAnimal.getAge() <= 600000 && lAnimal.getHunger() >= 0 && lAnimal.getHunger() < 40 && lAnimal.getSickness() == 0)
				{
					if (lAnimal.isReady())
					{
						EntityPlayer owner = null;
						if (lAnimal.getLastToucher() != null && !lAnimal.getLastToucher().equals(""))
						{
							owner = animal.worldObj.getPlayerEntityByUUID(lAnimal.getLastToucher());
						}
						if (owner != null || lAnimal.getLastToucher() == null || lAnimal.getLastToucher().equals(""))
						{
							// System.out.println(animal + " isReady:" + animal.isReady());
							List list = world.getEntitiesWithinAABB(animal.getClass(), animal.getEntityBoundingBox().expand((double) 4.0, (double) 4.0, (double) 4.0));
							boolean foundOne = false;
							EntityAnimal entityanimal = null;
							LoMaS_Animal entitylAnimal = null;
							for (int i = 0; i < list.size() && !foundOne; ++i)
							{
								entityanimal = (EntityAnimal) list.get(i);
								entitylAnimal = LoMaS_Animal.getLoMaSAnimal(entityanimal);
								if (entitylAnimal != null && entityanimal != animal && entitylAnimal.isReady() && (!(entityanimal instanceof EntityWolf) || !((EntityWolf) entityanimal).isSitting()))
								{
									foundOne = true;
								}
							}
							if (foundOne && entityanimal != null)
							{
								// System.out.println(animal + " foundOne:" + foundOne + " " + entityanimal);
								animal.setInLove(owner);
								lAnimal.setReady(false);
								for (EntityAITaskEntry entry : animal.tasks.taskEntries)
								{
									if (entry.action instanceof EntityAIMate)
									{
										EntityAIMate task = (EntityAIMate) entry.action;
										try
										{
											Field f = task.getClass().getDeclaredFields()[2];
											f.setAccessible(true);
											f.set(task, entityanimal);
											task.updateTask();
										}
										catch (IllegalArgumentException e)
										{
											e.printStackTrace();
										}
										catch (IllegalAccessException e)
										{
											e.printStackTrace();
										}
									}
								}
								entityanimal.setInLove(owner);
								entitylAnimal.setReady(false);
								for (EntityAITaskEntry entry : entityanimal.tasks.taskEntries)
								{
									if (entry.action instanceof EntityAIMate)
									{
										EntityAIMate task = (EntityAIMate) entry.action;
										try
										{
											Field f = task.getClass().getDeclaredFields()[2];
											f.setAccessible(true);
											f.set(task, entityanimal);
											task.updateTask();
										}
										catch (IllegalArgumentException e)
										{
											e.printStackTrace();
										}
										catch (IllegalAccessException e)
										{
											e.printStackTrace();
										}
									}
								}
							}
						}
					}
					else if (!animal.worldObj.isRemote && animal.ticksExisted % 240 == 0 && world.rand.nextInt(100) <= 5 && world.isDaytime())
					{
						lAnimal.setReady(true);
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onAnimalSpawn(EntityJoinWorldEvent event)
	{
		if (event.getEntity() instanceof EntityAnimal)
		{
			World world = event.getWorld();
			EntityAnimal animal = (EntityAnimal) event.getEntity();
			LoMaS_Animal.addLoMaSAnimal(animal);
		}
	}

	@SubscribeEvent
	public void onEntityRightClick(EntityInteract event)
	{
		Entity e = event.getTarget();
		EntityPlayer player = event.getEntityPlayer();
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
		World world = event.getEntity().worldObj;
		if (e instanceof EntityAnimal)
		{
			EntityAnimal ea = (EntityAnimal) e;
			LoMaS_Animal lAnimal = LoMaS_Animal.getLoMaSAnimal(ea);
			lAnimal.setBeenTouched(true);
			lAnimal.setLastToucher(lplayer.uuid);
			if (lplayer.specClass.equalsIgnoreCase("farmer"))
			{
				lAnimal.setBreederLevel(lplayer.classlevel);
			}

			ItemStack i = player.inventory.getCurrentItem();
			if (i != null && SpecializationsMod.isBreedingItem(ea, i))// ea.isBreedingItem(i))
			{
				if (lplayer.specClass.equalsIgnoreCase("farmer"))
				{
					float progressEarned = 0.5f;
					if (!player.capabilities.isCreativeMode)
					{
						lplayer.addSpecProgress(progressEarned);
						player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
					}
				}

				if (!player.capabilities.isCreativeMode)
				{
					--i.stackSize;

					if (i.stackSize <= 0)
					{
						player.inventory.setInventorySlotContents(player.inventory.currentItem, (ItemStack) null);
					}
				}

				if (lAnimal.getHunger() > 0)
				{
					lAnimal.setHunger(lAnimal.getHunger() - 50);
					if (lAnimal.getHunger() < 0)
					{
						lAnimal.setHunger(0);
					}
				}
				else
				{
					lAnimal.setHunger(lAnimal.getHunger() - 5);
					if (lAnimal.getHunger() < -25)
					{
						lAnimal.setHunger(-25);
					}
				}

				if (world.isRemote)
				{
					for (int j = 0; j < 7; ++j)
					{
						// fed
						double d0 = world.rand.nextGaussian() * 0.02D;
						double d1 = world.rand.nextGaussian() * 0.02D;
						double d2 = world.rand.nextGaussian() * 0.02D;
						int[] info = SpecializationsMod.getAnimalParticleInfo(ea, 1);
						Minecraft.getMinecraft().renderGlobal.spawnParticle(77, false, ea.posX + (double) (world.rand.nextFloat() * ea.width * 2.0F) - (double) ea.width, ea.posY + 0.5D + (double) (world.rand.nextFloat() * ea.height), ea.posZ + (double) (world.rand.nextFloat() * ea.width * 2.0F) - (double) ea.width, d0, d1, d2, info);
					}
				}

				if (ea instanceof EntitySheep)
				{
					((EntitySheep) ea).setSheared(false);
				}

				event.setCanceled(true);
			}
			else if (i != null && (i.getItem() == Items.GOLDEN_APPLE || i.getItem() == Items.GOLDEN_CARROT || i.getItem() == Items.MUSHROOM_STEW))
			{
				if (!player.capabilities.isCreativeMode)
				{
					if (i.getItem() != Items.MUSHROOM_STEW)
					{
						--i.stackSize;

						if (i.stackSize <= 0)
						{
							player.inventory.setInventorySlotContents(player.inventory.currentItem, null);
						}
					}
					else
					{
						--i.stackSize;

						if (i.stackSize <= 0)
						{
							player.inventory.setInventorySlotContents(player.inventory.currentItem, null);
						}
						player.inventory.addItemStackToInventory(new ItemStack(Items.BOWL, 1));
					}
				}

				if (lAnimal.getHunger() > 0)
				{
					lAnimal.setHunger(lAnimal.getHunger() - 30);
					if (lAnimal.getHunger() < 0)
					{
						lAnimal.setHunger(0);
					}
				}
				else
				{
					lAnimal.setHunger(lAnimal.getHunger() - 3);
					if (lAnimal.getHunger() < -25)
					{
						lAnimal.setHunger(-25);
					}
				}

				if (world.isRemote)
				{
					for (int j = 0; j < 7; ++j)
					{
						// fed
						double d0 = world.rand.nextGaussian() * 0.02D;
						double d1 = world.rand.nextGaussian() * 0.02D;
						double d2 = world.rand.nextGaussian() * 0.02D;
						int[] info = SpecializationsMod.getAnimalParticleInfo(ea, 1);
						Minecraft.getMinecraft().renderGlobal.spawnParticle(77, false, ea.posX + (double) (world.rand.nextFloat() * ea.width * 2.0F) - (double) ea.width, ea.posY + 0.5D + (double) (world.rand.nextFloat() * ea.height), ea.posZ + (double) (world.rand.nextFloat() * ea.width * 2.0F) - (double) ea.width, d0, d1, d2, info);
					}
				}

				if (ea instanceof EntitySheep)
				{
					((EntitySheep) ea).setSheared(false);
				}

				if (lplayer.specClass.equals("farmer") && lAnimal.getSickness() > 0)
				{
					lAnimal.setBreederLevel(lplayer.classlevel);
					float progressEarned = 5.0f;
					if (!player.capabilities.isCreativeMode)
					{
						lplayer.addSpecProgress(progressEarned);
						player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
					}
				}
				else if (lplayer.specClass.equals("farmer"))
				{
					lAnimal.setBreederLevel(lplayer.classlevel);
					float progressEarned = 0.5f;
					if (!player.capabilities.isCreativeMode)
					{
						lplayer.addSpecProgress(progressEarned);
						player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
					}
				}

				lAnimal.setSickness(0);

				event.setCanceled(true);
			}
			else if (i != null && i.getItem().equals(Items.SHEARS))
			{
				if (ea instanceof IShearable && ((IShearable) ea).isShearable(i, e.worldObj, e.getPosition()))
				{
					float progressEarned = 0.0f;
					if (lplayer.specClass.equalsIgnoreCase("farmer"))
					{
						progressEarned = 2.0f;
						if (!player.capabilities.isCreativeMode)
						{
							lplayer.addSpecProgress(progressEarned);
							player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
						}
					}
				}
			}
			else if (i != null && i.getItem().equals(Items.BUCKET) && ea instanceof EntityCow && !(ea instanceof EntityMooshroom))
			{
				if (lAnimal.getSickness() <= 0 && lAnimal.getHunger() < 40)
				{
					if (i.stackSize-- == 1)
					{
						player.inventory.setInventorySlotContents(player.inventory.currentItem, new ItemStack(Items.MILK_BUCKET));
					}
					else if (!player.inventory.addItemStackToInventory(new ItemStack(Items.MILK_BUCKET)))
					{
						player.dropItem(new ItemStack(Items.MILK_BUCKET), true, true);
					}

					if (lplayer.specClass.equals("farmer"))
					{
						lAnimal.setBreederLevel(lplayer.classlevel);
						float progressEarned = 1.0f;
						if (!player.capabilities.isCreativeMode)
						{
							lplayer.addSpecProgress(progressEarned);
							player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
						}
					}

					event.setCanceled(true);
				}
			}
		}
	}

	@SubscribeEvent
	public void onSaveWorld(WorldEvent.Save event)
	{
		World world = event.getWorld();
		if (!world.isRemote && event.getWorld().provider.getDimension() == 0)
		{
			File f = world.getMinecraftServer().getDataDirectory();
			// System.out.println("Saving animals...");
			File saveDir = new File(f.getPath() + File.separator + "world" + File.separator + "LoMaS" + File.separator);
			saveDir.mkdir();
			File animalFile = new File(saveDir.getPath() + File.separator + event.getWorld().provider.getDimensionType().getName() + ".spec");
			try
			{
				FileOutputStream saveOut = new FileOutputStream(animalFile);
				ObjectOutputStream saveObj = new ObjectOutputStream(saveOut);

				saveObj.writeObject(LoMaS_Animal.animalList);

				saveObj.close();
				saveOut.close();
			}
			catch (Exception e1)
			{
				System.err.println("Failed to save animal file at " + animalFile.getPath() + " for world " + event.getWorld().provider.getDimensionType());
				// e1.printStackTrace();
			}
		}
	}

	@SubscribeEvent
	public void onLoadWorld(WorldEvent.Load event)
	{
		World world = event.getWorld();
		if (!world.isRemote && event.getWorld().provider.getDimension() == 0)
		{
			File f = world.getMinecraftServer().getDataDirectory();
			// System.out.println("Loading animals...");
			File loadDir = new File(f.getPath() + File.separator + "world" + File.separator + "LoMaS" + File.separator);
			loadDir.mkdir();
			File animalFile = new File(loadDir.getPath() + File.separator + event.getWorld().provider.getDimensionType() + ".spec");
			try
			{
				FileInputStream loadOut = new FileInputStream(animalFile);
				ObjectInputStream loadObj = new ObjectInputStream(loadOut);

				ArrayList<LoMaS_Animal> temp;
				temp = (ArrayList<LoMaS_Animal>) loadObj.readObject();
				for (LoMaS_Animal lAnimal : temp)
				{
					LoMaS_Animal.animalList.add(lAnimal);
				}

				loadObj.close();
				loadOut.close();
			}
			catch (Exception e1)
			{
				System.err.println("Failed to load animal file at " + animalFile.getPath() + " for world " + event.getWorld().provider.getDimensionType());
				// e1.printStackTrace();
			}
		}
	}

	/*
	 * @SubscribeEvent public void onXPPickup(PlayerPickupXpEvent event) { EntityXPOrb xp = event.orb; EntityPlayer player = event.getEntityPlayer(); LoMaS_Player lplayer =
	 * LoMaS_Player.getLoMaSPlayer(player); if (lplayer != null) { double xpMulti = 0.8; if (lplayer.specClass.equalsIgnoreCase("spellbinder") && lplayer.classlevel >= 1) { xpMulti = xpMulti + .25; }
	 * if (lplayer.specClass.equalsIgnoreCase("spellbinder") && lplayer.classlevel >= 6) { xpMulti = xpMulti + .10; } if (lplayer.specClass.equalsIgnoreCase("spellbinder") && lplayer.classlevel >= 9)
	 * { xpMulti = xpMulti + .15; } if (player.experienceLevel >= 5 && !lplayer.specClass.equalsIgnoreCase("spellbinder")) { player.experienceLevel = 5; player.experience = 0.0F;
	 * player.experienceTotal = 0; event.setCanceled(true); } else if (player.experienceLevel >= 10 && lplayer.specClass.equalsIgnoreCase("spellbinder") && lplayer.classlevel < 4) {
	 * player.experienceLevel = 10; player.experience = 0.0F; player.experienceTotal = 0; event.setCanceled(true); } else if (player.experienceLevel >= 20 &&
	 * lplayer.specClass.equalsIgnoreCase("spellbinder") && lplayer.classlevel < 8) { player.experienceLevel = 20; player.experience = 0.0F; player.experienceTotal = 0; event.setCanceled(true); } else
	 * if (player.experienceLevel >= 30 && lplayer.specClass.equalsIgnoreCase("spellbinder") && lplayer.classlevel < 11) { player.experienceLevel = 30; player.experience = 0.0F; player.experienceTotal
	 * = 0; event.setCanceled(true); } xp.xpValue = (int) (xp.xpValue * xpMulti); } }
	 */

	@SubscribeEvent
	public void replaceXP(EntityEvent.EnteringChunk event)
	{
		World world = event.getEntity().worldObj;
		Entity e = event.getEntity();

		if (!e.isDead && e instanceof EntityXPOrb && !(e instanceof EntityCustomXPOrb))
		{
			EntityXPOrb orb = (EntityXPOrb) e;
			EntityCustomXPOrb cxp = new EntityCustomXPOrb(world, orb.posX, orb.posY, orb.posZ, orb.xpValue);
			orb.setDead();
			world.spawnEntityInWorld(cxp);
			// System.out.println("XP ORB IS FeRND");
		}
	}

	@SubscribeEvent
	public void itemJoinsWorld(EntityJoinWorldEvent event)
	{
		Entity entity = event.getEntity();
		World world = event.getWorld();
		if (entity instanceof EntityItem)
		{
			EntityItem entItem = (EntityItem) entity;
			ItemStack stack = entItem.getEntityItem();
			if (stack != null)
			{
				if (stack.getItem().equals(Items.ARROW))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.ARROW, stack.stackSize, 1));
				}
				else if (stack.getItem().equals(Items.BOW))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.BOW, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Item.getItemFromBlock(Blocks.SAPLING)))
				{
					entItem.setEntityItemStack(new ItemStack(Item.getItemFromBlock(SpecializationsMod.SAPLING), stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Item.getItemFromBlock(Blocks.MELON_BLOCK)))
				{
					entItem.setEntityItemStack(new ItemStack(Item.getItemFromBlock(FoodInitializer.MELON_BLOCK), stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Item.getItemFromBlock(Blocks.PUMPKIN)))
				{
					entItem.setEntityItemStack(new ItemStack(Item.getItemFromBlock(FoodInitializer.PUMPKIN), stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Item.getItemFromBlock(Blocks.HAY_BLOCK)))
				{
					entItem.setEntityItemStack(new ItemStack(Item.getItemFromBlock(FoodInitializer.HAY_BLOCK), stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Item.getItemFromBlock(Blocks.CRAFTING_TABLE)))
				{
					entItem.setEntityItemStack(new ItemStack(Item.getItemFromBlock(SpecializationsMod.CRAFTING_TABLE), stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Item.getItemFromBlock(Blocks.FURNACE)))
				{
					entItem.setEntityItemStack(new ItemStack(Item.getItemFromBlock(SpecializationsMod.FURNACE), stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Item.getItemFromBlock(Blocks.BREWING_STAND)))
				{
					entItem.setEntityItemStack(new ItemStack(Item.getItemFromBlock(SpecializationsMod.BREWING_STAND), stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.WHEAT))
				{
					entItem.setEntityItemStack(new ItemStack(FoodInitializer.WHEAT, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.WHEAT_SEEDS))
				{
					entItem.setEntityItemStack(new ItemStack(FoodInitializer.WHEAT_SEEDS, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.REEDS))
				{
					entItem.setEntityItemStack(new ItemStack(FoodInitializer.REEDS, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.CARROT))
				{
					entItem.setEntityItemStack(new ItemStack(FoodInitializer.CARROT, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.POTATO))
				{
					entItem.setEntityItemStack(new ItemStack(FoodInitializer.POTATO, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.NETHER_WART))
				{
					entItem.setEntityItemStack(new ItemStack(FoodInitializer.NETHER_WART, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.MELON_SEEDS))
				{
					entItem.setEntityItemStack(new ItemStack(FoodInitializer.MELON_SEEDS, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.PUMPKIN_SEEDS))
				{
					entItem.setEntityItemStack(new ItemStack(FoodInitializer.PUMPKIN_SEEDS, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.WOODEN_AXE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.WOODEN_AXE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.WOODEN_PICKAXE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.WOODEN_PICKAXE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.WOODEN_HOE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.WOODEN_HOE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.WOODEN_SWORD))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.WOODEN_SWORD, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.WOODEN_SHOVEL))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.WOODEN_SHOVEL, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.STONE_AXE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.STONE_AXE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.STONE_PICKAXE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.STONE_PICKAXE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.STONE_HOE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.STONE_HOE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.STONE_SWORD))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.STONE_SWORD, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.STONE_SHOVEL))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.STONE_SHOVEL, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.GOLDEN_AXE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.GOLDEN_AXE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.GOLDEN_PICKAXE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.GOLDEN_PICKAXE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.GOLDEN_HOE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.GOLDEN_HOE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.GOLDEN_SWORD))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.GOLDEN_SWORD, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.GOLDEN_SHOVEL))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.GOLDEN_SHOVEL, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.IRON_AXE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.IRON_AXE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.IRON_PICKAXE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.IRON_PICKAXE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.IRON_HOE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.IRON_HOE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.IRON_SWORD))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.IRON_SWORD, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.IRON_SHOVEL))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.IRON_SHOVEL, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.DIAMOND_AXE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.DIAMOND_AXE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.DIAMOND_PICKAXE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.DIAMOND_PICKAXE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.DIAMOND_HOE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.DIAMOND_HOE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.DIAMOND_SWORD))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.DIAMOND_SWORD, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.DIAMOND_SHOVEL))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.DIAMOND_SHOVEL, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.LEATHER_HELMET))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.LEATHER_HELMET, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.LEATHER_CHESTPLATE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.LEATHER_CHESTPLATE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.LEATHER_LEGGINGS))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.LEATHER_LEGGINGS, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.LEATHER_BOOTS))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.LEATHER_BOOTS, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.CHAINMAIL_HELMET))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.CHAINMAIL_HELMET, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.CHAINMAIL_CHESTPLATE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.CHAINMAIL_CHESTPLATE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.CHAINMAIL_LEGGINGS))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.CHAINMAIL_LEGGINGS, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.CHAINMAIL_BOOTS))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.CHAINMAIL_BOOTS, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.IRON_HELMET))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.IRON_HELMET, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.IRON_CHESTPLATE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.IRON_CHESTPLATE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.IRON_LEGGINGS))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.IRON_LEGGINGS, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.IRON_BOOTS))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.IRON_BOOTS, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.GOLDEN_HELMET))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.GOLDEN_HELMET, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.GOLDEN_CHESTPLATE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.GOLDEN_CHESTPLATE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.GOLDEN_LEGGINGS))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.GOLDEN_LEGGINGS, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.GOLDEN_BOOTS))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.GOLDEN_BOOTS, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.DIAMOND_HELMET))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.DIAMOND_HELMET, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.DIAMOND_CHESTPLATE))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.DIAMOND_CHESTPLATE, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.DIAMOND_LEGGINGS))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.DIAMOND_LEGGINGS, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem().equals(Items.DIAMOND_BOOTS))
				{
					entItem.setEntityItemStack(new ItemStack(SpecializationsMod.DIAMOND_BOOTS, stack.stackSize, stack.getMetadata()));
				}
				else if (stack.getItem() instanceof net.minecraft.item.ItemFood && !(stack.getItem() instanceof ItemFood))
				{
					if (stack.getItem().equals(Items.BREAD))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.BREAD, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.WHEAT))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.WHEAT, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.FISH))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.FISH, stack.stackSize, stack.getMetadata() << 2));
					}
					else if (stack.getItem().equals(Items.COOKED_FISH))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.COOKED_FISH, stack.stackSize, stack.getMetadata() << 2));
					}
					else if (stack.getItem().equals(Items.APPLE))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.APPLE, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.MUSHROOM_STEW))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.MUSHROOM_STEW, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.RABBIT_STEW))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.RABBIT_STEW, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.POISONOUS_POTATO))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.POISONOUS_POTATO, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.SPIDER_EYE))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.SPIDER_EYE, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.CAKE))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.CAKE, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.PORKCHOP))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.PORKCHOP, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.COOKED_PORKCHOP))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.COOKED_PORKCHOP, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.COOKIE))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.COOKIE, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.MELON))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.MELON, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.BEEF))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.BEEF, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.COOKED_BEEF))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.COOKED_BEEF, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.CHICKEN))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.CHICKEN, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.COOKED_CHICKEN))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.COOKED_CHICKEN, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.BAKED_POTATO))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.BAKED_POTATO, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.GOLDEN_CARROT))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.GOLDEN_CARROT, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.PUMPKIN_PIE))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.PUMPKIN_PIE, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.RABBIT))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.RABBIT, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.COOKED_RABBIT))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.COOKED_RABBIT, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.MUTTON))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.MUTTON, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.COOKED_MUTTON))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.COOKED_MUTTON, stack.stackSize, stack.getMetadata()));
					}
					else if (stack.getItem().equals(Items.ROTTEN_FLESH))
					{
						entItem.setEntityItemStack(new ItemStack(FoodInitializer.ROTTEN_FLESH, stack.stackSize, stack.getMetadata()));
					}
				}
				else if (stack.getItem() instanceof ItemCoin)
				{
					EntityItemCoin ent2 = new EntityItemCoin(entItem.worldObj, entItem.posX, entItem.posY, entItem.posZ, entItem.getEntityItem());
					this.copyDataFromOld(entItem, ent2);
					ent2.copyLocationAndAnglesFrom(entItem);
					ent2.motionX = entItem.motionX;
					ent2.motionY = entItem.motionY;
					ent2.motionZ = entItem.motionZ;
					ent2.setPickupDelay(40);
					ent2.setOwner(entItem.getOwner());
					ent2.setThrower(entItem.getThrower());
					world.removeEntity(entItem);
					world.spawnEntityInWorld(ent2);
				}
			}
		}
		else if (entity instanceof EntityXPOrb)
		{

		}
	}

	private void copyDataFromOld(Entity entityOld, Entity entityNew)
	{
		NBTTagCompound nbttagcompound = entityOld.writeToNBT(new NBTTagCompound());
		entityNew.readFromNBT(nbttagcompound);
	}

	public static void setPlayerWalkSpeed(EntityPlayer player)
	{
		Field f;
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
		try
		{
			f = player.capabilities.getClass().getDeclaredFields()[6];// .getDeclaredField("walkSpeed");
			f.setAccessible(true);
			f.set(player.capabilities, SpecializationsMod.getWalkSpeed(player, lplayer));
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
		catch (IllegalAccessException e)
		{
			e.printStackTrace();
		}
		catch (SecurityException e)
		{
			e.printStackTrace();
		}
		catch (ConcurrentModificationException e)
		{
			e.printStackTrace();
		}
	}

	public static float getExhaustion(EntityPlayer player)
	{
		float exhaustion = 0.0f;
		Field f;
		try
		{
			f = player.getFoodStats().getClass().getDeclaredFields()[2];// .getDeclaredField("foodExhaustionLevel");
			f.setAccessible(true);
			exhaustion = (Float) f.get(player.getFoodStats());
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
		catch (IllegalAccessException e)
		{
			e.printStackTrace();
		}
		catch (SecurityException e)
		{
			e.printStackTrace();
		}
		return exhaustion;
	}

	public static String printStacksFromSlots(List a)
	{
		String c = "[";
		for (Object o : a)
		{
			if (o instanceof Slot)
			{
				Slot s = (Slot) o;
				c = c + s.getStack() + ", ";
			}
		}
		c = c + "]";
		return c;
	}

	// This is fired before the client receives the inventory data, and so we can use it for prepping the client to receive the inventory.
	@SubscribeEvent
	public void inventoryEvent(EntityJoinWorldEvent event)
	{
		Entity ent = event.getEntity();
		if (ent instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) ent;

			if (!(player.inventory instanceof InventoryPlayer))
			{
				InventoryPlayer newInv = new InventoryPlayer(player);
				newInv.readFromNBT(player.inventory.writeToNBT(new NBTTagList()));
				player.inventory = newInv;
			}

			if (!(player.inventoryContainer instanceof SpecPlayerContainer))
			{
				player.inventoryContainer = new SpecPlayerContainer((InventoryPlayer) player.inventory, !player.worldObj.isRemote, player);
				player.openContainer = player.inventoryContainer;
				player.inventory.markDirty();
			}
		}
	}

	@SubscribeEvent
	public void onPlayerLoad(AttachCapabilitiesEvent.Entity event)
	{
		if (event.getEntity() instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) event.getEntity();

			if (!event.getEntity().hasCapability(LoMaS_Player.PLAYER_CAP, null))
			{
				CapabilitiesProvider prov = new CapabilitiesProvider();
				prov.getCapability(LoMaS_Player.PLAYER_CAP, null).setEntityPlayer(player);
				prov.getCapability(LoMaS_Player.PLAYER_CAP, null).setInventoryBank(new InventoryBank(player));
				prov.getCapability(LoMaS_Player.PLAYER_CAP, null).setWorld(player.worldObj);
				event.addCapability(new ResourceLocation("SpecializationsMod:PlayerCap"), prov);
			}
		}
	}

	public static boolean useHoe(ItemStack stack, EntityPlayer player, World worldIn, BlockPos target, IBlockState newState)
	{
		worldIn.playSound(player, target, SoundEvents.ITEM_HOE_TILL, SoundCategory.BLOCKS, 1.0F, 1.0F);

		if (worldIn.isRemote)
		{
			return true;
		}
		else
		{
			worldIn.setBlockState(target, newState);
			stack.damageItem(1, player);
			return true;
		}
	}

	/*
	 * public static void getEntitiesWithinChunk(Chunk chunk, List listToFill) { int i = MathHelper.floor_double((0 - World.MAX_ENTITY_RADIUS) / 16.0D); int j = MathHelper.floor_double((16 +
	 * World.MAX_ENTITY_RADIUS) / 16.0D); i = MathHelper.clamp_int(i, 0, chunk.getEntityLists().length - 1); j = MathHelper.clamp_int(j, 0, chunk.getEntityLists().length - 1); for (int k = i; k <= j;
	 * ++k) { Iterator iterator = chunk.getEntityLists()[k].iterator(); while (iterator.hasNext()) { Entity entity1 = (Entity) iterator.next(); listToFill.add(entity1); Entity[] aentity =
	 * entity1.getParts(); if (aentity != null) { for (int l = 0; l < aentity.length; ++l) { entity1 = aentity[l]; listToFill.add(entity1); } } } } }
	 */
}
