package bitevo.Zetal.LoMaS.Citizens;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.item.Item;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import bitevo.Zetal.LoMaS.Citizens.Entities.EntityPigman;
import bitevo.Zetal.LoMaS.Citizens.Renderer.CitizenRendererFactory;
import bitevo.Zetal.LoMaS.Citizens.Renderer.ModelPigman;
import bitevo.Zetal.LoMaS.Citizens.Renderer.RenderPigman;
import bitevo.Zetal.LoMaS.Citizens.Renderer.TileEntityCitizenChestRenderer;
import bitevo.Zetal.LoMaS.Citizens.TileEntity.TileEntityCitizenChest;

public class CitizensClientProxy extends CitizensCommonProxy
{
	public static Minecraft mc;
	
	@Override
	public void registerRenderInformation()
	{
		mc = Minecraft.getMinecraft();
		
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCitizenChest.class, new TileEntityCitizenChestRenderer());
		RenderManager rm = mc.getRenderManager();
		RenderingRegistry.registerEntityRenderingHandler(EntityPigman.class, new CitizenRendererFactory());
		init();
	}

	@Override
	public void init()
	{
		
	}
}
