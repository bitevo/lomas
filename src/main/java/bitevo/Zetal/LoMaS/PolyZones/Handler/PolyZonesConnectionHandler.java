package bitevo.Zetal.LoMaS.PolyZones.Handler;

import java.net.InetSocketAddress;
import java.util.ArrayList;

import bitevo.Zetal.LoMaS.PolyZones.DataHandler;
import bitevo.Zetal.LoMaS.PolyZones.PolyZone;
import bitevo.Zetal.LoMaS.PolyZones.PolyZonesMod;
import bitevo.Zetal.LoMaS.PolyZones.Message.PZMessage;
import io.netty.channel.local.LocalAddress;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.PlayerTickEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent.ClientConnectedToServerEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PolyZonesConnectionHandler
{
	public static boolean sent = false;

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void onPlayerTick(PlayerTickEvent event)
	{
		if (Minecraft.getMinecraft().thePlayer != null && (!sent))
		{
			EntityPlayer player = (EntityPlayer) Minecraft.getMinecraft().thePlayer;
			if (PolyZonesMod.processor == null)
			{
				PolyZonesMod.processor = DataHandler.getProcessor();
			}
			PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, 0, player.getPersistentID()));
			PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, 1, player.getPersistentID()));
			PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, 2, player.getPersistentID()));
			// PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, 3, player.getPersistentID()));
			ArrayList<PolyZone> tempArray = new ArrayList();
			for (int i = 0; i < PolyZonesMod.processor.getZones().size(); i++)
			{
				tempArray.clear();
				tempArray.add(PolyZonesMod.processor.getZones().get(i));
				PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, 4 + i, player.getPersistentID()));
			}
			if (PolyZonesMod.processor.getZones().size() == 0)
			{
				PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, -1, player.getPersistentID()));
			}
			sent = true;
			PolyZonesMod.playersMove.put(player.getPersistentID(), new Boolean(false));
		}
	}

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void onClientConnected(ClientConnectedToServerEvent event)
	{
		if (event.getManager().getRemoteAddress() instanceof LocalAddress)
		{

			PolyZonesMod.ip = "localhost";
		}
		else
		{
			InetSocketAddress s = (InetSocketAddress) event.getManager().getRemoteAddress();
			PolyZonesMod.ip = s.getAddress().getHostAddress();
		}
		System.out.println(PolyZonesMod.ip);
		sent = false;
	}

	@SubscribeEvent
	public void playerConnected(PlayerLoggedInEvent event)
	{
		if (event.player.worldObj != null && !event.player.worldObj.isRemote)
		{
			PolyZonesMod.playersMove.put(event.player.getPersistentID(), new Boolean(false));
		}
	}
}
