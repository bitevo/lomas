package bitevo.Zetal.LoMaS.Factions.Block;

import java.util.Random;

import net.minecraft.block.BlockSlab;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import bitevo.Zetal.LoMaS.BigFMod.BigFMod;
import bitevo.Zetal.LoMaS.Factions.FactionsMod;

public abstract class BlockRoadSlab extends BlockSlab
{
    private static final PropertyBool VARIANT_PROPERTY =
            PropertyBool.create("variant");
    
    public BlockRoadSlab()
    {
        super(Material.ROCK);
        IBlockState iblockstate = this.blockState.getBaseState();

        if (!this.isDouble())
        {
            iblockstate = iblockstate.withProperty(HALF, BlockSlab.EnumBlockHalf.BOTTOM);
            this.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
        }

        setDefaultState(iblockstate);
        this.setHarvestLevel("pickaxe", 0);
        
        this.useNeighborBrightness = !this.isDouble();
    }

    public Item getItemDropped(IBlockState state, Random rand, int fortune)
    {
        return Item.getItemFromBlock(FactionsMod.ROAD_SLAB);
    }

    @SideOnly(Side.CLIENT)
    public Item getItem(World worldIn, BlockPos pos)
    {
        return Item.getItemFromBlock(FactionsMod.ROAD_SLAB);
    }

    public String getUnlocalizedName(int meta)
    {
        return this.getUnlocalizedName();
    }
    
    /**
     * Creates the block state object.
     * @return the block state with properties defined.
     */
    @Override
    protected final BlockStateContainer createBlockState() {
        if (this.isDouble()) {
            return new BlockStateContainer(this, new IProperty[] {VARIANT_PROPERTY});
        } else {
            return new BlockStateContainer(
                this,
                new IProperty[] {VARIANT_PROPERTY, HALF});
        }
    }

    public IBlockState getStateFromMeta(int meta)
    {
    	IBlockState iblockstate = this.getDefaultState();
        if (!this.isDouble())
        {
            iblockstate = iblockstate.withProperty(HALF, (meta) == 0 ? BlockSlab.EnumBlockHalf.BOTTOM : BlockSlab.EnumBlockHalf.TOP);
        }

        return iblockstate;
    }

    public int getMetaFromState(IBlockState state)
    {
    	int i = 1;
        if (!this.isDouble() && state.getValue(HALF) == BlockSlab.EnumBlockHalf.TOP)
        {
            i = 0;
        }

        return i;
    }

	@Override
	public IProperty getVariantProperty()
	{
        return VARIANT_PROPERTY;
	}
}
