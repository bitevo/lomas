package bitevo.Zetal.LoMaS.BigFMod;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderBoat;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.item.Item;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import bitevo.Zetal.LoMaS.BigFMod.Entities.EntityBoat;
import bitevo.Zetal.LoMaS.BigFMod.TileEntity.TileEntityBookstand;
import bitevo.Zetal.LoMaS.Specializations.Entities.EntityCustomArrow;
import bitevo.Zetal.LoMaS.Specializations.Renderer.RenderCustomArrow;

public class BigFClientProxy extends BigFCommonProxy
{
	public static Minecraft mc;

	public class BoatRendererFactory implements IRenderFactory<EntityBoat>
	{
		@Override
		public Render<? super EntityBoat> createRenderFor(RenderManager manager)
		{
			return new RenderBoat(manager);
		}
	}
	
	@Override
	public void registerRenderInformation() 
	{  
		String modid = BigFMod.modid;
		mc = Minecraft.getMinecraft();
		RenderItem itemRenderer = mc.getRenderItem();
		
		TileEntityBookstandRenderer bookstandRenderer = new TileEntityBookstandRenderer();
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBookstand.class, bookstandRenderer);

		RenderingRegistry.registerEntityRenderingHandler(EntityBoat.class, new BoatRendererFactory());
		
//		itemRenderer.getItemModelMesher().register(Item.getItemFromBlock(BigFMod.ROUGHSTAIRS), 0, new ModelResourceLocation(modid + ":roughStairs", "inventory"));
//		itemRenderer.getItemModelMesher().register(Item.getItemFromBlock(BigFMod.ROUGH_SLAB), 0, new ModelResourceLocation(modid + ":rough_slab", "inventory"));
//		itemRenderer.getItemModelMesher().register(Item.getItemFromBlock(BigFMod.ROUGH_DOUBLE_SLAB), 0, new ModelResourceLocation(modid + ":rough_double_slab", "inventory"));
//		itemRenderer.getItemModelMesher().register(Item.getItemFromBlock(BigFMod.BOOKSTAND), 0, new ModelResourceLocation(modid + ":bookstand", "inventory"));
//		itemRenderer.getItemModelMesher().register(BigFMod.STICK, 0, new ModelResourceLocation(modid + ":oakStick", "inventory"));
//		itemRenderer.getItemModelMesher().register(BigFMod.STICK, 1, new ModelResourceLocation(modid + ":spruceStick", "inventory"));
//		itemRenderer.getItemModelMesher().register(BigFMod.STICK, 2, new ModelResourceLocation(modid + ":birchStick", "inventory"));
//		itemRenderer.getItemModelMesher().register(BigFMod.STICK, 3, new ModelResourceLocation(modid + ":jungleStick", "inventory"));
//		itemRenderer.getItemModelMesher().register(BigFMod.STICK, 4, new ModelResourceLocation(modid + ":acaciaStick", "inventory"));
//		itemRenderer.getItemModelMesher().register(BigFMod.STICK, 5, new ModelResourceLocation(modid + ":darkoakStick", "inventory"));
//		
//		itemRenderer.getItemModelMesher().register(BigFMod.BOAT, 0, new ModelResourceLocation(modid + ":boat", "inventory"));
//
//		ModelBakery.registerItemVariants(Item.getItemFromBlock(BigFMod.ROUGHSTAIRS), new ModelResourceLocation(modid + ":roughStairs"));
//		ModelBakery.registerItemVariants(Item.getItemFromBlock(BigFMod.ROUGH_SLAB), new ModelResourceLocation(modid + ":rough_slab"));
//		ModelBakery.registerItemVariants(Item.getItemFromBlock(BigFMod.BOOKSTAND), new ModelResourceLocation(modid + ":bookstand"));
//		ModelBakery.registerItemVariants(BigFMod.STICK, new ModelResourceLocation(modid + ":oakStick"), new ModelResourceLocation(modid + ":spruceStick"), new ModelResourceLocation(modid + ":birchStick"), new ModelResourceLocation(modid + ":jungleStick"), new ModelResourceLocation(modid + ":acaciaStick"), new ModelResourceLocation(modid + ":darkoakStick"));
	}
}