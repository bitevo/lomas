package bitevo.Zetal.LoMaS.Specializations.Handler;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import org.lwjgl.input.Keyboard;

import bitevo.Zetal.LoMaS.Specializations.GUI.GuiSpecInfo;

public class SpecInfoGUIKeyBind
{
	/**
	 * Storing an instance of Minecraft in a local variable saves having to get
	 * it every time
	 */
	private final Minecraft mc;
	
	/** Key index for easy handling */
	public static final int CUSTOM_INV = 0;
	
	/** Key descriptions; use a language file to localize the description later */
	private static final String[] desc = { "Class Information" };
	
	/** Default key values */
	private static final int[] keyValues = { Keyboard.KEY_K };
	
	/**
	 * Make this public or provide a getter if you'll need access to the key
	 * bindings from elsewhere
	 */
	public static final KeyBinding[] keys = new KeyBinding[desc.length];
	
	@SideOnly(Side.CLIENT)
	public SpecInfoGUIKeyBind()
	{
		mc = Minecraft.getMinecraft();
		for (int i = 0; i < desc.length; ++i)
		{
			keys[i] = new KeyBinding(desc[i], keyValues[i], I18n.translateToLocal("LoMaS"));
			ClientRegistry.registerKeyBinding(keys[i]);
		}
	}
	
	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void keyDown(KeyInputEvent event)
	{
		if (mc.theWorld != null && mc.theWorld.isRemote)
		{
			if (keys[0].isPressed())
			{
				EntityPlayerSP player = mc.thePlayer;
				if (mc.inGameHasFocus && mc.currentScreen == null)
				{
					GuiSpecInfo gui = new GuiSpecInfo(player);
					mc.displayGuiScreen(gui);
				}
				/*else if (mc.currentScreen instanceof GuiSpecInfo)
				{
					System.out.println("off");
					player.closeScreenNoPacket();
				}*/
			}
		}
	}
}