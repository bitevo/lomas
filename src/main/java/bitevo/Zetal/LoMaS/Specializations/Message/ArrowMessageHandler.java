package bitevo.Zetal.LoMaS.Specializations.Message;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ArrowMessageHandler implements IMessageHandler<ArrowMessage, IMessage>
{
	private static World world;

	@Override
	@SideOnly(Side.CLIENT)
	public IMessage onMessage(ArrowMessage message, MessageContext ctx)
	{
		world = Minecraft.getMinecraft().theWorld;
		handleArrowMessage(message);
		return null;
	}

	@SideOnly(Side.CLIENT)
	private void handleArrowMessage(ArrowMessage packet)
	{
		EntityPlayer pplayer = world.getPlayerEntityByUUID(packet.playerUUID);
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(pplayer);
		if (lplayer != null && pplayer != null)
		{
			lplayer.arrowStack = new ItemStack(SpecializationsMod.ARROW, 1, packet.arrowType);
		}
	}
}
