package bitevo.Zetal.LoMaS.Specializations.Renderer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityCustomArrow;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderCustomArrow<T extends EntityArrow> extends Render<T>
{
	private static final ResourceLocation field_110780_a = new ResourceLocation("textures/entity/arrow.png");

	public RenderCustomArrow(RenderManager p_i46193_1_)
	{
		super(p_i46193_1_);
	}

	public void renderCustomArrow(EntityCustomArrow par1EntityArrow, double par2, double par4, double par6, float par8, float par9)
	{
		this.bindEntityTexture((T) par1EntityArrow);
		GL11.glPushMatrix();
		GL11.glTranslatef((float) par2, (float) par4, (float) par6);
		GL11.glRotatef(par1EntityArrow.prevRotationYaw + (par1EntityArrow.rotationYaw - par1EntityArrow.prevRotationYaw) * par9 - 90.0F, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(par1EntityArrow.prevRotationPitch + (par1EntityArrow.rotationPitch - par1EntityArrow.prevRotationPitch) * par9, 0.0F, 0.0F, 1.0F);
		Tessellator tessellator = Tessellator.getInstance();
		VertexBuffer vertexbuffer = tessellator.getBuffer();
		byte b0 = 0;
		float f2 = 0.0F;
		float f3 = 0.5F;
		float f4 = (float) (0 + b0 * 10) / 32.0F;
		float f5 = (float) (5 + b0 * 10) / 32.0F;
		float f6 = 0.0F;
		float f7 = 0.15625F;
		float f8 = (float) (5 + b0 * 10) / 32.0F;
		float f9 = (float) (10 + b0 * 10) / 32.0F;
		// Specializations
		// System.out.println("Arrowtype: " + par1EntityArrow.arrowType);
		if (par1EntityArrow.arrowType == -1)
		{
			// par1EntityArrow.arrowType = LoMaS_Player.getLoMaSPlayer((EntityPlayer) par1EntityArrow.shootingEntity).arrowStack.getItemDamage();
			if (par1EntityArrow.shootingEntity instanceof EntityPlayer)
			{
				if (LoMaS_Player.getLoMaSPlayer((EntityPlayer) par1EntityArrow.shootingEntity) != null)
				{
					if (LoMaS_Player.getLoMaSPlayer((EntityPlayer) par1EntityArrow.shootingEntity).arrowStack != null)
					{
						par1EntityArrow.arrowType = LoMaS_Player.getLoMaSPlayer((EntityPlayer) par1EntityArrow.shootingEntity).arrowStack.getItemDamage();
					}
					else
					{
						// System.err.println("null arrowstack");
					}
				}
				else
				{
					// System.err.println("null shooter2");
				}
			}
			else
			{
				// System.err.println("null shooter1");
			}
		}
		if (par1EntityArrow.arrowType != 2)
		{
			if (par1EntityArrow.arrowType == 0)
			{
				f2 = (float) (16) / 32.0F;
				f3 = (float) (32) / 32.0F;
				f4 = (float) (0 + b0 * 10) / 32.0F;
				f5 = (float) (5 + b0 * 10) / 32.0F;
				f6 = (float) (16) / 32.0F;
				f7 = (float) (21) / 32.0F;
				f8 = (float) (5 + b0 * 10) / 32.0F;
				f9 = (float) (10 + b0 * 10) / 32.0F;
			}
			else if (par1EntityArrow.arrowType == 1)
			{
				b0 = 1;
				f2 = (float) (16) / 32.0F;
				f3 = (float) (32) / 32.0F;
				f4 = (float) (0 + b0 * 10) / 32.0F;
				f5 = (float) (5 + b0 * 10) / 32.0F;
				f6 = (float) (16) / 32.0F;
				f7 = (float) (21) / 32.0F;
				f8 = (float) (5 + b0 * 10) / 32.0F;
				f9 = (float) (10 + b0 * 10) / 32.0F;
			}
			else if (par1EntityArrow.arrowType == 3)
			{
				b0 = 2;
				f2 = (float) (16) / 32.0F;
				f3 = (float) (32) / 32.0F;
				f4 = (float) (0 + b0 * 10) / 32.0F;
				f5 = (float) (5 + b0 * 10) / 32.0F;
				f6 = (float) (16) / 32.0F;
				f7 = (float) (21) / 32.0F;
				f8 = (float) (5 + b0 * 10) / 32.0F;
				f9 = (float) (10 + b0 * 10) / 32.0F;
			}
			else if (par1EntityArrow.arrowType == 4)
			{
				b0 = 2;
				f2 = (float) (0) / 32.0F;
				f3 = (float) (16) / 32.0F;
				f4 = (float) (0 + b0 * 10) / 32.0F;
				f5 = (float) (5 + b0 * 10) / 32.0F;
				f6 = (float) (0) / 32.0F;
				f7 = (float) (5) / 32.0F;
				f8 = (float) (5 + b0 * 10) / 32.0F;
				f9 = (float) (10 + b0 * 10) / 32.0F;
			}
		}
		float f10 = 0.05625F;
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		float f11 = (float) par1EntityArrow.arrowShake - par9;

		if (f11 > 0.0F)
		{
			float f12 = -MathHelper.sin(f11 * 3.0F) * f11;
			GL11.glRotatef(f12, 0.0F, 0.0F, 1.0F);
		}

		GL11.glRotatef(45.0F, 1.0F, 0.0F, 0.0F);
		GL11.glScalef(f10, f10, f10);
		GL11.glTranslatef(-4.0F, 0.0F, 0.0F);
		GL11.glNormal3f(f10, 0.0F, 0.0F);
		vertexbuffer.begin(7, DefaultVertexFormats.POSITION_TEX);
		vertexbuffer.pos(-7.0D, -2.0D, -2.0D).tex((double) f6, (double) f8).endVertex();
		vertexbuffer.pos(-7.0D, -2.0D, 2.0D).tex((double) f7, (double) f8).endVertex();
		vertexbuffer.pos(-7.0D, 2.0D, 2.0D).tex((double) f7, (double) f9).endVertex();
		vertexbuffer.pos(-7.0D, 2.0D, -2.0D).tex((double) f6, (double) f9).endVertex();
		tessellator.draw();
		GL11.glNormal3f(-f10, 0.0F, 0.0F);
		vertexbuffer.begin(7, DefaultVertexFormats.POSITION_TEX);
		vertexbuffer.pos(-7.0D, 2.0D, -2.0D).tex((double) f6, (double) f8).endVertex();
		vertexbuffer.pos(-7.0D, 2.0D, 2.0D).tex((double) f7, (double) f8).endVertex();
		vertexbuffer.pos(-7.0D, -2.0D, 2.0D).tex((double) f7, (double) f9).endVertex();
		vertexbuffer.pos(-7.0D, -2.0D, -2.0D).tex((double) f6, (double) f9).endVertex();
		tessellator.draw();

		for (int i = 0; i < 4; ++i)
		{
			GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
			GL11.glNormal3f(0.0F, 0.0F, f10);
			vertexbuffer.begin(7, DefaultVertexFormats.POSITION_TEX);
			vertexbuffer.pos(-8.0D, -2.0D, 0.0D).tex((double) f2, (double) f4).endVertex();
			vertexbuffer.pos(8.0D, -2.0D, 0.0D).tex((double) f3, (double) f4).endVertex();
			vertexbuffer.pos(8.0D, 2.0D, 0.0D).tex((double) f3, (double) f5).endVertex();
			vertexbuffer.pos(-8.0D, 2.0D, 0.0D).tex((double) f2, (double) f5).endVertex();
			tessellator.draw();
		}

		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		GL11.glPopMatrix();
	}

	/**
	 * Actually renders the given argument. This is a synthetic bridge method, always casting down its argument and then handing it off to a worker function which does the actual work. In all
	 * probabilty, the class Render is generic (Render<T extends Entity) and this method has signature public void doRender(T entity, double d, double d1, double d2, float f, float f1). But JAD is pre
	 * 1.5 so doesn't do that.
	 */
	@Override
	public void doRender(T entity, double x, double y, double z, float entityYaw, float partialTicks)
	{
		this.renderCustomArrow((EntityCustomArrow) entity, x, y, z, entityYaw, partialTicks);
	}

	@Override
	protected ResourceLocation getEntityTexture(T entity)
	{
		return this.field_110780_a;
	}
}
