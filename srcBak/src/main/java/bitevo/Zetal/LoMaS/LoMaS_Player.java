package bitevo.Zetal.LoMaS;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Capabilities.IPlayerCapability;
import bitevo.Zetal.LoMaS.Specializations.Handler.FMLHandler;
import bitevo.Zetal.LoMaS.Specializations.Handler.SCMEventHandler;
import bitevo.Zetal.LoMaS.Specializations.Message.SpecializationMessage;

public class LoMaS_Player implements Serializable
{
	public static ArrayList<LoMaS_Player> playerList = new ArrayList<LoMaS_Player>();
	private static final long serialVersionUID = 1L;
	public static final float abilCooldown = 150.0f;

	public UUID uuid;
	public String name = "ERROR";
	public String specClass = "Basic";
	public int classlevel = 0;
	public int counter = 0;
	public boolean isActive = false;
	public boolean levelDisplay = false;
	public float progress = 0.0f;
	public String rank = "Member";
	public String guildtag = "";
	public float secCounter = 0.0f;
	public float prevfoodExhaustionLevel = -1.0f;
	public ArrayList<UUID> citizens = new ArrayList();
	public transient EntityPlayer player;
	public transient ItemStack arrowStack;
	public transient boolean notYet = true;

    @CapabilityInject(IPlayerCapability.class)
    public transient static final Capability<IPlayerCapability> PLAYER_CAP = null;
    
	private int bankCC = 90;

	public static void addLoMaSPlayer(EntityPlayer ePlayer)
	{
		if (ePlayer != null)
		{
			if (getLoMaSPlayer(ePlayer) == null)
			{
				LoMaS_Player p = new LoMaS_Player();
				p.player = ePlayer;
				p.uuid = ePlayer.getPersistentID();
				p.name = ePlayer.getDisplayNameString();
				playerList.add(p);
				System.out.println("Added a new LoMaSPlayer with the name " + p.name);
			}
			else
			{
				getLoMaSPlayer(ePlayer).player = ePlayer;
			}
		}
		else
		{
			System.err.println("Tried to add a NULL player to LoMaS_Player!");
		}
	}

	public static LoMaS_Player getLoMaSPlayer(EntityPlayer ePlayer)
	{
		LoMaS_Player retPlayer = null;
		if (ePlayer != null)
		{
			UUID playID = ePlayer.getPersistentID();
			for (int i = 0; i < playerList.size(); i++)
			{
				if (playID.equals(playerList.get(i).uuid))
				{
					retPlayer = playerList.get(i);
					if (retPlayer.player == null)
					{
						retPlayer.player = ePlayer;
					}
				}
			}
		}
		return retPlayer;
	}

	public static LoMaS_Player getLoMaSPlayer(UUID pid)
	{
		LoMaS_Player retPlayer = null;
		if (pid != null)
		{
			for (int i = 0; i < playerList.size(); i++)
			{
				if (pid.equals(playerList.get(i).uuid))
				{
					retPlayer = playerList.get(i);
				}
			}
		}
		return retPlayer;
	}

	public void copyTraitsFromLoMaSPlayer(LoMaS_Player lPlayer)
	{
		this.progress = lPlayer.progress;
		this.classlevel = lPlayer.classlevel;
		this.rank = lPlayer.rank;
		this.guildtag = lPlayer.guildtag;
		this.counter = lPlayer.counter;
		this.secCounter = lPlayer.secCounter;
		this.isActive = lPlayer.isActive;
		this.levelDisplay = lPlayer.levelDisplay;
		this.specClass = lPlayer.specClass;
		this.prevfoodExhaustionLevel = lPlayer.prevfoodExhaustionLevel;
		this.uuid = lPlayer.uuid;
		this.citizens = lPlayer.citizens;
		this.bankCC = lPlayer.bankCC;
	}

	public void sendPlayerAbilities()
	{
		if (this.player.worldObj.isRemote != true)
		{
			SpecializationsMod.snw.sendToAll(new SpecializationMessage(this.progress, this.specClass, this.classlevel, this.uuid, this.rank, this.guildtag, this.levelDisplay));
		}
		else
		{
			SpecializationsMod.snw.sendToServer(new SpecializationMessage(this.progress, this.specClass, this.classlevel, this.uuid, this.rank, this.guildtag, this.levelDisplay));
		}
	}

	public void changeSpecClass(String specClass)
	{
		System.out.println(this.name + " changed their class from " + this.specClass + " to " + specClass);
		this.specClass = specClass;
		this.progress = 0.0f;
		if (specClass.equalsIgnoreCase("farmer") && this.classlevel == 0)
		{
			ItemStack firstTool = new ItemStack(SpecializationsMod.WOODEN_HOE, 1);
			player.inventory.addItemStackToInventory(firstTool);
		}
		else if (specClass.equalsIgnoreCase("warrior") && this.classlevel == 0)
		{
			ItemStack firstTool = new ItemStack(SpecializationsMod.WOODEN_SWORD, 1);
			player.inventory.addItemStackToInventory(firstTool);
		}
		else if (specClass.equalsIgnoreCase("miner") && this.classlevel == 0)
		{
			ItemStack firstTool = new ItemStack(SpecializationsMod.WOODEN_PICKAXE, 1);
			player.inventory.addItemStackToInventory(firstTool);
		}
		else if (specClass.equalsIgnoreCase("woodsman") && this.classlevel == 0)
		{
			ItemStack firstTool = new ItemStack(SpecializationsMod.WOODEN_AXE, 1);
			player.inventory.addItemStackToInventory(firstTool);
		}
		else if (specClass.equalsIgnoreCase("spellbinder") && this.classlevel == 0)
		{
			ItemStack books = new ItemStack(Items.BOOK, 3);
			ItemStack bottles = new ItemStack(Items.GLASS_BOTTLE, 1);
			player.inventory.addItemStackToInventory(books);
			player.inventory.addItemStackToInventory(bottles);
		}
		else if (specClass.equalsIgnoreCase("blacksmith") && this.classlevel == 0)
		{
			ItemStack firstTool = new ItemStack(SpecializationsMod.CRAFTING_TABLE, 1);
			player.inventory.addItemStackToInventory(firstTool);
		}
		this.classlevel = 1;
		SCMEventHandler.setPlayerWalkSpeed(player);
		FMLHandler.setPlayerMaxHealth(player);
	}

	public void setSpecClassLevel(int classlevel)
	{
		if (classlevel > 10)
		{
			this.classlevel = 10;
		}
		else
		{
			this.classlevel = classlevel;
		}

		// MoveSpeed
		SCMEventHandler.setPlayerWalkSpeed(player);
		FMLHandler.setPlayerMaxHealth(player);
	}

	public void setSpecProgress(float progress)
	{
		this.progress = progress;
	}

	public void addSpecClassLevel(int addclasslevel)
	{
		if (this.classlevel + addclasslevel > 10)
		{
			this.classlevel = 10;
		}
		else
		{
			this.classlevel = this.classlevel + addclasslevel;
			//player.playSound(SpecializationsMod.modid + ":" + "levelup", 1.0F, 1.0F);
			//player.worldObj.playSoundEffect(player.posX, player.posY, player.posZ, SpecializationsMod.modid + ":" + "levelup", 1.0F, 1.0F);
			this.levelDisplay = true;
		}

		// MoveSpeed
		SCMEventHandler.setPlayerWalkSpeed(player);
		FMLHandler.setPlayerMaxHealth(player);
	}

	public void addSpecProgress(float addprogress)
	{
		this.progress = this.progress + addprogress;
		if (this.progress >= this.getReqProgress() && this.classlevel != 0)
		{
			this.addSpecClassLevel(1);
			this.progress = 0;
		}
	}

	public void doDeathPenalty()
	{
		this.progress = this.progress - (this.progress * 0.05f);
		if (this.progress >= this.getReqProgress() && this.classlevel != 0)
		{
			this.addSpecClassLevel(1);
			this.progress = 0.0f;
		}
		else if(this.progress <= 0.0f)
		{
			this.progress = 0.0f;
		}
	}

	public float getReqProgress()
	{
		int nextLevel = this.classlevel + 1;
		float reqProgress = (float) ((500 * Math.pow(nextLevel, 2)) + (Math.pow(nextLevel, 5)) - 1500);
		return reqProgress;
	}

	public void setSpecClass(String specClass)
	{
		this.specClass = specClass;
		SCMEventHandler.setPlayerWalkSpeed(player);
		FMLHandler.setPlayerMaxHealth(player);
	}

	public int getBankCC()
	{
		return bankCC;
	}

	public void setBankCC(int bankCC)
	{
		this.bankCC = bankCC;
	}
}
