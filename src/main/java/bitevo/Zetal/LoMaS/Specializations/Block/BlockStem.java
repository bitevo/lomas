package bitevo.Zetal.LoMaS.Specializations.Block;

import java.util.Iterator;
import java.util.Random;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.FoodInitializer;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntityPlant;
import net.minecraft.block.Block;
import net.minecraft.block.BlockBush;
import net.minecraft.block.BlockTorch;
import net.minecraft.block.IGrowable;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockStem extends BlockBush implements IGrowable, ITileEntityProvider
{
	public static final PropertyInteger AGE = PropertyInteger.create("age", 0, 7);
	public static final PropertyDirection FACING = BlockTorch.FACING;
	private final Block crop;
	protected static final AxisAlignedBB[] STEM_AABB = new AxisAlignedBB[] { new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 0.125D, 0.625D), new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 0.25D, 0.625D), new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 0.375D, 0.625D), new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 0.5D, 0.625D), new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 0.625D, 0.625D), new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 0.75D, 0.625D), new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 0.875D, 0.625D), new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 1.0D, 0.625D) };

	public BlockStem(Block crop)
	{
		this.setDefaultState(this.blockState.getBaseState().withProperty(AGE, Integer.valueOf(0)).withProperty(FACING, EnumFacing.UP));
		this.crop = crop;
		this.setTickRandomly(true);
		this.setCreativeTab((CreativeTabs) null);
		this.isBlockContainer = true;
	}

	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		return STEM_AABB[((Integer) state.getValue(AGE)).intValue()];
	}

	@Override
	public IBlockState onBlockPlaced(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
	{
		if (world.getTileEntity(pos) != null && world.getTileEntity(pos) instanceof TileEntityPlant && placer instanceof EntityPlayer)
		{
			TileEntityPlant tile = (TileEntityPlant) world.getTileEntity(pos.up());
			tile.setOwner(placer.getPersistentID());
		}
		this.updateTick(world, pos, world.getBlockState(pos), this.RANDOM);
		return this.getStateFromMeta(meta);
	}

	/**
	 * Get the actual Block state of this Block at the given position. This applies properties not visible in the metadata, such as fence connections.
	 */
	@Override
	public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos)
	{
		int i = ((Integer) state.getValue(AGE)).intValue();
		state = state.withProperty(FACING, EnumFacing.UP);

		for (EnumFacing enumfacing : EnumFacing.Plane.HORIZONTAL)
		{
			if (worldIn.getBlockState(pos.offset(enumfacing)).getBlock() == this.crop && i == 7)
			{
				state = state.withProperty(FACING, enumfacing);
				break;
			}
		}

		return state;
	}

	/**
	 * is the block grass, dirt or farmland
	 */
	@Override
	protected boolean canSustainBush(IBlockState ground)
	{
		return ground.getBlock() == FoodInitializer.FARMLAND;
	}

	@Override
	public void randomTick(World world, BlockPos pos, IBlockState state, Random rand)
	{
		this.updateTick(world, pos, state, rand);

		if (world.canSeeSky(pos) && world.getLightFromNeighbors(pos.up()) >= 9 && world.isDaytime())
		{
			float f = BlockCrops.getGrowthChance(this, world, pos);

			if (f > 0.0f && rand.nextInt((int) (25.0F / f) + 1) == 0)
			{
				int i = ((Integer) state.getValue(AGE)).intValue();

				if (i < 7)
				{
					state = state.withProperty(AGE, Integer.valueOf(i + 1));
					world.setBlockState(pos, state, 2);
				}
				else
				{
					Iterator iterator = EnumFacing.Plane.HORIZONTAL.iterator();

					while (iterator.hasNext())
					{
						EnumFacing enumfacing = (EnumFacing) iterator.next();

						if (world.getBlockState(pos.offset(enumfacing)).getBlock() == this.crop)
						{
							return;
						}
					}
					TileEntityPlant stemTile = null;
					TileEntityPlant gourdTile = null;
					if (world.getTileEntity(pos) != null && world.getTileEntity(pos) instanceof TileEntityPlant)
					{
						stemTile = (TileEntityPlant) world.getTileEntity(pos);
					}
					pos = pos.offset(EnumFacing.Plane.HORIZONTAL.random(rand));
					Block block = world.getBlockState(pos.down()).getBlock();

					if (world.isAirBlock(pos) && (SpecializationsMod.canSustainPlant(world, pos.down(), EnumFacing.UP, this) || block == Blocks.DIRT || block == Blocks.GRASS))
					{
						world.setBlockState(pos, this.crop.getDefaultState());
						if (world.getTileEntity(pos) != null && world.getTileEntity(pos) instanceof TileEntityPlant)
						{
							gourdTile = (TileEntityPlant) world.getTileEntity(pos);
							if (stemTile != null)
							{
								gourdTile.setQuality(stemTile.getQuality());
								stemTile.setQuality(0);
								stemTile.setFertilized(false);
								stemTile.markDirty();
								world.notifyBlockUpdate(pos, world.getBlockState(pos), world.getBlockState(pos), 3);
							}
						}
					}
				}
			}
		}
	}

	@Override
	public void updateTick(World world, BlockPos pos, IBlockState state, Random random)
	{
		world.scheduleUpdate(pos, state.getBlock(), this.tickRate(world));
		if (world.getTileEntity(pos) != null && world.getTileEntity(pos) instanceof TileEntityPlant)
		{
			TileEntityPlant tile = (TileEntityPlant) world.getTileEntity(pos);
			if (tile.getFertTime() <= 0)
			{
				tile.setFertilized(false);
				tile.markDirty();
				world.notifyBlockUpdate(pos, state, state, lightOpacity);
			}

			if (tile.isFertilized())
			{
				tile.setFertTime(tile.getFertTime() - 1);
			}
		}
		super.updateTick(world, pos, state, random);
	}

	@Override
	public boolean canBlockStay(World worldIn, BlockPos pos, IBlockState state)
	{
		boolean p1 = (worldIn.getLight(pos) >= 8 || worldIn.canSeeSky(pos));
		return p1 && this.canSustainBush(worldIn.getBlockState(pos.down()));
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void randomDisplayTick(IBlockState stateIn, World world, BlockPos pos, Random rand)
	{
		if (world.getTileEntity(pos) != null && world.getTileEntity(pos) instanceof TileEntityPlant)
		{
			TileEntityPlant tile = (TileEntityPlant) world.getTileEntity(pos);
			if (tile.isFertilized())
			{
				ItemDye.spawnBonemealParticles(world, pos, 1);
			}
		}
	}

	public void growStem(World world, BlockPos pos, IBlockState state)
	{
		int i = ((Integer) state.getValue(AGE)).intValue() + MathHelper.getRandomIntegerInRange(world.rand, 2, 5);
		world.setBlockState(pos, state.withProperty(AGE, Integer.valueOf(Math.min(7, i))), 2);
	}

	/**
	 * Spawns this Block's drops into the World as EntityItems.
	 *
	 * @param chance
	 *            The chance that each Item is actually spawned (1.0 = always, 0.0 = never)
	 * @param fortune
	 *            The player's fortune level
	 */
	@Override
	public void dropBlockAsItemWithChance(World worldIn, BlockPos pos, IBlockState state, float chance, int fortune)
	{
		super.dropBlockAsItemWithChance(worldIn, pos, state, chance, fortune);
	}

	@Override
	public java.util.List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
	{
		java.util.List<ItemStack> ret = new java.util.ArrayList<ItemStack>();
		{
			Item item = this.getSeedItem();

			if (item != null)
			{
				int j = ((Integer) state.getValue(AGE)).intValue();

				for (int k = 0; k < 3; ++k)
				{
					if (RANDOM.nextInt(15) <= j)
					{
						ret.add(new ItemStack(item));
					}
				}
			}
		}
		return ret;
	}

	protected Item getSeedItem()
	{
		return this.crop == FoodInitializer.PUMPKIN ? FoodInitializer.PUMPKIN_SEEDS : (this.crop == FoodInitializer.MELON_BLOCK ? FoodInitializer.MELON_SEEDS : null);
	}

	/**
	 * Get the Item that this Block should drop when harvested.
	 *
	 * @param fortune
	 *            the level of the Fortune enchantment on the player's tool
	 */
	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune)
	{
		return null;
	}

	/**
	 * Whether this IGrowable can grow
	 */
	@Override
	public boolean canGrow(World worldIn, BlockPos pos, IBlockState state, boolean isClient)
	{
		return ((Integer) state.getValue(AGE)).intValue() != 7 && worldIn.canSeeSky(pos);
	}

	@Override
	public boolean canUseBonemeal(World worldIn, Random rand, BlockPos pos, IBlockState state)
	{
		return true;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state)
	{
		Item item = this.getSeedItem();
		return item != null ? new ItemStack(item) : null;
	}

	@Override
	public void grow(World worldIn, Random rand, BlockPos pos, IBlockState state)
	{
		this.growStem(worldIn, pos, state);
	}

	/**
	 * Convert the given metadata into a BlockState for this Block
	 */
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty(AGE, Integer.valueOf(meta));
	}

	/**
	 * Convert the BlockState into the correct metadata value
	 */
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return ((Integer) state.getValue(AGE)).intValue();
	}

	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, new IProperty[] { AGE, FACING });
	}

	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
	{
		super.breakBlock(worldIn, pos, state);
		worldIn.removeTileEntity(pos);
	}

	/**
	 * Called on both Client and Server when World#addBlockEvent is called
	 */
	@Override
	public boolean eventReceived(IBlockState state, World worldIn, BlockPos pos, int eventID, int eventParam)
	{
		super.eventReceived(state, worldIn, pos, eventID, eventParam);
		TileEntity tileentity = worldIn.getTileEntity(pos);
		return tileentity == null ? false : tileentity.receiveClientEvent(eventID, eventParam);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileEntityPlant();
	}
}