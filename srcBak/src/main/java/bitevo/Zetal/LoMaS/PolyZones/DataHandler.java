package bitevo.Zetal.LoMaS.PolyZones;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class DataHandler
{

	public static void writeObject(PolyProcessor o)
	{
		PolyProcessor object = o;
		FileOutputStream fos;
		ObjectOutputStream oos = null;
		try
		{
			fos = new FileOutputStream("PolyZones.dat", false);
			try
			{
				oos = new ObjectOutputStream(fos);
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		try
		{
			oos.writeObject(object);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		try
		{
			oos.flush();
			oos.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@SuppressWarnings("resource")
	public static PolyProcessor getProcessor()
	{
		FileInputStream fis = null;
		try
		{
			fis = new FileInputStream("PolyZones.dat");
		}
		catch (FileNotFoundException e1)
		{
			System.out.println("Couldn't find PolyZones.dat 1");
			return new PolyProcessor();
		}
		ObjectInputStream ois = null;
		try
		{
			ois = new ObjectInputStream(fis);
		}
		catch (IOException e)
		{
			System.out.println("Couldn't find PolyZones.dat 2");
			return new PolyProcessor();
		}
		Object o = null;
		try
		{
			o = ois.readObject();
		}
		catch (IOException e)
		{
			System.out.println("Couldn't find PolyZones.dat 3");
			return new PolyProcessor();
		}
		catch (ClassNotFoundException e)
		{
			System.out.println("Couldn't find PolyZones.dat 4");
			return new PolyProcessor();
		}
		catch (Exception g)
		{
			System.out.println("Couldn't find PolyZones.dat 5");
			return new PolyProcessor();
		}
		try
		{
			ois.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return (PolyProcessor) o;
	}

	public static void writeLandsObject(ArrayList<CustomPlayer> o)
	{
		ArrayList<CustomPlayer> object = o;
		FileOutputStream fos;
		ObjectOutputStream oos = null;
		try
		{
			fos = new FileOutputStream("players.dat", false);
			try
			{
				oos = new ObjectOutputStream(fos);
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		try
		{
			oos.writeObject(object);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		try
		{
			oos.flush();
			oos.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@SuppressWarnings("resource")
	public static ArrayList<CustomPlayer> getPlayers()
	{
		FileInputStream fis = null;
		try
		{
			fis = new FileInputStream("players.dat");
		}
		catch (FileNotFoundException e1)
		{
			return new ArrayList<CustomPlayer>();
		}
		ObjectInputStream ois = null;
		try
		{
			ois = new ObjectInputStream(fis);
		}
		catch (IOException e)
		{
			return new ArrayList<CustomPlayer>();
		}
		Object o = null;
		try
		{
			o = ois.readObject();
		}
		catch (IOException e)
		{
			return new ArrayList<CustomPlayer>();
		}
		catch (ClassNotFoundException e)
		{
			return new ArrayList<CustomPlayer>();
		}
		catch (Exception g)
		{
			return new ArrayList<CustomPlayer>();
		}
		try
		{
			ois.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return (ArrayList<CustomPlayer>) o;
	}

	public static boolean isValidDataFile(PolyProcessor x)
	{
		if (PolyZonesMod.processor.zones.size() == x.zones.size())
		{
			for (int i = 0; i < PolyZonesMod.processor.zones.size(); i++)
			{
				if (PolyZonesMod.processor.zones.get(i).getPolygon().getEdges().size() == x.zones.get(i).getPolygon().getEdges().size())
				{
					if (x.zones.get(i) == null)
					{
						return false;
					}
					BlockPolygon temp = x.zones.get(i).getPolygon();
					if (temp == null)
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
		return true;
	}

	/*public static String bytesToHex(byte[] b)
	{
		char hexDigit[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
		StringBuffer buf = new StringBuffer();
		for (int j = 0; j < b.length; j++)
		{
			buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
			buf.append(hexDigit[b[j] & 0x0f]);
		}
		return buf.toString();
	}

	public static void sendProcessor(PolyProcessor x, String serverIP, String playerName) throws IOException
	{
		Socket s = new Socket(serverIP, PolyZonesMod.portAttempt);
		ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());
		try
		{
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(x.toString().getBytes());
			String hash = bytesToHex(md.digest());
			oos.writeObject(playerName);
			oos.writeObject(new HashObject(hash));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		oos.flush();
		oos.close();
		s.close();
	}*/
}
