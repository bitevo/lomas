package bitevo.Zetal.LoMaS.Economy;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import bitevo.Zetal.LoMaS.Economy.Block.BlockSafe;
import bitevo.Zetal.LoMaS.Economy.Block.BlockTeller;
import bitevo.Zetal.LoMaS.Economy.Entities.EntityItemCoin;
import bitevo.Zetal.LoMaS.Economy.Handlers.EconEventHandler;
import bitevo.Zetal.LoMaS.Economy.Item.ItemCoin;
import bitevo.Zetal.LoMaS.Economy.Message.BankMessage;
import bitevo.Zetal.LoMaS.Economy.Message.BankMessageHandler;
import bitevo.Zetal.LoMaS.Economy.TileEntity.TileEntitySafe;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.command.ServerCommandManager;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;

@Mod(modid = EconomyMod.modid, name = "Economy", version = "0.1")
public class EconomyMod
{
	public static final String modid = "lomas_econ";
	public static MinecraftServer server;
	
	@SidedProxy(clientSide = "bitevo.Zetal.LoMaS.Economy.EconClientProxy", serverSide = "bitevo.Zetal.LoMaS.Economy.EconCommonProxy")
	public static EconCommonProxy proxy = new EconCommonProxy();
	
	public static final EconEventHandler serverEventHandler = new EconEventHandler();
	
	public static Block safe = (new BlockSafe(Material.IRON)).setUnlocalizedName("safe").setCreativeTab(CreativeTabs.DECORATIONS);
	public static Block teller = (new BlockTeller(Material.GLASS)).setUnlocalizedName("teller").setCreativeTab(CreativeTabs.DECORATIONS);
	
	public static Item bronze_cc = (new ItemCoin()).setUnlocalizedName("bronze_cc").setCreativeTab(CreativeTabs.MATERIALS);
	public static Item silver_cc = (new ItemCoin()).setUnlocalizedName("silver_cc").setCreativeTab(CreativeTabs.MATERIALS);
	public static Item gold_cc = (new ItemCoin()).setUnlocalizedName("gold_cc").setCreativeTab(CreativeTabs.MATERIALS);
	public static SimpleNetworkWrapper snw;
	public static EconomyMod instance;
	
	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		NetworkRegistry.INSTANCE.registerGuiHandler(instance, proxy);
		
		LoMaS_Utils.registerItem(bronze_cc.setRegistryName( "bronze_cc"));
		LoMaS_Utils.registerItem(silver_cc.setRegistryName( "silver_cc"));
		LoMaS_Utils.registerItem(gold_cc.setRegistryName( "gold_cc"));
		
		LoMaS_Utils.registerBlock(safe.setRegistryName( "safe"));
		LoMaS_Utils.registerBlock(teller.setRegistryName( "teller"));

		EntityRegistry.registerModEntity(EntityItemCoin.class, "entityitemcoin", LoMaS_Utils.getNextEntityID(), EconomyMod.instance, 80, 3, false);
		
		GameRegistry.registerTileEntity(TileEntitySafe.class, "TileEntitySafe");
		GameRegistry.addRecipe(new ItemStack(safe, 1), new Object[] { "$$$", "$ $", "$$$", Character.valueOf('$'), Blocks.OBSIDIAN });	
	}

	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		this.instance = this;
		snw = NetworkRegistry.INSTANCE.newSimpleChannel(modid);
		snw.registerMessage(BankMessageHandler.class, BankMessage.class, 7786, Side.CLIENT);
		snw.registerMessage(BankMessageHandler.class, BankMessage.class, 7786, Side.SERVER);	
		proxy.registerRenderInformation();
	}
	
	@EventHandler
	public void serverStart(FMLServerStartingEvent event)
	{
		server = event.getServer();
		MinecraftForge.EVENT_BUS.register(this.serverEventHandler);
		
		ServerCommandManager scm = (ServerCommandManager) event.getServer().getCommandManager();
	}
	
	public static int getRawValue(ItemCoin coin)
	{
		if(coin == EconomyMod.bronze_cc)
		{
			return 1;
		}
		else if(coin == EconomyMod.silver_cc)
		{
			return 10;
		}
		else if(coin == EconomyMod.gold_cc)
		{
			return 100;
		}
		return 0;
	}
}
