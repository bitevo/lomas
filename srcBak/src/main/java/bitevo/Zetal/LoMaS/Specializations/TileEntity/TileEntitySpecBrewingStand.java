package bitevo.Zetal.LoMaS.Specializations.TileEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.potion.PotionHelper;
import net.minecraft.tileentity.TileEntityLockable;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockSpecBrewingStand;
import bitevo.Zetal.LoMaS.Specializations.Inventory.SpecContainerBrewingStand;

public class TileEntitySpecBrewingStand extends TileEntityLockable implements ITickable, ISidedInventory
{
	/** an array of the input slot indices */
	private static final int[] SLOTS_FOR_UP = new int[] { 3 };
	private static final int[] SLOTS_FOR_DOWN = new int[] { 0, 1, 2, 3 };
	/** an array of the output slot indices */
	private static final int[] OUTPUT_SLOTS = new int[] { 0, 1, 2, 4 };
	/** The ItemStacks currently placed in the slots of the brewing stand */
	private ItemStack[] brewingItemStacks = new ItemStack[5];
	private int brewTime;
	/** an integer with each bit specifying whether that slot of the stand contains a potion */
	private boolean[] filledSlots;
	/** used to check if the current ingredient has been removed from the brewing stand during brewing */
	private Item ingredientID;
	private String customName;
	private int fuel;

	private boolean[] isFresh = new boolean[] { false, false, false, false };
	private int modTime = 400;
	private byte brewerLevel = -1;

	/**
	 * Gets the name of this command sender (usually username, but possibly "Rcon")
	 */
	public String getName()
	{
		return this.hasCustomName() ? this.customName : "container.brewing";
	}

	/**
	 * Returns true if this thing is named
	 */
	public boolean hasCustomName()
	{
		return this.customName != null && this.customName.length() > 0;
	}

	public void func_145937_a(String p_145937_1_)
	{
		this.customName = p_145937_1_;
	}

	/**
	 * Returns the number of slots in the inventory.
	 */
	public int getSizeInventory()
	{
		return this.brewingItemStacks.length;
	}

	/**
	 * Updates the JList with a new model.
	 */
	public void update()
	{
		if (this.brewTime > 0)
		{
			--this.brewTime;

			if (this.brewTime == 0)
			{
				this.brewPotions();
				this.markDirty();
			}
			else if (!this.canBrew())
			{
				this.brewTime = 0;
				this.markDirty();
			}
			else if (this.ingredientID != this.brewingItemStacks[3].getItem())
			{
				this.brewTime = 0;
				this.markDirty();
			}
		}
		else if (this.canBrew())
		{
			modTime = 400;
			if (this.brewerLevel >= 1)
			{
				modTime = modTime - 20;
			}
			if (this.brewerLevel >= 3)
			{
				modTime = modTime - 20;
			}
			if (this.brewerLevel >= 7)
			{
				modTime = modTime - 32;
			}
			if (this.brewerLevel >= 7)
			{
				modTime = modTime - 8;
			}
			this.brewTime = modTime;
			this.ingredientID = this.brewingItemStacks[3].getItem();
		}

		if (!this.worldObj.isRemote)
		{
            boolean[] aboolean = this.createFilledSlotsArray();

			if (!Arrays.equals(aboolean, this.filledSlots))
			{
				this.filledSlots = aboolean;
				IBlockState iblockstate = this.worldObj.getBlockState(this.getPos());
				// System.out.println("1: " + this.worldObj.isRemote + " \n" + iblockstate + " \n" + this);

				if (!(iblockstate.getBlock() instanceof BlockSpecBrewingStand))
				{
					return;
				}

				for (int i = 0; i < BlockSpecBrewingStand.HAS_BOTTLE.length; ++i)
				{
					iblockstate = iblockstate.withProperty(BlockSpecBrewingStand.HAS_BOTTLE[i], Boolean.valueOf(aboolean[i]));
				}

				// System.out.println("2: " + this.worldObj.isRemote + " \n" + iblockstate + " \n" + this);
				this.worldObj.setBlockState(this.pos, iblockstate, 2);
			}
		}
	}
	
    public boolean[] createFilledSlotsArray()
    {
        boolean[] aboolean = new boolean[3];

        for (int i = 0; i < 3; ++i)
        {
            if (this.brewingItemStacks[i] != null)
            {
                aboolean[i] = true;
            }
        }

        return aboolean;
    }

	@Override
	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newState)
	{
		return !oldState.getBlock().equals(newState.getBlock());
	}

	public int getTotalTime()
	{
		modTime = 400;
		if (this.brewerLevel >= 1)
		{
			modTime = modTime - 20;
		}
		if (this.brewerLevel >= 3)
		{
			modTime = modTime - 20;
		}
		if (this.brewerLevel >= 7)
		{
			modTime = modTime - 32;
		}
		if (this.brewerLevel >= 7)
		{
			modTime = modTime - 8;
		}
		return this.modTime;
	}

	@SuppressWarnings("unused")
	private boolean canBrew()
	{
		if (this.brewingItemStacks[3] != null && this.brewingItemStacks[3].stackSize > 0 && false) // Code moved to net.minecraftforge.common.brewing.VanillaBrewingRecipe
		{
			ItemStack itemstack = this.brewingItemStacks[3];

			if (!PotionHelper.isReagent(itemstack))
			{
				return false;
			}
			else
			{
				for (int i = 0; i < 3; ++i)
				{
					ItemStack itemstack1 = this.brewingItemStacks[i];

					if (itemstack1 != null && PotionHelper.hasConversions(itemstack1, itemstack))
					{
						return true;
					}
				}

				return false;
			}
		}
		else
		{
			return net.minecraftforge.common.brewing.BrewingRecipeRegistry.canBrew(brewingItemStacks, brewingItemStacks[3], OUTPUT_SLOTS);
		}
	}

	private void brewPotions()
	{

        if (net.minecraftforge.event.ForgeEventFactory.onPotionAttemptBrew(brewingItemStacks)) return;
        ItemStack itemstack = this.brewingItemStacks[3];

        net.minecraftforge.common.brewing.BrewingRecipeRegistry.brewPotions(brewingItemStacks, brewingItemStacks[3], OUTPUT_SLOTS);

		Random rand = new Random();
		int retChance = 0;
		if (this.brewerLevel >= 2)
		{
			retChance = retChance + 5;
		}
		if (this.brewerLevel >= 6)
		{
			retChance = retChance + 2;
		}
		if (this.brewerLevel >= 7)
		{
			retChance = retChance + 3;
		}

		if (rand.nextInt(100) > (retChance))
		{
			--itemstack.stackSize;
		}
		
        BlockPos blockpos = this.getPos();

        if (itemstack.getItem().hasContainerItem(itemstack))
        {
            ItemStack itemstack1 = itemstack.getItem().getContainerItem(itemstack);

			int chance = 0;
			if (this.brewerLevel >= 4)
			{
				chance = chance + 2;
			}
			if (this.brewerLevel >= 5)
			{
				chance = chance + 3;
			}
			
			for(int i: OUTPUT_SLOTS)
			{

				if (rand.nextInt(100) < (chance))
				{
					if (this.brewingItemStacks[i].stackSize == 1 || (brewerLevel >= 10 && this.brewingItemStacks[i].stackSize < 4))
					{
						this.brewingItemStacks[i].stackSize++;
					}
				}

				/*if (brewerLevel >= 10)
				{
					int cDamage = this.brewingItemStacks[i].getItemDamage();
					int potionCount = this.brewingItemStacks[i].stackSize;
					ItemStack compactPotion = new ItemStack(SpecializationsMod.COMPACTPOTION, potionCount);
					compactPotion.setItemDamage(cDamage);
					this.brewingItemStacks[i] = compactPotion;
				}*/
				this.getIsFresh()[i] = true;
			}
			
            if (itemstack.stackSize <= 0)
            {
                itemstack = itemstack1;
            }
            else
            {
                InventoryHelper.spawnItemStack(this.worldObj, (double)blockpos.getX(), (double)blockpos.getY(), (double)blockpos.getZ(), itemstack1);
            }
        }

        if (itemstack.stackSize <= 0)
        {
            itemstack = null;
        }

        this.brewingItemStacks[3] = itemstack;
        this.worldObj.playEvent(1035, blockpos, 0);
        net.minecraftforge.event.ForgeEventFactory.onPotionBrewed(brewingItemStacks);
	}

	public void readFromNBT(NBTTagCompound compound)
	{
		super.readFromNBT(compound);
		NBTTagList nbttaglist = compound.getTagList("Items", 10);
		this.brewingItemStacks = new ItemStack[this.getSizeInventory()];

		for (int i = 0; i < nbttaglist.tagCount(); ++i)
		{
			NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
			byte b0 = nbttagcompound1.getByte("Slot");

			if (b0 >= 0 && b0 < this.brewingItemStacks.length)
			{
				this.brewingItemStacks[b0] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
			}
		}

		NBTTagList nbttaglist2 = compound.getTagList("isFresh", 10);

		for (int i = 0; i < nbttaglist2.tagCount(); ++i)
		{
			NBTTagCompound nbttagcompound2 = (NBTTagCompound) nbttaglist2.getCompoundTagAt(i);
			this.getIsFresh()[i] = nbttagcompound2.getBoolean("isFresh");
		}

		this.brewerLevel = compound.getByte("brewerLevel");
		this.brewTime = compound.getShort("BrewTime");

		if (compound.hasKey("CustomName", 8))
		{
			this.customName = compound.getString("CustomName");
		}
	}

	public NBTTagCompound writeToNBT(NBTTagCompound compound)
	{
		super.writeToNBT(compound);
		compound.setShort("BrewTime", (short) this.brewTime);
		compound.setByte("brewerLevel", this.brewerLevel);
		NBTTagList nbttaglist = new NBTTagList();

		for (int i = 0; i < this.brewingItemStacks.length; ++i)
		{
			if (this.brewingItemStacks[i] != null)
			{
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte) i);
				this.brewingItemStacks[i].writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}

		NBTTagList nbttaglist2 = new NBTTagList();

		for (int i = 0; i < this.brewingItemStacks.length; ++i)
		{
			if (this.brewingItemStacks[i] != null)
			{
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setBoolean("isFresh", this.getIsFresh()[i]);
				nbttaglist2.appendTag(nbttagcompound1);
			}
		}

		compound.setTag("Items", nbttaglist);
		compound.setTag("isFresh", nbttaglist2);

		if (this.hasCustomName())
		{
			compound.setString("CustomName", this.customName);
		}
		return compound;
	}

	/**
	 * Returns the stack in slot i
	 */
	public ItemStack getStackInSlot(int index)
	{
		return index >= 0 && index < this.brewingItemStacks.length ? this.brewingItemStacks[index] : null;
	}

	/**
	 * Removes from an inventory slot (first arg) up to a specified number (second arg) of items and returns them in a new stack.
	 */
	public ItemStack decrStackSize(int index, int count)
	{
		if (index >= 0 && index < this.brewingItemStacks.length)
		{
			ItemStack itemstack = this.brewingItemStacks[index];
			this.brewingItemStacks[index] = null;
			return itemstack;
		}
		else
		{
			return null;
		}
	}

	/**
	 * When some containers are closed they call this on each slot, then drop whatever it returns as an EntityItem - like when you close a workbench GUI.
	 */
	public ItemStack removeStackFromSlot(int index)
	{
		if (index >= 0 && index < this.brewingItemStacks.length)
		{
			ItemStack itemstack = this.brewingItemStacks[index];
			this.brewingItemStacks[index] = null;
			return itemstack;
		}
		else
		{
			return null;
		}
	}

	/**
	 * Sets the given item stack to the specified slot in the inventory (can be crafting or armor sections).
	 */
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		if (index >= 0 && index < this.brewingItemStacks.length)
		{
			this.brewingItemStacks[index] = stack;
		}
	}

	/**
	 * Returns the maximum stack size for a inventory slot. Seems to always be 64, possibly will be extended. *Isn't this more of a set than a get?*
	 */
	public int getInventoryStackLimit()
	{
		return 64;
	}

	/**
	 * Do not make give this method the name canInteractWith because it clashes with Container
	 */
	public boolean isUseableByPlayer(EntityPlayer player)
	{
		return this.worldObj.getTileEntity(this.pos) != this ? false : player.getDistanceSq((double) this.pos.getX() + 0.5D, (double) this.pos.getY() + 0.5D, (double) this.pos.getZ() + 0.5D) <= 64.0D;
	}

	public void openInventory(EntityPlayer player)
	{
	}

	public void closeInventory(EntityPlayer player)
	{
	}

	/**
	 * Returns true if automation is allowed to insert the given stack (ignoring stack size) into the given slot.
	 */
    public boolean isItemValidForSlot(int index, ItemStack stack)
    {
        if (index == 3)
        {
            return net.minecraftforge.common.brewing.BrewingRecipeRegistry.isValidIngredient(stack);
        }
        else
        {
            Item item = stack.getItem();
            return index == 4 ? item == Items.BLAZE_POWDER : net.minecraftforge.common.brewing.BrewingRecipeRegistry.isValidInput(stack);
        }
    }

	public int[] getSlotsForFace(EnumFacing side)
	{
        return side == EnumFacing.UP ? SLOTS_FOR_UP : (side == EnumFacing.DOWN ? SLOTS_FOR_DOWN : OUTPUT_SLOTS);
	}

	/**
	 * Returns true if automation can insert the given item in the given slot from the given side. Args: slot, item, side
	 */
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction)
	{
		return this.isItemValidForSlot(index, itemStackIn);
	}

	/**
	 * Returns true if automation can extract the given item in the given slot from the given side. Args: slot, item, side
	 */
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction)
	{
		return true;
	}

	public String getGuiID()
	{
		return "minecraft:brewing_stand";
	}

	public Container createContainer(InventoryPlayer playerInventory, EntityPlayer playerIn)
	{
		return new SpecContainerBrewingStand(playerInventory, this);
	}

	public int getField(int id)
	{
		switch (id)
		{
			case 0:
				return this.brewTime;
			case 1:
				return this.booleansToInt(this.getIsFresh());
			case 2:
				return this.brewerLevel;
			default:
				return 0;
		}
	}

	public void setField(int id, int value)
	{
		switch (id)
		{
			case 0:
				this.brewTime = value;
			case 1:
				this.setIsFresh(this.intToBooleans(value, this.getIsFresh().length));
			case 2:
				this.brewerLevel = (byte) value;
			default:
		}
	}

	public int getFieldCount()
	{
		return 3;
	}

	public void clear()
	{
		for (int i = 0; i < this.brewingItemStacks.length; ++i)
		{
			this.brewingItemStacks[i] = null;
		}
	}

	public boolean[] getIsFresh()
	{
		return isFresh;
	}

	public void setIsFresh(boolean[] isFresh)
	{
		this.isFresh = isFresh;
	}

	public static void printArray(boolean[] hi)
	{
		String complete = "[";
		for (int i = 0; i < hi.length; i++)
		{
			complete = complete + hi[i];
			if (i + 1 != hi.length)
			{
				complete = complete + ", ";
			}
		}
		complete = complete + "]";
		System.out.println(complete);
	}

	public static int booleansToInt(boolean[] arr)
	{
		int n = 0;
		for (boolean b : arr)
			n = (n << 1) | (b ? 1 : 0);
		return n;
	}

	public static boolean[] intToBooleans(int number, int length)
	{
		boolean[] ret = new boolean[length];
		for (int i = 0; i < length; i++)
		{
			ret[length - 1 - i] = (1 << i & number) != 0;
		}
		return ret;
	}
}