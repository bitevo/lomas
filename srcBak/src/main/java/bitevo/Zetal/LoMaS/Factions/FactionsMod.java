package bitevo.Zetal.LoMaS.Factions;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import bitevo.Zetal.LoMaS.Factions.Block.BlockDoubleRoadSlab;
import bitevo.Zetal.LoMaS.Factions.Block.BlockHalfRoadSlab;
import bitevo.Zetal.LoMaS.Factions.Block.BlockRoad;
import bitevo.Zetal.LoMaS.Factions.Block.BlockRoadSlab;
import bitevo.Zetal.LoMaS.Factions.Block.BlockRoadStairs;
import bitevo.Zetal.LoMaS.Factions.Handler.FactionEventHandler;
import bitevo.Zetal.LoMaS.Factions.Items.ItemRoadSlab;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = FactionsMod.modid, name = "Factions", version = "0.1")
public class FactionsMod
{
	public static final String modid = "lomas_factions";
	@SidedProxy(clientSide = "bitevo.Zetal.LoMaS.Factions.FactionsClientProxy", serverSide = "bitevo.Zetal.LoMaS.Factions.FactionsCommonProxy")
	public static FactionsCommonProxy proxy = new FactionsCommonProxy();
	@Instance(modid)
	public static FactionsMod instance;

	public static final Block ROAD = new BlockRoad().setUnlocalizedName("road").setHardness(3.0F);
	public static final Block OBELISK = new Block(Material.ROCK).setUnlocalizedName("obelisk").setHardness(-1.0F).setBlockUnbreakable().setResistance(6000000.0F).setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
	public static final BlockRoadStairs ROAD_STAIRS = (BlockRoadStairs) (new BlockRoadStairs(ROAD.getDefaultState())).setHardness(3.0F).setCreativeTab(CreativeTabs.BUILDING_BLOCKS).setUnlocalizedName("road_stairs");
	public static final Block ROAD_SLAB = (BlockRoadSlab) (new BlockHalfRoadSlab()).setHardness(3.0F).setUnlocalizedName("road_slab");
	public static final Block ROAD_DOUBLE_SLAB = (BlockRoadSlab) (new BlockDoubleRoadSlab()).setHardness(3.0F).setUnlocalizedName("road_double_slab");
	
	public static final Item OBELISK_CORE = new Item().setUnlocalizedName("obelisk_core").setCreativeTab(CreativeTabs.MATERIALS);
	
	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		NetworkRegistry.INSTANCE.registerGuiHandler(instance, proxy);
		MinecraftForge.EVENT_BUS.register(new FactionEventHandler());
		LoMaS_Utils.registerBlock(OBELISK.setRegistryName( "obelisk"));
		LoMaS_Utils.registerBlock(ROAD.setRegistryName( "road"));
		LoMaS_Utils.registerBlock(ROAD_STAIRS.setRegistryName( "road_stairs"));
		LoMaS_Utils.registerBlock(ROAD_SLAB.setRegistryName( "road_slab"));
		GameRegistry.register(ROAD_DOUBLE_SLAB.setRegistryName( "road_double_slab"));
		
		LoMaS_Utils.registerItem(OBELISK_CORE);

		ItemRoadSlab.singleSlab = (BlockSlab) ROAD_SLAB;
		ItemRoadSlab.doubleSlab = (BlockSlab) ROAD_DOUBLE_SLAB;
		
		doRecipes();
	}
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent evt)
	{
		proxy.registerRenderInformation();
	}

	public void doRecipes()
	{
	}

	public void registerModEntityWithEgg(Class parEntityClass, int eID, String parEntityName, int parEggColor, int parEggSpotsColor)
	{
		EntityRegistry.registerModEntity(parEntityClass, parEntityName, eID, FactionsMod.instance, 80, 3, false);
		EntityRegistry.registerEgg(parEntityClass, parEggColor, parEggSpotsColor);
	}
}
