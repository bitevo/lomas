package bitevo.Zetal.LoMaS.Specializations.Handler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Animal;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import bitevo.Zetal.LoMaS.Shared.Entity.EntityFactionMember;
import bitevo.Zetal.LoMaS.Specializations.FoodInitializer;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockSapling;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockSpecAnvil;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityCustomArrow;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityCustomXPOrb;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityDiamondShard;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityFactionGolem;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntitySpecialFishHook;
import bitevo.Zetal.LoMaS.Specializations.Item.ItemFood;
import bitevo.Zetal.LoMaS.Specializations.Item.ItemIronFishingRod;
import bitevo.Zetal.LoMaS.Specializations.Item.ItemSapBottle;
import bitevo.Zetal.LoMaS.Specializations.Message.AnimalMessage;
import bitevo.Zetal.LoMaS.Specializations.Message.ArrowMessage;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntityPlant;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySpecBrewingStand;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySpecFurnace;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAnvil;
import net.minecraft.block.BlockEnchantmentTable;
import net.minecraft.block.BlockFlower;
import net.minecraft.block.BlockGrass;
import net.minecraft.block.BlockLog;
import net.minecraft.block.BlockObsidian;
import net.minecraft.block.BlockOre;
import net.minecraft.block.BlockRedstoneOre;
import net.minecraft.block.BlockTallGrass;
import net.minecraft.block.BlockWorkbench;
import net.minecraft.block.IGrowable;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIMate;
import net.minecraft.entity.ai.EntityAITasks.EntityAITaskEntry;
import net.minecraft.entity.ai.EntityAITempt;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.entity.passive.EntityMooshroom;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntityFishHook;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.MobEffects;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemShield;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.potion.PotionEffect;
import net.minecraft.potion.PotionUtils;
import net.minecraft.stats.StatList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityBrewingStand;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.DamageSource;
import net.minecraft.util.FoodStats;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraftforge.common.IShearable;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingJumpEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.ArrowNockEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.EntityInteract;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.ExplosionEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.ReflectionHelper;

public class SCMEventHandler
{
	private Random rand = new Random();
	private long lastRightClickTick = -1L;
	List<Item> cureStacks = new ArrayList<Item>()
	{
		{
			add(FoodInitializer.MUSHROOM_STEW);
			add(FoodInitializer.GOLDEN_CARROT);
			add(Items.GOLDEN_APPLE);
		}
	};
	
	@SubscribeEvent
	public void itemTooltip(ItemTooltipEvent event)
	{
		ItemStack itemstack = event.getItemStack();
		if (FMLHandler.isTool(itemstack) || itemstack.getItem() instanceof ItemArmor)
		{
			SpecializationsMod.addInformation(event.getItemStack(), event.getEntityPlayer(), event.getToolTip(), event.isShowAdvancedItemTooltips());
		}

		if (itemstack.getItem() instanceof ItemPotion && itemstack.getMaxStackSize() > 1)
		{
			event.getToolTip().add(TextFormatting.GOLD + "Compact");
		}
	}

	@SubscribeEvent(priority = EventPriority.HIGH)
	public void breakSpeedEvent(PlayerEvent.BreakSpeed event)
	{
		float f = event.getNewSpeed();
		Block block = event.getState().getBlock();
		EntityPlayer player = event.getEntityPlayer();
		LoMaS_Player lPlayer = LoMaS_Player.getLoMaSPlayer(player);
		Material blockMat = event.getState().getMaterial();
		float multi = 0.8f;
		float miningMulti = lPlayer.getSkills().getPercentMagnitudeFromName("Mining spd");
		float axeMulti = lPlayer.getSkills().getPercentMagnitudeFromName("Axe spd");
		float punchMulti = lPlayer.getSkills().getPercentMagnitudeFromName("Punch eff");

		if (isPlayerUnarmed(event.getEntityPlayer()))
		{
			multi += punchMulti;
			if (lPlayer.getSkills().getMatchingNodesFromName("Stone Fist") >= 1)
			{
				if (isPlayerUnarmed(player))
				{
					int reqLevel = block.getHarvestLevel(event.getState());
					int stoneFist = ToolMaterial.STONE.getHarvestLevel();
					if (stoneFist >= reqLevel)
					{
						f += ToolMaterial.STONE.getEfficiencyOnProperMaterial();
					}
				}
			}
		}

		if ((block instanceof BlockLog || blockMat == Material.WOOD))
		{
			multi += axeMulti;
		}

		if (block instanceof BlockOre || blockMat == Material.ROCK || blockMat == Material.ANVIL || blockMat == Material.IRON || blockMat == Material.GROUND || blockMat == Material.CLAY || blockMat == Material.GRASS || blockMat == Material.ICE || blockMat == Material.SAND)
		{
			multi += miningMulti;
		}

		f *= multi;

		event.setNewSpeed(f);
	}

	@SubscribeEvent(priority = EventPriority.HIGH)
	public void harvestCheck(PlayerEvent.HarvestCheck event)
	{
		EntityPlayer player = event.getEntityPlayer();
		IBlockState state = event.getTargetBlock();
		Block block = state.getBlock();
		LoMaS_Player lPlayer = LoMaS_Player.getLoMaSPlayer(player);
		if (lPlayer.getSkills().getMatchingNodesFromName("Stone Fist") >= 1)
		{
			if (isPlayerUnarmed(player))
			{
				int reqLevel = block.getHarvestLevel(state);
				int stoneFist = ToolMaterial.STONE.getHarvestLevel();
				if (stoneFist >= reqLevel)
				{
					event.setCanHarvest(true);
				}
			}
		}
	}

	@SubscribeEvent
	public void onHarvest(BlockEvent.HarvestDropsEvent event)
	{
		if (event.getHarvester() != null && !event.getHarvester().capabilities.isCreativeMode)
		{
			EntityPlayer player = event.getHarvester();
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
			Block block = event.getState().getBlock();
			World world = event.getWorld();
			BlockPos pos = event.getPos();
			IBlockState state = event.getState();
			int fortune = event.getFortuneLevel();
			float progressEarned = SpecializationsMod.getProgress(player, lplayer, block, event);
			if (progressEarned > 0.0f)
			{
				lplayer.addSpecProgress(progressEarned);
				player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
			}

			if (block instanceof BlockOre || block instanceof BlockRedstoneOre || block instanceof BlockObsidian)
			{
				// Chance for a completely extra drop!!!
				float chance = 0;
				chance = chance + lplayer.getSkills().getMagnitudeFromName("Chance for double ores");
				if (player.getRNG().nextInt(100) < chance)
				{
					LoMaS_Utils.addChatMessage(player, ("\u00A72Your honed abilities improved the yield of that ore!"));
					event.getDrops().addAll(block.getDrops(world, pos, state, fortune));
				}
				/*
				 * else if (randy < 50) { int amt = world.rand.nextInt(2) + 1; ItemStack itemstack = null; if (!(block instanceof BlockObsidian) && !(block.equals(Blocks.QUARTZ_ORE))) { itemstack = new ItemStack(Blocks.COBBLESTONE, amt); } else if (block.equals(Blocks.QUARTZ_ORE)) { itemstack = new ItemStack(Blocks.NETHERRACK, amt); } player.addChatMessage(new TextComponentTranslation("\u00a7cYour unskilled hands have destroyed the valuable ore.")); if (itemstack != null) { event.getDrops().clear(); event.getDrops().add(itemstack); } }
				 */
			}
			else if (block instanceof BlockLog)
			{
				// Chance for a completely extra drop!!!
				float chance = 0;
				chance = chance + lplayer.getSkills().getMagnitudeFromName("Chance for double logs");
				if (player.getRNG().nextInt(100) < chance)
				{
					LoMaS_Utils.addChatMessage(player, ("\u00A72Your honed abilities improved the yield of that log!"));
					event.getDrops().addAll(block.getDrops(world, pos, state, fortune));
				}
				/*
				 * else if (randy < 50) { int amt = world.rand.nextInt(5) + 4; ItemStack itemstack = null; if (event.getState().getBlock() == Blocks.LOG) { BlockPlanks.EnumType type = ((BlockPlanks.EnumType) event.getState().getValue(BlockOldLog.VARIANT)); itemstack = LoMaS_Utils.BFM ? new ItemStack(BigFMod.STICK, amt, type.getMetadata()) : new ItemStack(Items.STICK, amt); } else if (event.getState().getBlock() == Blocks.LOG2) { BlockPlanks.EnumType type = ((BlockPlanks.EnumType) event.getState().getValue(BlockNewLog.VARIANT)); itemstack = LoMaS_Utils.BFM ? new ItemStack(BigFMod.STICK, amt, type.getMetadata()) : new ItemStack(Items.STICK, amt); } player.addChatMessage(new TextComponentTranslation("\u00a7cYour unskilled hands have destroyed the log.")); event.getDrops().clear(); event.getDrops().add(itemstack); }
				 */
			}
		}
	}

	@SubscribeEvent
	public void onEntityMove(LivingUpdateEvent event)
	{
		if (event.getEntity() != null && event.getEntity() instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) event.getEntity();
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
			World world = event.getEntity().worldObj;

			if (player != null && lplayer != null)
			{
				/**
				 * Rain Exhaustion
				 */
				if (player.isWet() && player.ticksExisted % 120 == 0 && player.getRidingEntity() == null)
				{
					player.addExhaustion(0.1f);
				}

				/**
				 * Active Ability Logic
				 */
				lplayer.secCounter = lplayer.counter / 20;
				if ((lplayer.secCounter <= LoMaS_Player.abilCooldown && lplayer.secCounter != 0) || (lplayer.secCounter == 0 && lplayer.isActive == true))
				{
					lplayer.counter++;
					// System.out.println(player.counter);
					if (lplayer.counter % 20 == 0)
					{
						// System.out.println((player.secCounter));
						if (lplayer.secCounter > 5)
						{
							lplayer.isActive = false;
						}
						if (lplayer.secCounter >= LoMaS_Player.abilCooldown)
						{
							lplayer.counter = 0;
						}
					}
				}

				/**
				 * Adrenaline Rush Healing
				 */
				if (lplayer.getSkills().getMatchingNodesFromName("Adrenaline Rush") >= 1 && lplayer.isActive && player.getHealth() < player.getMaxHealth() && player.ticksExisted % 5 * 12 == 0)
				{
					player.heal(1);
				}

				/**
				 * 'Natural' Healing Logic
				 */
				float healTime = 120.0f * (1.0f - lplayer.getSkills().getPercentMagnitudeFromName("Regen"));
				int modulo = (int) ((20.0f * healTime));
				if (player.getFoodStats().getFoodLevel() > 0 && player.getHealth() < player.getMaxHealth() && (player.ticksExisted % modulo) == modulo - 1)
				{
					player.heal(1.0F);
					player.getFoodStats().addExhaustion(1.0f);
				}

				if (lplayer.progress > lplayer.getReqProgress())
				{
					lplayer.addSpecProgress(0);
				}

				if (!player.worldObj.isRemote && lplayer.notYet && ((EntityPlayerMP) player).connection != null)
				{
					((EntityPlayerMP) player).sendContainerToPlayer(player.inventoryContainer);
					player.inventoryContainer.detectAndSendChanges();
					lplayer.notYet = false;
				}

				/**
				 * Tree Climbing Ticking
				 */
				if (lplayer.getSkills().getMatchingNodesFromName("Tree Climbing") >= 1 && SpecializationsMod.isTouchingLog(player))
				{
					float f5 = 0.15F;

					if (player.motionY < -0.15D)
					{
						player.motionY = -0.15D;
					}

					boolean flag = player.isSneaking();

					if (flag && player.motionY < 0.0D)
					{
						player.motionY = 0.0D;
					}

					if (player.isCollidedHorizontally)
					{
						player.motionY = 0.1D;
					}

					player.motionX = MathHelper.clamp_double(player.motionX, (double) (-f5), (double) f5);
					player.motionZ = MathHelper.clamp_double(player.motionZ, (double) (-f5), (double) f5);
					player.fallDistance = 0.0F;

					player.moveEntity(player.motionX, player.motionY, player.motionZ);
				}

				/**
				 * Food Exhaustion Logic
				 */
				float foodExhaustionLevel = this.getExhaustion(player);
				if (lplayer.prevfoodExhaustionLevel != -1.0f && foodExhaustionLevel != lplayer.prevfoodExhaustionLevel)
				{
					// If diff > 0, exhaustion increased. If diff < 0, it decreased.
					float diff = foodExhaustionLevel - lplayer.prevfoodExhaustionLevel;
					if (diff > 0)
					{
						float reduction = 6.0f;
						reduction *= (1.0f - lplayer.getSkills().getPercentMagnitudeFromName("Reduced exh"));
						player.addExhaustion(diff * reduction);
						lplayer.prevfoodExhaustionLevel = this.getExhaustion(player);
					}
				}

				/**
				 * Range Updates
				 */
				/*
				 * if(player instanceof EntityPlayerMP) { EntityPlayerMP emp = (EntityPlayerMP) player; double reach = emp.interactionManager.getBlockReachDistance(); System.out.println("hi1 " + reach); ItemStack stack = player.getActiveItemStack() == null ? player.getHeldItemOffhand() == null ? player.getHeldItemMainhand() : player.getHeldItemOffhand() : player.getActiveItemStack(); if(stack != null && stack.getItem() != null) { if(stack.getItem() instanceof ItemAxe) { reach += (lplayer.getSkills().getMagnitudeFromName("Axe range")); System.out.println("hi2 " + reach); } } reach += (lplayer.getSkills().getMagnitudeFromName("Interaction range")); emp.interactionManager.setBlockReachDistance(reach); System.out.println("hi3 " + reach); }
				 */

				/**
				 * Iron Legs
				 */
				if (lplayer.getSkills().getMatchingNodesFromName("Iron Legs") >= 1)
				{
					player.stepHeight = 1.2f;
				}
				else
				{
					player.stepHeight = 0.6f;
				}

				/**
				 * Dark Vision
				 */
				BlockPos pos = new BlockPos(Math.floor(player.posX), player.posY + 0.5D, Math.floor(player.posZ));
				if (!world.isRemote && lplayer.getSkills().getMatchingNodesFromName("Dark Vision") >= 1)
				{
					boolean isActive = true;
					boolean isDark = !world.isDaytime() || world.getLight(player.getPosition()) <= 2.0f;
					if (isDark && isActive)
					{
						player.addPotionEffect(new PotionEffect(MobEffects.NIGHT_VISION, 15, -1, true, false));
					}
				}

				/**
				 * Slow Resistance
				 */
				if (lplayer.getSkills().getMatchingNodesFromName("Slow Resistance") >= 1)
				{
					double minSpeed = player.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getBaseValue();
					float strafe = player.moveStrafing;
					float forward = player.moveForward;
					float g = strafe * strafe + forward * forward;
					if (g >= 1.0E-4F)
					{
						g = MathHelper.sqrt_float(g);

						if (g < 1.0F)
						{
							g = 1.0F;
						}
						
						g = (float) ((minSpeed * 1.5D) / g);
						strafe = strafe * g;
						forward = forward * g;
						float f1 = MathHelper.sin(player.rotationYaw * 0.017453292F);
						float f2 = MathHelper.cos(player.rotationYaw * 0.017453292F);
						double mX = (double) (strafe * f2 - forward * f1);
						double mZ = (double) (forward * f2 + strafe * f1);
						
						if(Math.abs(player.motionX) <= Math.abs(mX))
						{
							player.motionX = mX;
						}
						
						if(Math.abs(player.motionZ) <= Math.abs(mZ))
						{
							player.motionZ = mZ;
						}
					}
				}

				/**
				 * Sprint Speed
				 */
				AttributeModifier speed = new AttributeModifier(new UUID(77777775L, 88888886L), "sprint speed", 0.0D, 2).setSaved(false);
				if (player.isSprinting())
				{
					double amount = 0.0D;
					amount = amount + lplayer.getSkills().getPercentMagnitudeFromName("Sprint speed");
					speed = new AttributeModifier(new UUID(77777775L, 88888886L), "sprint speed", amount, 2).setSaved(false);
					player.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).removeModifier(speed);
					player.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).applyModifier(speed);
				}
				else
				{
					player.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).removeModifier(speed);
				}

				/**
				 * Reduced Sprint Hunger
				 */
				if (player.isSprinting())
				{
					double dX = player.prevPosX - player.posX;
					double dZ = player.prevPosZ - player.posZ;
					int k = Math.round(MathHelper.sqrt_double(dX * dX + dZ * dZ) * 100.0F);
					float foodExh = ReflectionHelper.getPrivateValue(FoodStats.class, player.getFoodStats(), 2);
					float subtractExh = 0.099999994F * (float) k * 0.01F;
					subtractExh *= lplayer.getSkills().getPercentMagnitudeFromName("Reduced sprint hunger");
					ReflectionHelper.setPrivateValue(FoodStats.class, player.getFoodStats(), foodExh - subtractExh, 2);
				}

				/**
				 * Potion Effect
				 */
				Collection<PotionEffect> effects = player.getActivePotionEffects();
				Collection<PotionEffect> removeEffects = new ArrayList<PotionEffect>();
				int badCount = 0;
				for (PotionEffect effect : effects)
				{
					if (effect.getPotion().isBadEffect())
					{
						badCount++;

						if (badCount > 1)
						{
							removeEffects.add(effect);
						}

						/**
						 * Negative Potion Duration
						 */
						if (lplayer.getSkills().getPercentMagnitudeFromName("Red. neg. pot. dura.") >= 1)
						{
							try
							{
								ReflectionHelper.findMethod(PotionEffect.class, effect, new String[] { "deincrementDuration" }).invoke(effect);
							}
							catch (Exception e)
							{
								e.printStackTrace();
							}
						}
					}
				}

				/**
				 * Limit One Negative Potion Effect
				 */
				if (lplayer.getSkills().getMatchingNodesFromName("Limit one negative potion effect") >= 1)
				{
					for (PotionEffect effect : removeEffects)
					{
						player.removePotionEffect(effect.getPotion());
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onJump(LivingJumpEvent event)
	{
		Entity entity = event.getEntity();
		if (entity != null && entity instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) event.getEntity();
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);

			if (player != null && lplayer != null)
			{
				/**
				 * Iron Legs
				 */
				if (lplayer.getSkills().getMatchingNodesFromName("Iron Legs") >= 1)
				{
					player.motionY += 0.21f;
				}
			}
		}
	}

	@SubscribeEvent
	public void onFallDamage(LivingFallEvent event)
	{
		if (event.getEntityLiving() instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) event.getEntityLiving();
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
			if (lplayer != null)
			{
				if (lplayer.getSkills().getMatchingNodesFromName("Iron Legs") >= 1)
				{
					event.setDistance(event.getDistance() - 3.0f);
				}

				if (lplayer.getSkills().getMatchingNodesFromName("Tree Climbing") >= 1 && SpecializationsMod.isTouchingLog(player))
				{
					event.setDamageMultiplier(0.0f);
				}
			}
		}
	}

	@SubscribeEvent
	public void onPlace(BlockEvent.PlaceEvent event)
	{
		if (event.getPlacedBlock().getBlock() instanceof BlockOre && !event.getPlayer().isCreative())
		{
			event.setCanceled(true);
			return;
		}

		EntityPlayer player = event.getPlayer();
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
		player.addExhaustion(0.1f);
		ItemStack stack = event.getItemInHand();
		World world = event.getWorld();
		// System.out.println(event.getPlacedBlock().getBlock());
		if (event.getPlacedBlock().getBlock().equals(Blocks.FARMLAND))
		{
			world.setBlockState(event.getPos(), FoodInitializer.FARMLAND.getDefaultState());
		}
	}

	@SubscribeEvent(priority = EventPriority.HIGH)
	public void rightClick(PlayerInteractEvent.RightClickBlock event)
	{
		BlockPos pos = event.getPos();
		World world = event.getWorld();
		IBlockState state = world.getBlockState(pos);
		Block b = state.getBlock();
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(event.getEntityPlayer());
		// System.out.println(world.getBlockState(pos) + " " + world.getTileEntity(pos));
		if (event.getWorld().getTileEntity(event.getPos()) != null)
		{
			TileEntity tileentity = world.getTileEntity(pos);
			if (tileentity instanceof TileEntityBrewingStand && !(tileentity instanceof TileEntitySpecBrewingStand))
			{
				world.setBlockState(pos, SpecializationsMod.BREWING_STAND.getDefaultState());
				System.out.println("Converted old: " + world.getTileEntity(pos));
			}

			if (tileentity instanceof TileEntityFurnace && !(tileentity instanceof TileEntitySpecFurnace))
			{
				world.setBlockState(pos, SpecializationsMod.FURNACE.getDefaultState());
				System.out.println("Converted old: " + world.getTileEntity(pos));
			}

			if (tileentity instanceof TileEntityPlant)
			{
				TileEntityPlant tep = (TileEntityPlant) tileentity;
				if (lplayer.getSkills().getMatchingNodesFromName("Farmer Sense") >= 1)
				{
					if (lplayer.player.ticksExisted > this.lastRightClickTick && !world.isRemote)
					{
						if (tep.getQuality() < 75)
						{
							LoMaS_Utils.addChatMessage(lplayer.player, ("This plant is looking pretty poor... " + tep.getQuality()));
						}
						else if (tep.getQuality() <= 100)
						{
							LoMaS_Utils.addChatMessage(lplayer.player, ("This plant is starting to look pretty decent. " + tep.getQuality()));
						}
						else if (tep.getQuality() <= 150)
						{
							LoMaS_Utils.addChatMessage(lplayer.player, ("This plant looks absolutely superb! " + tep.getQuality()));
						}
						this.lastRightClickTick = lplayer.player.ticksExisted;
					}
				}
			}
		}

		if (state.getBlock() instanceof BlockAnvil)
		{
			if (!(state.getBlock() instanceof BlockSpecAnvil))
			{
				world.setBlockState(pos, SpecializationsMod.ANVIL.getDefaultState());
				System.out.println("Converted old: " + world.getTileEntity(pos));
			}

			/*
			 * if (!world.isRemote) { if (lplayer != null && lplayer.getSkills().getMatchingNodesFromName("Anvil repair") >= 1) { ((EntityPlayerMP) event.getEntityPlayer()).displayGui(new SpecContainerRepair.Anvil(world, pos)); } else { if (lplayer.player.ticksExisted > this.lastRightClickTick && !world.isRemote) { event.getEntityPlayer().addChatMessage(new TextComponentTranslation("You aren't trained to use this.")); this.lastRightClickTick = lplayer.player.ticksExisted; } } event.setCanceled(true); }
			 */
		}

		if (state.getBlock() instanceof BlockEnchantmentTable)
		{
			if (lplayer == null || lplayer.getSkills().getMatchingNodesFromName("Enchanting") == 0)
			{
				if (lplayer.player.ticksExisted > this.lastRightClickTick && !world.isRemote)
				{
					LoMaS_Utils.addChatMessage(event.getEntityPlayer(), ("You aren't trained to use this."));
					this.lastRightClickTick = lplayer.player.ticksExisted;
				}
				event.getEntityPlayer().closeScreen();
				event.setCanceled(true);
			}
		}

		if (state.getBlock() instanceof BlockWorkbench)
		{
			world.setBlockState(pos, SpecializationsMod.CRAFTING_TABLE.getDefaultState());
		}

		ItemStack stack = event.getEntityPlayer().getHeldItemOffhand() != null ? event.getEntityPlayer().getHeldItemOffhand() : event.getEntityPlayer().getHeldItemMainhand();
		if (stack != null && event.getUseItem() != Event.Result.DENY)
		{
			/**
			 * Bonemeal? Cancel it.
			 */
			if (stack.getItem() instanceof ItemDye && b instanceof IGrowable && stack.getItemDamage() == 15)
			{
				float treeOvergrowth = lplayer.getSkills().getMagnitudeFromName("Tree Overgrowth");
				if(treeOvergrowth > 0 && b instanceof BlockSapling)
				{
					if(treeOvergrowth >= rand.nextInt(100))
					{
						((BlockSapling) b).generateTree(world, pos, state, rand, 0);
						ItemDye.spawnBonemealParticles(world, pos, 5);
					}
					--stack.stackSize;
					event.setCanceled(true);
				}
				// Check to make sure it's not something we want to be bonemealable
				else if(!(b instanceof BlockFlower) && !(b instanceof BlockGrass) && !(b instanceof BlockTallGrass))
				{
					event.setCanceled(true);
				}
			}
			/**
			 * Is Sap on Sapling?
			 */
			if(stack.getItem() instanceof ItemSapBottle)
			{
				float sapMutation = lplayer.getSkills().getMagnitudeFromName("Sapling Mutation");
				if(sapMutation > 0 && b instanceof BlockSapling)
				{
					if(sapMutation >= rand.nextInt(100))
					{
						world.setBlockState(pos, b.getStateFromMeta(rand.nextInt(5)), 3);
						ItemDye.spawnBonemealParticles(world, pos, 5);
					}
					--stack.stackSize;
				}
			}
		}
	}

	@SubscribeEvent
	public void playerAttacking(AttackEntityEvent event)
	{
		EntityPlayer player = event.getEntityPlayer();
		this.doAttackSpeedMod(player);
	}

	@SubscribeEvent
	public void emptyLeftClick(PlayerInteractEvent.LeftClickEmpty event)
	{
		EntityPlayer player = event.getEntityPlayer();
		this.doAttackSpeedMod(player);
	}

	public static void doAttackSpeedMod(EntityPlayer player)
	{
		if (player != null)
		{
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
			double amount = 0.0D;
			if(player.getHeldItemMainhand() != null && player.getHeldItemMainhand().getItem() != null)
			{
				if (player.getHeldItemMainhand().getItem() instanceof ItemSword)
				{
					amount = amount + lplayer.getSkills().getPercentMagnitudeFromName("Swd spd");
				}
				if (player.getHeldItemMainhand().getItem() instanceof ItemAxe)
				{
					amount = amount + lplayer.getSkills().getPercentMagnitudeFromName("Axe spd");
				}
			}
			AttributeModifier speed = new AttributeModifier(new UUID(77777776L, 88888887L), "swingspd", amount, 2).setSaved(false);
			player.getEntityAttribute(SharedMonsterAttributes.ATTACK_SPEED).removeModifier(speed);
			player.getEntityAttribute(SharedMonsterAttributes.ATTACK_SPEED).applyModifier(speed);
		}
	}

	@SubscribeEvent
	public void entityAttacked(LivingHurtEvent event)
	{
		Entity targetEntity = event.getEntity();
		Entity attackerEntity = event.getSource().getEntity();
		Entity originalAttacker = attackerEntity;
		DamageSource damageSrc = event.getSource();
		/**
		 * Fixes arrows
		 */
		if (attackerEntity instanceof EntityCustomArrow)
		{
			attackerEntity = ((EntityCustomArrow) attackerEntity).shootingEntity;
		}
		else if (attackerEntity instanceof EntityDiamondShard)
		{
			attackerEntity = ((EntityDiamondShard) attackerEntity).shootingEntity;
		}
			

		/**
		 * TargetEntity is Living
		 */
		if (targetEntity instanceof EntityLivingBase)
		{
			EntityLivingBase target = (EntityLivingBase) targetEntity;
			float tempDamg = event.getAmount();
			
			/**
			 * Attacker is player
			 */
			if (attackerEntity instanceof EntityPlayer)
			{
				EntityPlayer attacker = (EntityPlayer) attackerEntity;
				LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(attacker);

				tempDamg = SpecializationsMod.getAttackerDamage(event, target, attacker);

				if (target instanceof EntityLivingBase && tempDamg >= ((EntityLivingBase) target).getHealth() && tempDamg >= 3)
				{
					float progressEarned = 20.0f;
					if (target instanceof EntityMob)
					{
						progressEarned = 25.0f;
					}
					if (target instanceof EntityAnimal)
					{
						progressEarned = 35.0f;
					}
					if (!attacker.capabilities.isCreativeMode)
					{
						lplayer.addSpecProgress(progressEarned);
						attacker.worldObj.playSound((EntityPlayer) null, attacker.posX, attacker.posY, attacker.posZ, SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, SoundCategory.PLAYERS, 0.1F, 0.5F * ((this.rand.nextFloat() - this.rand.nextFloat()) * 0.7F + 1.8F));
					}
				}

				// held.getAttributeModifiers(EntityEquipmentSlot.MAINHAND).get(SharedMonsterAttributes.ATTACK_DAMAGE.getAttributeUnlocalizedName());
				if(this.isPlayerUnarmed(attacker))
				{
					float multi = 1.0f - lplayer.getSkills().getPercentMagnitudeFromName("Punch eff");
					target.hurtResistantTime *= multi;
					target.hurtTime *= multi;
				}
				
				/**
				 * Healing Arrows
				 */
				if (lplayer.getSkills().getMatchingNodesFromName("Healing Arrows") >= 1)
				{
					if(originalAttacker instanceof EntityCustomArrow)
					{
						EntityCustomArrow arrowEnt = (EntityCustomArrow) originalAttacker;
						ItemStack arrows = arrowEnt.getArrowStack();
						List<PotionEffect> effects = PotionUtils.getEffectsFromStack(arrows);
						if(effects != null && !effects.isEmpty())
						{
							for(PotionEffect effect : effects)
							{
								if(effect.getPotion().isBeneficial())
								{
									event.setAmount(0);
									return;
								}
							}
						}
					}
					
				}
			}
			/**
			 * Attacker is Pigman Warrior
			 */
			else if (LoMaS_Utils.FAC && attackerEntity instanceof bitevo.Zetal.LoMaS.Factions.Entity.EntityPigmanWarrior)
			{
				EntityFactionMember efm = (EntityFactionMember) attackerEntity;
				LoMaS_Player cplayer = LoMaS_Player.getLoMaSPlayer(efm.getCommanderId());
				if (cplayer != null)
				{
					float multi = 1.0f + cplayer.getSkills().getPercentMagnitudeFromName("Soldier Damage");
					tempDamg = tempDamg * multi;
				}
			}
			/**
			 * Attacker is Golem
			 */
			else if (attackerEntity instanceof EntityFactionGolem)
			{
				EntityFactionGolem efg = (EntityFactionGolem) attackerEntity;
				LoMaS_Player cplayer = LoMaS_Player.getLoMaSPlayer(efg.getCommanderId());
				if (cplayer != null)
				{
					float multi = 1.0f + cplayer.getSkills().getPercentMagnitudeFromName("Golem Dmg");
					tempDamg = tempDamg * multi;
				}
			}
			
			/**
			 * Target is Player
			 */
			if (damageSrc != null && target instanceof EntityPlayer)
			{
				EntityPlayer tp = (EntityPlayer) target;
				LoMaS_Player tplayer = LoMaS_Player.getLoMaSPlayer(tp);
				
				/**
				 * Starvation
				 */
				if(damageSrc.equals(DamageSource.starve))
				{
					tp.closeScreen();

					if (tplayer.getSkills().getMatchingNodesFromName("Cannot Die From Starvation") >= 1)
					{
						if (tp.getHealth() <= 1.0f)
						{
							event.setCanceled(true);
						}
					}

					return;
				}
				
				if (tempDamg <= 0) return;

				/**
				 * Damage Resistance
				 */
				float multi = 1.0f - tplayer.getSkills().getPercentMagnitudeFromName("Damage Resistance");
				tempDamg = tempDamg * multi; 
				if(damageSrc.isMagicDamage() || damageSrc.getDamageType().equals(DamageSource.wither.getDamageType()))
				{
					multi = 1.0f - tplayer.getSkills().getPercentMagnitudeFromName("Poison Damage Resistance");
					tempDamg = tempDamg * multi;
				}

				/**
				 * Shield Effect
				 */
				if (!damageSrc.isUnblockable() && tp.getActiveItemStack() != null && tp.getActiveItemStack().getItem() == Items.SHIELD && tempDamg > 0.0F)
				{
					multi = 0.6f - tplayer.getSkills().getPercentMagnitudeFromName("Shield effect");
					tempDamg = tempDamg * multi;
				}
				
				tempDamg = SpecializationsMod.ApplyArmor(target, ((EntityPlayer) target).inventory.armorInventory, damageSrc, tempDamg);
				
				if (tempDamg <= 0) return;
				
				tempDamg = SpecializationsMod.applyPotionDamageCalculations(damageSrc, tempDamg, tp);
				float f1 = tempDamg;
				tempDamg = Math.max(tempDamg - tp.getAbsorptionAmount(), 0.0F);
				tp.setAbsorptionAmount(tp.getAbsorptionAmount() - (f1 - tempDamg));

				if (tempDamg != 0.0F)
				{
					tp.addExhaustion(damageSrc.getHungerDamage());
					float f2 = tp.getHealth();
					tp.setHealth(tp.getHealth() - tempDamg);
					tp.getCombatTracker().trackDamage(damageSrc, f2, tempDamg);

					if (tempDamg < 3.4028235E37F)
					{
						tp.addStat(StatList.DAMAGE_TAKEN, Math.round(tempDamg * 10.0F));
					}
				}
				event.setCanceled(true);
			}
			else
			{
				event.setAmount(tempDamg);// target.attackEntityFrom(event.source, tempDamg);
			}
		}
	}

	private boolean isPlayerUnarmed(EntityPlayer attacker)
	{
		ItemStack held = attacker.inventory.getCurrentItem();
		return held == null || held.getItem() == null;
	}

	@SubscribeEvent
	public void livingDrops(LivingDropsEvent event)
	{
		if (event.getSource().getEntity() instanceof EntityPlayer && event.getEntityLiving() instanceof EntityAnimal)
		{
			EntityAnimal animal = (EntityAnimal) event.getEntityLiving();
			LoMaS_Animal lAnimal = LoMaS_Animal.getLoMaSAnimal(animal);
			System.out.println(lAnimal + " " + animal);
			EntityPlayer attacker = (EntityPlayer) event.getSource().getEntity();
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(attacker);
			// int failureChance = lplayer.specClass.equalsIgnoreCase("farmer") ? 0 : 50;
			if (!event.getDrops().isEmpty() && lplayer != null)
			{
				/*
				 * if (attacker.worldObj.rand.nextInt(100) < failureChance) { attacker.addChatMessage(new TextComponentTranslation("\u00a7cYour unskilled hands have caused most of the animal to be unusable.")); for (EntityItem i : event.getDrops()) { if (i.getEntityItem().getItem() instanceof ItemSaddle) { animal.entityDropItem(i.getEntityItem(), 1); } } event.setCanceled(true); } else
				 */if (lAnimal != null && lAnimal.getHunger() < 50 && lAnimal.getSickness() <= 0 && lAnimal.getAge() < 600000)
				{
					for (EntityItem i : event.getDrops())
					{
						if (i.getEntityItem().getItem() instanceof net.minecraft.item.ItemFood)
						{
							i.getEntityItem().setItemDamage(SpecializationsMod.getQualityIndexFromDamage(SpecializationsMod.getQualityIndexFromDamage((int) (lAnimal.getQuality() * (1 + lplayer.getSkills().getPercentMagnitudeFromName("Animal Produce Quality"))))));
						}
					}
					return;
				}
				else
				{
					LoMaS_Utils.addChatMessage(attacker, "\u00a7cThe animal was not fit enough to produce anything useful.");
					event.setCanceled(true);
				}

				if (lAnimal != null)
				{
					LoMaS_Animal.removeLoMaSAnimal(animal);
				}
			}
		}
		else if (event.getEntityLiving() instanceof EntityAnimal)
		{
			EntityAnimal animal = (EntityAnimal) event.getEntityLiving();
			LoMaS_Animal lAnimal = LoMaS_Animal.getLoMaSAnimal(animal);
			if (!event.getDrops().isEmpty())
			{
				if (lAnimal != null && lAnimal.getHunger() < 50 && lAnimal.getSickness() <= 0 && lAnimal.getAge() < 600000)
				{
					for (EntityItem i : event.getDrops())
					{
						if (i.getEntityItem().getItem() instanceof net.minecraft.item.ItemFood)
						{
							i.getEntityItem().setItemDamage(SpecializationsMod.getQualityIndexFromDamage(lAnimal.getQuality()));
						}
					}
					return;
				}
				else
				{
					event.setCanceled(true);
				}

				if (lAnimal != null)
				{
					LoMaS_Animal.removeLoMaSAnimal(animal);
				}
			}
		}
	}

	@SubscribeEvent
	public void onDrops(BlockEvent.HarvestDropsEvent event)
	{
		EntityPlayer player = event.getHarvester();
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
		BlockPos pos = event.getPos();
		World world = event.getWorld();
		IBlockState state = event.getState();

		if (player != null && lplayer != null)
		{
			if (state.getBlock() instanceof BlockTallGrass)
			{
				if (event.isSilkTouching())
				{
					return;
				}

				float seedChance = 20.0f;
				if (world.rand.nextInt(100) > seedChance)
				{
					event.getDrops().clear();
				}
			}
		}
		else
		{
			if (state.getBlock() instanceof BlockTallGrass)
			{
				if (event.isSilkTouching())
				{
					return;
				}
				event.getDrops().clear();
			}
		}
	}

	@SubscribeEvent
	public void onItemStartUse(LivingEntityUseItemEvent.Start event)
	{
		EntityLivingBase ent = event.getEntityLiving();
		if (ent instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) ent;
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);

			if (event.getItem().getItem() instanceof ItemShield)
			{
				if (lplayer.getSkills().getMatchingNodesFromName("Shieldbearing") == 0)
				{
					player.resetActiveHand();
					event.setDuration(-1);
					event.setCanceled(true);
				}
			}
		}
	}

	@SubscribeEvent
	public void onItemFinishUse(LivingEntityUseItemEvent.Finish event)
	{
		ItemStack stack = event.getItem();
		EntityLivingBase ent = event.getEntityLiving();
		if (ent instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) ent;
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
			if (stack.getItem() instanceof ItemPotion)
			{
				if (((stack.getItemDamage() != 0) && (stack.getItemDamage() != 16) && (stack.getItemDamage() != 32) && (stack.getItemDamage() != 64) && (stack.getItemDamage() != 8192)))
				{
					float progressEarned = 12.5f;
					if (!player.capabilities.isCreativeMode)
					{
						lplayer.addSpecProgress(progressEarned);
						player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
					}
				}
			}
			else if (stack.getItem() instanceof ItemFood)
			{
				float addFood = lplayer.getSkills().getMagnitudeFromName("Food Efficiency");
				player.getFoodStats().addStats((int) (addFood * 2), (addFood / 2));
			}
		}
	}
	
	public static ItemStack getInventorySlotContainItem(EntityPlayer entityplayer, Item par1)
	{
		for (int j = 0; j < entityplayer.inventory.mainInventory.length; ++j)
		{
			if (entityplayer.inventory.mainInventory[j] != null && entityplayer.inventory.mainInventory[j].getItem() == par1)
			{
				return entityplayer.inventory.mainInventory[j];
			}
		}

		return null;
	}

	@SubscribeEvent
	public void bowNock(ArrowNockEvent event)
	{
		if (event.getEntity() instanceof EntityPlayer)
		{
			EntityPlayer player = event.getEntityPlayer();
			ItemStack itemstack = event.getBow();
			World world = player.worldObj;
			if (itemstack != null && itemstack.getItem() != null)
			{
				Item item = itemstack.getItem();
				if (getInventorySlotContainItem(player, SpecializationsMod.ARROW) != null)
				{
					LoMaS_Player.getLoMaSPlayer(player).arrowStack = getInventorySlotContainItem(player, SpecializationsMod.ARROW);
					if (!world.isRemote)
					{
						SpecializationsMod.snw.sendToAll(new ArrowMessage(LoMaS_Player.getLoMaSPlayer(player).arrowStack.getItemDamage(), player));
					}
					// System.out.println(LoMaS_Player.getLoMaSPlayer(player).arrowStack);
				}
			}
		}
	}

	@SubscribeEvent(priority = EventPriority.HIGH)
	public void onExplosion(ExplosionEvent.Detonate event)
	{
		Explosion explosion = event.getExplosion();
		World world = event.getWorld();
		List<Entity> e = event.getAffectedEntities();
		List<BlockPos> b = event.getAffectedBlocks();
		if (explosion.getExplosivePlacedBy() instanceof EntityCreeper)
		{
			EntityCreeper creeper = (EntityCreeper) explosion.getExplosivePlacedBy();
			float creepMult = 1.0f;
			if (creeper.getPowered())
			{
				creepMult = 2.0f;
			}
			Iterator iterator = b.iterator();
			/*
			 * while (iterator.hasNext()) { BlockPos blockpos = (BlockPos) iterator.next(); if (world.getBlockState(blockpos).getBlock().getMaterial() == Material.air && world.getBlockState(blockpos.down()).getBlock().isFullBlock() && world.rand.nextInt(3) == 0) { world.setBlockState(blockpos, Blocks.fire.getDefaultState()); } }
			 */

			b.clear();

			float f3 = (3.0f * creepMult) * 3.0F;
			for (Entity ent : e)
			{
				Vec3d Vec3d = explosion.getPosition();
				double d12 = ent.getDistance(explosion.getPosition().xCoord, explosion.getPosition().yCoord, explosion.getPosition().zCoord) / (double) f3;
				double d14 = (double) world.getBlockDensity(Vec3d, ent.getEntityBoundingBox());
				double d10 = (1.0D - d12) * d14;
				ent.attackEntityFrom(DamageSource.causeExplosionDamage(explosion), (float) ((int) ((d10 * d10 + d10) / 2.0D * 7.0D * (double) f3 + 1.0D)));
			}
		}
	}

	@SubscribeEvent
	public void onPlayerSpawn(EntityJoinWorldEvent event)
	{
		Entity e = event.getEntity();
		if (e instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) e;
			LoMaS_Player.addLoMaSPlayer(player);
			if (player != null && LoMaS_Player.getLoMaSPlayer(player) != null)
			{
				// FMLHandler.setPlayerCurrentHealth(player);
				FMLHandler.setPlayerMaxHealth(player);
				if (player.capabilities != null)
				{
					SCMEventHandler.setPlayerWalkSpeed(player);
				}
				if(!event.getWorld().isRemote)
				{
					SpecializationsMod.sendPlayerAbilitiesToClients(LoMaS_Player.getLoMaSPlayer(player));
				}
			}
		}
	}

	/*
	 * @SubscribeEvent public void onAnimalDies(LivingDeathEvent event) { if (event.getEntityLiving() instanceof EntityAnimal) { LoMaS_Animal.animalList.remove(LoMaS_Animal.getLoMaSAnimal(event.getEntityLiving().getEntityId())); } }
	 */

	@SubscribeEvent
	public void onAnimalUpdate(LivingUpdateEvent event)
	{
		if (event.getEntityLiving() instanceof EntityAnimal)
		{
			World world = event.getEntity().worldObj;
			EntityAnimal animal = (EntityAnimal) event.getEntityLiving();
			if (animal.ticksExisted % 10 == (10 - 1))
			{
				LoMaS_Animal lAnimal = LoMaS_Animal.getLoMaSAnimal(animal);
				if (lAnimal != null)
				{
					if (world.isRemote) // if it's on the client
					{
						double d0 = world.rand.nextGaussian() * 0.02D;
						double d1 = world.rand.nextGaussian() * 0.02D;
						double d2 = world.rand.nextGaussian() * 0.02D;
						if (lAnimal.getHunger() > 40)
						{
							lAnimal.setReady(false);
							lAnimal.setQuality(lAnimal.getQuality() - 1);
							int[] info = SpecializationsMod.getAnimalParticleInfo(animal, 0);
							Minecraft.getMinecraft().renderGlobal.spawnParticle(77, false, animal.posX + (double) (world.rand.nextFloat() * animal.width * 2.0F) - (double) animal.width, animal.posY + 0.5D + (double) (world.rand.nextFloat() * animal.height), animal.posZ + (double) (world.rand.nextFloat() * animal.width * 2.0F) - (double) animal.width, d0, d1, d2, info);
						}

						if (lAnimal.getAge() > 600000)
						{
							lAnimal.setReady(false);
							lAnimal.setQuality(lAnimal.getQuality() - 2);
							int[] info = SpecializationsMod.getAnimalParticleInfo(animal, 2);
							Minecraft.getMinecraft().renderGlobal.spawnParticle(77, false, animal.posX + (double) (world.rand.nextFloat() * animal.width * 2.0F) - (double) animal.width, animal.posY + 0.5D + (double) (world.rand.nextFloat() * animal.height), animal.posZ + (double) (world.rand.nextFloat() * animal.width * 2.0F) - (double) animal.width, d0, d1, d2, info);
						}

						if (lAnimal.getSickness() > 0)
						{
							lAnimal.setReady(false);
							lAnimal.setQuality(lAnimal.getQuality() - 1);
							int[] info = SpecializationsMod.getAnimalParticleInfo(animal, 3);
							Minecraft.getMinecraft().renderGlobal.spawnParticle(77, false, animal.posX + (double) (world.rand.nextFloat() * animal.width * 2.0F) - (double) animal.width, animal.posY + 0.5D + (double) (world.rand.nextFloat() * animal.height), animal.posZ + (double) (world.rand.nextFloat() * animal.width * 2.0F) - (double) animal.width, d0, d1, d2, info);
						}
					}
					else // if it's on the server
					{
						if (animal.ticksExisted % 80 == 0 && lAnimal != null && lAnimal.getHunger() >= 100)
						{
							lAnimal.setQuality(lAnimal.getQuality() - 10);
							animal.attackEntityFrom(DamageSource.starve, 1.0F);
						}

						if (animal.ticksExisted % (36 * 20) == ((36 * 20) - 1))
						{
							EntityPlayer owner = lAnimal.getLastToucher() != null && !lAnimal.getLastToucher().equals("") ? animal.worldObj.getPlayerEntityByUUID(lAnimal.getLastToucher()) : null;
							boolean shouldTick = (owner != null || lAnimal.getLastToucher() == null || lAnimal.getLastToucher().equals(""));
							if (shouldTick)
							{
								if (world.rand.nextInt(100) >= 20)
								{
									if (lAnimal.getHunger() < 0)
									{
										lAnimal.setAge(Math.max(lAnimal.getAge() - 126, 300));
									}
									lAnimal.setHunger(Math.min(lAnimal.getHunger() + 1, 100));
								}

								int sicknessChance = lAnimal.getHunger() < 10 ? 700 : 400;
								if ((world.rand.nextInt(sicknessChance) == 0 || lAnimal.getSickness() > 0))
								{
									lAnimal.setAge(lAnimal.getAge() + 2520);
									lAnimal.setSickness(lAnimal.getSickness() + 1);

									List list = animal.worldObj.getEntitiesWithinAABB(animal.getClass(), animal.getEntityBoundingBox().expand((double) 4.0, (double) 4.0, (double) 4.0));
									EntityAnimal entityanimal = null;
									for (int i = 0; i < list.size(); ++i)
									{
										entityanimal = (EntityAnimal) list.get(i);
										if (entityanimal != animal && world.rand.nextInt(100) <= 20)
										{
											lAnimal.setSickness(1);
										}
									}
								}

								if (lAnimal.getAge() >= 604800)
								{
									animal.setDead();
									if (owner != null)
									{
										LoMaS_Utils.addChatMessage(owner, ("\u00a7cOne of your animals has passed away due to old age..."));
									}
								}
							}
						}

						if (animal.ticksExisted % 100 == 0)
						{
							Long secFactor = 1000L;
							Long timeDiff = ((System.currentTimeMillis() - lAnimal.getLastSystemTime()) / secFactor);
							lAnimal.setAge((int) (timeDiff + lAnimal.getAge()));
							SpecializationsMod.snw.sendToAll(new AnimalMessage(animal.getEntityId(), lAnimal.getAge(), lAnimal.getHunger(), lAnimal.getSickness(), lAnimal.isReady()));
							lAnimal.setLastSystemTime(System.currentTimeMillis());

							if (animal.ticksExisted % 200 == 0 && this.rand.nextInt(100) >= 50)
							{
								for (EntityAITaskEntry entry : animal.tasks.taskEntries)
								{
									if (entry.action instanceof EntityAITempt)
									{
										SCMEventHandler.updateTemptations((EntityAITempt) entry.action, animal);
										break;
									}
								}
							}
						}

						if (!(lAnimal.getSickness() <= 0 && lAnimal.getHunger() < 40))
						{
							if (animal instanceof EntityChicken)
							{
								((EntityChicken) animal).timeUntilNextEgg = world.rand.nextInt(6000) + 6000;
							}
							else if (animal instanceof EntitySheep)
							{
								((EntitySheep) animal).setSheared(true);
							}
						}
						else
						{
							lAnimal.setQuality(lAnimal.getQuality() + 1);
							LoMaS_Player lPlayer = LoMaS_Player.getLoMaSPlayer(lAnimal.getLastToucher());
							if (lPlayer != null)
							{
								if (animal instanceof EntityChicken)
								{
									EntityChicken chik = (EntityChicken) animal;
									chik.timeUntilNextEgg -= (lPlayer.getSkills().getPercentMagnitudeFromName("Animal produce spd"));
								}
								else if (animal instanceof EntitySheep)
								{
									EntitySheep shep = (EntitySheep) animal;
									int normalTicks = 40;
									normalTicks *= (1.0f - lPlayer.getSkills().getPercentMagnitudeFromName("Animal produce spd"));
									if (shep.ticksExisted % normalTicks == 0)
									{
										ReflectionHelper.setPrivateValue(EntitySheep.class, shep, 0, 3);
										shep.eatGrassBonus();
									}
								}
							}
						}

						if (lAnimal.getAge() >= 604800 || lAnimal.getSickness() > 0 || lAnimal.getHunger() > 40)
						{
							animal.resetInLove();
						}

						if (animal.getGrowingAge() == 0 && !animal.isInLove())
						{
							if (lAnimal != null && lAnimal.getAge() >= 300 && lAnimal.getAge() <= 600000 && lAnimal.getHunger() >= 0 && lAnimal.getHunger() < 40 && lAnimal.getSickness() == 0)
							{
								if (lAnimal.isReady())
								{
									EntityPlayer owner = null;
									if (lAnimal.getLastToucher() != null && !lAnimal.getLastToucher().equals(""))
									{
										owner = animal.worldObj.getPlayerEntityByUUID(lAnimal.getLastToucher());
									}
									if (owner != null || lAnimal.getLastToucher() == null || lAnimal.getLastToucher().equals(""))
									{
										// System.out.println(animal + " isReady:" + animal.isReady());
										List list = world.getEntitiesWithinAABB(animal.getClass(), animal.getEntityBoundingBox().expand((double) 4.0, (double) 4.0, (double) 4.0));
										boolean foundOne = false;
										EntityAnimal entityanimal = null;
										LoMaS_Animal entitylAnimal = null;
										for (int i = 0; i < list.size() && !foundOne; ++i)
										{
											entityanimal = (EntityAnimal) list.get(i);
											entitylAnimal = LoMaS_Animal.getLoMaSAnimal(entityanimal);
											if (entitylAnimal != null && entityanimal != animal && entitylAnimal.isReady() && (!(entityanimal instanceof EntityWolf) || !((EntityWolf) entityanimal).isSitting()))
											{
												foundOne = true;
											}
										}
										if (foundOne && entityanimal != null)
										{
											// System.out.println(animal + " foundOne:" + foundOne + " " + entityanimal);
											animal.setInLove(owner);
											lAnimal.setReady(false);
											for (EntityAITaskEntry entry : animal.tasks.taskEntries)
											{
												if (entry.action instanceof EntityAIMate)
												{
													EntityAIMate task = (EntityAIMate) entry.action;
													try
													{
														Field f = task.getClass().getDeclaredFields()[2];
														f.setAccessible(true);
														f.set(task, entityanimal);
														task.updateTask();
													}
													catch (Exception e)
													{
														e.printStackTrace();
													}
												}
											}
											entityanimal.setInLove(owner);
											entitylAnimal.setReady(false);
											for (EntityAITaskEntry entry : entityanimal.tasks.taskEntries)
											{
												if (entry.action instanceof EntityAIMate)
												{
													EntityAIMate task = (EntityAIMate) entry.action;
													try
													{
														Field f = task.getClass().getDeclaredFields()[2];
														f.setAccessible(true);
														f.set(task, entityanimal);
														task.updateTask();
													}
													catch (Exception e)
													{
														e.printStackTrace();
													}
												}
											}
										}
									}
								}
								else if (!animal.worldObj.isRemote && animal.ticksExisted % 240 == 0 && world.rand.nextInt(100) <= 5 && world.isDaytime())
								{
									lAnimal.setReady(true);
								}
							}
						}
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onAnimalSpawn(EntityJoinWorldEvent event)
	{
		if (event.getEntity() instanceof EntityAnimal)
		{
			World world = event.getWorld();
			EntityAnimal animal = (EntityAnimal) event.getEntity();
			if (LoMaS_Animal.getLoMaSAnimal(animal) == null)
			{
				LoMaS_Animal.addLoMaSAnimal(animal);
				if (animal.isChild())
				{
					EntityAnimal parentAnimal = world.findNearestEntityWithinAABB(animal.getClass(), new AxisAlignedBB(animal.getPosition().add(-2, -2, -2), animal.getPosition().add(2, 2, 2)), animal);
					if (parentAnimal != null)
					{
						LoMaS_Animal parentLAnimal = LoMaS_Animal.getLoMaSAnimal(parentAnimal);
						if (parentLAnimal != null)
						{
							LoMaS_Player lPlayer = LoMaS_Player.getLoMaSPlayer(parentLAnimal.getLastToucher());
							if (lPlayer != null)
							{
								float chance = 0.0f;
								chance += lPlayer.getSkills().getMagnitudeFromName("Chance for double animals");
								if (chance > this.rand.nextInt(100))
								{
									EntityAnimal cloneAnimal = (EntityAnimal) animal.createChild(parentAnimal);
									if (cloneAnimal != null)
									{
										cloneAnimal.setGrowingAge(-24000);
										cloneAnimal.setLocationAndAngles(parentAnimal.posX, parentAnimal.posY, parentAnimal.posZ, 0.0F, 0.0F);
										LoMaS_Animal.addLoMaSAnimal(cloneAnimal);
										world.spawnEntityInWorld(cloneAnimal);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onEntityRightClick(EntityInteract event)
	{
		Entity e = event.getTarget();
		EntityPlayer player = event.getEntityPlayer();
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
		World world = event.getEntity().worldObj;
		float progressEarned = 0.0f;
		if (e instanceof EntityAnimal)
		{
			EntityAnimal ea = (EntityAnimal) e;
			LoMaS_Animal lAnimal = LoMaS_Animal.getLoMaSAnimal(ea);
			lAnimal.setLastToucher(lplayer.uuid);

			ItemStack i = player.inventory.getCurrentItem();
			if (i != null && SpecializationsMod.isBreedingItem(ea, i) && (lAnimal.getHunger() > 24))// ea.isBreedingItem(i))
			{
				progressEarned = 10.0f;
				if (!player.capabilities.isCreativeMode)
				{
					lplayer.addSpecProgress(progressEarned);
					player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
				}

				if (lplayer.getSkills().getMatchingNodesFromName("Feeding Treats Sickness") >= 1)
				{
					lAnimal.setSickness(0);
				}

				this.feedAnimal(player, lAnimal, ea, world);
				event.setCanceled(true);
			}
			else if (i != null && lAnimal.getSickness() > 0 && cureStacks.contains(i.getItem()))
			{
				progressEarned = 50.0f;
				if (!player.capabilities.isCreativeMode)
				{
					lplayer.addSpecProgress(progressEarned);
					player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
				}

				this.feedAnimal(player, lAnimal, ea, world);

				lAnimal.setSickness(0);
				event.setCanceled(true);
			}
			else if (i != null && i.getItem().equals(Items.SHEARS))
			{
				if (ea instanceof IShearable && ((IShearable) ea).isShearable(i, e.worldObj, e.getPosition()))
				{
					progressEarned = 20.0f;
					if (!player.capabilities.isCreativeMode)
					{
						lplayer.addSpecProgress(progressEarned);
						player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
					}
				}
			}
			else if (i != null && i.getItem().equals(Items.BUCKET) && ea instanceof EntityCow && !(ea instanceof EntityMooshroom))
			{
				if (lAnimal.getSickness() <= 0 && lAnimal.getHunger() < 40)
				{
					if (i.stackSize-- == 1)
					{
						player.inventory.setInventorySlotContents(player.inventory.currentItem, new ItemStack(Items.MILK_BUCKET));
					}
					else if (!player.inventory.addItemStackToInventory(new ItemStack(Items.MILK_BUCKET)))
					{
						player.dropItem(new ItemStack(Items.MILK_BUCKET), true, true);
					}

					progressEarned = 10.0f;
					if (!player.capabilities.isCreativeMode)
					{
						lplayer.addSpecProgress(progressEarned);
						player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((rand.nextFloat() - rand.nextFloat()) * 0.7F + 1.8F));
					}

					event.setCanceled(true);
				}
			}

			if (lplayer.getSkills().getMatchingNodesFromName("Farmer Sense") >= 1)
			{
				if (lplayer.player.ticksExisted > this.lastRightClickTick && !world.isRemote)
				{
					if (lAnimal.getQuality() < 75)
					{
						LoMaS_Utils.addChatMessage(lplayer.player, ("This animal is looking pretty poor... " + lAnimal.getQuality()));
					}
					else if (lAnimal.getQuality() <= 100)
					{
						LoMaS_Utils.addChatMessage(lplayer.player, ("This animal is starting to look pretty decent. " + lAnimal.getQuality()));
					}
					else if (lAnimal.getQuality() <= 150)
					{
						LoMaS_Utils.addChatMessage(lplayer.player, ("This animal looks absolutely superb! " + lAnimal.getQuality()));
					}
					this.lastRightClickTick = lplayer.player.ticksExisted;
				}
			}
		}
	}

	public static void feedAnimal(EntityPlayer player, LoMaS_Animal lAnimal, EntityAnimal ea, World world)
	{
		ItemStack i = player.inventory.getCurrentItem();
		if (!player.capabilities.isCreativeMode)
		{
			if (i.getItem() != FoodInitializer.MUSHROOM_STEW)
			{
				--i.stackSize;

				if (i.stackSize <= 0)
				{
					player.inventory.setInventorySlotContents(player.inventory.currentItem, null);
				}
			}
			else
			{
				--i.stackSize;

				if (i.stackSize <= 0)
				{
					player.inventory.setInventorySlotContents(player.inventory.currentItem, null);
				}
				player.inventory.addItemStackToInventory(new ItemStack(Items.BOWL, 1));
			}
		}

		if (lAnimal.getHunger() > 0)
		{
			lAnimal.setHunger(lAnimal.getHunger() - 30);
			if (lAnimal.getHunger() < 0)
			{
				lAnimal.setHunger(0);
			}
		}
		else
		{
			lAnimal.setHunger(lAnimal.getHunger() - 3);
			if (lAnimal.getHunger() < -25)
			{
				lAnimal.setHunger(-25);
			}
		}

		if (world.isRemote)
		{
			for (int j = 0; j < 7; ++j)
			{
				// fed
				double d0 = world.rand.nextGaussian() * 0.02D;
				double d1 = world.rand.nextGaussian() * 0.02D;
				double d2 = world.rand.nextGaussian() * 0.02D;
				int[] info = SpecializationsMod.getAnimalParticleInfo(ea, 1);
				Minecraft.getMinecraft().renderGlobal.spawnParticle(77, false, ea.posX + (double) (world.rand.nextFloat() * ea.width * 2.0F) - (double) ea.width, ea.posY + 0.5D + (double) (world.rand.nextFloat() * ea.height), ea.posZ + (double) (world.rand.nextFloat() * ea.width * 2.0F) - (double) ea.width, d0, d1, d2, info);
			}
		}

		if (ea instanceof EntitySheep)
		{
			((EntitySheep) ea).setSheared(false);
		}
	}

	@SubscribeEvent
	public void onSaveWorld(WorldEvent.Save event)
	{
		World world = event.getWorld();
		if (!world.isRemote && event.getWorld().provider.getDimension() == 0)
		{
			File f = world.getMinecraftServer().getDataDirectory();
			// System.out.println("Saving animals...");
			File saveDir = new File(f.getPath() + File.separator + "world" + File.separator + "LoMaS" + File.separator);
			saveDir.mkdir();
			File animalFile = new File(saveDir.getPath() + File.separator + event.getWorld().provider.getDimensionType().getName() + ".spec");
			try
			{
				animalFile.delete();
				animalFile.createNewFile();
				FileOutputStream saveOut = new FileOutputStream(animalFile);
				ObjectOutputStream saveObj = new ObjectOutputStream(saveOut);

				saveObj.writeObject(LoMaS_Animal.animalList);

				saveObj.close();
				saveOut.close();
			}
			catch (Exception e1)
			{
				System.err.println("Failed to save animal file at " + animalFile.getPath() + " for world " + event.getWorld().provider.getDimensionType() + " " + e1.getMessage());
				// e1.printStackTrace();
			}
		}
	}

	@SubscribeEvent
	public void onLoadWorld(WorldEvent.Load event)
	{
		World world = event.getWorld();
		if (!world.isRemote && event.getWorld().provider.getDimension() == 0)
		{
			File f = world.getMinecraftServer().getDataDirectory();
			// System.out.println("Loading animals...");
			File loadDir = new File(f.getPath() + File.separator + "world" + File.separator + "LoMaS" + File.separator);
			loadDir.mkdir();
			File animalFile = new File(loadDir.getPath() + File.separator + event.getWorld().provider.getDimensionType().getName() + ".spec");
			try
			{
				FileInputStream loadOut = new FileInputStream(animalFile);
				ObjectInputStream loadObj = new ObjectInputStream(loadOut);

				HashMap<UUID, LoMaS_Animal> temp = (HashMap<UUID, LoMaS_Animal>) loadObj.readObject();
				LoMaS_Animal.animalList = temp;

				loadObj.close();
				loadOut.close();
			}
			catch (Exception e1)
			{
				System.err.println("Failed to load animal file at " + animalFile.getPath() + " for world " + event.getWorld().provider.getDimensionType() + " " + e1.getMessage());
				// e1.printStackTrace();
			}

			// Required settings changes
			world.getMinecraftServer().setAllowFlight(true);
			world.getMinecraftServer().setDifficultyForAllWorlds(EnumDifficulty.HARD);
			for (net.minecraft.world.WorldServer w : world.getMinecraftServer().worldServers)
			{
				w.getGameRules().setOrCreateGameRule("naturalRegeneration ", "false");
			}
		}
	}

	/*
	 * @SubscribeEvent public void onXPPickup(PlayerPickupXpEvent event) { EntityXPOrb xp = event.orb; EntityPlayer player = event.getEntityPlayer(); LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player); if (lplayer != null) { double xpMulti = 0.8; if (lplayer.specClass.equalsIgnoreCase("spellbinder") && lplayer.classlevel >= 1) { xpMulti = xpMulti + .25; } if (lplayer.specClass.equalsIgnoreCase("spellbinder") && lplayer.classlevel >= 6) { xpMulti = xpMulti + .10; } if (lplayer.specClass.equalsIgnoreCase("spellbinder") && lplayer.classlevel >= 9) { xpMulti = xpMulti + .15; } if (player.experienceLevel >= 5 && !lplayer.specClass.equalsIgnoreCase("spellbinder")) { player.experienceLevel = 5; player.experience = 0.0F; player.experienceTotal = 0; event.setCanceled(true); } else if (player.experienceLevel >= 10 && lplayer.specClass.equalsIgnoreCase("spellbinder") && lplayer.classlevel < 4) { player.experienceLevel = 10; player.experience = 0.0F; player.experienceTotal = 0; event.setCanceled(true); } else if (player.experienceLevel >= 20 && lplayer.specClass.equalsIgnoreCase("spellbinder") && lplayer.classlevel < 8) { player.experienceLevel = 20; player.experience = 0.0F; player.experienceTotal = 0; event.setCanceled(true); } else if (player.experienceLevel >= 30 && lplayer.specClass.equalsIgnoreCase("spellbinder") && lplayer.classlevel < 11) { player.experienceLevel = 30; player.experience = 0.0F; player.experienceTotal = 0; event.setCanceled(true); } xp.xpValue = (int) (xp.xpValue * xpMulti); } }
	 */

	@SubscribeEvent
	public void replaceXP(EntityEvent.EnteringChunk event)
	{
		World world = event.getEntity().worldObj;
		Entity e = event.getEntity();

		if (!e.isDead && e instanceof EntityXPOrb && !(e instanceof EntityCustomXPOrb))
		{
			EntityXPOrb orb = (EntityXPOrb) e;
			EntityCustomXPOrb cxp = new EntityCustomXPOrb(world, orb.posX, orb.posY, orb.posZ, orb.xpValue);
			orb.setDead();
			world.spawnEntityInWorld(cxp);
			// System.out.println("XP ORB IS FeRND");
		}
	}

	@SubscribeEvent
	public void itemJoinsWorld(EntityJoinWorldEvent event)
	{
		Entity entity = event.getEntity();
		World world = event.getWorld();
		if (entity instanceof EntityItem)
		{
			EntityItem entItem = (EntityItem) entity;
			ItemStack stackOriginal = entItem.getEntityItem();
			ItemStack stack = SCMEventHandler.getConvertedItemStack(stackOriginal);
			if (stack != null)
			{
				if (!stackOriginal.getItem().equals(stack.getItem()))
				{
					/**
					 * Chicken Quality
					 */
					if (stackOriginal.getItem() == Items.EGG)
					{
						EntityChicken chickenAnimal = (EntityChicken) world.findNearestEntityWithinAABB(EntityChicken.class, new AxisAlignedBB(entItem.getPosition().add(-2, -2, -2), entItem.getPosition().add(2, 2, 2)), entItem);
						if (chickenAnimal != null)
						{
							LoMaS_Animal chickenLAnimal = LoMaS_Animal.getLoMaSAnimal(chickenAnimal);
							if (chickenLAnimal != null)
							{
								LoMaS_Player lPlayer = LoMaS_Player.getLoMaSPlayer(chickenLAnimal.getLastToucher());
								if (lPlayer != null)
								{
									stack.setItemDamage(SpecializationsMod.getQualityIndexFromDamage((int) (chickenLAnimal.getQuality() * (1 + lPlayer.getSkills().getPercentMagnitudeFromName("Animal Produce Quality")))));
								}
							}
						}
					}
					entItem.setEntityItemStack(stack);
				}
			}
		}
		else if (!entity.isDead && entity instanceof EntityFishHook && !(entity instanceof EntitySpecialFishHook))
		{
			EntityFishHook origHook = (EntityFishHook) entity;
			EntitySpecialFishHook newHook = new EntitySpecialFishHook(origHook.worldObj, origHook.angler);
			if (!world.isRemote)
			{
				if ((origHook.angler.getHeldItemMainhand() != null && origHook.angler.getHeldItemMainhand().getItem() instanceof ItemIronFishingRod) || (origHook.angler.getHeldItemOffhand() != null && origHook.angler.getHeldItemOffhand().getItem() instanceof ItemIronFishingRod))
				{
					newHook.setType(1);
				}
				world.removeEntity(origHook);
				world.spawnEntityInWorld(newHook);
				newHook.angler.fishEntity = newHook;
				event.setCanceled(true);
			}
		}
	}

	public static ItemStack getConvertedItemStack(ItemStack stack)
	{
		if (stack == null || stack.getItem() == null)
		{
			return stack;
		}

		ItemStack ret = stack;
		if (stack.getItem().equals(Items.ARROW))
		{
			ret = new ItemStack(SpecializationsMod.ARROW, stack.stackSize, 1);
		}
		else if (stack.getItem().equals(Items.BOW))
		{
			ret = new ItemStack(SpecializationsMod.BOW, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Item.getItemFromBlock(Blocks.SAPLING)))
		{
			ret = new ItemStack(Item.getItemFromBlock(SpecializationsMod.SAPLING), stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Item.getItemFromBlock(Blocks.MELON_BLOCK)))
		{
			ret = new ItemStack(Item.getItemFromBlock(FoodInitializer.MELON_BLOCK), stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Item.getItemFromBlock(Blocks.PUMPKIN)))
		{
			ret = new ItemStack(Item.getItemFromBlock(FoodInitializer.PUMPKIN), stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Item.getItemFromBlock(Blocks.HAY_BLOCK)))
		{
			ret = new ItemStack(Item.getItemFromBlock(FoodInitializer.HAY_BLOCK), stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Item.getItemFromBlock(Blocks.CRAFTING_TABLE)))
		{
			ret = new ItemStack(Item.getItemFromBlock(SpecializationsMod.CRAFTING_TABLE), stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Item.getItemFromBlock(Blocks.FURNACE)))
		{
			ret = new ItemStack(Item.getItemFromBlock(SpecializationsMod.FURNACE), stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Item.getItemFromBlock(Blocks.BREWING_STAND)))
		{
			ret = new ItemStack(Item.getItemFromBlock(SpecializationsMod.BREWING_STAND), stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.WHEAT))
		{
			ret = new ItemStack(FoodInitializer.WHEAT, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.WHEAT_SEEDS))
		{
			ret = new ItemStack(FoodInitializer.WHEAT_SEEDS, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.REEDS))
		{
			ret = new ItemStack(FoodInitializer.REEDS, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.CARROT))
		{
			ret = new ItemStack(FoodInitializer.CARROT, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.POTATO))
		{
			ret = new ItemStack(FoodInitializer.POTATO, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.NETHER_WART))
		{
			ret = new ItemStack(FoodInitializer.NETHER_WART, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.MELON_SEEDS))
		{
			ret = new ItemStack(FoodInitializer.MELON_SEEDS, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.PUMPKIN_SEEDS))
		{
			ret = new ItemStack(FoodInitializer.PUMPKIN_SEEDS, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.WOODEN_AXE))
		{
			ret = new ItemStack(SpecializationsMod.WOODEN_AXE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.WOODEN_PICKAXE))
		{
			ret = new ItemStack(SpecializationsMod.WOODEN_PICKAXE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.WOODEN_HOE))
		{
			ret = new ItemStack(SpecializationsMod.WOODEN_HOE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.WOODEN_SWORD))
		{
			ret = new ItemStack(SpecializationsMod.WOODEN_SWORD, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.WOODEN_SHOVEL))
		{
			ret = new ItemStack(SpecializationsMod.WOODEN_SHOVEL, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.STONE_AXE))
		{
			ret = new ItemStack(SpecializationsMod.STONE_AXE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.STONE_PICKAXE))
		{
			ret = new ItemStack(SpecializationsMod.STONE_PICKAXE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.STONE_HOE))
		{
			ret = new ItemStack(SpecializationsMod.STONE_HOE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.STONE_SWORD))
		{
			ret = new ItemStack(SpecializationsMod.STONE_SWORD, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.STONE_SHOVEL))
		{
			ret = new ItemStack(SpecializationsMod.STONE_SHOVEL, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.GOLDEN_AXE))
		{
			ret = new ItemStack(SpecializationsMod.GOLDEN_AXE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.GOLDEN_PICKAXE))
		{
			ret = new ItemStack(SpecializationsMod.GOLDEN_PICKAXE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.GOLDEN_HOE))
		{
			ret = new ItemStack(SpecializationsMod.GOLDEN_HOE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.GOLDEN_SWORD))
		{
			ret = new ItemStack(SpecializationsMod.GOLDEN_SWORD, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.GOLDEN_SHOVEL))
		{
			ret = new ItemStack(SpecializationsMod.GOLDEN_SHOVEL, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.IRON_AXE))
		{
			ret = new ItemStack(SpecializationsMod.IRON_AXE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.IRON_PICKAXE))
		{
			ret = new ItemStack(SpecializationsMod.IRON_PICKAXE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.IRON_HOE))
		{
			ret = new ItemStack(SpecializationsMod.IRON_HOE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.IRON_SWORD))
		{
			ret = new ItemStack(SpecializationsMod.IRON_SWORD, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.IRON_SHOVEL))
		{
			ret = new ItemStack(SpecializationsMod.IRON_SHOVEL, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.DIAMOND_AXE))
		{
			ret = new ItemStack(SpecializationsMod.DIAMOND_AXE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.DIAMOND_PICKAXE))
		{
			ret = new ItemStack(SpecializationsMod.DIAMOND_PICKAXE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.DIAMOND_HOE))
		{
			ret = new ItemStack(SpecializationsMod.DIAMOND_HOE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.DIAMOND_SWORD))
		{
			ret = new ItemStack(SpecializationsMod.DIAMOND_SWORD, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.DIAMOND_SHOVEL))
		{
			ret = new ItemStack(SpecializationsMod.DIAMOND_SHOVEL, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.LEATHER_HELMET))
		{
			ret = new ItemStack(SpecializationsMod.LEATHER_HELMET, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.LEATHER_CHESTPLATE))
		{
			ret = new ItemStack(SpecializationsMod.LEATHER_CHESTPLATE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.LEATHER_LEGGINGS))
		{
			ret = new ItemStack(SpecializationsMod.LEATHER_LEGGINGS, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.LEATHER_BOOTS))
		{
			ret = new ItemStack(SpecializationsMod.LEATHER_BOOTS, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.CHAINMAIL_HELMET))
		{
			ret = new ItemStack(SpecializationsMod.CHAINMAIL_HELMET, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.CHAINMAIL_CHESTPLATE))
		{
			ret = new ItemStack(SpecializationsMod.CHAINMAIL_CHESTPLATE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.CHAINMAIL_LEGGINGS))
		{
			ret = new ItemStack(SpecializationsMod.CHAINMAIL_LEGGINGS, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.CHAINMAIL_BOOTS))
		{
			ret = new ItemStack(SpecializationsMod.CHAINMAIL_BOOTS, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.IRON_HELMET))
		{
			ret = new ItemStack(SpecializationsMod.IRON_HELMET, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.IRON_CHESTPLATE))
		{
			ret = new ItemStack(SpecializationsMod.IRON_CHESTPLATE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.IRON_LEGGINGS))
		{
			ret = new ItemStack(SpecializationsMod.IRON_LEGGINGS, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.IRON_BOOTS))
		{
			ret = new ItemStack(SpecializationsMod.IRON_BOOTS, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.GOLDEN_HELMET))
		{
			ret = new ItemStack(SpecializationsMod.GOLDEN_HELMET, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.GOLDEN_CHESTPLATE))
		{
			ret = new ItemStack(SpecializationsMod.GOLDEN_CHESTPLATE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.GOLDEN_LEGGINGS))
		{
			ret = new ItemStack(SpecializationsMod.GOLDEN_LEGGINGS, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.GOLDEN_BOOTS))
		{
			ret = new ItemStack(SpecializationsMod.GOLDEN_BOOTS, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.DIAMOND_HELMET))
		{
			ret = new ItemStack(SpecializationsMod.DIAMOND_HELMET, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.DIAMOND_CHESTPLATE))
		{
			ret = new ItemStack(SpecializationsMod.DIAMOND_CHESTPLATE, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.DIAMOND_LEGGINGS))
		{
			ret = new ItemStack(SpecializationsMod.DIAMOND_LEGGINGS, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem().equals(Items.DIAMOND_BOOTS))
		{
			ret = new ItemStack(SpecializationsMod.DIAMOND_BOOTS, stack.stackSize, stack.getMetadata());
		}
		else if (stack.getItem() instanceof net.minecraft.item.ItemFood && !(stack.getItem() instanceof ItemFood))
		{
			if (stack.getItem().equals(Items.BREAD))
			{
				ret = new ItemStack(FoodInitializer.BREAD, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.WHEAT))
			{
				ret = new ItemStack(FoodInitializer.WHEAT, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.FISH))
			{
				ret = new ItemStack(FoodInitializer.FISH, stack.stackSize, stack.getMetadata() * 3);
			}
			else if (stack.getItem().equals(Items.COOKED_FISH))
			{
				ret = new ItemStack(FoodInitializer.COOKED_FISH, stack.stackSize, stack.getMetadata() * 3);
			}
			else if (stack.getItem().equals(Items.APPLE))
			{
				ret = new ItemStack(FoodInitializer.APPLE, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.MUSHROOM_STEW))
			{
				ret = new ItemStack(FoodInitializer.MUSHROOM_STEW, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.RABBIT_STEW))
			{
				ret = new ItemStack(FoodInitializer.RABBIT_STEW, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.POISONOUS_POTATO))
			{
				ret = new ItemStack(FoodInitializer.POISONOUS_POTATO, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.SPIDER_EYE))
			{
				ret = new ItemStack(FoodInitializer.SPIDER_EYE, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.CAKE))
			{
				ret = new ItemStack(FoodInitializer.CAKE, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.PORKCHOP))
			{
				ret = new ItemStack(FoodInitializer.PORKCHOP, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.COOKED_PORKCHOP))
			{
				ret = new ItemStack(FoodInitializer.COOKED_PORKCHOP, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.COOKIE))
			{
				ret = new ItemStack(FoodInitializer.COOKIE, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.MELON))
			{
				ret = new ItemStack(FoodInitializer.MELON, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.BEEF))
			{
				ret = new ItemStack(FoodInitializer.BEEF, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.COOKED_BEEF))
			{
				ret = new ItemStack(FoodInitializer.COOKED_BEEF, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.CHICKEN))
			{
				ret = new ItemStack(FoodInitializer.CHICKEN, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.COOKED_CHICKEN))
			{
				ret = new ItemStack(FoodInitializer.COOKED_CHICKEN, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.BAKED_POTATO))
			{
				ret = new ItemStack(FoodInitializer.BAKED_POTATO, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.GOLDEN_CARROT))
			{
				ret = new ItemStack(FoodInitializer.GOLDEN_CARROT, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.PUMPKIN_PIE))
			{
				ret = new ItemStack(FoodInitializer.PUMPKIN_PIE, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.RABBIT))
			{
				ret = new ItemStack(FoodInitializer.RABBIT, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.COOKED_RABBIT))
			{
				ret = new ItemStack(FoodInitializer.COOKED_RABBIT, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.MUTTON))
			{
				ret = new ItemStack(FoodInitializer.MUTTON, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.COOKED_MUTTON))
			{
				ret = new ItemStack(FoodInitializer.COOKED_MUTTON, stack.stackSize, stack.getMetadata());
			}
			else if (stack.getItem().equals(Items.ROTTEN_FLESH))
			{
				ret = new ItemStack(FoodInitializer.ROTTEN_FLESH, stack.stackSize, stack.getMetadata());
			}
		}
		return ret;
	}

	private void copyDataFromOld(Entity entityOld, Entity entityNew)
	{
		NBTTagCompound nbttagcompound = entityOld.writeToNBT(new NBTTagCompound());
		entityNew.readFromNBT(nbttagcompound);
	}

	public static void setPlayerWalkSpeed(EntityPlayer player)
	{
		Field f;
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
		try
		{
			f = player.capabilities.getClass().getDeclaredFields()[6];// .getDeclaredField("walkSpeed");
			f.setAccessible(true);
			f.set(player.capabilities, SpecializationsMod.getWalkSpeed(player, lplayer));
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
		catch (IllegalAccessException e)
		{
			e.printStackTrace();
		}
		catch (SecurityException e)
		{
			e.printStackTrace();
		}
		catch (ConcurrentModificationException e)
		{
			e.printStackTrace();
		}
	}

	public static float getExhaustion(EntityPlayer player)
	{
		float exhaustion = 0.0f;
		Field f;
		try
		{
			f = player.getFoodStats().getClass().getDeclaredFields()[2];// .getDeclaredField("foodExhaustionLevel");
			f.setAccessible(true);
			exhaustion = (Float) f.get(player.getFoodStats());
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
		catch (IllegalAccessException e)
		{
			e.printStackTrace();
		}
		catch (SecurityException e)
		{
			e.printStackTrace();
		}
		return exhaustion;
	}

	public static String printStacksFromSlots(List a)
	{
		String c = "[";
		for (Object o : a)
		{
			if (o instanceof Slot)
			{
				Slot s = (Slot) o;
				c = c + s.getStack() + ", ";
			}
		}
		c = c + "]";
		return c;
	}

	public static void updateTemptations(EntityAITempt temptAI, EntityAnimal animal)
	{
		try
		{
			Field f = temptAI.getClass().getDeclaredFields()[10]; // temptItem //NoSuchFieldException
			f.setAccessible(true);
			Set<Item> temptItemsOld = (Set<Item>) f.get(temptAI); // IllegalAccessException
			Set<Item> temptItemsNew = new HashSet<Item>();
			for (Item i : temptItemsOld)
			{
				Item converted = SCMEventHandler.getConvertedItemStack(new ItemStack(i)).getItem();
				temptItemsNew.add(converted);
			}

			if (temptItemsOld.equals(temptItemsNew))
			{
				return;
			}

			f = temptAI.getClass().getDeclaredFields()[1]; // speed //NoSuchFieldException
			f.setAccessible(true);
			double speed = (Double) f.get(temptAI); // IllegalAccessException

			f = temptAI.getClass().getDeclaredFields()[11]; // scaredByPlayerMovement //NoSuchFieldException
			f.setAccessible(true);
			boolean isScaredy = (Boolean) f.get(temptAI); // IllegalAccessException

			animal.tasks.removeTask(temptAI);
			EntityAITempt newTempt = new EntityAITempt(animal, speed, isScaredy, temptItemsNew);
			animal.tasks.addTask(3, newTempt);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static boolean useHoe(ItemStack stack, EntityPlayer player, World worldIn, BlockPos target, IBlockState newState)
	{
		worldIn.playSound(player, target, SoundEvents.ITEM_HOE_TILL, SoundCategory.BLOCKS, 1.0F, 1.0F);

		if (worldIn.isRemote)
		{
			return true;
		}
		else
		{
			worldIn.setBlockState(target, newState);
			stack.damageItem(1, player);
			return true;
		}
	}

	public static int isOnBlock(EntityPlayer player, Block block)
	{
		AxisAlignedBB axisalignedbb = player.getEntityBoundingBox();
		BlockPos.PooledMutableBlockPos blockpos$pooledmutableblockpos = BlockPos.PooledMutableBlockPos.retain(axisalignedbb.minX + 0.001D, axisalignedbb.minY + 0.001D, axisalignedbb.minZ + 0.001D);
		BlockPos.PooledMutableBlockPos blockpos$pooledmutableblockpos1 = BlockPos.PooledMutableBlockPos.retain(axisalignedbb.maxX - 0.001D, axisalignedbb.maxY - 0.001D, axisalignedbb.maxZ - 0.001D);
		BlockPos.PooledMutableBlockPos blockpos$pooledmutableblockpos2 = BlockPos.PooledMutableBlockPos.retain();

		int count = 0;
		if (player.worldObj.isAreaLoaded(blockpos$pooledmutableblockpos, blockpos$pooledmutableblockpos1))
		{
			for (int i = blockpos$pooledmutableblockpos.getX(); i <= blockpos$pooledmutableblockpos1.getX(); ++i)
			{
				for (int j = blockpos$pooledmutableblockpos.getY(); j <= blockpos$pooledmutableblockpos1.getY(); ++j)
				{
					for (int k = blockpos$pooledmutableblockpos.getZ(); k <= blockpos$pooledmutableblockpos1.getZ(); ++k)
					{
						blockpos$pooledmutableblockpos2.setPos(i, j, k);
						IBlockState iblockstate = player.worldObj.getBlockState(blockpos$pooledmutableblockpos2);
						if (iblockstate.getBlock() == block)
						{
							count++;
						}
					}
				}
			}
		}

		blockpos$pooledmutableblockpos.release();
		blockpos$pooledmutableblockpos1.release();
		blockpos$pooledmutableblockpos2.release();
		return count;
	}

	/*
	 * public static void getEntitiesWithinChunk(Chunk chunk, List listToFill) { int i = MathHelper.floor_double((0 - World.MAX_ENTITY_RADIUS) / 16.0D); int j = MathHelper.floor_double((16 + World.MAX_ENTITY_RADIUS) / 16.0D); i = MathHelper.clamp_int(i, 0, chunk.getEntityLists().length - 1); j = MathHelper.clamp_int(j, 0, chunk.getEntityLists().length - 1); for (int k = i; k <= j; ++k) { Iterator iterator = chunk.getEntityLists()[k].iterator(); while (iterator.hasNext()) { Entity entity1 = (Entity) iterator.next(); listToFill.add(entity1); Entity[] aentity = entity1.getParts(); if (aentity != null) { for (int l = 0; l < aentity.length; ++l) { entity1 = aentity[l]; listToFill.add(entity1); } } } } }
	 */
}
