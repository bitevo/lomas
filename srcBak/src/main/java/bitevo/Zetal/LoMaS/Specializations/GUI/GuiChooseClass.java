package bitevo.Zetal.LoMaS.Specializations.GUI;

import java.io.IOException;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiOptionButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import org.lwjgl.opengl.GL11;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Handler.ChooseClassGUIKeyBind;
import bitevo.Zetal.LoMaS.Specializations.Handler.FMLHandler;

@SideOnly(Side.CLIENT)
public class GuiChooseClass extends GuiScreen
{
    private static EntityPlayer thePlayer;
    private float progress = 0.0f;
    private int classlevel = 0;
    private String className;
    private String selectedClass = "";
    /**Miner Blacksmith Farmer Warrior Stealth Woodsman Spellbinder**/
    private String[] classStringArray = new String[]{"Miner", "Blacksmith", "Farmer", "Warrior", "WIP", "Woodsman", "Spellbinder"};
	private boolean[] buttonArray = new boolean[]{false, false, false, false, false, false, false};
	private boolean isConfirming = false;

    public GuiChooseClass(EntityPlayer par1EntityPlayer)
    {
        this.allowUserInput = true;
        this.thePlayer = par1EntityPlayer;
        this.progress = LoMaS_Player.getLoMaSPlayer(par1EntityPlayer).progress;
        this.classlevel = LoMaS_Player.getLoMaSPlayer(par1EntityPlayer).classlevel;
        buttonArray = new boolean[]{false, false, false, false, false, false, false};
        classStringArray = new String[]{"Miner", "Blacksmith", "Farmer", "Warrior", "WIP", "Woodsman", "Spellbinder"};
        selectedClass = LoMaS_Player.getLoMaSPlayer(par1EntityPlayer).specClass;
        isConfirming = false;
        if(LoMaS_Player.getLoMaSPlayer(par1EntityPlayer).specClass.length() >= 2)
        {
            this.className = LoMaS_Player.getLoMaSPlayer(par1EntityPlayer).specClass.substring(0, 1).toUpperCase() + LoMaS_Player.getLoMaSPlayer(par1EntityPlayer).specClass.substring(1);
        }
        else
        {
        	this.className = "Basic";
        }
        for(int i = 0; i < classStringArray.length; i++)
        {
        	if(this.className.equals(classStringArray[i]))
        	{
        		buttonArray[i] = true;
        	}
        	else
        	{
        		buttonArray[i] = false;
        	}
        }
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    @Override
    public void initGui()
    {
        this.buttonList.clear();
        if(!this.isConfirming)
        {
            //System.out.println("FOREGROUND");
    		ScaledResolution res = new ScaledResolution(this.mc);
    		int width = res.getScaledWidth();
    		int height = res.getScaledHeight();
    		int k = (width - 62) / 2;
    		int l = (height - 57) / 2;
    		int x = -71;
    		int y = -28;
    		/////Class Buttons
    		for(int i = 0; i < classStringArray.length; i++)
    		{
    			if(i+1 == classStringArray.length)
    			{
    				x = 2;
    			}
    	        GuiButton guibutton = new GuiButton(i, k + x, l + y, 62, 20, classStringArray[i]);
    	        guibutton.enabled = true;
    	        guibutton.displayString = classStringArray[i];
    	        this.buttonList.add(guibutton);
    			x = x + 72;
    			if((i + 1) % 3 == 0)
    			{
    				if((i) != classStringArray.length)
    				{
    					x = -71;
    					y = y + 57;
    				}
    			}
    			if(this.buttonArray[i])
    			{
    				guibutton.enabled = false;
    			}
    		}
    		//Confirm button
            GuiButton confirmbutton = new GuiButton(7, k + x, l + y - 25, 62, 20, "Confirm?");
            confirmbutton.enabled = true;
            confirmbutton.displayString = "Confirm?";
            this.buttonList.add(confirmbutton);
            if(LoMaS_Player.getLoMaSPlayer(mc.thePlayer).specClass.equalsIgnoreCase(this.selectedClass))
            {
                confirmbutton.enabled = false;
            }
            //Cancel button
            GuiButton cancelbutton = new GuiButton(8, k + x, l + y, 62, 20, "Cancel");
            cancelbutton.enabled = true;
            cancelbutton.displayString = "Cancel";
            this.buttonList.add(cancelbutton);
            
            ((GuiButton)this.buttonList.get(4)).enabled = false;
        }
        else
        {
            this.buttonList.add(new GuiOptionButton(9, this.width / 2 - 155, this.height / 6 + 86, "I'm sure!"));
            this.buttonList.add(new GuiOptionButton(10, this.width / 2 - 155 + 160, this.height / 6 + 86, "Nevermind"));
        }
    }

    /**
     * Draws the screen and all the components in it.
     */
    @Override
    public void drawScreen(int par1, int par2, float par3)
    {
        this.drawDefaultBackground();
        super.drawScreen(par1, par2, par3);
    }

    /**
     * Fired when a control is clicked. This is the equivalent of ActionListener.actionPerformed(ActionEvent e).
     * @throws IOException 
     */
    @Override
    protected void actionPerformed(GuiButton par1GuiButton) throws IOException
    {
    	super.actionPerformed(par1GuiButton);
    	if(par1GuiButton.id < 7)
    	{
        	this.buttonArray = new boolean[]{false, false, false, false, false, false, false};
        	this.buttonArray[par1GuiButton.id] = true;
            selectedClass = this.classStringArray[par1GuiButton.id];
        	this.initGui();
    	}
    	else if(par1GuiButton.id == 7)
    	{
    		this.isConfirming = true;
        	this.initGui();
    	}
    	else if(par1GuiButton.id == 8)
    	{
            this.mc.thePlayer.closeScreen();
    	}
    	else if(par1GuiButton.id == 9)
    	{
    		LoMaS_Player.getLoMaSPlayer(mc.thePlayer).setSpecClass(this.selectedClass.toLowerCase());
    		LoMaS_Player.getLoMaSPlayer(mc.thePlayer).setSpecClassLevel(1);
    		LoMaS_Player.getLoMaSPlayer(mc.thePlayer).setSpecProgress(0.0f);
    		LoMaS_Player.getLoMaSPlayer(mc.thePlayer).sendPlayerAbilities();
    		FMLHandler.setPlayerMaxHealth(mc.thePlayer);
            this.mc.thePlayer.closeScreen();
    	}
    	else if(par1GuiButton.id == 10)
    	{
    		this.isConfirming = false;
        	this.initGui();
    	}
    }
    
    @Override
    public void drawDefaultBackground() 
    {
    	if(!this.isConfirming)
    	{
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.mc.getTextureManager().bindTexture(new ResourceLocation(SpecializationsMod.modid, "gui/specinfo.png"));
    		ScaledResolution res = new ScaledResolution(this.mc);
    		int width = res.getScaledWidth();
    		int height = res.getScaledHeight();
    		int k = (width - 72) / 2;
    		int l = (height - 57) / 2;
    		int x = -70;
    		int y = -60;
    		for(int i = 0; i < 3; i++)
    		{
    	        this.drawTexturedModalRect(k + x, l + y, 0, 198, 72, 58);
    	        this.drawTexturedModalRect(k + x + 20, l + y + 3, 72 + (25 * i), 198, 25, 25);
    			x = x + 72;
    		}
    		x = -70;
    		y = y + 57;
    		for(int i = 3; i < 6; i++)
    		{
    	        this.drawTexturedModalRect(k + x, l + y, 0, 198, 72, 58);
    	        this.drawTexturedModalRect(k + x + 20, l + y + 3, 72 + (25 * i), 198, 25, 25);
    			x = x + 72;
    		}
    		x = 2;
    		y = y + 57;
    	    this.drawTexturedModalRect(k + x, l + y, 0, 198, 72, 58);
            this.drawTexturedModalRect(k + x + 24, l + y + 3, 72 + (25 * 6), 198, 25, 25);
    	}
    	else
    	{
            super.drawDefaultBackground();
            this.drawCenteredString(this.fontRendererObj, "Are you sure you wish to change classes?", this.width / 2, 70, 16777215);
            this.drawCenteredString(this.fontRendererObj, "All levels and progress gained will be permanently lost!", this.width / 2, 90, 16777215);
    	}
    }
    
    @Override
    public boolean doesGuiPauseGame()
    {
        return false;
    }
    
    @Override
    protected void keyTyped(char par1, int par2)
    {
        if (par2 == 1 || par2 == this.mc.gameSettings.keyBindInventory.getKeyCode() || par2 == ChooseClassGUIKeyBind.keys[0].getKeyCode())
        {
            this.mc.thePlayer.closeScreen();
        }
    }
    
    @Override
    public void updateScreen()
    {
    	super.updateScreen();
        if (!this.mc.thePlayer.isEntityAlive() || this.mc.thePlayer.isDead)
        {
            this.mc.thePlayer.closeScreen();
        }
    }
}
