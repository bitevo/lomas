package bitevo.Zetal.LoMaS.BigFMod.TileEntity;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import bitevo.Zetal.LoMaS.BigFMod.BigFMod;
import bitevo.Zetal.LoMaS.BigFMod.Block.BlockBookstand;
import bitevo.Zetal.LoMaS.BigFMod.Message.BookstandMessage;

public class TileEntityBookstand extends TileEntity
{
	private boolean hasBook = false;
	private ItemStack storedBook;
	private int renderCounter = 0;
	
    /**
     * Reads a tile entity from NBT.
     */
    @Override
    public void readFromNBT(NBTTagCompound compound)
    {
        super.readFromNBT(compound);
        NBTTagList nbttaglist = compound.getTagList("book", 10);
        if(nbttaglist.tagCount() >= 1)
        {
            NBTTagCompound nbttagcompound1 = (NBTTagCompound)nbttaglist.getCompoundTagAt(0);
            this.storedBook = ItemStack.loadItemStackFromNBT(nbttagcompound1);
            if(storedBook != null)
            {
                this.hasBook = true;
            }
            else
            {
            	this.hasBook = false;
            }
        }
        else
        {
        	this.storedBook = null;
        	this.hasBook = false;
        }
    }

    /**
     * Writes a tile entity to NBT.
     * @return 
     */
    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound)
    {
        super.writeToNBT(compound);
        if(this.storedBook != null)
        {
	        NBTTagList nbttaglist = new NBTTagList();
	        NBTTagCompound nbttagcompound1 = new NBTTagCompound();
	        this.storedBook.writeToNBT(nbttagcompound1);
	        nbttaglist.appendTag(nbttagcompound1);
	
	        compound.setTag("book", nbttaglist);
        }
        else
        {
	        NBTTagList nbttaglist = new NBTTagList();
	        NBTTagCompound nbttagcompound1 = new NBTTagCompound();
	        nbttaglist.appendTag(nbttagcompound1);
	
	        compound.setTag("book", nbttaglist);
        }
        return compound;
    }
    
    @Override
    public void updateContainingBlockInfo()
    {
    	super.updateContainingBlockInfo();
    	if(this.worldObj.isRemote)
    	{
    		if(renderCounter % 20 == 0)
    		{
    			worldObj.markBlockRangeForRenderUpdate(pos, pos);
    		}
    		renderCounter++;
    	}
    	else
    	{
    		if(renderCounter % 20 == 0)
    		{
                BigFMod.snw.sendToDimension(new BookstandMessage(this.pos, this.storedBook, this.hasBook), this.worldObj.provider.getDimension());
    		}
    		renderCounter++;
    	}
    }
    
    @Override
    public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newState)
    {
    	return !oldState.getBlock().equals(newState.getBlock());
    }
	
	public boolean hasBook()
	{
		return hasBook;
	}
	
	public void addBook(ItemStack book)
	{
		this.storedBook = book.copy();
		this.hasBook = true;
		if (!worldObj.isRemote)
		{
	        BigFMod.snw.sendToDimension(new BookstandMessage(this.pos, this.storedBook, this.hasBook), this.worldObj.provider.getDimension());
		}
		worldObj.notifyBlockUpdate(pos, worldObj.getBlockState(pos), worldObj.getBlockState(pos), 3);
	}
	
	public ItemStack getStoredBook()
	{
		return this.storedBook;
	}
	
	public void removeBook()
	{
		if (!worldObj.isRemote)
		{
	        float f = 0.3F;
	        double d0 = (double)(worldObj.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
	        double d1 = (double)(worldObj.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
	        double d2 = (double)(worldObj.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
	        EntityItem entityitem = new EntityItem(worldObj, (double)pos.getX() + d0, (double)pos.getY() + d1, (double)pos.getZ() + d2, this.storedBook.copy());
			this.storedBook = null;
			this.hasBook = false; 
	        worldObj.spawnEntityInWorld(entityitem);
	        BigFMod.snw.sendToDimension(new BookstandMessage(this.pos, this.storedBook, this.hasBook), this.worldObj.provider.getDimension());
    	}
		this.storedBook = null;
		this.hasBook = false; 
		worldObj.markBlockRangeForRenderUpdate(pos, pos);
	}
	
	@Override
    public SPacketUpdateTileEntity getUpdatePacket()
    {
        return new SPacketUpdateTileEntity(pos, 0, this.getUpdateTag());
    }

	@Override
    public NBTTagCompound getUpdateTag()
    {
        return this.writeToNBT(new NBTTagCompound());
    }

	@Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt)
    {
        NBTTagCompound nbttagcompound = pkt.getNbtCompound();
        this.readFromNBT(nbttagcompound);
    }
	
	public int getBlockMetadata()
	{
		return ((BlockBookstand) worldObj.getBlockState(pos).getBlock()).getMetaFromState(worldObj.getBlockState(pos));
	}
}
