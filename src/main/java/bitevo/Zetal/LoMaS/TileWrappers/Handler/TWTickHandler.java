package bitevo.Zetal.LoMaS.TileWrappers.Handler;

import java.util.Iterator;

import bitevo.Zetal.LoMaS.TileWrappers.TileWrapper.TileWrapper;
import bitevo.Zetal.LoMaS.TileWrappers.TileWrapper.TileWrapperContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.tileentity.TileEntityPiston;
import net.minecraft.util.ITickable;
import net.minecraft.util.ReportedException;
import net.minecraft.util.Timer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ClientTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.WorldTickEvent;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TWTickHandler
{
	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void clientTickStart(ClientTickEvent event)
	{
		if (event.phase == TickEvent.Phase.START)
		{
			if(Minecraft.getMinecraft().theWorld != null)
			{
				Timer timer = ReflectionHelper.getPrivateValue(Minecraft.class, Minecraft.getMinecraft(), 20);
		        for (int j = 0; j < timer.elapsedTicks; ++j)
		        {
					this.worldTickStart(event, Minecraft.getMinecraft().theWorld);
					//System.out.println("client tick");
		        }
			}
		}
	}

	@SubscribeEvent
	public void serverTickStart(WorldTickEvent event)
	{
		if (event.phase == TickEvent.Phase.START)
		{
			this.worldTickStart(event, event.world);
			//System.out.println("server tick");
		}
	}

	private void worldTickStart(TickEvent event, World world)
	{
		if (event.phase == TickEvent.Phase.START)
		{
			TileWrapperContainer container = TileWrapperContainer.instance;
			container.processingLoadedTiles = true;

			for (BlockPos loadPos : container.loadedTileWrapperList.keySet())
			{
				Iterator<TileWrapper> iterator = container.loadedTileWrapperList.get(loadPos).iterator();
				while (iterator.hasNext())
				{
					TileWrapper tilewrapper = (TileWrapper) iterator.next();

					if (!tilewrapper.isInvalid() && tilewrapper.hasWorld())
					{
						BlockPos blockpos = tilewrapper.getPos();

						if (world.isBlockLoaded(blockpos, false) && world.getWorldBorder().contains(blockpos))
						{
							try
							{
								world.theProfiler.startSection(tilewrapper.getClass().getSimpleName());
								tilewrapper.setWorld(world);
								((ITickable) tilewrapper).update();
								world.theProfiler.endSection();
							}
							catch (Throwable throwable)
							{
								CrashReport crashreport2 = CrashReport.makeCrashReport(throwable, "Ticking block entity");
								CrashReportCategory crashreportcategory2 = crashreport2.makeCategory("Block entity being ticked");
								tilewrapper.addInfoToCrashReport(crashreportcategory2);
								if (net.minecraftforge.common.ForgeModContainer.removeErroringTileEntities)
								{
									net.minecraftforge.fml.common.FMLLog.severe(crashreport2.getCompleteReport());
									tilewrapper.invalidate();
									container.removeTileWrapper(tilewrapper.getPos(), tilewrapper.getIndex());
								}
								else
									throw new ReportedException(crashreport2);
							}
						}
					}

					if (tilewrapper.isInvalid())
					{
						iterator.remove();
						container.loadedTileWrapperList.remove(tilewrapper);
					}
				}
			}

			if (!container.tileWrappersToBeRemoved.isEmpty())
			{
				for (BlockPos removePos : container.tileWrappersToBeRemoved.keySet())
				{
					for (TileWrapper wrapper : container.tileWrappersToBeRemoved.get(removePos))
					{
						container.removeTileWrapper(removePos, wrapper.getIndex());
					}
				}
				container.tileWrappersToBeRemoved.clear();
			}

			container.processingLoadedTiles = false;

			world.theProfiler.endStartSection("pendingBlockEntities");

			if (!container.addedTileWrapperList.isEmpty())
			{
				for (BlockPos addPos : container.addedTileWrapperList.keySet())
				{
					if (!container.addedTileWrapperList.get(addPos).isEmpty())
					{
						for (int j1 = 0; j1 < container.addedTileWrapperList.size(); ++j1)
						{
							TileWrapper tilewrapper1 = (TileWrapper) container.addedTileWrapperList.get(addPos).get(j1);

							if (!tilewrapper1.isInvalid())
							{
								if (container.loadedTileWrapperList.get(addPos) == null || !container.loadedTileWrapperList.get(addPos).contains(tilewrapper1))
								{
									container.addTileWrapper(tilewrapper1);
								}

								if (world.isBlockLoaded(tilewrapper1.getPos()))
								{
									Chunk chunk = world.getChunkFromBlockCoords(tilewrapper1.getPos());
									IBlockState iblockstate = chunk.getBlockState(tilewrapper1.getPos());
									world.notifyBlockUpdate(tilewrapper1.getPos(), iblockstate, iblockstate, 3);
								}
							}
						}
					}
				}

				container.addedTileWrapperList.clear();
			}
		}
	}
}