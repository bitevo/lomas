package bitevo.Zetal.LoMaS.Specializations.Items;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDirt;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import bitevo.Zetal.LoMaS.Specializations.FoodInitializer;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;

public class ItemCustomHoe extends ItemHoe
{
	protected Item.ToolMaterial theToolMaterial;

	public ItemCustomHoe(Item.ToolMaterial material)
	{
		super(material);
		this.theToolMaterial = material;
		this.maxStackSize = 1;
		this.setMaxDamage(material.getMaxUses());
		this.setCreativeTab(CreativeTabs.TOOLS);
	}

	@Override
	public int getMaxDamage(ItemStack stack)
	{
		return SpecializationsMod.getMaxDamage(stack);
	}

	// adds 'tooltip' text
	@SideOnly(Side.CLIENT)
	@SuppressWarnings("unchecked")
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List tooltip, boolean advanced)
	{
		SpecializationsMod.addInformation(stack, playerIn, tooltip, advanced);
	}

	/**
	 * Called when a Block is right-clicked with this Item
	 * 
	 * @param pos
	 *            The block being right-clicked
	 * @param side
	 *            The side being right-clicked
	 */
	public boolean onItemUse(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		if (!playerIn.canPlayerEdit(pos.offset(side), side, stack))
		{
			return false;
		}
		else
		{
			int hook = net.minecraftforge.event.ForgeEventFactory.onHoeUse(stack, playerIn, worldIn, pos);
			if (hook != 0) return hook > 0;

			IBlockState iblockstate = worldIn.getBlockState(pos);
			Block block = iblockstate.getBlock();

			if (side != EnumFacing.DOWN && worldIn.isAirBlock(pos.up()))
			{
				if (block == Blocks.GRASS)
				{
					return this.useHoe(stack, playerIn, worldIn, pos, FoodInitializer.FARMLAND.getDefaultState());
				}

				if (block == Blocks.DIRT)
				{
					switch (ItemCustomHoe.SwitchDirtType.TYPE_LOOKUP[((BlockDirt.DirtType) iblockstate.getValue(BlockDirt.VARIANT)).ordinal()])
					{
						case 1:
							return this.useHoe(stack, playerIn, worldIn, pos, FoodInitializer.FARMLAND.getDefaultState());
						case 2:
							return this.useHoe(stack, playerIn, worldIn, pos, Blocks.DIRT.getDefaultState().withProperty(BlockDirt.VARIANT, BlockDirt.DirtType.DIRT));
					}
				}
			}

			return false;
		}
	}

	protected boolean useHoe(ItemStack stack, EntityPlayer player, World worldIn, BlockPos target, IBlockState newState)
	{
        worldIn.playSound(player, target, SoundEvents.ITEM_HOE_TILL, SoundCategory.BLOCKS, 1.0F, 1.0F);

		if (worldIn.isRemote)
		{
			return true;
		}
		else
		{
			worldIn.setBlockState(target, newState);
			stack.damageItem(1, player);
			return true;
		}
	}

	/**
	 * Returns True is the item is renderer in full 3D when hold.
	 */
	@SideOnly(Side.CLIENT)
	public boolean isFull3D()
	{
		return true;
	}

	/**
	 * Returns the name of the material this tool is made from as it is declared in EnumToolMaterial (meaning diamond would return "EMERALD")
	 */
	public String getMaterialName()
	{
		return this.theToolMaterial.toString();
	}

	static final class SwitchDirtType
	{
		static final int[] TYPE_LOOKUP = new int[BlockDirt.DirtType.values().length];
		private static final String __OBFID = "CL_00002179";

		static
		{
			try
			{
				TYPE_LOOKUP[BlockDirt.DirtType.DIRT.ordinal()] = 1;
			}
			catch (NoSuchFieldError var2)
			{
				;
			}

			try
			{
				TYPE_LOOKUP[BlockDirt.DirtType.COARSE_DIRT.ordinal()] = 2;
			}
			catch (NoSuchFieldError var1)
			{
				;
			}
		}
	}
}
