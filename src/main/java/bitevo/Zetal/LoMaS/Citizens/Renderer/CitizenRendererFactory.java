package bitevo.Zetal.LoMaS.Citizens.Renderer;

import bitevo.Zetal.LoMaS.Citizens.Entity.EntityPigmanCitizen;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraftforge.fml.client.registry.IRenderFactory;

public class CitizenRendererFactory implements IRenderFactory<EntityPigmanCitizen>
{
	@Override
	public Render<? super EntityPigmanCitizen> createRenderFor(RenderManager manager)
	{
		return new RenderPigman(manager, new ModelPigman(), 0.5F);
	}
}
