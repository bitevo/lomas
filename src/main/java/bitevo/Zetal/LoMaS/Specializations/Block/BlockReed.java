package bitevo.Zetal.LoMaS.Specializations.Block;

import java.util.Iterator;
import java.util.Random;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.FoodInitializer;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntityPlant;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockReed extends BlockContainer implements net.minecraftforge.common.IPlantable
{
	public static final PropertyInteger AGE = PropertyInteger.create("age", 0, 15);
	protected static final AxisAlignedBB REED_AABB = new AxisAlignedBB(0.125D, 0.0D, 0.125D, 0.875D, 1.0D, 0.875D);

	public BlockReed()
	{
		super(Material.PLANTS);
		this.setDefaultState(this.blockState.getBaseState().withProperty(AGE, Integer.valueOf(0)));
		this.setTickRandomly(true);
	}

	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		return REED_AABB;
	}

	@Override
    public EnumBlockRenderType getRenderType(IBlockState state)
    {
        return EnumBlockRenderType.MODEL;
    }

	@Override
	public IBlockState onBlockPlaced(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
	{
		if (world.getTileEntity(pos) != null && world.getTileEntity(pos) instanceof TileEntityPlant && placer instanceof EntityPlayer)
		{
			TileEntityPlant tile = (TileEntityPlant) world.getTileEntity(pos.up());
			tile.setOwner(placer.getPersistentID());
		}
		return this.getStateFromMeta(meta);
	}

	@Override
	public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random rand)
	{
		if (worldIn.getBlockState(pos.down()).getBlock() == FoodInitializer.REEDS_BLOCK || this.checkForDrop(worldIn, pos, state))
		{
			if (worldIn.canSeeSky(pos) && worldIn.getLightFromNeighbors(pos.up()) >= 9 && worldIn.isDaytime())
			{
				if (worldIn.isAirBlock(pos.up()))
				{
					int i;

					for (i = 1; worldIn.getBlockState(pos.down(i)).getBlock() == this; ++i)
					{
						;
					}

					TileEntityPlant plant = null;
					boolean hasTileEntity = false;
					if (worldIn.getTileEntity(pos) != null && worldIn.getTileEntity(pos) instanceof TileEntityPlant)
					{
						hasTileEntity = true;
						plant = (TileEntityPlant) worldIn.getTileEntity(pos);
					}

					if (i < 3)
					{
						int j = ((Integer) state.getValue(AGE)).intValue();

						int timer = 150;
						if (hasTileEntity)
						{
							plant = (TileEntityPlant) worldIn.getTileEntity(pos);

							LoMaS_Player lPlayer = plant.getlPlayer();
							if(lPlayer != null && lPlayer.getSkills() != null)
							{
								float growthSpeedMod = Math.max(0.1f, 1.0f - lPlayer.getSkills().getPercentMagnitudeFromName("Crop Growth spd"));
								timer *= growthSpeedMod;
							}
						}

						if (j * 10 == timer)
						{
							worldIn.setBlockState(pos.up(), this.getDefaultState());
							worldIn.setBlockState(pos, state.withProperty(AGE, Integer.valueOf(0)), 4);
							if (worldIn.getTileEntity(pos.up()) != null && worldIn.getTileEntity(pos.up()) instanceof TileEntityPlant && worldIn.getTileEntity(pos) != null && worldIn.getTileEntity(pos) instanceof TileEntityPlant)
							{
								TileEntityPlant upperPlant = (TileEntityPlant) worldIn.getTileEntity(pos.up());
								TileEntityPlant lowerPlant = (TileEntityPlant) worldIn.getTileEntity(pos);
								upperPlant.setOwner(lowerPlant.getOwner());
							}
						}
						else
						{
							worldIn.setBlockState(pos, state.withProperty(AGE, Integer.valueOf(j + 1)), 4);
						}
					}
				}
			}
		}
	}

	@Override
	public boolean canPlaceBlockAt(World worldIn, BlockPos pos)
	{
		//System.out.println("STep1");
        IBlockState state = worldIn.getBlockState(pos.down());
        Block block = state.getBlock();
		if (SpecializationsMod.canSustainPlant(worldIn, pos.down(), EnumFacing.UP, this)) return true;
		
		//System.out.println("STep2");
		if (block == this)
		{
			//System.out.println("STep2a");
			return true;
		}
		else if (block != Blocks.GRASS && block != Blocks.DIRT && block != Blocks.SAND)
		{
			//System.out.println("STep2b");
			return false;
		}
		else
		{
			//System.out.println("STep2c");
			Iterator iterator = EnumFacing.Plane.HORIZONTAL.iterator();
			EnumFacing enumfacing;

			do
			{
				if (!iterator.hasNext())
				{
					return false;
				}

				enumfacing = (EnumFacing) iterator.next();
			} while (worldIn.getBlockState(pos.offset(enumfacing).down()).getMaterial() != Material.WATER);

			//System.out.println("STep2d");
			return true;
		}
	}

	/**
	 * Called when a neighboring block changes.
	 */
	@Override
    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn)
	{
		this.checkForDrop(worldIn, pos, state);
	}

    //Is not Override; used in local methods
    protected final boolean checkForDrop(World worldIn, BlockPos pos, IBlockState state)
	{
        if (this.canBlockStay(worldIn, pos))
        {
            return true;
        }
        else
        {
            this.dropBlockAsItem(worldIn, pos, state, 0);
            worldIn.setBlockToAir(pos);
            return false;
        }
	}

    //Is not Override; used in local methods
    public boolean canBlockStay(World worldIn, BlockPos pos)
	{
		return this.canPlaceBlockAt(worldIn, pos);
	}

	@Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, World worldIn, BlockPos pos)
	{
		return NULL_AABB;
	}

	/**
	 * Get the Item that this Block should drop when harvested.
	 *
	 * @param fortune
	 *            the level of the Fortune enchantment on the player's tool
	 */
	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune)
	{
		return FoodInitializer.REEDS;
	}

	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}

	/**
	 * Convert the given metadata into a BlockState for this Block
	 */
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty(AGE, Integer.valueOf(meta));
	}

	@Override
	@SideOnly(Side.CLIENT)
	public BlockRenderLayer getBlockLayer()
	{
		return BlockRenderLayer.CUTOUT;
	}

	/**
	 * Convert the BlockState into the correct metadata value
	 */
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return ((Integer) state.getValue(AGE)).intValue();
	}

	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, new IProperty[] { AGE });
	}

	@Override
	public net.minecraftforge.common.EnumPlantType getPlantType(IBlockAccess world, BlockPos pos)
	{
		return net.minecraftforge.common.EnumPlantType.Beach;
	}

	@Override
	public IBlockState getPlant(IBlockAccess world, BlockPos pos)
	{
		return this.getDefaultState();
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileEntityPlant();
	}
}