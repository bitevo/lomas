package bitevo.Zetal.LoMaS.Economy.GUI;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.lwjgl.opengl.GL11;

import com.google.common.collect.Lists;

import bitevo.Zetal.LoMaS.Economy.EconomyMod;
import bitevo.Zetal.LoMaS.Economy.Inventory.ContainerTrade;
import bitevo.Zetal.LoMaS.Economy.Inventory.InventoryTrade;
import bitevo.Zetal.LoMaS.Economy.Message.TradeMessage;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiTrade extends GuiContainer
{
	private static final ResourceLocation trade = new ResourceLocation(EconomyMod.modid, "gui/player_trading.png");
	private ContainerTrade container;
	private Random rand = new Random();
	private EntityPlayer thePlayer;

	public GuiTrade(InventoryPlayer inventoryPlayer, InventoryTrade trade)
	{
		super(new ContainerTrade(inventoryPlayer, trade));
		this.thePlayer = inventoryPlayer.player;
		this.container = (ContainerTrade) this.inventorySlots;
	}

	@Override
	public void initGui()
	{
		super.initGui();
		this.drawGuiContainerBackgroundLayer(0, 0, 0);
		this.drawGuiContainerForegroundLayer(0, 0);
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		super.drawScreen(mouseX, mouseY, partialTicks);
		if (this.isPointInRegion(76, 42, 24, 24, mouseX, mouseY))
		{
			List<String> list = Lists.<String> newArrayList();
			list.add(this.getHoverText());
			this.drawHoveringText(list, mouseX, mouseY);
		}
	}

	public String getHoverText()
	{
		return this.container.canTradeFit() ? "Ready" : "Not enough room";
	}

	/**
	 * Called when the mouse is clicked. Args : mouseX, mouseY, clickedButton
	 */
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);
		int i = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;

		int l = mouseX - (i + 76);
		int i1 = mouseY - (j + 42);

		if (l >= 0 && i1 >= 0 && l < 24 && i1 < 24)
		{
			// System.out.println("telling server: " + !this.container.theReady);
			EconomyMod.snw.sendToServer(new TradeMessage(Minecraft.getMinecraft().thePlayer.getPersistentID(), !this.container.theReady, this.container.traderReady));
		}
	}

	/**
	 * Draw the foreground layer for the GuiContainer (everything in front of the items)
	 */
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		this.mc.thePlayer.inventoryContainer.detectAndSendChanges();
		int coins = 0;
		for (int i = 0; i < 3; i++)
		{
			if (this.mc.thePlayer.inventory.getStackInSlot(41 + i) instanceof ItemStack)
			{
				coins += ((ItemStack) this.mc.thePlayer.inventory.getStackInSlot(41 + i)).stackSize;
			}
		}
		if (coins > 64)
		{
			coins = 64;
		}
		this.fontRendererObj.drawString(coins + "/64", 92, 14, 4210752);
	}

	/**
	 * Draw the background layer for the GuiContainer (everything behind the items)
	 */
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(trade);
		int k = (this.width - this.xSize) / 2;
		int l = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);

		if (this.container.theReady)
		{
			this.drawTexturedModalRect(k + 76, l + 42, 232, 0, 24, 14);
		}

		if (this.container.traderReady)
		{
			this.drawTexturedModalRect(k + 76, l + 53, 207, 0, 24, 13);
		}
	}
}
