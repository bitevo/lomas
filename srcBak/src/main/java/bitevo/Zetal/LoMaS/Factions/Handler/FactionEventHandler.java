package bitevo.Zetal.LoMaS.Factions.Handler;

import java.util.UUID;

import bitevo.Zetal.LoMaS.Factions.FactionsMod;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class FactionEventHandler
{
	public AttributeModifier roadMod = new AttributeModifier(new UUID(77777777L, 88888888L), "roadmod", 0.025D, 0);
	
	@SubscribeEvent
	public void onPlayerRoad(LivingUpdateEvent event)
	{
		if(event.getEntityLiving() instanceof EntityPlayer && event.getEntityLiving().ticksExisted % 20 == 0)
		{
			EntityPlayer player = (EntityPlayer) event.getEntityLiving();
			World world = player.worldObj;
			BlockPos pos = new BlockPos(MathHelper.floor_double(player.posX), MathHelper.floor_double(player.posY), MathHelper.floor_double(player.posZ));
			IBlockState state = world.getBlockState(pos.down());
			boolean isRoad = state.getBlock() == FactionsMod.ROAD || state.getBlock() == FactionsMod.ROAD_SLAB 
			|| state.getBlock() == FactionsMod.ROAD_STAIRS || state.getBlock() == FactionsMod.ROAD_DOUBLE_SLAB;
			if(!isRoad)
			{
				state = world.getBlockState(pos);
				isRoad = state.getBlock() == FactionsMod.ROAD || state.getBlock() == FactionsMod.ROAD_SLAB 
				|| state.getBlock() == FactionsMod.ROAD_STAIRS || state.getBlock() == FactionsMod.ROAD_DOUBLE_SLAB;
			}
			if(isRoad && player.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getModifier(this.roadMod.getID()) == null)
			{
				player.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).applyModifier(roadMod);
			}
			else if(!isRoad && player.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getModifier(this.roadMod.getID()) != null)
			{
				player.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).removeModifier(roadMod);
			}
		}
	}
	
	@SubscribeEvent
	public void obeliskInteract(PlayerInteractEvent.RightClickBlock event)
	{
		EntityPlayer player = event.getEntityPlayer();
		World world = event.getWorld();
		BlockPos pos = event.getPos();
		IBlockState state = world.getBlockState(pos);
		Block block = state.getBlock();
		if(block == FactionsMod.OBELISK)
		{
			
		}
	}
}
