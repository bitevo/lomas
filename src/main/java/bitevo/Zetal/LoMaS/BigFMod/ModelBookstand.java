package bitevo.Zetal.LoMaS.BigFMod;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelBookstand extends ModelBase
{
	// fields
	ModelRenderer lip;
	ModelRenderer backboard;
	ModelRenderer support;

	public ModelBookstand()
	{
		textureWidth = 40;
		textureHeight = 40;

		lip = new ModelRenderer(this, 0, 22);
		lip.addBox(0F, 0F, 0F, 3, 1, 15);
		lip.setRotationPoint(-4F, 22.4F, -7.5F);
		lip.setTextureSize(40, 40);
		lip.mirror = true;
		setRotation(lip, 0F, 0F, 0.2185276F);
		backboard = new ModelRenderer(this, -13, 5);
		backboard.addBox(0F, 0F, 0F, 10, 1, 16);
		backboard.setRotationPoint(-2F, 23.46667F, -8F);
		backboard.setTextureSize(40, 40);
		backboard.mirror = true;
		setRotation(backboard, 0F, 0F, -1.006094F);
		support = new ModelRenderer(this, -2, 1);
		support.addBox(0F, 0F, 0F, 5, 1, 16);
		support.setRotationPoint(1F, 20.46667F, -8F);
		support.setTextureSize(40, 40);
		support.mirror = true;
		setRotation(support, 0F, 0F, 0.5948578F);
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		super.render(entity, f, f1, f2, f3, f4, f5);
		setRotationAngles(f, f1, f2, f3, f4, f5, entity);
		lip.render(f5);
		backboard.render(f5);
		support.render(f5);
	}

	public void renderModel(float f5)
	{
		lip.render(f5);
		backboard.render(f5);
		support.render(f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
	{
		super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
	}

}
