package bitevo.Zetal.LoMaS.Economy;

import bitevo.Zetal.LoMaS.Economy.GUI.GuiBank;
import bitevo.Zetal.LoMaS.Economy.GUI.GuiSafe;
import bitevo.Zetal.LoMaS.Economy.GUI.GuiTrade;
import bitevo.Zetal.LoMaS.Economy.Inventory.ContainerBank;
import bitevo.Zetal.LoMaS.Economy.Inventory.ContainerSafe;
import bitevo.Zetal.LoMaS.Economy.Inventory.ContainerTrade;
import bitevo.Zetal.LoMaS.Economy.Inventory.InventoryTrade;
import bitevo.Zetal.LoMaS.Economy.TileEntity.TileEntitySafe;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class EconCommonProxy implements IGuiHandler
{
	public EconCommonProxy()
	{
	}

	public void init()
	{
	}

	public void registerRenderInformation()
	{
		// unused server side. -- see ClientProxy for implementation
	}

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		switch (ID)
		{
			case 0:
				return new ContainerSafe(player.inventory, (TileEntitySafe) world.getTileEntity(new BlockPos(x, y, z)));
			case 1:
				return new ContainerBank(player.inventory, player);
			case 2:
				return new ContainerTrade(player.inventory, new InventoryTrade());
		}
		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		switch (ID)
		{
			case 0:
				return new GuiSafe(player.inventory, (TileEntitySafe) world.getTileEntity(new BlockPos(x, y, z)));
			case 1:
				return new GuiBank(player);
			case 2:
				return new GuiTrade(player.inventory, new InventoryTrade());
		}
		return null;
	}
}
