package bitevo.Zetal.LoMaS.Specializations.TileEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockSpecBrewingStand;
import bitevo.Zetal.LoMaS.Specializations.Inventory.SpecContainerBrewingStand;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.PotionTypes;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.potion.PotionEffect;
import net.minecraft.potion.PotionType;
import net.minecraft.potion.PotionUtils;
import net.minecraft.tileentity.TileEntityLockable;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.brewing.BrewingRecipeRegistry;
import net.minecraftforge.common.brewing.IBrewingRecipe;
import net.minecraftforge.fml.relauncher.ReflectionHelper;

public class TileEntitySpecBrewingStand extends TileEntityLockable implements ITickable, ISidedInventory
{
	/** an array of the input slot indices */
	private static final int[] SLOTS_FOR_UP = new int[] { 3 };
	private static final int[] SLOTS_FOR_DOWN = new int[] { 0, 1, 2, 3 };
	/** an array of the output slot indices */
	private static final int[] OUTPUT_SLOTS = new int[] { 0, 1, 2, 4 };
	/** The ItemStacks currently placed in the slots of the brewing stand */
	private ItemStack[] brewingItemStacks = new ItemStack[5];
	private int brewTime;
	/** an integer with each bit specifying whether that slot of the stand contains a potion */
	private boolean[] filledSlots;
	/** used to check if the current ingredient has been removed from the brewing stand during brewing */
	private Item ingredientID;
	private String customName;
	private int fuel;

	private boolean[] isFresh = new boolean[] { false, false, false, false };
	private static HashMap<Item, Item> convertTypes = new HashMap<Item, Item>();
	{
		convertTypes.put(Items.POTIONITEM, SpecializationsMod.COMPACT_POTION);
		convertTypes.put(Items.LINGERING_POTION, SpecializationsMod.COMPACT_LINGERING_POTION);
		convertTypes.put(Items.SPLASH_POTION, SpecializationsMod.COMPACT_SPLASH_POTION);
		convertTypes.put(SpecializationsMod.COMPACT_POTION, Items.POTIONITEM);
		convertTypes.put(SpecializationsMod.COMPACT_LINGERING_POTION, Items.LINGERING_POTION);
		convertTypes.put(SpecializationsMod.COMPACT_SPLASH_POTION, Items.SPLASH_POTION);
	}
	private int modTime = 400;

	private UUID owner;
	private LoMaS_Player lPlayer;

	/**
	 * Gets the name of this command sender (usually username, but possibly "Rcon")
	 */
	@Override
	public String getName()
	{
		return this.hasCustomName() ? this.customName : "container.brewing";
	}

	/**
	 * Returns true if this thing is named
	 */
	@Override
	public boolean hasCustomName()
	{
		return this.customName != null && this.customName.length() > 0;
	}

	public void func_145937_a(String p_145937_1_)
	{
		this.customName = p_145937_1_;
	}

	/**
	 * Returns the number of slots in the inventory.
	 */
	@Override
	public int getSizeInventory()
	{
		return this.brewingItemStacks.length;
	}

	/**
	 * Updates the JList with a new model.
	 */
	@Override
	public void update()
	{
		if (this.getlPlayer() != null)
		{
			if (this.fuel <= 0 && this.brewingItemStacks[4] != null && this.brewingItemStacks[4].getItem() == Items.BLAZE_POWDER)
			{
				this.fuel = 20;
				--this.brewingItemStacks[4].stackSize;

				if (this.brewingItemStacks[4].stackSize <= 0)
				{
					this.brewingItemStacks[4] = null;
				}

				this.markDirty();
			}

			boolean flag = this.canBrew();
			boolean flag1 = this.brewTime > 0;

			if (flag1)
			{
				--this.brewTime;
				boolean flag2 = this.brewTime == 0;

				if (flag2 && flag)
				{
					this.brewPotions();
					this.markDirty();
				}
				else if (!flag)
				{
					this.brewTime = 0;
					this.markDirty();
				}
				else if (this.ingredientID != this.brewingItemStacks[3].getItem())
				{
					this.brewTime = 0;
					this.markDirty();
				}
			}
			else if (flag && this.fuel > 0)
			{
				--this.fuel;
				this.brewTime = this.getTotalTime();
				this.ingredientID = this.brewingItemStacks[3].getItem();
				this.markDirty();
			}

			if (!this.worldObj.isRemote)
			{
				boolean[] aboolean = this.createFilledSlotsArray();

				if (!Arrays.equals(aboolean, this.filledSlots))
				{
					this.filledSlots = aboolean;
					IBlockState iblockstate = this.worldObj.getBlockState(this.getPos());

					if (!(iblockstate.getBlock() instanceof BlockSpecBrewingStand))
					{
						return;
					}

					for (int i = 0; i < BlockSpecBrewingStand.HAS_BOTTLE.length; ++i)
					{
						iblockstate = iblockstate.withProperty(BlockSpecBrewingStand.HAS_BOTTLE[i], Boolean.valueOf(aboolean[i]));
					}

					this.worldObj.setBlockState(this.pos, iblockstate, 2);
				}
			}
		}
	}

	public boolean[] createFilledSlotsArray()
	{
		boolean[] aboolean = new boolean[3];

		for (int i = 0; i < 3; ++i)
		{
			if (this.brewingItemStacks[i] != null)
			{
				aboolean[i] = true;
			}
		}

		return aboolean;
	}

	@Override
	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newState)
	{
		return !oldState.getBlock().equals(newState.getBlock());
	}

	public int getTotalTime()
	{
		modTime = 400;
		float speed = 1.0f - this.getlPlayer().getSkills().getPercentMagnitudeFromName("Brewing spd")- this.getlPlayer().getSkills().getPercentMagnitudeFromName("Rune/Brewing spd");
		modTime *= speed;
		return this.modTime;
	}

	private boolean canBrew()
	{
		return this.canBrew(brewingItemStacks, brewingItemStacks[3], OUTPUT_SLOTS);
	}

	private void brewPotions()
	{
		if (net.minecraftforge.event.ForgeEventFactory.onPotionAttemptBrew(brewingItemStacks)) return;
		ItemStack itemstack = this.brewingItemStacks[3];

		int chance = 0;
		Random rand = new Random();
		for (int i = 0; i < OUTPUT_SLOTS.length; i++)
		{
			int out_id = OUTPUT_SLOTS[i];
			ItemStack output = this.getOutput(brewingItemStacks[i], brewingItemStacks[3]);
			PotionType potionType = PotionUtils.getPotionFromItem(output);
			ArrayList<PotionEffect> newEffects = new ArrayList<PotionEffect>();
			for(PotionEffect effect : potionType.getEffects())
			{
				if(effect.getPotion().isBadEffect())
				{
					int ampMod = this.getlPlayer().getSkills().getPercentMagnitudeFromName("Negative Potion Brewing") >= rand.nextInt(100) ? 1 : 0;
					float durMod = 1.0f + this.getlPlayer().getSkills().getPercentMagnitudeFromName("Negative Potion Duration");
					effect = new PotionEffect(effect.getPotion(), (int) (effect.getDuration() * durMod), effect.getAmplifier() + ampMod, effect.getIsAmbient(), effect.doesShowParticles());
				}
				else if(effect.getPotion().isBeneficial()) 
				{
					int ampMod = this.getlPlayer().getSkills().getPercentMagnitudeFromName("Beneficial Potion Brewing") >= rand.nextInt(100) ? 1 : 0;
					float durMod = 1.0f + this.getlPlayer().getSkills().getPercentMagnitudeFromName("Beneficial Potion Duration");
					effect = new PotionEffect(effect.getPotion(), (int) (effect.getDuration() * durMod), effect.getAmplifier() + ampMod, effect.getIsAmbient(), effect.doesShowParticles());
				}
				newEffects.add(effect);
			}
			potionType = new PotionType((String) ReflectionHelper.getPrivateValue(PotionType.class, potionType, 3), (PotionEffect[]) newEffects.toArray());
			
			if (output != null && potionType != PotionTypes.WATER && potionType != PotionTypes.AWKWARD && potionType != PotionTypes.MUNDANE && potionType != PotionTypes.THICK)
			{
				int potionCount = output.stackSize;
				Item potionItem = output.getItem();
				if (this.getlPlayer().getSkills().getMatchingNodesFromName("Brewed potions stack") >= 1)
				{
					if(output.getMaxStackSize() == 1)
					{
						potionItem = this.convertTypes.get(output.getItem());
					}
					ItemStack compactPotion = new ItemStack(potionItem, potionCount);
					PotionUtils.addPotionToItemStack(compactPotion, potionType);
					this.brewingItemStacks[out_id] = compactPotion;
				}
				else
				{
					if(output.getMaxStackSize() > 1)
					{
						potionItem = this.convertTypes.get(output.getItem());
					}
					ItemStack normalPotion = new ItemStack(potionItem, potionCount);
					PotionUtils.addPotionToItemStack(normalPotion, potionType);
					this.brewingItemStacks[out_id] = normalPotion;
				}

				chance += this.getlPlayer().getSkills().getMagnitudeFromName("Double potion/rune crafting");
				if (rand.nextInt(100) < chance)
				{
					this.brewingItemStacks[out_id].stackSize++;
				}

				this.setIsFresh(true, i);
				// System.out.println("Set fresh for " + i);
			}
		}

		this.brewPotions(brewingItemStacks, brewingItemStacks[3], OUTPUT_SLOTS);

		--itemstack.stackSize;
		BlockPos blockpos = this.getPos();

		if (itemstack.getItem().hasContainerItem(itemstack))
		{
			ItemStack itemstack1 = itemstack.getItem().getContainerItem(itemstack);

			if (itemstack.stackSize <= 0)
			{
				itemstack = itemstack1;
			}
			else
			{
				InventoryHelper.spawnItemStack(this.worldObj, (double) blockpos.getX(), (double) blockpos.getY(), (double) blockpos.getZ(), itemstack1);
			}
		}

		if (itemstack.stackSize <= 0)
		{
			itemstack = null;
		}

		this.brewingItemStacks[3] = itemstack;
		this.worldObj.playEvent(1035, blockpos, 0);
		net.minecraftforge.event.ForgeEventFactory.onPotionBrewed(brewingItemStacks);
	}

	@Override
	public void readFromNBT(NBTTagCompound compound)
	{
		super.readFromNBT(compound);
		NBTTagList nbttaglist = compound.getTagList("Items", 10);
		this.brewingItemStacks = new ItemStack[this.getSizeInventory()];

		for (int i = 0; i < nbttaglist.tagCount(); ++i)
		{
			NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
			byte b0 = nbttagcompound1.getByte("Slot");

			if (b0 >= 0 && b0 < this.brewingItemStacks.length)
			{
				this.brewingItemStacks[b0] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
			}
		}

		NBTTagList nbttaglist2 = compound.getTagList("isFresh", 10);

		for (int i = 0; i < nbttaglist2.tagCount(); ++i)
		{
			NBTTagCompound nbttagcompound2 = (NBTTagCompound) nbttaglist2.getCompoundTagAt(i);
			this.setIsFresh(nbttagcompound2.getBoolean("isFresh"), i);
		}

		this.brewTime = compound.getShort("BrewTime");
		if (compound.hasKey("owner"))
		{
			try
			{
				this.setOwner(UUID.fromString(compound.getString("owner")));
			}
			catch (Exception e)
			{
				this.setOwner(null);
			}
		}

		if (compound.hasKey("CustomName", 8))
		{
			this.customName = compound.getString("CustomName");
		}

		this.fuel = compound.getByte("Fuel");
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound)
	{
		super.writeToNBT(compound);
		compound.setShort("BrewTime", (short) this.brewTime);
		if (this.getOwner() != null)
		{
			try
			{
				compound.setString("owner", getOwner().toString());
			}
			catch (Exception e)
			{
			}
		}
		NBTTagList nbttaglist = new NBTTagList();

		for (int i = 0; i < this.brewingItemStacks.length; ++i)
		{
			if (this.brewingItemStacks[i] != null)
			{
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte) i);
				this.brewingItemStacks[i].writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}

		NBTTagList nbttaglist2 = new NBTTagList();

		for (int i = 0; i < this.OUTPUT_SLOTS.length; ++i)
		{
			if (this.brewingItemStacks[this.OUTPUT_SLOTS[i]] != null)
			{
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setBoolean("isFresh", this.getIsFresh()[i]);
				nbttaglist2.appendTag(nbttagcompound1);
			}
		}

		compound.setTag("Items", nbttaglist);
		compound.setTag("isFresh", nbttaglist2);

		if (this.hasCustomName())
		{
			compound.setString("CustomName", this.customName);
		}

		compound.setByte("Fuel", (byte) this.fuel);
		return compound;
	}

	/**
	 * Returns the stack in slot i
	 */
	@Override
	public ItemStack getStackInSlot(int index)
	{
		return index >= 0 && index < this.brewingItemStacks.length ? this.brewingItemStacks[index] : null;
	}

	/**
	 * Removes up to a specified number of items from an inventory slot and returns them in a new stack.
	 */
	@Nullable
	public ItemStack decrStackSize(int index, int count)
	{
		return ItemStackHelper.getAndSplit(this.brewingItemStacks, index, count);
	}

	/**
	 * Removes a stack from the given slot and returns it.
	 */
	@Nullable
	public ItemStack removeStackFromSlot(int index)
	{
		return ItemStackHelper.getAndRemove(this.brewingItemStacks, index);
	}

	/**
	 * Sets the given item stack to the specified slot in the inventory (can be crafting or armor sections).
	 */
	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		if (index >= 0 && index < this.brewingItemStacks.length)
		{
			this.brewingItemStacks[index] = stack;
		}
	}

	/**
	 * Returns the maximum stack size for a inventory slot. Seems to always be 64, possibly will be extended. *Isn't this more of a set than a get?*
	 */
	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}

	/**
	 * Do not make give this method the name canInteractWith because it clashes with Container
	 */
	@Override
	public boolean isUseableByPlayer(EntityPlayer player)
	{
		return this.worldObj.getTileEntity(this.pos) != this ? false : player.getDistanceSq((double) this.pos.getX() + 0.5D, (double) this.pos.getY() + 0.5D, (double) this.pos.getZ() + 0.5D) <= 64.0D;
	}

	@Override
	public void openInventory(EntityPlayer player)
	{
	}

	@Override
	public void closeInventory(EntityPlayer player)
	{
	}

	/**
	 * Returns true if automation is allowed to insert the given stack (ignoring stack size) into the given slot.
	 */
	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		if (index == 3)
		{
			return net.minecraftforge.common.brewing.BrewingRecipeRegistry.isValidIngredient(stack);
		}
		else
		{
			Item item = stack.getItem();
			return index == 4 ? item == Items.BLAZE_POWDER : this.isValidInput(stack);
		}
	}
	
	public static boolean isValidInput(ItemStack stack)
	{
		if(stack.getItem() instanceof ItemPotion)
		{
			return true;
		}
		return net.minecraftforge.common.brewing.BrewingRecipeRegistry.isValidInput(stack);
	}

	@Override
	public int[] getSlotsForFace(EnumFacing side)
	{
		return side == EnumFacing.UP ? SLOTS_FOR_UP : (side == EnumFacing.DOWN ? SLOTS_FOR_DOWN : OUTPUT_SLOTS);
	}

	/**
	 * Returns true if automation can insert the given item in the given slot from the given side. Args: slot, item, side
	 */
	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction)
	{
		return this.isItemValidForSlot(index, itemStackIn);
	}

	/**
	 * Returns true if automation can extract the given item in the given slot from the given side.
	 */
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction)
	{
		return index == 3 ? stack.getItem() == Items.GLASS_BOTTLE : true;
	}

	@Override
	public String getGuiID()
	{
		return "minecraft:brewing_stand";
	}

	@Override
	public Container createContainer(InventoryPlayer playerInventory, EntityPlayer playerIn)
	{
		return new SpecContainerBrewingStand(playerInventory, this);
	}

	@Override
	public int getField(int id)
	{
		switch (id)
		{
			case 0:
				return this.brewTime;
			case 1:
				return this.fuel;
			case 2:
				return this.booleansToInt(this.getIsFresh());
			case 3:
				return this.getTotalTime();
			default:
				return 0;
		}
	}

	@Override
	public void setField(int id, int value)
	{
		switch (id)
		{
			case 0:
				this.brewTime = value;
				break;
			case 1:
				this.fuel = value;
				break;
			case 2:
				this.setIsFresh(this.intToBooleans(value, this.getIsFresh().length));
				break;
			case 3:
				break;
			default:
		}
	}

	@Override
	public int getFieldCount()
	{
		return 4;
	}

	@Override
	public void clear()
	{
		for (int i = 0; i < this.brewingItemStacks.length; ++i)
		{
			this.brewingItemStacks[i] = null;
		}
	}

	net.minecraftforge.items.IItemHandler handlerInput = new net.minecraftforge.items.wrapper.SidedInvWrapper(this, net.minecraft.util.EnumFacing.UP);
	net.minecraftforge.items.IItemHandler handlerOutput = new net.minecraftforge.items.wrapper.SidedInvWrapper(this, net.minecraft.util.EnumFacing.DOWN);
	net.minecraftforge.items.IItemHandler handlerSides = new net.minecraftforge.items.wrapper.SidedInvWrapper(this, net.minecraft.util.EnumFacing.NORTH);

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getCapability(net.minecraftforge.common.capabilities.Capability<T> capability, net.minecraft.util.EnumFacing facing)
	{
		if (facing != null && capability == net.minecraftforge.items.CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if (facing == EnumFacing.UP)
				return (T) handlerInput;
			else if (facing == EnumFacing.DOWN)
				return (T) handlerOutput;
			else
				return (T) handlerSides;
		}
		return super.getCapability(capability, facing);
	}

	public boolean[] getIsFresh()
	{
		return isFresh;
	}

	public void setIsFresh(boolean[] isFresh)
	{
		this.isFresh = isFresh;
	}

	public void setIsFresh(boolean isFresh, int i)
	{
		this.isFresh[i] = isFresh;
	}

	public UUID getOwner()
	{
		return owner;
	}

	public void setOwner(UUID owner)
	{
		this.owner = owner;
		if (owner != null)
		{
			this.setlPlayer(LoMaS_Player.getLoMaSPlayer(owner));
		}
		else
		{
			System.out.println("Brewing Stand Set Owner Null???");
		}
	}

	public LoMaS_Player getlPlayer()
	{
		return lPlayer;
	}

	public void setlPlayer(LoMaS_Player lPlayer)
	{
		this.lPlayer = lPlayer;
	}

	@Override
	public SPacketUpdateTileEntity getUpdatePacket()
	{
		return new SPacketUpdateTileEntity(pos, 0, this.getUpdateTag());
	}

	@Override
	public NBTTagCompound getUpdateTag()
	{
		return this.writeToNBT(new NBTTagCompound());
	}

	@Override
	public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt)
	{
		NBTTagCompound nbttagcompound = pkt.getNbtCompound();
		this.readFromNBT(nbttagcompound);
	}


    /**
     * Returns the output ItemStack obtained by brewing the passed input and
     * ingredient. Null if no matches are found.
     */
    public static ItemStack getOutput(ItemStack input, ItemStack ingredient)
    {
        if (input == null || /*input.getMaxStackSize() != 1 ||*/ input.stackSize != 1) return null;
        if (ingredient == null || ingredient.stackSize <= 0) return null;

        for (IBrewingRecipe recipe : BrewingRecipeRegistry.getRecipes())
        {
            ItemStack output = recipe.getOutput(input, ingredient);
            if (output != null)
            {
                return output;
            }
        }
        return null;
    }

    /**
     * Returns true if the passed input and ingredient have an output
     */
    public static boolean hasOutput(ItemStack input, ItemStack ingredient)
    {
        return getOutput(input, ingredient) != null;
    }

    /**
     * Used by the brewing stand to determine if its contents can be brewed.
     * Extra parameters exist to allow modders to create bigger brewing stands
     * without much hassle
     */
    public static boolean canBrew(ItemStack[] inputs, ItemStack ingredient, int[] inputIndexes)
    {
        if (ingredient == null || ingredient.stackSize <= 0) return false;

        for (int i : inputIndexes)
        {
            if (hasOutput(inputs[i], ingredient))
            {
                return true;
            }
        }

        return false;
    }

    public static void brewPotions(ItemStack[] inputs, ItemStack ingredient, int[] inputIndexes)
    {
        for (int i : inputIndexes)
        {
            ItemStack output = getOutput(inputs[i], ingredient);
            if (output != null)
            {
                inputs[i] = output;
            }
        }
    }

	public static void printArray(boolean[] hi)
	{
		String complete = "[";
		for (int i = 0; i < hi.length; i++)
		{
			complete = complete + hi[i];
			if (i + 1 != hi.length)
			{
				complete = complete + ", ";
			}
		}
		complete = complete + "]";
		System.out.println(complete);
	}

	public static int booleansToInt(boolean[] arr)
	{
		int n = 0;
		for (boolean b : arr)
			n = (n << 1) | (b ? 1 : 0);
		return n;
	}

	public static boolean[] intToBooleans(int number, int length)
	{
		boolean[] ret = new boolean[length];
		for (int i = 0; i < length; i++)
		{
			ret[length - 1 - i] = (1 << i & number) != 0;
		}
		return ret;
	}
}