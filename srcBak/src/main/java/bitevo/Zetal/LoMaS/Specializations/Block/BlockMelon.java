package bitevo.Zetal.LoMaS.Specializations.Block;

import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.Specializations.FoodInitializer;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntityQualityBlock;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Enchantments;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockMelon extends Block implements ITileEntityProvider
{
	public BlockMelon()
	{
		super(Material.GOURD);
		this.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
		this.isBlockContainer = true;
	}

	/**
	 * Get the Item that this Block should drop when harvested.
	 * 
	 * @param fortune
	 *            the level of the Fortune enchantment on the player's tool
	 */
    @Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune)
	{
		return FoodInitializer.MELON;
	}
    
    public void harvestBlock(World worldIn, EntityPlayer player, BlockPos pos, IBlockState state, @Nullable TileEntity te, @Nullable ItemStack stack)
    {
        if (!worldIn.isRemote) //Forge: Noop this
        {
            player.addStat(StatList.getBlockStats(this));
            player.addExhaustion(0.025F);

            if (this.canSilkHarvest(worldIn, pos, state, player) && EnchantmentHelper.getEnchantmentLevel(Enchantments.SILK_TOUCH, stack) > 0)
            {
                java.util.List<ItemStack> items = new java.util.ArrayList<ItemStack>();
                ItemStack itemstack = this.createStackedBlock(state);

                if (itemstack != null)
                {
                    items.add(itemstack);
                }

                net.minecraftforge.event.ForgeEventFactory.fireBlockHarvesting(items, worldIn, pos, state, 0, 1.0f, true, player);
                for (ItemStack item : items)
                {
                    spawnAsEntity(worldIn, pos, item);
                }
            }
            else
            {
                int i = EnchantmentHelper.getEnchantmentLevel(Enchantments.FORTUNE, stack);
        		Random rand = worldIn instanceof World ? ((World) worldIn).rand : new Random();
                spawnAsEntity(worldIn, pos, new ItemStack(this.getItemDropped(state, rand, i), this.quantityDropped(state, i, rand), ((TileEntityQualityBlock) te).getQuality()));
            }
        }
        else
        {
            super.harvestBlock(worldIn, player, pos, state, te, stack);
        }
    }

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
    @Override
	public int quantityDropped(Random random)
	{
		return 3 + random.nextInt(5);
	}

	/**
	 * Get the quantity dropped based on the given fortune level
	 */
    @Override
	public int quantityDroppedWithBonus(int fortune, Random random)
	{
		return Math.min(9, this.quantityDropped(random) + random.nextInt(1 + fortune));
	}

	@SideOnly(Side.CLIENT)
    @Override
	public void getSubBlocks(Item itemIn, CreativeTabs tab, List list)
	{
		list.add(new ItemStack(itemIn, 1, 0));
		list.add(new ItemStack(itemIn, 1, 1));
		list.add(new ItemStack(itemIn, 1, 2));
	}

	/**
	 * Called on both Client and Server when World#addBlockEvent is called
	 */
	@Override
	public boolean eventReceived(IBlockState state, World worldIn, BlockPos pos, int eventID, int eventParam)
	{
		super.eventReceived(state, worldIn, pos, eventID, eventParam);
		TileEntity tileentity = worldIn.getTileEntity(pos);
		return tileentity == null ? false : tileentity.receiveClientEvent(eventID, eventParam);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileEntityQualityBlock();
	}
}
