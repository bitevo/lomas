package bitevo.Zetal.LoMaS.Specializations.Entity;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.Shared.Entity.EntityFactionMember;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.World;

public abstract class EntityFactionGolem extends EntityFactionMember
{

	public EntityFactionGolem(World worldIn)
	{
		super(worldIn);
	}

	@Override
    public void fall(float distance, float damageMultiplier)
    {
    }

    @Nullable
	@Override
    protected SoundEvent getAmbientSound()
    {
        return null;
    }

    @Nullable
	@Override
    protected SoundEvent getHurtSound()
    {
        return null;
    }

    @Nullable
	@Override
    protected SoundEvent getDeathSound()
    {
        return null;
    }

    /**
     * Get number of ticks, at least during which the living entity will be silent.
     */
	@Override
    public int getTalkInterval()
    {
        return 120;
    }

    /**
     * Determines if an entity can be despawned, used on idle far away entities
     */
	@Override
    protected boolean canDespawn()
    {
        return false;
    }

}
