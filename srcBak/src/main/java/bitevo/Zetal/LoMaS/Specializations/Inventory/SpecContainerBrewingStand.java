package bitevo.Zetal.LoMaS.Specializations.Inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.AchievementList;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySpecBrewingStand;

public class SpecContainerBrewingStand extends Container
{
    private IInventory tileBrewingStand;
    /** Instance of Slot. */
    private final Slot theSlot;
    private int brewTime;

    public SpecContainerBrewingStand(InventoryPlayer playerInventory, IInventory p_i45802_2_)
    {
        this.tileBrewingStand = p_i45802_2_;
        this.addSlotToContainer(new SpecContainerBrewingStand.Potion(playerInventory.player, p_i45802_2_, 0, 56, 46));
        this.addSlotToContainer(new SpecContainerBrewingStand.Potion(playerInventory.player, p_i45802_2_, 1, 79, 53));
        this.addSlotToContainer(new SpecContainerBrewingStand.Potion(playerInventory.player, p_i45802_2_, 2, 102, 46));
        this.theSlot = this.addSlotToContainer(new SpecContainerBrewingStand.Ingredient(p_i45802_2_, 3, 79, 17));
        int i;

        for (i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(playerInventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        for (i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(playerInventory, i, 8 + i * 18, 142));
        }
    }

    /**
     * Add the given Listener to the list of Listeners. Method name is for legacy.
     */
    public void addListener(IContainerListener listener)
    {
        super.addListener(listener);
        listener.sendAllWindowProperties(this, this.tileBrewingStand);
    }

    /**
     * Looks for changes made in the container, sends them to every listener.
     */
    public void detectAndSendChanges()
    {
        super.detectAndSendChanges();

        for (int i = 0; i < this.listeners.size(); ++i)
        {
        	IContainerListener icrafting = (IContainerListener)this.listeners.get(i);

            if (this.brewTime != this.tileBrewingStand.getField(0))
            {
                icrafting.sendProgressBarUpdate(this, 0, this.tileBrewingStand.getField(0));
            }
        }

        this.brewTime = this.tileBrewingStand.getField(0);
    }

    @SideOnly(Side.CLIENT)
    public void updateProgressBar(int id, int data)
    {
        this.tileBrewingStand.setField(id, data);
    }

    public boolean canInteractWith(EntityPlayer playerIn)
    {
        return this.tileBrewingStand.isUseableByPlayer(playerIn);
    }
    
    @Override
    public ItemStack slotClick(int slotId, int clickedButton, ClickType mode, EntityPlayer playerIn)
    {
    	LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(playerIn);
    	if(lplayer.specClass.equalsIgnoreCase("spellbinder"))
    	{
        	this.tileBrewingStand.setField(2, lplayer.classlevel);
    	}
    	else
    	{
        	this.tileBrewingStand.setField(2, 0);
    	}
    	return super.slotClick(slotId, clickedButton, mode, playerIn);
    }

    /**
     * Take a stack from the specified inventory slot.
     */
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(index);

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if ((index < 0 || index > 2) && index != 3)
            {
                if (!this.theSlot.getHasStack() && this.theSlot.isItemValid(itemstack1))
                {
                    if (!this.mergeItemStack(itemstack1, 3, 4, false))
                    {
                        return null;
                    }
                }
                else if (SpecContainerBrewingStand.Potion.canHoldPotion(itemstack))
                {
                    if (!this.mergeItemStack(itemstack1, 0, 3, false))
                    {
                        return null;
                    }
                }
                else if (index >= 4 && index < 31)
                {
                    if (!this.mergeItemStack(itemstack1, 31, 40, false))
                    {
                        return null;
                    }
                }
                else if (index >= 31 && index < 40)
                {
                    if (!this.mergeItemStack(itemstack1, 4, 31, false))
                    {
                        return null;
                    }
                }
                else if (!this.mergeItemStack(itemstack1, 4, 40, false))
                {
                    return null;
                }
            }
            else
            {
                if (!this.mergeItemStack(itemstack1, 4, 40, true))
                {
                    return null;
                }

                slot.onSlotChange(itemstack1, itemstack);
            }

            if (itemstack1.stackSize == 0)
            {
                slot.putStack((ItemStack)null);
            }
            else
            {
                slot.onSlotChanged();
            }

            if (itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }

            slot.onPickupFromSlot(playerIn, itemstack1);
        }

        return itemstack;
    }

    class Ingredient extends Slot
    {
        public Ingredient(IInventory p_i1803_2_, int p_i1803_3_, int p_i1803_4_, int p_i1803_5_)
        {
            super(p_i1803_2_, p_i1803_3_, p_i1803_4_, p_i1803_5_);
        }

        /**
         * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
         */
        public boolean isItemValid(ItemStack stack)
        {
            return stack != null && net.minecraftforge.common.brewing.BrewingRecipeRegistry.isValidIngredient(stack);
        }

        /**
         * Returns the maximum stack size for a given slot (usually the same as getInventoryStackLimit(), but 1 in the
         * case of armor slots)
         */
        public int getSlotStackLimit()
        {
            return 64;
        }
    }

    static class Potion extends Slot
        {
            /** The player that has this container open. */
            private EntityPlayer player;
            private static final String __OBFID = "CL_00001740";

            public Potion(EntityPlayer p_i1804_1_, IInventory p_i1804_2_, int p_i1804_3_, int p_i1804_4_, int p_i1804_5_)
            {
                super(p_i1804_2_, p_i1804_3_, p_i1804_4_, p_i1804_5_);
                this.player = p_i1804_1_;
            }

            /**
             * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
             */
            public boolean isItemValid(ItemStack stack)
            {
                /**
                 * Returns true if this itemstack can be filled with a potion
                 */
                return canHoldPotion(stack);
            }

            /**
             * Returns the maximum stack size for a given slot (usually the same as getInventoryStackLimit(), but 1 in
             * the case of armor slots)
             */
            public int getSlotStackLimit()
            {
                return 1;
            }

            public void onPickupFromSlot(EntityPlayer playerIn, ItemStack stack)
            {
                if (stack.getItem() instanceof net.minecraft.item.ItemPotion && stack.getMetadata() > 0)
                {
                    this.player.hasAchievement(AchievementList.POTION);
            		//System.out.println("hi!");
                    ////////////SPECIALIZATIONS
                    LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(playerIn);
                    if(((stack.getItemDamage() != 0)
                    && (stack.getItemDamage() != 16)
                    && (stack.getItemDamage() != 32)
                    && (stack.getItemDamage() != 64)
                    && (stack.getItemDamage() != 8192)
                    && (lplayer.specClass.equalsIgnoreCase("spellbinder"))))
                    {
                    	float progressEarned = 0.5f;
                    	if((stack.getItemDamage() >= 8225))
                    	{
                    		progressEarned = progressEarned + 1.0f;
                    	}
                        if (!playerIn.capabilities.isCreativeMode)
        	        	{
                        	//TileEntitySpecBrewingStand.printArray((TileEntitySpecBrewingStand.intToBooleans(this.inventory.getField(1), 4)));
                        	boolean[] freshArray = (TileEntitySpecBrewingStand.intToBooleans(this.inventory.getField(1), 4)).clone();
                        	boolean isFresh = freshArray[this.slotNumber];
                        	if(isFresh)
                        	{
                        		//System.out.println("isFresh!");
                        		lplayer.addSpecProgress(progressEarned);
                        		freshArray[this.slotNumber] = false;
                        		this.inventory.setField(1, TileEntitySpecBrewingStand.booleansToInt(freshArray));
            	                this.player.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((this.player.worldObj.rand.nextFloat() - this.player.worldObj.rand.nextFloat()) * 0.7F + 1.8F));
                        	}
        	        	}
                    }
                }

                super.onPickupFromSlot(playerIn, stack);
            }

            /**
             * Returns true if this itemstack can be filled with a potion
             */
            public static boolean canHoldPotion(ItemStack p_75243_0_)
            {
                return p_75243_0_ != null && (p_75243_0_.getItem() instanceof net.minecraft.item.ItemPotion || p_75243_0_.getItem() == Items.GLASS_BOTTLE);
            }
        }
}
