package bitevo.Zetal.LoMaS.Factions.Message;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.UUID;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Faction;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class FactionMessage implements IMessage
{
	public UUID playerContext;
	public LoMaS_Faction dummyFaction = new LoMaS_Faction("", UUID.randomUUID());

	public FactionMessage()
	{
	}
	
	public FactionMessage(UUID playerContext, UUID id, String name, UUID owner)
	{
		this.playerContext = playerContext;
		this.dummyFaction = new LoMaS_Faction(name, owner);
		this.dummyFaction.setId(id);
	}

	public FactionMessage(UUID playerContext, LoMaS_Faction faction)
	{
		this.playerContext = playerContext;
		if (faction != null)
		{
			this.dummyFaction = faction.clone();
		}
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		playerContext = UUID.fromString(ByteBufUtils.readUTF8String(buf));
		byte[] bytes = new byte[buf.readableBytes()];
		buf.readBytes(bytes);
		Object a = this.bytesToObject(bytes);
		if (a != null)
		{
			this.dummyFaction = (LoMaS_Faction) a;
		}
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, playerContext.toString());
		byte[] bytes = this.objectToBytes(dummyFaction);
		if (bytes != null)
		{
			if (bytes.length > 32600)
			{
				System.err.println("ERROR! Packet too large in Factions.");
			}
			buf.writeBytes(bytes);
		}
	}

	public byte[] objectToBytes(Object yourObject)
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		try
		{
			out = new ObjectOutputStream(bos);
			out.writeObject(yourObject);
			byte[] yourBytes = bos.toByteArray();
			return yourBytes;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if (out != null)
				{
					out.close();
				}
			}
			catch (IOException ex)
			{
			}
			try
			{
				bos.close();
			}
			catch (IOException ex)
			{
			}
		}
		return null;
	}

	public Object bytesToObject(byte[] yourBytes)
	{
		ByteArrayInputStream bis = new ByteArrayInputStream(yourBytes);
		ObjectInput in = null;
		try
		{
			in = new ObjectInputStream(bis);
			Object o = in.readObject();
			return o;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				bis.close();
			}
			catch (IOException ex)
			{
			}
			try
			{
				if (in != null)
				{
					in.close();
				}
			}
			catch (IOException ex)
			{
			}
		}
		return null;
	}
}
