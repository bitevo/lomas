package bitevo.Zetal.LoMaS.TileWrappers.Message;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Faction;
import bitevo.Zetal.LoMaS.TileWrappers.TileWrapper.TileWrapper;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.handler.codec.EncoderException;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTSizeTracker;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class TileWrapperMessage implements IMessage
{
    private BlockPos pos;
    private int index;
	private NBTTagCompound nbt;
	private Class<? extends TileWrapper> wrapperClass;
    
    public TileWrapperMessage()
    {
    	
    }
    
    public TileWrapperMessage(BlockPos blockPosIn, int index, NBTTagCompound compoundIn, Class<? extends TileWrapper> wrapperClass)
    {
        this.pos = blockPosIn;
        this.index = index;
        this.nbt = compoundIn;
        this.wrapperClass = wrapperClass;
    }

	@Override
	public void fromBytes(ByteBuf buf)
	{
        this.pos = BlockPos.fromLong(buf.readLong());
        this.index = buf.readInt();
        try
		{
			this.nbt = this.readNBTTagCompoundFromBuffer(buf);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		byte[] bytes = new byte[buf.readableBytes()];
		buf.readBytes(bytes);
		Object a = this.bytesToObject(bytes);
		if (a != null)
		{
			this.wrapperClass = (Class<? extends TileWrapper>) a;
		}
		else
		{
			System.err.println("ERROR! Wrapper message failed to read!");
		}
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
        buf.writeLong(this.getPos().toLong());
        buf.writeInt(index);
        this.writeNBTTagCompoundToBuffer(buf, this.nbt);
		byte[] bytes = this.objectToBytes(this.wrapperClass);
		if (bytes != null)
		{
			if (bytes.length > 32600)
			{
				System.err.println("ERROR! Packet too large in Wrappers.");
			}
			buf.writeBytes(bytes);
		}
		else
		{
			System.err.println("ERROR! Wrapper message class null!!");
		}
	}
	
    public BlockPos getPos()
	{
		return pos;
	}
	
    public int getIndex()
	{
		return index;
	}

	public NBTTagCompound getNbt()
	{
		return nbt;
	}

    /**
     * Writes a compressed NBTTagCompound to this buffer
     */
    public ByteBuf writeNBTTagCompoundToBuffer(ByteBuf buf, @Nullable NBTTagCompound nbt)
    {
        if (nbt == null)
        {
            buf.writeByte(0);
        }
        else
        {
            try
            {
                CompressedStreamTools.write(nbt, new ByteBufOutputStream(buf));
            }
            catch (IOException ioexception)
            {
                throw new EncoderException(ioexception);
            }
        }

        return buf;
    }

    /**
     * Reads a compressed NBTTagCompound from this buffer
     */
    @Nullable
    public NBTTagCompound readNBTTagCompoundFromBuffer(ByteBuf buf) throws IOException
    {
        int i = this.readerIndex(buf);
        byte b0 = this.readByte(buf);

        if (b0 == 0)
        {
            return null;
        }
        else
        {
            this.readerIndex(buf, i);

            try
            {
                return CompressedStreamTools.read(new ByteBufInputStream(buf), new NBTSizeTracker(2097152L));
            }
            catch (IOException ioexception)
            {
                throw new EncoderException(ioexception);
            }
        }
    }

    public int readerIndex(ByteBuf buf)
    {
        return buf.readerIndex();
    }

    public ByteBuf readerIndex(ByteBuf buf, int p_readerIndex_1_)
    {
        return buf.readerIndex(p_readerIndex_1_);
    }

    public byte readByte(ByteBuf buf)
    {
        return buf.readByte();
    }

	public byte[] objectToBytes(Object yourObject)
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		try
		{
			out = new ObjectOutputStream(bos);
			out.writeObject(yourObject);
			byte[] yourBytes = bos.toByteArray();
			return yourBytes;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if (out != null)
				{
					out.close();
				}
			}
			catch (IOException ex)
			{
			}
			try
			{
				bos.close();
			}
			catch (IOException ex)
			{
			}
		}
		return null;
	}

	public Object bytesToObject(byte[] yourBytes)
	{
		ByteArrayInputStream bis = new ByteArrayInputStream(yourBytes);
		ObjectInput in = null;
		try
		{
			in = new ObjectInputStream(bis);
			Object o = in.readObject();
			return o;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				bis.close();
			}
			catch (IOException ex)
			{
			}
			try
			{
				if (in != null)
				{
					in.close();
				}
			}
			catch (IOException ex)
			{
			}
		}
		return null;
	}

	public Class<? extends TileWrapper> getWrapperClass()
	{
		return this.wrapperClass;
	}
}
