package bitevo.Zetal.LoMaS.Specializations.Renderer;

import org.lwjgl.opengl.GL11;

import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockSap;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySapBlock;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public class BlockSapRenderer extends TileEntitySpecialRenderer
{
	private ModelBlockSap sapBlock;

	public BlockSapRenderer()
	{
		sapBlock = new ModelBlockSap();
	}

	public void renderAModelAt(TileEntity tile, double d, double d1, double d2, float f)
	{
		// System.out.println(tile.getBlockMetadata());
		if (tile instanceof TileEntitySapBlock && tile.getWorld().getBlockState(tile.getPos()).getBlock() instanceof BlockSap)
		{
			EnumFacing facing = (EnumFacing) tile.getWorld().getBlockState(tile.getPos()).getValue(BlockSap.FACING);

			switch (facing)
			{
				case NORTH:
					// System.out.println("2");
					this.bindTexture(new ResourceLocation(SpecializationsMod.MODID, "textures/entities/sapBlock.png"));
					GL11.glPushMatrix(); // start
					GL11.glTranslatef((float) d + 0.51F, (float) d1 + 1.1F, (float) d2 - 0.49F); // size
					GL11.glRotatef(-180, 0.0F, 1.0F, 0.0F); // change the first 0 in like 90 to make it rotate 90 degrees.
					GL11.glScalef(1.0F, -1F, -1F); // to make your block have a normal positioning. comment out to see what happens
					GL11.glEnable(GL11.GL_CULL_FACE);
					sapBlock.renderModel(0.0625F);
					GL11.glPopMatrix(); // end
					break;
				case SOUTH:
					// System.out.println("0");
					this.bindTexture(new ResourceLocation(SpecializationsMod.MODID, "textures/entities/sapBlock.png"));
					GL11.glPushMatrix(); // start
					GL11.glTranslatef((float) d + 0.51F, (float) d1 + 1.1F, (float) d2 + 1.5F); // size
					GL11.glRotatef(0, 0.0F, 1.0F, 0.0F); // change the first 0 in like 90 to make it rotate 90 degrees.
					GL11.glScalef(1.0F, -1F, -1F); // to make your block have a normal positioning. comment out to see what happens
					GL11.glEnable(GL11.GL_CULL_FACE);
					sapBlock.renderModel(0.0625F);
					GL11.glPopMatrix(); // end
					break;
				case WEST:
					// System.out.println("1");
					this.bindTexture(new ResourceLocation(SpecializationsMod.MODID, "textures/entities/sapBlock.png"));
					GL11.glPushMatrix(); // start
					GL11.glTranslatef((float) d + 0.51F, (float) d1 + 1.1F, (float) d2 + 0.51F); // size
					GL11.glRotatef(90, 0.0F, 1.0F, 0.0F); // change the first 0 in like 90 to make it rotate 90 degrees.
					GL11.glScalef(1.0F, -1F, -1F); // to make your block have a normal positioning. comment out to see what happens
					GL11.glEnable(GL11.GL_CULL_FACE);
					sapBlock.renderModel(0.0625F);
					GL11.glPopMatrix(); // end
					break;
				case EAST:
					// System.out.println("3");
					this.bindTexture(new ResourceLocation(SpecializationsMod.MODID, "textures/entities/sapBlock.png"));
					GL11.glPushMatrix(); // start
					GL11.glTranslatef((float) d + 0.49F, (float) d1 + 1.1F, (float) d2 + 0.51F); // size
					GL11.glRotatef(-90, 0.0F, 1.0F, 0.0F); // change the first 0 in like 90 to make it rotate 90 degrees.
					GL11.glScalef(1.0F, -1F, -1F); // to make your block have a normal positioning. comment out to see what happens
					GL11.glEnable(GL11.GL_CULL_FACE);
					sapBlock.renderModel(0.0625F);
					GL11.glPopMatrix(); // end
					break;
				default:
					break;
			}
		}
	}

	@Override
	public void renderTileEntityAt(TileEntity p_180535_1_, double posX, double posY, double posZ, float f, int p_180535_9_)
	{
		renderAModelAt((TileEntitySapBlock) p_180535_1_, posX, posY, posZ, f); // where to render
	}
}
