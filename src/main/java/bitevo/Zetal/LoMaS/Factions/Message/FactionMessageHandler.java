package bitevo.Zetal.LoMaS.Factions.Message;

import java.util.UUID;

import bitevo.Zetal.LoMaS.Factions.FactionsMod;
import bitevo.Zetal.LoMaS.Factions.GUI.GuiFaction;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Faction;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

public class FactionMessageHandler implements IMessageHandler<FactionMessage, IMessage>
{
	@Override
	public IMessage onMessage(FactionMessage message, MessageContext ctx)
	{
		if (ctx.side == Side.CLIENT)
		{
			handleCFactionMessage(message);
		}
		else
		{
			handleSFactionMessage(message);
		}
		return null;
	}

	private void handleCFactionMessage(FactionMessage packet)
	{
		LoMaS_Faction received = packet.dummyFaction;
		String name = received.getName();
		UUID owner = received.getOwner();
		if (LoMaS_Faction.getLoMaSFaction(received.getId()) != null)
		{
			LoMaS_Faction match = LoMaS_Faction.getLoMaSFaction(received.getId());
			if (name != null && !name.equals(""))
			{
				match.setName(name);
			}

			if (owner != null)
			{
				match.setOwner(owner);
			}
		}
		else
		{
			LoMaS_Faction faction = new LoMaS_Faction(name, owner);
			faction.setId(received.getId());
			LoMaS_Faction.addFaction(faction);
		}
		
		GuiScreen screen = Minecraft.getMinecraft().currentScreen;
		if (screen instanceof GuiFaction)
		{
			screen.initGui();
		}
	}

	private void handleSFactionMessage(FactionMessage packet)
	{
		LoMaS_Faction received = packet.dummyFaction;
		String name = received.getName();
		UUID owner = received.getOwner();
		UUID context = packet.playerContext;
		if (LoMaS_Faction.getLoMaSFaction(received.getId()) != null)
		{
			LoMaS_Faction match = LoMaS_Faction.getLoMaSFaction(received.getId());
			if (name != null && !name.equals(""))
			{
				match.setName(name);
			}

			if (owner != null)
			{
				match.setOwner(owner);
			}
			FactionsMod.snw.sendToAll(new FactionMessage(context, match));
		}
		else
		{
			LoMaS_Faction faction = new LoMaS_Faction(name, owner);
			faction.setId(received.getId());
			LoMaS_Faction.addFaction(faction);
			FactionsMod.snw.sendToAll(new FactionMessage(context, faction));
		}
	}
}
