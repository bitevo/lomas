package bitevo.Zetal.LoMaS.Citizens.Renderer;

import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelPigman extends ModelBiped
{
	ModelRenderer nose;

	public ModelPigman()
	{
		super();
		textureWidth = 64;
		textureHeight = 32;

		nose = new ModelRenderer(this, 31, 2);
		nose.addBox(-2F, -4F, -5F, 4, 3, 1);
		nose.setRotationPoint(0F, 0F, 0F);
		nose.setTextureSize(64, 32);
		nose.mirror = true;
		setRotation(nose, 0F, 0F, 0F);
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		super.render(entity, f, f1, f2, f3, f4, f5);
		setRotationAngles(f, f1, f2, f3, f4, f5, entity);
		nose.render(f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
	{
		super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
		this.copyModelAngles(bipedHead, nose);
	}
}
