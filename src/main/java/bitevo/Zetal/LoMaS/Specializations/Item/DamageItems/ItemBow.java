package bitevo.Zetal.LoMaS.Specializations.Item.DamageItems;

import java.util.List;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import bitevo.Zetal.LoMaS.Specializations.SCMClientProxy;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityCustomArrow;
import bitevo.Zetal.LoMaS.Specializations.Message.ArrowMessage;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.init.Enchantments;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.EnumAction;
import net.minecraft.item.IItemPropertyGetter;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.ArrowLooseEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemBow extends net.minecraft.item.ItemBow
{
	public ItemStack arrowStack = null;

	public ItemBow()
	{
		super();
		this.addPropertyOverride(new ResourceLocation("pull"), new IItemPropertyGetter()
		{
			@Override
			@SideOnly(Side.CLIENT)
			public float apply(ItemStack stack, @Nullable World worldIn, @Nullable EntityLivingBase entityIn)
			{
				if (entityIn == null)
				{
					return 0.0F;
				}
				else
				{
					ItemStack itemstack = entityIn.getActiveItemStack();
					return itemstack != null && itemstack.getItem() == SpecializationsMod.BOW ? (float) (stack.getMaxItemUseDuration() - entityIn.getItemInUseCount()) / 20.0F : 0.0F;
				}
			}
		});
		this.addPropertyOverride(new ResourceLocation("type"), new IItemPropertyGetter()
		{
			@Override
			@SideOnly(Side.CLIENT)
			public float apply(ItemStack stack, @Nullable World worldIn, @Nullable EntityLivingBase entityIn)
			{
				if (entityIn == null)
				{
					return 0.0F;
				}
				else
				{
					ItemStack itemstack = entityIn.getActiveItemStack();
					if (itemstack != null && itemstack.getItem() == SpecializationsMod.BOW && entityIn instanceof EntityPlayer)
					{
						EntityPlayer player = (EntityPlayer) entityIn;
						LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
						return lplayer.arrowStack != null ? lplayer.arrowStack.getItemDamage() : 0.0F;
					}
					else if (LoMaS_Utils.FAC && itemstack != null && itemstack.getItem() == SpecializationsMod.BOW && entityIn instanceof bitevo.Zetal.LoMaS.Factions.Entity.EntityPigmanWarrior)
					{
						bitevo.Zetal.LoMaS.Factions.Entity.EntityPigmanWarrior warrior = (bitevo.Zetal.LoMaS.Factions.Entity.EntityPigmanWarrior) entityIn;
						return warrior.findAmmo() != null ? warrior.findAmmo().getItemDamage() : 0.0F;
					}
					return 0.0F;
				}
			}
		});
		this.addPropertyOverride(new ResourceLocation("pulling"), new IItemPropertyGetter()
		{
			@Override
			@SideOnly(Side.CLIENT)
			public float apply(ItemStack stack, @Nullable World worldIn, @Nullable EntityLivingBase entityIn)
			{
				return entityIn != null && entityIn.isHandActive() && entityIn.getActiveItemStack() == stack ? 1.0F : 0.0F;
			}
		});
		this.addPropertyOverride(new ResourceLocation("warrior5"), new IItemPropertyGetter()
		{
			@Override
			@SideOnly(Side.CLIENT)
			public float apply(ItemStack stack, @Nullable World worldIn, @Nullable EntityLivingBase entityIn)
			{
				return entityIn != null && entityIn instanceof EntityPlayer ? LoMaS_Player.getLoMaSPlayer((EntityPlayer) entityIn).getSkills().getMatchingNodesFromName("Bow arm spd") : 0.0F;
			}
		});
	}

	private ItemStack findAmmo(EntityPlayer player)
	{
		if (this.isArrow(player.getHeldItem(EnumHand.OFF_HAND)))
		{
			return player.getHeldItem(EnumHand.OFF_HAND);
		}
		else if (this.isArrow(player.getHeldItem(EnumHand.MAIN_HAND)))
		{
			return player.getHeldItem(EnumHand.MAIN_HAND);
		}
		else
		{
			for (int i = 0; i < player.inventory.getSizeInventory(); ++i)
			{
				ItemStack itemstack = player.inventory.getStackInSlot(i);

				if (this.isArrow(itemstack))
				{
					return itemstack;
				}
			}

			return null;
		}
	}

	@Override
	public int getMaxDamage(ItemStack stack)
	{
		return SpecializationsMod.getMaxDamage(stack);
	}

	/**
	 * called when the player releases the use item button. Args: itemstack, world, entityplayer, itemInUseCount
	 */
	@Override
	public void onPlayerStoppedUsing(ItemStack stack, World worldIn, EntityLivingBase entityLiving, int timeLeft)
	{
		if (entityLiving instanceof EntityPlayer)
		{
			EntityPlayer entityplayer = (EntityPlayer) entityLiving;
			int j = this.getMaxItemUseDuration(stack) - timeLeft;

			boolean flag = entityplayer.capabilities.isCreativeMode || EnchantmentHelper.getEnchantmentLevel(Enchantments.INFINITY, stack) > 0;
			ItemStack itemstack = this.findAmmo(entityplayer);

			LoMaS_Player lPlayer = LoMaS_Player.getLoMaSPlayer(entityplayer);
			j = (int) (j * (1 + lPlayer.getSkills().getPercentMagnitudeFromName("Bow arm spd")));

			if (EnchantmentHelper.getEnchantmentLevel(SpecializationsMod.skill, stack) > 0)
			{
				j = (int) (j * 1.25D);
			}

			ArrowLooseEvent event = new ArrowLooseEvent(entityplayer, stack, worldIn, j, itemstack != null || flag);
			MinecraftForge.EVENT_BUS.post(event);
			if (event.isCanceled())
			{
				return;
			}
			j = event.getCharge();

			if (lPlayer.arrowStack != null)
			{
				float f = (float) j / 20.0F;
				f = (f * f + f * 2.0F) / 3.0F;
				float m = 1.0f + lPlayer.getSkills().getPercentMagnitudeFromName("Arrow spd");
				f *= m;

				if ((double) f < 0.1D)
				{
					return;
				}

				if (f > m)
				{
					f = m;
				}

				EntityCustomArrow entityarrow = SpecializationsMod.ARROW.createArrow(worldIn, lPlayer.arrowStack, entityLiving, lPlayer.arrowStack.getItemDamage());
				entityarrow.setAim(entityplayer, entityplayer.rotationPitch, entityplayer.rotationYaw, 0.0F, f * 3.0F, 1.0F);

				if (f == 1.0F)
				{
					entityarrow.setIsCritical(true);
				}

				int k = EnchantmentHelper.getEnchantmentLevel(Enchantments.POWER, stack);

				if (k > 0)
				{
					entityarrow.setDamage(entityarrow.getDamage() + (double) k * 0.5D + 0.5D);
				}

				int l = EnchantmentHelper.getEnchantmentLevel(Enchantments.PUNCH, stack);

				if (l > 0)
				{
					entityarrow.setKnockbackStrength(l);
				}

				if (EnchantmentHelper.getEnchantmentLevel(Enchantments.FLAME, stack) > 0)
				{
					entityarrow.setFire(100);
				}

				stack.damageItem(1, entityLiving);
				worldIn.playSound((EntityPlayer) null, entityplayer.posX, entityplayer.posY, entityplayer.posZ, SoundEvents.ENTITY_ARROW_SHOOT, SoundCategory.NEUTRAL, 1.0F, 1.0F / (itemRand.nextFloat() * 0.4F + 1.2F) + f * 0.5F);

				if (flag)
				{
					entityarrow.pickupStatus = EntityArrow.PickupStatus.CREATIVE_ONLY;
				}
				else
				{
					--itemstack.stackSize;

					if (itemstack.stackSize == 0)
					{
						entityplayer.inventory.deleteStack(itemstack);
					}
				}

				if (!worldIn.isRemote)
				{
					worldIn.spawnEntityInWorld(entityarrow);
				}
			}
		}
	}

	/**
	 * How long it takes to use or consume an item
	 */
	@Override
	public int getMaxItemUseDuration(ItemStack par1ItemStack)
	{
		return 72000;
	}

	/**
	 * returns the action that specifies what animation to play when the items is being used
	 */
	@Override
	public EnumAction getItemUseAction(ItemStack par1ItemStack)
	{
		return EnumAction.BOW;
	}

	/**
	 * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
	 */
	@Override
	public ActionResult<ItemStack> onItemRightClick(ItemStack itemStackIn, World worldIn, EntityPlayer playerIn, EnumHand hand)
	{
		boolean flag = this.findAmmo(playerIn) != null;

		ActionResult<ItemStack> ret = net.minecraftforge.event.ForgeEventFactory.onArrowNock(itemStackIn, worldIn, playerIn, hand, flag);
		if (ret != null) return ret;

		if (!flag)
		{
			return !flag ? new ActionResult(EnumActionResult.FAIL, itemStackIn) : new ActionResult(EnumActionResult.PASS, itemStackIn);
		}
		else
		{
			playerIn.setActiveHand(hand);

			// Specializations
			arrowStack = this.findAmmo(playerIn);
			if (!worldIn.isRemote)
			{
				SpecializationsMod.snw.sendToAll(new ArrowMessage(arrowStack.getItemDamage(), playerIn));
			}

			return new ActionResult(EnumActionResult.SUCCESS, itemStackIn);
		}
	}

	/**
	 * Return the enchantability factor of the item, most of the time is based on material.
	 */
	@Override
	public int getItemEnchantability()
	{
		return 1;
	}

	/*
	 * @SideOnly(Side.CLIENT)
	 * @Override public ModelResourceLocation getModel(ItemStack stack, EntityPlayer player, int useRemaining) { int j = this.getMaxItemUseDuration(stack) - useRemaining; LoMaS_Player lplayer =
	 * LoMaS_Player.getLoMaSPlayer(player); if (lplayer != null) { this.arrowStack = lplayer.arrowStack; } // System.out.println("useRemaining: " + useRemaining + " j: " + j); if (useRemaining == 0) {
	 * return new ModelResourceLocation("bow", "inventory"); } if (stack.getItem() instanceof ItemBow) { if (lplayer != null && lplayer.specClass.equalsIgnoreCase("warrior") && lplayer.classlevel >=
	 * 5) { if (j >= 13.5) { return this.getItemIconForUseDuration(2); } if (j > 9.75) { return this.getItemIconForUseDuration(1); } if (j > 0) { return this.getItemIconForUseDuration(0); } } else {
	 * if (j >= 18) { return this.getItemIconForUseDuration(2); } if (j > 13) { return this.getItemIconForUseDuration(1); } if (j > 0) { return this.getItemIconForUseDuration(0); } } } return new
	 * ModelResourceLocation("bow", "inventory"); }
	 */

	@SideOnly(Side.CLIENT)
	public ModelResourceLocation getItemIconForUseDuration(int par1)
	{
		if (this.arrowStack != null)
		{
			// System.out.println("Hi " + this.iconArray[par1 + (this.arrowStack.getItemDamage() * 3)]);
			return new ModelResourceLocation(SCMClientProxy.bowTextures[par1 + (this.arrowStack.getItemDamage() * 3)], "inventory");
		}
		else
		{
			// System.out.println("Error in rendering.. :(");
			return new ModelResourceLocation("bow", "inventory");
		}
	}
}
