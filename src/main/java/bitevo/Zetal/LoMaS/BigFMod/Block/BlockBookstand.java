package bitevo.Zetal.LoMaS.BigFMod.Block;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.BigFMod.TileEntity.TileEntityBookstand;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreenBook;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemWrittenBook;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.network.play.server.SPacketSetSlot;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentUtils;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockBookstand extends BlockContainer
{
	public static final PropertyDirection FACING = PropertyDirection.create("facing", EnumFacing.Plane.HORIZONTAL);

	protected static final AxisAlignedBB NORTH_AABB = new AxisAlignedBB(0D, 0D, 0.17D, 1D, 0.6D, 0.82D);
	protected static final AxisAlignedBB SOUTH_AABB = new AxisAlignedBB(0D, 0D, 0.17D, 1D, 0.6D, 0.82D);
	protected static final AxisAlignedBB WEST_AABB = new AxisAlignedBB(0.23D, 0D, 0D, 0.85D, 0.6D, 1D);
	protected static final AxisAlignedBB EAST_AABB = new AxisAlignedBB(0.17D, 0D, 0D, 0.82D, 0.6D, 1D);
	protected static final AxisAlignedBB DEFAULT_AABB = new AxisAlignedBB(0.23D, 0D, 0D, 0.85D, 0.6D, 1D);

	public BlockBookstand()
	{
		super(Material.WOOD);
		this.setDefaultState(this.blockState.getBaseState().withProperty(FACING, EnumFacing.NORTH));
		this.setTickRandomly(true);
		this.setCreativeTab(LoMaS_Utils.tabLoMaS);
		// this.setRequiresSelfNotify();
	}

	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}

	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}

	@Override
	public IBlockState onBlockPlaced(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
	{
		return this.getDefaultState().withProperty(FACING, placer.getHorizontalFacing());
	}

	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
	{
		EnumFacing enumfacing = EnumFacing.getHorizontal(MathHelper.floor_double((double) (placer.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3).getOpposite();
		state = state.withProperty(FACING, enumfacing);
		worldIn.setBlockState(pos, state, 3);
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, World worldIn, BlockPos pos)
	{
		int x = pos.getX();
		int y = pos.getY();
		int z = pos.getZ();
		EnumFacing face = (EnumFacing) blockState.getValue(FACING);
		if (face == EnumFacing.NORTH)
		{
			return this.NORTH_AABB;
		}
		else if (face == EnumFacing.SOUTH)
		{
			return this.SOUTH_AABB;
		}
		else if (face == EnumFacing.WEST)
		{
			return this.WEST_AABB;
		}
		else if (face == EnumFacing.EAST)
		{
			return this.EAST_AABB;
		}
		return this.DEFAULT_AABB;
	}

	/**
	 * Called upon block activation (right click on the block.)
	 */
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, @Nullable ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		TileEntityBookstand tileentitybookstand = (TileEntityBookstand) worldIn.getTileEntity(pos);
		if (tileentitybookstand != null)
		{
			// System.out.println(player.inventory.getCurrentItem() + " " + tileentitybookstand.getStoredBook());
			if (heldItem != null && heldItem.getItem().equals(Items.WRITTEN_BOOK))
			{
				if (tileentitybookstand.getStoredBook() != null)
				{
					tileentitybookstand.removeBook();
				}
				System.out.println(heldItem);
				tileentitybookstand.addBook(heldItem);
				heldItem.stackSize--;
				return true;
			}
			else if (tileentitybookstand.getStoredBook() != null)
			{
				this.showBook(playerIn, tileentitybookstand.getStoredBook());
				return true;
			}
		}
		return false;
	}

	@SideOnly(Side.CLIENT)
	public void showBook(EntityPlayer player, ItemStack book)
	{
		Minecraft.getMinecraft().displayGuiScreen(new GuiScreenBook(player, book, false));
	}

	/**
	 * Called when the block is clicked by a player. Args: x, y, z, entityPlayer
	 */
	@Override
	public void onBlockClicked(World par1World, BlockPos pos, EntityPlayer par5EntityPlayer)
	{
		TileEntityBookstand tileentitybookstand = (TileEntityBookstand) par1World.getTileEntity(pos);

		if (tileentitybookstand != null)
		{
			if (tileentitybookstand.getStoredBook() != null)
			{
				tileentitybookstand.removeBook();
			}
		}
		par1World.markBlockRangeForRenderUpdate(pos, pos);
		par1World.notifyBlockUpdate(pos, par1World.getBlockState(pos), par1World.getBlockState(pos), 3);
	}

	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		EnumFacing enumfacing = EnumFacing.getFront(meta);

		if (enumfacing.getAxis() == EnumFacing.Axis.Y)
		{
			enumfacing = EnumFacing.NORTH;
		}

		return this.getDefaultState().withProperty(FACING, enumfacing);
	}

	/**
	 * Convert the BlockState into the correct metadata value
	 */
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return ((EnumFacing) state.getValue(FACING)).getIndex();
	}

	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, new IProperty[] { FACING });
	}

	/**
	 * Returns a new instance of a block's tile entity class. Called on placing the block.
	 */
	@Override
	public TileEntity createNewTileEntity(World par1World, int i)
	{
		TileEntity te = new TileEntityBookstand();
		te.setWorldObj(par1World);
		return te;
	}

	private void resolveContents(ItemStack stack, EntityPlayer player)
	{
		if (stack != null && stack.getTagCompound() != null)
		{
			NBTTagCompound nbttagcompound = stack.getTagCompound();

			if (!nbttagcompound.getBoolean("resolved"))
			{
				nbttagcompound.setBoolean("resolved", true);

				if (ItemWrittenBook.validBookTagContents(nbttagcompound))
				{
					NBTTagList nbttaglist = nbttagcompound.getTagList("pages", 8);

					for (int i = 0; i < nbttaglist.tagCount(); ++i)
					{
						String s = nbttaglist.getStringTagAt(i);
						ITextComponent object;

						try
						{
							object = ITextComponent.Serializer.fromJsonLenient(s);
							object = TextComponentUtils.processComponent(player, object, player);
						}
						catch (Exception var9)
						{
							object = new TextComponentString(s);
						}

						nbttaglist.set(i, new NBTTagString(ITextComponent.Serializer.componentToJson(object)));
					}

					nbttagcompound.setTag("pages", nbttaglist);

					if (player instanceof EntityPlayerMP && player.getHeldItemMainhand() == stack)
					{
						Slot slot = player.openContainer.getSlotFromInventory(player.inventory, player.inventory.currentItem);
						((EntityPlayerMP) player).connection.sendPacket(new SPacketSetSlot(0, slot.slotNumber, stack));
					}
				}
			}
		}
	}
}
