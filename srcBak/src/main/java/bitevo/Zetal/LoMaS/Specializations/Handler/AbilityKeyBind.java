package bitevo.Zetal.LoMaS.Specializations.Handler;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import org.lwjgl.input.Keyboard;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Message.AbilityMessage;

public class AbilityKeyBind
{
	/**
	 * Storing an instance of Minecraft in a local variable saves having to get
	 * it every time
	 */
	private final Minecraft mc;
	
	/** Key index for easy handling */
	public static final int CUSTOM_INV = 0;
	
	/** Key descriptions; use a language file to localize the description later */
	private static final String[] desc = { "Activate Ability" };
	
	/** Default key values */
	private static final int[] keyValues = { Keyboard.KEY_V };
	
	/**
	 * Make this public or provide a getter if you'll need access to the key
	 * bindings from elsewhere
	 */
	public static final KeyBinding[] keys = new KeyBinding[desc.length];
	
	@SideOnly(Side.CLIENT)
	public AbilityKeyBind()
	{
		mc = Minecraft.getMinecraft();
		for (int i = 0; i < desc.length; ++i)
		{
			keys[i] = new KeyBinding(desc[i], keyValues[i], I18n.translateToLocal("LoMaS"));
			ClientRegistry.registerKeyBinding(keys[i]);
		}
	}
	
	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void onKeyInput(KeyInputEvent event)
	{
		if (mc.theWorld != null && mc.theWorld.isRemote)
		{
			if (keys[0].isPressed())
			{
				EntityPlayerSP player = Minecraft.getMinecraft().thePlayer;
				LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
				if (lplayer.isActive == false && lplayer.counter == 0 && Minecraft.getMinecraft().inGameHasFocus && Minecraft.getMinecraft().currentScreen == null
				&& lplayer.classlevel >= 10 && (lplayer.specClass.equalsIgnoreCase("warrior") || lplayer.specClass.equalsIgnoreCase("stealth") || lplayer.specClass.equalsIgnoreCase("miner")))
				{
					SpecializationsMod.snw.sendToServer(new AbilityMessage(player));
					lplayer.isActive = true;
				}
			}
		}
	}
}