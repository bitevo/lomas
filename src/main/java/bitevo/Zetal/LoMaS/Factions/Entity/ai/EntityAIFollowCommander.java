package bitevo.Zetal.LoMaS.Factions.Entity.ai;

import bitevo.Zetal.LoMaS.Factions.Entity.EntityPigmanWarrior;
import bitevo.Zetal.LoMaS.Shared.Entity.EntityFactionMember;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.passive.EntityTameable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.pathfinding.PathNavigate;
import net.minecraft.pathfinding.PathNavigateGround;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

public class EntityAIFollowCommander extends EntityAIBase
{
    private final EntityPigmanWarrior theWarrior;
    private EntityPlayer theCommander;
    World theWorld;
    private final double followSpeed;
    private final PathNavigate warriorPathfinder;
    private int timeToRecalcPath;
    float maxDist;
    float minDist;
    private float oldWaterCost;

    public EntityAIFollowCommander(EntityPigmanWarrior thePetIn, double followSpeedIn, float minDistIn, float maxDistIn)
    {
        this.theWarrior = thePetIn;
        this.theWorld = thePetIn.worldObj;
        this.followSpeed = followSpeedIn;
        this.warriorPathfinder = thePetIn.getNavigator();
        this.minDist = minDistIn;
        this.maxDist = maxDistIn;
        this.setMutexBits(3);

        if (!(thePetIn.getNavigator() instanceof PathNavigateGround))
        {
            throw new IllegalArgumentException("Unsupported mob type for FollowOwnerGoal");
        }
    }

    /**
     * Returns whether the EntityAIBase should begin execution.
     */
    public boolean shouldExecute()
    {
        EntityLivingBase entitylivingbase = this.theWarrior.getCommander();

        if (entitylivingbase == null)
        {
            return false;
        }
        else if (entitylivingbase instanceof EntityPlayer && ((EntityPlayer)entitylivingbase).isSpectator())
        {
            return false;
        }
        else if (this.theWarrior.isIdle())
        {
            return false;
        }
        else if (this.theWarrior.getDistanceSqToEntity(entitylivingbase) < (double)(this.minDist * this.minDist))
        {
            return false;
        }
        else if(entitylivingbase instanceof EntityPlayer)
        {
            this.theCommander = (EntityPlayer) entitylivingbase;
            return true;
        }
        return false;
    }

    /**
     * Returns whether an in-progress EntityAIBase should continue executing
     */
    public boolean continueExecuting()
    {
        return !this.warriorPathfinder.noPath() && this.theWarrior.getDistanceSqToEntity(this.theCommander) > (double)(this.maxDist * this.maxDist) && !this.theWarrior.isIdle();
    }

    /**
     * Execute a one shot task or start executing a continuous task
     */
    public void startExecuting()
    {
        this.timeToRecalcPath = 0;
        this.oldWaterCost = this.theWarrior.getPathPriority(PathNodeType.WATER);
        this.theWarrior.setPathPriority(PathNodeType.WATER, 0.0F);
    }

    /**
     * Resets the task
     */
    public void resetTask()
    {
        this.theCommander = null;
        this.warriorPathfinder.clearPathEntity();
        this.theWarrior.setPathPriority(PathNodeType.WATER, this.oldWaterCost);
    }

    private boolean isEmptyBlock(BlockPos pos)
    {
        IBlockState iblockstate = this.theWorld.getBlockState(pos);
        return iblockstate.getMaterial() == Material.AIR ? true : !iblockstate.isFullCube();
    }

    /**
     * Updates the task
     */
    public void updateTask()
    {
        this.theWarrior.getLookHelper().setLookPositionWithEntity(this.theCommander, 10.0F, (float)this.theWarrior.getVerticalFaceSpeed());

        if (!this.theWarrior.isIdle())
        {
            if (--this.timeToRecalcPath <= 0)
            {
                this.timeToRecalcPath = 10;

                if (!this.warriorPathfinder.tryMoveToEntityLiving(this.theCommander, this.followSpeed))
                {
                    if (!this.theWarrior.getLeashed())
                    {
                        if (this.theWarrior.getDistanceSqToEntity(this.theCommander) >= 144.0D)
                        {
                            int i = MathHelper.floor_double(this.theCommander.posX) - 2;
                            int j = MathHelper.floor_double(this.theCommander.posZ) - 2;
                            int k = MathHelper.floor_double(this.theCommander.getEntityBoundingBox().minY);

                            for (int l = 0; l <= 4; ++l)
                            {
                                for (int i1 = 0; i1 <= 4; ++i1)
                                {
                                    if ((l < 1 || i1 < 1 || l > 3 || i1 > 3) && this.theWorld.getBlockState(new BlockPos(i + l, k - 1, j + i1)).isFullyOpaque() && this.isEmptyBlock(new BlockPos(i + l, k, j + i1)) && this.isEmptyBlock(new BlockPos(i + l, k + 1, j + i1)))
                                    {
                                        this.theWarrior.setLocationAndAngles((double)((float)(i + l) + 0.5F), (double)k, (double)((float)(j + i1) + 0.5F), this.theWarrior.rotationYaw, this.theWarrior.rotationPitch);
                                        this.warriorPathfinder.clearPathEntity();
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}