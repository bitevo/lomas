package bitevo.Zetal.LoMaS.Specializations.Item.DamageItems;

import java.util.List;

import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemCustomSpade extends ItemSpade
{
	public ItemCustomSpade(Item.ToolMaterial material)
	{
		super(material);
	}

	@Override
	public int getMaxDamage(ItemStack stack)
	{
		return SpecializationsMod.getMaxDamage(stack);
	}
}
