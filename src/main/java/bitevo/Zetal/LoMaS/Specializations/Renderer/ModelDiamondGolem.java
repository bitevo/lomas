package bitevo.Zetal.LoMaS.Specializations.Renderer;

import bitevo.Zetal.LoMaS.Specializations.Entity.EntityFactionDiamondGolem;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;

public class ModelDiamondGolem extends ModelBiped
{
    public ModelRenderer body1;
    public ModelRenderer body2;
    public ModelRenderer rightarm;
    public ModelRenderer leftarm;
    public ModelRenderer head;
    public ModelRenderer leftshoulder;
    public ModelRenderer rightshoulder;

    public ModelDiamondGolem() {
        this.textureWidth = 128;
        this.textureHeight = 128;
        this.leftarm = new ModelRenderer(this, 0, 30);
        this.leftarm.setRotationPoint(10.0F, 0.0F, 0.0F);
        this.leftarm.addBox(-1.5F, 0.0F, -1.5F, 3, 16, 3, 0.0F);
        this.body2 = new ModelRenderer(this, 0, 50);
        this.body2.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.body2.addBox(-2.5F, -3.0F, -8.0F, 5, 11, 11, 0.0F);
        this.rightarm = new ModelRenderer(this, 0, 30);
        this.rightarm.setRotationPoint(-10.0F, 0.0F, 0.0F);
        this.rightarm.addBox(-1.5F, 0.0F, -1.5F, 3, 16, 3, 0.0F);
        this.leftshoulder = new ModelRenderer(this, 0, 23);
        this.leftshoulder.setRotationPoint(10.0F, -4.0F, 0.0F);
        this.leftshoulder.addBox(-1.5F, -1.5F, -1.5F, 3, 3, 3, 0.0F);
        this.rightshoulder = new ModelRenderer(this, 0, 23);
        this.rightshoulder.setRotationPoint(-10.0F, -4.0F, 0.0F);
        this.rightshoulder.addBox(-1.5F, -1.5F, -1.5F, 3, 3, 3, 0.0F);
        this.body1 = new ModelRenderer(this, 0, 50);
        this.body1.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.body1.addBox(-2.5F, -3.0F, -8.0F, 5, 11, 11, 0.0F);
        this.head = new ModelRenderer(this, 0, 0);
        this.head.setRotationPoint(0.0F, -15.0F, 0.0F);
        this.head.addBox(-3.5F, -3.5F, -3.5F, 7, 7, 7, 0.0F);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) { 
        this.leftarm.render(f5);
        GlStateManager.pushMatrix();
        GlStateManager.translate(this.body2.offsetX, this.body2.offsetY, this.body2.offsetZ);
        GlStateManager.translate(this.body2.rotationPointX * f5, this.body2.rotationPointY * f5, this.body2.rotationPointZ * f5);
        GlStateManager.scale(0.7D, 2.0D, 1.0D);
        GlStateManager.translate(-this.body2.offsetX, -this.body2.offsetY, -this.body2.offsetZ);
        GlStateManager.translate(-this.body2.rotationPointX * f5, -this.body2.rotationPointY * f5, -this.body2.rotationPointZ * f5);
        this.body2.render(f5);
        GlStateManager.popMatrix();
        this.rightarm.render(f5);
        this.leftshoulder.render(f5);
        this.rightshoulder.render(f5);
        GlStateManager.pushMatrix();
        GlStateManager.translate(this.body1.offsetX, this.body1.offsetY, this.body1.offsetZ);
        GlStateManager.translate(this.body1.rotationPointX * f5, this.body1.rotationPointY * f5, this.body1.rotationPointZ * f5);
        GlStateManager.scale(1.0D, 2.0D, 0.7D);
        GlStateManager.translate(-this.body1.offsetX, -this.body1.offsetY, -this.body1.offsetZ);
        GlStateManager.translate(-this.body1.rotationPointX * f5, -this.body1.rotationPointY * f5, -this.body1.rotationPointZ * f5);
        this.body1.render(f5);
        GlStateManager.popMatrix();
        this.head.render(f5);
    }

    /**
     * Sets the model's various rotation angles. For bipeds, par1 and par2 are used for animating the movement of arms
     * and legs, where par1 represents the time(so that arms and legs swing back and forth) and par2 represents how
     * "far" arms and legs can swing at most.
     */
    public void setRotationAngles(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn)
    {
        this.leftarm.rotateAngleZ = -0.15707963267948966F;
        this.rightarm.rotateAngleZ = 0.15707963267948966F;
        this.body1.rotateAngleX = 0.7853981633974483F;
        this.body2.rotateAngleX = 0.7853981633974483F;
        this.body2.rotateAngleY = 1.5707963267948966F;
        this.head.rotateAngleX = -0.7853981633974483F;
        this.head.rotateAngleY = 1.2217304763960306F;
        this.head.rotateAngleZ = 1.5707963267948966F;
        this.rightshoulder.rotateAngleX = -0.7853981633974483F;
        this.rightshoulder.rotateAngleY = -0.5235987755982988F;
        this.rightshoulder.rotateAngleZ = -0.6108652381980153F;
        this.leftshoulder.rotateAngleX = 0.7853981633974483F;
        this.leftshoulder.rotateAngleY = 0.5235987755982988F;
        this.leftshoulder.rotateAngleZ = 0.6108652381980153F;
        this.head.rotateAngleX += headPitch * 0.017453292F;
        this.head.rotateAngleY += netHeadYaw * 0.017453292F;

        this.leftshoulder.rotateAngleX += (ageInTicks % 360)* 0.017453292F;
        this.leftshoulder.rotateAngleZ += (ageInTicks % 360)* 0.017453292F;
        this.rightshoulder.rotateAngleX += (-ageInTicks % 360)* 0.017453292F;
        this.rightshoulder.rotateAngleZ += (-ageInTicks % 360)* 0.017453292F;
    }

    /**
     * Used for easily adding entity-dependent animations. The second and third float params here are the same second
     * and third as in the setRotationAngles method.
     */
    public void setLivingAnimations(EntityLivingBase entitylivingbaseIn, float p_78086_2_, float p_78086_3_, float partialTickTime)
    {
    	EntityFactionDiamondGolem entitydiamondgolem = (EntityFactionDiamondGolem)entitylivingbaseIn;
    	EntityLivingBase target = entitydiamondgolem.getAttackTarget();
        this.rightarm.rotateAngleX = (-0.2F + 1.5F * this.triangleWave(p_78086_2_, 13.0F)) * p_78086_3_;
        this.leftarm.rotateAngleX = (-0.2F - 1.5F * this.triangleWave(p_78086_2_, 13.0F)) * p_78086_3_;
    }

    private float triangleWave(float p_78172_1_, float p_78172_2_)
    {
        return (Math.abs(p_78172_1_ % p_78172_2_ - p_78172_2_ * 0.5F) - p_78172_2_ * 0.25F) / (p_78172_2_ * 0.25F);
    }
}
