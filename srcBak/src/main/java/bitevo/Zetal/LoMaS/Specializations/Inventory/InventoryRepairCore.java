package bitevo.Zetal.LoMaS.Specializations.Inventory;

import net.minecraft.inventory.InventoryBasic;
import net.minecraft.item.ItemStack;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;

//private
public class InventoryRepairCore extends InventoryBasic
{
    /** Container of this anvil's block. */
    final SpecContainerRepair theContainer;

    //private
    public InventoryRepairCore(SpecContainerRepair specContainerRepair, String par2Str, boolean par3, int par4)
    {
        super(par2Str, par3, par4);
        this.theContainer = specContainerRepair;
    }

    /**
     * Called when an the contents of an Inventory change, usually
     */
    @Override
    public void markDirty()
    {
        super.markDirty();
        this.theContainer.onCraftMatrixChanged(this);
    }

    /**
     * Returns true if automation is allowed to insert the given stack (ignoring stack size) into the given slot.
     */
    @Override
    public boolean isItemValidForSlot(int slotID, ItemStack par2ItemStack)
    {
        if(slotID == 0 && par2ItemStack.getItem().equals(SpecializationsMod.EMERALDHEART))
        {
        	return true;
        }
        else if(slotID == 1 && par2ItemStack.getItem().equals(SpecializationsMod.IRONHEART))
        {
        	return true;
        }
        return false;
    }
}
