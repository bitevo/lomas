package bitevo.Zetal.LoMaS.PolyZones.Message;

import java.util.ArrayList;
import java.util.HashMap;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import bitevo.Zetal.LoMaS.PolyZones.CustomPlayer;
import bitevo.Zetal.LoMaS.PolyZones.PolyProcessor;
import bitevo.Zetal.LoMaS.PolyZones.PolyZonesMod;

public class PZMessageHandler implements IMessageHandler<PZMessage, IMessage>
{
	public static HashMap<CustomPlayer, PolyProcessor> receivingProcessors = new HashMap();
	
	@Override
	public IMessage onMessage(PZMessage message, MessageContext ctx) 
	{
		if(ctx.side == Side.CLIENT)
		{
			handleCPZMessage(message);
		}
		else
		{
			handleSPZMessage(message);
		}
		return null;
	}

	@SideOnly(Side.CLIENT)
	private void handleCPZMessage(PZMessage packet) 
	{
		int piece = packet.piece;
		ArrayList array = packet.array;
		PolyProcessor processor = PolyZonesMod.processor;
		EntityPlayer player = Minecraft.getMinecraft().thePlayer;
		//System.out.println("Hi! " + piece);
		switch (piece)
		{
			case 0:
				processor.players = array;
				//PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, 1, player.getDisplayNameString()));
				break;
			case 1:
				processor.activePlayers = array;
				//PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, 2, player.getDisplayNameString()));
				break;
			case 2:
				processor.defaultAccess = array;
				//PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, 3, player.getDisplayNameString()));
		    	//PolyZonesMod.playersMove.put(Minecraft.getMinecraft().thePlayer.getDisplayNameString(), new Boolean(true));
				break;
			case 3:
				processor.zones = array;
				//PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, 0, player.getDisplayNameString()));
				PolyZonesMod.snw.sendToServer(new PZMessage(new PolyProcessor(), 4, player.getPersistentID()));
				break;
		}
	}

	private void handleSPZMessage(PZMessage packet) 
	{
		int piece = packet.piece;
		EntityPlayer player = PolyZonesMod.server.getPlayerList().getPlayerByUUID(packet.sender);
		if(player != null)
		{
			if(piece == 4)
			{
				if(!PolyZonesMod.playersMove.get(player.getDisplayNameString()))
				{
			    	PolyZonesMod.playersMove.put(player.getDisplayNameString(), new Boolean(true));
			    	player.addChatMessage(new TextComponentTranslation("Your data files have been confirmed. You are now unlocked."));
				}
			}
			ArrayList array = packet.array;
			PolyProcessor processor;
			CustomPlayer sender = PolyZonesMod.processor.getPlayer(packet.sender);
			
			if(receivingProcessors.get(sender) != null)
			{
				processor = receivingProcessors.get(sender);
			}
			else
			{
				processor = new PolyProcessor();
				receivingProcessors.put(sender, processor);
			}
			
			switch (piece)
			{
				case 0:
					processor.players = array;
					if(processor.equals(PolyZonesMod.processor))
					{
						if(!PolyZonesMod.playersMove.get(player.getDisplayNameString()))
						{
					    	PolyZonesMod.playersMove.put(player.getDisplayNameString(), new Boolean(true));
					    	player.addChatMessage(new TextComponentTranslation("Your data files have been confirmed. You are now unlocked."));
						}
					}
					else
					{
				    	PolyZonesMod.playersMove.put(player.getDisplayNameString(), new Boolean(false));
				    	player.addChatMessage(new TextComponentTranslation("Your data files are out of sync. Please wait."));
						PolyZonesMod.snw.sendTo(new PZMessage(PolyZonesMod.processor, 0, player.getPersistentID()), PolyZonesMod.server.getPlayerList().getPlayerByUUID(packet.sender));
					}
					break;
				case 1:
					processor.activePlayers = array;
					if(processor.equals(PolyZonesMod.processor))
					{
						if(!PolyZonesMod.playersMove.get(player.getDisplayNameString()))
						{
					    	PolyZonesMod.playersMove.put(player.getDisplayNameString(), new Boolean(true));
					    	player.addChatMessage(new TextComponentTranslation("Your data files have been confirmed. You are now unlocked."));
						}
					}
					else
					{
						PolyZonesMod.snw.sendTo(new PZMessage(PolyZonesMod.processor, 1, player.getPersistentID()), PolyZonesMod.server.getPlayerList().getPlayerByUUID(packet.sender));
					}
					break;
				case 2:
					processor.defaultAccess = array;
					if(processor.equals(PolyZonesMod.processor))
					{
						if(!PolyZonesMod.playersMove.get(player.getDisplayNameString()))
						{
					    	PolyZonesMod.playersMove.put(player.getDisplayNameString(), new Boolean(true));
					    	player.addChatMessage(new TextComponentTranslation("Your data files have been confirmed. You are now unlocked."));
						}
					}
					else
					{
						PolyZonesMod.snw.sendTo(new PZMessage(PolyZonesMod.processor, 2, player.getPersistentID()), PolyZonesMod.server.getPlayerList().getPlayerByUUID(packet.sender));
					}
					break;
				case 3:
					processor.zones = array;		
					if(processor.equals(PolyZonesMod.processor))
					{
						if(!PolyZonesMod.playersMove.get(player.getDisplayNameString()))
						{
					    	PolyZonesMod.playersMove.put(player.getDisplayNameString(), new Boolean(true));
					    	player.addChatMessage(new TextComponentTranslation("Your data files have been confirmed. You are now unlocked."));
						}
				    	return;
					}
					else
					{
						PolyZonesMod.snw.sendTo(new PZMessage(PolyZonesMod.processor, 3, player.getPersistentID()), PolyZonesMod.server.getPlayerList().getPlayerByUUID(packet.sender));
					}
					break;
			}
		}
	}
}
