package bitevo.Zetal.LoMaS.PolyZones;

import java.io.Serializable;
import java.util.ArrayList;

public class BlockLine implements Serializable
{

	BlockLoc point1;
	BlockLoc point2;
	int addValue = 0;
	ArrayList<BlockLoc> containedBlocks = new ArrayList<BlockLoc>();

	/**
	 * Constructs a new line between the given Block Locations
	 *
	 * @param one
	 *            The first BlockLoc.
	 * @param two
	 *            The second BlockLoc.
	 */
	public BlockLine(BlockLoc one, BlockLoc two)
	{
		point1 = one;
		point2 = two;
		this.setContainedBlocks();
	}

	/**
	 * Sets all blocks contained within this line.
	 */
	public void setContainedBlocks()
	{
		BlockLoc largeTemp = null;
		BlockLoc smallTemp = null;
		if (point1.getX() > point2.getX())
		{
			largeTemp = point1;
			smallTemp = point2;
			addValue = (int) point2.getZ();
		}
		else
		{
			largeTemp = point2;
			smallTemp = point1;
			addValue = (int) point1.getZ();
		}
		if (point1.getX() == point2.getX())
		{
			// add the two points to the graph and then
			// call the addInZDifferences() function
			containedBlocks.add(smallTemp);
			containedBlocks.add(largeTemp);
			this.addInZDifferences();
		}
		else
		{
			for (int i = 0; i < this.getLineLength(); i++)
			{
				if (smallTemp.getX() + i <= largeTemp.getX())
				{
					int x = (int) (smallTemp.getX() + i);
					containedBlocks.add(new BlockLoc(x, this.getZ(x)));
				}
				else
				{
					break;
				}
			}
			this.addInZDifferences();
		}
	}

	private void addInZDifferences()
	{
		// this method will handle points that are the same x value as well
		// to be implemented
		// scan through the arraylist of blocks and check their values
		// checks the differences in z values. if its greater than one we need
		// to insert blocks
		// between them
		for (int i = 0; i < containedBlocks.size() - 1; i++)
		{
			BlockLoc one = containedBlocks.get(i);
			BlockLoc two = containedBlocks.get(i + 1);
			int difference = (int) (Math.abs(two.getZ() - one.getZ()));
			if (difference > 1 || difference < -1)
			{
				// we need to add in more z blocks so there's no hole in the
				// line
				// first lets find out which z is higher
				BlockLoc highZ = one;
				BlockLoc lowZ = two;
				if (lowZ.getZ() > highZ.getZ())
				{
					highZ = two;
					lowZ = one;
				}
				difference = Math.abs(difference);
				if (difference % 2 == 1)
				{ // its odd
					int amount = difference / 2;
					amount = Math.abs(amount);
					int z = (int) lowZ.getZ();
					int x = (int) lowZ.getX();
					for (int j = 0; j < amount; j++)
					{
						// insert half the amounts at two's position w/ the x of
						// the first
						z = z + 1;
						containedBlocks.add(i + 1, new BlockLoc(x, z));
					}
					z = z + 1;
					containedBlocks.add(i + 1, new BlockLoc(x, z));
					x = (int) highZ.getX();
					for (int j = 0; j < amount - 1; j++)
					{
						z = z + 1;
						containedBlocks.add(i + 1, new BlockLoc(x, z));
					}
				}
				else
				{ // its even
					int amount = difference / 2;
					amount = Math.abs(amount);
					int z = (int) lowZ.getZ();
					int x = (int) lowZ.getX();
					int y = (int) lowZ.getY();
					for (int j = 0; j < amount; j++)
					{
						// insert half the amounts at two's position w/ the x of
						// the first
						z = z + 1;
						containedBlocks.add(i + 1, new BlockLoc(x, z));
					}
					x = (int) highZ.getX();
					for (int j = 0; j < amount - 1; j++)
					{
						z = z + 1;
						containedBlocks.add(i + 1, new BlockLoc(x, z));
					}
				}
			}
		}
	}

	/**
	 * Calculates and returns the length of this line cast to an integer.
	 *
	 * @return The length of this line rounded down to an int.
	 */
	public double getLineLength()
	{
		double xd = Math.abs(point1.getX() - point2.getX());
		double zd = Math.abs(point1.getZ() - point2.getZ());
		xd = Math.pow(xd, 2);
		zd = Math.pow(zd, 2);
		return Math.sqrt(xd + zd);
	}

	private int getZ(int x)
	{
		// had to spread these out because of interesting operation order...
		if (addValue == point2.getZ())
		{
			double deltaY = Math.abs(point1.getZ() - point2.getZ());
			double deltaX = Math.abs(point1.getX() - point2.getX());
			double slope = deltaY / deltaX;
			double place = x - point2.getX();
			double total = slope * place;
			total = total + addValue;
			return (int) total;
		}
		else
		{
			double deltaY = Math.abs(point2.getZ() - point1.getZ());
			double deltaX = Math.abs(point2.getX() - point1.getX());
			double slope = deltaY / deltaX;
			double place = x - point1.getX();
			double total = slope * place;
			total = total + addValue;
			return (int) total;
		}
	}

	/**
	 * Returns the ArrayList of contained blocks of this line.
	 *
	 * @return containedBlocks - List of all blocks contained in this line.
	 */
	public ArrayList<BlockLoc> getContainedBlocks()
	{
		return containedBlocks;
	}

	/**
	 * Checks to see whether a given block is contained within this line.
	 *
	 * @param b
	 *            The Block(Bukkit) to be checked.
	 * @return True if the Block is contained within the line. Ignores the y coordinate.
	 */
	/*
	 * public boolean crossThis(Block b) { BlockLoc temp = BlockLoc.getBlockLoc(b); for (BlockLoc x : containedBlocks) { if (x.equalsIgnoreY(temp)) return true; } return false; }
	 */

	/**
	 * Checks to see whether a given BlockLoc is contained within this line.
	 *
	 * @param b
	 *            Block Location to be checked.
	 * @return True if the Block Location is contained within the line. Ignores the y coordinate.
	 */
	public boolean crossThis(BlockLoc b)
	{
		BlockLoc temp = b;
		for (BlockLoc x : containedBlocks)
		{
			if (x.equalsIgnoreY(temp)) return true;
		}
		return false;
	}

	@Override
	public String toString()
	{
		return this.containedBlocks.toString();
	}
}
