package bitevo.Zetal.LoMaS.Specializations.Renderer;

import bitevo.Zetal.LoMaS.Specializations.Entity.EntityCustomArrow;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityCustomXPOrb;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityDiamondShard;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityFactionDiamondGolem;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntityFactionIronGolem;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntitySpecialFishHook;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderFish;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderXPOrb;
import net.minecraftforge.fml.client.registry.IRenderFactory;

public class SpecializationsRendererFactory
{
	public class ArrowRendererFactory implements IRenderFactory<EntityCustomArrow>
	{
		@Override
		public Render<? super EntityCustomArrow> createRenderFor(RenderManager manager)
		{
			return new RenderCustomArrow(manager);
		}
	}

	public class XPRendererFactory implements IRenderFactory<EntityCustomXPOrb>
	{
		@Override
		public Render<? super EntityCustomXPOrb> createRenderFor(RenderManager manager)
		{
			return new RenderXPOrb(manager);
		}
	}

	public class FishHookRendererFactory implements IRenderFactory<EntitySpecialFishHook>
	{
		@Override
		public Render<? super EntitySpecialFishHook> createRenderFor(RenderManager manager)
		{
			return new RenderFish(manager);
		}
	}
	
	public class IronGolemRendererFactory implements IRenderFactory<EntityFactionIronGolem>
	{
		@Override
		public Render<? super EntityFactionIronGolem> createRenderFor(RenderManager manager)
		{
			return new RenderIronGolem(manager);
		}
	}
	
	public class DiamondGolemRendererFactory implements IRenderFactory<EntityFactionDiamondGolem>
	{
		@Override
		public Render<? super EntityFactionDiamondGolem> createRenderFor(RenderManager manager)
		{
			return new RenderDiamondGolem(manager);
		}
	}
	
	public class DiamondShardRendererFactory implements IRenderFactory<EntityDiamondShard>
	{
		@Override
		public Render<? super EntityDiamondShard> createRenderFor(RenderManager manager)
		{
			return new RenderDiamondShard(manager);
		}
	}
}
