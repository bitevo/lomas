package bitevo.Zetal.LoMaS.Specializations.Message;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Animal;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AnimalMessageHandler implements IMessageHandler<AnimalMessage, IMessage>
{
	static World world;

	@Override
	@SideOnly(Side.CLIENT)
	public IMessage onMessage(AnimalMessage message, MessageContext ctx)
	{
		world = Minecraft.getMinecraft().theWorld;
		handleAnimalMessage(message);
		return null;
	}

	@SideOnly(Side.CLIENT)
	private void handleAnimalMessage(AnimalMessage packet)
	{
		int entityID = packet.entityId;
		int age = packet.age;
		int hunger = packet.hunger;
		int sickness = packet.sickness;
		boolean isReady = packet.isReady;
		Entity entity = world.getEntityByID(entityID);
		if (entity != null && entity instanceof EntityAnimal)
		{
			EntityAnimal ea = (EntityAnimal) entity;
			LoMaS_Animal lAnimal = LoMaS_Animal.getLoMaSAnimal(ea);
			if (lAnimal == null)
			{
				LoMaS_Animal.addLoMaSAnimal(ea);
				lAnimal = LoMaS_Animal.getLoMaSAnimal(ea);
			}

			lAnimal.setAge(age);
			lAnimal.setHunger(hunger);
			lAnimal.setSickness(sickness);
			lAnimal.setReady(isReady);
		}
	}
}
