package bitevo.Zetal.LoMaS.PolyZones.Handler;

import java.util.ConcurrentModificationException;

import net.minecraft.block.Block;
import net.minecraft.block.BlockButton;
import net.minecraft.block.BlockChest;
import net.minecraft.block.BlockDoor;
import net.minecraft.block.BlockFenceGate;
import net.minecraft.block.BlockFurnace;
import net.minecraft.block.BlockLever;
import net.minecraft.block.BlockTrapDoor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.player.FillBucketEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import bitevo.Zetal.LoMaS.PolyZones.BlockLoc;
import bitevo.Zetal.LoMaS.PolyZones.BlockPolygon;
import bitevo.Zetal.LoMaS.PolyZones.CustomPlayer;
import bitevo.Zetal.LoMaS.PolyZones.PolyZone;
import bitevo.Zetal.LoMaS.PolyZones.PolyZonesMod;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockSpecFurnace;

/**
 * Name and cast of this class are irrelevant
 */
public class ClientEventHookContainerClass
{
	public BlockPolygon bPoly = null;
	public PolyZone temp = null;
	public PolyZone editingZone = null;
	public String isEditing = "";
	public String name = "";
	public World world;
	public int x;
	public int y;
	public int z;
   
        @SubscribeEvent
        @SideOnly(Side.CLIENT)
        public void onEntityMove(LivingUpdateEvent event)
        {
	        if(event.getEntity() instanceof EntityPlayer && event.getEntity() != null)
	    	{
    			EntityPlayer player = (EntityPlayer) event.getEntity();
	        	if((((int)(player.posY - player.getEyeHeight()) == (int)player.posY) && FMLCommonHandler.instance().getSide() == Side.CLIENT) || ((int)(player.posY + player.getEyeHeight()) == (int)(player.posY+1.62)))
	        	{
		        	if(PolyZonesMod.processor.getPlayer(((EntityPlayer)event.getEntity()).getPersistentID()) == null)
		        	{
			        	PolyZonesMod.processor.addPlayer((EntityPlayer)event.getEntity());
		    			System.out.println("Added a new CustomPlayer with the name " + ((EntityPlayer)event.getEntity()).getDisplayNameString());
		        	}
		        	if (player.worldObj.provider.getDimension() == -1 || player.worldObj.provider.getDimension() == 1) 
		        	{
			        	event.setCanceled(false);
			        	return;
		        	}
		        	CustomPlayer t = PolyZonesMod.processor.getPlayer(player.getPersistentID());
		        	PolyZone temp = PolyZonesMod.processor.contained(player);
		        	if (temp != null) 
		        	{
			        	t.insideNow(temp.getName());
			        	int p = t.getAccess(temp.getName()).getPermissions();
			        	
			        	if ((p & 2) == 2) 
			        	{
			        		player.setPositionAndRotation(player.prevPosX, player.prevPosY, player.prevPosZ, (float) (player.prevCameraYaw + 2*Math.PI), player.cameraPitch);
			        	}
			        	if ((p & 32) != 32) 
			        	{
			        		try
			        		{
				        		if (player.getHealth() < 20) 
				        		{
				        			player.heal(1);
				        		}
			        		}
			        		catch(ConcurrentModificationException e){}
			        	}
		        	}
		        	else
		        	{
		        		t.outsideNow();
		        	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		    		if (player.worldObj.provider.getDimension() != -1 && player.worldObj.provider.getDimension() != 1)
		    		{
		    			if(player.worldObj.isRemote == false)
		    			{
			    		    double x = player.posX;
			    		    double z = player.posZ;
			    		    /*if(x > -100 && x < 100 && z < 100 && z > -100 && !t.getIsInTown())
			    		    {
			    				player.addChatMessage(new ChatComponentText("You are now in the Central Town."));
				    			t.insideLandNow("Town");
			    		    	t.setIsInTown(true);
			    		    }
			    		    if((x < -100 || x > 100 || z > 100 || z < -100) && t.getIsInTown())
			    		    {
			    		    	t.setIsInTown(false);
			    		    }*/
				    		///////////////////////////////////////////////////////////////////////
				    		if(x > 0.1 && !t.getIsPositiveX())
				    		{
				    			t.setIsPositiveX(true);
				    		}
				    		else if(x < -0.1 && t.getIsPositiveX())
				    		{
				    			t.setIsPositiveX(false);
				    		}
				    		if(z > 0.1 && !t.getIsPositiveZ())
				    		{
				    			t.setIsPositiveZ(true);
				    		}
				    		else if(z < -0.1 && t.getIsPositiveZ())
				    		{
				    			t.setIsPositiveZ(false);
				    		}
				    		/*if(t.getIsPositiveX() && t.getIsPositiveZ() && !t.getIsInTown() && (t.getCurrentLand() != "Mountains"))
				    		{
			    				player.addChatMessage(new ChatComponentText("You are now in the Land of Mountains."));
				    			t.insideLandNow("Mountains");
				    		}
				    		else if(t.getIsPositiveX() && !t.getIsPositiveZ() && !t.getIsInTown() && (t.getCurrentLand() != "Stone"))
				    		{
				    			player.addChatMessage(new ChatComponentText("You are now in the Land of Stone."));
				    			t.insideLandNow("Stone");
				    		}
				    		else if(!t.getIsPositiveX() && t.getIsPositiveZ() && !t.getIsInTown() && (t.getCurrentLand() != "Light"))
				    		{
				    			player.addChatMessage(new ChatComponentText("You are now in the Land of Light."));
				    			t.insideLandNow("Light");
				    		}
				    		else if(!t.getIsPositiveX() && !t.getIsPositiveZ() && !t.getIsInTown() && (t.getCurrentLand() != "Water"))
				    		{
				    			player.addChatMessage(new ChatComponentText("You are now in the Land of Water."));
				    			t.insideLandNow("Water");
				    		}*/
			    		}
		    		}
	        	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        	}
        }
        
        @SubscribeEvent
        @SideOnly(Side.CLIENT)
        public void entityAttacked(LivingAttackEvent event)
        {	    		
        	if (event.getEntity().worldObj.provider.getDimension() == -1 || event.getEntity().worldObj.provider.getDimension() == 1)
        	{
        		return;
        	}
    		BlockLoc lol = new BlockLoc((int)event.getEntity().posX, (int)event.getEntity().posY, (int)event.getEntity().posZ);
    		PolyZone t = PolyZonesMod.processor.contained(lol, event.getEntity().worldObj);
    		if (t != null) 
    		{
    			if (t.getPermissions().getPvp()) 
    			{
    			    event.setCanceled(true);
    		    }
    		}
        }
        
    	@SubscribeEvent(priority = EventPriority.HIGHEST)
        @SideOnly(Side.CLIENT)
    	public void breakSpeedEvent(PlayerEvent.BreakSpeed event)
    	{
    		EntityPlayer player = event.getEntityPlayer();
    		ItemStack itemstack = player.getHeldItemMainhand();//.getItem();
    		Block block = event.getState().getBlock();
    		
        	if (event.getEntity().worldObj.provider.getDimension() == -1 || event.getEntity().worldObj.provider.getDimension() == 1)
        	{
        		return;
        	}

    		CustomPlayer temp = PolyZonesMod.processor.getPlayer(event.getEntityPlayer().getPersistentID());
    		BlockLoc lol = new BlockLoc(event.getPos().getX(), event.getPos().getY(), event.getPos().getZ());
    		PolyZone t = PolyZonesMod.processor.contained(lol, event.getEntityPlayer().worldObj);
    		if (t != null) 
    		{
    			int p = temp.getAccess(t.getName()).getPermissions();
    			if ((p & 16) == 16) 
    			{
    			    event.setNewSpeed(0.0f);
    			    event.setCanceled(true);
    		    }
    		}
    	}
        
        @SubscribeEvent
        @SideOnly(Side.CLIENT)
        public void playerRightClickOrInteract(PlayerInteractEvent.RightClickBlock event)
        {
        	if (event.getEntity().worldObj.provider.getDimension() == -1 || event.getEntity().worldObj.provider.getDimension() == 1)
        	{
        		return;
        	}
    		Block b = event.getEntityPlayer().worldObj.getBlockState(event.getPos()).getBlock();
        	if(b instanceof BlockLever
    		|| b instanceof BlockButton
			|| b instanceof BlockFurnace
			|| b instanceof BlockSpecFurnace
			|| b instanceof BlockChest
			|| b instanceof BlockDoor
			|| b instanceof BlockTrapDoor
			|| b instanceof BlockFenceGate)
        	{
        		CustomPlayer temp = PolyZonesMod.processor.getPlayer(event.getEntityPlayer().getPersistentID());
        		BlockLoc lol = new BlockLoc(event.getPos().getX(), event.getPos().getY(), event.getPos().getZ());
        		PolyZone t = PolyZonesMod.processor.contained(lol, event.getEntityPlayer().worldObj);
        		if (t != null) 
        		{
        			int p = temp.getAccess(t.getName()).getPermissions();
        			if ((p & 4) == 4) 
        			{
        			    event.setCanceled(true);
        		    }
        		}
        	}
        }
        
        @SubscribeEvent
        @SideOnly(Side.CLIENT)
        public void playerRightClickWithBucket(FillBucketEvent event)
        {
        	if (event.getEntity().worldObj.provider.getDimension() == -1 || event.getEntity().worldObj.provider.getDimension() == 1)
        	{
        		return;
        	}
        	CustomPlayer temp = PolyZonesMod.processor.getPlayer(event.getEntityPlayer().getPersistentID());
        	BlockLoc lol = new BlockLoc(event.getTarget().getBlockPos().getX(), event.getTarget().getBlockPos().getY(), event.getTarget().getBlockPos().getZ());
        	PolyZone t = PolyZonesMod.processor.contained(lol, event.getEntityPlayer().worldObj);
        	if (t != null) 
        	{
        		int p = temp.getAccess(t.getName()).getPermissions();
        		if (event.getEmptyBucket().getItem() != Items.BUCKET && ((p & 8) == 8)) 
        		{
        		    event.setCanceled(true);
        	    }
        		else if (event.getEmptyBucket().getItem() == Items.BUCKET && ((p & 16) == 16)) 
        		{
        		    event.setCanceled(true);
        	    }
        	}
        }
}