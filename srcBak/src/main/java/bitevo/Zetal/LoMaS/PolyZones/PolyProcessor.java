package bitevo.Zetal.LoMaS.PolyZones;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class PolyProcessor implements Serializable {

	private static final long serialVersionUID = 1L;
	public ArrayList<PolyZone> zones = new ArrayList<PolyZone>();
	public ArrayList<CustomPlayer> players = new ArrayList<CustomPlayer>();
	public ArrayList<CustomPlayer> activePlayers = new ArrayList<CustomPlayer>();
	public ArrayList<Access> defaultAccess = new ArrayList<Access>();

	public PolyProcessor() {}

	public CustomPlayer getPlayer(UUID uuid) {
		for (CustomPlayer x : players) {
			if (uuid != null && x.getUUID().equals(uuid)) {
				return x;
			}
		}
		return null;
	}

	/**
	 * Adds a player to the active players list. If the player was not on the
	 * list of all players before hand then the player is added. If the player
	 * is brand new default permissions for all zones are assigned.
	 * 
	 * @param p
	 *            The player to be added to this game.
	 */
	public void addPlayer(EntityPlayer p) {
		UUID uuid = p.getPersistentID();
		for (CustomPlayer x : players) {
			if (x.getUUID().equals(uuid)) {
				return;
			}
		}
		CustomPlayer temp = new CustomPlayer(p);
		temp.setZoneAccessArray(defaultAccess);
		players.add(temp);
	}
	
	public void addPlayer(UUID p) {
		for (CustomPlayer x : players) {
			if (x.getUUID().equals(p)) {
				return;
			}
		}
		CustomPlayer temp = new CustomPlayer(p);
		temp.setZoneAccessArray(defaultAccess);
		players.add(temp);
	}

	public void removePlayer(EntityPlayer p) {
		UUID uuid = p.getPersistentID();
		for (CustomPlayer y : activePlayers) {
			if (y.getUUID().equals(uuid)) {
				activePlayers.remove(y);
			}
		}
	}

	public PolyZone getZone(String name) {
		for (PolyZone x : zones) {
			if (x.getName().equals(name))
				return x;
		}
		return null;
	}

	/**
	 * This is called after changing the default permissions of a certain zone.
	 * We say update all players permissions for this zone and also update the
	 * defaultAcess array with the new permissions.
	 * 
	 * @param polyname
	 */
	public void updateDefaultPermissions(String polyname) {
		PolyZone temp = this.getZone(polyname);
		for (CustomPlayer y : players) {
			y.getAccess(polyname).setPermissionValue(
					temp.getPermissions().getDefaultPermissions()
							.getPermissions());
		}
		for (Access x : defaultAccess) {
			if (x.getPolyZoneName().equals(polyname)) {
				x.setPermissionValue(temp.getPermissions()
						.getDefaultPermissions().getPermissions());
			}
		}
	}

	/**
	 * Adds a new polyZone to this processor object.  The zones default permissions
	 * are loaded to the database.
	 * @param x
	 * The zone to be added.
	 */
	public void addZone(PolyZone x) {
		zones.add(x);
		defaultAccess.add(x.getPermissions().getDefaultPermissions());
		for(CustomPlayer g : players){
			g.addZone(x.getName(), x.getPermissions().getDefaultPermissions().getPermissions());
		}
		/*if(x.getName().equalsIgnoreCase("sedrons"))
		{
			Access def = x.getPermissions().getDefaultPermissions();
			def.denyAll();
			def.allowEnter();
			def.allowHealthRegen();
            Access tem = PolyZonesMod.processor.getPlayer("Sedrons").getAccess(x.getName());
            tem.allowAll();
		}
		else if(x.getName().equalsIgnoreCase("dylan"))
		{
			Access def = x.getPermissions().getDefaultPermissions();
			def.denyAll();
			def.allowEnter();
			def.allowHealthRegen();
            Access tem = PolyZonesMod.processor.getPlayer("dylanyates92").getAccess(x.getName());
            tem.allowAll();
		}
		else if(x.getName().equalsIgnoreCase("zetal"))
		{
			Access def = x.getPermissions().getDefaultPermissions();
			def.denyAll();
			def.allowEnter();
			def.allowHealthRegen();
            Access tem = PolyZonesMod.processor.getPlayer("Zetal").getAccess(x.getName());
            tem.allowAll();
		}
		else if(x.getName().equalsIgnoreCase("pwebbz"))
		{
			Access def = x.getPermissions().getDefaultPermissions();
			def.denyAll();
			def.allowEnter();
			def.allowHealthRegen();
            Access tem = PolyZonesMod.processor.getPlayer("pwebbz").getAccess(x.getName());
            tem.allowAll();
		}*/
	}

	/**
	 * Called when a polyZone is removed from the map.
	 * 
	 * @param x
	 *            The zone to be removed.
	 */
	public boolean removeZone(PolyZone x) {
		if (zones.contains(x)) {
			zones.remove(x);
			defaultAccess.remove(x.getPermissions().getDefaultPermissions());
			DataHandler.writeObject(this);
			return true;
		}
		return false;
	}

	/**
	 * Called at the start up of the server. Clears the active player list.
	 */
	public void newGame() {
		activePlayers.clear();
	}

	/**
	 * Returns true if the given player is inside a polygon. Operation time
	 * O(n).
	 * 
	 * @param p
	 *            The player to be checked.
	 * @return True or False.
	 */
	public PolyZone contained(EntityPlayer p) {
		for (PolyZone x : zones) {
			if (x.isInside(p)) {
				return x;
			}
		}
		return null;
	}
	
	/**
	 * Returns true if the given blockloc is inside a polygon. Operation time
	 * O(n).
	 * 
	 * @param p
	 *            The blockloc to be checked.
	 * @return True or False.
	 */
	public PolyZone contained(BlockLoc p, World w){
		for (PolyZone x : zones) {
			if (x.isInside(p, w)) {
				return x;
			}
		}
		return null;
	}
	
	public PolyZone contained(World w, Chunk c){
		for (PolyZone x : zones) {
			if (x.isInside(w, c)) {
				return x;
			}
		}
		return null;
	}
	
	@Override
	public String toString() {
		String retVal = "";
		for(PolyZone x : zones){
			retVal = retVal + x.toString();
		}
		for (Access s : defaultAccess){
			retVal = retVal + s.getPermissions();
		}
		return retVal;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof PolyProcessor)
		{
			PolyProcessor in = (PolyProcessor) o;
			if(in.zones.equals(this.zones) && in.players.equals(this.players)
			&& in.activePlayers.equals(this.activePlayers) && in.defaultAccess.equals(this.defaultAccess))
			{
				return true;
			}
		}
		return false;
	}
}
