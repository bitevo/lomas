package bitevo.Zetal.LoMaS.Specializations.Message;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Handler.FMLHandler;
import bitevo.Zetal.LoMaS.Specializations.Handler.SCMEventHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class SpecializationMessageHandler implements IMessageHandler<SpecializationMessage, IMessage>
{
	@Override
	public IMessage onMessage(SpecializationMessage message, MessageContext ctx)
	{
		if (ctx.side == Side.CLIENT)
		{
			handleClientSpecializationMessage(message);
		}
		else
		{
			handleServerSpecializationMessage(message);
		}
		return null;
	}

	@SideOnly(Side.CLIENT)
	private void handleClientSpecializationMessage(SpecializationMessage packet)
	{
		if (packet.username != null)
		{
			World world = Minecraft.getMinecraft().theWorld;
			if (world != null)
			{
				EntityPlayer player = world.getPlayerEntityByUUID(packet.username);
				LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
				if (player != null)
				{
					if (lplayer == null)
					{
						LoMaS_Player.addLoMaSPlayer(player);
						lplayer = LoMaS_Player.getLoMaSPlayer(player);
					}
					lplayer.setSpecClassLevel(packet.classlevel);
					lplayer.progress = packet.progress;
					lplayer.levelDisplay = packet.levelDisplay;
					//System.out.println(packet.skills + " " + lplayer.getSkills().toString());
					if(packet.skills != null && !packet.skills.isEmpty() && !packet.skills.equalsIgnoreCase(lplayer.getSkills().toString()))
					{
						//System.out.println("HI THERE");
						lplayer.setSkills(packet.skills);
					}
					lplayer.rank = packet.rank;
					lplayer.guildtag = packet.guildtag;
					lplayer.uuid = packet.username;
					FMLHandler.setPlayerMaxHealth(player);
					SCMEventHandler.setPlayerWalkSpeed(player);
				}
				else
				{
					System.err.println("Client could not find matching player for Specialization Packet! Out of Render Distance, or serious error!");
				}
			}
		}
		else
		{
			System.err.println("A null UUID was received from SpecializationsHandler on the Client.");
		}
	}

	private void handleServerSpecializationMessage(SpecializationMessage packet)
	{
		EntityPlayer player = (EntityPlayer) SpecializationsMod.server.getEntityFromUuid(packet.username);
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(player);
		if (player != null)
		{
			if(packet.skills != null && !packet.skills.isEmpty())
			{
				int spentPoints = packet.skills.split(" ").length - lplayer.getSkills().getOwnedNodes().size();
				if(spentPoints <= lplayer.getUnspentPoints())
				{
					lplayer.setSkills(packet.skills);
				}
			}
			//System.out.println("serrrveerrr " + packet.skills);
			FMLHandler.setPlayerMaxHealth(player);
			SCMEventHandler.setPlayerWalkSpeed(player);
			SpecializationsMod.sendPlayerAbilitiesToClients(lplayer);
		}
		else
		{
			System.err.println("Server could not find matching player for Specialization Packet, they're probably just dead though.");
		}
	}
}
