package bitevo.Zetal.LoMaS.PolyZones;

import java.util.HashMap;

import net.minecraft.command.ServerCommandManager;
import net.minecraft.entity.Entity;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import bitevo.Zetal.LoMaS.PolyZones.Handler.ClientEventHookContainerClass;
import bitevo.Zetal.LoMaS.PolyZones.Handler.CommandCreatePolyzone;
import bitevo.Zetal.LoMaS.PolyZones.Handler.CommonProxyPolyZones;
import bitevo.Zetal.LoMaS.PolyZones.Handler.EventHookContainerClass;
import bitevo.Zetal.LoMaS.PolyZones.Handler.PolyZonesConnectionHandler;
import bitevo.Zetal.LoMaS.PolyZones.Handler.TickHandler;
import bitevo.Zetal.LoMaS.PolyZones.Message.PZMessage;
import bitevo.Zetal.LoMaS.PolyZones.Message.PZMessageHandler;

@Mod(modid = PolyZonesMod.modid, name = "PolyZones", version = "0.1")
public class PolyZonesMod 
{
	public static final String modid = "lomas_polyzones";

	public static CommonProxyPolyZones proxy = new CommonProxyPolyZones();
	public static PolyProcessor processor = new PolyProcessor();
	public static String playerEditing = "";
	public static MinecraftServer server;
	public static boolean grabbing = false;
	public static boolean height = false;
	public static String ip;
	public static int portAttempt = 25568;
	public static int mapBoundaries = 10000;
	public static HashMap<String, Boolean> playersMove = new HashMap<String, Boolean>();
    public static SimpleNetworkWrapper snw;

	@EventHandler
	public void init(FMLInitializationEvent event) {
		processor = DataHandler.getProcessor();
		FMLCommonHandler.instance().bus().register(new PolyZonesConnectionHandler());
		MinecraftForge.EVENT_BUS.register(new ClientEventHookContainerClass());
	}

	@EventHandler
	public void serverStart(FMLServerStartingEvent event) 
	{
		server = event.getServer();
		processor = DataHandler.getProcessor();
		MinecraftForge.EVENT_BUS.register(new EventHookContainerClass());
		MinecraftForge.EVENT_BUS.register(new TickHandler());

		ServerCommandManager scm = (ServerCommandManager) event.getServer().getCommandManager();
		scm.registerCommand(new CommandCreatePolyzone());
	}

    @EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		snw = NetworkRegistry.INSTANCE.newSimpleChannel(modid);
		snw.registerMessage(PZMessageHandler.class, PZMessage.class, 7783, Side.CLIENT);
		snw.registerMessage(PZMessageHandler.class, PZMessage.class, 7783, Side.SERVER);
	}
	
	public static boolean canEntitySpawnHere(Entity entity)
	{
    	BlockLoc entLoc = new BlockLoc((int)entity.posX, (int)entity.posY, (int)entity.posZ);
    	PolyZone temp = PolyZonesMod.processor.contained(entLoc, entity.worldObj);
    	if(temp == null)
    	{
    		return true;
    	}
    	else
    	{
    		if(!temp.getPermissions().getMobs())
    		{
    			return false;
    		}
    		else
    		{
    			return true;
    		}
    	}
	}
}
