package bitevo.Zetal.LoMaS.PolyZones;

import java.io.Serializable;

public class Access implements Serializable {

	private int permission = 62;
	private int lastpermission = permission;
	private String polyName = "";
	private int defaultPermission = 0;

	public Access(String p) {
		polyName = p;
	}
	
	public Access(String p, int l){
		polyName = p;
		permission = l;
		lastpermission = permission;
	}
	
	/**
	 * Gets the permissions for this object.
	 * @return
	 * permission - the integer for permissions
	 */
	public int getPermissions(){
		return permission;
	}
	
	/**
	 * Returns the name of the polygon this object refers to.
	 * @return
	 * polyName - The name of the polygon.
	 */
	public String getPolyZoneName(){
		return polyName;
	}
	
	/**
	 * Sets this objects polygon to t.
	 * @param t
	 * The name of the new polygon.
	 */
	public void setPolyZoneName(String t){
		polyName = t;
	}

	/**
	 * Deny all access to this zone.
	 */
	public void denyAll() {
		lastpermission = permission;
		denyEnter();
		denyInteract();
		denyPlace();
		denyDestroy();
		denyHealthRegen();
	}

	/**
	 * Allow full access to this zone.
	 */
	public void allowAll() {
		lastpermission = permission;
		allowEnter(); //done
		allowInteract(); 
		allowPlace(); //done
		allowDestroy(); //done
		allowHealthRegen(); //done
	}
	
	/**
	 * Revert the permissions to one before.
	 */
	public void revert() {
		int temp = permission;
		permission = lastpermission;
		lastpermission = temp;
	}

	/**
	 * Allow entering permissions.
	 */
	public void allowEnter() {
		lastpermission = permission;
		permission = permission & ~2;
	}

	/**
	 * Allow entering permissions.
	 */
	public void denyEnter() {
		lastpermission = permission;
		permission = permission | 2;
	}

	/**
	 * Deny interaction.
	 */
	public void allowInteract() {
		lastpermission = permission;
		permission = permission & ~4;
	}

	/**
	 * Allow interaction.
	 */
	public void denyInteract() {
		lastpermission = permission;
		permission = permission | 4;
	}

	/**
	 * Deny placing of blocks.
	 */
	public void allowPlace() {
		lastpermission = permission;
		permission = permission & ~8;
	}

	/**
	 * Allow placing of blocks.
	 */
	public void denyPlace() {
		lastpermission = permission;
		permission = permission | 8;
	}

	/**
	 * Deny destruction of blocks.
	 */
	public void allowDestroy() {
		lastpermission = permission;
		permission = permission & ~16;
	}

	/**
	 * Allow destruction of blocks.
	 */
	public void denyDestroy() {
		lastpermission = permission;
		permission = permission | 16;
	}

	/**
	 * Deny health regeneration.
	 */
	public void allowHealthRegen() {
		lastpermission = permission;
		permission = permission & ~32;
	}

	/**
	 * Allow health regeneration.
	 */
	public void denyHealthRegen() {
		lastpermission = permission;
		permission = permission | 32;
	}
	
	/**
	 * Returns a clone of this object with matching zone name and permissions.
	 */
	public Object clone(){
		Access temp = new Access(polyName);
		temp.permission = this.permission;
		return temp;
	}
	
	public void setPermissionValue(int x){
		lastpermission = permission;
		permission = x;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Access)
		{
			Access in = (Access) o;
			if(in.polyName.equalsIgnoreCase(this.polyName) && in.permission == this.permission)
			{
				return true;
			}
		}
		return false;
	}
}
