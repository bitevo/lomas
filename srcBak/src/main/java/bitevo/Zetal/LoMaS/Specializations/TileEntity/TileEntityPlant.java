package bitevo.Zetal.LoMaS.Specializations.TileEntity;

import java.util.UUID;

import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class TileEntityPlant extends TileEntityQualityBlock
{
    private byte farmerLevel = -1;
    private UUID owner;
	/**
	 * Quality is a scale 0-150, with a base of 100.
	 * Every tick that a plant is unfertilized, it drops by 15.
	 * 0-75 = Poor, 75-100 = Decent, 100-150 = Superb.
	 * Fertilizing a plant raises its quality by 5.
	 */
	private boolean isFertilized = false;
	/**
	 * Measured in half-seconds.
	 */
	private int fertTime = 0;

    public NBTTagCompound writeToNBT(NBTTagCompound compound)
    {
        super.writeToNBT(compound);
        compound.setByte("farmerLevel", this.getFarmerLevel());
        if(this.owner != null)
        {
            try
            {
                compound.setString("owner", getOwner().toString());
            }
            catch(Exception e)
            {
            	//System.err.println("There was an error while writing an existing TileEntityPlant.");
            }
        }
        compound.setBoolean("isFertilized", this.isFertilized);
        compound.setInteger("fertTime", this.fertTime);
		return compound;
    }
    
    public void readFromNBT(NBTTagCompound compound)
    {
        super.readFromNBT(compound);
        this.setFarmerLevel(compound.getByte("farmerLevel"));
        if(compound.hasKey("owner"))
        {
            try
            {
                this.setOwner(UUID.fromString(compound.getString("owner")));
            }
            catch(Exception e)
            {
            	//System.err.println("There was an error while reading an existing TileEntityPlant.");
            	this.setOwner(null);
            }
        }
        this.isFertilized = compound.getBoolean("isFertilized");
        this.fertTime = compound.getInteger("fertTime");
    }
    
    @Override
    public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newState)
    {
    	return !oldState.getBlock().equals(newState.getBlock());
    }
    
    public void setFarmerLevel(byte newLevel)
    {
        this.farmerLevel = newLevel;
    }
    
    public void setOwner(UUID owner)
    {
        this.owner = owner;
    }

	public UUID getOwner()
	{
		return owner;
	}

	public byte getFarmerLevel()
	{
		return farmerLevel;
	}

	public boolean isFertilized()
	{
		return isFertilized;
	}

	public void setFertilized(boolean isFertilized)
	{
		if(isFertilized)
		{
			this.setFertTime(6000);
		}
		this.isFertilized = isFertilized;
	}

	/**
	 * Measured in half-seconds.
	 * @return
	 */
	public int getFertTime()
	{
		return fertTime;
	}

	/**
	 * Measured in half-seconds.
	 * @param fertTime
	 */
	public void setFertTime(int fertTime)
	{
		this.fertTime = fertTime;
	}
	
	@Override
    public SPacketUpdateTileEntity getUpdatePacket()
    {
        return new SPacketUpdateTileEntity(pos, 0, this.getUpdateTag());
    }

	@Override
    public NBTTagCompound getUpdateTag()
    {
        return this.writeToNBT(new NBTTagCompound());
    }

	@Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt)
    {
        NBTTagCompound nbttagcompound = pkt.getNbtCompound();
        this.readFromNBT(nbttagcompound);
    }
}
