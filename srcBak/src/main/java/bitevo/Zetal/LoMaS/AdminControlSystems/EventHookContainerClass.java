package bitevo.Zetal.LoMaS.AdminControlSystems;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Dictionary;
import java.util.List;
import java.util.Scanner;

import net.minecraft.block.Block;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandKill;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.storage.WorldInfo;
import net.minecraftforge.event.CommandEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.RightClickBlock;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;

public class EventHookContainerClass
{
	public String editor1;
	public String editor2;
	public ArrayList<Point3d> pointsArray1;
	public ArrayList<Point3d> pointsArray2;
	public String shapetype1;
	public String shapetype2;
	public int radius1;
	public int radius2;
	public int lenx1;
	public int lenx2;
	public int lenz1;
	public int lenz2;
	public int height1;
	public int height2;
	public int ID1;
	public int ID2;
	public boolean isHollow1;
	public boolean isHollow2;
	public boolean isConcaveUp1;
	public boolean isConcaveUp2;
	public static ArrayList<String> motd;
	
	///////////////////////////////
	public Dictionary colors;
	public World world;
	public int oldx;
	public int oldy;
	public int oldz;
	
	    @SubscribeEvent
	    public void playerAddShapePoint(RightClickBlock event)
	    {
	    	if(editor1 == event.getEntityPlayer().getDisplayNameString()
	    			&& (oldx != event.getPos().getX() || oldy != event.getPos().getY() || oldz != event.getPos().getZ()))
	    	{
				//System.out.println("step 1");
	    		oldx = event.getPos().getX();
	    		oldy = event.getPos().getY();
	    		oldz = event.getPos().getZ();
	        	if(event.getEntityPlayer().getDisplayNameString() != null && editor1 == event.getEntityPlayer().getDisplayNameString())
	        	{
					//System.out.println("step 2");
	        		world = event.getEntityPlayer().worldObj;				        		
	        		if(world.isRemote == false)
	        		{
		        		this.sendChatToPlayer(event.getEntityPlayer(), "This point has been set with X: " + event.getPos().getX() + " Y: " + event.getPos().getY() + " Z: " + event.getPos().getZ());
		        		this.sendChatToPlayer(event.getEntityPlayer(), "Remember to type '/createshape done' when you are finished.");
	        		}
	        		if(pointsArray1 != null)
	        		{
	    				//System.out.println("step 3");
	        			pointsArray1.add(new Point3d(event.getPos().getX(), event.getPos().getY(), event.getPos().getZ()));
	        		}
	        		else
	        		{
	    				//System.out.println("step 4");
	        			pointsArray1 = new ArrayList<Point3d>();
	        			pointsArray1.add(new Point3d(event.getPos().getX(), event.getPos().getY(), event.getPos().getZ()));	        		
	        		}
	        	}
	    	}
	    	if(editor2 == event.getEntityPlayer().getDisplayNameString()
	    			&& (oldx != event.getPos().getX() || oldy != event.getPos().getY() || oldz != event.getPos().getZ()))
	    	{
				//System.out.println("step 5");
	    		oldx = event.getPos().getX();
	    		oldy = event.getPos().getY();
	    		oldz = event.getPos().getZ();
	        	if(event.getEntityPlayer().getDisplayNameString() != null && editor2 == event.getEntityPlayer().getDisplayNameString())
	        	{
					//System.out.println("step 6");
	        		world = event.getEntityPlayer().worldObj;				        		
	        		if(world.isRemote == false)
	        		{
		        		this.sendChatToPlayer(event.getEntityPlayer(), "This point has been set with X: " + event.getPos().getX() + " Y: " + event.getPos().getY() + " Z: " + event.getPos().getZ());
		        		this.sendChatToPlayer(event.getEntityPlayer(), "Remember to type '/createshape done' when you are finished.");
	        		}
	        		if(pointsArray2 != null)
	        		{
	    				//System.out.println("step 7");
	        			pointsArray2.add(new Point3d(event.getPos().getX(), event.getPos().getY(), event.getPos().getZ()));
	        		}
	        		else
	        		{
	    				//System.out.println("step 8");
	        			pointsArray2 = new ArrayList<Point3d>();
	        			pointsArray2.add(new Point3d(event.getPos().getX(), event.getPos().getY(), event.getPos().getZ()));	        		
	        		}
	        	}
	    	}
	    }
	
		/*@SubscribeEvent
		public void playerInteractsChest(PlayerInteractEvent event)
		{
			World world = event.getEntityPlayer().worldObj;
			if(event.useBlock != null && world.getBlockTileEntity(event.getPos().getX(), event.getPos().getY(), event.getPos().getZ()) instanceof TileEntityChest 
					&& event.action.equals(event.action.RIGHT_CLICK_BLOCK))
			{
				ArrayList<Point3d> arrayList = readData("Chests.txt");
				if(arrayList.size() == 0)
				{
					//System.out.println("Added a chest. EVENT. " + event.getPos().getX() + " " + event.getPos().getY() + " " + event.getPos().getZ());
	        		writeData(event.getPos().getX() + " " + event.getPos().getY() + " " + event.getPos().getZ(), "Chests.txt", true);
				}
				else
				{
					boolean allValuesTrue = true;
					for(int i = 0; i < arrayList.size(); i++)
					{
						if((arrayList.get(i).x == event.getPos().getX()) && (arrayList.get(i).y == event.getPos().getY()) && (arrayList.get(i).z == event.getPos().getZ()))
						{
							allValuesTrue = false;
						}
					}
					if(allValuesTrue)
					{
						//System.out.println("Added a chest. EVENT. " + event.getPos().getX() + " " + event.getPos().getY() + " " + event.getPos().getZ());
		        		writeData("\r\n" + event.getPos().getX() + " " + event.getPos().getY() + " " + event.getPos().getZ(), "Chests.txt", true);
					}
				}
			}
		}
	
		@SubscribeEvent
		public void playerAcquiresItemEvent(EntityItemPickupEvent event)
		{
			EntityPlayer sender = event.getEntityPlayer();
			String dateStamp = new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime());
    		String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
    		String finalString = "Player " + sender.username + " picked up " + event.item.getEntityName() + " on " + dateStamp + " at " + timeStamp + ".\r\n";
			writeData(finalString, "itemslog.txt", true);
		}
		
		@SubscribeEvent
		public void playerThrowsItemEvent(ItemTossEvent event)
		{
			EntityPlayer sender = event.player;
    		String dateStamp = new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime());
    		String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
    		String finalString = "Player " + sender.username + " threw item " + event.entityItem.getEntityName() + " on " + dateStamp + " at " + timeStamp + ".\r\n";
			writeData(finalString, "playerlog.txt", true);
		}
		
		@SubscribeEvent
		public void playerDies(LivingDropsEvent event)
		{
			if(event.entityLiving instanceof EntityPlayer)
			{
				EntityPlayer sender = (EntityPlayer) event.entityLiving;
	    		String dateStamp = new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime());
	    		String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
	    		String finalString = "Player " + sender.username + " died on " + dateStamp + " at " + timeStamp + " from " + event.source + ".\r\n";
				writeData(finalString, "playerlog.txt", true);
			}
		}
		
		@SubscribeEvent
		public void itemExpires(ItemExpireEvent event)
		{
			EntityItem expiredItem = event.entityItem;
			String dateStamp = new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime());
    		String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
    		String finalString = "Item " + expiredItem.getEntityName() + " expired "+ " on " + dateStamp + " at " + timeStamp + ".\r\n";
			writeData(finalString, "itemslog.txt", true);
		}
		
        @SubscribeEvent
        public void playerPlaceBlock(PlayerInteractEvent event)
        {
        	if(event.action != null && event.action.equals(event.action.RIGHT_CLICK_BLOCK))
        	{
        		if ((event.getEntityPlayer().getHeldItem() != null && (event.getEntityPlayer().getHeldItem().getItemUseAction() != EnumAction.eat 
                		&& event.getEntityPlayer().getHeldItem().getItemUseAction() != EnumAction.drink))) 
        		{
        			String dateStamp = new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime());
            		String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
            		String finalString = "Player " + event.getEntityPlayer().getDisplayNameString() + " placed a block " + event.getEntityPlayer().getHeldItem().getDisplayNameString() + " on " + dateStamp + " at " + timeStamp + ".\r\n";
            		if(isNotCommon(event.getEntityPlayer().getHeldItem().itemID))
            		{
            			writeData(finalString, "blocksplaced.txt", true);
            		}
        		}
        	}
        }*/
        
/*        public static void playerDestroyBlock(PlayerInteractEvent event)
        {
        	String dateStamp = new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime());
            String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
            String finalString = "Player " + event.getEntityPlayer().getDisplayNameString() + " destroyed a block " + event.getEntityPlayer().getHeldItem().getDisplayNameString() + " on " + dateStamp + " at " + timeStamp + ".\r\n";
            if(isNotCommon(event.getEntityPlayer().getHeldItem().itemID))
            {
            	writeData(finalString, "blocksplaced.txt", true);
            }
        }*/
        		
		@SubscribeEvent
		public void commandFreeze(CommandEvent event) throws PlayerNotFoundException
		{			
        	if(event.getCommand() instanceof CommandFreeze)
        	{
        		ICommandSender sender = event.getSender();
        		if(LoMaS_Utils.isSenderAdmin(sender))
        		{
	        		String[] parameters = event.getParameters();
	                EntityPlayerMP entityplayermp = ((CommandBase) event.getCommand()).getCommandSenderAsPlayer(sender);
	                WorldServer server = entityplayermp.getServerWorld();
	                List usernames = new ArrayList();
	                for(int i = 0; i < server.playerEntities.size(); i++)
	                {
	                	EntityPlayerMP tempPlayer = (EntityPlayerMP) server.playerEntities.get(i);
	                	usernames.add(i, tempPlayer.getDisplayNameString());
	                }
	                
	        		if(parameters.length == 0)
	        		{
	        			this.sendChatToPlayer(sender, "Proper usage is /freeze <PlayerName> (Case Sensitive!)");
	        		}
	        		else if (usernames.contains(parameters[0]))
	        		{
	        		}
	        		else
	        		{
	        			this.sendChatToPlayer(sender, "'" + parameters[0] + "'" + " is not a valid player name. (Case Sensitive!)");
	        		}
        		}
        	}
		}
		
		@SubscribeEvent
		public void commandCheckInventory(CommandEvent event) throws PlayerNotFoundException
		{			
        	if(event.getCommand() instanceof CommandCheckInventory)
        	{
        		ICommandSender sender = event.getSender();
        		if(LoMaS_Utils.isSenderAdmin(sender))
        		{
	        		String[] parameters = event.getParameters();
	                EntityPlayerMP entityplayermp = ((CommandBase) event.getCommand()).getCommandSenderAsPlayer(sender);
	                WorldServer server = entityplayermp.getServerWorld();
	                List usernames = new ArrayList();
	                for(int i = 0; i < server.playerEntities.size(); i++)
	                {
	                	EntityPlayerMP tempPlayer = (EntityPlayerMP) server.playerEntities.get(i);
	                	usernames.add(i, tempPlayer.getDisplayNameString());
	                }
	                
	        		if(parameters.length == 0)
	        		{
	        			this.sendChatToPlayer(sender, "Proper usage is /checkinv <PlayerName> (Case Sensitive!)");
	        		}
	        		else if (usernames.contains(parameters[0]))
	        		{
	        			EntityPlayerMP target = (EntityPlayerMP) server.getPlayerEntityByName(parameters[0]);
	        			InventoryPlayer targetInventory = target.inventory;
	        			System.out.println("Starting scan of player " + target.getDisplayNameString() + ". Scan initiated by " + sender.getName() + ".");
	        			String finalString = "Starting scan of player " + target.getDisplayNameString() + ". Scan initiated by " + sender.getName() + ".\r\n";
	        			for(int j = 0; j < targetInventory.getSizeInventory(); j++)
	        			{
	        				if(targetInventory.getStackInSlot(j) != null)
	        				{
	        					finalString = finalString + "Name: " + targetInventory.getStackInSlot(j).getDisplayName() + " Amount: " + targetInventory.getStackInSlot(j).stackSize + "\r\n";
	            				System.out.println("Name: " + targetInventory.getStackInSlot(j).getDisplayName() + " Amount: " + targetInventory.getStackInSlot(j).stackSize);
	    	        			this.sendChatToPlayer(sender, "Name: " + targetInventory.getStackInSlot(j).getDisplayName() + " Amount: " + targetInventory.getStackInSlot(j).stackSize);
	        				}
	        			}
	        			writeData(finalString, "InventoryScan.txt", false);
	        		}
	        		else
	        		{
	        			this.sendChatToPlayer(sender, "'" + parameters[0] + "'" + " is not a valid player name. (Case Sensitive!)");
	        		}
        		}
        	}
		}
		
		@SubscribeEvent
		public void commandCheckChests(CommandEvent event) throws PlayerNotFoundException
		{			
        	if(event.getCommand() instanceof CommandCheckChests)
        	{
        		ICommandSender sender = event.getSender();
        		if(LoMaS_Utils.isSenderAdmin(sender))
        		{
	        		String[] parameters = event.getParameters();
	                EntityPlayerMP entityplayermp = ((CommandBase) event.getCommand()).getCommandSenderAsPlayer(sender);
	                WorldServer server = entityplayermp.getServerWorld();
	        		System.out.println("Starting scan of all chests." + " Scan initiated by " + sender.getName() + ".");
	        		String dateStamp = new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime());
	        		String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
	        		String finalString = "Starting scan of all chests." + " Scan initiated by " + sender.getName() + " on " + dateStamp + " at " + timeStamp + ".\r\n";
	        		ArrayList<Point3d> pointArray = readData("Chests.txt");
        			String checkChestString = "";
	        		for (int i = 0; i < pointArray.size(); i++)
	        		{
	        			TileEntity tileentity = server.getTileEntity(new BlockPos(pointArray.get(i).x, pointArray.get(i).y, pointArray.get(i).z));
	        			if(tileentity instanceof TileEntityChest)
	        			{
	        				checkChestString = checkChestString + pointArray.get(i).x + " " +  pointArray.get(i).y + " " +  pointArray.get(i).z + "\r\n";
	        				TileEntityChest chest = (TileEntityChest) tileentity;
    		        		finalString = finalString + "Scanning Chest at: X:" + pointArray.get(i).x + " Y:" +  pointArray.get(i).y + " Z:" +  pointArray.get(i).z + "\r\n";
	        				for(int j = 0; j < chest.getSizeInventory(); j++)
	        				{
	        					if(chest.getStackInSlot(j) != null)
	        					{
	        		        		finalString = finalString + "Name: " + chest.getStackInSlot(j).getDisplayName() + " Amount: " + chest.getStackInSlot(j).stackSize + "\r\n";
	        					}
	        				}
	        			}
	        		}
	        		writeData(finalString, "ChestScan.txt", true);
	        		writeData(checkChestString, "Chests.txt", false);
        		}
        	}
		}
		
		@SubscribeEvent
		public void commandLog(CommandEvent event)
		{			
			String congregateString = event.getSender().getName() + " tried to use " + event.getCommand().getCommandName();
			for(int i = 0; i < event.getParameters().length; i++)
			{
				congregateString = congregateString + " " + event.getParameters()[i];
			}
			/*if(event.getCommand().canCommandSenderUseCommand(event.getSender()))
			{
				congregateString = congregateString + " and succeeded.";
			}
			else
			{
				congregateString = congregateString + " and we think they failed.";
			}*/
			System.out.println(congregateString);
		}
		
		@SubscribeEvent
		public void commandKill(CommandEvent event) throws PlayerNotFoundException
		{			
        	if(event.getCommand() instanceof CommandKill)
        	{
        		ICommandSender sender = event.getSender();
        		if(LoMaS_Utils.isSenderAdmin(sender))
        		{
	        		String[] parameters = event.getParameters();
	                EntityPlayerMP entityplayermp = ((CommandBase) event.getCommand()).getCommandSenderAsPlayer(sender);
	                WorldServer server = entityplayermp.getServerWorld();
	                List usernames = new ArrayList();
	                for(int i = 0; i < server.playerEntities.size(); i++)
	                {
	                	EntityPlayerMP tempPlayer = (EntityPlayerMP) server.playerEntities.get(i);
	                	usernames.add(i, tempPlayer.getDisplayNameString());
	                }
	                
	        		if(parameters.length == 0)
	        		{
	        			event.setCanceled(true);
	        			this.sendChatToPlayer(sender, "Proper usage is /kill <PlayerName> (Case Sensitive!)");
	        		}
	        		else if (usernames.contains(parameters[0]))
	        		{
	        			event.setCanceled(true);
	        			EntityPlayerMP target = (EntityPlayerMP) server.getPlayerEntityByName(parameters[0]);
	        	        target.attackEntityFrom(DamageSource.outOfWorld, 1000);
	        	        //target.addChatMessage("Ouch. That looks like it hurt.");
	        		}
	        		else
	        		{
	        			event.setCanceled(true);
	        			this.sendChatToPlayer(sender, "'" + parameters[0] + "'" + " is not a valid player name. (Case Sensitive!)");
	        		}
        		}
        	}
		}
	
		@SubscribeEvent
        public void commandCreateShapeEvent(CommandEvent event) throws PlayerNotFoundException
        {
        	if(event.getCommand() instanceof CommandCreateShape) //Replace with new Command
        	{
	        	ICommandSender sender = event.getSender();
	        	String[] parameters = event.getParameters();	                
        		EntityPlayerMP entityplayermp = ((CommandBase) event.getCommand()).getCommandSenderAsPlayer(sender);
        		WorldServer server = entityplayermp.getServerWorld();
	        	if(LoMaS_Utils.isSenderAdmin(sender))
	        	{
	    	        if (parameters.length > 0)
	    	        {
			        	if(parameters[0].matches("help"))
			        	{
			        		if(server.isRemote == false)
			        		{
				        		this.sendChatToPlayer(sender, "Syntax includes:");
				        		this.sendChatToPlayer(sender, "/createshape rectangle lenx lenz height isHollow(true/false) blockID");
				        		this.sendChatToPlayer(sender, "/createshape sphere radius height isHollow(true/false) blockID");
				        		this.sendChatToPlayer(sender, "/createshape cylinder radius height isHollow(true/false) blockID");
				        		this.sendChatToPlayer(sender, "/createshape bowl radius height (UP/DOWN) isHollow(true/false) blockID");
			        		}
			        	}
			        	///////////////////////////////////////////////////////////////
			        	////////////////////////Start Rectangle////////////////////////
			        	///////////////////////////////////////////////////////////////
			        	if(parameters[0].matches("rectangle"))
			        	{
			        		int lenx = Integer.parseInt(parameters[1]);
			        		int lenz = Integer.parseInt(parameters[2]);
			        		int height = Integer.parseInt(parameters[3]);
			        		boolean isHollow = false;
			        		if(parameters[4].matches("yes") || parameters[4].matches("true") || parameters[4].matches("t")
			        				 || parameters[4].matches("y"))
			        		{
			        			isHollow = true;
			        		}
			        		int ID = Integer.parseInt(parameters[5]);
			        		if(editor1 == "" || editor1 == null)
			        		{
			        			ID1 = ID;
			        			lenx1 = lenx;
			        			lenz1 = lenz;
			        			height1 = height;
			        			isHollow1 = isHollow;
			        			editor1 = sender.getName();
			        			shapetype1 = "rectangle";
			        		}
			        		else if (editor2 == "" || editor2 == null)
			        		{
			        			ID2 = ID;
			        			lenx2 = lenx;
			        			lenz2 = lenz;
			        			height2 = height;
			        			isHollow2 = isHollow;
			        			editor2 = sender.getName();
			        			shapetype2 = "rectangle";
			        		}
			        		this.sendChatToPlayer(sender, "Data confirmed. Right click your desired corners. The last four clicked points will be used.");
			        	}
			        	///////////////////////////////////////////////////////////////
			        	////////////////////////Start Sphere///////////////////////////
			        	///////////////////////////////////////////////////////////////
			        	else if(parameters[0].matches("sphere"))
			        	{
			        		int radius = Integer.parseInt(parameters[1]);
			        		int height = Integer.parseInt(parameters[2]);
			        		boolean isHollow = false;
			        		if(parameters[3].matches("yes") || parameters[3].matches("true") || parameters[3].matches("t")
			        				 || parameters[3].matches("y"))
			        		{
			        			isHollow = true;
			        		}
			        		int ID = Integer.parseInt(parameters[4]);
			        		if(editor1 == "" || editor1 == null)
			        		{
			        			radius1 = radius;
			        			height1 = height;
			        			isHollow1 = isHollow;
			        			ID1 = ID;
			        			shapetype1 = "sphere";
			        			editor1 = sender.getName();
			        		}
			        		else if (editor2 == "" || editor2 == null)
			        		{
			        			radius2 = radius;
			        			height2 = height;
			        			isHollow2 = isHollow;
			        			ID2 = ID;
			        			shapetype2 = "sphere";
			        			editor2 = sender.getName();
			        		}
			        		this.sendChatToPlayer(sender, "Data confirmed. Right click your desired center. The last clicked block will be used.");
			        	}
			        	///////////////////////////////////////////////////////////////
			        	//////////////////////////Start Cylinder///////////////////////
			        	///////////////////////////////////////////////////////////////
			        	else if(parameters[0].matches("cylinder"))
			        	{
			        		int radius = Integer.parseInt(parameters[1]);
			        		int height = Integer.parseInt(parameters[2]);
			        		boolean isHollow = false;
			        		if(parameters[3].matches("yes") || parameters[3].matches("true") || parameters[3].matches("t")
			        				 || parameters[3].matches("y"))
			        		{
			        			isHollow = true;
			        		}
			        		int ID = Integer.parseInt(parameters[4]);
			        		if(editor1 == "" || editor1 == null)
			        		{
			        			radius1 = radius;
			        			height1 = height;
			        			isHollow1 = isHollow;
			        			ID1 = ID;
			        			shapetype1 = "cylinder";
			        			editor1 = sender.getName();
			        		}
			        		else if (editor2 == "" || editor2 == null)
			        		{
			        			radius2 = radius;
			        			height2 = height;
			        			isHollow2 = isHollow;
			        			ID2 = ID;
			        			shapetype2 = "cylinder";
			        			editor2 = sender.getName();
			        		}
			        		this.sendChatToPlayer(sender, "Data confirmed. Right click your desired center. The last clicked block will be used.");
			        	}
			        	///////////////////////////////////////////////////////////////
			        	//////////////////////////Start Bowl///////////////////////////
			        	///////////////////////////////////////////////////////////////
			        	else if(parameters[0].matches("bowl"))
			        	{
			        		int radius = Integer.parseInt(parameters[1]);
			        		int height = Integer.parseInt(parameters[2]);
			        		boolean isConcaveUp = true;
			        		boolean isHollow = false;
			        		if(parameters[3].matches("no") || parameters[3].matches("false") || parameters[3].matches("f")
			        				 || parameters[3].matches("n") || parameters[3].matches("down"))
			        		{
			        			isConcaveUp = false;
			        		}
			        		if(parameters[4].matches("yes") || parameters[4].matches("true") || parameters[4].matches("t")
			        				 || parameters[4].matches("y"))
			        		{
			        			isHollow = true;
			        		}
			        		int ID = Integer.parseInt(parameters[5]);
			        		if(editor1 == "" || editor1 == null)
			        		{
			        			radius1 = radius;
			        			height1 = height;
			        			isConcaveUp1 = isConcaveUp;
			        			isHollow1 = isHollow;
			        			ID1 = ID;
			        			shapetype1 = "bowl";
			        			editor1 = sender.getName();
			        		}
			        		else if (editor2 == "" || editor2 == null)
			        		{
			        			radius2 = radius;
			        			height2 = height;
			        			isConcaveUp2 = isConcaveUp;
			        			isHollow2 = isHollow;
			        			ID2 = ID;
			        			shapetype2 = "bowl";
			        			editor2 = sender.getName();
			        		}
			        		this.sendChatToPlayer(sender, "Data confirmed. Right click your desired center. The last clicked block will be used.");
			        	}
			        	else if(parameters[0].matches("done") || parameters[0].matches("finish"))
			        	{	                
			        		entityplayermp = ((CommandBase) event.getCommand()).getCommandSenderAsPlayer(sender);
			        		server = entityplayermp.getServerWorld();
			        		if((editor1 != null && editor1.matches(sender.getName()) && shapetype1.matches("rectangle")))
			        		{
			        			int startX = (int) entityplayermp.posX;
			        			int startY = (int) entityplayermp.posY;
			        			int startZ = (int) entityplayermp.posZ;
			        			int h = height1;
			        			int lenX = lenx1;
			        			int lenZ = lenz1;
			        			if(isHollow1)
			        			{
			        				for(int j = startY; j < startY + h; j++)
			        				{
			        					int Y = j;
			        					for(int i = startX; i < startX + lenX; i++)
			        					{ 
				        					LoMaS_Utils.setBlock(server, i, Y, startZ, Block.getBlockById(ID1));
				        					LoMaS_Utils.setBlock(server, i, Y, startZ+lenZ, Block.getBlockById(ID1));
				        					if(i == startX || i == (startX + lenX-1))
				        					{ 
					        					for(int k = startZ; k < startZ + lenZ; k++)
					        					{ 
					        						LoMaS_Utils.setBlock(server, i, j, k, Block.getBlockById(ID1));
					        					}
				        					}
				        				}
			        				}
			                        this.sendChatToPlayer(sender, "Created a hollow rectangular prism with starting corner: " + startX + " " + startY + " " + startZ);
			        			}
			        			else
			        			{
			        				for(int j = startY; j < startY + h; j++)
			        				{
			        					int Y = j;
			        					for(int i = startX; i < startX + lenX; i++)
			        					{ 
				        					LoMaS_Utils.setBlock(server, i, Y, startZ, Block.getBlockById(ID1));
				        					LoMaS_Utils.setBlock(server, i, Y, startZ+lenZ, Block.getBlockById(ID1));
					        				for(int k = startZ; k < startZ + lenZ; k++)
					        				{ 
					        					LoMaS_Utils.setBlock(server, i, j, k, Block.getBlockById(ID1));
					        				}
				        				}
			        				}
			                        this.sendChatToPlayer(sender, "Created a solid rectangular prism with starting corner: " + startX + " " + startY + " " + startZ);
			        			}
			        		}
			        		else if((editor2 != null && editor2.matches(sender.getName()) &&  shapetype2.matches("rectangle")))
			        		{
			        			int startX = (int) entityplayermp.posX;
			        			int startY = (int) entityplayermp.posY;
			        			int startZ = (int) entityplayermp.posZ;
			        			int h = height2;
			        			int lenX = lenx2;
			        			int lenZ = lenz2;
			        			if(isHollow2)
			        			{
			        				for(int j = startY; j < startY + h; j++)
			        				{
			        					int Y = j;
			        					for(int i = startX; i < startX + lenX; i++)
			        					{ 
				        					LoMaS_Utils.setBlock(server, i, Y, startZ, Block.getBlockById(ID2));
				        					LoMaS_Utils.setBlock(server, i, Y, startZ+lenZ, Block.getBlockById(ID2));
				        					if(i == startX || i == (startX + lenX-1))
				        					{ 
					        					for(int k = startZ; k < startZ + lenZ; k++)
					        					{ 
					        						LoMaS_Utils.setBlock(server, i, j, k, Block.getBlockById(ID2));
					        					}
				        					}
				        				}
			        				}
			                        this.sendChatToPlayer(sender, "Created a hollow rectangular prism with starting corner: " + startX + " " + startY + " " + startZ);
			        			}
			        			else
			        			{
			        				for(int j = startY; j < startY + h; j++)
			        				{
			        					int Y = j;
			        					for(int i = startX; i < startX + lenX; i++)
			        					{ 
				        					LoMaS_Utils.setBlock(server, i, Y, startZ, Block.getBlockById(ID2));
				        					LoMaS_Utils.setBlock(server, i, Y, startZ+lenZ, Block.getBlockById(ID2));
					        				for(int k = startZ; k < startZ + lenZ; k++)
					        				{ 
					        					LoMaS_Utils.setBlock(server, i, j, k, Block.getBlockById(ID2));
					        				}
				        				}
			        				}
			                        this.sendChatToPlayer(sender, "Created a solid rectangular prism with starting corner: " + startX + " " + startY + " " + startZ);
			        			}
			        		}
			        		////////////////////////////////////////////////////
			        		else if((editor1 != null && editor1.matches(sender.getName()) &&  shapetype1.matches("cylinder")))
			        		{
			        			if(isHollow1)
			        			{
			        				//2pir^2 + 2pirh
			        				Point3d center = new Point3d(0, -10000, 0);
			        				try {
										center = pointsArray1.get(pointsArray1.size()-1);
									} catch (Exception e) {
										this.sendChatToPlayer(sender, "No points selected! You need to right click a block before finishing to create a shape.");
										e.printStackTrace();
									}
			                        for(int j = 0; j < height1; j++)
			                        {
			                            for(int i = 0; i < 720; i++)
			                            {
			                                double lol = (double)i * 0.5D;
			                                float temp[] = PointOnCircle(radius1, (float)lol, center.x, center.z);
			                                LoMaS_Utils.setBlock(server, (int)temp[0], center.y + j, (int)temp[1], Block.getBlockById(ID1));
			                            }
			                        }
			                        this.sendChatToPlayer(sender, "Created a hollow cylinder with starting center: " + center.x + " " + center.y + " " + center.z);
			        			}
			        			else
			        			{
			        				Point3d center = new Point3d(0, -10000, 0);
			        				try {
										center = pointsArray1.get(pointsArray1.size()-1);
									} catch (Exception e) {
										this.sendChatToPlayer(sender, "No points selected! You need to right click a block before finishing to create a shape.");
										e.printStackTrace();
									}
			        				int size = radius1;
			        				do
			        				{
			        			       for (int j = 0; j < height1; j++) 
			        			       {
			        			          for (int i = 0; i < 720; i++) 
			        			          {
			        			            double lol = i * 0.5D;
			        			            float[] temp = PointOnCircle(size, (float)lol, center.x, center.z);
			        			            LoMaS_Utils.setBlock(server, (int)temp[0], center.y + j, (int)temp[1], Block.getBlockById(ID1));
			        			          }
			        			       }
			        			       size = size - 1;
			        				}
				        			while(size > -1); //so it processes zero
			                        this.sendChatToPlayer(sender, "Created a solid cylinder with starting center: " + center.x + " " + center.y + " " + center.z);
			        			}
			        		}
			        		else if((editor2 != null && editor2.matches(sender.getName()) &&  shapetype2.matches("cylinder")))
			        		{
			        			if(isHollow2)
			        			{

			        				//2pir^2 + 2pirh
			        				Point3d center = new Point3d(0, -10000, 0);
			        				try {
										center = pointsArray2.get(pointsArray2.size()-1);
									} catch (Exception e) {
										this.sendChatToPlayer(sender, "No points selected! You need to right click a block before finishing to create a shape.");
										e.printStackTrace();
									}
			                        for(int j = 0; j < height2; j++)
			                        {
			                            for(int i = 0; i < 720; i++)
			                            {
			                                double lol = (double)i * 0.5D;
			                                float temp[] = PointOnCircle(radius2, (float)lol, center.x, center.z);
			                                LoMaS_Utils.setBlock(server, (int)temp[0], center.y + j, (int)temp[1], Block.getBlockById(ID2));
			                            }
			                        }
			                        this.sendChatToPlayer(sender, "Created a hollow cylinder with starting center: " + center.x + " " + center.y + " " + center.z);
			        			}
			        			else
			        			{
			        				Point3d center = new Point3d(0, -10000, 0);
			        				try {
										center = pointsArray2.get(pointsArray1.size()-1);
									} catch (Exception e) {
										this.sendChatToPlayer(sender, "No points selected! You need to right click a block before finishing to create a shape.");
										e.printStackTrace();
									}
			        				int size = radius2;
			        				do
			        				{
			        			       for (int j = 0; j < height2; j++) 
			        			       {
			        			          for (int i = 0; i < 720; i++) 
			        			          {
			        			            double lol = i * 0.5D;
			        			            float[] temp = PointOnCircle(size, (float)lol, center.x, center.z);
			        			            LoMaS_Utils.setBlock(server, (int)temp[0], center.y + j, (int)temp[1], Block.getBlockById(ID2));
			        			          }
			        			       }
			        			       size = size - 1;
			        				}
				        			while(size > -1); //so it processes zero
			                        this.sendChatToPlayer(sender, "Created a solid cylinder with starting center: " + center.x + " " + center.y + " " + center.z);
			        			}
			        		}
			        		////////////////////////////////////////////////////
			        		else if((editor1 != null && editor1.matches(sender.getName()) &&  shapetype1.matches("sphere")))
			        		{
			        			if(isHollow1)
			        			{
			        				int r = radius1;
			        				int h = height1;
			        				if(r%2 != 0) r++;
			        				Point3d center = new Point3d(0, -10000, 0);
			        				try {
										center = pointsArray1.get(pointsArray1.size()-1);
									} catch (Exception e) {
										this.sendChatToPlayer(sender, "No points selected! You need to right click a block before finishing to create a shape.");
										e.printStackTrace();
									}		

	        						for( int y = center.y-(h); y<= center.y+(h); y++)
	        						{
			        					for( int x = (center.x-r); x<= (center.x+r); x++)
			        					{
			        						for( int z = (center.z-r); z<= (center.z+r); z++)
			        						{
			        							int dx = x-center.x;
			        							int dy = y-center.y;
			        							int dz = z-center.z;
			        							double omnomnom = (((Math.pow(dx, 2) + Math.pow(dz,2))/Math.pow(r, 2)) + (Math.pow(dy,2)/Math.pow(h,2)));
			        							if(omnomnom < 1 && omnomnom > .5)
			        							{
			        								LoMaS_Utils.setBlock(server, x, y, z, Block.getBlockById(ID1));
			        							}
			        						}
			        					}
			        				}
			                        this.sendChatToPlayer(sender, "Created a hollow spheroid with starting center: " + center.x + " " + center.y + " " + center.z);
			        			}
			        			else
			        			{
			        				int r = radius1;
			        				int h = height1;
			        				if(r%2 != 0) r++;
			        				Point3d center = new Point3d(0, -10000, 0);
			        				try {
										center = pointsArray1.get(pointsArray1.size()-1);
									} catch (Exception e) {
										this.sendChatToPlayer(sender, "No points selected! You need to right click a block before finishing to create a shape.");
										e.printStackTrace();
									}		

	        						for( int y = center.y-(h); y<= center.y+(h); y++)
	        						{
			        					for( int x = (center.x-r); x<= (center.x+r); x++)
			        					{
			        						for( int z = (center.z-r); z<= (center.z+r); z++)
			        						{
			        							int dx = x-center.x;
			        							int dy = y-center.y;
			        							int dz = z-center.z;
			        							double omnomnom = (((Math.pow(dx, 2) + Math.pow(dz,2))/Math.pow(r, 2)) + (Math.pow(dy,2)/Math.pow(h,2)));
			        							if(omnomnom < 1)
			        							{
			        								LoMaS_Utils.setBlock(server, x, y, z, Block.getBlockById(ID1));
			        							}
			        						}
			        					}
			        				}
			                        this.sendChatToPlayer(sender, "Created a solid spheroid with starting center: " + center.x + " " + center.y + " " + center.z);
			        			}
			        		}
			        		else if((editor2 != null && editor2.matches(sender.getName()) &&  shapetype2.matches("sphere")))
			        		{
			        			if(isHollow2)
			        			{
			        				int r = radius2;
			        				int h = height2;
			        				if(r%2 != 0) r++;
			        				Point3d center = new Point3d(0, -10000, 0);
			        				try {
										center = pointsArray2.get(pointsArray2.size()-1);
									} catch (Exception e) {
										this.sendChatToPlayer(sender, "No points selected! You need to right click a block before finishing to create a shape.");
										e.printStackTrace();
									}		

	        						for( int y = center.y-(h); y<= center.y+(h); y++)
	        						{
			        					for( int x = (center.x-r); x<= (center.x+r); x++)
			        					{
			        						for( int z = (center.z-r); z<= (center.z+r); z++)
			        						{
			        							int dx = x-center.x;
			        							int dy = y-center.y;
			        							int dz = z-center.z;
			        							double omnomnom = (((Math.pow(dx, 2) + Math.pow(dz,2))/Math.pow(r, 2)) + (Math.pow(dy,2)/Math.pow(h,2)));
			        							if(omnomnom < 1 && omnomnom > .5)
			        							{
			        								LoMaS_Utils.setBlock(server, x, y, z, Block.getBlockById(ID2));
			        							}
			        						}
			        					}
			        				}
			                        this.sendChatToPlayer(sender, "Created a hollow spheroid with starting center: " + center.x + " " + center.y + " " + center.z);
			        			}
			        			else
			        			{
			        				int r = radius2;
			        				int h = height2;
			        				if(r%2 != 0) r++;
			        				Point3d center = new Point3d(0, -10000, 0);
			        				try {
										center = pointsArray2.get(pointsArray2.size()-1);
									} catch (Exception e) {
										this.sendChatToPlayer(sender, "No points selected! You need to right click a block before finishing to create a shape.");
										e.printStackTrace();
									}		

	        						for( int y = center.y-(h); y<= center.y+(h); y++)
	        						{
			        					for( int x = (center.x-r); x<= (center.x+r); x++)
			        					{
			        						for( int z = (center.z-r); z<= (center.z+r); z++)
			        						{
			        							int dx = x-center.x;
			        							int dy = y-center.y;
			        							int dz = z-center.z;
			        							double omnomnom = (((Math.pow(dx, 2) + Math.pow(dz,2))/Math.pow(r, 2)) + (Math.pow(dy,2)/Math.pow(h,2)));
			        							if(omnomnom < 1)
			        							{
			        								LoMaS_Utils.setBlock(server, x, y, z, Block.getBlockById(ID2));
			        							}
			        						}
			        					}
			        				}
			                        this.sendChatToPlayer(sender, "Created a solid spheroid with starting center: " + center.x + " " + center.y + " " + center.z);
			        			}
			        		}
			        		////////////////////////////////////////////////////
			        		else if((editor1 != null && editor1.matches(sender.getName()) &&  shapetype1.matches("bowl")))
			        		{
			        			if(isHollow1)
			        			{
			        				int r = radius1;
			        				int h = height1;
			        				if(r%2 != 0) r++;
			        				Point3d center = new Point3d(0, -10000, 0);
			        				try {
										center = pointsArray1.get(pointsArray1.size()-1);
									} catch (Exception e) {
										this.sendChatToPlayer(sender, "No points selected! You need to right click a block before finishing to create a shape.");
										e.printStackTrace();
									}		
			        				if(isConcaveUp1)
			        				{
		        						for(int y = center.y-h; y <= center.y; y++)
		        						{
				        					for(int x = (center.x-r); x<= (center.x+r); x++)
				        					{
				        						for(int z = (center.z-r); z<= (center.z+r); z++)
				        						{
				        							int dx = x-center.x;
				        							int dy = y-center.y;
				        							int dz = z-center.z;
				        							double omnomnom = (((Math.pow(dx, 2) + Math.pow(dz,2))/Math.pow(r, 2)) + (Math.pow(dy,2)/Math.pow(h,2)));
				        							if(omnomnom < 1 && omnomnom > .5)
				        							{
				        								LoMaS_Utils.setBlock(server, x, y, z, Block.getBlockById(ID1));
				        							}
				        						}
				        					}
				        				}
			        				}
			        				else
			        				{
		        						for(int y = center.y; y <= center.y+h; y++)
		        						{
				        					for(int x = (center.x-r); x<= (center.x+r); x++)
				        					{
				        						for(int z = (center.z-r); z<= (center.z+r); z++)
				        						{
				        							int dx = x-center.x;
				        							int dy = y-center.y;
				        							int dz = z-center.z;
				        							double omnomnom = (((Math.pow(dx, 2) + Math.pow(dz,2))/Math.pow(r, 2)) + (Math.pow(dy,2)/Math.pow(h,2)));
				        							if(omnomnom < 1 && omnomnom > .5)
				        							{
				        								LoMaS_Utils.setBlock(server, x, y, z, Block.getBlockById(ID1));
				        							}
				        						}
				        					}
				        				}
			        				}
			                        this.sendChatToPlayer(sender, "Created a hollow bowl with starting center: " + center.x + " " + center.y + " " + center.z);
			        			}
			        			else
			        			{
			        				int r = radius1;
			        				int h = height1;
			        				if(r%2 != 0) r++;
			        				Point3d center = new Point3d(0, -10000, 0);
			        				try {
										center = pointsArray1.get(pointsArray1.size()-1);
									} catch (Exception e) {
										this.sendChatToPlayer(sender, "No points selected! You need to right click a block before finishing to create a shape.");
										e.printStackTrace();
									}		
			        				if(isConcaveUp1)
			        				{
		        						for(int y = center.y-h; y <= center.y; y++)
		        						{
				        					for(int x = (center.x-r); x<= (center.x+r); x++)
				        					{
				        						for(int z = (center.z-r); z<= (center.z+r); z++)
				        						{
				        							int dx = x-center.x;
				        							int dy = y-center.y;
				        							int dz = z-center.z;
				        							double omnomnom = (((Math.pow(dx, 2) + Math.pow(dz,2))/Math.pow(r, 2)) + (Math.pow(dy,2)/Math.pow(h,2)));
				        							if(omnomnom < 1)
				        							{
				        								LoMaS_Utils.setBlock(server, x, y, z, Block.getBlockById(ID1));
				        							}
				        						}
				        					}
				        				}
			        				}
			        				else
			        				{
		        						for(int y = center.y; y <= center.y+h; y++)
		        						{
				        					for(int x = (center.x-r); x<= (center.x+r); x++)
				        					{
				        						for(int z = (center.z-r); z<= (center.z+r); z++)
				        						{
				        							int dx = x-center.x;
				        							int dy = y-center.y;
				        							int dz = z-center.z;
				        							double omnomnom = (((Math.pow(dx, 2) + Math.pow(dz,2))/Math.pow(r, 2)) + (Math.pow(dy,2)/Math.pow(h,2)));
				        							if(omnomnom < 1)
				        							{
				        								LoMaS_Utils.setBlock(server, x, y, z, Block.getBlockById(ID1));
				        							}
				        						}
				        					}
				        				}
			        				}
			                        this.sendChatToPlayer(sender, "Created a solid bowl with starting center: " + center.x + " " + center.y + " " + center.z);
			        			}
			        		}
			        		else if((editor2 != null && editor2.matches(sender.getName()) &&  shapetype2.matches("bowl")))
			        		{
			        			if(isHollow1)
			        			{
			        				int r = radius2;
			        				int h = height2;
			        				if(r%2 != 0) r++;
			        				Point3d center = new Point3d(0, -10000, 0);
			        				try {
										center = pointsArray2.get(pointsArray2.size()-1);
									} catch (Exception e) {
										this.sendChatToPlayer(sender, "No points selected! You need to right click a block before finishing to create a shape.");
										e.printStackTrace();
									}		
			        				if(isConcaveUp2)
			        				{
		        						for(int y = center.y-h; y <= center.y; y++)
		        						{
				        					for(int x = (center.x-r); x<= (center.x+r); x++)
				        					{
				        						for(int z = (center.z-r); z<= (center.z+r); z++)
				        						{
				        							int dx = x-center.x;
				        							int dy = y-center.y;
				        							int dz = z-center.z;
				        							double omnomnom = (((Math.pow(dx, 2) + Math.pow(dz,2))/Math.pow(r, 2)) + (Math.pow(dy,2)/Math.pow(h,2)));
				        							if(omnomnom < 1 && omnomnom > .5)
				        							{
				        								LoMaS_Utils.setBlock(server, x, y, z, Block.getBlockById(ID2));
				        							}
				        						}
				        					}
				        				}
			        				}
			        				else
			        				{
		        						for(int y = center.y; y <= center.y+h; y++)
		        						{
				        					for(int x = (center.x-r); x<= (center.x+r); x++)
				        					{
				        						for(int z = (center.z-r); z<= (center.z+r); z++)
				        						{
				        							int dx = x-center.x;
				        							int dy = y-center.y;
				        							int dz = z-center.z;
				        							double omnomnom = (((Math.pow(dx, 2) + Math.pow(dz,2))/Math.pow(r, 2)) + (Math.pow(dy,2)/Math.pow(h,2)));
				        							if(omnomnom < 1 && omnomnom > .5)
				        							{
				        								LoMaS_Utils.setBlock(server, x, y, z, Block.getBlockById(ID2));
				        							}
				        						}
				        					}
				        				}
			        				}
			                        this.sendChatToPlayer(sender, "Created a hollow bowl with starting center: " + center.x + " " + center.y + " " + center.z);
			        			}
			        			else
			        			{
			        				int r = radius2;
			        				int h = height2;
			        				if(r%2 != 0) r++;
			        				Point3d center = new Point3d(0, -10000, 0);
			        				try {
										center = pointsArray2.get(pointsArray2.size()-1);
									} catch (Exception e) {
										this.sendChatToPlayer(sender, "No points selected! You need to right click a block before finishing to create a shape.");
										e.printStackTrace();
									}		
			        				if(isConcaveUp2)
			        				{
		        						for(int y = center.y-h; y <= center.y; y++)
		        						{
				        					for(int x = (center.x-r); x<= (center.x+r); x++)
				        					{
				        						for(int z = (center.z-r); z<= (center.z+r); z++)
				        						{
				        							int dx = x-center.x;
				        							int dy = y-center.y;
				        							int dz = z-center.z;
				        							double omnomnom = (((Math.pow(dx, 2) + Math.pow(dz,2))/Math.pow(r, 2)) + (Math.pow(dy,2)/Math.pow(h,2)));
				        							if(omnomnom < 1)
				        							{
				        								LoMaS_Utils.setBlock(server, x, y, z, Block.getBlockById(ID2));
				        							}
				        						}
				        					}
				        				}
			        				}
			        				else
			        				{
		        						for(int y = center.y; y <= center.y+h; y++)
		        						{
				        					for(int x = (center.x-r); x<= (center.x+r); x++)
				        					{
				        						for(int z = (center.z-r); z<= (center.z+r); z++)
				        						{
				        							int dx = x-center.x;
				        							int dy = y-center.y;
				        							int dz = z-center.z;
				        							double omnomnom = (((Math.pow(dx, 2) + Math.pow(dz,2))/Math.pow(r, 2)) + (Math.pow(dy,2)/Math.pow(h,2)));
				        							if(omnomnom < 1)
				        							{
				        								LoMaS_Utils.setBlock(server, x, y, z, Block.getBlockById(ID2));
				        							}
				        						}
				        					}
				        				}
			        				}
			                        this.sendChatToPlayer(sender, "Created a solid bowl with starting center: " + center.x + " " + center.y + " " + center.z);
			        			}
			        		}
			        		////////////////////////////////////////////////////
			        		if((editor1 != null && editor1.matches(sender.getName())))
			        		{
			        			radius1 = 0;
			        			height1 = 0;
			        			isConcaveUp1 = true;
			        			isHollow1 = false;
			        			ID1 = 0;
			        			shapetype1 = "";
			        			editor1 = "";
			        			pointsArray1 = null;
			        			oldx = 0;
			        			oldy = 0;
			        			oldz = 0;
			        		}
			        		else if((editor2 != null && editor2.matches(sender.getName())))
			        		{
			        			radius2 = 0;
			        			height2 = 0;
			        			isConcaveUp2 = true;
			        			isHollow2 = false;
			        			ID2 = 0;
			        			shapetype2 = "";
			        			editor2 = "";
			        			pointsArray2 = null;
			        			oldx = 0;
			        			oldy = 0;
			        			oldz = 0;
			        		}
			        	}
	    	        }
	    	        else
	    	        {
		        		this.sendChatToPlayer(sender, "Incorrect syntax. Type '/createshape help' for help");
	    	        }
	        	}
	        	else
	        	{
	        		this.sendChatToPlayer(sender, "[LoMaS] You do not have permission to use this command.");
	        	}
        	}
        }
        
        @SubscribeEvent
        public void commandACSEvent(CommandEvent event) throws PlayerNotFoundException
        {
        	if(event.getCommand() instanceof CommandACS) //Replace with new Command
        	{
	        	ICommandSender sender = event.getSender();
	        	String[] parameters = event.getParameters();
                EntityPlayerMP entityplayermp = ((CommandBase) event.getCommand()).getCommandSenderAsPlayer(sender);
                WorldServer server = entityplayermp.getServerWorld();
	        	if(LoMaS_Utils.isSenderAdmin(sender))
	        	{
	    	        if (parameters.length > 0)
	    	        {
			        	if(parameters[0].matches("help"))
			        	{
			        		if(server.isRemote == false && 
					        		(LoMaS_Utils.isSenderAdmin(sender)))
			        		{
				        		this.sendChatToPlayer(sender, "Syntax includes:");
				        		this.sendChatToPlayer(sender, "/acs day");
				        		this.sendChatToPlayer(sender, "/acs dry");
				        		this.sendChatToPlayer(sender, "/acs setSpawn <x> <y> <z>");
				        		this.sendChatToPlayer(sender, "/acs setMotD <Message>");
				        		this.sendChatToPlayer(sender, "/acs addMotD <Message>");
				        		this.sendChatToPlayer(sender, "/checkchests");
				        		this.sendChatToPlayer(sender, "/checkinv <name>");
			        		}
			        		else if(server.isRemote == false)
			        		{
				        		this.sendChatToPlayer(sender, "Syntax includes:");
				        		this.sendChatToPlayer(sender, "/acs viewMotD");
			        		}
			        	}
			        	if(parameters[0].matches("dry") && 
				        		(LoMaS_Utils.isSenderAdmin(sender)))
			        	{
			                 WorldInfo worldinfo = server.getWorldInfo();
			                 worldinfo.setRaining(false);
			                 worldinfo.setThundering(false);
			        	}
/*			        	if(parameters[0].matches("wet"))
			        	{
			                 WorldServer worldserver = MinecraftServer.getServer().worldServers[0];
			                 WorldInfo worldinfo = worldserver.getWorldInfo();
			                 worldinfo.setRaining(true);
			                 worldinfo.setThundering(true);
			        	}*/
			        	if(parameters[0].matches("day") && 
				        		(LoMaS_Utils.isSenderAdmin(sender)))
			        	{
			        		server.setWorldTime((long)50);
			        	}
			        	/*if(parameters[0].matches("night"))
			        	{
			                for (int j = 0; j < MinecraftServer.getServer().worldServers.length; ++j)
			                {
			                    MinecraftServer.getServer().worldServers[j].setWorldTime((long)15000);
			                }
			        	}*/
			        	if(parameters[0].matches("setSpawn") && 
			        		(LoMaS_Utils.isSenderAdmin(sender)))
			        	{
			        		if(parameters[1] != null && parameters[2] != null && parameters[3] != null)
			        		{
			        			server.getWorldInfo().setSpawn(new BlockPos(Integer.parseInt(parameters[1]), Integer.parseInt(parameters[2]), Integer.parseInt(parameters[3])));
			        		}
			        		else
			        		{
			        			this.sendChatToPlayer(sender, "Incorrect syntax. Type /acs help for help.");
			        		}
			        	}
			        	if(parameters[0].matches("setMotD") && 
				        	(LoMaS_Utils.isSenderAdmin(sender)))
				        {
			        		String finalLine = "";
			        		for(int i = 1; i < parameters.length; i++)
			        		{
			        			finalLine = finalLine + " " + parameters[i];
			        		}
			        		this.writeData(finalLine, "motd.txt", false);
		    	        	this.sendChatToPlayer(sender, "Message of the day successfully set.");
				        }
			        	if(parameters[0].matches("addMotD") && 
					        	(LoMaS_Utils.isSenderAdmin(sender)))
				        {
			        		String finalLine = "";
			        		for(int i = 1; i < parameters.length; i++)
			        		{
			        			finalLine = finalLine + " " + parameters[i];
			        		}
			        		this.writeData(finalLine, "motd.txt", true);
		    	        	this.sendChatToPlayer(sender, "Message of the day successfully changed.");
				        }
			        	if(parameters[0].matches("viewMotD"))
			        	{
			        		ArrayList<String> motd = EventHookContainerClass.readTextData("motd.txt");
			        		for(int i = 0; i < motd.size(); i++)
			        		{
			        			this.sendChatToPlayer(sender, "\u00a75\u00a7l" + motd.get(i).substring(1, motd.get(i).length()));
			        		}
			        	}
	    	        }
	        	}
	        	else
	        	{
	        		this.sendChatToPlayer(sender, "[LoMaS] You do not have permission to use this command.");
	        	}
        	}
        }

        public void sendChatToPlayer(ICommandSender player, String message)
        {
        	TextComponentTranslation mess = new TextComponentTranslation(message);
        	player.addChatMessage(mess);
        }
        
        public void sendChatToPlayer(EntityPlayer player, String message)
        {
        	TextComponentTranslation mess = new TextComponentTranslation(message);
        	player.addChatMessage(mess);
        }
        
    	public static void writeData(String object, String file, Boolean append) 
    	{
    		PrintWriter out = null;
    		try {
				out = new PrintWriter(new BufferedWriter(new FileWriter(file, append)));
			} catch (IOException e) {
				e.printStackTrace();
			}
        	out.println(object);
    		out.close();
    	}
    	
    	public static ArrayList<String> readTextData(String file)
    	{
    		Scanner sc = null;
			try 
			{
				sc = new Scanner(new File(file));
			} 
			catch (FileNotFoundException e) 
			{
				System.out.println("Could not find " + file);
			}
	    	ArrayList<String> lines = new ArrayList<String>();
	    	if(sc != null)
	    	{
		    	while (sc.hasNextLine()) 
		    	{
			    	lines.add(sc.nextLine());
		    	}
		    	sc.close();
	    	}
	    	return lines;
    	}
    	
    	public static ArrayList<Point3d> readData(String file)
    	{
    	    FileInputStream fstream = null;
			try {
				fstream = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				System.out.println("Couldn't find " + file + "!!!!");
				System.out.println("Couldn't find " + file + "!!!!");
				System.out.println("Couldn't find " + file + "!!!!");
				System.out.println("Couldn't find " + file + "!!!!");
				e.printStackTrace();
			}
    	    DataInputStream in = null;
			try {
				in = new DataInputStream(fstream);
			} catch (Exception e1) {
				System.out.println("Failed in");
				e1.printStackTrace();
			}
    	    BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(in));
			} catch (Exception e) {
				System.out.println("Failed br");
				e.printStackTrace();
			}
    	    String output = "";
    	    ArrayList<Point3d> arrayList = new ArrayList(); 
    	    try {
				while (((output = br.readLine()) != null))// && (!((output = br.readLine()).equals(""))))
				{
					String[] tmp = output.split(" ");
					ArrayList<Integer> coordList = new ArrayList();
					for(String s: tmp)
						if(!s.equals(""))
						{
							coordList.add(Integer.parseInt(s));
						}
					if(coordList.size() == 3)
					{
						Point3d point = new Point3d(coordList.get(0), coordList.get(1), coordList.get(2));
						arrayList.add(point);
					}
				}
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
    	    return arrayList;
    	}
    	
        /*public static boolean isNotCommon(int itemID)
        {
        	if((Item.itemsList[itemID].itemID != Block.grass.blockID && Item.itemsList[itemID].itemID != Block.gravel.blockID 
        		&& Item.itemsList[itemID].itemID != Block.stone.blockID && Item.itemsList[itemID].itemID != Block.dirt.blockID
        		&& Item.itemsList[itemID].itemID != Block.sand.blockID && Item.itemsList[itemID].itemID != Block.sandStone.blockID
        		&& Item.itemsList[itemID].itemID != Item.flintAndSteel.itemID))
        	{
        		return true;
        	}
        	else
        	{
        		return false;
        	}
        }*/
        
        public static float[] PointOnCircle(float radius, float angleInDegrees, int x, int y)
        {
            float Xa = (float)((double)radius * Math.cos(((double)angleInDegrees * 3.1415926535897931D) / 180D)) + (float)x;
            float Ya = (float)((double)radius * Math.sin(((double)angleInDegrees * 3.1415926535897931D) / 180D)) + (float)y;
            float XY[] = new float[2];
            XY[0] = Xa;
            XY[1] = Ya;
            return XY;
        }
        
        public static double getDistance(int x1, int y1, int z1, int x2, int y2, int z2)
        {
        	int dx = x1-x2;
        	int dy = y1-y2;
        	int dz = z1-z2;
        	return Math.sqrt( (dx*dx + dy*dy + dz*dz ) );
        }
}