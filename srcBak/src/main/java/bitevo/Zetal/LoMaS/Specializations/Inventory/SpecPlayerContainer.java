package bitevo.Zetal.LoMaS.Specializations.Inventory;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Economy.Inventory.SlotCoin;
import bitevo.Zetal.LoMaS.Economy.Inventory.SlotCustom;
import bitevo.Zetal.LoMaS.Economy.Item.ItemCoin;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryCraftResult;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotCrafting;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class SpecPlayerContainer extends Container
{    
    private static final EntityEquipmentSlot[] VALID_EQUIPMENT_SLOTS = new EntityEquipmentSlot[] {EntityEquipmentSlot.HEAD, EntityEquipmentSlot.CHEST, EntityEquipmentSlot.LEGS, EntityEquipmentSlot.FEET};
    /** The crafting matrix inventory. */
    public InventoryCrafting craftMatrix = new InventoryCrafting(this, 2, 2);
    public IInventory craftResult = new InventoryCraftResult();
    /** Determines if inventory manipulation should be handled. */
    public boolean isLocalWorld;
    private final EntityPlayer thePlayer;

	public SpecPlayerContainer(final InventoryPlayer playerInventory, boolean localWorld, EntityPlayer player)
	{
		this.isLocalWorld = localWorld;
		this.thePlayer = player;
		this.addSlotToContainer(new SlotCrafting(playerInventory.player, this.craftMatrix, this.craftResult, 0, 142, 56));
		int i;
		int j;
		for (i = 0; i < 2; ++i)
		{
			for (j = 0; j < 2; ++j)
			{
				this.addSlotToContainer(new SlotCustom(this.craftMatrix, j + i * 2, 88 + j * 18, 46 + i * 18));
			}
		}

        for (int k = 0; k < 4; ++k)
        {
            final EntityEquipmentSlot entityequipmentslot = VALID_EQUIPMENT_SLOTS[k];
            this.addSlotToContainer(new Slot(playerInventory, 36 + (3 - k), 8, 8 + k * 18)
            {
                /**
                 * Returns the maximum stack size for a given slot (usually the same as getInventoryStackLimit(), but 1
                 * in the case of armor slots)
                 */
                public int getSlotStackLimit()
                {
                    return 1;
                }
                /**
                 * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
                 */
                public boolean isItemValid(@Nullable ItemStack stack)
                {
                    if (stack == null)
                    {
                        return false;
                    }
                    else
                    {
                        return stack.getItem().isValidArmor(stack, entityequipmentslot, thePlayer);
                    }
                }
                @Nullable
                @SideOnly(Side.CLIENT)
                public String getSlotTexture()
                {
                    return ItemArmor.EMPTY_SLOT_NAMES[entityequipmentslot.getIndex()];
                }
            });
        }

		int p;
		for (p = 0; p < 3; ++p)
		{
			for (j = 0; j < 9; ++j)
			{
				this.addSlotToContainer(new SlotCustom(playerInventory, j + (p + 1) * 9, 8 + j * 18, 84 + p * 18));
			}
		}

		for (p = 0; p < 9; ++p)
		{
			this.addSlotToContainer(new SlotCustom(playerInventory, p, 8 + p * 18, 142));
		}

        this.addSlotToContainer(new SlotCustom(playerInventory, 40, 88, 26)
        {
            /**
             * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
             */
            public boolean isItemValid(@Nullable ItemStack stack)
            {
                return super.isItemValid(stack);
            }
            @Nullable
            @SideOnly(Side.CLIENT)
            public String getSlotTexture()
            {
                return "minecraft:items/empty_armor_slot_shield";
            }
        });

		for (int k = 0; k < 3; ++k)
		{
			this.addSlotToContainer(new SlotCoin(playerInventory, 41 + k, 107 + k * 18, 26));
		}

		this.onCraftMatrixChanged(this.craftMatrix);
	}

	/**
	 * Callback for when the crafting matrix is changed.
	 */
	// //////////////////////////////////////////////////
	// ////////////////SPECIALIZATIONS///////////////////
	// //////////////////////////////////////////////////
	public void onCraftMatrixChanged(IInventory par1IInventory)
	{
		this.craftResult.setInventorySlotContents(0, findSpecRecipe(this.craftMatrix, this.thePlayer.worldObj, this.thePlayer));
	}

	/**
	 * Called when the container is closed.
	 */
	public void onContainerClosed(EntityPlayer playerIn)
	{
		super.onContainerClosed(playerIn);

		for (int i = 0; i < 4; ++i)
		{
			ItemStack itemstack = this.craftMatrix.removeStackFromSlot(i);

			if (itemstack != null)
			{
				playerIn.dropItem(itemstack, false);
			}
		}

		this.craftResult.setInventorySlotContents(0, (ItemStack) null);
	}

	public boolean canInteractWith(EntityPlayer par1EntityPlayer)
	{
		return true;
	}

	/**
	 * Called when a player shift-clicks on a slot. You must override this or you will crash when someone does that.
	 */
    @Nullable
	public ItemStack transferStackInSlot(EntityPlayer player, int index)
	{
		ItemStack itemstack = null;
		Slot slot = (Slot) this.inventorySlots.get(index);

		if (slot != null && slot.getHasStack())
		{
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();
            EntityEquipmentSlot entityequipmentslot = EntityLiving.getSlotForItemStack(itemstack);

			if (index == 0)
			{
				if (!player.worldObj.isRemote && this.shouldDamage(itemstack1))
				{
					SpecializationsMod.setDamagedItem(itemstack1, player, false);
				}

				if (!this.mergeItemStack(itemstack1, 9, 45, true))// if (!this.mergeItemStack(itemstack1, 9, 45, true, par1EntityPlayer))
				{
					return null;
				}
				slot.onSlotChange(itemstack1, itemstack);
			}
			else if (index >= 1 && index < 5)
			{
				if (!this.mergeItemStack(itemstack1, 9, 45, false))// if (!this.mergeItemStack(itemstack1, 9, 45, false, par1EntityPlayer))
				{
					return null;
				}
			}
			else if (index >= 5 && index < 9)
			{
				if (!this.mergeItemStack(itemstack1, 9, 45, false))// if (!this.mergeItemStack(itemstack1, 9, 45, false, par1EntityPlayer))
				{
					return null;
				}
			}
            else if (entityequipmentslot.getSlotType() == EntityEquipmentSlot.Type.ARMOR && !((Slot)this.inventorySlots.get(8 - entityequipmentslot.getIndex())).getHasStack())
			{
                int i = 8 - entityequipmentslot.getIndex();

                if (!this.mergeItemStack(itemstack1, i, i + 1, false))
                {
                    return null;
                }
			}
            else if (entityequipmentslot == EntityEquipmentSlot.OFFHAND && !((Slot)this.inventorySlots.get(45)).getHasStack())
            {
                if (!this.mergeItemStack(itemstack1, 45, 46, false))
                {
                    return null;
                }
            }
			else if (index >= 9 && index < 36)
			{
				if (!this.mergeItemStack(itemstack1, 36, 45, false))// if (!this.mergeItemStack(itemstack1, 36, 45, false, par1EntityPlayer))
				{
					return null;
				}
			}
			else if (index >= 36 && index < 45)
			{
				if (!this.mergeItemStack(itemstack1, 9, 36, false))// if (!this.mergeItemStack(itemstack1, 9, 36, false, par1EntityPlayer))
				{
					return null;
				}
			}
			else if (index >= 45 && index < 48)
			{
				return null;
			}
			else if (!this.mergeItemStack(itemstack1, 9, 45, false))// if (!this.mergeItemStack(itemstack1, 9, 45, false, par1EntityPlayer))
			{
				return null;
			}

			if (itemstack1.stackSize == 0)
			{
				slot.putStack((ItemStack) null);
			}
			else
			{
				slot.onSlotChanged();
			}

			if (itemstack1.stackSize == itemstack.stackSize)
			{
				return null;
			}

			slot.onPickupFromSlot(player, itemstack1);
		}

		return itemstack;
	}

	public boolean canMergeSlot(ItemStack p_94530_1_, Slot p_94530_2_)
	{
		return p_94530_2_.inventory != this.craftResult && super.canMergeSlot(p_94530_1_, p_94530_2_);
	}

	/**
	 * Handles slot click. Args : slotId, clickedButton, mode (0 = basic click, 1 = shift click, 2 = Hotbar, 3 = pickBlock, 4 = Drop, 5 = ?, 6 = Double click), player
	 */
	public ItemStack slotClick(int slotId, int clickedButton, ClickType mode, EntityPlayer player)
	{
		ItemStack apply = player.inventory.getItemStack();
		//System.out.println("Mode: " + mode + " Button: " + clickedButton + " " + slotId);
		if (apply != null && apply.getItem() instanceof ItemCoin && slotId >= 0)
		{
			if (mode == ClickType.PICKUP || mode == ClickType.PICKUP_ALL)
			{
				int coins = 0;
				for (int k = 0; k < 3; k++)
				{
					if (player.inventory.getStackInSlot(41 + k) instanceof ItemStack)
					{
						coins += ((ItemStack) player.inventory.getStackInSlot(41 + k)).stackSize;
					}
				}
				if (clickedButton == 0)
				{
					ItemStack target = this.getSlot(slotId).getStack();
					if (((target == null || (target != null && target.isItemEqual(apply))) && coins + apply.stackSize > 64) || (target != null && !target.isItemEqual(apply) && (coins + apply.stackSize - target.stackSize) > 64))
					{
						ItemStack splitter = apply.copy();
						splitter.stackSize = Math.min(64 - coins, apply.stackSize);
						return splitter;
					}
					else if(((target == null || (target != null && target.isItemEqual(apply))) && coins + apply.stackSize <= 64) || (target != null && !target.isItemEqual(apply) && (coins + apply.stackSize - target.stackSize) <= 64))
					{
						return super.slotClick(slotId, clickedButton, mode, player);
					}
					return null;
				}
				else if (clickedButton == 1)
				{
					if (coins + 1 > 64)
					{
						return null;
					}
				}
			}
			
			if(mode == ClickType.QUICK_CRAFT)
			{
				return null;
			}
		}
		return super.slotClick(slotId, clickedButton, mode, player);
	}

	public ItemStack findSpecRecipe(InventoryCrafting par1InventoryCrafting, World worldIn, EntityPlayer player)
	{
		ItemStack finalStack = CraftingManager.getInstance().findMatchingRecipe(this.craftMatrix, this.thePlayer.worldObj);
		LoMaS_Player specEntityPlayer = LoMaS_Player.getLoMaSPlayer(thePlayer);

		int i = 0;
		ItemStack itemstack = null;
		ItemStack itemstack1 = null;
		int j;

		for (j = 0; j < par1InventoryCrafting.getSizeInventory(); ++j)
		{
			ItemStack itemstack2 = par1InventoryCrafting.getStackInSlot(j);

			if (itemstack2 != null)
			{
				if (i == 0)
				{
					itemstack = itemstack2;
				}

				if (i == 1)
				{
					itemstack1 = itemstack2;
				}

				++i;
			}
		}

		if (i == 2 && itemstack != null && itemstack1 != null && itemstack.getItem().equals(itemstack1.getItem()) && itemstack.stackSize == 1 && itemstack1.stackSize == 1)
		{
			if (itemstack.getItem().isRepairable())
			{
				// System.out.println("It's a repair recipe! Kill it!");
				// if in here, it's a repair recipe!!!
				finalStack = null;
			}
		}

		if (finalStack != null)
		{
			//System.out.println(finalStack.getItem() + " " + SpecializationsMod.axeHardWood);
			if ((!(specEntityPlayer.specClass.equalsIgnoreCase("woodsman")) || (specEntityPlayer.specClass.equalsIgnoreCase("woodsman") && specEntityPlayer.classlevel < 2)) && finalStack != null)
			{
				if ((finalStack.getItem().equals(SpecializationsMod.AXEHARDWOOD) || finalStack.getItem().equals(SpecializationsMod.PICKAXEHARDWOOD)
						|| finalStack.getItem().equals(SpecializationsMod.SHOVELHARDWOOD) || finalStack.getItem().equals(SpecializationsMod.HOEHARDWOOD) 
						|| finalStack.getItem().equals(SpecializationsMod.SWORDHARDWOOD)))
				{
					finalStack = null;
				}
			}

			if ((!(specEntityPlayer.specClass.equalsIgnoreCase("spellbinder")) || (specEntityPlayer.specClass.equalsIgnoreCase("spellbinder") && specEntityPlayer.classlevel < 2)) && finalStack != null)
			{
				if ((finalStack.getItem().equals(SpecializationsMod.ARROW) && finalStack.getItemDamage() == 3))
				{
					finalStack = null;
				}
			}

			if ((!(specEntityPlayer.specClass.equalsIgnoreCase("blacksmith")) || (specEntityPlayer.specClass.equalsIgnoreCase("blacksmith") && specEntityPlayer.classlevel < 2)) && finalStack != null)
			{
				if (((finalStack.getItem().equals(SpecializationsMod.ARROW) && finalStack.getItemDamage() == 1)
						|| (finalStack.getItem().equals(SpecializationsMod.ARROW) && finalStack.getItemDamage() == 2)
						|| (finalStack.getItem().equals(SpecializationsMod.ARROW) && finalStack.getItemDamage() == 4) 
						|| finalStack.getItem().equals(SpecializationsMod.STONE_SWORD)
						|| finalStack.getItem().equals(SpecializationsMod.STONE_PICKAXE) 
						|| finalStack.getItem().equals(SpecializationsMod.STONE_SHOVEL) 
						|| finalStack.getItem().equals(SpecializationsMod.STONE_HOE)
						|| finalStack.getItem().equals(SpecializationsMod.STONE_AXE) 
						|| finalStack.getItem().equals(SpecializationsMod.IRON_SWORD) 
						|| finalStack.getItem().equals(SpecializationsMod.IRON_HOE)
						|| finalStack.getItem().equals(SpecializationsMod.IRON_AXE) 
						|| finalStack.getItem().equals(SpecializationsMod.IRON_PICKAXE) 
						|| finalStack.getItem().equals(SpecializationsMod.IRON_SHOVEL)
						|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_SWORD) 
						|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_HOE) 
						|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_PICKAXE)
						|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_SHOVEL) 
						|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_AXE) 
						|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_SWORD)
						|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_HOE) 
						|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_PICKAXE) 
						|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_SHOVEL)
						|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_AXE) 
						|| finalStack.getItem().equals(SpecializationsMod.IRON_BOOTS) 
						|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_BOOTS)
						|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_BOOTS) 
						|| finalStack.getItem().equals(SpecializationsMod.IRON_CHESTPLATE) 
						|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_CHESTPLATE)
						|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_CHESTPLATE) 
						|| finalStack.getItem().equals(SpecializationsMod.IRON_HELMET) 
						|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_HELMET)
						|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_HELMET) 
						|| finalStack.getItem().equals(SpecializationsMod.IRON_LEGGINGS) 
						|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_LEGGINGS)
						|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_LEGGINGS) 
						|| finalStack.getItem().equals(SpecializationsMod.SWORDOBSIDIAN)
						|| finalStack.getItem().equals(SpecializationsMod.HOEOBSIDIAN) 
						|| finalStack.getItem().equals(SpecializationsMod.PICKAXEOBSIDIAN)
						|| finalStack.getItem().equals(SpecializationsMod.SHOVELOBSIDIAN) 
						|| finalStack.getItem().equals(SpecializationsMod.AXEOBSIDIAN)
						|| finalStack.getItem().equals(SpecializationsMod.BOOTSOBSIDIAN) 
						|| finalStack.getItem().equals(SpecializationsMod.PLATEOBSIDIAN)
						|| finalStack.getItem().equals(SpecializationsMod.HELMETOBSIDIAN) 
						|| finalStack.getItem().equals(SpecializationsMod.LEGSOBSIDIAN)))
				{
					finalStack = null;
				}
			}
			else if (finalStack != null)
			{
				if (specEntityPlayer.classlevel < 4)
				{
					if (((finalStack.getItem().equals(SpecializationsMod.ARROW) && finalStack.getItemDamage() == 2) || (finalStack.getItem().equals(SpecializationsMod.ARROW) && finalStack.getItemDamage() == 4)
							|| finalStack.getItem().equals(SpecializationsMod.IRON_SWORD) || finalStack.getItem().equals(SpecializationsMod.IRON_HOE) || finalStack.getItem().equals(SpecializationsMod.IRON_AXE)
							|| finalStack.getItem().equals(SpecializationsMod.IRON_PICKAXE) || finalStack.getItem().equals(SpecializationsMod.IRON_SHOVEL) || finalStack.getItem().equals(SpecializationsMod.GOLDEN_SWORD)
							|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_HOE) || finalStack.getItem().equals(SpecializationsMod.GOLDEN_PICKAXE) || finalStack.getItem().equals(SpecializationsMod.GOLDEN_SHOVEL)
							|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_AXE) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_SWORD) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_HOE)
							|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_PICKAXE) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_SHOVEL) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_AXE)
							|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_BOOTS) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_BOOTS) || finalStack.getItem().equals(SpecializationsMod.IRON_CHESTPLATE)
							|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_CHESTPLATE) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_CHESTPLATE) || finalStack.getItem().equals(SpecializationsMod.IRON_HELMET)
							|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_HELMET) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_HELMET) || finalStack.getItem().equals(SpecializationsMod.IRON_LEGGINGS)
							|| finalStack.getItem().equals(SpecializationsMod.GOLDEN_LEGGINGS) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_LEGGINGS)
							|| finalStack.getItem().equals(SpecializationsMod.SWORDOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.HOEOBSIDIAN)
							|| finalStack.getItem().equals(SpecializationsMod.PICKAXEOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.SHOVELOBSIDIAN)
							|| finalStack.getItem().equals(SpecializationsMod.AXEOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.BOOTSOBSIDIAN)
							|| finalStack.getItem().equals(SpecializationsMod.PLATEOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.HELMETOBSIDIAN) || finalStack.getItem().equals(
							SpecializationsMod.LEGSOBSIDIAN)))
					{
						finalStack = null;
					}
				}

				else if (specEntityPlayer.classlevel < 8)
				{
					if (((finalStack.getItem().equals(SpecializationsMod.ARROW) && finalStack.getItemDamage() == 4)) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_SWORD)
							|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_HOE) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_PICKAXE) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_SHOVEL)
							|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_AXE) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_BOOTS) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_CHESTPLATE)
							|| finalStack.getItem().equals(SpecializationsMod.DIAMOND_HELMET) || finalStack.getItem().equals(SpecializationsMod.DIAMOND_LEGGINGS)
							|| finalStack.getItem().equals(SpecializationsMod.SWORDOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.HOEOBSIDIAN)
							|| finalStack.getItem().equals(SpecializationsMod.PICKAXEOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.SHOVELOBSIDIAN)
							|| finalStack.getItem().equals(SpecializationsMod.AXEOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.BOOTSOBSIDIAN)
							|| finalStack.getItem().equals(SpecializationsMod.PLATEOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.HELMETOBSIDIAN)
							|| finalStack.getItem().equals(SpecializationsMod.LEGSOBSIDIAN))
					{
						finalStack = null;
					}
				}

				else if (specEntityPlayer.classlevel < 10)
				{
					if ((finalStack.getItem().equals(SpecializationsMod.SWORDOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.HOEOBSIDIAN)
							|| finalStack.getItem().equals(SpecializationsMod.PICKAXEOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.SHOVELOBSIDIAN)
							|| finalStack.getItem().equals(SpecializationsMod.AXEOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.BOOTSOBSIDIAN) || finalStack.getItem().equals(
							SpecializationsMod.PLATEOBSIDIAN))
							|| finalStack.getItem().equals(SpecializationsMod.HELMETOBSIDIAN) || finalStack.getItem().equals(SpecializationsMod.LEGSOBSIDIAN))
					{
						finalStack = null;
					}
				}
			}
		}
		return finalStack;
	}

	public boolean shouldDamage(ItemStack crafted)
	{
		if (crafted.getItem() instanceof ItemTool)
		{
			return true;
		}
		if (crafted.getItem() instanceof ItemHoe)
		{
			return true;
		}
		else if (crafted.getItem() instanceof ItemSword)
		{
			return true;
		}
		else if (crafted.getItem() instanceof ItemArmor)
		{
			return true;
		}
		else if (crafted.getItem() instanceof ItemBow)
		{
			return true;
		}
		return false;
	}
}
