package bitevo.Zetal.LoMaS.Economy.GUI;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import org.lwjgl.opengl.GL11;

import bitevo.Zetal.LoMaS.Economy.EconomyMod;
import bitevo.Zetal.LoMaS.Economy.Inventory.ContainerSafe;
import bitevo.Zetal.LoMaS.Economy.TileEntity.TileEntitySafe;

@SideOnly(Side.CLIENT)
public class GuiSafe extends GuiContainer
{
    private static final ResourceLocation safe = new ResourceLocation(EconomyMod.modid, "gui/safe.png");
    public TileEntitySafe safeTile;

    public GuiSafe(InventoryPlayer par1InventoryPlayer, TileEntitySafe par2TileEntitySafe)
    {
        super(new ContainerSafe(par1InventoryPlayer, par2TileEntitySafe));
        this.safeTile = par2TileEntitySafe;
    }
    
    public void initGui()
    {
        super.initGui();
        this.drawGuiContainerBackgroundLayer(0, 0, 0);
        this.drawGuiContainerForegroundLayer(0, 0);
    }

    /**
     * Draw the foreground layer for the GuiContainer (everything in front of the items)
     */
    protected void drawGuiContainerForegroundLayer(int par1, int par2)
    {
        String s = this.safeTile.hasCustomName() ? this.safeTile.getName() : I18n.format(this.safeTile.getName());
        this.fontRendererObj.drawString(s, this.xSize / 2 - this.fontRendererObj.getStringWidth(s) / 2, 6, 4210752);
		this.mc.thePlayer.inventoryContainer.detectAndSendChanges();
    	int coins = 0;
    	for(int i = 0; i < 3; i++)
    	{
    		if(this.mc.thePlayer.inventory.getStackInSlot(41 + i) instanceof ItemStack)
    		{
    			coins += ((ItemStack) this.mc.thePlayer.inventory.getStackInSlot(41 + i)).stackSize;
    		}
    	}
    	if(coins > 64)
    	{
    		coins = 64;
    	}
        this.fontRendererObj.drawString(coins + "/64", 80, 76, 4210752);
    }

    /**
     * Draw the background layer for the GuiContainer (everything behind the items)
     */
    protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3)
    {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(safe);
        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
    }
}
