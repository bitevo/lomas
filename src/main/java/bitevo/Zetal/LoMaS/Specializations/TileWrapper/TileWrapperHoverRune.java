package bitevo.Zetal.LoMaS.Specializations.TileWrapper;

import java.util.ArrayList;
import java.util.UUID;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class TileWrapperHoverRune extends TileWrapperRune
{
	public TileWrapperHoverRune(String name, World world, BlockPos pos, EnumFacing facing)
	{
		super(name, world, pos, facing);
	}

	public TileWrapperHoverRune(String name, World world, BlockPos pos, EnumFacing facing, UUID owner)
	{
		super(name, world, pos, facing, owner);
	}
	
	@Override
	public void update()
	{
		double dist = 0.0f;
        AxisAlignedBB axisalignedbb = this.getDetectionBox(this.getPos());
		ArrayList<EntityLivingBase> riders = (ArrayList<EntityLivingBase>) this.getWorld().getEntitiesWithinAABB(EntityLivingBase.class, axisalignedbb);
		for(EntityLivingBase living : riders)
		{
			dist = living.getDistance(this.getPos().getX(), this.getPos().getY(), this.getPos().getZ());
			double add = ((3.0D/dist) - 1.0D);
			living.fallDistance = 0.0f;
			living.motionY += add;
			living.moveEntity(living.motionX, living.motionY, living.motionZ);
		}
	}
	
    private AxisAlignedBB getDetectionBox(BlockPos pos)
    {
        float f = 0.2F;
        return new AxisAlignedBB((double)((float)pos.getX() - f), (double)pos.getY() - f, (double)((float)pos.getZ() - f), (double)((float)(pos.getX() + 1) + f), (double)((float)(pos.getY() + 6) + f), (double)((float)(pos.getZ() + 1) + f));
    }

	@Override
	public String getDefaultName()
	{
		return "hover_rune";
	}
}
