package bitevo.Zetal.LoMaS.Economy.Inventory;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.Economy.EconomyMod;
import bitevo.Zetal.LoMaS.Economy.Item.ItemCoin;
import bitevo.Zetal.LoMaS.Economy.Message.BankMessage;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class SlotBankInput extends Slot
{
	public SlotBankInput(InventoryBank inventoryIn, int index, int xPosition, int yPosition)
	{
		super(inventoryIn, index, xPosition, yPosition);
	}

	@Override
    @Nullable
    @SideOnly(Side.CLIENT)
    public String getSlotTexture()
    {
		return EconomyMod.modid + ":" + "items/CC";
	}

	@Override
	public void putStack(ItemStack stack)
	{
		EntityPlayer player = ((InventoryBank) this.inventory).player;
		if (!player.worldObj.isRemote && stack != null && stack.getItem() instanceof ItemCoin)
		{
			int value = ((ItemCoin) stack.getItem()).getCoinValue();
			value *= stack.stackSize;
			LoMaS_Player ep = LoMaS_Player.getLoMaSPlayer(player);
			ep.setBankCC(ep.getBankCC() + value);
			stack.stackSize = 0;
			EconomyMod.snw.sendTo(new BankMessage(ep.getBankCC()), (EntityPlayerMP) player);
		}
		this.inventory.setInventorySlotContents(this.getSlotIndex(), null);
		this.onSlotChanged();
	}

	@Override
	public boolean isItemValid(ItemStack par1ItemStack)
	{
		Item item = (par1ItemStack == null ? null : par1ItemStack.getItem());
		boolean isCoin = item instanceof ItemCoin;
		return isCoin;
	}
}
