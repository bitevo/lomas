package bitevo.Zetal.LoMaS.Economy.Inventory;

import bitevo.Zetal.LoMaS.Economy.EconomyMod;
import bitevo.Zetal.LoMaS.Economy.Item.ItemCoin;
import bitevo.Zetal.LoMaS.Economy.Message.BankMessage;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotBankOutput extends Slot
{
	public InventoryBank bank;

	public SlotBankOutput(InventoryBank inventoryIn, int index, int xPosition, int yPosition)
	{
		super(inventoryIn, index, xPosition, yPosition);
		this.bank = inventoryIn;
	}

	@Override
	public boolean isItemValid(ItemStack stack)
	{
		return false;
	}

	@Override
	public boolean canTakeStack(EntityPlayer player)
	{
		ItemStack stackTaken = this.getStack();
		if (stackTaken != null && stackTaken.getItem() instanceof ItemCoin)
		{
			int takenValue = ((ItemCoin) stackTaken.getItem()).getCoinValue() * stackTaken.stackSize;
			int ccStorage = LoMaS_Player.getLoMaSPlayer(player).getBankCC();
			if (ccStorage >= takenValue)
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public void onPickupFromSlot(EntityPlayer player, ItemStack stack)
	{
		if (stack != null && stack.getItem() instanceof ItemCoin)
		{
			int takenValue = ((ItemCoin) stack.getItem()).getCoinValue();
			int ccStorage = LoMaS_Player.getLoMaSPlayer(player).getBankCC();

			if (!player.worldObj.isRemote && ccStorage >= takenValue)
			{
				LoMaS_Player.getLoMaSPlayer(player).setBankCC(ccStorage - takenValue);
				LoMaS_Player ep = LoMaS_Player.getLoMaSPlayer(player);
				EconomyMod.snw.sendTo(new BankMessage(ep.getBankCC()), (EntityPlayerMP) player);
			}
			this.putStack(new ItemStack(stack.getItem()));
			this.onSlotChanged();
		}
	}
}
