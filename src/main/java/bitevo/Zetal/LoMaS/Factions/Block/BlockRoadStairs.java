package bitevo.Zetal.LoMaS.Factions.Block;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.block.BlockStairs;
import net.minecraft.block.state.IBlockState;

public class BlockRoadStairs extends BlockStairs
{
	public BlockRoadStairs(IBlockState state)
	{
		super(state);
		this.useNeighborBrightness = true;
		this.setHarvestLevel("pickaxe", 0);
		this.setCreativeTab(LoMaS_Utils.tabLoMaS);
	}
}
