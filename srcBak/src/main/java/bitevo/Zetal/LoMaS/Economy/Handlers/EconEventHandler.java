package bitevo.Zetal.LoMaS.Economy.Handlers;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import bitevo.Zetal.LoMaS.Economy.EconomyMod;
import bitevo.Zetal.LoMaS.Economy.Inventory.ContainerTrade;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.event.entity.player.PlayerContainerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.EntityInteract;
import net.minecraftforge.fml.common.eventhandler.Event.Result;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * Name and cast of this class are irrelevant
 */
public class EconEventHandler
{
	public static HashMap<EntityPlayer, HashMap<EntityPlayer, Integer>> tradeTicker = new HashMap();
	public int tradeTimer = 8;

	@SubscribeEvent
	public void onPlayerItemPickup(EntityItemPickupEvent event)
	{
		EntityPlayer player = event.getEntityPlayer();
		if (player.openContainer instanceof ContainerTrade)
		{
			event.setResult(Result.DENY);
		}
	}

	@SubscribeEvent
	public void onPlayerInteractPlayer(EntityInteract event)
	{
		if (!event.getEntity().worldObj.isRemote)
		{
			if (event.getEntityPlayer().getHeldItem(event.getHand()) == null)
			{
				Entity t = event.getTarget();
				EntityPlayer source = event.getEntityPlayer();
				if (t instanceof EntityPlayer)
				{
					EntityPlayer target = (EntityPlayer) event.getTarget();
					HashMap<EntityPlayer, Integer> pt2 = tradeTicker.get(source);
					if (pt2 == null)
					{
						pt2 = new HashMap<EntityPlayer, Integer>();
					}
					HashMap<EntityPlayer, Integer> pt2Target = tradeTicker.get(target);
					if (pt2Target == null)
					{
						pt2Target = new HashMap<EntityPlayer, Integer>();
					}

					if (pt2Target.containsKey(source))
					{
						int targetCounter = pt2Target.get(source);
						if (targetCounter <= this.tradeTimer)
						{
							source.openGui(EconomyMod.instance, 2, event.getEntity().worldObj, 0, 0, 0);
							target.openGui(EconomyMod.instance, 2, event.getEntity().worldObj, 0, 0, 0);
							pt2.put(target, -1);
							pt2Target.put(source, -1);
							tradeTicker.put(target, pt2);
							tradeTicker.put(source, pt2Target);
						}
					}
					else
					{
						pt2.put(target, 0);
						tradeTicker.put(source, pt2);
					}
				}
				event.setResult(Result.ALLOW);
			}
		}
	}

	@SubscribeEvent
	public void onPlayerUpdate(LivingUpdateEvent event)
	{
		if (!event.getEntity().worldObj.isRemote)
		{
			if (event.getEntityLiving() instanceof EntityPlayer && event.getEntityLiving().ticksExisted % 20 == 0)
			{
				EntityPlayer player = (EntityPlayer) event.getEntityLiving();
				HashMap<EntityPlayer, Integer> sourceMap = this.tradeTicker.get(player);
				if (sourceMap != null)
				{
					Iterator<Entry<EntityPlayer, Integer>> targetIter = sourceMap.entrySet().iterator();
					while (targetIter.hasNext())
					{
						HashMap.Entry<EntityPlayer, Integer> targetPair = targetIter.next();
						EntityPlayer target = targetPair.getKey();
						int ticker = targetPair.getValue();
						System.out.println(ticker);
						if (ticker >= this.tradeTimer)
						{
							this.tradeTicker.get(player).remove(target);
						}
						else if (ticker >= 0)
						{
							targetPair.setValue(ticker + 1);
						}
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onContainerClosed(PlayerContainerEvent.Close event)
	{
		System.out.println(event.getContainer());
		if (!event.getEntityPlayer().worldObj.isRemote)
		{
			if (event.getContainer() instanceof ContainerTrade)
			{
				EntityPlayerMP closer = (EntityPlayerMP) event.getEntityPlayer();
				HashMap<EntityPlayer, Integer> sourceMap = this.tradeTicker.get(closer);
				if (sourceMap != null)
				{
					Iterator<Entry<EntityPlayer, Integer>> targetIter = sourceMap.entrySet().iterator();
					HashMap.Entry<EntityPlayer, Integer> targetPair = null;
					while (targetIter.hasNext())
					{
						targetPair = targetIter.next();
						EntityPlayerMP target = (EntityPlayerMP) targetPair.getKey();
						int ticker = targetPair.getValue();
						if (ticker == -1)
						{
							this.tradeTicker.remove(closer);
							this.tradeTicker.remove(target);
							closer.openGui(null, 0, null, 0, 0, 0);
							target.openGui(null, 0, null, 0, 0, 0);
							closer.closeContainer();
							target.closeContainer();
						}
					}
				}
			}
		}
	}
}