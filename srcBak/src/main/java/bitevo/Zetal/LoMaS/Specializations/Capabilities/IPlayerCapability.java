package bitevo.Zetal.LoMaS.Specializations.Capabilities;

import bitevo.Zetal.LoMaS.Economy.Inventory.InventoryBank;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class IPlayerCapability
{
	private InventoryBank bank;
	public EntityPlayer player;
	public World world;
	
    public InventoryBank getInventoryBank() { return bank; }
    public void setInventoryBank(InventoryBank value) { this.bank = value; }
	public EntityPlayer getEntityPlayer(){ return player; }
	public void setEntityPlayer(EntityPlayer entity){ this.player = entity; }
	public World getWorld(){ return this.world; }
	public void setWorld(World world){ this.world = world; }
}