package bitevo.Zetal.LoMaS.TileWrappers.TileWrapper;

import javax.annotation.Nullable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bitevo.Zetal.LoMaS.TileWrappers.Message.TileWrapperMessage;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.crash.ICrashReportDetail;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public abstract class TileWrapper implements ITickable
{
    private static final Logger LOGGER = LogManager.getLogger();
	protected String name = "defwrapper";
	/** The identifying index for this tilewrapper in the block it occupies */
	protected int index = 0;
    private World world = null;
    protected BlockPos pos = BlockPos.ORIGIN;
    /** The side the TileWrapper was placed on */
    protected EnumFacing facing = null;
    /** Initial metadata of the block the TileWrapper was placed on */
    protected int blockMetadata = -1;
    /** The Block type that this TileWrapper is contained within */
    protected Block blockType = null;
    /** Used to determine whether the TileWrapper should be removed */
    protected boolean isInvalid = false;
    
    /**
     * This constructor is required for all TileWrappers
     * @param name
     * @param world
     * @param pos
     * @param facing
     */
    public TileWrapper(String name, World world, BlockPos pos, EnumFacing facing)
    {
    	this.name = name;
    	this.world = world;
    	this.pos = pos;
    	this.blockType = world != null && pos != null ? world.getBlockState(pos).getBlock() : null;
    	this.facing = facing;
    }
    
    /**
     * Returns the worldObj for this tileEntity.
     */
    public World getWorld()
    {
        return this.world;
    }

    /**
     * Sets the worldObj for this tileEntity.
     */
    public void setWorld(World worldIn)
    {
        this.world = worldIn;
    }

    /**
     * Returns true if the worldObj isn't null.
     */
    public boolean hasWorld()
    {
        return this.world != null;
    }

    public BlockPos getPos()
    {
        return this.pos;
    }

    public void setPos(BlockPos posIn)
    {
        if (posIn instanceof BlockPos.MutableBlockPos || posIn instanceof BlockPos.PooledMutableBlockPos)
        {
            LOGGER.warn((String)"Tried to assign a mutable BlockPos to a TileWrapper...", (Throwable)(new Error(posIn.getClass().toString())));
            posIn = new BlockPos(posIn);
        }

        this.pos = posIn;
    }

    /**
     * Gets the block type at the location of this entity (client-only).
     */
    public Block getBlockType()
    {
        if (this.blockType == null && this.world != null)
        {
            this.blockType = this.world.getBlockState(this.pos).getBlock();
        }

        return this.blockType;
    }

    public int getBlockMetadata()
    {
        if (this.blockMetadata == -1)
        {
            IBlockState iblockstate = this.world.getBlockState(this.pos);
            this.blockMetadata = iblockstate.getBlock().getMetaFromState(iblockstate);
        }

        return this.blockMetadata;
    }

    /**
     * Ensures the chunk is saved to disk later - the game won't think it hasn't changed and skip it.
     */
    public void markDirty()
    {
        if (this.world != null)
        {
            IBlockState iblockstate = this.world.getBlockState(this.pos);
            this.blockMetadata = iblockstate.getBlock().getMetaFromState(iblockstate);
            if (world.isBlockLoaded(pos))
            {
                world.getChunkFromBlockCoords(pos).setChunkModified();
            }

            if (this.getBlockType() != Blocks.AIR)
            {
                this.world.updateComparatorOutputLevel(this.pos, this.getBlockType());
            }
        }
    }
    
    public boolean shouldRefresh()
    {
        return this.getWorld().getBlockState(this.getPos()).getBlock() != this.getBlockType();
    }

    public boolean isInvalid()
    {
        return this.isInvalid;
    }

    /**
     * invalidates a tile wrapper
     */
    public void invalidate()
    {
        this.isInvalid = true;
    }

    /**
     * validates a tile wrapper
     */
    public void validate()
    {
        this.isInvalid = false;
    }

    public boolean shouldRenderInPass(int pass)
    {
        return pass == 0;
    }
    
    public void onUpdatePacket(TileWrapperMessage pkt)
    {
        this.readFromNBT(pkt.getNbt());
    }

    @Nullable
    public IMessage getUpdatePacket()
    {
        return new TileWrapperMessage(this.pos, this.index, this.getUpdateTag(), this.getClass());
    }

    public NBTTagCompound getUpdateTag()
    {
        return this.writeInternal(new NBTTagCompound());
    }

    public void readFromNBT(NBTTagCompound compound)
    {
    	this.name = compound.getString("id");
    	this.index = compound.getInteger("index");
        this.pos = new BlockPos(compound.getInteger("x"), compound.getInteger("y"), compound.getInteger("z"));
        this.facing = EnumFacing.VALUES[compound.getByte("facing")];
        this.isInvalid = compound.getBoolean("invalid");
    }

    public NBTTagCompound writeToNBT(NBTTagCompound compound)
    {
        return this.writeInternal(compound);
    }

    private NBTTagCompound writeInternal(NBTTagCompound compound)
    {
        compound.setString("id", name);
        compound.setInteger("index", this.index);
        compound.setInteger("x", this.pos.getX());
        compound.setInteger("y", this.pos.getY());
        compound.setInteger("z", this.pos.getZ());
        compound.setByte("facing", (byte) facing.getIndex());
        compound.setBoolean("invalid", this.isInvalid());
        return compound;
    }

	public void addInfoToCrashReport(CrashReportCategory reportCategory)
	{
        reportCategory.setDetail("Name", new ICrashReportDetail<String>()
        {
            public String call() throws Exception
            {
                return TileWrapper.this.name;
            }
        });

        if (this.world != null)
        {
            CrashReportCategory.addBlockInfo(reportCategory, this.pos, this.getBlockType(), this.getBlockMetadata());
            reportCategory.setDetail("Actual block type", new ICrashReportDetail<String>()
            {
                public String call() throws Exception
                {
                    int i = Block.getIdFromBlock(TileWrapper.this.world.getBlockState(TileWrapper.this.pos).getBlock());

                    try
                    {
                        return String.format("ID #%d (%s // %s)", new Object[] {Integer.valueOf(i), Block.getBlockById(i).getUnlocalizedName(), Block.getBlockById(i).getClass().getCanonicalName()});
                    }
                    catch (Throwable var3)
                    {
                        return "ID #" + i;
                    }
                }
            });
            reportCategory.setDetail("Actual block data value", new ICrashReportDetail<String>()
            {
                public String call() throws Exception
                {
                    IBlockState iblockstate = TileWrapper.this.world.getBlockState(TileWrapper.this.pos);
                    int i = iblockstate.getBlock().getMetaFromState(iblockstate);

                    if (i < 0)
                    {
                        return "Unknown? (Got " + i + ")";
                    }
                    else
                    {
                        String s = String.format("%4s", new Object[] {Integer.toBinaryString(i)}).replace(" ", "0");
                        return String.format("%1$d / 0x%1$X / 0b%2$s", new Object[] {Integer.valueOf(i), s});
                    }
                }
            });
        }
	}

	public int getIndex()
	{
		return index;
	}
}
