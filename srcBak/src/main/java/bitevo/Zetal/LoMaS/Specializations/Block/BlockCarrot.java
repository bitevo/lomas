package bitevo.Zetal.LoMaS.Specializations.Block;

import net.minecraft.item.Item;
import bitevo.Zetal.LoMaS.Specializations.FoodInitializer;

public class BlockCarrot extends BlockCrops
{
    protected Item getSeed()
    {
        return FoodInitializer.CARROT_SEEDS;
    }

    protected Item getCrop()
    {
        return FoodInitializer.CARROT;
    }
}