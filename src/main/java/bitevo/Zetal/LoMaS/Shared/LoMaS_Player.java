package bitevo.Zetal.LoMaS.Shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class LoMaS_Player implements Serializable
{
	public static HashMap<UUID, LoMaS_Player> playerList = new HashMap<UUID, LoMaS_Player>();
	private static final long serialVersionUID = 1L;
	public static final float abilCooldown = 150.0f;
	public static final int maxLevel = 20;

	public UUID uuid;
	public String name = "ERROR";
	private int classlevel = 1;
	public int counter = 0;
	public boolean isActive = false;
	public boolean levelDisplay = false;
	public float progress = 0.0f;
	public String rank = "Member";
	public String guildtag = "";
	public float secCounter = 0.0f;
	public float prevfoodExhaustionLevel = -1.0f;
	private ArrayList<UUID> citizens = new ArrayList();

	public UUID faction;
	private int bankCC = 90;
	private LoMaS_Skills skills;

	public transient EntityPlayer player;
	public transient ItemStack arrowStack;
	public transient boolean notYet = true;

	public static void addLoMaSPlayer(EntityPlayer ePlayer)
	{
		if (ePlayer != null)
		{
			if (getLoMaSPlayer(ePlayer) == null)
			{
				LoMaS_Player p = new LoMaS_Player();
				p.player = ePlayer;
				p.uuid = ePlayer.getPersistentID();
				p.name = ePlayer.getDisplayNameString();
				playerList.put(p.uuid, p);
				
				p.progress = LoMaS_Player.getReqProgressFromLevel(p.classlevel - 1);
				
				System.out.println("Added a new LoMaSPlayer with the name " + p.name);
			}
			else
			{
				getLoMaSPlayer(ePlayer).player = ePlayer;
			}
		}
		else
		{
			System.err.println("Tried to add a NULL player to LoMaS_Player!");
		}
	}
	
	public static LoMaS_Player getLoMaSPlayer(EntityPlayer ePlayer)
	{
		LoMaS_Player retPlayer = ePlayer != null ? getLoMaSPlayer(ePlayer.getPersistentID()) : null;
		if(retPlayer != null && retPlayer.player == null)
		{
			retPlayer.player = ePlayer;
		}
		return retPlayer;
	}

	public static LoMaS_Player getLoMaSPlayer(UUID pid)
	{
		return pid != null ? playerList.get(pid) : null;
	}

	public void copyTraitsFromLoMaSPlayer(LoMaS_Player lPlayer)
	{
		this.progress = lPlayer.progress;
		this.classlevel = lPlayer.classlevel;
		this.rank = lPlayer.rank;
		this.guildtag = lPlayer.guildtag;
		this.counter = lPlayer.counter;
		this.secCounter = lPlayer.secCounter;
		this.isActive = lPlayer.isActive;
		this.levelDisplay = lPlayer.levelDisplay;
		this.prevfoodExhaustionLevel = lPlayer.prevfoodExhaustionLevel;
		this.uuid = lPlayer.uuid;
		this.setCitizens(lPlayer.getCitizens());
		this.bankCC = lPlayer.bankCC;
		this.faction = lPlayer.faction;
		this.skills = lPlayer.skills;
	}

	public void setSpecClassLevel(int classlevel)
	{
		if (classlevel > this.maxLevel)
		{
			this.classlevel = this.maxLevel;
		}
		else
		{
			this.classlevel = classlevel;
		}
		
		this.progress = LoMaS_Player.getReqProgressFromLevel(this.classlevel - 1);
	}

	public void setSpecProgress(float progress)
	{
		this.progress = progress;
	}

	public void addSpecClassLevel(int addclasslevel)
	{
		if (this.classlevel + addclasslevel > this.maxLevel)
		{
			this.classlevel = this.maxLevel;
		}
		else
		{
			this.classlevel = this.classlevel + addclasslevel;
			this.levelDisplay = true;
		}
		
		this.progress = LoMaS_Player.getReqProgressFromLevel(this.classlevel - 1);
	}

	public void addSpecProgress(float addprogress)
	{
		this.progress = this.progress + addprogress;
		if (this.progress >= this.getReqProgress() && this.classlevel != 0)
		{
			this.addSpecClassLevel(1);
		}
	}

	public void doDeathPenalty()
	{
		this.progress = this.progress - (this.getReqProgress() * 0.05f);
		if (this.progress <= this.getReqProgressFromLevel(classlevel--) && this.classlevel != 1)
		{
			this.addSpecClassLevel(-1);
		}
	}

	/**
	 * This returns the required progress to reach the next level from the input level
	 */
	public static float getReqProgressFromLevel(int classlevel)
	{
		int nextLevel = classlevel + 1;
		//float reqProgress = (float) ((500 * Math.pow(nextLevel, 2)) + (Math.pow(nextLevel, 5)) - 1500);
		float reqProgress = (float) (200 * ((1 * nextLevel) + (0.02*Math.pow(Math.E, nextLevel / 2.2))));
		return reqProgress;
	}

	public float getReqProgress()
	{
		return LoMaS_Player.getReqProgressFromLevel(classlevel);
	}

	public int getBankCC()
	{
		return bankCC;
	}

	public void setBankCC(int bankCC)
	{
		this.bankCC = bankCC;
	}

	public ArrayList<UUID> getCitizens()
	{
		if(citizens == null) {
			setCitizens(new ArrayList<UUID>());
		}
		return citizens;
	}

	public void setCitizens(ArrayList<UUID> citizens)
	{
		this.citizens = citizens;
	}

	public LoMaS_Skills getSkills()
	{
		if(this.skills == null)
		{
			this.skills = new LoMaS_Skills();
		}
		return skills;
	}

	public void setSkills(LoMaS_Skills skills)
	{
		this.skills = skills;
	}

	public void setSkills(String skills)
	{
		this.skills = new LoMaS_Skills(skills);
	}

	public int getClassLevel()
	{
		return classlevel;
	}
	
	public int getUnspentPoints()
	{
		return this.getClassLevel() - (this.getSkills().getOwnedNodes().size() - 1);
	}
}
