package bitevo.Zetal.LoMaS.AdminControlSystems.Handler;

import java.util.ArrayList;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;

public class MotDConnectionHandler
{
	@SubscribeEvent
	public void playerLoggedIn(PlayerLoggedInEvent event)
	{
		ArrayList<String> motd = EventHookContainerClass.readTextData("motd.txt");
		for (int i = 0; i < motd.size(); i++)
		{
			EntityPlayerMP ply = (EntityPlayerMP) event.player;
			if (ply != null)
			{
				if (motd.get(i) != null && motd.get(i).length() > 0)
				{
					LoMaS_Utils.addChatMessage(ply, "\u00a75\u00a7l" + motd.get(i).substring(0, motd.get(i).length()));
				}
				else
				{
					LoMaS_Utils.addChatMessage(ply, "");
				}
			}
		}
	}
}
