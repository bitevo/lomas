package bitevo.Zetal.LoMaS.Specializations.Inventory.Slot;

import java.lang.reflect.Field;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.stats.AchievementList;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.fml.common.FMLCommonHandler;
import bitevo.Zetal.LoMaS.Specializations.FoodInitializer;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Handler.FMLHandler;
import bitevo.Zetal.LoMaS.Specializations.Inventory.SpecContainerWorkbench;
import bitevo.Zetal.LoMaS.Specializations.Items.ItemFood;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySpecWorkbench;

public class SlotCrafting extends Slot
{
    /** The player that is using the GUI where this slot resides. */
	private final TileEntitySpecWorkbench bench;
    private final EntityPlayer thePlayer;
    /** The number of items that have been crafted so far. Gets passed to ItemStack.onCrafting before being reset. */
    private int amountCrafted;
    private static final String __OBFID = "CL_00001761";

    public SlotCrafting(EntityPlayer player, TileEntitySpecWorkbench bench, int slotIndex, int xPosition, int yPosition)
    {
        super(bench, slotIndex, xPosition, yPosition);
        this.bench = bench;
        this.thePlayer = player;
    }

    /**
     * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
     */
    public boolean isItemValid(ItemStack stack)
    {
        return false;
    }

    /**
     * Decrease the size of the stack in slot (first int arg) by the amount of the second int arg. Returns the new
     * stack.
     */
    public ItemStack decrStackSize(int amount)
    {
        if (this.getHasStack())
        {
            this.amountCrafted += Math.min(amount, this.getStack().stackSize);
        }

        return super.decrStackSize(amount);
    }

    /**
     * the itemStack passed in is the output - ie, iron ingots, and pickaxes, not ore and wood. Typically increases an
     * internal count then calls onCrafting(item).
     */
    protected void onCrafting(ItemStack stack, int amount)
    {
        this.amountCrafted += amount;
        this.onCrafting(stack);
    }

    /**
     * the itemStack passed in is the output - ie, iron ingots, and pickaxes, not ore and wood.
     */
    protected void onCrafting(ItemStack stack)
    {
        if (this.amountCrafted > 0)
        {
            stack.onCrafting(this.thePlayer.worldObj, this.thePlayer, this.amountCrafted);
        }

        this.amountCrafted = 0;

        if (stack.getItem() == Item.getItemFromBlock(SpecializationsMod.CRAFTING_TABLE))
        {
            this.thePlayer.hasAchievement(AchievementList.BUILD_WORK_BENCH);
        }

        if (stack.getItem() instanceof ItemPickaxe)
        {
            this.thePlayer.hasAchievement(AchievementList.BUILD_PICKAXE);
        }

        if (stack.getItem() == Item.getItemFromBlock(SpecializationsMod.FURNACE))
        {
            this.thePlayer.hasAchievement(AchievementList.BUILD_FURNACE);
        }

        if (stack.getItem() instanceof ItemHoe)
        {
            this.thePlayer.hasAchievement(AchievementList.BUILD_HOE);
        }

        if (stack.getItem() == FoodInitializer.BREAD)
        {
            this.thePlayer.hasAchievement(AchievementList.MAKE_BREAD);
        }

        if (stack.getItem() == FoodInitializer.CAKE)
        {
            this.thePlayer.hasAchievement(AchievementList.BAKE_CAKE);
        }

        if (stack.getItem() instanceof ItemPickaxe && ((ItemPickaxe)stack.getItem()).getToolMaterial() != Item.ToolMaterial.WOOD)
        {
            this.thePlayer.hasAchievement(AchievementList.BUILD_BETTER_PICKAXE);
        }

        if (stack.getItem() instanceof ItemSword)
        {
            this.thePlayer.hasAchievement(AchievementList.BUILD_SWORD);
        }

        if (stack.getItem() == Item.getItemFromBlock(Blocks.ENCHANTING_TABLE))
        {
            this.thePlayer.hasAchievement(AchievementList.ENCHANTMENTS);
        }

        if (stack.getItem() == Item.getItemFromBlock(Blocks.BOOKSHELF))
        {
            this.thePlayer.hasAchievement(AchievementList.BOOKCASE);
        }

        if (stack.getItem() == Items.GOLDEN_APPLE && stack.getMetadata() == 1)
        {
            this.thePlayer.hasAchievement(AchievementList.OVERPOWERED);
        }
    }    
    
    @Override
    public boolean canTakeStack(EntityPlayer par1EntityPlayer)
    {
    	if(this.bench != null && getStack() != null && getStack().getItem() != null && 
    	(this.bench.getCurCraftTime() >= this.bench.getReqCraftTime() || getStack().getItem() instanceof ItemFood || FMLHandler.getProgressForStack(getStack()) == 0.0f))
    	{
    		return true;
    	}
		return false;
    }

    public void onPickupFromSlot(EntityPlayer playerIn, ItemStack stack)
    {
    	InventoryCrafting craftMatrix = SlotCrafting.getDummyInventory(playerIn, this.bench, playerIn.worldObj);
        FMLCommonHandler.instance().firePlayerCraftingEvent(playerIn, stack, craftMatrix);
        this.onCrafting(stack);
        ForgeHooks.setCraftingPlayer(playerIn);
        ItemStack[] aitemstack = CraftingManager.getInstance().getRemainingItems(craftMatrix, playerIn.worldObj);
        ForgeHooks.setCraftingPlayer(null);
        this.bench.setCurCraftTime(0);
        
        for(int i = 1; i < this.bench.getSizeInventory(); i++)
        {
            ItemStack itemstack1 = this.bench.getStackInSlot(i);

            if (itemstack1 != null)
            {
            	this.bench.decrStackSize(i, 1);
            }
        }

        for (int i = 0; i < aitemstack.length; ++i)
        {
            ItemStack itemstack2 = aitemstack[i];

            if (itemstack2 != null)
            {
                if (this.bench.getStackInSlot(i) == null)
                {
                	this.bench.setInventorySlotContents(i, itemstack2);
                }
                else if (!this.thePlayer.inventory.addItemStackToInventory(itemstack2))
                {
                    this.thePlayer.dropItem(itemstack2, false);
                }
            }
        }
    }
    
    public static InventoryCrafting getDummyInventory(EntityPlayer player, TileEntitySpecWorkbench bench, World world)
    {
    	InventoryCrafting craftMatrix = new InventoryCrafting(new SpecContainerWorkbench(player.inventory, bench, world, null), 3, 3);
    	ItemStack[] stacks = new ItemStack[bench.getSizeInventory() - 1];
    	for(int i = 1; i < bench.getSizeInventory(); i++)
    	{
    		stacks[i-1] = bench.getStackInSlot(i);
    	}
		try
		{
			Field f = craftMatrix.getClass().getDeclaredFields()[0];
			f.setAccessible(true);
			f.set(craftMatrix, stacks);
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
		catch (IllegalAccessException e)
		{
			e.printStackTrace();
		}
    	return craftMatrix;
    }
}
