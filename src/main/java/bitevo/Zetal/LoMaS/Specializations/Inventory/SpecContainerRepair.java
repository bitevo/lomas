package bitevo.Zetal.LoMaS.Specializations.Inventory;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Handler.FMLHandler;
import bitevo.Zetal.LoMaS.Specializations.Inventory.Slot.SlotCore;
import bitevo.Zetal.LoMaS.Specializations.Inventory.Slot.SlotCrafting;
import bitevo.Zetal.LoMaS.Specializations.Item.ItemFood;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntitySpecAnvil;
import net.minecraft.block.BlockAnvil;
import net.minecraft.block.state.IBlockState;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ContainerRepair;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.inventory.InventoryCraftResult;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.IInteractionObject;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class SpecContainerRepair extends ContainerRepair
{
	private static final Logger logger = LogManager.getLogger();
	/** Here comes out item you merged and/or renamed. */
	public IInventory outputSlot;
	/** The 2slots where you put your items in that you want to merge and/or rename. */
	public IInventory inputSlots;
	public IInventory coreSlots = new InventoryRepairCore(this, "RepairCore", true, 2);
	private World theWorld;
	private BlockPos selfPosition;
	/** The maximum cost of repairing/renaming in the anvil. */
	public int maximumCost;
	/** determined by damage of input item and stackSize of repair materials */
	public int materialCost;
	private String repairedItemName;
	/** The player that has this container open. */
	private final EntityPlayer thePlayer;
	private TileEntitySpecAnvil tileAnvil;

	@SideOnly(Side.CLIENT)
	public SpecContainerRepair(InventoryPlayer playerInventory, World worldIn, EntityPlayer player, TileEntitySpecAnvil anvil)
	{
		this(playerInventory, worldIn, BlockPos.ORIGIN, player, anvil);
	}

	public SpecContainerRepair(InventoryPlayer playerInventory, final World worldIn, final BlockPos blockPosIn, EntityPlayer player, TileEntitySpecAnvil anvil)
	{
		super(playerInventory, worldIn, blockPosIn, player);
		this.tileAnvil = anvil;
		this.inventoryItemStacks = Lists.<ItemStack>newArrayList();
	    this.inventorySlots = Lists.<Slot>newArrayList();
		this.outputSlot = new InventoryCraftResult();
		this.inputSlots = new InventoryBasic("Repair", true, 2)
		{
			/**
			 * For tile entities, ensures the chunk containing the tile entity is saved to disk later - the game won't think it hasn't changed and skip it.
			 */
			@Override
			public void markDirty()
			{
				super.markDirty();
				SpecContainerRepair.this.onCraftMatrixChanged(this);
			}
		};
		this.selfPosition = blockPosIn;
		this.theWorld = worldIn;
		this.thePlayer = player;
		this.addSlotToContainer(new Slot(this.inputSlots, 0, 27, 30));
		this.addSlotToContainer(new Slot(this.inputSlots, 1, 76, 30));
		this.addSlotToContainer(new Slot(this.outputSlot, 2, 134, 30)
		{
			private SpecContainerRepair anvil = SpecContainerRepair.this;

			/**
			 * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
			 */
			@Override
			public boolean isItemValid(ItemStack stack)
			{
				return false;
			}

			/**
			 * Return whether this slot's stack can be taken from this slot.
			 */
			@Override
			public boolean canTakeStack(EntityPlayer playerIn)
			{
				boolean canAfford = this.anvil.canAfford(playerIn);

				if (this.anvil.tileAnvil != null && this.getStack() != null && this.getStack().getItem() != null && this.anvil.tileAnvil.getCurCraftTime() >= this.anvil.tileAnvil.getReqCraftTime())
				{
					return canAfford;
				}
				
				return false;
			}

			@Override
			public void onPickupFromSlot(EntityPlayer playerIn, ItemStack stack)
			{
				this.anvil.tileAnvil.setCurCraftTime(0);
				if (!playerIn.capabilities.isCreativeMode)
				{
					int[] coreCosts = this.anvil.getCoreCost();
					anvil.coreSlots.decrStackSize(0, coreCosts[1]);
					anvil.coreSlots.decrStackSize(1, coreCosts[0]);
					// playerIn.addExperienceLevel(-SpecContainerRepair.this.maximumCost);
				}

				float breakChance = net.minecraftforge.common.ForgeHooks.onAnvilRepair(playerIn, stack, SpecContainerRepair.this.inputSlots.getStackInSlot(0), SpecContainerRepair.this.inputSlots.getStackInSlot(1));

				SpecContainerRepair.this.inputSlots.setInventorySlotContents(0, (ItemStack) null);

				if (SpecContainerRepair.this.materialCost > 0)
				{
					ItemStack itemstack1 = SpecContainerRepair.this.inputSlots.getStackInSlot(1);

					if (itemstack1 != null && itemstack1.stackSize > SpecContainerRepair.this.materialCost)
					{
						itemstack1.stackSize -= SpecContainerRepair.this.materialCost;
						SpecContainerRepair.this.inputSlots.setInventorySlotContents(1, itemstack1);
					}
					else
					{
						SpecContainerRepair.this.inputSlots.setInventorySlotContents(1, (ItemStack) null);
					}
				}
				else
				{
					SpecContainerRepair.this.inputSlots.setInventorySlotContents(1, (ItemStack) null);
				}

				SpecContainerRepair.this.maximumCost = 0;
				IBlockState iblockstate = worldIn.getBlockState(blockPosIn);

				if (!playerIn.capabilities.isCreativeMode && !worldIn.isRemote && iblockstate.getBlock() == Blocks.ANVIL && playerIn.getRNG().nextFloat() < breakChance)
				{
					int l = ((Integer) iblockstate.getValue(BlockAnvil.DAMAGE)).intValue();
					++l;

					if (l > 2)
					{
						worldIn.setBlockToAir(blockPosIn);
						worldIn.playEvent(1029, blockPosIn, 0);
					}
					else
					{
						worldIn.setBlockState(blockPosIn, iblockstate.withProperty(BlockAnvil.DAMAGE, Integer.valueOf(l)), 2);
						worldIn.playEvent(1030, blockPosIn, 0);
					}
				}
				else if (!worldIn.isRemote)
				{
					worldIn.playEvent(1030, blockPosIn, 0);
				}

				// ///////////SPECIALIZATIONS
				float progressEarned = 0;
				progressEarned = (this.anvil.maximumCost * 5) + 10.0f;

				LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(playerIn);
				if (!playerIn.capabilities.isCreativeMode)
				{
					lplayer.addSpecProgress(progressEarned);
					playerIn.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_TOUCH, 0.1F, 0.5F * ((playerIn.worldObj.rand.nextFloat() - playerIn.worldObj.rand.nextFloat()) * 0.7F + 1.8F));
				}
			}
		});
		this.addSlotToContainer(new SlotCore(this, this.coreSlots, 0, 94, 51, theWorld, blockPosIn));
		this.addSlotToContainer(new SlotCore(this, this.coreSlots, 1, 116, 51, theWorld, blockPosIn));
		int i;

		for (i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 9; ++j)
			{
				this.addSlotToContainer(new Slot(playerInventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
			}
		}

		for (i = 0; i < 9; ++i)
		{
			this.addSlotToContainer(new Slot(playerInventory, i, 8 + i * 18, 142));
		}
	}
	
	/**
	 * 0 is iron, 1 is emerald
	 * @return
	 */
	public int[] getCoreCost()
	{
		int cost = this.maximumCost;
		float magMod = 1.0f;
		this.tileAnvil.setOwner(this.thePlayer.getUniqueID());
		magMod -= this.tileAnvil.getlPlayer().getSkills().getPercentMagnitudeFromName("Anvil Cost");
		cost *= magMod;
		int ironCost = cost % 10;
		int emeraldCost = (int) Math.floor(cost / 10);
		return new int[]{ironCost, emeraldCost};
	}

	public boolean canAfford(EntityPlayer player)
	{
		int emeraldCores = this.coreSlots.getStackInSlot(0) != null ? this.coreSlots.getStackInSlot(0).stackSize : 0;
		int ironCores = this.coreSlots.getStackInSlot(1) != null ? this.coreSlots.getStackInSlot(1).stackSize : 0;
		int ironCost = this.getCoreCost()[0];
		int emeraldCost = this.getCoreCost()[1];
		return (player.capabilities.isCreativeMode || (ironCost <= ironCores && emeraldCost <= emeraldCores)) && this.maximumCost > 0 && this.getSlot(2).getHasStack();
	}

	/**
	 * Callback for when the crafting matrix is changed.
	 */
	@Override
	public void onCraftMatrixChanged(IInventory inventoryIn)
	{
		super.onCraftMatrixChanged(inventoryIn);

		if (inventoryIn == this.inputSlots)
		{
			this.updateRepairOutput();
		}
	}

	@Override
	public ItemStack slotClick(int slotId, int clickedButton, ClickType mode, EntityPlayer playerIn)
	{
		// If it's the output slot...
		if (slotId == 2)
		{
			Slot slotClicked = this.getSlot(slotId);
			if (this.tileAnvil.getCurCraftTime() == 0)
			{
				ItemStack stack = slotClicked.getStack();
				if (stack != null && stack.getItem() != null)
				{
					this.tileAnvil.setCurCraftTime(1);
				}
			}
		}
		return super.slotClick(slotId, clickedButton, mode, playerIn);
	}

    /**
     * called when the Anvil Input Slot changes, calculates the new result and puts it in the output slot
     */
    public void updateRepairOutput()
    {
        ItemStack itemstack = this.inputSlots.getStackInSlot(0);
        this.maximumCost = 1;
        int i = 0;
        int j = 0;
        int k = 0;

        if (itemstack == null)
        {
            this.outputSlot.setInventorySlotContents(0, (ItemStack)null);
            this.maximumCost = 0;
        }
        else
        {
            ItemStack itemstack1 = itemstack.copy();
            ItemStack itemstack2 = this.inputSlots.getStackInSlot(1);
            Map<Enchantment, Integer> map = EnchantmentHelper.getEnchantments(itemstack1);
            j = j + itemstack.getRepairCost() + (itemstack2 == null ? 0 : itemstack2.getRepairCost());
            this.materialCost = 0;
            boolean flag = false;

            if (itemstack2 != null)
            {
                // if (!net.minecraftforge.common.ForgeHooks.onAnvilChange(this, itemstack, itemstack2, outputSlot, repairedItemName, j)) return;
                flag = itemstack2.getItem() == Items.ENCHANTED_BOOK && !Items.ENCHANTED_BOOK.getEnchantments(itemstack2).hasNoTags();

                if (itemstack1.isItemStackDamageable() && itemstack1.getItem().getIsRepairable(itemstack, itemstack2))
                {
                    int j2 = Math.min(itemstack1.getItemDamage(), itemstack1.getMaxDamage() / 4);

                    if (j2 <= 0)
                    {
                        this.outputSlot.setInventorySlotContents(0, (ItemStack)null);
                        this.maximumCost = 0;
                        return;
                    }

                    int k2;

                    for (k2 = 0; j2 > 0 && k2 < itemstack2.stackSize; ++k2)
                    {
                        int l2 = itemstack1.getItemDamage() - j2;
                        itemstack1.setItemDamage(l2);
                        ++i;
                        j2 = Math.min(itemstack1.getItemDamage(), itemstack1.getMaxDamage() / 4);
                    }

                    this.materialCost = k2;
                }
                else
                {
                    if (!flag && (itemstack1.getItem() != itemstack2.getItem() || !itemstack1.isItemStackDamageable()))
                    {
                        this.outputSlot.setInventorySlotContents(0, (ItemStack)null);
                        this.maximumCost = 0;
                        return;
                    }

                    if (itemstack1.isItemStackDamageable() && !flag)
                    {
                        int l = itemstack.getMaxDamage() - itemstack.getItemDamage();
                        int i1 = itemstack2.getMaxDamage() - itemstack2.getItemDamage();
                        int j1 = i1 + itemstack1.getMaxDamage() * 12 / 100;
                        int k1 = l + j1;
                        int l1 = itemstack1.getMaxDamage() - k1;

                        if (l1 < 0)
                        {
                            l1 = 0;
                        }

                        if (l1 < itemstack1.getMetadata())
                        {
                            itemstack1.setItemDamage(l1);
                            i += 2;
                        }
                    }

                    Map<Enchantment, Integer> map1 = EnchantmentHelper.getEnchantments(itemstack2);

                    for (Enchantment enchantment1 : map1.keySet())
                    {
                        if (enchantment1 != null)
                        {
                            int i3 = map.containsKey(enchantment1) ? ((Integer)map.get(enchantment1)).intValue() : 0;
                            int j3 = ((Integer)map1.get(enchantment1)).intValue();
                            j3 = i3 == j3 ? j3 + 1 : Math.max(j3, i3);
                            boolean flag1 = enchantment1.canApply(itemstack);

                            if (this.thePlayer.capabilities.isCreativeMode || itemstack.getItem() == Items.ENCHANTED_BOOK)
                            {
                                flag1 = true;
                            }

                            for (Enchantment enchantment : map.keySet())
                            {
                                if (enchantment != enchantment1 && !(enchantment1.canApplyTogether(enchantment) && enchantment.canApplyTogether(enchantment1)))  //Forge BugFix: Let Both enchantments veto being together
                                {
                                    flag1 = false;
                                    ++i;
                                }
                            }

                            if (flag1)
                            {
                                if (j3 > enchantment1.getMaxLevel())
                                {
                                    j3 = enchantment1.getMaxLevel();
                                }

                                map.put(enchantment1, Integer.valueOf(j3));
                                int k3 = 0;

                                switch (enchantment1.getRarity())
                                {
                                    case COMMON:
                                        k3 = 1;
                                        break;
                                    case UNCOMMON:
                                        k3 = 2;
                                        break;
                                    case RARE:
                                        k3 = 4;
                                        break;
                                    case VERY_RARE:
                                        k3 = 8;
                                }

                                if (flag)
                                {
                                    k3 = Math.max(1, k3 / 2);
                                }

                                i += k3 * j3;
                            }
                        }
                    }
                }
            }

            if (flag && !itemstack1.getItem().isBookEnchantable(itemstack1, itemstack2)) itemstack1 = null;

            if (StringUtils.isBlank(this.repairedItemName))
            {
                if (itemstack.hasDisplayName())
                {
                    k = 1;
                    i += k;
                    itemstack1.clearCustomName();
                }
            }
            else if (!this.repairedItemName.equals(itemstack.getDisplayName()))
            {
                k = 1;
                i += k;
                itemstack1.setStackDisplayName(this.repairedItemName);
            }

            this.maximumCost = j + i;

            if (i <= 0)
            {
                itemstack1 = null;
            }

            if (k == i && k > 0 && this.maximumCost >= 40)
            {
                this.maximumCost = 39;
            }

            if (this.maximumCost >= 40 && !this.thePlayer.capabilities.isCreativeMode)
            {
                itemstack1 = null;
            }

            if (itemstack1 != null)
            {
                int i2 = itemstack1.getRepairCost();

                if (itemstack2 != null && i2 < itemstack2.getRepairCost())
                {
                    i2 = itemstack2.getRepairCost();
                }

                if (k != i || k == 0)
                {
                    i2 = i2 * 2 + 1;
                }

                itemstack1.setRepairCost(i2);
                EnchantmentHelper.setEnchantments(map, itemstack1);
            }
            
            /**
             * SetReqTime
             */
            this.tileAnvil.setReqCraftTime(120);
			float magMod = 1.0f;
			this.tileAnvil.setOwner(this.thePlayer.getUniqueID());
			magMod -= this.tileAnvil.getlPlayer().getSkills().getPercentMagnitudeFromName("Anvil spd");
			this.tileAnvil.setReqCraftTime((int) (this.tileAnvil.getReqCraftTime() * magMod));
			this.tileAnvil.setCurCraftTime(0);
			
            this.outputSlot.setInventorySlotContents(0, itemstack1);
            this.detectAndSendChanges();
        }
    }

	/**
	 * Add the given Listener to the list of Listeners. Method name is for legacy.
	 */
	@Override
	public void addListener(IContainerListener listener)
	{
		super.addListener(listener);
		listener.sendProgressBarUpdate(this, 0, this.maximumCost);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int id, int data)
	{
		if (id == 0)
		{
			this.maximumCost = data;
		}
	}

	/**
	 * Called when the container is closed.
	 */
	@Override
	public void onContainerClosed(EntityPlayer playerIn)
	{
		super.onContainerClosed(playerIn);

		if (!this.theWorld.isRemote)
		{
			for (int i = 0; i < this.inputSlots.getSizeInventory(); ++i)
			{
				ItemStack itemstack = this.inputSlots.removeStackFromSlot(i);

				if (itemstack != null)
				{
					playerIn.dropItem(itemstack, false);
				}
			}

			for (int i = 0; i < this.coreSlots.getSizeInventory(); ++i)
			{
				ItemStack itemstack = this.coreSlots.removeStackFromSlot(i);

				if (itemstack != null)
				{
					playerIn.dropItem(itemstack, false);
				}
			}
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn)
	{
		return !(this.theWorld.getBlockState(this.selfPosition).getBlock() instanceof BlockAnvil) ? false : playerIn.getDistanceSq((double) this.selfPosition.getX() + 0.5D, (double) this.selfPosition.getY() + 0.5D, (double) this.selfPosition.getZ() + 0.5D) <= 64.0D;
	}

	/**
	 * Take a stack from the specified inventory slot.
	 */
	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index)
	{
		ItemStack itemstack = null;
		Slot slot = (Slot) this.inventorySlots.get(index);

		if (slot != null && slot.getHasStack())
		{
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (index == 2)
			{
				if (!this.mergeItemStack(itemstack1, 5, 41, true))
				{
					return null;
				}

				slot.onSlotChange(itemstack1, itemstack);
			}
			else if (index != 0 && index != 1 && index != 3 && index != 4)
			{
				if (index >= 5 && index < 41 && (itemstack1.getItem().equals(SpecializationsMod.EMERALDHEART)))
				{
					if ((!this.mergeItemStack(itemstack1, 3, 4, true)))
					{
						return null;
					}
				}
				else if (index >= 5 && index < 41 && (itemstack1.getItem().equals(SpecializationsMod.IRONHEART)))
				{
					if ((!this.mergeItemStack(itemstack1, 4, 5, true)))
					{
						return null;
					}
				}
				else if (index >= 5 && index < 41 && !this.mergeItemStack(itemstack1, 0, 2, false))
				{
					return null;
				}
			}
			else if (!this.mergeItemStack(itemstack1, 5, 41, false))
			{
				return null;
			}

			if (itemstack1.stackSize == 0)
			{
				slot.putStack((ItemStack) null);
			}
			else
			{
				slot.onSlotChanged();
			}

			if (itemstack1.stackSize == itemstack.stackSize)
			{
				return null;
			}

			slot.onPickupFromSlot(playerIn, itemstack1);
		}

		return itemstack;
	}

	/**
	 * used by the Anvil GUI to update the Item Name being typed by the player
	 */
    public void updateItemName(String newName)
    {
        this.repairedItemName = newName;

        if (this.getSlot(2).getHasStack())
        {
            ItemStack itemstack = this.getSlot(2).getStack();

            if (StringUtils.isBlank(newName))
            {
                itemstack.clearCustomName();
            }
            else
            {
                itemstack.setStackDisplayName(this.repairedItemName);
            }
        }

        this.updateRepairOutput();
    }
}
