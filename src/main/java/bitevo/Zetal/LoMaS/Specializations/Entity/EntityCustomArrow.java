package bitevo.Zetal.LoMaS.Specializations.Entity;

import java.util.ArrayList;
import java.util.Set;

import com.google.common.collect.Sets;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityTippedArrow;
import net.minecraft.init.PotionTypes;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.potion.PotionEffect;
import net.minecraft.potion.PotionType;
import net.minecraft.potion.PotionUtils;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.ReflectionHelper;

public class EntityCustomArrow extends EntityTippedArrow
{
	/*
	 * Arrow Types: 0 = Wooden 1 = Stone 2 = Iron 3 = Gold 4 = Diamond 5 = Obsidian
	 */
	///// SPECIALIZATIONS
	public int arrowType = -1;
	private double damage;
	private static final DataParameter<Integer> COLOR = EntityDataManager.<Integer> createKey(EntityTippedArrow.class, DataSerializers.VARINT);
	private PotionType potion = PotionTypes.EMPTY;
	private final Set<PotionEffect> customPotionEffects = Sets.<PotionEffect> newHashSet();

	public EntityCustomArrow(World worldIn)
	{
		super(worldIn);
	}

	public EntityCustomArrow(World worldIn, double x, double y, double z, int arrowType)
	{
		super(worldIn, x, y, z);
		// Wood = 0.5
		// Stone = 1.0;
		// Iron = 1.5;
		// Gold = 0.5;
		// Diamond = 2.5;
		this.arrowType = arrowType;
		int arrowMod = 0;
		switch (this.arrowType)
		{
			case 0:
				arrowMod = 0;
				break;
			case 1:
				arrowMod = 1;
				break;
			case 2:
				arrowMod = 2;
				break;
			case 3:
				arrowMod = 0;
				break;
			case 4:
				arrowMod = 4;
				break;
		}
		this.damage = (this.damage * arrowMod) + this.damage;
	}

	public EntityCustomArrow(World worldIn, EntityLivingBase shooter, int arrowType)
	{
		super(worldIn, shooter);
		// Wood = 0.5
		// Stone = 1.0;
		// Iron = 1.5;
		// Gold = 0.5;
		// Diamond = 2.5;
		this.arrowType = arrowType;
		int arrowMod = 0;
		switch (this.arrowType)
		{
			case 0:
				arrowMod = 0;
				break;
			case 1:
				arrowMod = 1;
				break;
			case 2:
				arrowMod = 2;
				break;
			case 3:
				arrowMod = 0;
				break;
			case 4:
				arrowMod = 4;
				break;
		}
		this.damage = (this.damage * arrowMod) + this.damage;
	}

	@Override
	protected void entityInit()
	{
		super.entityInit();
	}

	@Override
	protected void arrowHit(EntityLivingBase living)
	{
		EntityLivingBase entityhit = (EntityLivingBase) living;
		float f = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionY * this.motionY + this.motionZ * this.motionZ);
		int i = MathHelper.ceiling_double_int((double) f * this.getDamage());
		if (this.shootingEntity != null && this.shootingEntity instanceof EntityPlayer)
		{
			EntityPlayer player = ((EntityPlayer) this.shootingEntity);
			LoMaS_Player lPlayer = LoMaS_Player.getLoMaSPlayer(player);
			if(lPlayer != null)
			{
				/**
				 * Poison Skills
				 */
				PotionType potionType = this.potion;
				PotionEffect[] newEffects = new PotionEffect[0];
				for(PotionEffect effect : potionType.getEffects())
				{
					newEffects = new PotionEffect[newEffects.length + 1];
					if(effect.getPotion().isBadEffect())
					{
						int ampMod = lPlayer.getSkills().getPercentMagnitudeFromName("Arrow Poison Effect") >= rand.nextInt(100) ? 1 : 0;
						float durMod = 1.0f + lPlayer.getSkills().getPercentMagnitudeFromName("Arrow Poison duration");
						effect = new PotionEffect(effect.getPotion(), (int) (effect.getDuration() * durMod), effect.getAmplifier() + ampMod, effect.getIsAmbient(), effect.doesShowParticles());
					}
					newEffects[newEffects.length - 1] = effect;
				}
				this.potion = new PotionType((String) ReflectionHelper.getPrivateValue(PotionType.class, potionType, 3), newEffects);
			}
		}
		
		super.arrowHit(living);

		if ((this.isBurning() || this.arrowType == 3) && !(living instanceof EntityEnderman))
		{
			living.setFire(5);
		}
	}

	@Override
	public ItemStack getArrowStack()
	{
		if (this.customPotionEffects.isEmpty() && this.potion == PotionTypes.EMPTY)
		{
			return new ItemStack(SpecializationsMod.ARROW, 1, this.arrowType);
		}
		else
		{
			ItemStack itemstack = new ItemStack(SpecializationsMod.ARROW, 1, this.arrowType);
			PotionUtils.addPotionToItemStack(itemstack, this.potion);
			PotionUtils.appendEffects(itemstack, this.customPotionEffects);
			return itemstack;
		}
	}

	@Override
	public void writeEntityToNBT(NBTTagCompound compound)
	{
		super.writeEntityToNBT(compound);
		///// SPECIALIZATIONS
		compound.setByte("arrowType", (byte) this.arrowType);
	}

	@Override
	public void readEntityFromNBT(NBTTagCompound compound)
	{
		super.readEntityFromNBT(compound);
		///// SPECIALIZATIONS
		this.arrowType = compound.getByte("arrowType");
	}
}
