package bitevo.Zetal.LoMaS.Specializations.Message;

import io.netty.buffer.ByteBuf;

import java.util.UUID;

import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class TimedUsageMessage implements IMessage
{
	public UUID username;
	public BlockPos pos;
	public int stack;
	
	public TimedUsageMessage(){}

	public TimedUsageMessage(UUID username, BlockPos pos, int stack)
	{
		this.username = username;
		this.pos = pos;
		this.stack = stack;
	}
	
	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.username = UUID.fromString(ByteBufUtils.readUTF8String(buf));
		this.pos = new BlockPos(buf.readInt(), buf.readInt(), buf.readInt());
		this.stack = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, username.toString());
		buf.writeInt(pos.getX());
		buf.writeInt(pos.getY());
		buf.writeInt(pos.getZ());
		buf.writeInt(stack);
	}
}
