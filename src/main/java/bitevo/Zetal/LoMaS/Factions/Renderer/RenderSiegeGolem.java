package bitevo.Zetal.LoMaS.Factions.Renderer;

import bitevo.Zetal.LoMaS.Factions.FactionsMod;
import bitevo.Zetal.LoMaS.Factions.Entity.EntitySiegeGolem;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;

public class RenderSiegeGolem extends RenderLiving<EntitySiegeGolem>
{
	private static final ResourceLocation GOLEM_TEXTURES = new ResourceLocation(FactionsMod.modid, "textures/entity/siege_golem.png");

	public RenderSiegeGolem(RenderManager renderManagerIn, ModelBase modelBaseIn, float shadowSizeIn)
	{
		super(renderManagerIn, modelBaseIn, shadowSizeIn);
	}

	@Override
	protected ResourceLocation getEntityTexture(EntitySiegeGolem entity)
	{
		return GOLEM_TEXTURES;
	}
}
