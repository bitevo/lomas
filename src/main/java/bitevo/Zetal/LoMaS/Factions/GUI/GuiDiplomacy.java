package bitevo.Zetal.LoMaS.Factions.GUI;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import bitevo.Zetal.LoMaS.Factions.FactionsMod;
import bitevo.Zetal.LoMaS.Factions.Handler.FactionKeyBinds;
import bitevo.Zetal.LoMaS.Factions.Message.FactionMessage;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Faction;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiOptionButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiDiplomacy extends GuiScreen
{
	private GuiTextField inputField;
	private static EntityPlayer thePlayer;
    private GuiScreen parentScreen;
	ResourceLocation texture = new ResourceLocation(FactionsMod.modid, "gui/faction_gui.png");
	public LoMaS_Faction faction;
	public boolean inFaction = false;
	public boolean isLeader = false;
	public boolean isOwner = false;

	private boolean isConfirming = false;
	
	public HashMap<Integer, UUID> factionMap;

	public GuiDiplomacy(GuiScreen parentScreenIn, EntityPlayer player)
	{
		super();
		this.thePlayer = player;
		this.updateFactionStatus();
        this.parentScreen = parentScreenIn;
    }

	public void updateFactionStatus()
	{
		LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(thePlayer);
		if (lplayer != null)
		{
			this.faction = LoMaS_Faction.getLoMaSFaction(lplayer.faction);
			if (faction != null)
			{
				this.inFaction = true;
				if (faction.getLeaders().contains(lplayer.uuid))
				{
					this.isLeader = true;
					if (faction.getOwner().equals(lplayer.uuid))
					{
						this.isOwner = true;
					}
				}
			}
		}
	}

	@Override
	public void initGui()
	{
		ScaledResolution res = new ScaledResolution(this.mc);
		int width = res.getScaledWidth();
		int height = res.getScaledHeight();
		int k = (width - 256) / 2;
		int l = (height - 227) / 2;
		this.buttonList.clear();
		this.updateFactionStatus();

		if (!isConfirming)
		{
			if (inFaction)
			{
				for(int i = 0; i < this.faction.getMembers().size(); i++)
				{
					GuiOptionButton memberbutton = new GuiOptionButton(20 + i, k + 14, l + 24 + (12 * i), 12, 12, "");
					memberbutton.enabled = true;
					memberbutton.displayString = "";
					this.buttonList.add(memberbutton);
				}
			}
			this.buttonList.add(new GuiOptionButton(0, k + 167, l + 167, 80, 20, I18n.format("gui.done", new Object[0])));
		}
		else if (isConfirming)
		{
			this.buttonList.add(new GuiOptionButton(9, this.width / 2 - 155, this.height / 6 + 86, "I'm sure!"));
			this.buttonList.add(new GuiOptionButton(10, this.width / 2 - 155 + 160, this.height / 6 + 86, "Nevermind"));
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		this.drawGuiContainerBackgroundLayer(partialTicks, mouseX, mouseY);
		super.drawScreen(mouseX, mouseY, partialTicks);
		this.drawGuiContainerForegroundLayer(mouseX, mouseY);
	}

	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		ScaledResolution res = new ScaledResolution(this.mc);
		int width = res.getScaledWidth();
		int height = res.getScaledHeight();
		int k = (width - 256) / 2;
		int l = (height - 227) / 2;

		if (!isConfirming)
		{
			if (inFaction)
			{
				this.fontRendererObj.drawString("Factions", k + 32, l + 12, 4210752);
				for(int i = 0; i < LoMaS_Faction.getFactionList().size(); i++)
				{
					this.fontRendererObj.drawString(LoMaS_Faction.getFactionList().get(i).getName(), k + 30, l + 26 + (12 * i), 4210752);
				}
			}
		}
		else if (isConfirming)
		{
			this.drawCenteredString(this.fontRendererObj, "Are you sure?", this.width / 2, 70, 16777215);
			this.drawCenteredString(this.fontRendererObj, "This is permanent!", this.width / 2, 90, 16777215);
		}
	}

	/**
	 * Draw the background layer for the GuiContainer (everything behind the items)
	 */
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(texture);
		ScaledResolution res = new ScaledResolution(this.mc);
		int width = res.getScaledWidth();
		int height = res.getScaledHeight();
		int k = (width - 256) / 2;
		int l = (height - 227) / 2;

		if (!isConfirming)
		{
			this.drawTexturedModalRect(k, l, 0, 0, 256, 197);
		}
		else if (isConfirming)
		{
			super.drawDefaultBackground();
		}
	}

	@Override
	protected void actionPerformed(GuiButton button)
	{
		// 
		if (button.id == 0)
		{
			this.mc.displayGuiScreen(this.parentScreen);
		}
		// 
		else if (button.id == 1)
		{
		}
		// Confirm Yes
		else if (button.id == 9)
		{
			System.out.println("Yes");
			this.isConfirming = false;
			this.initGui();
		}
		// Confirm Cancel
		else if (button.id == 10)
		{
			System.out.println("Cancel");
			this.isConfirming = false;
			this.initGui();
		}
		// Member modifier button
		else if (button.id >= 20)
		{
			System.out.println("Member button");
		}
	}

	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);
		if (this.inputField != null)
		{
			this.inputField.mouseClicked(mouseX, mouseY, mouseButton);
		}
	}

	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		if (this.inputField == null || !this.inputField.textboxKeyTyped(typedChar, keyCode))
		{
			if (keyCode == 1 || keyCode == this.mc.gameSettings.keyBindInventory.getKeyCode() || keyCode == FactionKeyBinds.keys[0].getKeyCode())
			{
				this.mc.thePlayer.closeScreen();
			}
		}
	}
	
	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
}
