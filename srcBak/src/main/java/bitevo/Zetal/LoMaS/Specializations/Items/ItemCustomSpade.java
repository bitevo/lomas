package bitevo.Zetal.LoMaS.Specializations.Items;

import java.util.List;
import java.util.Set;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import bitevo.Zetal.LoMaS.Specializations.FoodInitializer;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;

import com.google.common.collect.Sets;

public class ItemCustomSpade extends ItemSpade
{
	public ItemCustomSpade(Item.ToolMaterial material)
	{
		super(material);
	}

	@Override
	public int getMaxDamage(ItemStack stack)
	{
		return SpecializationsMod.getMaxDamage(stack);
	}

	// adds 'tooltip' text
	@SideOnly(Side.CLIENT)
	@SuppressWarnings("unchecked")
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List tooltip, boolean advanced)
	{
		SpecializationsMod.addInformation(stack, playerIn, tooltip, advanced);
	}
}
