package bitevo.Zetal.LoMaS.Economy.Item;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemCoin extends Item
{
	private int coinValue;
	
	public ItemCoin(int val)
	{
		super();
		this.coinValue = val;
		this.maxStackSize = 64;
		this.setCreativeTab(LoMaS_Utils.tabLoMaS);
	}

	public int getCoinValue()
	{
		return coinValue;
	}
}
