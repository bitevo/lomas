package bitevo.Zetal.LoMaS.Economy.Inventory;

import bitevo.Zetal.LoMaS.Economy.Item.ItemCoin;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotCustom extends Slot
{
	public SlotCustom(IInventory inventoryIn, int index, int xPosition, int yPosition)
	{
		super(inventoryIn, index, xPosition, yPosition);
	}

	@Override
	public boolean isItemValid(ItemStack stack)
	{
		if (stack.getItem() instanceof ItemCoin)
		{
			return false;
		}
		return true;
	}
}
