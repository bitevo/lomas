package bitevo.Zetal.LoMaS.TileWrappers.Message;

import java.lang.reflect.InvocationTargetException;

import bitevo.Zetal.LoMaS.TileWrappers.TileWrapper.TileWrapper;
import bitevo.Zetal.LoMaS.TileWrappers.TileWrapper.TileWrapperContainer;
import net.minecraft.client.Minecraft;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileWrapperMessageHandler implements IMessageHandler<TileWrapperMessage, IMessage>
{

	@Override
	@SideOnly(Side.CLIENT)
	public IMessage onMessage(TileWrapperMessage message, MessageContext ctx)
	{
		if (ctx.side == Side.CLIENT)
		{
	        if (Minecraft.getMinecraft().theWorld.isBlockLoaded(message.getPos()))
	        {
	            TileWrapper tilewrapper = TileWrapperContainer.instance.getTileWrapper(message.getPos(), message.getIndex());
	            if(tilewrapper == null)
	            {
	            	try
					{
						tilewrapper = message.getWrapperClass().getConstructor(String.class, World.class, BlockPos.class, EnumFacing.class).newInstance(null, Minecraft.getMinecraft().theWorld, message.getPos(), null);
		            	TileWrapperContainer.instance.setTileWrapper(message.getPos(), tilewrapper);
					}
					catch (Exception e)
					{
						System.err.println("Error receiving a TileWrapper message! Could not create a TileWrapper using class: " + message.getWrapperClass());
						e.printStackTrace();
					}
	            }
	            
	            if(tilewrapper != null)
	            {
		            tilewrapper.onUpdatePacket(message);
	            }
	        }
		}
		return null;
	}

}
