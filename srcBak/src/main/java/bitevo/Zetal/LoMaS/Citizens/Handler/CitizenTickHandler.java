package bitevo.Zetal.LoMaS.Citizens.Handler;

import java.util.ArrayList;
import java.util.UUID;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.WorldTickEvent;
import net.minecraftforge.fml.relauncher.Side;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Citizens.CitizensMod;
import bitevo.Zetal.LoMaS.Citizens.Entities.EntityPigman;
import bitevo.Zetal.LoMaS.Citizens.TileEntity.TileEntityCitizenChest;
import bitevo.Zetal.LoMaS.Economy.EconomyMod;
import bitevo.Zetal.LoMaS.Economy.Message.BankMessage;

public class CitizenTickHandler
{
	public int ticker = 0;
	public static final int tickFinal = 1200;
	
	@SubscribeEvent
	public void serverTickEnd(WorldTickEvent event)
	{
		if (event.side == Side.SERVER)
		{
			if (event.phase == TickEvent.Phase.END)
			{
				if(ticker % 1440 == 0)
				{
					WorldServer world = (WorldServer) event.world;
					
					for(LoMaS_Player lplayer : LoMaS_Player.playerList)
					{
						if(lplayer.citizens == null)
						{
							lplayer.citizens = new ArrayList();
						}
						for (UUID u : lplayer.citizens)
						{
							Entity ent = world.getMinecraftServer().getEntityFromUuid(u);
							if(ent instanceof EntityPigman)
							{
								EntityPigman cit = (EntityPigman) ent;
								if(!cit.getJob().equalsIgnoreCase("None") && cit.getHomeChest() != null && world.getTileEntity(cit.getHomeChest()) instanceof TileEntityCitizenChest)
								{
									cit.incomeTicks = cit.incomeTicks + 1;
									//System.out.println("income tick");
									if (cit.incomeTicks >= tickFinal && cit.worldObj.getTileEntity(cit.getHomeChest()) instanceof TileEntityCitizenChest)
									{
										cit.incomeTicks = 0;
										TileEntityCitizenChest home = (TileEntityCitizenChest) cit.worldObj.getTileEntity(cit.getHomeChest());
										ItemStack product = cit.getStackForJob(cit.getJob());
										if (product != null)
										{
											home.addItemStackToInventory(product);
										}
										int income = cit.getIncomeForJob(cit.getJob());
										//System.out.println("yay income2 " + world.getTileEntity(cit.getHomeChest()));
										lplayer.setBankCC(lplayer.getBankCC() + income);
										UUID uuid = ((TileEntityCitizenChest)world.getTileEntity(cit.getHomeChest())).owner;
										if(income > 0 && uuid != null && world.getPlayerEntityByUUID(uuid) != null)
										{
											EconomyMod.snw.sendTo(new BankMessage(lplayer.getBankCC()), (EntityPlayerMP) world.getPlayerEntityByUUID(uuid));
											world.getPlayerEntityByUUID(uuid).addChatMessage(new TextComponentTranslation("You received income from a citizen! They paid you " + income + " in taxes!"));
										}
									}
								}
								else
								{
									//System.out.println(cit.getJob() + " " + cit.getHomeChest() + " " + world.getTileEntity(cit.getHomeChest()));
								}
							}
						}
					}
					ticker = 0;
				}
				ticker++;
			}
		}
	}
}
