package bitevo.Zetal.LoMaS.Citizens.Entities.ai;

import java.util.Random;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import bitevo.Zetal.LoMaS.Citizens.Entities.EntityPigman;

public class EntityAIGoOutside extends EntityAIBase
{
    private EntityCreature theCreature;
    private double sunX;
    private double sunY;
    private double sunZ;
	public int ticks = 0;
    private double movementSpeed;
    private World theWorld;

    public EntityAIGoOutside(EntityCreature p_i1623_1_, double p_i1623_2_)
    {
        this.theCreature = p_i1623_1_;
        this.movementSpeed = p_i1623_2_;
        this.theWorld = p_i1623_1_.worldObj;
        this.setMutexBits(1);
    }

    /**
     * Returns whether the EntityAIBase should begin execution.
     */
    public boolean shouldExecute()
    {
		ticks = ticks + 1;
    	if(ticks % 300 == 0)
    	{
            if (!this.theWorld.isDaytime())
            {
                return false;
            }
            else if (this.theWorld.isRaining())
            {
                return false;
            }
            else if (this.theWorld.canSeeSky(new BlockPos(this.theCreature.posX, this.theCreature.getEntityBoundingBox().minY, this.theCreature.posZ)))
            {
                return false;
            }
            else
            {
                Vec3d vec3 = this.findOutside();

                if (vec3 == null)
                {
                    return false;
                }
                else
                {
                    this.sunX = vec3.xCoord;
                    this.sunY = vec3.yCoord;
                    this.sunZ = vec3.zCoord;
                    return true;
                }
            }
    	}
    	return false;
    }

    /**
     * Returns whether an in-progress EntityAIBase should continue executing
     */
    public boolean continueExecuting()
    {
    	if(this.theCreature.getNavigator().noPath())
    	{
    		EntityPigman pig = (EntityPigman) this.theCreature;
    		if(this.theWorld.canSeeSky(new BlockPos(this.theCreature.posX, this.theCreature.getEntityBoundingBox().minY, this.theCreature.posZ)))
    		{
    			//System.out.println("I see the light!");
        		pig.setHappiness(pig.getHappiness() + 2);
    		}
    		else
    		{
    			//System.out.println("You were my sunshine... my only sunshine...");
        		pig.setHappiness(pig.getHappiness() - 2);
    			pig.setUnhappyReason("I miss sky");
    		}
    	}
        return !this.theCreature.getNavigator().noPath();
    }

    /**
     * Execute a one shot task or start executing a continuous task
     */
    public void startExecuting()
    {
        this.theCreature.getNavigator().tryMoveToXYZ(this.sunX, this.sunY, this.sunZ, this.movementSpeed);
    }

    private Vec3d findOutside()
    {
        Random random = this.theCreature.getRNG();
        BlockPos blockpos = new BlockPos(this.theCreature.posX, this.theCreature.posY, this.theCreature.posZ);

        for (int i = 0; i < 10; ++i)
        {
            BlockPos blockpos1 = blockpos.add(random.nextInt(20) - 10, random.nextInt(6) - 3, random.nextInt(20) - 10);

            if (this.theWorld.canSeeSky(blockpos1))
            {
                return new Vec3d((double)blockpos1.getX(), (double)blockpos1.getY(), (double)blockpos1.getZ());
            }
        }

        return null;
    }
}