package bitevo.Zetal.LoMaS.Factions.GUI;

import bitevo.Zetal.LoMaS.Factions.FactionsMod;
import bitevo.Zetal.LoMaS.Factions.Entity.EntityPigmanWarrior;
import bitevo.Zetal.LoMaS.Factions.Inventory.ContainerWarriorInventory;
import bitevo.Zetal.LoMaS.Factions.Message.FactionMessage;
import bitevo.Zetal.LoMaS.Factions.Message.WarriorMessage;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Faction;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiWarriorInventory extends GuiContainer
{
	private static final ResourceLocation warriorGuiTexture = new ResourceLocation(FactionsMod.modid, "gui/warrior.png");
	/** The player inventory bound to this GUI. */
	private IInventory playerInventory;
	/** The horse inventory bound to this GUI. */
	private EntityPigmanWarrior warriorEntity;
	/** The mouse x-position recorded during the last rendered frame. */
	private float mousePosx;
	/** The mouse y-position recorded during the last renderered frame. */
	private float mousePosY;
	
	private int state = 0;
	private String[] states = {"Idle", "Follow"};

	public GuiWarriorInventory(IInventory playerInv, IInventory warriorInv, EntityPigmanWarrior war)
	{
		super(new ContainerWarriorInventory(playerInv, warriorInv, war, Minecraft.getMinecraft().thePlayer));
		this.playerInventory = playerInv;
		this.warriorEntity = war;
		this.allowUserInput = false;
		this.state = war.getState();
	}

	@Override
	public void initGui()
	{
		super.initGui();
		int k = (this.width - this.xSize) / 2;
		int l = (this.height - this.ySize) / 2;
		this.buttonList.clear();
		
		GuiButton statebutton = new GuiButton(0, k + 83, l + 58, 62, 20, this.states[this.state]);
		this.buttonList.add(statebutton);
	}

	/**
	 * Draw the foreground layer for the GuiContainer (everything in front of the items). Args : mouseX, mouseY
	 */
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		int k = (this.width - this.xSize) / 2;
		int l = (this.height - this.ySize) / 2;
		if(LoMaS_Faction.getLoMaSFaction(this.warriorEntity.getFaction()) == null)
		{
			this.fontRendererObj.drawString("Faction: " + "----", 83, 29, 4210752);
		}
		else
		{
			this.fontRendererObj.drawString("Faction: " + LoMaS_Faction.getLoMaSFaction(this.warriorEntity.getFaction()).getName(), 83, 29, 4210752);
		}
	}

	/**
	 * Args : renderPartialTicks, mouseX, mouseY
	 */
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(warriorGuiTexture);
		int k = (this.width - this.xSize) / 2;
		int l = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
		GuiInventory.drawEntityOnScreen(k + 51, l + 68, 23, (float) (k + 51) - this.mousePosx, (float) (l + 25) - this.mousePosY, this.warriorEntity);
	}

	/**
	 * Draws the screen and all the components in it. Args : mouseX, mouseY, renderPartialTicks
	 */
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		this.mousePosx = (float) mouseX;
		this.mousePosY = (float) mouseY;
		super.drawScreen(mouseX, mouseY, partialTicks);
	}

	@Override
	protected void actionPerformed(GuiButton button)
	{
		// Creating
		if (button.id == 0)
		{
			this.state = (this.state + 1) % (this.states.length);
			this.warriorEntity.setState(this.state);
			FactionsMod.snw.sendToServer(new WarriorMessage(this.warriorEntity.getPersistentID(), Minecraft.getMinecraft().thePlayer.getPersistentID(), this.state));
			this.initGui();
		}
	}
}