package bitevo.Zetal.LoMaS.Specializations.Block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockBush;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.FoodInitializer;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntityPlant;

public class BlockNetherWart extends BlockBush implements ITileEntityProvider
{
    public static final PropertyInteger AGE = PropertyInteger.create("age", 0, 3);    
    private static final AxisAlignedBB[] NETHER_WART_AABB = new AxisAlignedBB[] {new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.3125D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.5D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.6875D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.875D, 1.0D)};

    public BlockNetherWart()
    {
        super(Material.PLANTS, MapColor.RED);
        this.setDefaultState(this.blockState.getBaseState().withProperty(AGE, Integer.valueOf(0)));
        this.setTickRandomly(true);
        this.setCreativeTab((CreativeTabs)null);
    }

    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        return NETHER_WART_AABB[((Integer)state.getValue(AGE)).intValue()];
    }

    /**
     * Return true if the block can sustain a Bush
     */
    protected boolean canSustainBush(IBlockState state)
    {
        return state.getBlock() == Blocks.SOUL_SAND;
    }

    public boolean canBlockStay(World worldIn, BlockPos pos, IBlockState state)
    {
        return super.canBlockStay(worldIn, pos, state);
    }
	
    @Override
    public IBlockState onBlockPlaced(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
		if(world.getTileEntity(pos) != null && world.getTileEntity(pos) instanceof TileEntityPlant && placer instanceof EntityPlayer)
		{
			TileEntityPlant tile = (TileEntityPlant) world.getTileEntity(pos.up());
			tile.setOwner(placer.getPersistentID());
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer((EntityPlayer) placer);
			if (lplayer != null && lplayer.specClass.equalsIgnoreCase("farmer"))
			{
				tile.setFarmerLevel((byte) LoMaS_Player.getLoMaSPlayer((EntityPlayer) placer).classlevel);
			}
		}
        return this.getStateFromMeta(meta);
    }

    public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random rand)
    {
        int i = ((Integer)state.getValue(AGE)).intValue();

        int chance = 1;
        if(worldIn.getTileEntity(pos) != null && worldIn.getTileEntity(pos) instanceof TileEntityPlant)
        {
            TileEntityPlant plant = null;
            boolean hasTileEntity = false;
            if(worldIn.getTileEntity(pos) != null && worldIn.getTileEntity(pos) instanceof TileEntityPlant)
            {
            	hasTileEntity = true;
                plant = (TileEntityPlant)worldIn.getTileEntity(pos);
            }

            if ((!hasTileEntity || (plant.getOwner() != null && worldIn.getPlayerEntityByUUID(plant.getOwner()) != null)))
            {
                if(hasTileEntity)
                {
                    if(plant.getFarmerLevel() >= 9)
                    {
                   	 	chance = chance + 1;
                    }
                }
            }
            else
            {
            	chance = 0;
            }
        }
        
        if (i < 3 && rand.nextInt(10) <= 0)
        {
            state = state.withProperty(AGE, Integer.valueOf(i + 1));
            worldIn.setBlockState(pos, state, 2);
        }

        super.updateTick(worldIn, pos, state, rand);
    }

    /**
     * Spawns this Block's drops into the World as EntityItems.
     *  
     * @param chance The chance that each Item is actually spawned (1.0 = always, 0.0 = never)
     * @param fortune The player's fortune level
     */
    @SuppressWarnings("unused")
    public void dropBlockAsItemWithChance(World worldIn, BlockPos pos, IBlockState state, float chance, int fortune)
    {
        super.dropBlockAsItemWithChance(worldIn, pos, state, chance, fortune);
        if (false && !worldIn.isRemote)
        {
            int j = 1;

            if (((Integer)state.getValue(AGE)).intValue() >= 3)
            {
                j = 2 + worldIn.rand.nextInt(3);

                if (fortune > 0)
                {
                    j += worldIn.rand.nextInt(fortune + 1);
                }
            }

            for (int k = 0; k < j; ++k)
            {
                spawnAsEntity(worldIn, pos, new ItemStack(FoodInitializer.NETHER_WART));
            }
        }
    }

    /**
     * Get the Item that this Block should drop when harvested.
     *  
     * @param fortune the level of the Fortune enchantment on the player's tool
     */
    public Item getItemDropped(IBlockState state, Random rand, int fortune)
    {
        return null;
    }

    /**
     * Returns the quantity of items to drop on block destruction.
     */
    public int quantityDropped(Random random)
    {
        return 0;
    }

    /**
     * Convert the given metadata into a BlockState for this Block
     */
    @Override
    public IBlockState getStateFromMeta(int meta)
    {
        return this.getDefaultState().withProperty(AGE, Integer.valueOf(meta));
    }

    @SideOnly(Side.CLIENT)
    @Override
    public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state)
    {
        return new ItemStack(FoodInitializer.NETHER_WART);
    }

    /**
     * Convert the BlockState into the correct metadata value
     */
    @Override
    public int getMetaFromState(IBlockState state)
    {
        return ((Integer)state.getValue(AGE)).intValue();
    }

    @Override
    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, new IProperty[] {AGE});
    }

    @Override
    public java.util.List<ItemStack> getDrops(net.minecraft.world.IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
    {
        java.util.List<ItemStack> ret = new java.util.ArrayList<ItemStack>();
        Random rand = world instanceof World ? ((World)world).rand : new Random();
        int count = 1;

        if (((Integer)state.getValue(AGE)) >= 3)
        {
            count = 2 + rand.nextInt(3) + (fortune > 0 ? rand.nextInt(fortune + 1) : 0);
        }
        
        if(world.getTileEntity(pos) != null && world.getTileEntity(pos) instanceof TileEntityPlant)
        {
        	int chance = 0;
        	TileEntityPlant tilePlant = (TileEntityPlant)world.getTileEntity(pos);
            if(tilePlant.getFarmerLevel() >= 8)
            {
            	chance = chance + 6;
            }
            if(tilePlant.getFarmerLevel() >= 9)
            {
            	chance = chance + 4;
            }
            if(rand.nextInt(100) < chance)
            {
            	count++;
            }
        }

        for (int i = 0; i < count; i++)
        {
            ret.add(new ItemStack(FoodInitializer.NETHER_WART));
        }

        return ret;
    }

    @Override
    public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
    {
        super.breakBlock(worldIn, pos, state);
        worldIn.removeTileEntity(pos);
    }

    /**
     * Called on both Client and Server when World#addBlockEvent is called
     */
    @Override
	public boolean eventReceived(IBlockState state, World worldIn, BlockPos pos, int eventID, int eventParam)
	{
		super.eventReceived(state, worldIn, pos, eventID, eventParam);
        TileEntity tileentity = worldIn.getTileEntity(pos);
        return tileentity == null ? false : tileentity.receiveClientEvent(eventID, eventParam);
    }

	@Override
	public TileEntity createNewTileEntity(World world, int meta) 
	{
		return new TileEntityPlant();
	}
}