package bitevo.Zetal.LoMaS.Shared;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;

import bitevo.Zetal.LoMaS.Shared.SkillTree.GraphMLUtils;
import bitevo.Zetal.LoMaS.Shared.SkillTree.SkillNode;

public class LoMaS_Skills implements Serializable
{
	private HashSet<String> ownedNodes = new HashSet<String>();
	
	public LoMaS_Skills()
	{
		this.addOwnedNode("n135");
	}	
	
	public LoMaS_Skills(String nodes)
	{
		this.setOwnedNodes(new HashSet<String>(Arrays.asList(nodes.split(" "))));
	}
	
	@Override
	public String toString()
	{
		String s = "";
		for(String node : ownedNodes)
		{
			s = s + node + " ";
		}
		return s.trim();
	}

	public HashSet<String> getOwnedNodes()
	{
		return ownedNodes;
	}

	public void setOwnedNodes(HashSet<String> ownedNodes)
	{
		this.ownedNodes = ownedNodes;
	}
	
	public void addOwnedNode(String node)
	{
		this.ownedNodes.add(node);
	}
	
	public void removeOwnedNode(String node)
	{
		this.ownedNodes.remove(node);
	}
	
	public int getMatchingNodesFromName(String name)
	{
		int count = 0;
		for(String id : getOwnedNodes())
		{
			SkillNode matchingNode = GraphMLUtils.nodes.get(id);
			if(matchingNode.getName().equalsIgnoreCase(name))
			{
				count++;
			}
		}
		return count;
	}
	
	public float getMagnitudeFromName(String name)
	{
		float count = 0.0f;
		for(String id : getOwnedNodes())
		{
			SkillNode matchingNode = GraphMLUtils.nodes.get(id);
			if(matchingNode.getName().equalsIgnoreCase(name))
			{
				count += matchingNode.getEffect();
			}
		}
		return count;
	}
	
	public float getPercentMagnitudeFromName(String name)
	{
		float count = 0.0f;
		for(String id : getOwnedNodes())
		{
			SkillNode matchingNode = GraphMLUtils.nodes.get(id);
			if(matchingNode.getName().equalsIgnoreCase(name))
			{
				count += matchingNode.getEffect();
			}
		}
		return (count / 100.0f);
	}
}