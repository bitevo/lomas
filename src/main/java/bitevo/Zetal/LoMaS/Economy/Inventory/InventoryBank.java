package bitevo.Zetal.LoMaS.Economy.Inventory;

import java.io.Serializable;

import bitevo.Zetal.LoMaS.Economy.Item.ItemCoin;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;

public class InventoryBank implements IInventory, Serializable
{
	public ItemStack[] bankStorage = new ItemStack[27];
	public ItemStack[] inputCoin = new ItemStack[3];
	public ItemStack[] outputCoin = new ItemStack[3];
	public EntityPlayer player;

	public InventoryBank(EntityPlayer player)
	{
		this.player = player;
	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		Item item = (stack == null ? null : stack.getItem());
		return !(item instanceof ItemCoin);
	}

	@Override
	public ItemStack removeStackFromSlot(int index)
	{
		ItemStack[] aitemstack = this.bankStorage;

		if (index >= aitemstack.length)
		{
			if (index >= aitemstack.length + 3)
			{
				index -= aitemstack.length;
				index -= 3;
				aitemstack = this.outputCoin;
			}
			else
			{
				index -= aitemstack.length;
				aitemstack = this.inputCoin;
			}
		}

		if (aitemstack[index] != null)
		{
			ItemStack itemstack = aitemstack[index];
			aitemstack[index] = null;
			return itemstack;
		}
		else
		{
			return null;
		}
	}

	@Override
	public ItemStack getStackInSlot(int index)
	{
		ItemStack[] aitemstack = this.bankStorage;

		if (index >= aitemstack.length)
		{
			if (index >= aitemstack.length + 3)
			{
				index -= aitemstack.length;
				index -= 3;
				aitemstack = this.outputCoin;
			}
			else
			{
				index -= aitemstack.length;
				aitemstack = this.inputCoin;
			}
		}

		return aitemstack[index];
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		ItemStack[] aitemstack = this.bankStorage;
		if (index >= aitemstack.length)
		{
			if (index >= aitemstack.length + 3)
			{
				index -= aitemstack.length;
				index -= 3;
				aitemstack = this.outputCoin;
			}
			else
			{
				index -= aitemstack.length;
				aitemstack = this.inputCoin;
			}
		}

		aitemstack[index] = stack;
	}

	@Override
	public ItemStack decrStackSize(int index, int count)
	{
		ItemStack[] aitemstack = this.bankStorage;

		if (index >= aitemstack.length)
		{
			if (index >= aitemstack.length + 3)
			{
				index -= aitemstack.length;
				index -= 3;
				aitemstack = this.outputCoin;
			}
			else
			{
				index -= aitemstack.length;
				aitemstack = this.inputCoin;
			}
		}

		if (aitemstack[index] != null)
		{
			ItemStack itemstack;

			if (aitemstack[index].stackSize <= count)
			{
				itemstack = aitemstack[index];
				aitemstack[index] = null;
				return itemstack;
			}
			else
			{
				itemstack = aitemstack[index].splitStack(count);

				if (aitemstack[index].stackSize == 0)
				{
					aitemstack[index] = null;
				}

				return itemstack;
			}
		}
		else
		{
			return null;
		}
	}

	@Override
	public String getName()
	{
		return "Bank Teller";
	}

	@Override
	public boolean hasCustomName()
	{
		return false;
	}

	@Override
	public ITextComponent getDisplayName()
	{
		return new TextComponentTranslation("Bank Teller");
	}

	@Override
	public int getSizeInventory()
	{
		return 33;
	}

	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}

	@Override
	public void markDirty()
	{
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player)
	{
		return true;
	}

	@Override
	public void openInventory(EntityPlayer player)
	{
	}

	@Override
	public void closeInventory(EntityPlayer player)
	{
	}

	@Override
	public int getField(int id)
	{
		return 0;
	}

	@Override
	public void setField(int id, int value)
	{
	}

	@Override
	public int getFieldCount()
	{
		return 0;
	}

	@Override
	public void clear()
	{
		for (int i = 0; i < this.bankStorage.length; i++)
		{
			this.bankStorage[i] = null;
		}
		for (int i = 0; i < this.inputCoin.length; i++)
		{
			this.inputCoin[i] = null;
		}
		for (int i = 0; i < this.outputCoin.length; i++)
		{
			this.outputCoin[i] = null;
		}
	}
}
