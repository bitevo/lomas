package bitevo.Zetal.LoMaS.PolyZones.Message;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.UUID;

import bitevo.Zetal.LoMaS.PolyZones.PolyProcessor;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class PZMessage implements IMessage
{
	public UUID sender;
	public int piece;
	public Object array;

	public PZMessage()
	{
	}

	public PZMessage(PolyProcessor processor, int piece, UUID sender)
	{
		this.sender = sender;
		this.piece = piece;
		switch (piece)
		{
			case -1:
				this.array = processor.getZones();
				break;
			case 0:
				this.array = processor.getPlayers();
				break;
			case 1:
				this.array = processor.getActivePlayers();
				break;
			case 2:
				this.array = processor.getDefaultAccess();
				break;
			case 3:
				this.array = processor.getZones();
				break;
			default:
				this.array = new ArrayList();
				((ArrayList) this.array).add(processor.getZones().get(piece - 4).getChecksum());
				break;
		}
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		sender = UUID.fromString(ByteBufUtils.readUTF8String(buf));
		byte[] bytes = new byte[buf.readableBytes() - 4];
		buf.readBytes(bytes);
		Object a = this.bytesToObject(bytes);
		if (a != null)
		{
			this.array = a;
		}
		piece = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, sender.toString());
		byte[] bytes = this.objectToBytes(array);
		if (bytes.length > 32600)
		{
			System.err.println("ERROR! Packet too large in PolyZones.");
		}
		if (bytes != null)
		{
			buf.writeBytes(bytes);
		}
		buf.writeInt(piece);
	}

	public byte[] objectToBytes(Object yourObject)
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		try
		{
			out = new ObjectOutputStream(bos);
			out.writeObject(yourObject);
			byte[] yourBytes = bos.toByteArray();
			return yourBytes;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if (out != null)
				{
					out.close();
				}
			}
			catch (IOException ex)
			{
			}
			try
			{
				bos.close();
			}
			catch (IOException ex)
			{
			}
		}
		return null;
	}

	public Object bytesToObject(byte[] yourBytes)
	{
		ByteArrayInputStream bis = new ByteArrayInputStream(yourBytes);
		ObjectInput in = null;
		try
		{
			in = new ObjectInputStream(bis);
			Object o = in.readObject();
			return o;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				bis.close();
			}
			catch (IOException ex)
			{
			}
			try
			{
				if (in != null)
				{
					in.close();
				}
			}
			catch (IOException ex)
			{
			}
		}
		return null;
	}
}
