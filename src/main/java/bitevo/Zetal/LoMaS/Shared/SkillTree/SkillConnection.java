package bitevo.Zetal.LoMaS.Shared.SkillTree;

public class SkillConnection
{
	private String id;
	private String sourceNode;
	private String targetNode;
	private String style;
	
	public SkillConnection(String id)
	{
		this.id = id;
	}
	
	public SkillConnection(String id, String sourceNode, String targetNode)
	{
		this(id);
		this.sourceNode = sourceNode;
		this.targetNode = targetNode;
	}
	
	public SkillConnection(String id, String sourceNode, String targetNode, String style)
	{
		this(id, sourceNode, targetNode);
		this.style = style;
	}
	
	@Override
	public String toString()
	{
		return "SkillConnection [getId()=" + getId() + ", getSourceNode()=" + getSourceNode() + ", getTargetNode()=" + getTargetNode() + ", getStyle()=" + getStyle() + "]";
	}

	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getSourceNode()
	{
		return sourceNode;
	}
	public void setSourceNode(String sourceNode)
	{
		this.sourceNode = sourceNode;
	}
	public String getTargetNode()
	{
		return targetNode;
	}
	public void setTargetNode(String targetNode)
	{
		this.targetNode = targetNode;
	}
	public String getStyle()
	{
		return style;
	}
	public void setStyle(String style)
	{
		this.style = style;
	}
}
