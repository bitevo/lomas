package bitevo.Zetal.LoMaS.TileWrappers.Command;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import bitevo.Zetal.LoMaS.TileWrappers.TileWrapper.TileWrapperContainer;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;

public class CommandWrapper extends CommandBase
{
	@Override
	public String getCommandName()
	{
		return "wrappers";
	}

	@Override
	public int getRequiredPermissionLevel()
	{
		return 0;
	}

	@Override
	public String getCommandUsage(ICommandSender icommandsender)
	{
		return "/wrappers help";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		EntityPlayer player = null;
		try
		{
			player = this.getCommandSenderAsPlayer(sender);
		}
		catch (PlayerNotFoundException e)
		{
			e.printStackTrace();
		}
		World world = player.worldObj;
		if (player != null && LoMaS_Utils.isPlayerAdmin(player))
		{
			if (args.length > 0)
			{
				if (args[0].matches("help"))
				{
					LoMaS_Utils.addChatMessage(player, ("Commands for Wrappers include: "));
					LoMaS_Utils.addChatMessage(player, ("/wrappers clear"));
					return;
				}
				else if ((args[0].matches("clear") && (args.length == 1)))
				{
					TileWrapperContainer.instance.addedTileWrapperList.clear();
					TileWrapperContainer.instance.loadedTileWrapperList.clear();
					TileWrapperContainer.instance.tileWrappersToBeRemoved.clear();
					return;
				}
				LoMaS_Utils.addChatMessage(player, ("Malformed command. Try '/wrappers help'"));
			}
		}
	}
}
