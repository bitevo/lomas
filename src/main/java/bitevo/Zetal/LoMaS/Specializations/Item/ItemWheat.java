package bitevo.Zetal.LoMaS.Specializations.Item;

import java.util.List;

import bitevo.Zetal.LoMaS.Specializations.FoodInitializer;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemWheat extends Item
{
	public ItemWheat()
	{
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
	}

	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack)
	{
		return super.getUnlocalizedName();
	}

	@Override
	public void getSubItems(Item par1, CreativeTabs par2CreativeTabs, List par3List)
	{
		for (int j = 0; j < FoodInitializer.qualities.length; ++j)
		{
			par3List.add(new ItemStack(this, 1, j));
		}
	}

	@Override
	public String getItemStackDisplayName(ItemStack stack)
	{
		int i = stack.getItemDamage();
		return (FoodInitializer.qualities[i] + " " + super.getItemStackDisplayName(stack));
	}
}
