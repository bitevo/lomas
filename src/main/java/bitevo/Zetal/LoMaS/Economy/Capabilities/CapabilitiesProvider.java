package bitevo.Zetal.LoMaS.Economy.Capabilities;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;

public class CapabilitiesProvider implements ICapabilitySerializable<NBTTagCompound>
{

	@CapabilityInject(IPlayerCapability.class)
	public transient static final Capability<IPlayerCapability> PLAYER_CAP = null;
	
	IPlayerCapability inst = PLAYER_CAP.getDefaultInstance();

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return capability == PLAYER_CAP;
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		return capability == PLAYER_CAP ? PLAYER_CAP.<T> cast(inst) : null;
	}

	@Override
	public NBTTagCompound serializeNBT()
	{
		return (NBTTagCompound) PLAYER_CAP.getStorage().writeNBT(PLAYER_CAP, inst, null);
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt)
	{
		PLAYER_CAP.getStorage().readNBT(PLAYER_CAP, inst, null, nbt);
	}
}
