package bitevo.Zetal.LoMaS.Specializations.GUI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import bitevo.Zetal.LoMaS.Shared.GUI.GuiScrollable;
import bitevo.Zetal.LoMaS.Shared.SkillTree.GraphMLUtils;
import bitevo.Zetal.LoMaS.Shared.SkillTree.SkillConnection;
import bitevo.Zetal.LoMaS.Shared.SkillTree.SkillNode;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiLabel;
import net.minecraft.client.gui.GuiOptionButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class GuiSkillTree extends GuiScrollable
{
	protected static final ResourceLocation SKILL_ICONS = new ResourceLocation(SpecializationsMod.MODID, "gui/skilltree.png");
	protected static final HashMap<String, Integer[][]> shapeList = new HashMap<String, Integer[][]>();
	public static LoMaS_Player lPlayer;
	public static EntityPlayer player;
	public static HashSet<String> queuedNodes = new HashSet<String>(); 

	public GuiSkillTree(GuiScreen parentScreenIn, EntityPlayer player)
	{
		super(parentScreenIn, GraphMLUtils.minMaxX, GraphMLUtils.minMaxY);
		this.title = "Skill Tree";

		this.lPlayer = LoMaS_Player.getLoMaSPlayer(player.getPersistentID());
		this.player = player;
		// Holds the coordinates for shapes: Top Left Image widths
		shapeList.put("ellipse", new Integer[][] { { 0, 0 }, { 18, 18 } });
		shapeList.put("diamond", new Integer[][] { { 19, 0 }, { 26, 26 } });
		shapeList.put("hexagon", new Integer[][] { { 46, 0 }, { 28, 28 } });
		shapeList.put("octagon", new Integer[][] { { 109, 0 }, { 120, 120 } });
	}
	
	@Override
	public void initGui()
	{
		this.buttonList.clear();
		if(this.isConfirming)
		{
	        GlStateManager.depthFunc(515);
	        GlStateManager.disableDepth();
	        GlStateManager.enableTexture2D();
			this.buttonList.add(new GuiOptionButton(9, this.width / 2 - 155, this.height / 6 + 86, "I'm sure!"));
			this.buttonList.add(new GuiOptionButton(10, this.width / 2 - 155 + 160, this.height / 6 + 86, "Nevermind"));
		}
		else
		{
			super.initGui();
			this.buttonList.add(new GuiOptionButton(11, this.width / 2 - 24, this.height / 2 + 74, 60, 20, "Reset"));
		}
	}

	@Override
	protected void trueMouseClick(int mouseX, int mouseY)
	{
		SkillNode node = this.getNodeContainingMouse(mouseX, mouseY);
		HashSet<String> extendableNodes = (HashSet<String>) lPlayer.getSkills().getOwnedNodes().clone();
		extendableNodes.addAll((HashSet<String>) this.queuedNodes.clone());
		if(node != null)
		{
			ArrayList<SkillNode> connectedNodes = GraphMLUtils.getConnectedNodes(node);
			if(!lPlayer.getSkills().getOwnedNodes().contains(node.getId()))
			{
		        for(SkillNode sibling : connectedNodes)
		        {
			        if(extendableNodes.contains(sibling.getId()))
					{
			        	if(this.queuedNodes.contains(node.getId()))
			        	{
			        		this.queuedNodes.remove(node.getId());
			        	}
			        	else
			        	{
							if(lPlayer.getUnspentPoints() > this.queuedNodes.size())
							{
					        	this.queuedNodes.add(node.getId());
							}
			        	}
			        	break;
					}
		        }
			}
			else
			{
				ArrayList<SkillNode> edgeNodes = new ArrayList<SkillNode>();

		        for(SkillNode sibling : connectedNodes)
		        {
		        	
		        }
			}
		}
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
		// If the player hits the 'Done' button
		if (button.id == 1)
		{
			if(this.queuedNodes.size() > 0)
			{
				if(lPlayer.getUnspentPoints() >= this.queuedNodes.size())
				{
					this.isConfirming = true;
					this.initGui();
				}
			}
			else
			{
				this.mc.thePlayer.closeScreen();
			}
		}
		// The player confirms their changes; tell the server!
		else if (button.id == 9)
		{
			this.isConfirming = false;
			LoMaS_Player dummyPlayer = new LoMaS_Player();
			dummyPlayer.copyTraitsFromLoMaSPlayer(lPlayer);
			for(String node : this.queuedNodes)
			{
				dummyPlayer.getSkills().addOwnedNode(node);
			}
			dummyPlayer.player = this.player;
			SpecializationsMod.sendPlayerAbilitiesToServer(dummyPlayer);
			GuiSkillTree.queuedNodes.clear();
			lPlayer.setSkills(dummyPlayer.getSkills());
			this.initGui();
		}
		// The player cancels his changes- wait.
		else if (button.id == 10)
		{
			this.isConfirming = false;
			this.initGui();
		}
		// The player confirms their changes; tell the server!
		else if (button.id == 11)
		{
			this.isConfirming = false;
			LoMaS_Player dummyPlayer = new LoMaS_Player();
			dummyPlayer.copyTraitsFromLoMaSPlayer(lPlayer);
			dummyPlayer.setSkills("n135");
			dummyPlayer.player = this.player;
			SpecializationsMod.sendPlayerAbilitiesToServer(dummyPlayer);
			GuiSkillTree.queuedNodes.clear();
			lPlayer.setSkills(dummyPlayer.getSkills());
			this.initGui();
		}
	}

	@Override
	public void drawScrollableContent(int mouseX, int mouseY, float partialTicks)
	{
		this.drawConnections(mouseX, mouseY);
		this.drawAvailableNodes(mouseX, mouseY);
		this.drawUnavailableNodes(mouseX, mouseY, partialTicks);
	}
	
	@Override
	public void drawScrollableOverlay(int mouseX, int mouseY, float partialTicks)
	{
		int x = (this.width - this.imageWidth) / 2;
		int y = (this.height - this.imageHeight) / 2;
		this.fontRendererObj.drawString("Skill Points: " + (lPlayer.getUnspentPoints() - this.queuedNodes.size()), x + 15, y + 175, 4210752);
		
		if(!this.isConfirming)
		{
			this.drawNodeStrings(mouseX, mouseY);
		}
		else if(this.isConfirming)
		{
	        GlStateManager.depthFunc(515);
	        GlStateManager.disableDepth();
	        GlStateManager.enableTexture2D();
			GlStateManager.enableDepth();
			GlStateManager.enableLighting();
			RenderHelper.disableStandardItemLighting();
			super.drawDefaultBackground();

	        for (int i = 0; i < this.buttonList.size(); ++i)
	        {
	            ((GuiButton)this.buttonList.get(i)).drawButton(this.mc, mouseX, mouseY);
	        }

	        for (int j = 0; j < this.labelList.size(); ++j)
	        {
	            ((GuiLabel)this.labelList.get(j)).drawLabel(this.mc, mouseX, mouseY);
	        }
	        
			this.drawCenteredString(this.fontRendererObj, "Are you sure?", this.width / 2, 70, 16777215);
			this.drawCenteredString(this.fontRendererObj, "This action is permanent!", this.width / 2, 90, 16777215);
		}
	}
	
	public void drawConnections(int mouseX, int mouseY)
	{
		int i, j, k, l;
		for (SkillConnection edge : GraphMLUtils.edges.values())
		{
			SkillNode sourceNode = GraphMLUtils.nodes.get(edge.getSourceNode());
			SkillNode targetNode = GraphMLUtils.nodes.get(edge.getTargetNode());
			int startX = 0, startY = 0, endX = 0, endY = 0;
			startX = (int) sourceNode.getPositionX();
			startY = (int) sourceNode.getPositionY();
			endX = (int) targetNode.getPositionX();
			endY = (int) targetNode.getPositionY();
			int[] temp = this.getCoordsFromPixels(startX, startY);
			i = temp[0];
			j = temp[1];
			temp = this.getCoordsFromPixels(endX, endY);
			k = temp[0];
			l = temp[1];

			LoMaS_Utils.drawLine(i, j, k, l, -2130706433, 3);
		}
	}
	
	public void drawUnavailableNodes(int mouseX, int mouseY, float partialTicks)
	{
		int i, j;

        RenderHelper.enableGUIStandardItemLighting();
        GlStateManager.disableLighting();
        GlStateManager.enableRescaleNormal();
        GlStateManager.enableColorMaterial();
		
		this.mc.getTextureManager().bindTexture(SKILL_ICONS);
		for (SkillNode node : GraphMLUtils.nodes.values())
		{
			if(!lPlayer.getSkills().getOwnedNodes().contains(node.getId()))
			{
		        GlStateManager.color(0.2F, 0.2F, 0.2F, 0.9F);
		        for(SkillNode sibling : GraphMLUtils.getConnectedNodes(node))
		        {
		    		HashSet<String> extendableNodes = (HashSet<String>) lPlayer.getSkills().getOwnedNodes().clone();
		    		extendableNodes.addAll((HashSet<String>) this.queuedNodes.clone());
			        if(extendableNodes.contains(sibling.getId()))
					{
			        	float mod = (float) (Math.cos(player.ticksExisted / 10.0D) / 4.0D) + 0.5F;
			        	mod = Math.max(0.35F, mod);
			        	mod = Math.min(0.85F, mod);
			        	if(this.queuedNodes.contains(node.getId()))
			        	{
		                    GlStateManager.color(0.5F, 0.5F, 1.5F, 1.0F);
			        	}
			        	else if(lPlayer.getUnspentPoints() > this.queuedNodes.size())
			        	{
		                    GlStateManager.color(0.85F * mod, 0.85F * mod, 1.00F * mod, 1.0F);
			        	}
					}
		        }
				int[] temp = this.getCoordsFromPixels((int) node.getPositionX(), (int) node.getPositionY());
				i = temp[0];
				j = temp[1];
				int topLeftx = this.shapeList.get(node.getShape())[0][0];
				int topLefty = this.shapeList.get(node.getShape())[0][1];
				int widthX = this.shapeList.get(node.getShape())[1][0];
				int lengthY = this.shapeList.get(node.getShape())[1][1];
				i = (int) (i - (widthX * 0.5D));
				j = (int) (j - (lengthY * 0.5D));
				if (this.areCoordsVisible(i, j, widthX, lengthY))
				{
					this.drawTexturedModalRect(i, j, topLeftx, topLefty, widthX, lengthY);
				}
			}
		}

        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
	}
	
	public void drawAvailableNodes(int mouseX, int mouseY)
	{
		int i, j;
		this.mc.getTextureManager().bindTexture(SKILL_ICONS);
		for (SkillNode node : GraphMLUtils.nodes.values())
		{
			if(lPlayer.getSkills().getOwnedNodes().contains(node.getId()))
			{
				int[] temp = this.getCoordsFromPixels((int) node.getPositionX(), (int) node.getPositionY());
				i = temp[0];
				j = temp[1];
				int topLeftx = this.shapeList.get(node.getShape())[0][0];
				int topLefty = this.shapeList.get(node.getShape())[0][1];
				int widthX = this.shapeList.get(node.getShape())[1][0];
				int lengthY = this.shapeList.get(node.getShape())[1][1];
				i = (int) (i - (widthX * 0.5D));
				j = (int) (j - (lengthY * 0.5D));
				if (this.areCoordsVisible(i, j, widthX, lengthY))
				{
					this.drawTexturedModalRect(i, j, topLeftx, topLefty, widthX, lengthY);
				}
			}
		}
	}
	
	public void drawNodeStrings(int mouseX, int mouseY)
	{
		int i, j;
		for (SkillNode node : GraphMLUtils.nodes.values())
		{
			int[] temp = this.getCoordsFromPixels((int) node.getPositionX(), (int) node.getPositionY());
			i = temp[0];
			j = temp[1];
			int widthX = this.shapeList.get(node.getShape())[1][0];
			int lengthY = this.shapeList.get(node.getShape())[1][1];
			i = (int) (i - (widthX * 0.5D));
			j = (int) (j - (lengthY * 0.5D));
			if (this.isMouseWithinRect(mouseX, mouseY, i, j, widthX))
			{
				String name = node.getName();
				String description = node.getDescription();

				if(description != null && !description.isEmpty())
				{
					int mouseXOffset = mouseX + 12;
					int mouseYOffset = mouseY - 4;
					int nameWidth = Math.max(this.fontRendererObj.getStringWidth(name), 120);
					int descriptionWidth = this.fontRendererObj.splitStringWidth(description, nameWidth);
					this.drawGradientRect(mouseXOffset - 3, mouseYOffset - 3, mouseXOffset + nameWidth + 3, mouseYOffset + descriptionWidth + 3 + 12, -1073741824, -1073741824);
					this.fontRendererObj.drawStringWithShadow(name, (float) mouseXOffset, (float) mouseYOffset, -1);
					this.fontRendererObj.drawSplitString(description, mouseXOffset, mouseYOffset + 12, nameWidth, -6250336);
				}
			}
		}
	}
	
	public SkillNode getNodeContainingMouse(int mouseX, int mouseY)
	{
		int i, j;
		SkillNode ret = null;
		for (SkillNode node : GraphMLUtils.nodes.values())
		{
			int[] temp = this.getCoordsFromPixels((int) node.getPositionX(), (int) node.getPositionY());
			i = temp[0];
			j = temp[1];
			int widthX = this.shapeList.get(node.getShape())[1][0];
			int lengthY = this.shapeList.get(node.getShape())[1][1];
			i = (int) (i - (widthX * 0.5D));
			j = (int) (j - (lengthY * 0.5D));
			if (this.isMouseWithinRect(mouseX, mouseY, i, j, widthX))
			{
				return node;
			}
		}
		return ret;
	}
}