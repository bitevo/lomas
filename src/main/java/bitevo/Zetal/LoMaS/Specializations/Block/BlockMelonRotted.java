package bitevo.Zetal.LoMaS.Specializations.Block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockMelonRotted extends Block
{

	public BlockMelonRotted()
	{
		super(Material.GOURD);
		this.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
	}

	/**
	 * Returns the ID of the items to drop on destruction.
	 */
	public int idDropped(int par1, Random par2Random, int par3)
	{
		return Item.getIdFromItem(Items.MELON);
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(Random par1Random)
	{
		return 0;// 3 + par1Random.nextInt(5);
	}

	/**
	 * Returns the usual quantity dropped by the block plus a bonus of 1 to 'i' (inclusive).
	 */
	@Override
	public int quantityDroppedWithBonus(int par1, Random par2Random)
	{
		int j = this.quantityDropped(par2Random) + par2Random.nextInt(1 + par1);

		if (j > 9)
		{
			j = 9;
		}
		j = 0;
		return j;
	}

	@Override
	public boolean removedByPlayer(IBlockState state, World world, BlockPos pos, EntityPlayer player, boolean willHarvest)
	{
		return world.setBlockToAir(pos);
	}
}
