package bitevo.Zetal.LoMaS.Specializations.Handler;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLog;
import net.minecraft.block.BlockOldLog;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngame;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.storage.ExtendedBlockStorage;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.common.gameevent.TickEvent.PlayerTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.WorldTickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.Block.BlockSap;

public class TickHandler
{
	private int tickCounter = 0;
	private int prevLevel = 0;
	private Minecraft mc;
	protected int updateLCG = (new Random()).nextInt();

	@SubscribeEvent
	public void tickEnd(TickEvent event)
	{
		if (event.side == Side.CLIENT)
		{
			clientTickEnd(event);
		}
		/*
		 * else { serverTickEnd(event); }
		 */
	}

	@SideOnly(Side.CLIENT)
	public void clientTickEnd(TickEvent event)
	{
		mc = Minecraft.getMinecraft();
		if (mc.thePlayer == null)
		{
			ScaledResolution res = new ScaledResolution(this.mc);
			int width = res.getScaledWidth();
			int height = res.getScaledHeight();
			short short1 = 274;
			int k = width / 2 - short1 / 2;
			GuiIngame gig = new GuiIngame(mc);
			if (this.mc.currentScreen instanceof GuiMainMenu)
			{
				Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation(SpecializationsMod.modid, "gui/levelup.png"));
				gig.drawTexturedModalRect(k + 16, 74, 0, 32, 100, 18);
			}
		}
		else
		{
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer(mc.thePlayer);
			if (lplayer == null)
			{
				LoMaS_Player.addLoMaSPlayer(mc.thePlayer);
				lplayer = LoMaS_Player.getLoMaSPlayer(mc.thePlayer);
			}

			if (event.phase == Phase.END && mc.thePlayer.ticksExisted % 20 == 0)
			{
				FMLHandler.setPlayerMaxHealth(mc.thePlayer);
			}

			if (lplayer.levelDisplay && tickCounter < 600)
			{
				ScaledResolution res = new ScaledResolution(this.mc);
				int width = res.getScaledWidth();
				int height = res.getScaledHeight();
				tickCounter++;
				GuiIngame gig = new GuiIngame(mc);
				Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation(SpecializationsMod.modid, "gui/levelup.png"));
				gig.drawTexturedModalRect((width / 3) - 10, 2, 0, 0, 160, 32);
			}
			else
			{
				lplayer.levelDisplay = false;
				tickCounter = 0;
			}
		}
	}

	@SubscribeEvent
	public void serverTickEnd(WorldTickEvent event)
	{
		if (event.side == Side.SERVER)
		{
			if (event.phase == TickEvent.Phase.START)
			{
				WorldServer world = (WorldServer) event.world;
				Collection<Chunk> chunks = world.getChunkProvider().getLoadedChunks();
				HashMap<BlockPos, EnumFacing> placeSap = new HashMap();
				int i = 0;
				int j = 0;

				try
				{
					for (Iterator iterator = chunks.iterator(); iterator.hasNext(); world.theProfiler.endSection())
					{
						world.theProfiler.startSection("getChunk");
						Chunk chunk = (Chunk) iterator.next();
						int k = chunk.xPosition * 16;
						int l = chunk.zPosition * 16;
						int i1;
						BlockPos blockpos;
						world.theProfiler.endStartSection("tickBlocks");
						i1 = world.getGameRules().getInt("randomTickSpeed");

						if (i1 > 0)
						{
							ExtendedBlockStorage[] aextendedblockstorage = chunk.getBlockStorageArray();
							int l2 = aextendedblockstorage.length;

							for (int j1 = 0; j1 < l2; ++j1)
							{
								ExtendedBlockStorage extendedblockstorage = aextendedblockstorage[j1];

								if (extendedblockstorage != null && extendedblockstorage.getNeedsRandomTick())
								{
									for (int k1 = 0; k1 < i1; ++k1)
									{
										updateLCG = updateLCG * 3 + 1013904223;
										int l1 = updateLCG >> 2;
										int i2 = l1 & 15;
										int j2 = l1 >> 8 & 15;
										int k2 = l1 >> 16 & 15;
										++j;
										BlockPos blockpos2 = new BlockPos(i2 + k, k2 + extendedblockstorage.getYLocation(), j2 + l);
										IBlockState iblockstate = extendedblockstorage.get(i2, k2, j2);
										Block block = iblockstate.getBlock();

										if (block == Blocks.LOG)
										{
											++i;
											if (world.getBlockState(blockpos2).getValue(BlockOldLog.VARIANT) == BlockPlanks.EnumType.BIRCH 
													&& world.rand.nextInt(8) == 0)
											{
												// System.out.println("1 in 6 success");
												if (world.isAirBlock(blockpos2.north()) && world.rand.nextInt(4) == 0)
												{
													placeSap.put(blockpos2.north(), EnumFacing.SOUTH);
													//this.spawnSapOnSide(world, blockpos2.north(), EnumFacing.SOUTH);
												}
												else if (world.isAirBlock(blockpos2.south()) && world.rand.nextInt(4) == 0)
												{
													placeSap.put(blockpos2.south(), EnumFacing.NORTH);
													//this.spawnSapOnSide(world, blockpos2.south(), EnumFacing.NORTH);
												}
												else if (world.isAirBlock(blockpos2.east()) && world.rand.nextInt(4) == 0)
												{
													placeSap.put(blockpos2.east(), EnumFacing.WEST);
													//this.spawnSapOnSide(world, blockpos2.east(), EnumFacing.WEST);
												}
												else if (world.isAirBlock(blockpos2.west()) && world.rand.nextInt(4) == 0)
												{
													placeSap.put(blockpos2.west(), EnumFacing.EAST);
													//this.spawnSapOnSide(world, blockpos2.west(), EnumFacing.EAST);
												}
											}
										}
									}
								}
							}
						}
					}
					
					Iterator iter = placeSap.entrySet().iterator();
					while(iter.hasNext())
					{
						HashMap.Entry pair = (Entry) iter.next();
						this.spawnSapOnSide(world, (BlockPos) pair.getKey(), (EnumFacing) pair.getValue());
					}
				}
				catch (ConcurrentModificationException e)
				{
					System.out.println("There was a concurrent modification exception... again.");
				}

			}
		}
	}

	@SubscribeEvent
	public void playerTick(PlayerTickEvent event)
	{
		if (event.player.worldObj.isRemote)
		{
			clientPlayerTick(event);
		}
		else
		{
			serverPlayerTick(event);
		}
	}

	@SideOnly(Side.CLIENT)
	public void clientPlayerTick(PlayerTickEvent event)
	{
		if (event.phase == Phase.END && event.player != null && LoMaS_Player.getLoMaSPlayer(event.player) != null)
		{
			event.player.heal(0);
			FMLHandler.setPlayerMaxHealth(event.player);
		}
	}

	public void serverPlayerTick(PlayerTickEvent event)
	{
		int ticker = event.player.ticksExisted;
		// System.out.println("SERVER");
		if (event.phase == Phase.END && event.player != null && LoMaS_Player.getLoMaSPlayer(event.player) != null && ticker % 100 == 0)
		{
			// event.player.heal(0);
			// FMLHandler.setPlayerMaxHealth(event.player);
			LoMaS_Player.getLoMaSPlayer(event.player).sendPlayerAbilities();
		}
	}

	public void spawnSapOnSide(World world, BlockPos pos, EnumFacing facing)
	{
		// System.out.println("spawn sap on side");
		boolean allTrue = true;
		for (int i = -1; i < 1; i++)
		{
			for (int j = -1; j < 1; j++)
			{
				for (int k = -1; k < 1; k++)
				{
					IBlockState state = world.getBlockState(pos.add(i, j, k));
					if (state.getBlock() instanceof BlockSap || state.getMaterial() == Material.GRASS || state.getMaterial() == Material.GROUND)
					{
						allTrue = false;
					}
				}
			}
		}

		if (allTrue)
		{
			// System.out.println("all true!");
			world.setBlockState(pos, SpecializationsMod.SAPBLOCK.getDefaultState().withProperty(BlockSap.FACING, facing));// , 2);
		}
	}
}