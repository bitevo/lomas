package bitevo.Zetal.LoMaS.Citizens.Renderer;

import bitevo.Zetal.LoMaS.Citizens.CitizensMod;
import bitevo.Zetal.LoMaS.Citizens.Entity.EntityPigmanCitizen;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.layers.LayerHeldItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;

public class RenderPigman extends RenderLiving<EntityPigmanCitizen>
{
	public RenderPigman(RenderManager p_i46153_1_, ModelBase p_i46153_2_, float p_i46153_3_)
	{
		super(p_i46153_1_, p_i46153_2_, p_i46153_3_);
		this.addLayer(new LayerHeldItem(this));
	}

	@Override
	public void doRender(EntityPigmanCitizen entity, double x, double y, double z, float entityYaw, float partialTicks)
	{
		if (entity != null && entity instanceof EntityPigmanCitizen)
		{
			ModelPigman modelpigman = (ModelPigman) super.getMainModel();
			EntityPigmanCitizen pigman = (EntityPigmanCitizen) entity;
			ItemStack itemstack = pigman.getHeldItem(EnumHand.MAIN_HAND);
			modelpigman.leftArmPose = ModelBiped.ArmPose.EMPTY;

			if (itemstack == null)
			{
				modelpigman.rightArmPose = ModelBiped.ArmPose.EMPTY;
			}
			else
			{
				modelpigman.rightArmPose = ModelBiped.ArmPose.ITEM;
			}
			super.doRender(entity, x, y, z, entityYaw, partialTicks);
		}
	}

	@Override
	protected ResourceLocation getEntityTexture(EntityPigmanCitizen entity)
	{
		return new ResourceLocation(CitizensMod.modid, "textures/entity/pigman/pigman.png");
	}
}
