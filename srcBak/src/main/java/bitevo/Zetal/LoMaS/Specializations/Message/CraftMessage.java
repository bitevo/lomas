package bitevo.Zetal.LoMaS.Specializations.Message;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class CraftMessage implements IMessage
{
    public int maxDamage = 0;
    public int damage = 0;

    public CraftMessage() {}

    public CraftMessage(int damage, int maxDamage)
    {
        this.damage = damage;
        this.maxDamage = maxDamage;
    }

	@Override
	public void fromBytes(ByteBuf buf) 
	{
        this.damage = buf.readInt();
        this.maxDamage = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) 
	{
        buf.writeInt(this.damage);
        buf.writeInt(this.maxDamage);
	}
}
