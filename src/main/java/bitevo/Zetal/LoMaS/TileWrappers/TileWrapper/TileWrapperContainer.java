package bitevo.Zetal.LoMaS.TileWrappers.TileWrapper;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldSavedData;
import net.minecraftforge.common.DimensionManager;

public class TileWrapperContainer implements Serializable
{
	public static TileWrapperContainer instance = new TileWrapperContainer();
	public HashMap<BlockPos, ArrayList<TileWrapper>> loadedTileWrapperList = new HashMap<BlockPos, ArrayList<TileWrapper>>();
	public HashMap<BlockPos, ArrayList<TileWrapper>> addedTileWrapperList = new HashMap<BlockPos, ArrayList<TileWrapper>>();
	public HashMap<BlockPos, ArrayList<TileWrapper>> tileWrappersToBeRemoved = new HashMap<BlockPos, ArrayList<TileWrapper>>();
	public boolean processingLoadedTiles;

	/**
	 * Use SET instead except in TileWrapper maintenance
	 * 
	 * @param wrapper
	 * @return
	 */
	public boolean addTileWrapper(TileWrapper wrapper)
	{
		BlockPos pos = wrapper.getPos();
		HashMap<BlockPos, ArrayList<TileWrapper>> dest = processingLoadedTiles ? addedTileWrapperList : loadedTileWrapperList;
		boolean flag = false;
		if (dest.get(pos) != null)
		{
			flag = dest.get(pos).add(wrapper);
			wrapper.index = dest.get(pos).size() - 1;
		}
		else
		{
			ArrayList<TileWrapper> array = new ArrayList<TileWrapper>();
			flag = array.add(wrapper);
			dest.put(wrapper.getPos(), array);
			wrapper.index = dest.get(pos).size() - 1;
		}

		World world = wrapper.getWorld();
		if (world != null && world.isRemote)
		{
			BlockPos blockpos = wrapper.getPos();
			IBlockState iblockstate = world.getBlockState(blockpos);
			world.notifyBlockUpdate(blockpos, iblockstate, iblockstate, 2);
		}

		return flag;
	}

	public void addTileWrappers(Collection<TileWrapper> tileWrapperCollection)
	{
		if (this.processingLoadedTiles)
		{
			for (TileWrapper wrapper : tileWrapperCollection)
			{
				BlockPos pos = wrapper.getPos();
				if (this.addedTileWrapperList.get(pos) != null)
				{
					this.addedTileWrapperList.get(pos).add(wrapper);
				}
				else
				{
					ArrayList<TileWrapper> array = new ArrayList<TileWrapper>();
					array.add(wrapper);
					this.addedTileWrapperList.put(wrapper.getPos(), array);
				}
			}
		}
		else
		{
			for (TileWrapper wrapper : tileWrapperCollection)
			{
				this.addTileWrapper(wrapper);
			}
		}
	}

	public TileWrapper getTileWrapper(BlockPos pos, int index)
	{
		if (this.isOutsideBuildHeight(pos))
		{
			return null;
		}
		TileWrapper wrapper = null;
		if (this.processingLoadedTiles)
		{
			wrapper = addedTileWrapperList.get(pos) != null ? addedTileWrapperList.get(pos).size() > index ? addedTileWrapperList.get(pos).get(index) : null : null;
		}
		if (wrapper == null)
		{
			wrapper = loadedTileWrapperList.get(pos) != null ? loadedTileWrapperList.get(pos).size() > index ? loadedTileWrapperList.get(pos).get(index) : null : null;
		}
		if (wrapper == null)
		{
			wrapper = addedTileWrapperList.get(pos) != null ? addedTileWrapperList.get(pos).size() > index ? addedTileWrapperList.get(pos).get(index) : null : null;
		}
		return wrapper;
	}

	@Nullable
	private TileWrapper getPendingTileWrapperAt(BlockPos pos, int index)
	{
		TileWrapper wrapper = addedTileWrapperList.get(pos) != null ? addedTileWrapperList.get(pos).size() > index ? addedTileWrapperList.get(pos).get(index) : null : null;

		if (!wrapper.isInvalid())
		{
			return wrapper;
		}

		return null;
	}

	public void setTileWrapper(BlockPos pos, @Nullable TileWrapper wrapper)
	{
		pos = pos.toImmutable(); // Forge - prevent mutable BlockPos leaks
		if (!isOutsideBuildHeight(pos))
		{
			if (wrapper != null && !wrapper.isInvalid())
			{
				wrapper.setPos(pos);
				if (this.processingLoadedTiles)
				{
					if (this.addedTileWrapperList.get(pos) != null)
					{
						this.addedTileWrapperList.get(pos).add(wrapper);
						wrapper.index = this.addedTileWrapperList.get(pos).size() - 1;
					}
					else
					{
						ArrayList<TileWrapper> array = new ArrayList<TileWrapper>();
						array.add(wrapper);
						this.addedTileWrapperList.put(wrapper.getPos(), array);
						wrapper.index = this.addedTileWrapperList.get(pos).size() - 1;
					}
				}
				else
				{
					this.addTileWrapper(wrapper);
				}
			}
		}
	}

	public void removeTileWrapper(BlockPos pos, int index)
	{
		TileWrapper tilewrapper = this.getTileWrapper(pos, index);

		if (tilewrapper != null && this.processingLoadedTiles)
		{
			tilewrapper.invalidate();
			this.addedTileWrapperList.get(pos).remove(index);
			if (!(tilewrapper instanceof ITickable)) // Forge: If they are not tickable they wont be removed in the update loop.
				this.loadedTileWrapperList.get(pos).remove(index);
		}
		else
		{
			if (tilewrapper != null)
			{
				this.addedTileWrapperList.get(pos).remove(index);
				this.loadedTileWrapperList.get(pos).remove(index);
			}
		}
	}

	/**
	 * Adds the specified TileWrapper to the pending removal list.
	 */
	public void markTileWrapperForRemoval(TileWrapper tileWrapperIn)
	{
		if (this.tileWrappersToBeRemoved.get(tileWrapperIn.pos) != null)
		{
			this.tileWrappersToBeRemoved.get(tileWrapperIn.pos).add(tileWrapperIn);
			return;
		}
		ArrayList<TileWrapper> insert = new ArrayList<TileWrapper>();
		insert.add(tileWrapperIn);
		this.tileWrappersToBeRemoved.put(tileWrapperIn.pos, insert);
	}

	private boolean isValid(BlockPos pos)
	{
		return !isOutsideBuildHeight(pos) && pos.getX() >= -30000000 && pos.getZ() >= -30000000 && pos.getX() < 30000000 && pos.getZ() < 30000000;
	}

	private boolean isOutsideBuildHeight(BlockPos pos)
	{
		return pos.getY() < 0 || pos.getY() >= 256;
	}

	private void writeObject(java.io.ObjectOutputStream out) throws IOException
	{
		HashMap<BlockPos, ArrayList<TileWrapper>>[] maps = new HashMap[] {this.loadedTileWrapperList, this.addedTileWrapperList, this.tileWrappersToBeRemoved};
		for(HashMap<BlockPos, ArrayList<TileWrapper>> map : maps)
		{
			out.writeInt(map.entrySet().size());
			for(Map.Entry<BlockPos, ArrayList<TileWrapper>> entry : map.entrySet())
			{
				out.writeInt(entry.getValue().size());
				out.writeLong(entry.getKey().toLong());
				for(TileWrapper wrapper : entry.getValue())
				{
					out.writeByte(wrapper.getWorld().provider.getDimension());
					out.writeObject(wrapper.getClass());
					CompressedStreamTools.write(wrapper.getUpdateTag(), out);
				}
			}
		}
	}

	private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException
	{
		this.loadedTileWrapperList = new HashMap<BlockPos, ArrayList<TileWrapper>>();
		this.addedTileWrapperList = new HashMap<BlockPos, ArrayList<TileWrapper>>();
		this.tileWrappersToBeRemoved = new HashMap<BlockPos, ArrayList<TileWrapper>>();
		
		HashMap<BlockPos, ArrayList<TileWrapper>>[] maps = new HashMap[] {this.loadedTileWrapperList, this.addedTileWrapperList, this.tileWrappersToBeRemoved};
		for(HashMap<BlockPos, ArrayList<TileWrapper>> map : maps)
		{
			int entrySetSize = (int) in.readInt();
			//System.out.println("entrySetSize: " + entrySetSize);
			for(int i = 0; i < entrySetSize; i++)
			{
				int entryValueSize = (int) in.readInt();
				//System.out.println("entryValueSize: " + entryValueSize);
				long posLong = (long) in.readLong();
				//System.out.println("posLong: " + posLong);
				BlockPos entryKey = BlockPos.fromLong(posLong);
				for(int j = 0; j < entryValueSize; j++)
				{
					byte dimension = (byte) in.readByte();
					//System.out.println("dimension: " + dimension);
					World world = DimensionManager.getWorld(dimension);
					Class<? extends TileWrapper> tileBase = (Class<? extends TileWrapper>) in.readObject();
					//System.out.println("tileBase: " + tileBase);
					
					TileWrapper wrapper = null;
					try
					{
						wrapper = tileBase.getConstructor(String.class, World.class, BlockPos.class, EnumFacing.class).newInstance(null, world, entryKey, null);
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
					
					if(wrapper != null)
					{
						wrapper.readFromNBT(CompressedStreamTools.read(new DataInputStream(in)));
						wrapper.setPos(entryKey);
						//System.out.println(wrapper + " " + map + " " + entryKey);
						if (map.get(entryKey) != null)
						{
							map.get(entryKey).add(wrapper);
							wrapper.index = map.get(entryKey).size() - 1;
						}
						else
						{
							ArrayList<TileWrapper> array = new ArrayList<TileWrapper>();
							array.add(wrapper);
							map.put(entryKey, array);
							wrapper.index = map.get(entryKey).size() - 1;
						}
					}
					else
					{
						System.err.println("Fuck.Fuck.Fuck.Fuck.Fuck.Fuck.");
						System.err.println("Fuck.Fuck.Fuck.Fuck.Fuck.");
						System.err.println("Fuck.Fuck.Fuck.Fuck.");
						System.err.println("Fuck.Fuck.Fuck.");
						System.err.println("Fuck.Fuck.");
						System.err.println("Fuck.");
					}
				}
			}
		}
	}

	private void readObjectNoData() throws ObjectStreamException{}
}
