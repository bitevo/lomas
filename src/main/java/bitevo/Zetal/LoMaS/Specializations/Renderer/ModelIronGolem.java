package bitevo.Zetal.LoMaS.Specializations.Renderer;

import bitevo.Zetal.LoMaS.Specializations.Entity.EntityFactionIronGolem;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;

public class ModelIronGolem extends ModelBiped
{
    /** The head model for the iron golem. */
    public ModelRenderer ironGolemHead;
    /** The body model for the iron golem. */
    public ModelRenderer ironGolemUpperBody;
    /** The body model for the iron golem. */
    public ModelRenderer ironGolemLowerBody;
    /** The right arm model for the iron golem. */
    public ModelRenderer ironGolemRightArm;
    /** The left arm model for the iron golem. */
    public ModelRenderer ironGolemLeftArm;
    /** The left leg model for the Iron Golem. */
    public ModelRenderer ironGolemLeftLeg;
    /** The right leg model for the Iron Golem. */
    public ModelRenderer ironGolemRightLeg;

    public ModelIronGolem()
    {
        this(0.0F);
    }

    public ModelIronGolem(float p_i1161_1_)
    {
        this(p_i1161_1_, -7.0F);
    }

    public ModelIronGolem(float p_i46362_1_, float p_i46362_2_)
    {
        this.textureWidth = 128;
        this.textureHeight = 128;
        this.ironGolemUpperBody = new ModelRenderer(this, 0, 40);
        this.ironGolemUpperBody.setRotationPoint(0.0F, -7.0F, 1.0F);
        this.ironGolemUpperBody.addBox(-9.0F, -2.0F, -6.0F, 18, 12, 11, 0.0F);
        this.ironGolemLowerBody = new ModelRenderer(this, 0, 70);
        this.ironGolemLowerBody.setRotationPoint(0.0F, -7.0F, 0.0F);
        this.ironGolemLowerBody.addBox(-7.0F, 10.0F, -3.6F, 14, 5, 7, 0.5F);
        this.ironGolemHead = new ModelRenderer(this, 0, 0);
        this.ironGolemHead.setRotationPoint(0.0F, -7.0F, 2.0F);
        this.ironGolemHead.addBox(-5.0F, -12.0F, -7.0F, 10, 10, 10, 0.0F);
        this.ironGolemRightArm = new ModelRenderer(this, 45, 0);
        this.ironGolemRightArm.mirror = true;
        this.ironGolemRightArm.setRotationPoint(0.0F, -5.0F, 1.5F);
        this.ironGolemRightArm.addBox(-13.0F, -2.5F, -6.0F, 4, 25, 9, 0.0F);
        this.ironGolemLeftArm = new ModelRenderer(this, 45, 0);
        this.ironGolemLeftArm.setRotationPoint(0.0F, -5.0F, 1.5F);
        this.ironGolemLeftArm.addBox(9.0F, -2.5F, -6.0F, 4, 25, 9, 0.0F);
        this.ironGolemRightLeg = new ModelRenderer(this, 0, 85);
        this.ironGolemRightLeg.setRotationPoint(-4.0F, 11.5F, 0.5F);
        this.ironGolemRightLeg.addBox(-3.5F, -3.0F, -3.0F, 6, 16, 5, 0.0F);
        this.ironGolemLeftLeg = new ModelRenderer(this, 0, 85);
        this.ironGolemLeftLeg.setRotationPoint(5.0F, 11.5F, 0.5F);
        this.ironGolemLeftLeg.addBox(-3.5F, -3.0F, -3.0F, 6, 16, 5, 0.0F);
    }

    /**
     * Sets the models various rotation angles then renders the model.
     */
    public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale)
    {
        this.setRotationAngles(limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale, entityIn);
        this.ironGolemHead.render(scale);
        this.ironGolemUpperBody.render(scale);
        this.ironGolemLowerBody.render(scale);
        this.ironGolemLeftLeg.render(scale);
        this.ironGolemRightLeg.render(scale);
        this.ironGolemRightArm.render(scale);
        this.ironGolemLeftArm.render(scale);
    }

    /**
     * Sets the model's various rotation angles. For bipeds, par1 and par2 are used for animating the movement of arms
     * and legs, where par1 represents the time(so that arms and legs swing back and forth) and par2 represents how
     * "far" arms and legs can swing at most.
     */
    public void setRotationAngles(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn)
    {
        this.ironGolemHead.rotateAngleY = netHeadYaw * 0.017453292F;
        this.ironGolemHead.rotateAngleX = headPitch * 0.017453292F;
        this.ironGolemLeftLeg.rotateAngleX = -1.5F * this.triangleWave(limbSwing, 13.0F) * limbSwingAmount;
        this.ironGolemRightLeg.rotateAngleX = 1.5F * this.triangleWave(limbSwing, 13.0F) * limbSwingAmount;
        this.ironGolemLeftLeg.rotateAngleY = 0.0F;
        this.ironGolemRightLeg.rotateAngleY = 0.0F;
    }

    /**
     * Used for easily adding entity-dependent animations. The second and third float params here are the same second
     * and third as in the setRotationAngles method.
     */
    public void setLivingAnimations(EntityLivingBase entitylivingbaseIn, float p_78086_2_, float p_78086_3_, float partialTickTime)
    {
    	EntityFactionIronGolem entityirongolem = (EntityFactionIronGolem)entitylivingbaseIn;
        int i = entityirongolem.getAttackTimer();

        if (i > 0)
        {
            this.ironGolemRightArm.rotateAngleX = -2.0F + 1.5F * this.triangleWave((float)i - partialTickTime, 10.0F);
            this.ironGolemLeftArm.rotateAngleX = -2.0F + 1.5F * this.triangleWave((float)i - partialTickTime, 10.0F);
        }
        else
        {
            this.ironGolemRightArm.rotateAngleX = (-0.2F + 1.5F * this.triangleWave(p_78086_2_, 13.0F)) * p_78086_3_;
            this.ironGolemLeftArm.rotateAngleX = (-0.2F - 1.5F * this.triangleWave(p_78086_2_, 13.0F)) * p_78086_3_;
        }
    }

    private float triangleWave(float p_78172_1_, float p_78172_2_)
    {
        return (Math.abs(p_78172_1_ % p_78172_2_ - p_78172_2_ * 0.5F) - p_78172_2_ * 0.25F) / (p_78172_2_ * 0.25F);
    }
}
