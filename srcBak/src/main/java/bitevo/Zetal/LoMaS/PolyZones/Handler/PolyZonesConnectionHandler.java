package bitevo.Zetal.LoMaS.PolyZones.Handler;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.PlayerTickEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent.ClientDisconnectionFromServerEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import bitevo.Zetal.LoMaS.PolyZones.DataHandler;
import bitevo.Zetal.LoMaS.PolyZones.PolyZonesMod;
import bitevo.Zetal.LoMaS.PolyZones.Message.PZMessage;

public class PolyZonesConnectionHandler
{
	public static boolean sent = false;

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void onPlayerTick(PlayerTickEvent event)
	{
		if (Minecraft.getMinecraft().thePlayer != null && (!sent))
		{
			EntityPlayer player = (EntityPlayer) Minecraft.getMinecraft().thePlayer;
			if (PolyZonesMod.processor == null)
			{
				PolyZonesMod.processor = DataHandler.getProcessor();
			}
			PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, 0, player.getPersistentID()));
			PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, 1, player.getPersistentID()));
			PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, 2, player.getPersistentID()));
			PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, 3, player.getPersistentID()));
			sent = true;
			PolyZonesMod.playersMove.put(player.getDisplayNameString(), new Boolean(false));
		}
	}
	
	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void onClientDisconnected(ClientDisconnectionFromServerEvent event)
	{
		sent = false;
	}
}
