package bitevo.Zetal.LoMaS.Economy.Handlers;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.UUID;

import bitevo.Zetal.LoMaS.Economy.EconomyMod;
import bitevo.Zetal.LoMaS.Economy.Capabilities.CapabilitiesProvider;
import bitevo.Zetal.LoMaS.Economy.Entity.EntityItemCoin;
import bitevo.Zetal.LoMaS.Economy.Inventory.ContainerTrade;
import bitevo.Zetal.LoMaS.Economy.Inventory.EconPlayerContainer;
import bitevo.Zetal.LoMaS.Economy.Inventory.InventoryBank;
import bitevo.Zetal.LoMaS.Economy.Inventory.InventoryPlayer;
import bitevo.Zetal.LoMaS.Economy.Inventory.SlotCoin;
import bitevo.Zetal.LoMaS.Economy.Item.ItemCoin;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.play.server.SPacketCloseWindow;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.event.entity.player.PlayerContainerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.Event.Result;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Name and cast of this class are irrelevant
 */
public class EconEventHandler
{
	public static HashMap<UUID, HashMap<UUID, Integer>> tradeTicker = new HashMap();
	public int tradeTimer = 8;

	public static EntityPlayer getTradePartner(EntityPlayer player)
	{
		EntityPlayer partner = null;

		if (FMLCommonHandler.instance().getSide() == Side.SERVER)
		{
			if (player.openContainer instanceof ContainerTrade)
			{
				HashMap<UUID, Integer> sourceMap = tradeTicker.get(player.getPersistentID());
				if (sourceMap != null)
				{
					Iterator<Entry<UUID, Integer>> targetIter = sourceMap.entrySet().iterator();
					HashMap.Entry<UUID, Integer> targetPair = null;
					while (targetIter.hasNext())
					{
						targetPair = targetIter.next();
						EntityPlayer target = ((EntityPlayerMP) player).getServerWorld().getPlayerEntityByUUID(targetPair.getKey());
						int ticker = targetPair.getValue();

						if (ticker < 0)
						{
							return target;
						}
					}
				}
			}
		}

		return partner;
	}

	@SubscribeEvent
	public void onPlayerItemPickup(EntityItemPickupEvent event)
	{
		EntityPlayer player = event.getEntityPlayer();
		if (player.openContainer instanceof ContainerTrade)
		{
			event.setResult(Result.DENY);
		}
	}

	@SubscribeEvent
	public void onPlayerInteractPlayer(PlayerInteractEvent.EntityInteract event)
	{
		if (FMLCommonHandler.instance().getSide() == Side.SERVER)
		{
			if (event.getEntityPlayer().getHeldItem(event.getHand()) == null)
			{
				Entity t = event.getTarget();
				EntityPlayer source = event.getEntityPlayer();
				if (t instanceof EntityPlayer)
				{
					EntityPlayer target = (EntityPlayer) event.getTarget();
					HashMap<UUID, Integer> sourceMap = tradeTicker.get(source.getPersistentID());
					if (sourceMap == null)
					{
						sourceMap = new HashMap<UUID, Integer>();
					}
					HashMap<UUID, Integer> targetMap = tradeTicker.get(target.getPersistentID());

					if (targetMap != null && targetMap.containsKey(source.getPersistentID()))
					{
						int targetCounter = targetMap.get(source.getPersistentID());
						if (targetCounter <= this.tradeTimer)
						{
							sourceMap.put(target.getPersistentID(), -2);
							targetMap.put(source.getPersistentID(), -2);
							tradeTicker.put(target.getPersistentID(), targetMap);
							tradeTicker.put(source.getPersistentID(), sourceMap);
							source.openGui(EconomyMod.instance, 2, event.getEntity().worldObj, 0, 0, 0);
							target.openGui(EconomyMod.instance, 2, event.getEntity().worldObj, 0, 0, 0);
							sourceMap.put(target.getPersistentID(), -1);
							targetMap.put(source.getPersistentID(), -1);
							tradeTicker.put(target.getPersistentID(), targetMap);
							tradeTicker.put(source.getPersistentID(), sourceMap);
						}
					}
					else
					{
						if (sourceMap.get(target.getPersistentID()) == null)
						{
							LoMaS_Utils.addChatMessage(target, "Received a trade request from " + " " + source.getDisplayNameString());
						}
						sourceMap.put(target.getPersistentID(), 0);
						tradeTicker.put(source.getPersistentID(), sourceMap);
					}
					event.setCanceled(true);
				}
			}
		}
	}

	@SubscribeEvent
	public void onPlayerUpdate(LivingUpdateEvent event)
	{
		if (FMLCommonHandler.instance().getSide() == Side.SERVER)
		{
			if (event.getEntityLiving() instanceof EntityPlayer && event.getEntityLiving().ticksExisted % 20 == 0)
			{
				EntityPlayerMP player = (EntityPlayerMP) event.getEntityLiving();
				HashMap<UUID, Integer> sourceMap = this.tradeTicker.get(player.getPersistentID());
				if (sourceMap != null)
				{
					Iterator<Entry<UUID, Integer>> targetIter = sourceMap.entrySet().iterator();
					while (targetIter.hasNext())
					{
						HashMap.Entry<UUID, Integer> targetPair = targetIter.next();
						int ticker = targetPair.getValue();
						if (ticker >= this.tradeTimer)
						{
							targetIter.remove();
						}
						else if (ticker >= 0)
						{
							ticker++;
							targetPair.setValue(ticker);
						}

						if (this.getTradePartner(player) != null && ticker > 0)
						{
							targetPair.setValue(-1);
						}
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onContainerClosed(PlayerContainerEvent.Close event)
	{
		if (FMLCommonHandler.instance().getSide() == Side.SERVER)
		{
			if (event.getContainer() instanceof ContainerTrade)
			{
				EntityPlayerMP closer = (EntityPlayerMP) event.getEntityPlayer();
				HashMap<UUID, Integer> sourceMap = this.tradeTicker.get(closer.getPersistentID());
				if (sourceMap != null)
				{
					Iterator<Entry<UUID, Integer>> targetIter = sourceMap.entrySet().iterator();
					HashMap.Entry<UUID, Integer> targetPair = null;
					while (targetIter.hasNext())
					{
						targetPair = targetIter.next();
						EntityPlayerMP target = (EntityPlayerMP) closer.getServerWorld().getPlayerEntityByUUID(targetPair.getKey());
						int ticker = targetPair.getValue();

						if (ticker == -1)
						{
							// System.out.println(ticker + " PID:" + closer.getPersistentID() +" map:" + this.tradeTicker);
							this.tradeTicker.remove(closer.getPersistentID());
							this.tradeTicker.remove(target.getPersistentID());
							target.connection.sendPacket(new SPacketCloseWindow(target.openContainer.windowId));
							target.openContainer.onContainerClosed(target);
							target.openContainer = target.inventoryContainer;
							target.inventoryContainer.detectAndSendChanges();
							closer.inventoryContainer.detectAndSendChanges();
						}
					}
				}
			}
		}
	}

	// This is fired before the client receives the inventory data, and so we can use it for prepping the client to receive the inventory.
	@SubscribeEvent
	public void inventoryEvent(EntityJoinWorldEvent event)
	{
		Entity ent = event.getEntity();
		if (ent instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) ent;

			if (!(player.inventory instanceof InventoryPlayer))
			{
				InventoryPlayer newInv = new InventoryPlayer(player);
				newInv.readFromNBT(player.inventory.writeToNBT(new NBTTagList()));
				player.inventory = newInv;
			}

			if (!(player.inventoryContainer instanceof EconPlayerContainer))
			{
				player.inventoryContainer = new EconPlayerContainer((InventoryPlayer) player.inventory, !player.worldObj.isRemote, player);
				player.openContainer = player.inventoryContainer;
				player.inventory.markDirty();
			}
		}
	}

	@SubscribeEvent
	public void onPlayerLoad(AttachCapabilitiesEvent.Entity event)
	{
		if (event.getEntity() instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) event.getEntity();

			if (!event.getEntity().hasCapability(CapabilitiesProvider.PLAYER_CAP, null))
			{
				CapabilitiesProvider prov = new CapabilitiesProvider();
				prov.getCapability(CapabilitiesProvider.PLAYER_CAP, null).setEntityPlayer(player);
				prov.getCapability(CapabilitiesProvider.PLAYER_CAP, null).setInventoryBank(new InventoryBank(player));
				prov.getCapability(CapabilitiesProvider.PLAYER_CAP, null).setWorld(player.worldObj);
				event.addCapability(new ResourceLocation("SpecializationsMod:PlayerCap"), prov);
			}
		}
	}

	@SubscribeEvent
	public void itemJoinsWorld(EntityJoinWorldEvent event)
	{
		Entity entity = event.getEntity();
		World world = event.getWorld();
		if (entity instanceof EntityItem)
		{
			EntityItem entItem = (EntityItem) entity;
			ItemStack stackOriginal = entItem.getEntityItem();
			if (stackOriginal != null)
			{
				if (stackOriginal.getItem() instanceof ItemCoin)
				{
					EntityItemCoin ent2 = new EntityItemCoin(entItem.worldObj, entItem.posX, entItem.posY, entItem.posZ, entItem.getEntityItem());
					this.copyDataFromOld(entItem, ent2);
					ent2.copyLocationAndAnglesFrom(entItem);
					ent2.motionX = entItem.motionX;
					ent2.motionY = entItem.motionY;
					ent2.motionZ = entItem.motionZ;
					ent2.setPickupDelay(40);
					ent2.setOwner(entItem.getOwner());
					ent2.setThrower(entItem.getThrower());
					world.removeEntity(entItem);
					world.spawnEntityInWorld(ent2);
				}
			}
		}
	}

	private void copyDataFromOld(Entity entityOld, Entity entityNew)
	{
		NBTTagCompound nbttagcompound = entityOld.writeToNBT(new NBTTagCompound());
		entityNew.readFromNBT(nbttagcompound);
	}
}