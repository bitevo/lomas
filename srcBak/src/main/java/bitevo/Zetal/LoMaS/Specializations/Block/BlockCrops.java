package bitevo.Zetal.LoMaS.Specializations.Block;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Specializations.FoodInitializer;
import bitevo.Zetal.LoMaS.Specializations.SpecializationsMod;
import bitevo.Zetal.LoMaS.Specializations.TileEntity.TileEntityPlant;
import net.minecraft.block.Block;
import net.minecraft.block.BlockBush;
import net.minecraft.block.IGrowable;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.SoundType;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockCrops extends BlockBush implements IGrowable, ITileEntityProvider
{
    public static final PropertyInteger AGE = PropertyInteger.create("age", 0, 7);
    private static final AxisAlignedBB[] CROPS_AABB = new AxisAlignedBB[] {new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.125D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.25D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.375D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.5D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.625D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.75D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.875D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D)};

    public BlockCrops()
    {
    	if(this.getDefaultState() == null)
    	{
            this.setDefaultState(this.blockState.getBaseState().withProperty(AGE, Integer.valueOf(0)));
    	}
        this.setTickRandomly(true);
        float f = 0.5F;
        this.setCreativeTab((CreativeTabs)null);
        this.setHardness(0.0F);
        this.setSoundType(SoundType.PLANT);
        this.disableStats();
        this.isBlockContainer = true;
    }
    
    public int getMaxAge()
    {
    	return 7;
    }

    /**
     * is the block grass, dirt or farmland
     */
    protected boolean canPlaceBlockOn(Block ground)
    {
        return ground == FoodInitializer.FARMLAND;
    }    

    @Override
    public void randomTick(World worldIn, BlockPos pos, IBlockState state, Random random) 
    {
        this.updateTick(worldIn, pos, state, random);
        if ((worldIn.getLightFor(EnumSkyBlock.SKY, pos) > 13) 
        && worldIn.getLightFromNeighbors(pos.up()) >= 9
        && worldIn.isDaytime())
        {
            int i = this.getAge(state);

            if (i < getMaxAge())
            {
                float f = getGrowthChance(this, worldIn, pos);
                if (f > 0.0f && random.nextInt((int)(25.0F / f) + 1) == 0)
                {
                    worldIn.setBlockState(pos, this.withAge(i + 1), 2);
                }
            }
        }
    }

    @Override
    public void updateTick(World world, BlockPos pos, IBlockState state, Random random)
    {
    	world.scheduleUpdate(pos, state.getBlock(), this.tickRate(world));
		if(world.getTileEntity(pos) != null && world.getTileEntity(pos) instanceof TileEntityPlant)
		{
			TileEntityPlant tile = (TileEntityPlant) world.getTileEntity(pos);
			if(tile.getFertTime() <= 0)
			{
				tile.setFertilized(false);
				tile.markDirty();
			    world.notifyBlockUpdate(pos, state, state, lightOpacity);
			}
			
			if(tile.isFertilized())
			{
				tile.setFertTime(tile.getFertTime() - 1);
			}
		}
        super.updateTick(world, pos, state, random);
    }

    public void grow(World worldIn, BlockPos pos, IBlockState state)
    {
        int i = this.getAge(state) + MathHelper.getRandomIntegerInRange(worldIn.rand, 2, 5);

        if (i > getMaxAge())
        {
            i = getMaxAge();
        }

        worldIn.setBlockState(pos, this.withAge(i), 2);
    }

    public EnumBlockRenderType getRenderType(IBlockState state)
    {
        return EnumBlockRenderType.MODEL;
    }

    protected static float getGrowthChance(Block blockIn, World worldIn, BlockPos pos)
    {
        float f = 1.0F;
        BlockPos blockpos1 = pos.down();

        for (int i = -1; i <= 1; ++i)
        {
            for (int j = -1; j <= 1; ++j)
            {
                float f1 = 0.0F;
                IBlockState iblockstate = worldIn.getBlockState(blockpos1.add(i, 0, j));

                if (SpecializationsMod.canSustainPlant(worldIn, blockpos1.add(i, 0, j), net.minecraft.util.EnumFacing.UP, (net.minecraftforge.common.IPlantable)blockIn))
                {
                    f1 = 1.0F;

                    if (iblockstate.getBlock().isFertile(worldIn, blockpos1.add(i, 0, j)))
                    {
                        f1 = 3.0F;
                    }
                }

                if (i != 0 || j != 0)
                {
                    f1 /= 4.0F;
                }

                f += f1;
            }
        }

        BlockPos blockpos2 = pos.north();
        BlockPos blockpos3 = pos.south();
        BlockPos blockpos4 = pos.west();
        BlockPos blockpos5 = pos.east();
        boolean flag = blockIn == worldIn.getBlockState(blockpos4).getBlock() || blockIn == worldIn.getBlockState(blockpos5).getBlock();
        boolean flag1 = blockIn == worldIn.getBlockState(blockpos2).getBlock() || blockIn == worldIn.getBlockState(blockpos3).getBlock();

        if (flag && flag1)
        {
            f /= 2.0F;
        }
        else
        {
            boolean flag2 = blockIn == worldIn.getBlockState(blockpos4.north()).getBlock() || blockIn == worldIn.getBlockState(blockpos5.north()).getBlock() || blockIn == worldIn.getBlockState(blockpos5.south()).getBlock() || blockIn == worldIn.getBlockState(blockpos4.south()).getBlock();

            if (flag2)
            {
                f /= 2.0F;
            }
        }
        
        TileEntityPlant plant = null;
        boolean hasTileEntity = false;
        if(worldIn.getTileEntity(pos) != null && worldIn.getTileEntity(pos) instanceof TileEntityPlant)
        {
        	hasTileEntity = true;
            plant = (TileEntityPlant)worldIn.getTileEntity(pos);
        }

        if ((!hasTileEntity || (plant.getOwner() != null && worldIn.getPlayerEntityByUUID(plant.getOwner()) != null)))
        {
            if(hasTileEntity)
            {
	        	plant = (TileEntityPlant)worldIn.getTileEntity(pos);
	            if(plant.getFarmerLevel() >= 4)
	            {
	           	 	f = f * 1.15f;
	            }
	            if(plant.getFarmerLevel() >= 5)
	            {
	           	 	f = f * 1.15f;
	            }
	            if(plant.getFarmerLevel() >= 6)
	            {
	           	 	f = f * 1.15f;
	            }
	            if(plant.getFarmerLevel() >= 7)
	            {
	           	 	f = f * 1.15f;
	            }
            }
        }
        else
        {
        	f = 0;
        }

        return f;
    }

    public boolean canBlockStay(World worldIn, BlockPos pos, IBlockState state)
    {
    	boolean p1 = (worldIn.getLight(pos) >= 8 || worldIn.getLightFor(EnumSkyBlock.SKY, pos) > 13);
        return p1 && this.canPlaceBlockOn(worldIn.getBlockState(pos.down()).getBlock());
    }

    protected Item getSeed()
    {
        return FoodInitializer.WHEAT_SEEDS;
    }

    protected Item getCrop()
    {
        return FoodInitializer.WHEAT;
    }
    
    @Override
    public IBlockState onBlockPlaced(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
		if(world.getTileEntity(pos) != null && world.getTileEntity(pos) instanceof TileEntityPlant && placer instanceof EntityPlayer)
		{
			TileEntityPlant tile = (TileEntityPlant) world.getTileEntity(pos);
			tile.setOwner(placer.getPersistentID());
			LoMaS_Player lplayer = LoMaS_Player.getLoMaSPlayer((EntityPlayer) placer);
			if (lplayer != null && lplayer.specClass.equalsIgnoreCase("farmer"))
			{
				tile.setFarmerLevel((byte) LoMaS_Player.getLoMaSPlayer((EntityPlayer) placer).classlevel);
			}
		}
    	this.updateTick(world, pos, world.getBlockState(pos), this.RANDOM);
        return this.getStateFromMeta(meta);
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void randomDisplayTick(IBlockState stateIn, World world, BlockPos pos, Random rand)
    {
		if(world.getTileEntity(pos) != null && world.getTileEntity(pos) instanceof TileEntityPlant)
		{
			TileEntityPlant tile = (TileEntityPlant) world.getTileEntity(pos);
			if (tile.isFertilized())
			{
	            ItemDye.spawnBonemealParticles(world, pos, 1);
			}
		}
    }

    /**
     * Spawns this Block's drops into the World as EntityItems.
     *  
     * @param chance The chance that each Item is actually spawned (1.0 = always, 0.0 = never)
     * @param fortune The player's fortune level
     */
    public void dropBlockAsItemWithChance(World worldIn, BlockPos pos, IBlockState state, float chance, int fortune)
    {
        super.dropBlockAsItemWithChance(worldIn, pos, state, chance, 0);
    }

    /**
     * Get the Item that this Block should drop when harvested.
     *  
     * @param fortune the level of the Fortune enchantment on the player's tool
     */
    public Item getItemDropped(IBlockState state, Random rand, int fortune)
    {
        return this.getAge(state) == getMaxAge() ? this.getCrop() : this.getSeed();
    }

    /**
     * Whether this IGrowable can grow
     */
    public boolean canGrow(World worldIn, BlockPos pos, IBlockState state, boolean isClient)
    {
        return this.getAge(state) < getMaxAge() && worldIn.canSeeSky(pos);
    }

    public boolean canUseBonemeal(World worldIn, Random rand, BlockPos pos, IBlockState state)
    {
        return true;
    }

    @SideOnly(Side.CLIENT)
    public Item getItem(World worldIn, BlockPos pos)
    {
        return this.getSeed();
    }

    public void grow(World worldIn, Random rand, BlockPos pos, IBlockState state)
    {
        this.grow(worldIn, pos, state);
    }

    /**
     * Convert the given metadata into a BlockState for this Block
     */
    public IBlockState getStateFromMeta(int meta)
    {
        return this.withAge(meta);
    }

    /**
     * Convert the BlockState into the correct metadata value
     */
    public int getMetaFromState(IBlockState state)
    {
        return this.getAge(state);
    }

    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, new IProperty[] {AGE});
    }
    
    @Override
    public int quantityDropped(IBlockState state, int fortune, Random random)
    {
    	int normal = super.quantityDropped(state, fortune, random);
    	return normal;
    }

    @Override
    public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
    {
        List<ItemStack> ret = new ArrayList<ItemStack>();
        //System.out.println("Hi? " + world.getTileEntity(pos) + " " + pos);
    	if(world.getTileEntity(pos) != null && world.getTileEntity(pos) instanceof TileEntityPlant)
    	{
            ret = super.getDrops(world, pos, state, fortune);
            int age = this.getAge(state);
            Random rand = world instanceof World ? ((World)world).rand : new Random();
            if (age >= getMaxAge())
            {
                int k = 3 + fortune;

                for (int i = 0; i < 3 + fortune; ++i)
                {
                    if (rand.nextInt(15) <= age)
                    {
                        ret.add(new ItemStack(this.getSeed(), 1, 0));
                    }
                }
            }
            //System.out.println("Hi? " + ret);
            
            int quality = ((TileEntityPlant) world.getTileEntity(pos)).getQuality();
            for(ItemStack stack : ret)
            {
            	if(stack.getItem().equals(this.getCrop()))
            	{
            		stack.setItemDamage(SpecializationsMod.getQualityIndexFromDamage(quality));
            	}
            }
    	}
        return ret;
    }

    protected PropertyInteger getAgeProperty()
    {
        return AGE;
    }

    protected int getAge(IBlockState state)
    {
        return ((Integer)state.getValue(this.getAgeProperty())).intValue();
    }

    public IBlockState withAge(int age)
    {
        return this.getDefaultState().withProperty(this.getAgeProperty(), Integer.valueOf(age));
    }

    public boolean isMaxAge(IBlockState state)
    {
        return ((Integer)state.getValue(this.getAgeProperty())).intValue() >= this.getMaxAge();
    }

    @Override
    public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
    {
        super.breakBlock(worldIn, pos, state);
        worldIn.removeTileEntity(pos);
    }

    /**
     * Called on both Client and Server when World#addBlockEvent is called
     */
    @Override
    public boolean eventReceived(IBlockState state, World worldIn, BlockPos pos, int eventID, int eventParam)
    {
        super.eventReceived(state, worldIn, pos, eventID, eventParam);
        TileEntity tileentity = worldIn.getTileEntity(pos);
        return tileentity == null ? false : tileentity.receiveClientEvent(eventID, eventParam);
    }

	@Override
	public TileEntity createNewTileEntity(World world, int meta) 
	{
		return new TileEntityPlant();
	}    
	
	@Override
    public net.minecraftforge.common.EnumPlantType getPlantType(net.minecraft.world.IBlockAccess world, BlockPos pos)
    {
        if (this == FoodInitializer.WHEAT_BLOCK)          return net.minecraftforge.common.EnumPlantType.Crop;
        if (this == FoodInitializer.CARROTS)        return net.minecraftforge.common.EnumPlantType.Crop;
        if (this == FoodInitializer.POTATOES)       return net.minecraftforge.common.EnumPlantType.Crop;
        if (this == FoodInitializer.MELON_STEM)     return net.minecraftforge.common.EnumPlantType.Crop;
        if (this == FoodInitializer.PUMPKIN_STEM)   return net.minecraftforge.common.EnumPlantType.Crop;
        if (this == FoodInitializer.BEETROOTS)   return net.minecraftforge.common.EnumPlantType.Crop;
        if (this == FoodInitializer.NETHER_WART_BLOCK)    return net.minecraftforge.common.EnumPlantType.Nether;
        if (this == SpecializationsMod.SAPLING)        return net.minecraftforge.common.EnumPlantType.Plains;
        return super.getPlantType(world, pos);
    }
}