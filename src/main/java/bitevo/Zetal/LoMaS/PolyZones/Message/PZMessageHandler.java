package bitevo.Zetal.LoMaS.PolyZones.Message;

import java.util.ArrayList;
import java.util.HashMap;

import bitevo.Zetal.LoMaS.PolyZones.Access;
import bitevo.Zetal.LoMaS.PolyZones.PolyProcessor;
import bitevo.Zetal.LoMaS.PolyZones.PolyZone;
import bitevo.Zetal.LoMaS.PolyZones.PolyZonePlayer;
import bitevo.Zetal.LoMaS.PolyZones.PolyZonesMod;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PZMessageHandler implements IMessageHandler<PZMessage, IMessage>
{
	public static HashMap<PolyZonePlayer, PolyProcessor> receivingProcessors = new HashMap<PolyZonePlayer, PolyProcessor>();
	public static HashMap<PolyZonePlayer, Boolean> hasFirstZone = new HashMap<PolyZonePlayer, Boolean>();

	@Override
	public IMessage onMessage(PZMessage message, MessageContext ctx)
	{
		if (ctx.side == Side.CLIENT)
		{
			handleCPZMessage(message);
		}
		else
		{
			handleSPZMessage(message);
		}
		return null;
	}

	@SideOnly(Side.CLIENT)
	private void handleCPZMessage(PZMessage packet)
	{
		int piece = packet.piece;
		Object array = packet.array;
		PolyProcessor processor = PolyZonesMod.processor;
		EntityPlayer player = Minecraft.getMinecraft().thePlayer;
		// System.out.println("Hi! " + piece);
		switch (piece)
		{
			case -1:
				processor.setZones((ArrayList<PolyZone>) array);
				ArrayList<PolyZone> tempArray = new ArrayList();
				for (int i = 0; i < PolyZonesMod.processor.getZones().size(); i++)
				{
					tempArray.clear();
					tempArray.add(PolyZonesMod.processor.getZones().get(i));
					PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, 4 + i, player.getPersistentID()));
				}
				break;
			case 0:
				processor.setPlayers((ArrayList<PolyZonePlayer>) array);
				PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, piece, player.getPersistentID()));
				break;
			case 1:
				processor.setActivePlayers((ArrayList<PolyZonePlayer>) array);
				PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, piece, player.getPersistentID()));
				break;
			case 2:
				processor.setDefaultAccess((HashMap<String, Access>) array);
				PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, piece, player.getPersistentID()));
				break;
			case 3:
				processor.setZones((ArrayList<PolyZone>) array);
				PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, piece, player.getPersistentID()));
				break;
			case 4:
				processor.setZones((ArrayList<PolyZone>) array);
				tempArray = new ArrayList();
				for (int i = 0; i < PolyZonesMod.processor.getZones().size(); i++)
				{
					tempArray.clear();
					tempArray.add(PolyZonesMod.processor.getZones().get(i));
					PolyZonesMod.snw.sendToServer(new PZMessage(PolyZonesMod.processor, 4 + i, player.getPersistentID()));
				}
				break;
		}
	}

	private void handleSPZMessage(PZMessage packet)
	{
		int piece = packet.piece;
		EntityPlayer player = PolyZonesMod.server.getPlayerList().getPlayerByUUID(packet.sender);
		if (player != null && PolyZonesMod.playersMove != null && player.getPersistentID() != null)
		{
			Object array = packet.array;
			PolyProcessor processor;
			PolyZonePlayer sender = PolyZonesMod.processor.getPlayer(packet.sender);
			if (sender == null)
			{
				PolyZonesMod.processor.addPlayer(packet.sender);
				sender = PolyZonesMod.processor.getPlayer(packet.sender);
			}

			if (receivingProcessors.get(sender) != null)
			{
				processor = receivingProcessors.get(sender);
			}
			else
			{
				processor = new PolyProcessor();
				receivingProcessors.put(sender, processor);
			}

			// System.out.println("server hi! " + piece);
			switch (piece)
			{
				case 0:
					processor.setPlayers((ArrayList<PolyZonePlayer>) array);
					if (processor.equals(PolyZonesMod.processor))
					{
						if (!PolyZonesMod.playersMove.get(player.getPersistentID()))
						{
							PolyZonesMod.playersMove.put(player.getPersistentID(), new Boolean(true));
							LoMaS_Utils.addChatMessage(player, ("Your data files have been confirmed. You are now unlocked."));
						}
					}
					else if (!processor.getPlayers().equals(PolyZonesMod.processor.getPlayers()))
					{
						PolyZonesMod.playersMove.put(player.getPersistentID(), new Boolean(false));
						LoMaS_Utils.addChatMessage(player, ("Your data files are out of sync. Please wait."));
						PolyZonesMod.snw.sendTo(new PZMessage(PolyZonesMod.processor, 0, player.getPersistentID()), PolyZonesMod.server.getPlayerList().getPlayerByUUID(packet.sender));
					}
					break;
				case 1:
					processor.setActivePlayers((ArrayList<PolyZonePlayer>) array);
					if (processor.equals(PolyZonesMod.processor))
					{
						if (!PolyZonesMod.playersMove.get(player.getPersistentID()))
						{
							PolyZonesMod.playersMove.put(player.getPersistentID(), new Boolean(true));
							LoMaS_Utils.addChatMessage(player, ("Your data files have been confirmed. You are now unlocked."));
						}
					}
					else if (!processor.getActivePlayers().equals(PolyZonesMod.processor.getActivePlayers()))
					{
						PolyZonesMod.snw.sendTo(new PZMessage(PolyZonesMod.processor, 1, player.getPersistentID()), PolyZonesMod.server.getPlayerList().getPlayerByUUID(packet.sender));
					}
					break;
				case 2:
					processor.setDefaultAccess((HashMap<String, Access>) array);
					if (processor.equals(PolyZonesMod.processor))
					{
						if (!PolyZonesMod.playersMove.get(player.getPersistentID()))
						{
							PolyZonesMod.playersMove.put(player.getPersistentID(), new Boolean(true));
							LoMaS_Utils.addChatMessage(player, ("Your data files have been confirmed. You are now unlocked."));
						}
					}
					else if (!processor.getDefaultAccess().equals(PolyZonesMod.processor.getDefaultAccess()))
					{
						PolyZonesMod.snw.sendTo(new PZMessage(PolyZonesMod.processor, 2, player.getPersistentID()), PolyZonesMod.server.getPlayerList().getPlayerByUUID(packet.sender));
					}
					break;
				case 3:
					processor.setZones((ArrayList<PolyZone>) array);
					if (processor.equals(PolyZonesMod.processor))
					{
						if (!PolyZonesMod.playersMove.get(player.getPersistentID()))
						{
							PolyZonesMod.playersMove.put(player.getPersistentID(), new Boolean(true));
							LoMaS_Utils.addChatMessage(player, ("Your data files have been confirmed. You are now unlocked."));
						}
						return;
					}
					else if (!processor.getZones().equals(PolyZonesMod.processor.getZones()))
					{
						PolyZonesMod.snw.sendTo(new PZMessage(PolyZonesMod.processor, 3, player.getPersistentID()), PolyZonesMod.server.getPlayerList().getPlayerByUUID(packet.sender));
					}
					break;
				default:
					String checksum = array != null && ((ArrayList) array).size() > 0 ? (String) ((ArrayList) array).get(0) : null;
					if (checksum == null || checksum.isEmpty())
					{
						PolyZonesMod.snw.sendTo(new PZMessage(PolyZonesMod.processor, -1, player.getPersistentID()), PolyZonesMod.server.getPlayerList().getPlayerByUUID(packet.sender));
						return;
					}
					PolyZone importZone = null;
					for (PolyZone zone : PolyZonesMod.processor.getZones())
					{
						if (zone.getChecksum().equals(checksum))
						{
							System.out.println("server check! " + checksum + " " + zone.getChecksum());
							importZone = zone;
							break;
						}
					}
					if (importZone != null && !processor.getZones().contains(importZone) && PolyZonesMod.processor.getZones().contains(importZone))
					{
						processor.getZones().add(importZone);
					}
					if (processor.equals(PolyZonesMod.processor))
					{
						if (!PolyZonesMod.playersMove.get(player.getPersistentID()))
						{
							PolyZonesMod.playersMove.put(player.getPersistentID(), new Boolean(true));
							LoMaS_Utils.addChatMessage(player, ("Your data files have been confirmed. You are now unlocked."));
						}
						return;
					}
					else if (!processor.getZones().get(piece - 4).equals(PolyZonesMod.processor.getZones().get(piece - 4)))
					{
						if (this.hasFirstZone != null && (this.hasFirstZone.get(sender) == null || !this.hasFirstZone.get(sender)))
						{
							this.hasFirstZone.put(sender, new Boolean(true));
							PolyZonesMod.snw.sendTo(new PZMessage(PolyZonesMod.processor, -1, player.getPersistentID()), PolyZonesMod.server.getPlayerList().getPlayerByUUID(packet.sender));
						}
					}
					break;
			}
		}
	}
}
