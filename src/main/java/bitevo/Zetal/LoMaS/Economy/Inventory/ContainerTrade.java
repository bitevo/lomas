package bitevo.Zetal.LoMaS.Economy.Inventory;

import bitevo.Zetal.LoMaS.Economy.Handlers.EconEventHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerTrade extends Container
{
	/** Determines if inventory manipulation should be handled. */
	protected final EntityPlayer thePlayer;
	private EntityPlayer receivingPlayer;
	public InventoryTrade tradeInventory = null;
	public boolean theReady, traderReady;

	public ContainerTrade(InventoryPlayer par1InventoryPlayer, InventoryTrade trade)
	{
		this.thePlayer = par1InventoryPlayer.player;
		this.receivingPlayer = EconEventHandler.getTradePartner(thePlayer);
		this.tradeInventory = trade;
		this.theReady = false;
		this.traderReady = false;
		int i;
		int j;

		// Player inventory
		for (i = 0; i < 3; ++i)
		{
			for (j = 0; j < 9; ++j)
			{
				this.addSlotToContainer(new SlotCustom(par1InventoryPlayer, j + (i + 1) * 9, 8 + j * 18, 84 + i * 18));
			}
		}
		// Player hotbar
		for (i = 0; i < 9; ++i)
		{
			this.addSlotToContainer(new SlotCustom(par1InventoryPlayer, i, 8 + i * 18, 142));
		}

		// Player coin inventory
		for (j = 0; j < 3; ++j)
		{
			this.addSlotToContainer(new SlotCoin(par1InventoryPlayer, 41 + j, 35 + j * 18, 7));
		}

		// Sending inventory
		for (i = 0; i < 3; ++i)
		{
			for (j = 0; j < 3; ++j)
			{
				this.addSlotToContainer(new Slot(trade, j + (i * 3), 17 + j * 18, 27 + i * 18));
			}
		}

		// Receiving inventory
		for (i = 0; i < 3; ++i)
		{
			for (j = 0; j < 3; ++j)
			{
				this.addSlotToContainer(new SlotLocked(trade, 9 + j + (i * 3), 107 + j * 18, 27 + i * 18));
			}
		}
	}

	@Override
	public void onContainerClosed(EntityPlayer playerIn)
	{
		super.onContainerClosed(playerIn);

		for (int i = 0; i < 9; ++i)
		{
			ItemStack itemstack = this.tradeInventory.removeStackFromSlot(i);

			if (itemstack != null)
			{
				playerIn.inventory.addItemStackToInventory(itemstack);
			}
		}

		this.tradeInventory.setInventorySlotContents(0, (ItemStack) null);
	}

	@Override
	public void detectAndSendChanges()
	{
		super.detectAndSendChanges();

		if (this.receivingPlayer != null && this.receivingPlayer.openContainer instanceof ContainerTrade)
		{
			ContainerTrade tradeCon = (ContainerTrade) this.receivingPlayer.openContainer;
			if (shouldUpdate())
			{
				// System.out.println(tradeCon.tradeInventory.player2Trade + " " + this.tradeInventory.player1Trade);
				tradeCon.tradeInventory.player2Trade = this.tradeInventory.player1Trade;
				tradeCon.receivingPlayer = this.thePlayer;
				tradeCon.detectAndSendChanges();
			}

			if (!canTradeFit())
			{
				theReady = false;
			}
		}

		if (!this.thePlayer.worldObj.isRemote)
		{
			if (this.receivingPlayer == null)
			{
				this.receivingPlayer = EconEventHandler.getTradePartner(thePlayer);
			}

			if (this.tradeInventory != null && this.receivingPlayer == null)
			{
				this.thePlayer.closeScreen();
				return;
			}

			EntityPlayerMP recPlayMP = (EntityPlayerMP) this.receivingPlayer;
			if (recPlayMP != null)
			{
				if (!recPlayMP.connection.netManager.isChannelOpen())
				{
					this.thePlayer.closeScreen();
				}
			}
		}
	}

	public boolean shouldUpdate()
	{
		ContainerTrade tradeCon = (ContainerTrade) this.receivingPlayer.openContainer;
		boolean noUpdate = true;
		for (int i = 0; i < this.tradeInventory.player1Trade.length; i++)
		{
			// This is just *constantly* fucking firing for some reason. :?
			if (!ItemStack.areItemStacksEqual(tradeCon.tradeInventory.player2Trade[i], this.tradeInventory.player1Trade[i]))
			{
				noUpdate = false;
				break;
			}
		}
		return !noUpdate;
	}

	public boolean canTradeFit()
	{
		boolean canFit = true;
		if (this.tradeInventory != null)
		{
			InventoryPlayer tempInventory = (new InventoryPlayer(null));
			this.copyInventoryOnto(tempInventory, this.thePlayer.inventory);
			ItemStack[] insertStacks = this.tradeInventory.player2Trade;
			for (ItemStack stack : insertStacks)
			{
				if (stack != null && stack.stackSize > 0)
				{
					if (!tempInventory.addItemStackToInventory(stack.copy()))
					{
						canFit = false;
					}
				}
			}
		}
		return canFit;
	}

	private void copyInventoryOnto(InventoryPlayer newInv, InventoryPlayer oldInv)
	{
		for (int i = 0; i < newInv.getSizeInventory(); ++i)
		{
			if (oldInv.getStackInSlot(i) != null)
			{
				newInv.setInventorySlotContents(i, oldInv.getStackInSlot(i).copy());
			}
			else
			{
				newInv.setInventorySlotContents(i, null);
			}
		}

		newInv.currentItem = oldInv.currentItem;
	}

	@Override
	public ItemStack slotClick(int slotId, int clickedButton, ClickType mode, EntityPlayer player)
	{
		return super.slotClick(slotId, clickedButton, mode, player);
	}

	@Override
	public boolean canInteractWith(EntityPlayer par1EntityPlayer)
	{
		return true;
	}

	/**
	 * Called when a player shift-clicks on a slot. You must override this or you will crash when someone does that.
	 */
	@Override
	public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int par2)
	{
		ItemStack itemstack = null;
		Slot slot = (Slot) this.inventorySlots.get(par2);

		if (slot != null && slot.getHasStack())
		{
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (par2 <= 26)
			{
				if (!this.mergeItemStack(itemstack1, 33, 57, true))
				{
					return null;
				}
			}
			else if (par2 <= 68 && par2 >= 33)
			{
				if (!this.mergeItemStack(itemstack1, 0, 27, true))
				{
					return null;
				}
			}

			if (itemstack1.stackSize == 0)
			{
				slot.putStack((ItemStack) null);
			}
			else
			{
				slot.onSlotChanged();
			}

			if (itemstack1.stackSize == itemstack.stackSize)
			{
				return null;
			}

			slot.onPickupFromSlot(par1EntityPlayer, itemstack1);
		}

		return itemstack;
	}
}
