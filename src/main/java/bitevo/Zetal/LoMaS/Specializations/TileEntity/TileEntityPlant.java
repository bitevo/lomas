package bitevo.Zetal.LoMaS.Specializations.TileEntity;

import java.util.UUID;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class TileEntityPlant extends TileEntityQualityBlock
{
	private UUID owner;
	/**
	 * Quality is a scale 0-150, with a base of 100. Every tick that a plant is unfertilized, it drops by 15. 0-75 = Poor, 75-100 = Decent, 100-150 = Superb. Fertilizing a plant raises its quality by
	 * 5.
	 */
	private boolean isFertilized = false;
	/**
	 * Measured in half-seconds.
	 */
	private int fertTime = 0;
	
	private LoMaS_Player lPlayer;

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound)
	{
		super.writeToNBT(compound);
		if (this.getOwner() != null)
		{
			compound.setString("owner", getOwner().toString());
		}
		compound.setBoolean("isFertilized", this.isFertilized);
		compound.setInteger("fertTime", this.fertTime);
		return compound;
	}

	@Override
	public void readFromNBT(NBTTagCompound compound)
	{
		super.readFromNBT(compound);
		if (compound.hasKey("owner"))
		{
			this.setOwner(UUID.fromString(compound.getString("owner")));
		}
		if(this.getOwner() != null)
		{
			this.lPlayer = LoMaS_Player.getLoMaSPlayer(getOwner());
		}
		this.isFertilized = compound.getBoolean("isFertilized");
		this.fertTime = compound.getInteger("fertTime");
	}

	@Override
	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newState)
	{
		return !oldState.getBlock().equals(newState.getBlock());
	}

	public void setOwner(UUID owner)
	{
		this.owner = owner;
		if(owner != null)
		{
			this.lPlayer = LoMaS_Player.getLoMaSPlayer(owner);
		}
	}

	public UUID getOwner()
	{
		return owner;
	}

	public boolean isFertilized()
	{
		return isFertilized;
	}

	public void setFertilized(boolean isFertilized)
	{
		if (isFertilized)
		{
			this.setFertTime(2400);
		}
		this.isFertilized = isFertilized;
	}

	/**
	 * Measured in half-seconds.
	 * 
	 * @return
	 */
	public int getFertTime()
	{
		return fertTime;
	}

	/**
	 * Measured in half-seconds.
	 * 
	 * @param fertTime
	 */
	public void setFertTime(int fertTime)
	{
		this.fertTime = fertTime;
	}

	public LoMaS_Player getlPlayer()
	{
		return lPlayer;
	}

	public void setlPlayer(LoMaS_Player lPlayer)
	{
		this.lPlayer = lPlayer;
	}

	@Override
	public SPacketUpdateTileEntity getUpdatePacket()
	{
		return new SPacketUpdateTileEntity(pos, 0, this.getUpdateTag());
	}

	@Override
	public NBTTagCompound getUpdateTag()
	{
		return this.writeToNBT(new NBTTagCompound());
	}

	@Override
	public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt)
	{
		NBTTagCompound nbttagcompound = pkt.getNbtCompound();
		this.readFromNBT(nbttagcompound);
	}
}
