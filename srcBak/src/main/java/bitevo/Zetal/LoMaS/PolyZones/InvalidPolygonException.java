/**
 * 
 */
package bitevo.Zetal.LoMaS.PolyZones;

import java.io.Serializable;

/**
 * @author Pwebbz
 * 
 */
public class InvalidPolygonException extends Exception implements Serializable {

	private String error = "Could not create Polygon!";

	public InvalidPolygonException() {
	}

	public InvalidPolygonException(String arg0) {
		error = arg0;
	}

	public String getError() {
		return error;
	}

}
