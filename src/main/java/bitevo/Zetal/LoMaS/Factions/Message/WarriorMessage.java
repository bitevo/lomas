package bitevo.Zetal.LoMaS.Factions.Message;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.UUID;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Faction;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class WarriorMessage implements IMessage
{
	public UUID warriorID;
	public UUID commanderID;
	public int command = -1;

	public WarriorMessage()
	{
	}

	public WarriorMessage(UUID warrior, UUID commander, int com)
	{
		this.warriorID = warrior;
		this.commanderID = commander;
		this.command = com;
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.warriorID = UUID.fromString(ByteBufUtils.readUTF8String(buf));
		this.commanderID = UUID.fromString(ByteBufUtils.readUTF8String(buf));
		this.command = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, this.warriorID.toString());
		ByteBufUtils.writeUTF8String(buf, this.commanderID.toString());
		buf.writeInt(this.command);
	}
}
