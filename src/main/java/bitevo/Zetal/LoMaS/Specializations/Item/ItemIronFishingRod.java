package bitevo.Zetal.LoMaS.Specializations.Item;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import bitevo.Zetal.LoMaS.Specializations.Entity.EntitySpecialFishHook;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityFishHook;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemFishingRod;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;

public class ItemIronFishingRod extends ItemFishingRod
{

	public ItemIronFishingRod()
	{
		super();
        this.setMaxDamage(192);
		this.setCreativeTab(LoMaS_Utils.tabLoMaS);
	}

    public ActionResult<ItemStack> onItemRightClick(ItemStack itemStackIn, World worldIn, EntityPlayer playerIn, EnumHand hand)
    {
        return super.onItemRightClick(itemStackIn, worldIn, playerIn, hand);
    }
}
