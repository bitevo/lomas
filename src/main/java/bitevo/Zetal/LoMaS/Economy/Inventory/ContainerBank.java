package bitevo.Zetal.LoMaS.Economy.Inventory;

import bitevo.Zetal.LoMaS.Economy.EconomyMod;
import bitevo.Zetal.LoMaS.Economy.Capabilities.CapabilitiesProvider;
import bitevo.Zetal.LoMaS.Economy.Item.ItemCoin;
import bitevo.Zetal.LoMaS.Economy.Message.BankMessage;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerBank extends Container
{
	/** Determines if inventory manipulation should be handled. */
	protected final EntityPlayer thePlayer;

	public ContainerBank(InventoryPlayer par1InventoryPlayer, EntityPlayer player)
	{
		this.thePlayer = player;
		int i;
		int j;
		// BankSlots
		for (i = 0; i < 3; ++i)
		{
			for (j = 0; j < 9; ++j)
			{
				this.addSlotToContainer(new SlotCustom(player.getCapability(CapabilitiesProvider.PLAYER_CAP, null).getInventoryBank(), i * 9 + j, 8 + j * 18, 18 + i * 18));
			}
		}

		// Bank Input coin inventory
		for (j = 0; j < 3; ++j)
		{
			this.addSlotToContainer(new SlotBankInput(player.getCapability(CapabilitiesProvider.PLAYER_CAP, null).getInventoryBank(), 27 + j, 182 + j * 18, 81));
		}

		// Bank Output coin inventory
		for (j = 0; j < 3; ++j)
		{
			this.addSlotToContainer(new SlotBankOutput(player.getCapability(CapabilitiesProvider.PLAYER_CAP, null).getInventoryBank(), 30 + j, 182 + j * 18, 116));
		}

		// Player inventory
		for (i = 0; i < 3; ++i)
		{
			for (j = 0; j < 9; ++j)
			{
				this.addSlotToContainer(new SlotCustom(par1InventoryPlayer, j + (i + 1) * 9, 8 + j * 18, 86 + i * 18));
			}
		}
		// Player hotbar
		for (i = 0; i < 9; ++i)
		{
			this.addSlotToContainer(new SlotCustom(par1InventoryPlayer, i, 8 + i * 18, 144));
		}

		// Player coin inventory
		for (j = 0; j < 3; ++j)
		{
			this.addSlotToContainer(new SlotCoin(par1InventoryPlayer, 41 + j, 182 + j * 18, 36));
		}

		if (!player.worldObj.isRemote)
		{
			LoMaS_Player ep = LoMaS_Player.getLoMaSPlayer(player);
			EconomyMod.snw.sendTo(new BankMessage(ep.getBankCC()), (EntityPlayerMP) player);
		}
		this.putStackInSlot(30, new ItemStack(EconomyMod.copper_cc));
		this.putStackInSlot(31, new ItemStack(EconomyMod.silver_cc));
		this.putStackInSlot(32, new ItemStack(EconomyMod.gold_cc));
	}

	@Override
	public ItemStack slotClick(int slotId, int clickedButton, ClickType mode, EntityPlayer player)
	{
		ItemStack apply = player.inventory.getItemStack();
		// System.out.println("Mode: " + mode + " Button: " + clickedButton + " " + slotId);
		if (slotId > 68 && apply != null && apply.getItem() instanceof ItemCoin && slotId >= 0)
		{
			if (mode == ClickType.PICKUP || mode == ClickType.PICKUP_ALL)
			{
				int coins = 0;
				for (int k = 0; k < 3; k++)
				{
					if (player.inventory.getStackInSlot(41 + k) instanceof ItemStack)
					{
						coins += ((ItemStack) player.inventory.getStackInSlot(41 + k)).stackSize;
					}
				}
				if (clickedButton == 0)
				{
					ItemStack target = this.getSlot(slotId).getStack();
					if (((target == null || (target != null && target.isItemEqual(apply))) && coins + apply.stackSize > 64) || (target != null && !target.isItemEqual(apply) && (coins + apply.stackSize - target.stackSize) > 64))
					{
						ItemStack splitter = apply.copy();
						splitter.stackSize = Math.min(64 - coins, apply.stackSize);
						return splitter;
					}
					else if (((target == null || (target != null && target.isItemEqual(apply))) && coins + apply.stackSize <= 64) || (target != null && !target.isItemEqual(apply) && (coins + apply.stackSize - target.stackSize) <= 64))
					{
						return super.slotClick(slotId, clickedButton, mode, player);
					}
					return null;
				}
				else if (clickedButton == 1)
				{
					if (coins + 1 > 64)
					{
						return null;
					}
				}
			}

			if (mode == ClickType.QUICK_CRAFT)
			{
				return null;
			}
		}
		return super.slotClick(slotId, clickedButton, mode, player);
	}

	@Override
	public boolean canInteractWith(EntityPlayer par1EntityPlayer)
	{
		return true;
	}

	/**
	 * Called when a player shift-clicks on a slot. You must override this or you will crash when someone does that.
	 */
	@Override
	public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int par2)
	{
		ItemStack itemstack = null;
		Slot slot = (Slot) this.inventorySlots.get(par2);

		if (slot != null && slot.getHasStack())
		{
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (par2 <= 26)
			{
				if (!this.mergeItemStack(itemstack1, 33, 69, true))
				{
					return null;
				}
			}
			else if (par2 <= 68 && par2 >= 33)
			{
				if (!this.mergeItemStack(itemstack1, 0, 27, true))
				{
					return null;
				}
			}

			if (itemstack1.stackSize == 0)
			{
				slot.putStack((ItemStack) null);
			}
			else
			{
				slot.onSlotChanged();
			}

			if (itemstack1.stackSize == itemstack.stackSize)
			{
				return null;
			}

			slot.onPickupFromSlot(par1EntityPlayer, itemstack1);
		}

		return itemstack;
	}
}
