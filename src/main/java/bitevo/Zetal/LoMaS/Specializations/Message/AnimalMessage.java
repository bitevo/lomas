package bitevo.Zetal.LoMaS.Specializations.Message;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class AnimalMessage implements IMessage
{
	/** The ID of this entity. */
	public int entityId;
	public int age;
	public int hunger;
	public int sickness;
	public boolean isReady;

	public AnimalMessage()
	{
	}

	public AnimalMessage(int entityid, int age, int hunger, int sickness, boolean isReady)
	{
		this.entityId = entityid;
		this.age = age;
		this.hunger = hunger;
		this.sickness = sickness;
		this.isReady = isReady;
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.entityId = buf.readInt();
		this.age = buf.readInt();
		this.hunger = buf.readInt();
		this.sickness = buf.readInt();
		this.isReady = buf.readBoolean();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeInt(entityId);
		buf.writeInt(this.age);
		buf.writeInt(this.hunger);
		buf.writeInt(this.sickness);
		buf.writeBoolean(this.isReady);
	}
}
