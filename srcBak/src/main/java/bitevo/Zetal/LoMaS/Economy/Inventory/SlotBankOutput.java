package bitevo.Zetal.LoMaS.Economy.Inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import bitevo.Zetal.LoMaS.Economy.EconomyMod;
import bitevo.Zetal.LoMaS.Economy.Item.ItemCoin;
import bitevo.Zetal.LoMaS.Economy.Message.BankMessage;

public class SlotBankOutput extends Slot
{
	public InventoryBank bank;

	public SlotBankOutput(InventoryBank inventoryIn, int index, int xPosition, int yPosition)
	{
		super(inventoryIn, index, xPosition, yPosition);
		this.bank = inventoryIn;
	}

	public boolean isItemValid(ItemStack stack)
	{
		return false;
	}

	@Override
	public boolean canTakeStack(EntityPlayer player)
	{
		ItemStack stackTaken = this.getStack();
		if (stackTaken != null && stackTaken.getItem() instanceof ItemCoin)
		{
			int takenValue = EconomyMod.getRawValue((ItemCoin) stackTaken.getItem()) * stackTaken.stackSize;
			int ccStorage = LoMaS_Player.getLoMaSPlayer(player).getBankCC();
			if (ccStorage >= takenValue)
			{
				return true;
			}
		}
		return false;
	}

	public void onPickupFromSlot(EntityPlayer player, ItemStack stack)
	{
		if (stack != null && stack.getItem() instanceof ItemCoin)
		{
			int takenValue = EconomyMod.getRawValue((ItemCoin) stack.getItem());
			int ccStorage = LoMaS_Player.getLoMaSPlayer(player).getBankCC();

			if (!player.worldObj.isRemote && ccStorage >= takenValue)
			{
				LoMaS_Player.getLoMaSPlayer(player).setBankCC(ccStorage - takenValue);
				LoMaS_Player ep = LoMaS_Player.getLoMaSPlayer(player);
				EconomyMod.snw.sendTo(new BankMessage(ep.getBankCC()), (EntityPlayerMP) player);
			}
			this.putStack(new ItemStack(stack.getItem()));
			this.onSlotChanged();
		}
	}
}
