package bitevo.Zetal.LoMaS.Factions;

import java.io.Serializable;
import java.util.ArrayList;

import bitevo.Zetal.LoMaS.Shared.LoMaS_Player;
import net.minecraft.util.math.BlockPos;

public class PatrolRoute implements Serializable
{
	public static ArrayList<PatrolRoute> routes = new ArrayList<PatrolRoute>();
	private static final long serialVersionUID = 1L;
	
	private ArrayList<BlockPos> points = new ArrayList<BlockPos>();
	
	public static PatrolRoute getRouteForPoint(BlockPos pos)
	{
		for(PatrolRoute route : routes)
		{
			if(route.containsPoint(pos))
			{
				return route;
			}
		}
		return null;
	}

	public PatrolRoute(){}
	
	public PatrolRoute(BlockPos... pos)
	{
		for(int i = 0; i < pos.length; i++)
		{
			this.addPoint(pos[i]);
		}
	}
	
	public BlockPos getNextPoint(boolean forward, BlockPos currentPos)
	{
		int nextIndex = points.indexOf(currentPos);
		nextIndex = forward ? nextIndex++ : nextIndex--;
		return points.get(nextIndex);
	}
	
	
	public void addPoint(BlockPos pos)
	{
		if(!points.contains(pos))
		{
			points.add(pos);
		}
	}
	
	public void removePoint(BlockPos pos)
	{
		if(points.contains(pos))
		{
			points.remove(pos);
		}
	}
	
	public boolean containsPoint(BlockPos pos)
	{
		return points.contains(pos);
	}
}
