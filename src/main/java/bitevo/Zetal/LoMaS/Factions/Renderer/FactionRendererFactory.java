package bitevo.Zetal.LoMaS.Factions.Renderer;

import bitevo.Zetal.LoMaS.Factions.Entity.EntityPigmanWarrior;
import bitevo.Zetal.LoMaS.Factions.Entity.EntitySiegeGolem;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraftforge.fml.client.registry.IRenderFactory;

public class FactionRendererFactory
{
	public class WarriorFactory implements IRenderFactory<EntityPigmanWarrior>
	{
		@Override
		public Render<? super EntityPigmanWarrior> createRenderFor(RenderManager manager)
		{
			return new RenderWarrior(manager, new ModelWarrior(), 0.5F);
		}
	}

	public class SiegeGolemFactory implements IRenderFactory<EntitySiegeGolem>
	{
		@Override
		public Render<? super EntitySiegeGolem> createRenderFor(RenderManager manager)
		{
			return new RenderSiegeGolem(manager, new ModelSiegeGolem(), 0.8F);
		}
	}
}
