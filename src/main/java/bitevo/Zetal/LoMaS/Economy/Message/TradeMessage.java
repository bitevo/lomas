package bitevo.Zetal.LoMaS.Economy.Message;

import java.util.UUID;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class TradeMessage implements IMessage
{
	public UUID p1;
	public boolean ready1, ready2;

	public TradeMessage()
	{
	}

	public TradeMessage(UUID p1, boolean ready1, boolean ready2)
	{
		this.p1 = p1;
		this.ready1 = ready1;
		this.ready2 = ready2;
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.p1 = UUID.fromString(ByteBufUtils.readUTF8String(buf));
		this.ready1 = buf.readBoolean();
		this.ready2 = buf.readBoolean();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, this.p1.toString());
		buf.writeBoolean(ready1);
		buf.writeBoolean(ready2);
	}
}
