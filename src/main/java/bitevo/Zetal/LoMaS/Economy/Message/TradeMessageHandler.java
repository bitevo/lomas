package bitevo.Zetal.LoMaS.Economy.Message;

import java.util.UUID;

import bitevo.Zetal.LoMaS.Economy.EconomyMod;
import bitevo.Zetal.LoMaS.Economy.Handlers.EconEventHandler;
import bitevo.Zetal.LoMaS.Economy.Inventory.ContainerTrade;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TradeMessageHandler implements IMessageHandler<TradeMessage, IMessage>
{
	@Override
	public IMessage onMessage(TradeMessage message, MessageContext ctx)
	{
		if (ctx.side == Side.CLIENT)
		{
			handleCTradeMessage(message);
		}
		else if (ctx.side == Side.SERVER)
		{
			handleSTradeMessage(message);
		}
		return null;
	}

	@SideOnly(Side.CLIENT)
	private void handleCTradeMessage(TradeMessage packet)
	{
		UUID theClient = packet.p1;
		boolean theReady = packet.ready1;
		boolean traderReady = packet.ready2;
		Minecraft minecraft = Minecraft.getMinecraft();
		EntityPlayer player = minecraft.thePlayer;
		if (theClient.equals(player.getPersistentID()))
		{
			if (player.openContainer instanceof ContainerTrade)
			{
				ContainerTrade con = (ContainerTrade) player.openContainer;
				// System.out.println("Client contact complete. Setting readies." + theReady + " " + traderReady);
				if (con.theReady != theReady)
				{
					con.theReady = theReady;
				}

				if (con.traderReady != traderReady)
				{
					con.traderReady = traderReady;
				}
			}
		}
	}

	@SideOnly(Side.SERVER)
	private void handleSTradeMessage(TradeMessage packet)
	{
		UUID clientFrom = packet.p1;
		boolean ready1 = packet.ready1;
		MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
		if (server.getEntityFromUuid(clientFrom) instanceof EntityPlayer)
		{
			EntityPlayerMP client = (EntityPlayerMP) server.getEntityFromUuid(clientFrom);
			EntityPlayerMP target = (EntityPlayerMP) EconEventHandler.getTradePartner(client);
			if (client != null && target != null && client.openContainer instanceof ContainerTrade && target.openContainer instanceof ContainerTrade)
			{
				ContainerTrade con1 = (ContainerTrade) client.openContainer;
				ContainerTrade con2 = (ContainerTrade) target.openContainer;
				if (EconEventHandler.getTradePartner(client).getPersistentID().equals(target.getPersistentID()))
				{
					// System.out.println("Server receives." + client + " " + target + " " + con1.theReady + " " + ready1);
					if (con1.theReady != ready1)
					{
						if (con1.canTradeFit())
						{
							con1.theReady = ready1;
						}
						else
						{
							con1.theReady = false;
						}
						// System.out.println("Contacting clients." + con1.theReady + " " + con2.theReady);
						EconomyMod.snw.sendTo(new TradeMessage(clientFrom, con1.theReady, con2.theReady), client);
						EconomyMod.snw.sendTo(new TradeMessage(target.getPersistentID(), con2.theReady, con1.theReady), target);
						if (con1.theReady && con2.theReady)
						{
							// System.out.println("Completing trade!");
							this.completeTrade(client, target);
						}
					}
				}
			}
		}
	}

	@SideOnly(Side.SERVER)
	public void completeTrade(EntityPlayerMP player1, EntityPlayerMP player2)
	{
		ContainerTrade con1 = (ContainerTrade) player1.openContainer;
		ContainerTrade con2 = (ContainerTrade) player2.openContainer;
		ItemStack[] player1Trade = con1.tradeInventory.player1Trade;
		ItemStack[] player2Trade = con2.tradeInventory.player1Trade;
		con1.tradeInventory.player1Trade = player2Trade;
		con1.tradeInventory.player2Trade = player1Trade;
		con2.tradeInventory.player1Trade = player1Trade;
		con2.tradeInventory.player2Trade = player2Trade;
		player1.closeScreen();
	}
}
