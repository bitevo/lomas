package bitevo.Zetal.LoMaS;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

import net.minecraft.entity.passive.EntityAnimal;

public class LoMaS_Animal implements Serializable
{
	public static ArrayList<LoMaS_Animal> animalList = new ArrayList<LoMaS_Animal>();
	private static final long serialVersionUID = 1L;

	private UUID id;
	private int breederLevel = 0;
	private int age = 0;
	private int sickness = 0;
	private int hunger = 20;
	private boolean isReady = false;

	private boolean beenTouched = false;
	private int removeTouchFlagTimer = 0;
	private UUID lastToucher;
	
	private int quality = 0;

	public transient EntityAnimal animal;

	private long lastSystemTime = System.currentTimeMillis();

	public static void addLoMaSAnimal(EntityAnimal eAnimal)
	{
		if (eAnimal != null)
		{
			if (getLoMaSAnimal(eAnimal) == null)
			{
				LoMaS_Animal p = new LoMaS_Animal();
				p.animal = eAnimal;
				p.setId(eAnimal.getPersistentID());
				animalList.add(p);
			}
			else
			{
				getLoMaSAnimal(eAnimal).animal = eAnimal;
			}
		}
		else
		{
			System.err.println("Tried to add a NULL animal to LoMaS_Animal!");
		}
	}

	public static LoMaS_Animal getLoMaSAnimal(EntityAnimal eAnimal)
	{
		LoMaS_Animal retAnimal = null;
		if (eAnimal != null)
		{
			for (int i = 0; i < animalList.size(); i++)
			{
				if (eAnimal.getPersistentID() == animalList.get(i).getId())
				{
					retAnimal = animalList.get(i);
					if (retAnimal.animal == null)
					{
						retAnimal.animal = eAnimal;
					}
				}
			}
		}
		return retAnimal;
	}

	public static LoMaS_Animal getLoMaSAnimal(UUID pid)
	{
		LoMaS_Animal retAnimal = null;
		for (int i = 0; i < animalList.size(); i++)
		{
			if (pid == animalList.get(i).getId())
			{
				retAnimal = animalList.get(i);
			}
		}
		return retAnimal;
	}

	public void copyTraitsFromLoMaSAnimal(LoMaS_Animal lAnimal)
	{
		this.setId(lAnimal.getId());
		this.setBreederLevel(lAnimal.getBreederLevel());
		this.age = lAnimal.age;
		this.sickness = lAnimal.sickness;
		this.hunger = lAnimal.hunger;
		this.isReady = lAnimal.isReady;
		this.beenTouched = lAnimal.beenTouched;
		this.removeTouchFlagTimer = lAnimal.removeTouchFlagTimer;
		this.lastToucher = lAnimal.lastToucher;
		this.quality = lAnimal.quality;
	}

	public int getAge()
	{
		return age;
	}

	public void setAge(int age)
	{
		this.age = age;
	}

	public int getHunger()
	{
		return hunger;
	}

	public void setHunger(int hunger)
	{
		this.hunger = hunger;
	}

	public int getSickness()
	{
		return sickness;
	}

	public void setSickness(int sickness)
	{
		this.sickness = sickness;
	}

	public boolean isReady()
	{
		return isReady;
	}

	public void setReady(boolean isReady)
	{
		this.isReady = isReady;
	}

	public boolean getBeenTouched()
	{
		return beenTouched;
	}

	public void setBeenTouched(boolean beenTouched)
	{
		this.beenTouched = beenTouched;
		if (beenTouched)
		{
			this.setRemoveTouchFlagTimer(3000);
		}
	}

	public int getRemoveTouchFlagTimer()
	{
		return removeTouchFlagTimer;
	}

	public void setRemoveTouchFlagTimer(int removeTouchFlagTimer)
	{
		this.removeTouchFlagTimer = removeTouchFlagTimer;
	}

	public void setLastToucher(UUID toucher)
	{
		this.lastToucher = toucher;
	}

	public UUID getLastToucher()
	{
		return this.lastToucher;
	}

	public UUID getId()
	{
		return id;
	}

	public void setId(UUID id)
	{
		this.id = id;
	}

	public long getLastSystemTime()
	{
		return lastSystemTime;
	}

	public void setLastSystemTime(long lastSystemTime)
	{
		this.lastSystemTime = lastSystemTime;
	}

	public int getBreederLevel()
	{
		return breederLevel;
	}

	public void setBreederLevel(int breederLevel)
	{
		this.breederLevel = breederLevel;
	}

	public int getQuality()
	{
		return quality;
	}

	public void setQuality(int quality)
	{
		this.quality = quality;
	}
}
