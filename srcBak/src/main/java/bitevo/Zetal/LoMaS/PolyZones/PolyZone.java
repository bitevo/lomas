package bitevo.Zetal.LoMaS.PolyZones;

import java.io.Serializable;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class PolyZone implements Serializable {

	private BlockPolygon polygon;
	private Permissions permissions;
	private String name;
	private int lowX = -1, highX = -1, lowZ = -1, highZ = -1;
	private BlockLoc[][] points;
	private BlockLoc[] chunks;

	// make it so this class has the y values and handles there checking.

	public PolyZone(String n) {
		name = n;
		polygon = new BlockPolygon();
		permissions = new Permissions();
		permissions.getDefaultPermissions().setPolyZoneName(n);
	}

	public PolyZone(String n, BlockPolygon p) {
		name = n;
		polygon = p;
		lowX = (int) polygon.getWest().getX();
		highX = (int) polygon.getEast().getX();
		lowZ = (int) polygon.getSouth().getZ();
		highZ = (int) polygon.getNorth().getZ();
		points = polygon.getContainedPoints();
		chunks = polygon.getContainedChunks();
		permissions = new Permissions();
		permissions.getDefaultPermissions().setPolyZoneName(n);
	}

	public Permissions getPermissions() {
		return permissions;
	}

	public void setName(String n) {
		name = n;
	}

	public String getName() {
		return name;
	}

	public BlockPolygon getPolygon() {
		return polygon;
	}

	public void setPolygon(BlockPolygon x) {
		polygon = x;
		lowX = (int) polygon.getWest().getX();
		highX = (int) polygon.getEast().getX();
		lowZ = (int) polygon.getSouth().getZ();
		highZ = (int) polygon.getNorth().getZ();
		points = polygon.getContainedPoints();
	}

	public boolean isInside(BlockLoc p, World w) 
	{
		if (w.provider.getDimension() != 0) {
			return false;
		}
		
		if(name.equalsIgnoreCase("sedrons")){
			if(p.getX() > 546 && p.getX() < 915){
				if (p.getZ() > 1368 && p.getZ() < 1817){
					return true;
				}
			}
		}
		if(name.equalsIgnoreCase("pwebbz")){
			if(p.getX() > 415 && p.getX() < 620){
				if(p.getZ() > -1010 && p.getZ() < -800){
					return true;
				}
			}
		}
		if(name.equalsIgnoreCase("town")){
			if(p.getX() > -100 && p.getX() < 100){
				if(p.getZ() > -100 && p.getZ() < 100){
					return true;
				}
			}
		}
		if(name.equalsIgnoreCase("zetal")){
			if(p.getX() < -536 && p.getX() > -760){
				if (p.getZ() > 504 && p.getZ() < 755){
					return true;
				}
			}
		}
		if(name.equalsIgnoreCase("dylan")){
			if(p.getX() < -530 && p.getX() > -281){
				if (p.getZ() > 504 && p.getZ() < -630){
					return true;
				}
			}
		}
		
		int x = (int) p.getX();
		if (x >= lowX && x <= highX) {
			int z = (int) p.getZ();
			if (z >= lowZ && z <= highZ) {
				int column = x - lowX;
				int row = z - lowZ;
				try {
					if (points[column][row].equals(p)) {
						return true;
					}
				} catch (Exception lol) {
					return false;
				}
			}
		}
		return false;
	}

	public boolean isInside(World w, Chunk chunk) 
	{
		if(chunks != null)
		{
			if (w.provider.getDimension() != 0) {
				return false;
			}
			BlockLoc c = new BlockLoc(chunk.xPosition, -1, chunk.zPosition);
			for(int i = 0; i < chunks.length; i++)
			{
				if(chunks[i] != null && chunks[i].equalsIgnoreY(c))
				{
					return true;
				}
			}
		}
		else
		{
			if(polygon.getContainedChunks() != null)
			{
				chunks = polygon.getContainedChunks();
			}
		}
		return false;
	}

	public boolean isInside(EntityPlayer a) 
	{
		if (a.dimension != 0) {
			return false;
		}
		
		if(name.equalsIgnoreCase("sedrons")){
			if(a.posX > 546 && a.posX < 915){
				if (a.posZ > 1368 && a.posZ < 1817){
					return true;
				}
			}
		}
		if(name.equalsIgnoreCase("pwebbz")){
			if(a.posX > 415 && a.posX < 620){
				if(a.posZ > -1010 && a.posZ < -800){
					return true;
				}
			}
		}
		if(name.equalsIgnoreCase("town")){
			if(a.posX > -100 && a.posX < 100){
				if(a.posZ > -100 && a.posZ < 100){
					return true;
				}
			}
		}
		if(name.equalsIgnoreCase("zetal")){
			if(a.posX < -536 && a.posX > -760){
				if (a.posZ > 504 && a.posZ < 755){
					return true;
				}
			}
		}
		if(name.equalsIgnoreCase("dylan")){
			if(a.posX < -530 && a.posX > -281){
				if (a.posZ > 504 && a.posZ < -630){
					return true;
				}
			}
		}
		BlockLoc p = new BlockLoc((int)a.posX, (int)a.posY, (int)a.posZ);
		int x = (int) p.getX();
		if (x >= lowX && x <= highX) {
			int z = (int) p.getZ();
			if (z >= lowZ && z <= highZ) {
				int column = x - lowX;
				int row = z - lowZ;
				try {
					if (points[column][row].equals(p)) {
						return true;
					}
				} catch (Exception lol) {
					return false;
				}
			}
		}
		return false;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name + lowX + lowZ + highX + highZ;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof PolyZone)
		{
			PolyZone in = (PolyZone) o;
			if(in.toString().equalsIgnoreCase(this.toString()))
			{
				return true;
			}
		}
		return false;
	}
}
