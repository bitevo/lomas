package bitevo.Zetal.LoMaS.Factions.Block;

import java.util.Random;

import javax.annotation.Nullable;

import bitevo.Zetal.LoMaS.Factions.FactionsMod;
import bitevo.Zetal.LoMaS.Factions.TileEntity.TileEntityObelisk;
import bitevo.Zetal.LoMaS.Shared.LoMaS_Utils;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockObelisk extends BlockContainer
{
	public BlockObelisk(Material par2Material)
	{
		super(par2Material);
		this.setDefaultState(this.blockState.getBaseState());
		this.setHardness(50.0F);
		this.setResistance(2000.0F);
		this.setHarvestLevel("pickaxe", 2);
		this.setCreativeTab(LoMaS_Utils.tabLoMaS);
	}

	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune)
	{
		return Item.getItemFromBlock(FactionsMod.OBELISK);
	}

	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.MODEL;
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, @Nullable ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		if (worldIn.isRemote)
		{
			return true;
		}
		else
		{
			TileEntity tileentity = worldIn.getTileEntity(pos);
			if (tileentity instanceof TileEntityObelisk)
			{
				playerIn.openGui(FactionsMod.instance, 0, worldIn, pos.getX(), pos.getY(), pos.getZ());
			}

			return true;
		}
	}

	@Override
	public TileEntity createNewTileEntity(World world, int i)
	{
		return new TileEntityObelisk();
	}
}
