package bitevo.Zetal.LoMaS.Citizens.Inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemFishingRod;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemShears;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import bitevo.Zetal.LoMaS.Citizens.Entities.EntityPigman;

public class ContainerCitizenInventory extends Container
{
    private IInventory citizenInventory;
    private EntityPigman citizen;

    public ContainerCitizenInventory(IInventory playerInventory, final IInventory citInventory, final EntityPigman cit, EntityPlayer player)
    {
        this.citizenInventory = citInventory;
        this.citizen = cit;
        byte b0 = 3;
        citInventory.openInventory(player);
        int i = (b0 - 4) * 18;
        this.addSlotToContainer(new Slot(citInventory, 0, 8, 18)
        {
            /**
             * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
             */
            public boolean isItemValid(ItemStack stack)
            {
                return super.isItemValid(stack) && (stack.getItem() instanceof ItemTool || stack.getItem() instanceof ItemShears || stack.getItem() instanceof ItemFishingRod || stack.getItem() instanceof ItemHoe) && !this.getHasStack();
            }
        });
        int j;
        int k;

        for (j = 0; j < 3; ++j)
        {
            for (k = 0; k < 9; ++k)
            {
                this.addSlotToContainer(new Slot(playerInventory, k + j * 9 + 9, 8 + k * 18, 102 + j * 18 + i));
            }
        }

        for (j = 0; j < 9; ++j)
        {
            this.addSlotToContainer(new Slot(playerInventory, j, 8 + j * 18, 160 + i));
        }
    }

    public boolean canInteractWith(EntityPlayer playerIn)
    {
        return this.citizenInventory.isUseableByPlayer(playerIn) && this.citizen.isEntityAlive() && this.citizen.getDistanceToEntity(playerIn) < 8.0F;
    }

    /**
     * Take a stack from the specified inventory slot.
     */
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(index);

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if (index < this.citizenInventory.getSizeInventory())
            {
                if (!this.mergeItemStack(itemstack1, this.citizenInventory.getSizeInventory(), this.inventorySlots.size(), true))
                {
                    return null;
                }
            }
            else if (this.getSlot(1).isItemValid(itemstack1) && !this.getSlot(1).getHasStack())
            {
                if (!this.mergeItemStack(itemstack1, 1, 2, false))
                {
                    return null;
                }
            }
            else if (this.getSlot(0).isItemValid(itemstack1))
            {
                if (!this.mergeItemStack(itemstack1, 0, 1, false))
                {
                    return null;
                }
            }
            else if (this.citizenInventory.getSizeInventory() <= 2 || !this.mergeItemStack(itemstack1, 2, this.citizenInventory.getSizeInventory(), false))
            {
                return null;
            }

            if (itemstack1.stackSize == 0)
            {
                slot.putStack((ItemStack)null);
            }
            else
            {
                slot.onSlotChanged();
            }
        }

        return itemstack;
    }

    /**
     * Called when the container is closed.
     */
    public void onContainerClosed(EntityPlayer playerIn)
    {
        super.onContainerClosed(playerIn);
        this.citizenInventory.closeInventory(playerIn);
    }
}