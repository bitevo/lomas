package bitevo.Zetal.LoMaS.Specializations.Entity.ai;

import net.minecraft.entity.EntityLiving;
import net.minecraft.pathfinding.PathFinder;
import net.minecraft.pathfinding.PathNavigateGround;
import net.minecraft.world.World;

public class PathNavigateHover extends PathNavigateGround
{

	public PathNavigateHover(EntityLiving entitylivingIn, World worldIn)
	{
		super(entitylivingIn, worldIn);
	}

    protected PathFinder getPathFinder()
    {
        this.nodeProcessor = new HoverNodeProcessor();
        return new PathFinder(this.nodeProcessor);
    }
	
    protected boolean canNavigate()
    {
        return true;
    }
}
