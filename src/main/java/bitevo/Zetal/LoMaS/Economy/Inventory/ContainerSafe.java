package bitevo.Zetal.LoMaS.Economy.Inventory;

import bitevo.Zetal.LoMaS.Economy.Item.ItemCoin;
import bitevo.Zetal.LoMaS.Economy.TileEntity.TileEntitySafe;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerSafe extends Container
{
	private TileEntitySafe tileEntitySafe;

	public ContainerSafe(IInventory par1IInventory, TileEntitySafe par2TileEntitySafe)
	{
		this.tileEntitySafe = par2TileEntitySafe;
		int i;
		int j;

		for (i = 0; i < 3; ++i)
		{
			for (j = 0; j < 6; ++j)
			{
				this.addSlotToContainer(new SlotCoin(par2TileEntitySafe, j + i * 6, 35 + j * 18, 17 + i * 18));
			}
		}

		for (j = 0; j < 3; ++j)
		{
			this.addSlotToContainer(new SlotCoin(par1IInventory, par1IInventory.getSizeInventory() - 5 - j, 63 + j * 18, 91));
		}

		for (i = 0; i < 9; ++i)
		{
			this.addSlotToContainer(new SlotCustom(par1IInventory, i, 8 + i * 18, 125));
		}
	}

	/**
	 * Handles slot click. Args : slotId, clickedButton, mode (0 = basic click, 1 = shift click, 2 = Hotbar, 3 = pickBlock, 4 = Drop, 5 = ?, 6 = Double click), player
	 */
	@Override
	public ItemStack slotClick(int slotId, int clickedButton, ClickType mode, EntityPlayer player)
	{
		ItemStack apply = player.inventory.getItemStack();
		// System.out.println("Mode: " + mode + " Button: " + clickedButton + " " + slotId);
		if (slotId > 17 && apply != null && apply.getItem() instanceof ItemCoin && slotId >= 0)
		{
			if (mode == ClickType.PICKUP || mode == ClickType.PICKUP_ALL)
			{
				int coins = 0;
				for (int k = 0; k < 3; k++)
				{
					if (player.inventory.getStackInSlot(41 + k) instanceof ItemStack)
					{
						coins += ((ItemStack) player.inventory.getStackInSlot(41 + k)).stackSize;
					}
				}
				if (clickedButton == 0)
				{
					ItemStack target = this.getSlot(slotId).getStack();
					if (((target == null || (target != null && target.isItemEqual(apply))) && coins + apply.stackSize > 64) || (target != null && !target.isItemEqual(apply) && (coins + apply.stackSize - target.stackSize) > 64))
					{
						ItemStack splitter = apply.copy();
						splitter.stackSize = Math.min(64 - coins, apply.stackSize);
						return splitter;
					}
					else if (((target == null || (target != null && target.isItemEqual(apply))) && coins + apply.stackSize <= 64) || (target != null && !target.isItemEqual(apply) && (coins + apply.stackSize - target.stackSize) <= 64))
					{
						return super.slotClick(slotId, clickedButton, mode, player);
					}
					return null;
				}
				else if (clickedButton == 1)
				{
					if (coins + 1 > 64)
					{
						return null;
					}
				}
			}

			if (mode == ClickType.QUICK_CRAFT)
			{
				return null;
			}
		}
		return super.slotClick(slotId, clickedButton, mode, player);
	}

	@Override
	public boolean canInteractWith(EntityPlayer par1EntityPlayer)
	{
		return this.tileEntitySafe.isUseableByPlayer(par1EntityPlayer);
	}

	/**
	 * Called when a player shift-clicks on a slot. You must override this or you will crash when someone does that.
	 */
	@Override
	public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int par2)
	{
		ItemStack itemstack = null;
		Slot slot = (Slot) this.inventorySlots.get(par2);

		if (slot != null && slot.getHasStack())
		{
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (par2 <= 17)
			{
				if (!this.mergeItemStack(itemstack1, 18, 21, true))
				{
					return null;
				}
			}
			else if (par2 <= 25)
			{
				if (!this.mergeItemStack(itemstack1, 0, 18, true))
				{
					return null;
				}
			}

			if (itemstack1.stackSize == 0)
			{
				slot.putStack((ItemStack) null);
			}
			else
			{
				slot.onSlotChanged();
			}

			if (itemstack1.stackSize == itemstack.stackSize)
			{
				return null;
			}

			slot.onPickupFromSlot(par1EntityPlayer, itemstack1);
		}

		return itemstack;
	}

	/**
	 * Merges provided ItemStack with the first avaliable one in the container/player inventor between minIndex (included) and maxIndex (excluded). Args : stack, minIndex, maxIndex, negativDirection.
	 * /!\ the Container implementation do not check if the item is valid for the slot
	 */
	@Override
	protected boolean mergeItemStack(ItemStack stack, int startIndex, int endIndex, boolean useEndIndex)
	{
		if (stack != null && stack.getItem() instanceof ItemCoin && startIndex >= 18 && endIndex <= 21)
		{
			int coins = 0;
			for (int k = 0; k < 3; k++)
			{
				if (this.getSlot(startIndex).inventory.getStackInSlot(41 + k) instanceof ItemStack)
				{
					coins += ((ItemStack) this.getSlot(startIndex).inventory.getStackInSlot(41 + k)).stackSize;
				}
			}
			// System.out.println("Total: " + (coins + stack.stackSize));
			if (coins + stack.stackSize > 64)
			{
				return false;
			}
		}
		return super.mergeItemStack(stack, startIndex, endIndex, useEndIndex);
	}
}
