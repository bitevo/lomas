package bitevo.Zetal.LoMaS.Economy.Handlers;

import bitevo.Zetal.LoMaS.Economy.Inventory.InventoryPlayer;
import bitevo.Zetal.LoMaS.Economy.Inventory.SlotCoin;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerRespawnEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class FMLHandler
{

	@SubscribeEvent
	@SideOnly(Side.SERVER)
	public void playerServerRespawn(PlayerRespawnEvent event)
	{
		EntityPlayer player = event.player;
		if (!(player.inventory instanceof InventoryPlayer))
		{
			InventoryPlayer newInv = new InventoryPlayer(player);
			player.inventory = newInv;
		}
	}

	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void textureEvent(TextureStitchEvent.Pre event)
	{
		event.getMap().registerSprite(SlotCoin.slotBG);
	}
}
