package bitevo.Zetal.LoMaS.Factions;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class FactionsCommonProxy implements IGuiHandler
{
	public FactionsCommonProxy()
	{
	}

	public void init()
	{
	}

	public void registerRenderInformation()
	{
		// unused server side. -- see ClientProxy for implementation
	}

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		switch(ID)
		{
			case 0:
		}
		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		switch(ID)
		{
			case 0:
		}
		return null;
	}
}
